#include <chrono>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <map>
#include <memory>
#include <ranges>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>
#include <random>
#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/protocol.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/common.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::benchmarks {

  using namespace std::chrono_literals;

  namespace {
    auto const t0 = core::create_from_iso8601_string("2000-01-01T00:00:00Z"),
               t1 = core::create_from_iso8601_string("2018-10-17T10:00:00Z"),
               t2 = core::create_from_iso8601_string("2018-10-17T09:00:00Z"),
               t3 = core::create_from_iso8601_string("2018-10-18T10:00:00Z");
    auto const dt0 = core::deltahours(1);
    auto const N = 240;
    auto const ta0 = time_axis::fixed_dt{t0, core::calendar::YEAR, 1},
               ta1 = time_axis::fixed_dt{t1, dt0, static_cast<std::size_t>((t3 - t1) / dt0)};
  }

  auto make_hps(std::shared_ptr<stm_system> const &s) {
    auto hps = std::make_shared<stm_hps>(1, "test", "", s);
    auto rsv = std::make_shared<reservoir>(1, "test", "", hps);
    hps->reservoirs.push_back(rsv);
    auto u0 = std::make_shared<unit>(1, "u0", "", hps);
    hps->units.push_back(u0);
    auto p0 = std::make_shared<power_plant>(1, "p0", "", hps);
    hps->power_plants.push_back(p0);
    power_plant::add_unit(p0, u0);
    auto w0 = std::make_shared<waterway>(1, "w0", "", hps);
    hps->waterways.push_back(w0);
    auto g0 = std::make_shared<gate>(1, "g0", "");
    hydro_power::waterway::add_gate(w0, g0);
    auto w1 = std::make_shared<waterway>(2, "w1", "", hps);
    hps->waterways.push_back(w1);
    auto w2 = std::make_shared<waterway>(3, "w2", "", hps);
    hps->waterways.push_back(w2);
    auto w3 = std::make_shared<waterway>(4, "w3", "", hps);
    hps->waterways.push_back(w3);
    auto w4 = std::make_shared<waterway>(5, "w4", "", hps);
    hps->waterways.push_back(w4);
    waterway::input_from(w0, rsv, hydro_power::connection_role::flood);
    waterway::output_to(w0, w4);
    waterway::input_from(w1, rsv);
    waterway::output_to(w1, w2);
    waterway::output_to(w2, u0);
    waterway::input_from(w3, u0);
    waterway::output_to(w3, w4);
    return hps;
  }

  auto make_model() {
    auto model = std::make_shared<stm_system>(1, "test");
    model->hps.push_back(make_hps(model));
    auto market = std::make_shared<energy_market_area>(1, "NO1", "", model);
    model->market.push_back(market);
    return model;
  }

  auto wait_for_tune(srv::dstm::client &client, std::string const &model_id) {
    while (client.get_state({model_id}).state != stm::model_state::tuning)
      std::this_thread::sleep_for(10ms);
  }

  void parallel_tuning(int argc, char **argv) {

    auto const max_num_threads = 2 << 5;

    std::unique_ptr<srv::dstm::server> dstm_server = std::make_unique<srv::dstm::server>();
    std::vector<std::unique_ptr<srv::compute::server>> compute_servers;
    auto tmpdir = std::filesystem::temp_directory_path() /  ("tmp_file_" + std::to_string(std::random_device{}()));
    add_container(*dstm_server->dispatch.state->dtss, "test", tmpdir);

    for (auto i : std::views::iota(0, max_num_threads)) {
      auto &compute_server = *(compute_servers.emplace_back(std::make_unique<srv::compute::server>()));
      compute_server.set_listening_ip("127.0.0.1");
      auto compute_port = compute_server.start_server();
      add_compute_server(*dstm_server, fmt::format("127.0.0.1:{}", compute_port));
    }
    auto dstm_port = dstm_server->start_server();

    auto benchmark = [&](benchmark::State &state) {
      srv::dstm::client client{{.connection{fmt::format("127.0.0.1:{}", dstm_port)}}};

      auto const model = make_model();
      auto const model_key = fmt::format("model_{}", state.thread_index());

      auto model_url = [&](auto str) {
        return fmt::format("{}{}", url_fmt_root(model_key), str);
      };

      auto const requests0 = std::make_tuple(
        srv::add_model_request{
          model_key, model
      },
        [&] {
          srv::set_attrs_request req{
            .attrs{
                   {model_url("H1/R1.volume_level_mapping"),
               [&] -> any_attr {
                 auto a = std::make_shared<std::map<core::utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
                 a->emplace(
                   t0,
                   new hydro_power::xy_point_curve{
                     {{0, 80.0}, {2.0e6, 90.0}, {3.0e6, 95.0}, {5.0e6, 100.0}, {16e6, 105.0}}});
                 return a;
               }()},
                   {model_url("H1/R1.level.regulation.min"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta0}, 80.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("H1/R1.level.regulation.max"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta0}, 100.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("H1/R1.level.realised"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{time_axis::fixed_dt(t0, dt0, N)},
                   90.0,
                   time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("H1/R1.inflow.schedule"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{time_axis::fixed_dt(t1, dt0, N + 1)},
                   55.0,
                   time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("H1/R1.water_value.endpoint"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{time_axis::fixed_dt(t1, dt0, N)},
                   36.5e-6,
                   time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("H1/U1.generator_description"),
               [&]() -> any_attr {
                 auto a = std::make_shared<std::map<core::utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
                 hydro_power::xy_point_curve b{
                   {{.x = 20.0e6, .y = 1.0e2 * 0.96},
                   {.x = 40.0e6, .y = 1.0e2 * 0.98},
                   {.x = 60.0e6, .y = 1.0e2 * 0.99},
                   {.x = 80.0e6, .y = 1.0e2 * 0.98}}};
                 a->emplace(t0, std::make_shared<hydro_power::xy_point_curve>(std::move(b)));
                 return a;
               }()},
                   {model_url("H1/U1.turbine_description"),
               [&] -> any_attr {
                 auto a =
                   std::make_shared<std::map<core::utctime, std::shared_ptr<hydro_power::turbine_description>>>();
                 hydro_power::turbine_description b{.operating_zones{{.efficiency_curves{
                   {.xy_curve{
                      {{.x = 20.0, .y = 1.0e2 * 0.70},
                       {.x = 40.0, .y = 1.0e2 * 0.85},
                       {.x = 60.0, .y = 1.0e2 * 0.92},
                       {.x = 80.0, .y = 1.0e2 * 0.94},
                       {.x = 100.0, .y = 1.0e2 * 0.92},
                       {.x = 110.0, .y = 1.0e2 * 0.90}}},
                    .z = 70.0}}}}};
                 a->emplace(t0, std::make_shared<hydro_power::turbine_description>(std::move(b)));
                 return {a};
               }()},
                   {model_url("H1/P1.outlet_level"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta0}, 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("H1/W0.discharge.static_max"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta0}, 150.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {
                model_url("H1/W1.head_loss_coef"),
                {time_series::dd::apoint_ts(
                  std::make_shared<time_series::dd::gpoint_ts>(
                    time_axis::generic_dt{ta0}, 0.0003, time_series::ts_point_fx::POINT_AVERAGE_VALUE))},
              }, {model_url("H1/G1.flow_description"),
               [&] -> any_attr {
                 auto a = std::make_shared<
                   std::map<core::utctime, std::shared_ptr<std::vector<hydro_power::xy_point_curve_with_z>>>>();
                 auto [it, _] = a->emplace(t0, std::make_shared<std::vector<hydro_power::xy_point_curve_with_z>>());
                 it->second->push_back({.xy_curve{{{100.0, 0}, {101.5, 25}, {103.0, 80}, {104.0, 150}}}, .z = 0.0});
                 return {a};
               }()},
                   {model_url("H1/W2.head_loss_coef"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta0}, 0.00005, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("m1.price"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta1}, 40.0e-6, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("m1.max_buy"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta1}, 9999.0e6, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("m1.max_sale"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta1}, 9999.0e6, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}},
                   {model_url("m1.load"),
               {time_series::dd::apoint_ts(
                 std::make_shared<time_series::dd::gpoint_ts>(
                   time_axis::generic_dt{ta1}, 5.0e6, time_series::ts_point_fx::POINT_AVERAGE_VALUE))}}}
          };

          return req;
        }(),
        srv::start_tune_request{model_key},
        srv::tune_request{
          model_key,
          time_axis::generic_dt{ta1},
          {shop::shop_command::set_method_primal(),
           shop::shop_command::set_code_full(),
           shop::shop_command::start_sim(3),
           shop::shop_command::set_code_incremental(),
           shop::shop_command::start_sim(3)}});
      auto const requests1 = std::make_tuple(
        srv::stop_tune_request{model_key},
        [&] {
          srv::get_attrs_request req{url_planning_outputs(model_key, *model)};
          return req;
        }(),
        srv::remove_model_request{model_key});

      for (auto _ : state) {
        std::apply(
          [&](auto const &...request) {
            (client.send(request), ...);
          },
          requests0);
        wait_for_tune(client, model_key);
        std::apply(
          [&](auto const &...request) {
            (client.send(request), ...);
          },
          requests1);
      }
    };

    benchmark::RegisterBenchmark("parallel_tuning", benchmark)->ThreadRange(1, max_num_threads);
    benchmark::Initialize(&argc, argv);
    benchmark::RunSpecifiedBenchmarks();
    benchmark::Shutdown();
  }
}

int main(int argc, char **argv) {
  shyft::energy_market::stm::benchmarks::parallel_tuning(argc, argv);
}
