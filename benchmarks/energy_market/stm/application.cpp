#ifdef SHYFT_WITH_PYTHON

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <stop_token>
#include <string>
#include <thread>
#include <vector>
#include <random>
#include <benchmark/benchmark.h>
#include <boost/scope_exit.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/srv/application/client.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/energy_market/stm/srv/application/server.h>
#include <shyft/energy_market/stm/srv/task/client.h>
#include <shyft/py/energy_market/stm/srv/application.h>
#include <shyft/py/energy_market/stm/srv/task.h>

#include <pylifecycle.h>

auto make_benchmark(auto make_client, auto do_work) {
  return [=](benchmark::State &state) {
    std::vector<std::jthread> threads{};
    for (int i = 0; i < 10; i++)
      threads.emplace_back([&, client = make_client()](std::stop_token token) mutable {
        while (!token.stop_requested()) {
          do_work(client);
        }
      });

    auto client = make_client();
    for (auto _ : state) {
      benchmark::DoNotOptimize(do_work(client));
    }
  };
};

int main(int argc, char *argv[]) {
  Py_Initialize();

  auto const dir = std::filesystem::path( ("tmp_file_" + std::to_string(std::random_device{}())));
  BOOST_SCOPE_EXIT(&dir) {
    std::filesystem::remove_all(dir);
    if (auto ret = Py_FinalizeEx(); ret < 0) {
      std::exit(ret);
    }
  }
  BOOST_SCOPE_EXIT_END

  auto py_noop = [](std::int64_t /*mid*/, std::string /*args*/) {
    return true;
  };

  namespace stm = shyft::energy_market::stm;
  namespace application = stm::srv::experimental::application;
  auto const server_ip = "127.0.0.1";

  auto new_server = application::server();
  new_server.manager.callback<application::model_tag::task>() = std::function<bool(std::int64_t, std::string)>(py_noop);
  new_server.set_listening_ip(server_ip);
  auto const new_server_port = new_server.start_server();

  auto do_new_task_callback = make_benchmark(
    [&] {
      return application::client{{shyft::core::srv_connection{fmt::format("{}:{}", server_ip, new_server_port), 1000}}};
    },
    [](auto &client) {
      return client.send(application::task_fx_request{42, "bird"});
    });

  auto old_server = stm::srv::py_task_server(dir, {shyft::core::from_seconds(60), shyft::core::from_seconds(0.1)});
  auto const old_server_port = old_server.start_server();
  old_server.impl.callback = py_noop;

  auto do_old_task_callback = make_benchmark(
    [&] {
      return stm::srv::task::client{fmt::format("{}:{}", server_ip, old_server_port), 1000};
    },
    [](auto &client) {
      return client.fx(42, "bird");
    });

  benchmark::RegisterBenchmark("new_task_callback", do_new_task_callback)->Unit(benchmark::kMicrosecond);
  benchmark::RegisterBenchmark("old_task_callback", do_old_task_callback)->Unit(benchmark::kMicrosecond);

  pybind11::gil_scoped_release scoped_release{};

  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}

#else

int main() {
}

#endif
