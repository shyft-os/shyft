#pragma once
#include <algorithm>
#include <memory>
#include <random>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::benchmarks {
  static std::random_device rd;
  static std::mt19937 gen{rd()};

  constexpr auto n_chars = 1000uz;
  constexpr auto n_tsm = 1000uz;
  constexpr auto n_hps = 10uz;
  constexpr auto n_comps = 10uz;
  static constexpr std::string_view alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  auto create_ts(std::string url, bool bound) {
    if (!bound)
      return time_series::dd::apoint_ts(url);
    auto time_axis = time_axis::generic_dt{core::from_seconds(0), core::from_seconds(10), 300};
    auto rep = time_series::dd::apoint_ts(time_axis, 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    return time_series::dd::apoint_ts(url, rep);
  }

  constexpr auto deltahours(int h) {
    return std::chrono::duration_cast<core::utctimespan>(std::chrono::hours(h));
  }

  constexpr auto gen_random_str(std::size_t const len) {
    std::string tmp_s(len, ' ');
    std::ranges::copy(
      std::views::transform(
        std::views::iota(0uz, len),
        [](auto) {
          return alphanum[gen() % (alphanum.size() - 1)];
        }),
      tmp_s.data());
    return tmp_s;
  }

  void add_random_custom_attr(auto &id_base, size_t n, bool bound = false) {
    std::ranges::for_each(std::views::iota(0uz, n), [&](std::size_t id_to_use) {
      id_base.tsm[fmt::format("tsm_id={}", id_to_use)] = create_ts("shyft://some_reference", bound);
      id_base.custom[fmt::format("custom_id={}", id_to_use)] = create_ts("shyft://some_other_reference", bound);
    });
  }

  auto create_stm_hps(std::size_t id, std::shared_ptr<energy_market::stm::stm_system> &sys, bool bound = false) {
    auto hps = std::make_shared<energy_market::stm::stm_hps>(
      id, fmt::format("hps_{}", id), gen_random_str(n_chars), sys);
    add_random_custom_attr(*hps, n_tsm);
    energy_market::stm::stm_hps_builder hps_b(hps);
    std::ranges::for_each(std::views::iota(0uz, n_comps), [&](std::size_t id_to_use) {
      auto r = hps_b.create_reservoir(id_to_use, fmt::format("simple_res_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*r, n_tsm);
      auto u = hps_b.create_unit(id_to_use, fmt::format("simple_unit_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*u, n_tsm);
      auto tun = hps_b.create_tunnel(id_to_use, fmt::format("r->u_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*tun, n_tsm);
      auto pp = hps_b.create_power_plant(id_to_use, fmt::format("simple_pp_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*pp, n_tsm);
      auto g = hps_b.create_gate(id_to_use, fmt::format("gate_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*g, n_tsm);
      auto ra = hps_b.create_reservoir_aggregate(id_to_use, fmt::format("ra_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*ra, n_tsm);
      auto ca = hps_b.create_catchment(id_to_use, fmt::format("cat_{}", id_to_use), gen_random_str(n_chars));
      add_random_custom_attr(*ca, n_tsm);
      // We'll also dress the model up with some time-series:
      core::utctimespan dt{deltahours(1)};
      time_axis::fixed_dt ta{core::utctime{std::chrono::seconds(0)}, dt, 6};
      time_series::ts_point_fx linear{time_series::POINT_INSTANT_VALUE};
      r->inflow.schedule = time_series::dd::apoint_ts(ta, std::vector<double>{1.0, 2.0, nan, 4.0, 3.0, 6.0}, linear);
      r->level.result = time_series::dd::apoint_ts(ta, 1.0, linear);
      r->volume.result = create_ts("shyft://test/r.volume.result", bound);
      ra->volume.schedule = create_ts("shyft://test/ra.volume.schedule", bound);
      ca->inflow_m3s = create_ts("shyft://test/ca.inflow_m3s", bound);
      u->discharge.constraint.min = time_series::dd::apoint_ts(ta, 0.87, linear);
      u->production.result = time_series::dd::apoint_ts(ta, std::vector<double>{1.0, 1.2, 1.3, 1.4, 1.5, 1.6}, linear);
      pp->production.schedule = create_ts("shyft://test/pp.production.schedule", bound);
      tun->discharge.result = time_series::dd::apoint_ts(ta, 5.0, linear);
      tun->discharge.static_max = create_ts("shyft://test/w.discharge.static_max", bound);
      g->discharge.result = create_ts("shyft://test/g.discharge.result", bound);
    });
    return hps;
  }

  auto create_stm_system(std::string model_id, bool bound = false) {
    auto mdl = std::make_shared<energy_market::stm::stm_system>(1, model_id, gen_random_str(n_chars));
    add_random_custom_attr(*mdl, n_tsm, bound);
    std::ranges::for_each(std::views::iota(0uz, n_hps), [&](std::size_t hps_id) {
      mdl->hps.push_back(create_stm_hps(hps_id, mdl, bound));
    });
    return mdl;
  }


}