#include <chrono>
#include <filesystem>
#include <iterator>
#include <string>
#include <type_traits>

#include <benchmark/benchmark.h>
#include <boost/asio/thread_pool.hpp>
#include <fmt/core.h>
#include <fmt/std.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>

// NOTE:
//   benchmark to measure the cost of introducing cycle guards in add_model, set_attrs and set_ts.
//   sets up a shared-models object with all model blob-files, identified by extension .blob, in current
//   directory. for each of these models it benchmarks the cycle-guard, as well as add_model, set_attrs,
//   and set_ts. set_attrs is done with all planning-input urls, and set_ts is done with all timeseries
//   (bound from now until 24 hours ahead).

static auto const models = [] {
  auto root = std::filesystem::current_path();
  std::vector<std::pair<std::string const, std::shared_ptr<shyft::energy_market::stm::stm_system>>> models;
  for (auto const &entry : std::filesystem::directory_iterator(root)) {
    auto path = entry.path();
    if (path.extension() != ".blob")
      continue;
    std::ifstream file{path, std::ios::binary};
    std::string blob((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    models.emplace_back(path.stem(), shyft::energy_market::stm::stm_system::from_blob(blob));
  }
  return models;
}();
static auto const new_models = [] {
  std::vector<std::pair<std::string, std::shared_ptr<shyft::energy_market::stm::stm_system>>> new_models;
  for (auto &&[name, model] : models)
    new_models.emplace_back(fmt::format("{}_new", name), model);
  return new_models;
}();

auto benchmark_causes_cycle = [](benchmark::State &state, auto &&model_id, auto &&model) {
  boost::asio::thread_pool thread_pool{};
  auto shared_models = shyft::energy_market::stm::make_shared_models(thread_pool.executor(), models);

  while (state.KeepRunning())
    benchmark::DoNotOptimize(shyft::energy_market::stm::causes_cycle(shared_models, model_id, model));
};
auto benchmark_add_model = [](benchmark::State &state, auto &&model_id, auto &&model) {
  boost::asio::thread_pool thread_pool{};
  auto shared_models = shyft::energy_market::stm::make_shared_models(thread_pool.executor(), models);

  shyft::energy_market::stm::srv::dstm::server server{};

  add_container(*server.dispatch.state->dtss, "stm", std::filesystem::temp_directory_path());
  server.set_listening_ip("127.0.0.1");
  auto port = server.start_server();

  shyft::energy_market::stm::srv::dstm::client client{shyft::core::srv_connection{fmt::format("localhost:{}", port)}};

  while (state.KeepRunning())
    benchmark::DoNotOptimize(client.add_model({model_id, model}));
};
auto benchmark_set_attrs = [](benchmark::State &state, auto &&model_id, auto &&model) {
  boost::asio::thread_pool thread_pool{};
  auto shared_models = shyft::energy_market::stm::make_shared_models(thread_pool.executor(), models);

  std::vector<std::pair<std::string, shyft::energy_market::stm::any_attr>> attrs;

  shyft::energy_market::stm::url_with_planning_inputs(model_id, model, [&]<typename T>(T attr, std::string_view url) {
    attrs.push_back(std::pair{std::string(url), shyft::energy_market::stm::any_attr{attr}});
  });
  shyft::energy_market::stm::srv::dstm::server server{};

  add_container(*server.dispatch.state->dtss, "stm", std::filesystem::temp_directory_path());
  server.set_listening_ip("127.0.0.1");
  auto port = server.start_server();

  shyft::energy_market::stm::srv::dstm::client client{shyft::core::srv_connection(fmt::format("localhost:{}", port))};

  while (state.KeepRunning())
    benchmark::DoNotOptimize(client.set_attrs({attrs}));
};
auto benchmark_set_ts = [](benchmark::State &state, auto &&model_id, auto &&model) {
  boost::asio::thread_pool thread_pool{};
  auto shared_models = shyft::energy_market::stm::make_shared_models(thread_pool.executor(), models);

  shyft::energy_market::stm::srv::dstm::server server{};

  add_container(*server.dispatch.state->dtss, "stm", std::filesystem::temp_directory_path());
  server.set_listening_ip("127.0.0.1");
  auto port = server.start_server();

  shyft::time_series::dd::ats_vector tsv;
  {
    auto tsv = [&] {
      shyft::time_series::dd::ats_vector tsv;
      constexpr auto dummy = "dummy"; // FIXME: need url-less traversal machinery - yeh
      shyft::energy_market::stm::url_with_all(
        dummy, model, [&]<typename T>(shyft::ignore_t, T const &a, shyft::ignore_t) {
          if constexpr (std::is_same_v<T, shyft::time_series::dd::apoint_ts>)
            if (a.needs_bind())
              tsv.push_back(a);
        });
      shyft::energy_market::stm::url_with_custom(
        dummy, model, [&]<typename T>(shyft::ignore_t, T const &a, shyft::ignore_t) {
          if constexpr (std::is_same_v<T, shyft::time_series::dd::apoint_ts>)
            if (a.needs_bind())
              tsv.push_back(a);
        });
      return tsv;
    }();
    shyft::core::utcperiod bind_period{
      shyft::core::utctime_now(), shyft::core::utctime_now() + shyft::core::utctime_from_seconds64(3600 * 24)};
    shyft::dtss::do_bind_ts(*server.dispatch.state->dtss, bind_period, tsv, true, true); // only recursive//
  }

  shyft::energy_market::stm::url_with_planning_inputs(model_id, model, [&]<typename T>(T attr, std::string_view url) {
    if constexpr (std::is_same_v<T, shyft::energy_market::stm::apoint_ts>)
      tsv.push_back(std::string(url), attr);
  });

  shyft::energy_market::stm::srv::dstm::client client{shyft::core::srv_connection(fmt::format("localhost:{}", port))};

  while (state.KeepRunning())
    benchmark::DoNotOptimize(client.set_ts({model_id, tsv}));
};

int main(int argc, char **argv) {
  for (auto &&[model_id, model] : new_models) {
    benchmark::RegisterBenchmark(fmt::format("{}_causes_cycle", model_id), benchmark_causes_cycle, model_id, model);
    benchmark::RegisterBenchmark(fmt::format("{}_add_model", model_id), benchmark_add_model, model_id, model);
    benchmark::RegisterBenchmark(fmt::format("{}_set_attrs", model_id), benchmark_add_model, model_id, model);
    benchmark::RegisterBenchmark(fmt::format("{}_set_ts", model_id), benchmark_add_model, model_id, model);
  }
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}
