/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <iostream>

#include <benchmark/benchmark.h>

#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

#include <energy_market/stm/stm_bench_utils.h>

namespace shyft::energy_market::stm::benchmarks {

  // a bit ugly, need to control the seed to control complexity:
  static std::mt19937 gen_rebind{22};

  time_series::dd::apoint_ts create_random_op_with_random_ta(int const to_depth = 5, int depth = 0) {

    time_series::dd::apoint_ts inner;
    auto time_axis = time_axis::generic_dt{core::from_seconds(0), core::from_seconds(10), gen_rebind() % 300};
    uint32_t fill_value_int = gen_rebind() % 10;
    double fill_value = 0.88 * fill_value_int;
    if (depth < to_depth)
      inner = create_random_op_with_random_ta(to_depth, ++depth);
    else
      inner = time_series::dd::apoint_ts(time_axis, fill_value);
    switch (gen_rebind() % 10) {
    case 0:
      return time_series::dd::ats_vector(
               {inner,
                time_series::dd::apoint_ts(time_axis, fill_value, time_series::ts_point_fx::POINT_AVERAGE_VALUE)})
        .sum();
    case 1:
      return time_series::dd::ats_vector(
               {inner,
                time_series::dd::apoint_ts(time_axis, fill_value, time_series::ts_point_fx::POINT_AVERAGE_VALUE)})
        .inside(fill_value - 0.01, fill_value + 0.01, 0.0, 2.0, 3.0)
        .sum();
    case 2:
      return time_series::dd::average(inner, time_axis::generic_dt{core::from_seconds(0), core::from_seconds(10), 5});
    default:
      return time_series::dd::integral(inner, time_axis::generic_dt{core::from_seconds(0), core::from_seconds(10), 5});
    }
  }

  auto get_sum_attr() {
    time_series::dd::ats_vector to_sum;
    std::ranges::for_each(std::views::iota(0, 1000), [&](int i) {
      to_sum.push_back(create_random_op_with_random_ta());
    });
    return to_sum.sum();
  }

  auto get_mdl(std::string mdl_id) {
    auto mdl = std::make_shared<energy_market::stm::stm_system>(1, mdl_id);
    auto hps = std::make_shared<energy_market::stm::stm_hps>(22, fmt::format("hps_{}", 22), "", mdl);
    hps->tsm["a"] = create_ts(fmt::format("dstm://M{}/H{}.ts.late_attr_sum", mdl_id, hps->id), true);
    hps->tsm["late_attr_sum"] = get_sum_attr();
    mdl->hps.push_back(std::move(hps));
    return mdl;
  }

  void run_bench(int argc, char **argv) {

    std::string model_id = "1#0#0";
    auto s2_obl = get_mdl(model_id);

    auto rebind = [&](benchmark::State &state, auto &&model) {
      while (state.KeepRunning()) {
        state.PauseTiming();
        // clone each time since we change the model:
        auto s2_obl_clone = shyft::energy_market::stm::stm_system::clone_stm_system(model);
        state.ResumeTiming();
        benchmark::DoNotOptimize(shyft::energy_market::stm::rebind_expression(*model, model_id, false));
      }
    };

    auto b1 = benchmark::RegisterBenchmark("system_gen", rebind, s2_obl);
    b1->Unit(benchmark::kMillisecond);

    benchmark::Initialize(&argc, argv);
    benchmark::RunSpecifiedBenchmarks();
    benchmark::Shutdown();
  }
}

int main(int argc, char **argv) {
  shyft::energy_market::stm::benchmarks::run_bench(argc, argv);
}
