/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <algorithm>
#include <memory>
#include <random>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <shyft/energy_market/stm/srv/compute/client.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include <energy_market/stm/stm_bench_utils.h>

/**
 * This benchmark measures the cost of serialization of a fully populated stm_system, including expressions, compared
 * with one stripped of all expressions
 */
namespace shyft::energy_market::stm::benchmarks {
  void run_bench(int argc, char **argv) {
    auto s2_obl = create_stm_system("test_system");
    auto s2_obl_clone = shyft::energy_market::stm::stm_system::clone_stm_system(s2_obl);
    auto stripped_blob = shyft::energy_market::stm::stm_system::to_blob_strip(s2_obl);
    auto stripped_model = shyft::energy_market::stm::stm_system::from_blob_strip(stripped_blob);

    // erase all expressions:
    shyft::energy_market::stm::reset_ts(*s2_obl_clone);

    auto serialize_to_string = [](benchmark::State &state, auto &&model) {
      while (state.KeepRunning())
        benchmark::DoNotOptimize(shyft::energy_market::stm::stm_system::to_blob(model));
    };

    shyft::energy_market::stm::srv::compute::server server{};
    constexpr auto ip = "127.0.0.1";
    server.set_listening_ip(ip);
    auto port = server.start_server();
    shyft::energy_market::stm::srv::compute::client client{{.connection{fmt::format("{}:{}", ip, port)}}};

    auto start_client = [&](benchmark::State &state, auto &&model) {
      while (state.KeepRunning()) {
        client.start({.model_id = "mid", .model = model});
        server.dispatch.state->state = shyft::energy_market::stm::srv::compute::state::idle;
      }
    };

    auto clone_model = [&](benchmark::State &state, auto &&model, bool fast) {
      while (state.KeepRunning())
        benchmark::DoNotOptimize(shyft::energy_market::stm::stm_system::clone_stm_system(model, fast));
    };

    auto b1 = benchmark::RegisterBenchmark("system_with_expr_string", serialize_to_string, s2_obl);
    b1->Unit(benchmark::kMillisecond);
    auto b2 = benchmark::RegisterBenchmark("system_with_stripped", serialize_to_string, stripped_model);
    b2->Unit(benchmark::kMillisecond);

    auto b3 = benchmark::RegisterBenchmark("system_with_expr_client_start", start_client, s2_obl);
    b3->Unit(benchmark::kMillisecond);
    auto b4 = benchmark::RegisterBenchmark("system_stripped_client_start", start_client, stripped_model);
    b4->Unit(benchmark::kMillisecond);

    auto b5 = benchmark::RegisterBenchmark("clone_full", clone_model, s2_obl, false);
    b5->Unit(benchmark::kMillisecond);
    auto b6 = benchmark::RegisterBenchmark("clone_stripped", clone_model, stripped_model, false);
    b6->Unit(benchmark::kMillisecond);
    auto b7 = benchmark::RegisterBenchmark("clone_full_fast", clone_model, s2_obl, true);
    b7->Unit(benchmark::kMillisecond);
    auto b8 = benchmark::RegisterBenchmark("clone_stripped_fast", clone_model, stripped_model, true);
    b8->Unit(benchmark::kMillisecond);

    benchmark::Initialize(&argc, argv);
    benchmark::RunSpecifiedBenchmarks();
    benchmark::Shutdown();
  }

}

int main(int argc, char **argv) {
  shyft::energy_market::stm::benchmarks::run_bench(argc, argv);
}
