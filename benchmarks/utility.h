#pragma once
#include <string>
#include <thread>

#include <shyft/core/fs_compat.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft {

  struct temp_dir {
    fs::path p;

    temp_dir(char const *tmp_pth = "shyft.") {
      p = fs::temp_directory_path()
        / (std::string(tmp_pth) + std::to_string(shyft::core::utctime_now().count())
           + std::to_string(std::hash<std::thread::id>()(std::this_thread::get_id())));
    }

    operator fs::path() const {
      return p;
    }

    fs::path operator/(std::string c) const {
      return p / c;
    }

    fs::path operator/(char const *c) const {
      return p / std::string(c);
    }

    fs::path operator/(fs::path const &c) const {
      return p / c;
    }

    std::string string() const {
      return p.string();
    }

    ~temp_dir() {
      fs::remove_all(p);
    }
  };

}
