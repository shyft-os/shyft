### Stable benchmark results
For the most stable benchmark results, be sure to disable CPU scaling, using `cpupower` (found in dnf package `kernel-tools`) by running 

`sudo cpupower frequency-set --governor performance`

and bind the benchmark to CPU0 by running it with:

`taskset -c 0 benchmark`
where `benchmark` is the benchmark code in question.

### Comparing for regression

In order to check for regression in performance, build binaries for the old and the new version and follow [this guide](https://github.com/google/benchmark/blob/main/docs/tools.md) on how to compare benchmarks.

E.g.  `compare.py benchmarks old_benchmark_build new_benchmark_build` 