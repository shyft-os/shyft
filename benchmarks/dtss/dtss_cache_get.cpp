/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <algorithm>
#include <cstdint>
#include <random>
#include <ranges>
#include <string>
#include <vector>

#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <shyft/dtss/dtss_cache.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

int main(int argc, char** argv) {
  std::size_t n = 100000;
  shyft::dtss::cache cache{n};
  std::vector<std::string> ids{};
  std::vector<shyft::core::utcperiod> periods;
  std::vector<shyft::dtss::ts_frag> frags;
  auto time_axis = shyft::time_axis::generic_dt{shyft::core::from_seconds(0), shyft::core::from_seconds(10), 3};
  auto point_ts = std::make_shared<shyft::time_series::dd::gpoint_ts const>(time_axis, std::vector{1.0, 2.0, 3.0});
  ids.reserve(n);
  frags.reserve(n);
  periods.reserve(n);
  for (auto i : std::views::iota(0uz, n)) {
    ids.push_back(fmt::format("id_{}", i));
    frags.push_back(point_ts);
    periods.push_back(time_axis.total_period());
  }
  cache.add(std::views::zip(ids, frags, periods), true);
  // shuffle ids:
  std::random_device rd;
  std::mt19937 gen{rd()};
  std::ranges::shuffle(ids, gen);

  auto get = [&](benchmark::State& state, auto&& ids) {
    while (state.KeepRunning()) {
      benchmark::DoNotOptimize(cache.get(ids, time_axis.total_period()));
    }
  };

  auto get_with = [&](benchmark::State& state, auto&& ids) {
    std::vector<shyft::time_series::dd::apoint_ts> points{ids.size()};
    while (state.KeepRunning()) {
      cache.get_with(
        ids, time_axis.total_period(), [&]([[maybe_unused]] auto const & id, auto const & index, auto const & fragopt) {
          if (fragopt) {
            points[index].ts = std::move(std::get<0>(*fragopt));
          } else {
            points[index].ts.reset();
          }
        });
      benchmark::DoNotOptimize(points);
    }
  };
  auto r = benchmark::RegisterBenchmark("get", get, ids);
  r->Iterations(1000);
  r->Unit(benchmark::kMillisecond);
  auto r2 = benchmark::RegisterBenchmark("get_with", get_with, ids);
  r2->Iterations(1000);
  r2->Unit(benchmark::kMillisecond);
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}
