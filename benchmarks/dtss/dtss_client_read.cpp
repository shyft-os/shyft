/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <algorithm>
#include <ranges>

#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <dtss/dtss_bench_utilities.h>

using namespace shyft::dtss::benchmarks;

int main(int argc, char** argv) {
  fmt::print(
    "This benchmark compares the functions do_read and try_read for read on a master "
    "client\n");

  auto options = bench_options();
  options.apply(argc, argv);
  options.uses_client = true;
  dtss_to_read d{options};
  auto& dbs = options.databases;

  if (!options.all_only) {
    std::ranges::for_each(std::views::cartesian_product(dbs, options.to_check), [&](auto const & el) {
      auto const& [db, ver] = el;
      d.register_b(fmt::format("client_{}_", ver), get_client_do_read(d, mode::master, ver), db);
    });
  }

  if (dbs.size() > 1) {
    std::ranges::for_each(options.to_check, [&](auto const & ver) {
      d.register_b(fmt::format("client_{}_", ver), get_client_do_read(d, mode::master, ver));
    });
  }
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}
