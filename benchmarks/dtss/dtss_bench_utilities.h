/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <algorithm>
#include <cstdint>
#include <filesystem>
#include <map>
#include <memory>
#include <optional>
#include <random>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

#include <benchmark/benchmark.h>
#include <boost/program_options.hpp>
#include <fmt/core.h>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/ref.h> // needed for the bind ops.
#include <shyft/time_series/time_axis.h>

#include <utility.h>

namespace shyft::dtss::benchmarks {
  SHYFT_DEFINE_ENUM(db, std::uint8_t, (rocks, ts, cache, external))
  SHYFT_DEFINE_ENUM(mode, std::uint8_t, (master, slave));
  SHYFT_DEFINE_ENUM(version, std::uint8_t, (read, try_read));
}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::benchmarks::db);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::benchmarks::mode);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::benchmarks::version);

template <>
struct fmt::formatter<std::optional<shyft::dtss::benchmarks::db>> {
  constexpr auto parse(auto& ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  template <typename FormatContext>
  auto format(std::optional<shyft::dtss::benchmarks::db> const & v, FormatContext& ctx) const {
    return fmt::format_to(ctx.out(), "{}", (v ? fmt::format("{}", *v) : "ALL"));
  }
};
namespace shyft::dtss::benchmarks {
  struct bench_options {
    std::size_t n_ts = 10000;
    std::optional<std::size_t> n_itr = std::nullopt; // events
    std::vector<db> databases{db::external, db::cache, db::rocks, db::ts};
    std::optional<std::size_t> n_failed;
    bool all_only = false;
    std::vector<version> to_check{version::read, version::try_read};
    bool uses_slave = false;
    bool uses_client = false;

    void apply(int argc, char** argv) {
      boost::program_options::options_description desc("Allowed options");
      desc.add_options()("help", "produce help message")(
        "ALL_ONLY", "Run all db types at once, not each itself (overrides db selectors below)")(
        "CACHE", "Run only cache db type (overrides db selectors below)")(
        "TS", "Run only cache ts type (overrides db selectors below)")("ROCKS", "Run only rocks db type")(
        "READ", "Run only read command (overrides TRY_READ)")("TRY_READ", "Run only try_read command")(
        "N_TS", boost::program_options::value<std::size_t>(), "Number of ts in each read (unsigend int)")(
        "N_ITR", boost::program_options::value<std::size_t>(), "Number of iterations in each loop (unsigend int)");
      boost::program_options::variables_map vm;
      try {
        boost::program_options::store(
          boost::program_options::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
      } catch (std::exception& e) {
        fmt::print("OPTION FAILED: {}, running with default\n", e.what());
        return;
      }
      if (vm.count("ALL_ONLY")) {
        fmt::print("Running only accumulation\n");
        all_only = true;
      } else if (vm.count("CACHE")) {
        fmt::print("Running only for CACHE\n");
        databases.clear();
        databases.push_back(db::cache);
      } else if (vm.count("TS")) {
        fmt::print("Running only for TS\n");
        databases.clear();
        databases.push_back(db::ts);
      } else if (vm.count("ROCKS")) {
        fmt::print("Running only for rocks\n");
        databases.clear();
        databases.push_back(db::rocks);
      }
      if (vm.count("READ")) {
        fmt::print("Running only read\n");
        to_check.clear();
        to_check.push_back(version::read);
      } else if (vm.count("TRY_READ")) {
        fmt::print("Running only try_read\n");
        to_check.clear();
        to_check.push_back(version::try_read);
      }
      if (vm.count("N_TS"))
        n_ts = vm["N_TS"].as<std::size_t>();
      if (vm.count("N_ITR"))
        n_itr = vm["N_ITR"].as<std::size_t>();
    }
  };

  struct dtss_to_read {
    explicit dtss_to_read(bench_options& opt)
      : options(opt) {
      time_axis = shyft::time_axis::generic_dt{shyft::core::from_seconds(0), shyft::core::from_seconds(10), 3};
      point_ts = shyft::time_series::dd::apoint_ts{
        time_axis, {1.0, 2.0, 3.0},
         shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };

      // callback that returns same ts for all
      auto external = [&](auto ts_ids_e, [[maybe_unused]] auto period) {
        return std::vector<shyft::time_series::dd::apoint_ts>(ts_ids_e.size(), point_ts);
      };

      master = std::make_unique<shyft::dtss::server_state>(server_callbacks{external});
      master_server = std::make_unique<shyft::dtss::binary_server>(*master);

      master_server->set_listening_ip(local_only);
      master_port_no = master_server->start_server();
      if (options.uses_client)
        mc = std::make_unique<shyft::dtss::client>(fmt::format("localhost:{}", master_port_no));
      if (options.uses_slave) {
        slave = std::make_unique<shyft::dtss::server_state>();
        set_master(*slave, "localhost", master_port_no, 0.001, 10, 0.1);

        slave_server = std::make_unique<shyft::dtss::binary_server>(*slave);
        slave_server->set_listening_ip(local_only);
        slave_port_no = slave_server->start_server();
        if (options.uses_client)
          sc = std::make_unique<shyft::dtss::client>(fmt::format("localhost:{}", slave_port_no));
      }

      if (opt.n_ts < 50) {
        unit = benchmark::kNanosecond;
      }
      setup_containers();
      static std::random_device rd;
      static std::mt19937 gen{rd()};
      for (auto db : opt.databases) {
        add_ts(opt.n_ts, db);
        if (opt.n_failed && db != db::external) {
          add_failed(opt.n_failed.value(), db);
        }
        auto* this_ts = &ts_ids[db];
        std::ranges::shuffle(*this_ts, gen);
        all_ts_ids.insert(all_ts_ids.end(), this_ts->begin(), this_ts->end());
      }
      std::ranges::shuffle(all_ts_ids, gen);
      fmt::print("Total number of ts_ids={}\n", all_ts_ids.size());
    }

    void setup_containers() {
      add_container(*master, std::string{""}, root_container.string());
      add_container(*master, rocks_container, fmt::format("{}/{}", root_container.string(), rocks_container), "ts_rdb");
      add_container(*master, ts_db_container, fmt::format("{}/{}", root_container.string(), ts_db_container), "ts_db");
    }

    void add_ts(size_t n_ts, db db) {
      shyft::time_series::dd::ats_vector to_store;
      to_store.reserve(n_ts);
      auto& ts_ids_d = this->ts_ids[db];
      auto add_ts = [&](std::string_view ts_name) {
        std::string url;
        switch (db) {
        case db::rocks:
          url = fmt::format("shyft://{}/{}", this->rocks_container, ts_name);
          break;
        case db::ts:
          url = fmt::format("shyft://{}/{}", this->ts_db_container, ts_name);
          break;
        case db::cache:
          url = fmt::format("shyft://{}/{}_cached", this->ts_db_container, ts_name);
          break;
        case db::external:
          url = fmt::format("external://{}", ts_name);
          break;
        }
        to_store.push_back(shyft::time_series::dd::apoint_ts(url, point_ts));
        ts_ids_d.push_back(url);
      };
      std::ranges::for_each(std::views::iota(0uz, n_ts), [&](auto const i) {
        add_ts(fmt::format("ts_{}", i));
      });
      auto store_policy = shyft::dtss::store_policy(true, true, db == db::cache);
      do_store_ts(*master, to_store, store_policy);
    }

    void add_failed(std::size_t n_failed, db db) {
      auto& ts_ids_d = this->ts_ids[db];
      auto add_ts = [&](std::string_view ts_name) {
        std::string url;
        switch (db) {
        case db::rocks:
          url = fmt::format("shyft://{}/failed_{}", this->rocks_container, ts_name);
          break;
        case db::ts:
          url = fmt::format("shyft://{}/failed_{}", this->ts_db_container, ts_name);
          break;
        case db::cache:
          url = fmt::format("shyft://{}/failed_{}_cached", this->ts_db_container, ts_name);
          break;
        case db::external:
          url = fmt::format("external://failed_{}_cached", ts_name);
          break;
        }
        ts_ids_d.push_back(url);
      };
      std::ranges::for_each(std::views::iota(0uz, n_failed), [&](auto const i) {
        add_ts(fmt::format("ts_{}", i));
      });
    }

    void register_b(std::string_view name, auto bench_fn, std::optional<db> db = std::nullopt) {
      auto* tid = &all_ts_ids;
      if (db) {
        tid = &ts_ids[*db];
      }
      auto b = benchmark::RegisterBenchmark(fmt::format("{}{}", name, db), bench_fn, *tid);
      if (options.n_itr)
        b->Iterations(*options.n_itr);
      b->Unit(unit);
    }

    std::string const local_only{"127.0.0.1"};
    std::unique_ptr<shyft::dtss::server_state> master;
    std::unique_ptr<shyft::dtss::binary_server> master_server;
    int master_port_no;
    std::unique_ptr<shyft::dtss::client> mc;

    std::unique_ptr<shyft::dtss::server_state> slave;
    std::unique_ptr<shyft::dtss::binary_server> slave_server;
    int slave_port_no;
    std::unique_ptr<shyft::dtss::client> sc;
    shyft::temp_dir root_container;

    std::string rocks_container = "rocks_container";
    std::string ts_db_container = "ts_db_container";

    std::map<db, std::vector<std::string>> ts_ids;
    std::vector<std::string> all_ts_ids;
    shyft::time_axis::generic_dt time_axis;
    shyft::time_series::dd::apoint_ts point_ts;

    bench_options options;
    benchmark::TimeUnit unit = benchmark::kMicrosecond;
  };

  auto get_server_do_read(dtss_to_read& d, mode m, version v)
    -> std::function<void(benchmark::State& state, std::vector<std::string> const & ids)> {
    shyft::dtss::server_state* srv = d.master.get();
    auto axis = d.time_axis;
    if (m == mode::slave) {
      if (!d.options.uses_slave) {
        throw std::runtime_error("Using slave client read without initialization");
      }
      srv = d.slave.get();
    }
    if (v == version::read)
      return [=](benchmark::State& state, std::vector<std::string> const & ids) {
        for ([[maybe_unused]] auto _ : state)
          benchmark::DoNotOptimize(shyft::dtss::try_read(*srv, ids, axis.total_period(), true, false));
      };
    return [=](benchmark::State& state, std::vector<std::string> const & ids) {
      for ([[maybe_unused]] auto _ : state)
        benchmark::DoNotOptimize(shyft::dtss::try_read(*srv, ids, axis.total_period(), true, false));
    };
  }

  auto get_client_do_read(dtss_to_read& d, mode m, version v)
    -> std::function<void(benchmark::State& state, std::vector<std::string> const & ids)> {
    if (!d.options.uses_client) {
      throw std::runtime_error("Using client read without initialization");
    }
    shyft::dtss::client* clt = d.mc.get();
    auto axis = d.time_axis;
    if (m == mode::slave) {
      if (!d.options.uses_slave) {
        throw std::runtime_error("Using slave client read without initialization");
      }
      clt = d.sc.get();
    }
    if (v == version::read)
      return [=](benchmark::State& state, std::vector<std::string> const & ids) {
        for ([[maybe_unused]] auto _ : state)
          benchmark::DoNotOptimize(clt->try_read(ids, axis.total_period(), true, false));
      };
    return [=](benchmark::State& state, std::vector<std::string> const & ids) {
      for ([[maybe_unused]] auto _ : state)
        benchmark::DoNotOptimize(clt->try_read(ids, axis.total_period(), true, false));
    };
  }

}


