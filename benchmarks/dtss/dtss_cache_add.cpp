/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <ranges>
#include <string>
#include <vector>

#include <benchmark/benchmark.h>
#include <fmt/core.h>

#include <shyft/dtss/dtss_cache.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

int main(int argc, char** argv) {
  std::size_t n = 100000;
  shyft::dtss::cache cache{n};
  std::vector<std::string> ids{};
  std::vector<shyft::core::utcperiod> periods;
  std::vector<shyft::dtss::ts_frag> frags;
  auto time_axis = shyft::time_axis::generic_dt{shyft::core::from_seconds(0), shyft::core::from_seconds(10), 3};
  auto point_ts = std::make_shared<shyft::time_series::dd::gpoint_ts const>(time_axis, std::vector{1.0, 2.0, 3.0});
  ids.reserve(n);
  frags.reserve(n);
  periods.reserve(n);
  for (auto i : std::views::iota(0uz, n)) {
    ids.push_back(fmt::format("id_{}", i));
    frags.push_back(point_ts);
    periods.push_back(time_axis.total_period());
  }

  auto add_func = [&](benchmark::State& state, auto&& ids, auto&& frags) {
    while (state.KeepRunning()) {
      cache.add(std::views::zip(ids, frags, periods), true);
    }
  };
  benchmark::RegisterBenchmark("add", add_func, ids, frags);
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
}
