#include <chrono>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include <fmt/color.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/core/reflection.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/protocol/messages.h>

#include <forge/create_message.h>
#include <forge/forge_dtss_version.h>
#include <forge/forged_version.h>

namespace shyft::dtss::version_forge {

  namespace detail {
    template <auto msg>
    requires(!protocols::is_internal_message<msg>)
    message get_msg() {
      std::stringstream req;
      auto request = get_request<msg>();
      auto oarchive_request = protocols::protocol_archives<msg>::make_oarchive(req);
      oarchive_request << request;
      auto reply = get_reply<msg>();
      std::stringstream res;
      auto oarchive_reply = protocols::protocol_archives<msg>::make_oarchive(res);
      oarchive_reply << reply;
      return {std::string(req.view()), std::string(res.view())};
    }
  }

  void forge_version(std::uint8_t version, std::filesystem::path path) {
    versioned_protocol_header header;
    header.version = version;
    std::vector<message> msgs;
    std::size_t expected_msg_size = 0;
    protocols::with_version_or<dtss::protocol>(
      version,
      [&](auto V) {
        if constexpr (V != 0) {
          fmt::print("VERSION {} FILENAME {}\n", V.value, path);
          using tags = dtss::message_tags<V>;
          expected_msg_size = enumerator_count<tags>;
          for_each_enum<tags>([&](auto tag) {
            constexpr auto msg = protocols::versioned_message<dtss::protocol, V>{tag.value};
            msgs.push_back(detail::get_msg<msg>());
          });
        }
      },
      [](auto error) {
        auto error_message = [&] {
          if constexpr (get_error_tag(error) == protocols::protocol_stream_error_tag::invalid_version)
            return fmt::format("Version {} not supported by client", error.read_version);
          else
            return fmt::format("invalid error tag");
        }();
        fmt::print("{}", error_message);
      });

    //  #####################################################################################################################
    //  #####################################################################################################################
    header.n_messages = msgs.size();
    if (header.n_messages != expected_msg_size) {
      fmt::print("Expected {} messages, got {} for version {}", expected_msg_size, header.n_messages, version);
      return;
    }

    std::fstream f(path, f.binary | f.trunc | f.out);
    if (!f.is_open()) {
      fmt::print("failed to open {}\n", path);
      return;
    }
    header.write(f);

    std::ranges::for_each(msgs, [&](auto& m) {
      m.write(f);
    });
    fmt::print("Done, file written to {}\n", path);
  }

}