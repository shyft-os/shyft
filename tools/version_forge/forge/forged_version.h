#pragma once
#include <cstdint>
#include <iostream>
#include <ranges>
#include <string>
#include <vector>

#include <fmt/color.h>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

namespace shyft::dtss::version_forge {

  namespace detail {
    template <typename T>
    auto write_impl(std::ostream& o, T const & data) {
      if constexpr (std::is_same_v<T, std::string>) {
        auto sz = std::ranges::size(data);
        write_impl(o, sz);
        o.write(reinterpret_cast<char const *>(std::ranges::data(data)), sz);
      } else if constexpr (std::is_integral_v<T> || std::is_same_v<T, bool>) {
        auto as_i32 = static_cast<std::int32_t>(data);
        o.write(reinterpret_cast<char const *>(&as_i32), sizeof(as_i32));
      } else {
        static_assert(false, "unexpected data type");
      }
    }

    template <typename T>
    auto read_impl(std::istream& i) {
      if constexpr (std::is_same_v<T, std::string>) {
        auto sz = read_impl<std::size_t>(i);
        std::string f(sz, '\0');
        i.read(reinterpret_cast<char*>(std::ranges::data(f)), sz);
        return f;
      } else if constexpr (std::is_integral_v<T>) {
        std::int32_t as_i32;
        i.read(reinterpret_cast<char*>(&as_i32), sizeof(as_i32));
        return static_cast<T>(as_i32);
      } else {
        static_assert(false, "unexpected data type");
      }
    }
  }

  struct versioned_protocol_header {
    std::uint8_t version;
    std::uint32_t n_messages;

    void write(std::ostream& o) const {
      detail::write_impl(o, version);
      detail::write_impl(o, n_messages);
    }

    static versioned_protocol_header read(std::istream& i) {
      return versioned_protocol_header{detail::read_impl<std::uint8_t>(i), detail::read_impl<std::uint32_t>(i)};
    }
  };

  struct message {
    std::string request;
    std::string reply;

    void write(std::ostream& o) const {
      detail::write_impl(o, request);
      detail::write_impl(o, reply);
    }

    static message read(std::istream& i) {
      return message{detail::read_impl<std::string>(i), detail::read_impl<std::string>(i)};
    }
  };
}