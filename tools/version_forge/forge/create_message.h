#pragma once
#include <memory>

#include <fmt/core.h>

#include <shyft/core/protocol.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/time_series_info.h>

namespace shyft::dtss::version_forge {

  template <auto m>
  constexpr auto get_request() -> protocols::request<m> {
    if constexpr (reflection::empty_struct<protocols::request<m>>)
      return {};
    if constexpr (m.tag == message_tags<m.version>::FIND_TS)
      return {.search_expression = "Somelongexpressiontoserialize"};
    if constexpr (m.tag == any_of(message_tags<m.version>::GET_TS_INFO, message_tags<m.version>::REMOVE_TS))
      return protocols::request<m>{.url = "shyft://some/or/other/url"};
    if constexpr (m.tag == message_tags<m.version>::STORE_TS_2)
      return {
        .policy = {.recreate = true, .strict = false, .cache = false, .best_effort = true},
        .fragments = {
                   {"shyft://some/url",
           time_series::point_ts{time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0}},
                   {"shyft://other/url2",
           time_series::point_ts{time_axis::generic_dt{core::utctime{77l}, core::utctime{6666l}, 2}, -3.0}}}
      };
    if constexpr (m.tag == message_tags<m.version>::MERGE_STORE_TS)
      return {
        .fragments =
          {{"shyft://some/url",
            time_series::point_ts{time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0}},
                      {"shyft://other/url2",
            time_series::point_ts{time_axis::generic_dt{core::utctime{77l}, core::utctime{6666l}, 2}, -3.0}}},
        .cache = true
      };
    if constexpr (m.tag == message_tags<m.version>::TRY_SLAVE_READ)
      return {
        .ids = {"iddd", "something", "else"},
        .period = {core::utctime{99l}, core::utctime{9000l}},
        .cache = true,
        .subscribe = true
      };
    if constexpr (m.tag == message_tags<m.version>::SLAVE_UNSUBSCRIBE)
      return {
        .ids = {"some", "ids"}
      };
    if constexpr (m.tag == any_of(dtss::message_tags<m.version>::Q_SIZE, message_tags<m.version>::Q_INFOS))
      return {.name = "somename"};
    if constexpr (m.tag == message_tags<m.version>::Q_INFO)
      return {.name = "some_queue_name", .message_id = "some_message_id"};
    if constexpr (m.tag == message_tags<m.version>::Q_PUT)
      return {
        .name = "some_queue_name",
        .message_id = "some_message_id",
        .description = "this is a description of a message",
        .ttl = core::utctime{22l},
        .fragments = {
                             {"shyft://some/url",
           time_series::point_ts{time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0}},
                             {"shyft://other/url2",
           time_series::point_ts{time_axis::generic_dt{core::utctime{77l}, core::utctime{6666l}, 2}, -3.0}}}
      };
    if constexpr (m.tag == message_tags<m.version>::Q_GET)
      return {.name = "some_name", .max_wait = core::utctime{99l}};
    if constexpr (m.tag == message_tags<m.version>::Q_ACK)
      return {.name = "some_name", .message_id = "some_id", .diagnosis = "some diag"};
    if constexpr (m.tag == message_tags<m.version>::Q_MAINTAIN)
      return {.name = "some_name", .keep_ttl_items = true, .flush = true};
    if constexpr (m.tag == message_tags<m.version>::SET_CONTAINER)
      return {
        .name = "some_name",
        .path = "some/path",
        .type = "some_type",
        .config = {
                   .compression = true,
                   .max_file_size = 127,
                   .write_buffer_size = 789,
                   .log_level{900},
                   .test_mode{22},
                   .ix_cache{1},
                   .ts_cache{123}}
      };
    if constexpr (m.tag == message_tags<m.version>::REMOVE_CONTAINER)
      return {.url = "shyft://something/else", .remove_from_disk = true};
    if constexpr (m.tag == message_tags<m.version>::SWAP_CONTAINER)
      return {.name_a = "some_name", .name_b = "some_other"};
    if constexpr (m.version > 1 && requires { requires(m.tag == message_tags<m.version>::Q_FIND); })
      return {.name = "some_msg", .message_id = "soem_id"};
  }

  template <auto m>
  constexpr auto get_reply() -> protocols::reply<m> {
    if constexpr (reflection::empty_struct<protocols::reply<m>>)
      return {};
    if constexpr (m.tag == message_tags<m.version>::FIND_TS)
      return {
        .infos = {
                  {"some_name",
           time_series::ts_point_fx::POINT_INSTANT_VALUE,
           core::utctime{222l},
           "some_id",
           {core::utctime{78910l}, core::utctime{110101l}},
           core::utctime{123456l},
           core::utctime{98766l}},
                  {"other_name",
           time_series::ts_point_fx::POINT_AVERAGE_VALUE,
           core::utctime{7768l},
           "other_id",
           {core::utctime{27891l}, core::utctime{121010l}},
           core::utctime{1234l},
           core::utctime{987l}}}
      };
    if constexpr (m.tag == message_tags<m.version>::GET_TS_INFO)
      return {
        .info = {
                 "some_name", time_series::ts_point_fx::POINT_INSTANT_VALUE,
                 core::utctime{222l},
                 "some_id", {core::utctime{78910l}, core::utctime{110101l}},
                 core::utctime{123456l},
                 core::utctime{98766l}}
      };
    if constexpr (m.tag == message_tags<m.version>::STORE_TS_2)
      return {
        .diagnostics = {
          {{22ul, ts_diagnostics::miss_matched_resolution}, {2ul, ts_diagnostics::external_store_failed}}}};
    if constexpr (m.tag == message_tags<m.version>::TRY_SLAVE_READ)
      return {
        .fragments =
          {std::make_shared<time_series::dd::gpoint_ts const>(
             time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0),
                      std::make_shared<time_series::dd::gpoint_ts const>(
             time_axis::generic_dt{core::utctime{3330l}, core::utctime{44444l}, 10}, -99.0)},
        .periods = {{core::utctime{1000l}, core::utctime{3000l}}, {core::utctime{8000l}, core::utctime{30000l}}},
        .fragment_updates{
                      time_series::dd::aref_ts{
            "bound",
            std::make_shared<time_series::dd::gpoint_ts const>(
              time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0)},
                      time_series::dd::aref_ts{
            "other",
            std::make_shared<time_series::dd::gpoint_ts const>(
              time_axis::generic_dt{core::utctime{8000l}, core::utctime{100000l}, 200}, -99.0)},
                      },
        .period_updates{{core::utctime{300l}, core::utctime{400l}}, {core::utctime{500l}, core::utctime{600l}}},
        .errors{{9, lookup_error::container_not_found}}
      };
    if constexpr (m.tag == message_tags<m.version>::SLAVE_READ_SUBSCRIPTION)
      return {
        .series =
          {
                   time_series::dd::aref_ts{"unbound"},
                   time_series::dd::aref_ts{
              "bound",
              std::make_shared<time_series::dd::gpoint_ts const>(
                time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0)},
                   },
        .periods = {
                   core::utcperiod{core::utctime{0l}, core::utctime{10l}},
                   core::utcperiod{core::utctime{300l}, core::utctime{9650l}},
                   core::utcperiod{core::utctime{600l}, core::utctime{70000l}}}
      };
    if constexpr (m.tag == message_tags<m.version>::GET_VERSION)
      return {.shyft_version = "some_version.0.1.2.3.4"};
    if constexpr (m.tag == message_tags<m.version>::Q_LIST)
      return {
        .queues = {"some", "other", "another"}
      };
    if constexpr (m.tag == message_tags<m.version>::Q_INFOS)
      return {
        .infos = {
                  {"some_id", "some_description", core::utctime{300l}, core::utctime{900l}},
                  {"other_id", "other_description", core::utctime{777l}, core::utctime{1859l}}}
      };
    if constexpr (m.tag == message_tags<m.version>::Q_INFO)
      return {
        .info = {"some_id", "some_description", core::utctime{300l}, core::utctime{900l}}
      };
    if constexpr (m.tag == message_tags<m.version>::Q_GET)
      return {
        .info = {"some_id", "some_description", core::utctime{300l}, core::utctime{900l}},
        .fragments = {
                 {"shyft://some/url",
           time_series::point_ts{time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0}},
                 {"shyft://other/url2",
           time_series::point_ts{time_axis::generic_dt{core::utctime{77l}, core::utctime{6666l}, 2}, -3.0}}}
      };
    if constexpr (m.tag == message_tags<m.version>::Q_SIZE)
      return {.size = 999};
    if constexpr (m.tag == message_tags<m.version>::GET_CONTAINERS)
      return {
        .names = {"some", "names", "are", "in", "this", "vector"}
      };
    if constexpr (m.tag == message_tags<m.version>::PROTOCOL_VERSIONS)
      return {.minimum = 80, .maximum = 120};
    if constexpr (m.version > 1 && requires { requires(m.tag == message_tags<m.version>::Q_FIND); })
      return {
        .info = {{"some_id", "some_description", core::utctime{300l}, core::utctime{900l}}},
        .fragments = {
                 {"shyft://some/url",
           time_series::point_ts{time_axis::generic_dt{core::utctime{0l}, core::utctime{3600l}, 200}, 2.0}},
                 {"shyft://other/url2",
           time_series::point_ts{time_axis::generic_dt{core::utctime{77l}, core::utctime{6666l}, 2}, -3.0}}}
      };
  }

}