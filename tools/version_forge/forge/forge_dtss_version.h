#pragma once
#include <cstdint>
#include <filesystem>

namespace shyft::dtss::version_forge {

  void forge_version(std::uint8_t version, std::filesystem::path path);

}
