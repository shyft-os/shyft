Tool to permanently forge a shyft dtss protocol version into a binary file that is used to verify that the protocol is unchanged.

When running it, provide the version you want to forge (defaults to latest version). Creates a binary archive called
dtss_protocol_{version_num}.bin, which should be moved into cpp/test/dtss/files so it will be picked up and verified
by the pipeline.

Runs automatically for the latest dtss protocol version when built with cmake option SHYFT_WITH_VERSION_FORGE