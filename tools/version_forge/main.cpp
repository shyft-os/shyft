#include <exception>
#include <filesystem>
#include <iostream>
#include <optional>

#include <boost/program_options.hpp>
#include <fmt/core.h>
#include <fmt/std.h>

#include <shyft/core/protocol.h>
#include <shyft/dtss/protocol/message_tags.h>

#include <forge/forge_dtss_version.h>

int main(int argc, char** argv) {
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()("help,H", "produce help message")(
    "path,P",
    boost::program_options::value<std::string>(),
    "path of the created output file (defaults to directory the file is run in)")(
    "version,V", boost::program_options::value<int>(), "Version to forge (defaults to latest)");
  boost::program_options::variables_map vm;
  try {
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  } catch (std::exception& e) {
    fmt::print("FAILED: {}\n", e.what());
    return 1;
  }
  boost::program_options::notify(vm);
  if (vm.count("help")) {
    std::cout << desc << std::endl;
    return 0;
  }

  int version = shyft::dtss::latest_version;
  if (vm.count("version"))
    version = vm["version"].as<int>();
  std::uint8_t valid_version = static_cast<std::uint8_t>(version);
  if (
    !shyft::protocols::is_valid_version<shyft::dtss::protocol>(valid_version)
    || shyft::protocols::is_internal_version<shyft::dtss::protocol>(valid_version)) {
    fmt::print(
      "Can not forge version {}, valid versions to forge are in the range [{},{}]\n",
      version,
      shyft::dtss::min_version,
      shyft::dtss::latest_version);
    return 1;
  }

  auto path = std::filesystem::current_path();
  if (vm.count("path")) {
    auto path_s = vm["path"].as<std::string>();
    path = std::filesystem::path(path_s);
  }
  if (!std::filesystem::is_directory(path)) {
    fmt::print("Path {} is not a valid directory\n", path);
    return 1;
  }

  auto file_name = fmt::format("dtss_protocol_{}.bin", static_cast<int>(valid_version));
  path.append(file_name);
  shyft::dtss::version_forge::forge_version(valid_version, path);

  return 0;
}