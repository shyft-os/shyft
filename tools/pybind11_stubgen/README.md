
Note
----
This part of the Shyft code is subject to the pybind11_stubgen open source project
licensed under a permissive license. 
All changes in this directory and subdirectories are subject to 
the original LICENSE as attached in this directory.

The use in Shyft is for stubs-generation only, and for pybind11, as long as
this is needed to get working intellisense in IDE like pycharm,clion,vscode.

The original README.rst contents follows 

About
----

Static analysis tools and IDE usually struggle to understand python binary extensions.
`pybind11-stubgen` generates [stubs](https://peps.python.org/pep-0561/) for python extensions to make them less opaque.

While the CLI tool includes tweaks to target modules compiled specifically
with [pybind11](https://github.com/pybind/pybind11) but it should work well with modules built with other libraries.

```bash
# Install
pip install pybind11-stubgen

# Generate stubs for numpy
pybind11-stubgen numpy
```

Usage
-----

```
pybind11-stubgen [-h]
                 [-o OUTPUT_DIR]
                 [--root-suffix ROOT_SUFFIX]
                 [--ignore-invalid-expressions REGEX]
                 [--ignore-invalid-identifiers REGEX]
                 [--ignore-unresolved-names REGEX]
                 [--ignore-all-errors]
                 [--enum-class-locations REGEX:LOC]
                 [--numpy-array-wrap-with-annotated|
                  --numpy-array-use-type-var|
                  --numpy-array-remove-parameters]
                 [--print-invalid-expressions-as-is]
                 [--print-safe-value-reprs REGEX]
                 [--exit-code]
                 [--stub-extension EXT]
                 MODULE_NAME
```
