#pragma once
#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <ranges>
#include <string>
#include <vector>

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

#include <shop_lib_interface.h> // External Shop library

namespace shop_cxx_api_generator {

  using namespace std::string_literals;

  static char const * indentation = "  ";

  constexpr auto n_attributes =
    256; // NB: Must be large enough to include all attributes for every type (as of Shop API
         // v14.4.1.1 type 13, global_settings, is largest with 164 attributes)
         // Will throw during execution if any type has n_attributes or more, needs to
         // always be bigger then maximum

  constexpr auto cstr_maxsize = 64;

  auto checked_attr_indeces(ShopSystem* shop, int const objectTypeIndex) {
    std::vector<int> att(n_attributes);
    int n_attributes_in_api = n_attributes;
    ShopGetObjectTypeAttributeIndices(shop, objectTypeIndex, n_attributes_in_api, att.data());
    if (n_attributes_in_api >= n_attributes) {
      throw std::runtime_error(
        fmt::format("ERROR: possibly not all attributes discovered, increase n_attributes from {}", n_attributes));
    }
    att.resize(n_attributes_in_api);
    return att;
  }

  //
  // Helper function for printing an enum from specified set of entries
  //
  void print_enum_vec(std::ostream& os, std::string const & enumName, auto const & enumEntries) {
    fmt::print(os, "enum class {}{{\n", enumName);
    std::ranges::for_each(enumEntries, [&](auto const & e) {
      fmt::print(os, "{},\n", e);
    });
    fmt::print(os, "}};\n");
    fmt::print(os, "using {}_info_t = std::tuple<{}, const char* const>;\n", enumName, enumName);
    fmt::print(os, "static constexpr {}_info_t {}_info[] {{\n", enumName, enumName);
    fmt::print(
      os,
      "{}",
      fmt::join(
        std::views::transform(
          enumEntries,
          [&](auto const & e) {
            return fmt::format("{{ {}::{}, \"{}\"}}", enumName, e, e);
          }),
        ",\n"));
    fmt::print(os, "\n}};\n");
  }

  void print_enum_map(std::ostream& os, std::string const & enumName, auto const & enumEntries) {
    fmt::print(os, "enum class {}{{\n", enumName);
    fmt::print(
      os,
      "{}",
      fmt::join(
        std::views::transform(
          enumEntries,
          [](auto const & e) {
            auto const& [name, type] = e;
            return fmt::format("{}", type);
          }),
        ",\n"));
    fmt::print(os, "\n}};\n");
    fmt::print(os, "using {}_info_t = std::tuple<{}, const char* const>;\n", enumName, enumName);
    fmt::print(os, "// clang-format off\n");
    fmt::print(os, "static constexpr {}_info_t {}_info[] {{\n", enumName, enumName);
    fmt::print(
      os,
      "{}",
      fmt::join(
        std::views::transform(
          enumEntries,
          [&](auto const & e) {
            auto const& [name, type] = e;
            return fmt::format("{}{{ {}::{}, \"{}\"}}", indentation, enumName, type, name);
          }),
        ",\n"));
    fmt::print(os, "\n}};\n");
    fmt::print(os, "// clang-format on\n");
  }

  //
  // Generate enum representing object types
  //
  void print_object_types(ShopSystem* shop, std::ostream& os) {
    int const n = ShopGetObjectTypeCount(shop);
    auto enumEntries = std::views::transform(std::views::iota(0, n), [&](auto const i) {
      char const * v = ShopGetObjectTypeName(shop, i);
      char const * w = v;
      if (v == std::string_view("system"))
        v = "opt_system";
      return std::make_tuple(w, v);
    });
    print_enum_map(os, "shop_object_type", enumEntries);
  }

  //
  // Generate various enums and structs for representing attribute information
  //
  auto print_attribute_info_types(
    ShopSystem* shop,
    std::ostream& os,
    std::map<std::string, std::string>& dataTypes,
    std::map<std::string, std::string>& xUnits,
    std::map<std::string, std::string>& yUnits) {
    // Collect attribute info
    auto makeUnitIdentifier =
      [](std::string unit) { // Helper func for converting unit string into valid enum identifier
        if (unit == "%") {
          unit = "percent";
          return unit;
        }
        std::size_t p = unit.find('/', 0);
        while (p != std::string::npos) {
          unit[p] = '_';
          unit.insert(p + 1, "per_");
          p = unit.find('/', p + 5);
        }
        std::ranges::transform(unit, unit.begin(), [](auto const & c) {
          return std::tolower(c);
        });
        return unit;
      };

    std::ranges::for_each(std::views::iota(0, ShopGetObjectTypeCount(shop)), [&](auto const i) {
      auto attributeIndices = checked_attr_indeces(shop, i);
      std::ranges::for_each(attributeIndices, [&](auto const attributeIndex) {
        char objectTypeName[cstr_maxsize];
        char attributeName[cstr_maxsize];
        char attributeDataFuncName[cstr_maxsize];
        char attributeDatatype[cstr_maxsize];
        char xUnit[cstr_maxsize];
        char yUnit[cstr_maxsize];
        bool isObjectParam, isInput, isOutput;
        ShopGetAttributeInfo(
          shop,
          attributeIndex,
          objectTypeName,
          isObjectParam,
          attributeName,
          attributeDataFuncName,
          attributeDatatype,
          xUnit,
          yUnit,
          isInput,
          isOutput);
        std::string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
        if (!dataTypes.contains(attributeDatatype))
          dataTypes[attributeDatatype] = "shop_"s + attributeDatatype;
        if (!xUnits.contains(xUnit))
          xUnits[xUnit] = makeUnitIdentifier(xUnit);
        if (!yUnits.contains(yUnit))
          yUnits[yUnit] = makeUnitIdentifier(yUnit);
      });
    });

    // Print attribute data type enum
    print_enum_map(os, "shop_data_type", dataTypes);
    fmt::print(os, "\n");

    // Print attribute X unit enum
    print_enum_map(os, "shop_x_unit", xUnits);
    fmt::print(os, "\n");

    // Print attribute Y unit enum
    print_enum_map(os, "shop_y_unit", yUnits);
    fmt::print(os, "\n");

    // Print attribute info struct
    fmt::print(os, "struct shop_attribute_info {{\n");
    fmt::print(os, "shop_data_type data_type;\n");
    fmt::print(os, "shop_x_unit x_unit;\n");
    fmt::print(os, "shop_y_unit y_unit;\n");
    fmt::print(os, "bool is_object_parameter;\n");
    fmt::print(os, "bool is_input;\n");
    fmt::print(os, "bool is_output;\n");
    fmt::print(os, "}};\n");
  }

  //
  // Generate enum representing attribute types for a single object type
  //
  void print_attribute_types_for_object_type(
    ShopSystem* shop,
    std::ostream& os,
    int const objectTypeIndex,
    std::map<std::string, std::string> const & dataTypes,
    std::map<std::string, std::string> const & xUnits,
    std::map<std::string, std::string> const & yUnits) {
    char const * const objectTypeName = ShopGetObjectTypeName(shop, objectTypeIndex);
    fmt::print(os, "enum class shop_{}_attribute {{\n", objectTypeName);

    auto attributeIndices = checked_attr_indeces(shop, objectTypeIndex);
    fmt::print(
      os,
      "{}",
      fmt::join(
        std::views::transform(
          attributeIndices,
          [&](auto const attributeIndex) {
            char const * attributeName = ShopGetAttributeName(shop, attributeIndex);
            return fmt::format("{} = {}", attributeName, attributeIndex);
          }),
        ",\n"));
    fmt::print(os, "\n}};\n");

    if (!attributeIndices.empty()) {
      fmt::print(
        os,
        "using {}_attribute_info_t = std::tuple<shop_{}_attribute, const char* const, shop_attribute_info>;\n",
        objectTypeName,
        objectTypeName);
      fmt::print(os, "// clang-format off\n");
      fmt::print(
        os, "static constexpr {}_attribute_info_t shop_{}_attribute_info[] {{\n", objectTypeName, objectTypeName);
      fmt::print(
        os,
        "{}",
        fmt::join(
          std::views::transform(
            attributeIndices,
            [&](auto const attributeIndex) {
              char objectTypeName[cstr_maxsize];
              char attributeName[cstr_maxsize];
              char attributeDataFuncName[cstr_maxsize];
              char attributeDatatype[cstr_maxsize];
              char xUnit[cstr_maxsize];
              char yUnit[cstr_maxsize];
              bool isObjectParam, isInput, isOutput;
              ShopGetAttributeInfo(
                shop,
                attributeIndex,
                objectTypeName,
                isObjectParam,
                attributeName,
                attributeDataFuncName,
                attributeDatatype,
                xUnit,
                yUnit,
                isInput,
                isOutput);
              std::string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
              if (auto dt_itr = dataTypes.find(attributeDatatype); dt_itr != dataTypes.end())
                dataTypeIdentifier = dt_itr->second;
              if (auto xu_itr = xUnits.find(xUnit); xu_itr != xUnits.end())
                xUnitIdentifier = xu_itr->second;
              if (auto yu_itr = yUnits.find(yUnit); yu_itr != yUnits.end())
                yUnitIdentifier = yu_itr->second;
              auto bool_str = [](bool const v) {
                return (v ? "true" : "false");
              };
              std::string flags = fmt::format(
                ".is_object_parameter = {}, .is_input = {}, .is_output = {}",
                bool_str(isObjectParam),
                bool_str(isInput),
                bool_str(isOutput));
              return fmt::format(
                "{}{{ shop_{}_attribute::{}, \"{}\", {{ .data_type = shop_data_type::{}, .x_unit = shop_x_unit::{}, "
                ".y_unit = shop_y_unit::{}, {} }} }}",
                indentation,
                objectTypeName,
                attributeName,
                attributeName,
                dataTypeIdentifier,
                xUnitIdentifier,
                yUnitIdentifier,
                flags);
            }),
          ",\n"));
      fmt::print(os, "\n}};\n");
      fmt::print(os, "// clang-format on\n");
    }
  }

  //
  // Generate enum representing attribute types for a single object type
  //

  std::string api_class_name(std::string_view sintef_name) {
    if (sintef_name == "generator")
      return "unit";
    if (sintef_name == "plant")
      return "power_plant";
    if (sintef_name == "system")
      return "opt_system";
    return std::string(sintef_name);
  }

  //
  // Extract enum entries representing relation types
  //
  inline std::vector<std::string> get_relation_types(ShopSystem* shop, char const * objectTypeName) {
    int nRelationTypes, maxRelationTypeLength;
    if (
      !ShopGetRelationTypesDimensions(shop, objectTypeName, nRelationTypes, maxRelationTypeLength)
      || nRelationTypes <= 0)
      return {};
    std::vector<std::unique_ptr<char[]>> relationTypeBuffers(nRelationTypes);
    for (int i = 0; i < nRelationTypes; ++i) {
      relationTypeBuffers[i] = std::make_unique_for_overwrite<char[]>(maxRelationTypeLength + 1);
      relationTypeBuffers[i].get()[0] = '\0';
    }
    std::vector<char*> relationTypePtrs(nRelationTypes);
    std::ranges::transform(relationTypeBuffers, std::ranges::begin(relationTypePtrs), [](auto& r) {
      return r.get();
    });

    if (!ShopGetRelationTypes(shop, objectTypeName, relationTypePtrs.data()))
      return {};

    std::vector<std::string> relationTypes(nRelationTypes);
    std::ranges::transform(relationTypePtrs, std::ranges::begin(relationTypes), [](auto* p) {
      return std::string(p);
    });
    return relationTypes;
  }

  void print_shop_proxy_class_begin(std::ostream& os, std::string_view class_name, int o_type) {
    auto c_name = api_class_name(class_name);
    fmt::print(os, "template<class A>\n");
    fmt::print(os, "struct {}:obj<A,{}> {{\n", c_name, o_type);
    fmt::print(os, "using super=obj<A,{}>;\n", o_type);
    fmt::print(os, "{}()=default;\n", c_name);
    fmt::print(os, "{}(A* s,int oid):super(s, oid) {{}}\n", c_name);
    fmt::print(os, "{}(const {}& o):super(o) {{}}\n", c_name, c_name);
    fmt::print(os, "{}({}&& o) noexcept :super(std::move(o)) {{}}\n", c_name, c_name);
    fmt::print(os, "{}& operator=(const {}& o) {{\n", c_name, c_name);
    fmt::print(os, "super::operator=(o);\n");
    fmt::print(os, "return *this;\n");
    fmt::print(os, "}}\n");
    fmt::print(os, "{}& operator=({}&& o) noexcept {{\n", c_name, c_name);
    fmt::print(os, "super::operator=(std::move(o));\n");
    fmt::print(os, "return *this;\n");
    fmt::print(os, "}}\n");
    fmt::print(os, "// attributes\n");
  }

  void print_shop_proxy_attribute(
    std::ostream& os,
    std::string_view class_name,
    std::string_view attr_name,
    int aid,
    std::string_view a_type,
    bool read_only,
    std::string_view x_unit,
    std::string_view y_unit,
    bool output_skip = true) {
    std::string attr_type;
    if (a_type == "int")
      attr_type = "int";
    else if (a_type == "int_array")
      attr_type = "std::vector<int>";
    else if (a_type == "double")
      attr_type = "double";
    else if (a_type == "double_array")
      attr_type = "std::vector<double>";
    else if (a_type == "txy")
      attr_type = "::shyft::time_series::dd::apoint_ts";
    else if (a_type == "xy")
      attr_type = "::shyft::energy_market::hydro_power::xy_point_curve_with_z";
    else if (a_type == "xy_array")
      attr_type = "std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>";
    else if (a_type == "xyt")
      attr_type = "std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>";
    else if (a_type == "string")
      attr_type = "std::string";
    if (attr_type.empty()) {
      fmt::print(os, "//--TODO: ");
      if (output_skip)
        fmt::print(
          std::cout, "Skipping proxy attribute for {}.{} of unsupported type {}\n", class_name, attr_name, a_type);
      attr_type = a_type;
    }
    fmt::print(
      os, "{}<{},{},{},{},{}> ", (read_only ? "ro" : "rw"), api_class_name(class_name), aid, attr_type, x_unit, y_unit);
    fmt::print(os, "{}{{this}};\n", attr_name);
  }

  void print_shop_proxy_relation_enum(ShopSystem* shop, std::ostream& os, char const * objectTypeName) {
    if (auto relationTypes = get_relation_types(shop, objectTypeName); !relationTypes.empty()) {
      fmt::print(os, "enum relation {{\n");
      fmt::print(
        os,
        "{}",
        fmt::join(
          std::views::transform(
            relationTypes,
            [](auto const & relationType) {
              auto enumEntry = relationType;
              std::ranges::replace(enumEntry, ' ', '_');
              return fmt::format("{}", enumEntry);
            }),
          ",\n"));
      fmt::print(os, "\n}};\n");
      fmt::print(os, "static constexpr const char *relation_name(relation R){{\n");
      fmt::print(os, "switch(R){{\n");
      fmt::print(
        os,
        "{}",
        fmt::join(
          std::views::transform(
            relationTypes,
            [](auto const & relationType) {
              auto enumEntry = relationType;
              std::ranges::replace(enumEntry, ' ', '_');
              return fmt::format("case relation::{}: return \"{}\"", enumEntry, relationType);
            }),
          ";\n"));
      fmt::print(os, ";\ndefault: return nullptr;\n");
      fmt::print(os, "}}\n");
      fmt::print(os, "}}\n");
    }
  }

  void print_shop_proxy_for_object_type(
    ShopSystem* shop,
    std::ostream& os,
    int const objectTypeIndex,
    std::map<std::string, std::string> const & dataTypes,
    std::map<std::string, std::string> const & xUnits,
    std::map<std::string, std::string> const & yUnits,
    bool output_skip = true) {
    char const * const objectTypeName = ShopGetObjectTypeName(shop, objectTypeIndex);
    print_shop_proxy_class_begin(os, objectTypeName, objectTypeIndex);

    print_shop_proxy_relation_enum(shop, os, objectTypeName);

    auto attributeIndeces = checked_attr_indeces(shop, objectTypeIndex);
    std::ranges::for_each(attributeIndeces, [&](auto const attributeIndex) {
      char objectTypeName[cstr_maxsize];
      char attributeName[cstr_maxsize];
      char attributeDataFuncName[cstr_maxsize];
      char attributeDatatype[cstr_maxsize];
      char xUnit[cstr_maxsize];
      char yUnit[cstr_maxsize];
      bool isObjectParam, isInput, isOutput;
      ShopGetAttributeInfo(
        shop,
        attributeIndex,
        objectTypeName,
        isObjectParam,
        attributeName,
        attributeDataFuncName,
        attributeDatatype,
        xUnit,
        yUnit,
        isInput,
        isOutput);
      std::string xUnitIdentifier, yUnitIdentifier;
      if (auto xu_itr = xUnits.find(xUnit); xu_itr != xUnits.end())
        xUnitIdentifier = xu_itr->second;
      if (auto yu_itr = yUnits.find(yUnit); yu_itr != yUnits.end())
        yUnitIdentifier = yu_itr->second;
      print_shop_proxy_attribute(
        os,
        objectTypeName,
        attributeName,
        attributeIndex,
        attributeDatatype,
        isOutput,
        xUnitIdentifier,
        yUnitIdentifier,
        output_skip);
    });
    fmt::print(os, "}};\n");
  }

  void print_shop_proxy_header_begin(std::ostream& os) {
    fmt::print(os, "#pragma once\n");
    fmt::print(os, "// auto genereated files based on active version of Sintef SHOP api dll\n");
    fmt::print(os, "#include <shyft/energy_market/stm/shop/api/shop_proxy.h>\n");
    fmt::print(os, "#include <shyft/energy_market/stm/shop/api/shop_units.h>\n");
    fmt::print(os, "namespace shop {{\n\n");
    fmt::print(os, "using proxy::obj;\n");
    fmt::print(os, "using proxy::rw;\n");
    fmt::print(os, "using proxy::ro;\n");
    fmt::print(os, "using namespace proxy::unit;\n\n");
  }

  void print_shop_proxy_header_end(std::ostream& os) {
    fmt::print(os, "\n}}\n");
  }

  //
  // Generate enum representing attribute types for all object types
  //
  void print_attribute_types(ShopSystem* shop, std::ostream& os, std::ostream& proxy_os, bool output_skip = true) {
    // Print attributes for each object, and collect unique attribute information such as data type, unit etc.
    std::map<std::string, std::string> dataTypes, xUnits, yUnits;
    print_attribute_info_types(shop, os, dataTypes, xUnits, yUnits);
    print_shop_proxy_header_begin(proxy_os);
    std::ranges::for_each(std::views::iota(0, ShopGetObjectTypeCount(shop)), [&](auto const i) {
      fmt::print(os, "\n");
      print_attribute_types_for_object_type(shop, os, i, dataTypes, xUnits, yUnits);
      print_shop_proxy_for_object_type(shop, proxy_os, i, dataTypes, xUnits, yUnits, output_skip);
    });
    print_shop_proxy_header_end(proxy_os);
  }

  //
  // Generate enum representing relation types
  //
  void print_relation_types(ShopSystem* shop, std::ostream& os) {
    for (int i = 0, n = ShopGetObjectTypeCount(shop); i < n; ++i) {
      char const * const objectTypeName = ShopGetObjectTypeName(shop, i);
      std::vector<std::pair<std::string, std::string>> enumEntries;
      std::ranges::transform(
        get_relation_types(shop, objectTypeName),
        std::back_inserter(enumEntries),
        [](std::string const & relationType) {
          auto enumEntry = relationType;
          std::ranges::replace(enumEntry, ' ', '_');
          return std::pair{relationType, enumEntry};
        });
      if (!enumEntries.empty())
        print_enum_map(os, std::string("shop_") + objectTypeName + "_relation", enumEntries);
    }
  }

  //
  // Generate enum representing command types
  //
  void print_command_types(ShopSystem* shop, std::ostream& os) {
    int const nCommandTypeBuffer = 1000;
    std::vector<char*> commandTypes(nCommandTypeBuffer);
    for (int i = 0; i < nCommandTypeBuffer; ++i)
      commandTypes[i] = new char[128];
    int nCommandTypes = nCommandTypeBuffer;
    if (ShopGetCommandTypesInSystem(shop, nCommandTypes, &commandTypes[0])) {
      std::vector<std::pair<std::string, std::string>> enumEntries;
      for (int i = 0; i < nCommandTypes; ++i) {
        std::string enumEntry = commandTypes[i];
        replace(enumEntry.begin(), enumEntry.end(), ' ', '_');
        enumEntries.emplace_back(commandTypes[i], enumEntry);
      }
      print_enum_map(os, "shop_command", enumEntries);
    }
    for (int i = 0; i < nCommandTypeBuffer; ++i)
      delete[] commandTypes[i];
  }

  //
  // Generate various other enums and structs
  //
  void print_other(ShopSystem* /*shop*/, std::ostream& os) {
    print_enum_map(
      os,
      "shop_severity",
      std::vector<std::pair<std::string, std::string>>({
        {"ok", "OK"}
    }));
    fmt::print(os, "\n");
    print_enum_vec(os, "shop_time_resolution_unit", std::vector<std::string>({{"hour"}, {"minute"}}));
  }
}
