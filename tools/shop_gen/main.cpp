#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include <fmt/core.h>
#include <fmt/ostream.h>

#include <shop_cxx_api_generator.h>
#include <shop_lib_interface.h> // External Shop library

#define STRINGIZE1(x) #x
#define STRINGIZE2(x) STRINGIZE1(x)

// Externals for Shop library
void SHOP_log_error(char*) {
}

void SHOP_log_info(char*) {
}

void SHOP_log_warning(char*) {
}

void SHOP_exit(void*) {
}

int main(int argc, char* argv[]) {
  fmt::print("Shop C++ API generator for version  {} \n", STRINGIZE2(SHOP_API_VERSION));
  std::string enum_file_name = "shop_enums.h";
  std::string api_file_name = "shop_api.h";
  fmt::print("nargs {}\n", argc);
  if (argc < 2) {
    fmt::print("Defaulting to working directory and file names {} and {} \n", enum_file_name, api_file_name);
  } else if (argc < 3) {
    enum_file_name = argv[1];
    fmt::print("Writing to enum file: {}\n", enum_file_name);
    fmt::print("Writing default api file in working directory: {}\n", api_file_name);
  } else if (argc < 4) {
    enum_file_name = argv[1];
    api_file_name = argv[2];
    fmt::print("Writing to enum file: {}\n", enum_file_name);
    fmt::print("Writing to api file: {}\n", api_file_name);
  } else {
    fmt::print(std::cerr, "Invalid usage: Maximum 2 arguments supported - output enum file and api file!");
    return 2;
  }

  std::ofstream ofs(enum_file_name, std::ofstream::out);
  if (!ofs) {
    fmt::print(std::cerr, "Failed to open enum file \"{}\" for writing\n", enum_file_name);
    return 2;
  }
  std::ofstream pofs(api_file_name, std::ofstream::out);
  if (!pofs) {
    fmt::print(std::cerr, "Failed to open api file \"{}\" for writing\n", api_file_name);
    return 2;
  }
  std::unique_ptr<ShopSystem, bool (*)(ShopSystem*)> shop_safe(ShopInit(true, true, SHOP_LICENCE_DIR), ShopFree);
  ShopSystem* shop = shop_safe.get(); // for convenience calling the next functions
  ShopSetSilentConsole(shop, true);
  fmt::print(ofs, "#pragma once\n");
  fmt::print(ofs, "#include <tuple>\n");
  fmt::print(ofs, "//NOTE: auto generated file\n");
  fmt::print(ofs, "namespace shop::enums {{\n");

  shop_cxx_api_generator::print_object_types(shop, ofs);
  fmt::print(ofs, "\n");
  shop_cxx_api_generator::print_attribute_types(shop, ofs, pofs);
  fmt::print(ofs, "\n");
  shop_cxx_api_generator::print_relation_types(shop, ofs);
  fmt::print(ofs, "\n");
  shop_cxx_api_generator::print_command_types(shop, ofs);
  fmt::print(ofs, "\n");
  shop_cxx_api_generator::print_other(shop, ofs);
  fmt::print(ofs, "\n}}\n");
}
