include_guard()

add_library(shyft-public_flags INTERFACE)
target_compile_features(shyft-public_flags INTERFACE cxx_std_23)
target_compile_definitions(shyft-public_flags INTERFACE $<$<CONFIG:Debug>:SHYFT_DEBUG> BOOST_ASIO_NO_DEPRECATED)

add_library(shyft-private_flags INTERFACE)
# We are consuming boost-archives when building or libs
# so interpreting this https://boost.org.cpp.al/development/separate_compilation.html,
# plus experience on windows ms vc ++ and msys2/mingw64, strongly support
# this option as a private flag, to ensure template implementations are available
# and instantiated (typical symptoms are undef sym to boost::archive::detail::..map::erase(..)
#
target_compile_definitions(shyft-private_flags INTERFACE BOOST_ARCHIVE_SOURCE)
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_link_libraries(shyft-public_flags INTERFACE quadmath)
  if(MINGW)
    #FIXME: this works similar when located in toolchains/gcc.cmake
    target_compile_definitions(shyft-public_flags INTERFACE WIN32_LEAN_AND_MEAN)
    target_compile_options(shyft-public_flags INTERFACE -Wa,-mbig-obj)
  endif()
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  target_compile_options(shyft-public_flags INTERFACE -bigobj -MP4 -Zc:preprocessor)
  #FIXME: most of these should probably be in tooclchain, set at top level - jeh
  target_compile_definitions(shyft-public_flags INTERFACE
    _CRT_SECURE_NO_WARNINGS _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS _WIN32_WINNT=0x0601 
    STRICT WIN32_LEAN_AND_MEAN NOMINMAX _HAS_DEPRECATED_IS_LITERAL_TYPE=1)
endif()

if(SHYFT_WITH_COVERAGE)
  # a small note here: the -fprofile-update=atomic ensure that the coverage counters are updated atomically, needed
  # in multithreaded environments, like Shyft
  target_compile_options(shyft-private_flags INTERFACE $<$<CONFIG:DEBUG>:--coverage -fprofile-abs-path -fprofile-update=atomic>)
  target_link_options(shyft-private_flags INTERFACE $<$<CONFIG:DEBUG>:--coverage>)
endif()
