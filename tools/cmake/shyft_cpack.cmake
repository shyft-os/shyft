include(shyft_package_components)
set(CPACK_COMPONENTS_ALL ${SHYFT_COMPONENTS_ALL})
set(CPACK_COMPONENTS_GROUPING "IGNORE")

cmake_host_system_information(RESULT DIST_ID QUERY DISTRIB_ID)

if("${DIST_ID}" STREQUAL "arch")
  set(CPACK_GENERATOR "External")
elseif("${DIST_ID}" STREQUAL "fedora")
  set(CPACK_GENERATOR "RPM")
else()
  set(CPACK_GENERATOR "ZIP")
endif()

include(CPack)
configure_file(${CMAKE_CURRENT_LIST_DIR}/shyft_cpack_config.cmake.in shyft_cpack_config.cmake @ONLY)

