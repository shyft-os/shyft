include_guard()

include(shyft_compiler)

find_package(fmt REQUIRED)
find_package(cblas)
find_package(dlib REQUIRED)
find_package(Armadillo REQUIRED CONFIG)
find_package(OpenSSL REQUIRED)
find_package(RocksDB REQUIRED)

target_compile_definitions(shyft-public_flags INTERFACE
        BOOST_PHOENIX_STL_TUPLE_H_
        BOOST_ALLOW_DEPRECATED_HEADERS 
        BOOST_BIND_GLOBAL_PLACEHOLDERS BOOST_BEAST_SEPARATE_COMPILATION
        BOOST_ERROR_CODE_HEADER_ONLY BOOST_VARIANT_MINIMIZE_SIZE
        BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS BOOST_MPL_LIMIT_LIST_SIZE=30
)

if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.30.0")
  cmake_policy(SET CMP0167 OLD)  # silence cmake warning that indicate use of cmake provided boost support in some cases
endif()
find_package(Boost REQUIRED COMPONENTS system filesystem serialization thread atomic chrono program_options
  OPTIONAL_COMPONENTS headers)
if(Boost_VERSION VERSION_LESS "1.84.0")
    message(STATUS "Boost version ${Boost_VERSION}, earlier than 1.84 detected. Adding BOOST_NO_CXX17_HDR_OPTIONAL and BOOST_NO_CXX17_HDR_VARIANT.")
    target_compile_definitions(shyft-public_flags INTERFACE BOOST_NO_CXX17_HDR_OPTIONAL BOOST_NO_CXX17_HDR_VARIANT)
endif()
if(Boost_VERSION VERSION_LESS "1.90.0")
    message(STATUS "Boost version ${Boost_VERSION}, earlier than 1.90 detected. Using geometry epsg fast")
    target_compile_definitions(shyft-public_flags INTERFACE SHYFT_EPSG_FAST)
endif()
if (SHYFT_WITH_PYTHON)
  find_package(Python3 REQUIRED COMPONENTS Interpreter Development NumPy)
  find_package(pybind11 REQUIRED)
endif()

include(CTest)
if(BUILD_TESTING)
  find_package(doctest REQUIRED)
  include(doctest)
  find_package(ShyftData REQUIRED)
endif()

if(SHYFT_WITH_ENERGY_MARKET)
  find_package(Qt5 REQUIRED COMPONENTS Widgets Charts)
  if(SHYFT_WITH_STM)
    if(SHYFT_WITH_SHOP)
      find_package(CPLEX REQUIRED)
      find_package(SHOP REQUIRED)
      find_program(PATCHELF NAMES patchelf REQUIRED)
      execute_process(COMMAND ${PATCHELF} "--add-rpath" ${CPLEX_LIBRARY_DIR} ${SHOP_CPLEX_LIBRARY}
	      OUTPUT_STRIP_TRAILING_WHITESPACE
	      COMMAND_ERROR_IS_FATAL ANY)
    endif()
  endif()
endif()

if(SHYFT_WITH_BENCHMARKS)
  find_package(benchmark REQUIRED)
  find_package(Boost REQUIRED COMPONENTS program_options)
endif()

