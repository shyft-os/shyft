include_guard()

include(shyft_install_dirs)
include(shyft_targets)
include(shyft_compiler)

function(shyft_export COMPONENT RESULT)
  set(${RESULT} shyft-${COMPONENT}-targets PARENT_SCOPE)
endfunction()

function(shyft_install_library)
  set(_opts)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  shyft_export(${COMPONENT} EXPORT)

  shyft_install_libdir(${COMPONENT} LIBDIR)
  shyft_install_bindir(${COMPONENT} BINDIR)

  shyft_rpath(COMPONENT ${COMPONENT} TYPE lib RESULT RPATH)
  set_target_properties(
    ${TARGET}
    PROPERTIES
    INSTALL_RPATH ${RPATH})
  install(TARGETS ${TARGET}
    COMPONENT ${COMPONENT}
    EXPORT ${EXPORT}
    LIBRARY DESTINATION ${LIBDIR} NAMELINK_SKIP
    ARCHIVE DESTINATION ${LIBDIR}
    RUNTIME DESTINATION ${BINDIR})
endfunction()

function(shyft_install_bundled_files)
  set(_opts)
  set(_single_opts COMPONENT)
  set(_multi_opts FILES)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})
  set(FILES ${_args_FILES})
  shyft_install_bundled_libdir(${COMPONENT} LIBDIR)
  install(FILES ${FILES} COMPONENT ${COMPONENT} DESTINATION ${LIBDIR})
endfunction()

function(shyft_install_bundled_library)
  set(_opts)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  get_target_property(LIB ${TARGET} IMPORTED_LOCATION)
  shyft_install_bundled_libdir(${COMPONENT} LIBDIR)

  install(PROGRAMS ${LIB} COMPONENT ${COMPONENT} DESTINATION ${LIBDIR})
endfunction()

shyft_export(runtime SHYFT_BUILTIN_EXPORT)
install(
  TARGETS shyft-public_flags shyft-private_flags
  EXPORT ${SHYFT_BUILTIN_EXPORT}
  LIBRARY DESTINATION ${SHYFT_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${SHYFT_INSTALL_LIBDIR})

function(shyft_install_headers)
  set(_opts)
  set(_single_opts COMPONENT DIRECTORY)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})
  set(DIRECTORY ${_args_DIRECTORY})
  shyft_install_includedir(${COMPONENT} DESTINATION)
  install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY}
    COMPONENT ${COMPONENT}
    DESTINATION ${DESTINATION}
    FILES_MATCHING REGEX ".*[.]h$"
    REGEX "private" EXCLUDE)
endfunction()

function(shyft_install_generated_headers)
  set(_opts)
  set(_single_opts COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})
  shyft_install_includedir(${COMPONENT} DESTINATION)
  install(DIRECTORY 
    ${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}/
    COMPONENT ${COMPONENT}
    DESTINATION ${DESTINATION}
    FILES_MATCHING REGEX ".*[.]h$"
    REGEX "private" EXCLUDE)
endfunction()

if(SHYFT_WITH_PYTHON)

  function(shyft_install_python_module)
    set(_opts)
    set(_single_opts TARGET COMPONENT PATH)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    set(COMPONENT ${_args_COMPONENT})
    set(PATH ${_args_PATH})
    shyft_install_pythondir(${COMPONENT} DESTINATION)

    shyft_rpath(COMPONENT ${COMPONENT} TYPE python PATH ${PATH} RESULT RPATH)
    set_target_properties(
      ${TARGET}
      PROPERTIES
      INSTALL_RPATH ${RPATH})

    if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
      # on windows, due to the os.add_dll_directory and related requirements
      # the dependent .dlls has to be in the 'safe' directories prior
      # to any loading of the python extension (e.g. time_series).
      # Alternatively: they need to be in same directory(our case)
      # and then we use the opportunity in time_series module to set
      # its location as a safe location.
      install(TARGETS ${TARGET}
        RUNTIME_DEPENDENCIES PRE_EXCLUDE_REGEXES
        "ext-ms-.*"
        "api-ms.*"
        "^python${Python3_VERSION_MAJOR}${Python3_VERSION_MINOR}"
        "^hvsifiletrust\\.dll$"
        "^pdmutilities\\.dll$"
        "^vc.*"
        "^msvcp.*"
        "^concrt.*"
        "^iehsims\\.dll$"
        "^devicelockhelpers\\.dll$"
        "^emclient\\.dll$"
        POST_EXCLUDE_REGEXES
        ".*WINDOWS[\\/]system32.*"
        ".*Windows[\\/]system32.*"
        COMPONENT ${COMPONENT}
        RUNTIME DESTINATION ${DESTINATION}
        LIBRARY DESTINATION ${DESTINATION}/${PATH})
    else()
      install(TARGETS ${TARGET}
        COMPONENT ${COMPONENT}
        LIBRARY DESTINATION ${DESTINATION}/${PATH})
    endif()
  endfunction()
  function(shyft_install_python_source)
    set(_opts)
    set(_single_opts COMPONENT DIRECTORY)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})
    set(DIRECTORY ${_args_DIRECTORY})
    shyft_install_pythondir(${COMPONENT} DESTINATION)

    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY}/
      COMPONENT ${COMPONENT}
      DESTINATION ${DESTINATION}
      FILES_MATCHING
      PATTERN "*.py"
      REGEX "__pycache__" EXCLUDE)
  endfunction()

  function(shyft_install_python_config)
    set(_opts)
    set(_single_opts COMPONENT DIRECTORY)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})
    set(DIRECTORY ${_args_DIRECTORY})
    shyft_install_pythondir(${COMPONENT} DESTINATION)

    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY}/
      COMPONENT ${COMPONENT}
      DESTINATION ${DESTINATION}
      FILES_MATCHING
      PATTERN "*.yml"
      REGEX "__pycache__" EXCLUDE)
  endfunction()
endif()

function(shyft_install_license)
  set(_opts)
  set(_single_opts COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})
  shyft_install_licensedir(${COMPONENT} DESTINATION)
  install(FILES ${PROJECT_SOURCE_DIR}/LICENSE
    COMPONENT ${COMPONENT}
    DESTINATION ${DESTINATION})
endfunction()

function(shyft_install_licenses)
  if(SHYFT_PACKAGE_PROJECT_LAYOUT)
    shyft_install_license(COMPONENT project)
  else()
    foreach(COMPONENT ${SHYFT_PACKAGE_COMPONENTS})
      shyft_install_license(COMPONENT ${COMPONENT})
    endforeach()
  endif()
endfunction()

include(CMakePackageConfigHelpers)

function(shyft_install_cmake_target_registry COMPONENT RESULT)
  if(SHYFT_PAKCAGE_PROJECT_LAYOUT)
    set(${RESULT} "SHYFT_CMAKE_PACKAGE_TARGET_REGISTRY")
  else()
    set(${RESULT} "SHYFT_CMAKE_${COMPONENT}_PACKAGE_TARGET_REGISTRY")
  endif()
  set(${RESULT} ${${RESULT}} PARENT_SCOPE)
endfunction()

foreach(SHYFT_COMPONENT ${SHYFT_COMPONENTS_ALL})
  shyft_install_cmake_target_registry(${SHYFT_COMPONENT} REGISTRY)
  set(${REGISTRY} ""  CACHE INTERNAL "" FORCE)
endforeach()

function(shyft_install_cmake_add_package_target COMPONENT TARGET_COMPONENT)
  shyft_install_cmake_target_registry(${COMPONENT} REGISTRY)
  set(${REGISTRY} "${${REGISTRY}};${TARGET_COMPONENT}" CACHE INTERNAL "" FORCE)
endfunction()

function(shyft_install_cmake_package_targets)
  set(_single_opts COMPONENT)
  set(_multi_opts TARGET_COMPONENTS)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

  set(COMPONENT ${_args_COMPONENT})
  set(TARGET_COMPONENTS ${_args_TARGET_COMPONENTS})
  shyft_install_cmakedir(${COMPONENT} DESTINATION)

  foreach(TARGET_COMPONENT ${TARGET_COMPONENTS})
    install(EXPORT shyft-${TARGET_COMPONENT}-targets
      COMPONENT ${TARGET_COMPONENT}
      NAMESPACE shyft::
      DESTINATION ${DESTINATION})
    shyft_install_cmake_add_package_target(${COMPONENT} ${TARGET_COMPONENT})
  endforeach()

  if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/depends.cmake.in)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/depends.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/shyft-${COMPONENT}-depends.cmake
      @ONLY)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/shyft-${COMPONENT}-depends.cmake
      COMPONENT ${COMPONENT}
      DESTINATION ${DESTINATION})
  endif()

endfunction()

function(shyft_install_cmake_package_name COMPONENT RESULT)
  set(PACKAGE_NAME shyft${COMPONENT_PREFIX_ID})
  if(SHYFT_INSTALL_LAYOUT STREQUAL "PROJECT")
    set(${RESULT} "shyft")
  else()
    shyft_component_name(${COMPONENT} COMPONENT_NAME)
    set(${RESULT} ${COMPONENT_NAME})
  endif()
  set(${RESULT} ${${RESULT}} PARENT_SCOPE)
endfunction()

function(shyft_install_cmake_package_config_should_skip COMPONENT RESULT)
  set(${RESULT} 1)
  if(SHYFT_INSTALL_LAYOUT STREQUAL "PROJECT")
    if(COMPONENT STREQUAL "project")
      set(${RESULT} 0)
    endif()
  else()
    if(NOT COMPONENT STREQUAL "project")
      set(${RESULT} 0)
    endif()
  endif()
  set(${RESULT} ${${RESULT}} PARENT_SCOPE)
endfunction()

function(shyft_install_cmake_package_config)
  set(_opts)
  set(_single_opts COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})

  shyft_install_cmakedir(${COMPONENT} DESTINATION)
  shyft_install_cmake_package_name(${COMPONENT} PACKAGE_NAME)

  set(SHYFT_CMAKE_PACKAGE_NAME ${PACKAGE_NAME})
  shyft_install_cmake_target_registry(${COMPONENT} REGISTRY)
  set(SHYFT_CMAKE_PACKAGE_COMPONENTS ${${REGISTRY}})

  if(NOT SHYFT_CMAKE_PACKAGE_COMPONENTS)
    message(STATUS "ShyftInstall: No components found for cmake-package '${COMPONENT}', skipping cmake-package-config for it.")
    return()
  endif()

  configure_package_config_file(
    ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/templates/package-config.cmake.in
    ${PACKAGE_NAME}-config.cmake
    INSTALL_DESTINATION ${DESTINATION})
  write_basic_package_version_file(
    ${PACKAGE_NAME}-config-version.cmake
    VERSION ${SHYFT_VERSION}
    COMPATIBILITY SameMajorVersion)
  install(
    FILES
    ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE_NAME}-config.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE_NAME}-config-version.cmake
    COMPONENT ${COMPONENT}
    DESTINATION ${DESTINATION})
endfunction()

function(shyft_install_cmake_package_configs)
  if(SHYFT_PACKAGE_PROJECT_LAYOUT)
    shyft_install_cmake_package_config(COMPONENT project)
  else()
    foreach(COMPONENT ${SHYFT_PACKAGE_COMPONENTS})
      shyft_install_cmake_package_config(COMPONENT ${COMPONENT})
    endforeach()
  endif()
endfunction()

if(SHYFT_WITH_DEBUG)
  find_package(Debugedit REQUIRED)

  function(shyft_install_debug_sources)
    set(_opts)
    set(_single_opts COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(COMPONENT ${_args_COMPONENT})

    shyft_install_sourcedir(${COMPONENT} SOURCEDIR)
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
      ${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}/
      COMPONENT ${COMPONENT}
      DESTINATION ${SOURCEDIR}
      FILES_MATCHING REGEX ".*[.][hc]pp$"
      REGEX "private" EXCLUDE)
  endfunction()

  function(shyft_install_debug_symbols)
    set(_opts)
    set(_single_opts TARGET COMPONENT)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    set(COMPONENT ${_args_COMPONENT})

    shyft_buildid(${TARGET} BUILDID_HEAD BUILDID_TAIL)

    shyft_install_symboldir(${COMPONENT} SYMBOLDIR)
    set(SYMBOLS_PATH "${SYMBOLDIR}/.build-id/${BUILDID_HEAD}")
    set(SYMBOLS_FILE "${BUILDID_TAIL}.debug")

    #NOTE:
    #  hacky as hell, but the thought of grepping a 5GB of
    #  symbol-file ascii is too daunting.
    #  - jeh
    if(CMAKE_GENERATOR STREQUAL "Unix Makefiles")
      set(COMP_DIR ${CMAKE_CURRENT_BINARY_DIR})
    elseif(CMAKE_GENERATOR STREQUAL "Ninja")
      set(COMP_DIR ${PROJECT_BINARY_DIR})
    else()
      message(FATAL_ERROR "ShyftTargets: Unsupported generator for symbol-stripping ${CMAKE_GENERATOR}")
    endif()
    
    install(CODE
      "\
execute_process(COMMAND ${DEBUGEDIT_EXECUTABLE}\
  --base-dir=${COMP_DIR}\
  --dest-dir=\${CMAKE_INSTALL_PREFIX}\
  ${SYMBOLS_FILE}\
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMAND_ERROR_IS_FATAL ANY)\
"
      COMPONENT ${COMPONENT})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${SYMBOLS_FILE}
      COMPONENT ${COMPONENT}
      DESTINATION ${SYMBOLS_PATH})

  endfunction()

endif()
