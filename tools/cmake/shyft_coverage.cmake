include_guard()

find_program(GCOV_EXECUTABLE gcov REQUIRED)
find_program(GCOVR_EXECUTABLE gcovr REQUIRED)
set(cov_fix_flags --gcov-ignore-errors --gcov-ignore-parse-errors -j 4)

if(NOT SHYFT_COVERAGE_DIR)
  set(SHYFT_COVERAGE_DIR ${PROJECT_SOURCE_DIR}/reports)
endif()

add_custom_target(shyft-coverage-cpp-html
  COMMAND ${CMAKE_COMMAND} -E rm -rf ${SHYFT_COVERAGE_DIR}/html
  COMMAND ${CMAKE_COMMAND} -E make_directory ${SHYFT_COVERAGE_DIR}/html
  COMMAND ${GCOVR_EXECUTABLE} ${cov_fix_flags} --html --html-details
  -r ${PROJECT_SOURCE_DIR} -f ${PROJECT_SOURCE_DIR}/cpp/shyft/*
  -o ${SHYFT_COVERAGE_DIR}/html/index.html
  BYPRODUCTS ${SHYFT_COVERAGE_DIR}/html
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
  VERBATIM
)

add_custom_target(shyft-coverage-cpp
  COMMAND ${CMAKE_COMMAND} -E rm -f ${SHYFT_COVERAGE_DIR}/shyft-cpp-cobertura.xml
  COMMAND ${CMAKE_COMMAND} -E make_directory ${SHYFT_COVERAGE_DIR}
  COMMAND ${GCOVR_EXECUTABLE} ${cov_fix_flags} -x -s
    -r ${PROJECT_SOURCE_DIR}/ -f ${PROJECT_SOURCE_DIR}/cpp/shyft/*
    -o ${SHYFT_COVERAGE_DIR}/shyft-cpp-cobertura.xml
  BYPRODUCTS ${SHYFT_COVERAGE_DIR}/shyft-cpp-cobertura.xml
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
  VERBATIM
)
