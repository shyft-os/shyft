 include_guard()

include(shyft_package_components)
include(GNUInstallDirs)

macro(shyft_install_generate name EXPRESSION)
  if(NOT COMMAND shyft_install_${name}dir)
    function(shyft_install_${name}dir COMPONENT RESULT)
      shyft_package_name(${COMPONENT} SHYFT_PACKAGE_NAME)
      shyft_package_component_absbase(${COMPONENT} SHYFT_PACKAGE_COMPONENT_ABSBASE)
      shyft_component_abspath(${COMPONENT} SHYFT_COMPONENT_ABSPATH)
      shyft_package_component_abspath(${COMPONENT} SHYFT_PACKAGE_COMPONENT_ABSPATH)
      set(${RESULT} ${EXPRESSION} PARENT_SCOPE)
    endfunction()
  endif()
endmacro()

shyft_install_generate(
  license
  ${CMAKE_INSTALL_DATADIR}/licenses/\${SHYFT_PACKAGE_NAME}-${SHYFT_VERSION})

shyft_install_generate(
  include
  ${CMAKE_INSTALL_INCLUDEDIR}/\${SHYFT_PACKAGE_NAME}-${SHYFT_VERSION})

shyft_install_generate(
  bin
  ${CMAKE_INSTALL_BINDIR})

shyft_install_generate(
  bundled_bin
  ${CMAKE_INSTALL_BINDIR}/\${SHYFT_PACKAGE_NAME}-${SHYFT_VERSION})

shyft_install_generate(
  lib
  ${CMAKE_INSTALL_LIBDIR})

shyft_install_generate(
  bundled_lib
  ${CMAKE_INSTALL_LIBDIR}/\${SHYFT_PACKAGE_NAME}-${SHYFT_VERSION})

shyft_install_generate(
  symbol
  ${CMAKE_INSTALL_LIBDIR}/debug)

shyft_install_generate(
  source
  src/debug/\${SHYFT_PACKAGE_NAME}-${SHYFT_VERSION})

shyft_install_generate(
  doc
  ${CMAKE_INSTALL_DOCDIR}\${SHYFT_PACKAGE_COMPONENT_ABSBASE}-${SHYFT_VERSION}\${SHYFT_PACKAGE_COMPONENT_ABSPATH})

function(shyft_rpath)
  set(_opts)
  set(_single_opts COMPONENT TYPE PATH RESULT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(COMPONENT ${_args_COMPONENT})
  set(PATH ${_args_PATH})
  set(TYPE ${_args_TYPE})
  set(RESULT ${_args_RESULT})

  cmake_language(CALL shyft_install_${TYPE}dir ${COMPONENT} DIR)
  shyft_install_bundled_libdir(${COMPONENT} BUNDLED_LIBDIR)
  cmake_path(APPEND DIR ${PATH} OUTPUT_VARIABLE DIR)
  cmake_path(RELATIVE_PATH BUNDLED_LIBDIR BASE_DIRECTORY ${DIR} OUTPUT_VARIABLE ${RESULT})
  if(SHYFT_WITH_PYTHON_DEVELOPMENT)
    # add extra rpath to the lib directory to allow IDE successfully use built shyft python
    # without the need of fiddle with the LD_LIBRARY_PATH
    cmake_path(RELATIVE_PATH CMAKE_INSTALL_LIBDIR BASE_DIRECTORY ${DIR} OUTPUT_VARIABLE EXTRA_DIR)
    set(${RESULT} "$ORIGIN/${${RESULT}}:$ORIGIN/${EXTRA_DIR}" PARENT_SCOPE)
  else()
    set(${RESULT} "$ORIGIN/${${RESULT}}" PARENT_SCOPE)
  endif()
endfunction()

if(SHYFT_WITH_PYTHON)
  if(NOT COMMAND shyft_install_pythondir)
    if("${SHYFT_HOST}" STREQUAL "LINUX")
      find_package(Python3 REQUIRED Interpreter)
      if("${SHYFT_HOST_ID}" STREQUAL "ARCH")
        set(SHYFT_INSTALL_PYDIR_PREFIX ${Python3_SITELIB})
        string(REGEX REPLACE "^/usr/" "" SHYFT_INSTALL_PYDIR_PREFIX ${SHYFT_INSTALL_PYDIR_PREFIX})
      elseif("${SHYFT_HOST_ID}" STREQUAL "FEDORA" OR "${SHYFT_HOST_ID}" STREQUAL "RHEL")
        set(SHYFT_INSTALL_PYDIR_PREFIX ${Python3_SITELIB})
        string(REGEX REPLACE "^/usr/local/" "" SHYFT_INSTALL_PYDIR_PREFIX ${SHYFT_INSTALL_PYDIR_PREFIX})
      else()
        message(FATAL_ERROR "ShyftHost: unsupported python linux platform ${SHYFT_HOST_ID}")
      endif()
    elseif("${SHYFT_HOST}" STREQUAL "WINDOWS")
      set(SHYFT_INSTALL_PYDIR_PREFIX "python")
    else()
      message(FATAL_ERROR "ShyftHost: unsupported python platform")
    endif()
  endif()
endif()

shyft_install_generate(
  python
  ${SHYFT_INSTALL_PYDIR_PREFIX}/shyft)

shyft_install_generate(
  cmake
  ${CMAKE_INSTALL_LIBDIR}/cmake/\${SHYFT_PACKAGE_NAME}-${SHYFT_VERSION})

