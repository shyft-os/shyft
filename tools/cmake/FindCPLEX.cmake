if(NOT CPLEX_FIND_VERSION)
  set(CPLEX_FIND_VERSION 20.1.0)
endif()

if(NOT CPLEX_FIND_PKG_TOKEN OR NOT CPLEX_FIND_PKG_URL)
  message(FATAL_ERROR
    "Variables CPLEX_FIND_PKG_TOKEN and CPLEX_FIND_PKG_URL must be provided."
    "Using gitlab generic package registry as source the format of these should be like:\n"
    "CPLEX_FIND_PKG_TOKEN=PRIVATE-TOKEN:token-goes-here-nospace\n"
    "CPLEX_FIND_PKG_URL=https://gitlab.com/api/v4/projects/projectid-goes-here/packages/generic\n"
    "and we expect packages shop to be available for the requested versions. For reference:\n"
    "https://docs.gitlab.com/ee/user/packages/generic_packages")
endif()

include(FetchContent)
FetchContent_Declare(
  cplex_pkg
  URL ${CPLEX_FIND_PKG_URL}/cplex/${CPLEX_FIND_VERSION}/cplex.tar.gz
  HTTP_HEADER ${CPLEX_FIND_PKG_TOKEN}
  DOWNLOAD_EXTRACT_TIMESTAMP true)
FetchContent_MakeAvailable(cplex_pkg)

cmake_host_system_information(RESULT OS_NAME QUERY OS_NAME)
string(TOLOWER ${OS_NAME} OS_NAME)

add_library(cplex SHARED IMPORTED GLOBAL)

string(REPLACE "." "" CPLEX_NAME "cplex${CPLEX_FIND_VERSION}")
set(CPLEX_LIBRARY_DIR ${cplex_pkg_SOURCE_DIR}/lib/${OS_NAME})
find_library(CPLEX_LIBRARY
  NAMES ${CMAKE_SHARED_LIBRARY_PREFIX}${CPLEX_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}
  PATHS ${CPLEX_LIBRARY_DIR})
set_target_properties(cplex PROPERTIES IMPORTED_LOCATION ${CPLEX_LIBRARY})

set(CPLEX_VERSION ${CPLEX_FIND_VERSION})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CPLEX
  FOUND_VAR CPLEX_FOUND
  REQUIRED_VARS CPLEX_LIBRARY_DIR CPLEX_LIBRARY
  VERSION_VAR CPLEX_VERSION
)
