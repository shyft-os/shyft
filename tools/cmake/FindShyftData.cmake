if(NOT SHYFT_DATA_VERSION)
  set(SHYFT_DATA_VERSION 1.3)
endif()

if(NOT SHYFT_DATA_FIND_PKG_URL)
  set(SHYFT_DATA_FIND_PKG_URL "https://gitlab.com/api/v4/projects/11841415/packages/generic/shyft-data")
endif()

if(DEFINED ENV{SHYFT_DATA})
  set(SHYFT_DATA "$ENV{SHYFT_DATA}")
endif()

if(DEFINED SHYFT_DATA)
  set(SHYFT_DATA_DIR_SPEC "SOURCE_DIR;${SHYFT_DATA}")
endif()

include(FetchContent)
FetchContent_Declare(
  shyft_data_pkg
  URL ${SHYFT_DATA_FIND_PKG_URL}/${SHYFT_DATA_VERSION}/shyft-data.tar.gz
  URL_HASH SHA256=d99c76013d3913205de569a33e3f3c4dd305851cacecabfeec1be38acc769eb3
  ${SHYFT_DATA_DIR_SPEC}
  DOWNLOAD_EXTRACT_TIMESTAMP true)

FetchContent_MakeAvailable(shyft_data_pkg)

set(SHYFT_DATA_DIR ${shyft_data_pkg_SOURCE_DIR})
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ShyftData
        FOUND_VAR ShyftData_FOUND
        REQUIRED_VARS SHYFT_DATA_DIR SHYFT_DATA_VERSION SHYFT_DATA_FIND_PKG_URL
        VERSION_VAR SHYFT_DATA_VERSION)
