find_library(CBLAS_LIBRARY
  NAMES cblas)

if(${CBLAS_LIBRARY} STREQUAL "CBLAS_LIBRARY-NOTFOUND")
  unset(CBLAS_LIBRARY)
else()
  add_library(cblas SHARED IMPORTED GLOBAL)
  set_target_properties(cblas PROPERTIES IMPORTED_LOCATION ${CBLAS_LIBRARY})
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(cblas
  FOUND_VAR CBLAS_FOUND
  REQUIRED_VARS CBLAS_LIBRARY)

