find_library(LAPACK_LIBRARY
  NAMES lapack
  REQUIRED)

if(${LAPACK_LIBRARY} STREQUAL "LAPACK-NOTFOUND")
  unset(LAPACK_LIBRARY)
else()
  add_library(lapack SHARED IMPORTED GLOBAL)
  set_target_properties(lapack PROPERTIES IMPORTED_LOCATION ${LAPACK_LIBRARY})
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LAPACK
  FOUND_VAR LAPACK_FOUND
  REQUIRED_VARS LAPACK_LIBRARY)
