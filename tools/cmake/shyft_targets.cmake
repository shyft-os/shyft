include_guard()

include(GenerateExportHeader)

set(SHYFT_GENERATED_SOURCEDIR "_shyft_generated_sources")
function(shyft_generate_source IN OUT)
  configure_file(${IN} ${SHYFT_GENERATED_SOURCEDIR}/${OUT})
endfunction()

function(shyft_generate_export_header)
  set(_opts)
  set(_single_opts TARGET BASE_NAME OUT)
  set(_multi_opts)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

  if (NOT DEFINED _args_BASE_NAME)
    string(TOUPPER ${_args_TARGET} _args_BASE_NAME)
  endif()
  generate_export_header(${_args_TARGET}
    BASE_NAME ${_args_BASE_NAME}
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}/${_args_OUT})
endfunction()

#TODO: compute a proper buildid - jeh
function(shyft_buildid TARGET BUILDID_HEAD_VARIABLE BUILDID_TAIL_VARIABLE)
  set(BUILDID_NAMESPACE "559ceca2-f227-484a-9c71-585744b89fc7")
  string(UUID BUILDID NAMESPACE ${BUILDID_NAMESPACE} NAME ${TARGET} TYPE SHA1)
  string(REGEX REPLACE "^(........)-(....)-(....)-(....)-(............)$" "\\1\\2\\3\\4\\5" BUILDID ${BUILDID})
  string(SUBSTRING ${BUILDID} 0 2 BUILDID_HEAD)
  string(SUBSTRING ${BUILDID} 2 -1 BUILDID_TAIL)

  set(${BUILDID_HEAD_VARIABLE} ${BUILDID_HEAD} PARENT_SCOPE)
  set(${BUILDID_TAIL_VARIABLE} ${BUILDID_TAIL} PARENT_SCOPE)
endfunction()

function(shyft_glob_sources)
  set(_single_opts TARGET RECURSE)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(RECURSE ${_args_RECURSE})
  if(RECURSE)
    file(GLOB_RECURSE sources CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
      ${_args_UNPARSED_ARGUMENTS})
  else()
    file(GLOB sources CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
      ${_args_UNPARSED_ARGUMENTS})
  endif()
  target_sources(${TARGET} PRIVATE ${sources})
endfunction()

function(shyft_configure_binary)
  set(_single_opts TARGET PATH TYPE COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(PATH ${_args_PATH})
  set(TYPE ${_args_TYPE})
  set(COMPONENT ${_args_COMPONENT})
  target_link_libraries(${TARGET}
    PRIVATE shyft-private_flags
    PUBLIC shyft-public_flags)

  if(SHYFT_WITH_DEBUG)
    #FIXME: assumes name of debug component! - jeh
    shyft_install_sourcedir(${COMPONENT}-debug SOURCEDIR)
    target_compile_options(${TARGET} PUBLIC
      "$<BUILD_INTERFACE:-ffile-prefix-map=${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}=${SOURCEDIR}>")
    target_compile_options(${TARGET} PUBLIC
      "$<BUILD_INTERFACE:-ffile-prefix-map=${CMAKE_CURRENT_SOURCE_DIR}=${SOURCEDIR}>")
    shyft_buildid(${TARGET} BUILDID_HEAD BUILDID_TAIL)
    target_link_options(${TARGET} PRIVATE
      "-Wl,--build-id=0x${BUILDID_HEAD}${BUILDID_TAIL}")
  endif()
endfunction()

function(shyft_configure_executable)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})

  set_target_properties(${TARGET} PROPERTIES VERSION ${SHYFT_VERSION})
  shyft_configure_binary(TARGET ${TARGET} COMPONENT ${COMPONENT} TYPE bin)

  target_include_directories(${TARGET} PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/cpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}>)
endfunction()

function(shyft_add_executable)
  set(_single_opts TARGET COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})

  add_executable(${TARGET} ${_args_UNPARSED_ARGUMENTS})
  shyft_configure_executable(TARGET ${TARGET} COMPONENT ${COMPONENT})
endfunction()

function(shyft_configure_library)
  set(_single_opts TARGET COMPONENT TYPE INCLUDE_COMPONENT PATH)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  set(TYPE ${_args_TYPE})
  set(INCLUDE_COMPONENT ${_args_INCLUDE_COMPONENT})
  set(PATH ${_args_PATH})


  shyft_configure_binary(TARGET ${TARGET} TYPE ${TYPE} COMPONENT ${COMPONENT}
    INCLUDE_COMPONENT ${INCLUDE_COMPONENT}
    PATH ${PATH})
  set_target_properties(${TARGET} PROPERTIES
    VERSION ${SHYFT_VERSION}
    SOVERSION ${SHYFT_VERSION_MAJOR})

  if(INCLUDE_COMPONENT)
    shyft_install_includedir(${INCLUDE_COMPONENT} INCLUDEDIR)
    target_include_directories(${TARGET} PUBLIC
      $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/cpp>
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}>
      $<INSTALL_INTERFACE:${INCLUDEDIR}>)
  else()
    target_include_directories(${TARGET} PUBLIC
      $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/cpp>
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${SHYFT_GENERATED_SOURCEDIR}>)
  endif()
endfunction()

function(shyft_add_library)
  set(_single_opts TARGET COMPONENT INCLUDE_COMPONENT)
  cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
  set(TARGET ${_args_TARGET})
  set(COMPONENT ${_args_COMPONENT})
  set(INCLUDE_COMPONENT ${_args_INCLUDE_COMPONENT})
  add_library(${TARGET} ${_args_UNPARSED_ARGUMENTS})
  shyft_configure_library(TARGET ${TARGET} COMPONENT ${COMPONENT} TYPE lib
    INCLUDE_COMPONENT ${INCLUDE_COMPONENT})
endfunction()

if(SHYFT_WITH_PYTHON)

  define_property(TARGET
    PROPERTY SHYFT_PYTHON_MODULE_PATH
    BRIEF_DOCS "Path of shyft python module")
  define_property(TARGET
    PROPERTY SHYFT_PYTHON_MODULE_DEPENDS
    BRIEF_DOCS "Python module targets depended on.")
  define_property(TARGET
    PROPERTY SHYFT_PYTHON_MODULE_IS_PARENT
    BRIEF_DOCS "Python module has submodules")
  define_property(TARGET
    PROPERTY SHYFT_PYTHON_MODULE_IS_COMPOSITE
    BRIEF_DOCS "Python module has extension-module internal submodules")

  if(SHYFT_WITH_PYTHON_STUBS)
    set(SHYFT_PYTHON_STUBS_DIR "${CMAKE_CURRENT_BINARY_DIR}/_python_stubs" CACHE INTERNAL "" FORCE)
  endif()
  set(SHYFT_PYTHON_MODULE_TARGETS "" CACHE INTERNAL "" FORCE)
  function(shyft_add_python_module)
    set(_single_opts TARGET COMPONENT NAME PATH IS_PARENT IS_COMPOSITE)
    set(_multi_opts DEPENDS)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    set(COMPONENT ${_args_COMPONENT})
    set(NAME ${_args_NAME})
    set(PATH ${_args_PATH})
    set(DEPENDS ${_args_DEPENDS})
    set(IS_PARENT ${_args_IS_PARENT})
    set(IS_COMPOSITE ${_args_IS_COMPOSITE})
    pybind11_add_module(${TARGET} ${_args_UNPARSED_ARGUMENTS})
    shyft_configure_library(TARGET ${TARGET} COMPONENT ${COMPONENT} PATH ${PATH} TYPE python)
    set_target_properties(${TARGET} PROPERTIES SHYFT_PYTHON_MODULE_PATH "${PATH}")
    set_target_properties(${TARGET} PROPERTIES OUTPUT_NAME ${NAME})
    set_target_properties(${TARGET} PROPERTIES SHYFT_PYTHON_MODULE_DEPENDS "${DEPENDS}")
    set_target_properties(${TARGET} PROPERTIES SHYFT_PYTHON_MODULE_IS_PARENT "${IS_PARENT}")
    set_target_properties(${TARGET} PROPERTIES SHYFT_PYTHON_MODULE_IS_COMPOSITE "${IS_COMPOSITE}")
    list(APPEND SHYFT_PYTHON_MODULE_TARGETS ${TARGET})

    set(SHYFT_PYTHON_MODULE_TARGETS ${SHYFT_PYTHON_MODULE_TARGETS} CACHE INTERNAL "" FORCE)
    if(SHYFT_WITH_PYTHON_STUBS)
      add_custom_command(TARGET ${TARGET} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E make_directory "${SHYFT_PYTHON_STUBS_DIR}/shyft/${PATH}"
        COMMAND ${CMAKE_COMMAND} -E create_symlink "$<TARGET_FILE_DIR:${TARGET}>/$<TARGET_FILE_NAME:${TARGET}>" "${SHYFT_PYTHON_STUBS_DIR}/shyft/${PATH}/$<TARGET_FILE_NAME:${TARGET}>")
    endif()

  endfunction()

endif()

if(BUILD_TESTING)

  function(shyft_add_test)
    set(_single_opts TARGET)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
    set(TARGET ${_args_TARGET})
    shyft_add_executable(TARGET ${TARGET} COMPONENT tests ${_args_UNPARSED_ARGUMENTS})

    target_include_directories(${TARGET} PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/cpp/test>)
    target_include_directories(${TARGET} PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/tools/shop_gen>)
    target_include_directories(${TARGET} PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/tools/version_forge>)
    target_link_libraries(${TARGET} PRIVATE doctest::doctest)
    target_compile_definitions(${TARGET} PRIVATE SHYFT_TESTING)
    doctest_discover_tests(${TARGET}
      PROPERTIES ENVIRONMENT "SHYFT_DATA=${SHYFT_DATA_DIR}" LABELS cpp ADD_LABELS 0)

  endfunction()

  if(SHYFT_WITH_PYTHON)
    function(shyft_add_python_tests)
      set(_opts)
      set(_single_opts DIRECTORY)
      cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})
      set(DIRECTORY ${_args_DIRECTORY})

      file(GLOB_RECURSE scripts RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY} test_*.py)
      foreach(script ${scripts})
        cmake_path(REMOVE_EXTENSION script OUTPUT_VARIABLE TARGET)
        set(TARGET "python/${TARGET}")
        add_test(
          NAME ${TARGET}
          COMMAND ${Python3_EXECUTABLE} -m pytest ${script}
          WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY})
        set_tests_properties(${TARGET}
          PROPERTIES
          SKIP_RETURN_CODE 5
          ENVIRONMENT "LD_LIBRARY_PATH=${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR};PYTHONPATH=${CMAKE_INSTALL_PREFIX}/${SHYFT_INSTALL_PYDIR_PREFIX};SHYFT_DATA=${SHYFT_DATA_DIR}"
          LABELS python)
      endforeach()

      if(SHYFT_WITH_COVERAGE)
        add_custom_target(
          shyft-coverage-python
          COMMAND ${CMAKE_COMMAND} -E env SHYFT_DATA=$ENV{SHYFT_DATA} LD_LIBRARY_PATH=${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR} PYTHONPATH=${CMAKE_INSTALL_PREFIX}/${SHYFT_INSTALL_PYDIR_PREFIX} ${Python3_EXECUTABLE} -m pytest --cov=shyft --cov-report term --cov-report xml:${SHYFT_COVERAGE_DIR}/shyft-python-cobertura.xml .
          WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${DIRECTORY})
      endif()
    endfunction()
  endif()

endif()

if(SHYFT_WITH_DEBUG)
  include(CMakeFindBinUtils)
  if(NOT CMAKE_OBJCOPY)
    message(FATAL_ERROR "'objcopy' not found")
  endif()
  if(NOT CMAKE_STRIP)
    message(FATAL_ERROR "'strip' not found")
  endif()
  function(shyft_add_debug_symbols)
    set(_opts)
    set(_single_opts TARGET)
    cmake_parse_arguments(_args "${_opts}" "${_single_opts}" "${_multi_opts}" ${ARGN})

    set(TARGET ${_args_TARGET})
    set(TARGET_FILE "$<TARGET_FILE:${TARGET}>")
    shyft_buildid(${TARGET} BUILDID_HEAD BUILDID_TAIL)
    set(SYMBOLS_FILE "${BUILDID_TAIL}.debug")
    add_custom_command(TARGET ${TARGET} POST_BUILD
      COMMAND ${CMAKE_OBJCOPY} --only-keep-debug ${TARGET_FILE} ${SYMBOLS_FILE}
      COMMAND ${CMAKE_STRIP} ${TARGET_FILE}
      BYPRODUCTS ${SYMBOLS_FILE})
  endfunction()
endif()
