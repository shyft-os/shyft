if(NOT SHOP_FIND_VERSION)
  set(SHOP_FIND_VERSION 16.6.1)
endif()

if(NOT SHOP_FIND_PKG_TOKEN OR NOT SHOP_FIND_PKG_URL)
  message(FATAL_ERROR
    "Variables SHOP_FIND_PKG_TOKEN and SHOP_FIND_PKG_URL must be provided."
    "Using gitlab generic package registry as source the format of these should be like:\n"
    "SHOP_FIND_PKG_TOKEN=PRIVATE-TOKEN:token-goes-here-nospace\n"
    "SHOP_FIND_PKG_URL=https://gitlab.com/api/v4/projects/projectid-goes-here/packages/generic\n"
    "and we expect packages license,cplex,shop to be available for the requested versions. For reference:\n"
    "https://docs.gitlab.com/ee/user/packages/generic_packages"
  )
endif()

include(FetchContent)
FetchContent_Declare(
  shop_license_pkg
  URL ${SHOP_FIND_PKG_URL}/license/1.0/license.tar.gz
  HTTP_HEADER ${SHOP_FIND_PKG_TOKEN}
  DOWNLOAD_EXTRACT_TIMESTAMP true)
FetchContent_Declare(
  shop_pkg
  URL ${SHOP_FIND_PKG_URL}/shop/${SHOP_FIND_VERSION}/shop.tar.gz
  HTTP_HEADER ${SHOP_FIND_PKG_TOKEN}
  DOWNLOAD_EXTRACT_TIMESTAMP true)
FetchContent_MakeAvailable(shop_license_pkg shop_pkg)

set(SHOP_LICENSE_DIR ${shop_license_pkg_SOURCE_DIR})
set(SHOP_LICENSE_FILE ${SHOP_LICENSE_DIR}/SHOP_license.dat)

cmake_host_system_information(RESULT OS_NAME QUERY OS_NAME)
string(TOLOWER ${OS_NAME} OS_NAME)

add_library(shop STATIC IMPORTED GLOBAL)
target_include_directories(shop INTERFACE $<BUILD_INTERFACE:${shop_pkg_SOURCE_DIR}/include>)
set(SHOP_LIBRARY_DIR ${shop_pkg_SOURCE_DIR}/lib/${OS_NAME}/release)
find_library(SHOP_LIBRARY
  NAMES ${CMAKE_STATIC_LIBRARY_PREFIX}shop${CMAKE_STATIC_LIBRARY_SUFFIX}
  PATHS ${SHOP_LIBRARY_DIR}
  REQUIRED)
set_target_properties(shop PROPERTIES IMPORTED_LOCATION ${SHOP_LIBRARY})

set(SHOP_CPLEX_NAME shop_cplex_interface${CMAKE_SHARED_LIBRARY_SUFFIX})
add_library(shop_cplex SHARED IMPORTED GLOBAL)
find_library(SHOP_CPLEX_LIBRARY
  NAMES ${SHOP_CPLEX_NAME}
  PATHS ${SHOP_LIBRARY_DIR}
  REQUIRED)
set_target_properties(shop_cplex PROPERTIES IMPORTED_LOCATION ${SHOP_CPLEX_LIBRARY})
target_link_libraries(shop_cplex INTERFACE ${CMAKE_DL_LIBS})

find_program(PATCHELF NAMES patchelf REQUIRED)
execute_process(COMMAND ${PATCHELF} --set-rpath \$ORIGIN ${SHOP_CPLEX_LIBRARY}
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SHOP
  FOUND_VAR SHOP_FOUND
  REQUIRED_VARS SHOP_LIBRARY_DIR SHOP_LIBRARY SHOP_CPLEX_LIBRARY SHOP_CPLEX_NAME SHOP_LICENSE_DIR SHOP_LICENSE_FILE
  VERSION_VAR SHOP_FILE)
