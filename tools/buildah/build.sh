#!/usr/bin/env bash
set -e
#set -x

run_as_ci() {
  buildah run --user ci --env 'CMAKE*' ${build} -- "$@"
}

run_as_root() {
  buildah run ${build} -- "$@"
}

clean_mount() {
  rm -rf ${build_mount}/"$1"/*
}

setup_sudo_nopwd_for_wheel() {
  printf '%%wheel ALL=(ALL) NOPASSWD:ALL' >> ${build_mount}/etc/sudoers
}

setup_ci_user_with_wheel_group() {
  # normally useradd -G wheel -p ci ci
  # but this does not work on fedora-minimal
  # so the manual steps try to mimic this for fedora/archlinux
  printf 'ci:x:1000:1000:user for ci tasks,,,:/home/ci:/bin/bash' >> ${build_mount}/etc/passwd
  printf 'wheel:x:1000:ci' >> ${build_mount}/etc/group
  printf 'ci:ci:19805:0:99999:7:::' >> ${build_mount}/etc/shadow
  run_as_root install -m 0700 -g wheel -o ci -d /home/ci
}

setup_container_config() {
  buildah config \
        --author "sigbjorn.helset@gmail.com" \
        --env LDFLAGS="-fuse-ld=mold" \
        ${build}
}

setup_container_std_user_ci() {
  buildah config \
          --user ci \
          ${build}
}

build_image() {
  src=$(readlink --canonicalize --no-newline `dirname ${0}`)

  distro_name=$1
  dest_image=$2

  if [[ ! -f $src/${distro_name}/image ]]; then
      echo "Error: unexpected distribution - ${distro_name}"
      (exit 1)
  fi

  # common pre-steps, required before distro specific parts
  src_image=$(cat ${src}/${distro_name}/image)
  echo "Building ${distro_name} from  ${src_image} to ${dest_image}"

  build=$(buildah from "${src_image}")
  build_mount=$(buildah mount ${build})
  setup_sudo_nopwd_for_wheel
  setup_ci_user_with_wheel_group
  setup_container_config
  # distro specific part
  cp -r ${src}/${distro_name} ${build_mount}/tmp/${distro_name}
  if [[ -f $src/${distro_name}/init.sh ]]; then
      run_as_root bash /tmp/${distro_name}/init.sh
  fi
  run_as_ci bash /tmp/${distro_name}/setup.sh
  run_as_ci bash /tmp/${distro_name}/clean.sh
  # common post steps, cleanup and commit
  clean_mount "tmp"
  clean_mount "var/cache"
  clean_mount "var/log"
  setup_container_std_user_ci
  buildah commit ${build} "${dest_image}"
  buildah rm ${build}
}


# Main entry point
if [ $# -ne 2 ]; then
  echo "usage: " "$0" "distro_name" "dest-image"
  echo "example:" "$0" "archlinux  archlinux-build:latest"
  echo "example:" "$0" "fedora   fedora-build:latest"
  echo " for local execution, prefix it with buildah unshare."
  (exit 1)
else
  build_image "$1" "$2"
fi

