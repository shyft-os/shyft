#!/bin/bash
set -o errexit
set -o pipefail
# ensure to keep aur packages so we can retrieve them later
# for the final target image
# rocksdb builds slow, so we need this to speedup builds
CMAKE_BUILD_PARALLEL_LEVEL="$(nproc)"
export CMAKE_BUILD_PARALLEL_LEVEL
src=$(readlink --canonicalize --no-newline $(dirname "${0}"))
tmpdir=$(mktemp -d)
pkgdir="${src}/built_packages"
sudo rm -rf "${pkgdir}" && sudo mkdir "${pkgdir}"

aur_pkgs="python-setuptools-git-versioning python-sphinxcontrib-mermaid  python-xyzservices python-bokeh armadillo dlib"
pushd "${tmpdir}"

for pkg in ${aur_pkgs}; do
  echo "build ${pkg}"
  wget -qO - "https://aur.archlinux.org/cgit/aur.git/snapshot/${pkg}.tar.gz" | tar -xvz
  pushd "${pkg}"
  makepkg -cf
  sudo pacman --noconfirm -U ${pkg}-*.pkg.tar.*
  sudo mv ${pkg}-*.pkg.tar.* "${pkgdir}"
  popd
  rm -rf "${pkg}"
done

# rocksdb as provided with patch (that sometime needs maintenance on updates)
cp -r "${src}/custom_packages/rocksdb" "${tmpdir}/"
pushd rocksdb
makepkg -cf
sudo pacman --noconfirm -U rocksdb*.pkg.tar.*
sudo mv rocksdb*.pkg.tar.* "${pkgdir}"
popd

# cmake pacman support from John Eivind R. Helset helps us to automate pacman packaging
curl https://gitlab.com/jehelset/cpack-makepkg/-/package_files/22406673/download > cpack-makepkg.pkg.tar.zst
sudo pacman -U cpack-makepkg.pkg.tar.zst --noconfirm --needed
popd

rm -rf "${tmpdir}"
# Note: We need this when building/test the angular api/demo apps
echo "installing @angular/cli at system level"
echo n | sudo npm install -g --silent @angular/cli

