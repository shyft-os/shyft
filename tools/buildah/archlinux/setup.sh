#!/usr/bin/env bash
set -e

src=$(readlink --canonicalize --no-newline `dirname ${0}`)
sudo pacman --noconfirm --needed -Suy $(cat ${src}/packages)
bash $src/custom_packages.sh
