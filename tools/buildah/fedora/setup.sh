#!/usr/bin/env bash
set -e

src=$(readlink --canonicalize --no-newline `dirname ${0}`)
sudo dnf --nodocs --setopt=install_weak_deps=False install -y $(cat ${src}/packages)
bash $src/custom_packages.sh
