#!/bin/bash
set -e
#set -o errexit
#set -o pipefail
f_version=$(cat /etc/os-release|grep VERSION_ID|cut -d= -f2)
if (( ${f_version} < 40)); then
  src=$(readlink --canonicalize --no-newline `dirname ${0}`)
  pkgdir=$src/built_packages
  sudo rm -rf $pkgdir && sudo mkdir $pkgdir

  tmpdir=$(mktemp -d)
  pushd ${tmpdir}

  cp -r ${src}/custom_packages/rocksdb .

  pushd rocksdb
  spectool -g rocksdb.spec
  fedpkg prep
  sudo dnf -y builddep rocksdb.spec
  export $(grep VERSION_ID= /etc/os-release)
  fedpkg --release f$VERSION_ID local
  sudo dnf install -y x86_64/rocksdb-*.rpm
  sudo mv x86_64/rocksdb-*.rpm $pkgdir
  popd

  rm -r $tmpdir
else
 echo "F40 can use distro rocksdb-devel"
 sudo dnf install -y rocksdb-devel
fi;