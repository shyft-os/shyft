#!/usr/bin/env bash
set -e

if [[ ! -f /usr/bin/dnf ]]; then
    ln -s /usr/bin/microdnf /usr/bin/dnf
fi

dnf --nodocs --setopt=install_weak_deps=False install -y sudo
