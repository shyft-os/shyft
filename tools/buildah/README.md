# shyft buildah scripts

This folder contains recipes to build linux images used in shyft ci.  

An image recipe is defined in subfolder with at least the following files:
```
distro/
  image (the url of the base-image)
  init.sh (optional bash script to prepare the image as root for building shyft)
  setup.sh (a bash script to prepare the image for building shyft)
  clean.sh (a bash script to clean up the image after building)
```

Images are built via the [build.sh](./build.sh) script which takes the distro-name
as well as the name of the image to be built.  

First the image in the distro/image file is configured with a user with sudo access
called ci. Then the distro folder is copied to /tmp inside the container before
init.sh runs as root, and setup.sh and clean.sh as the ci user.  

Any package that is built in this process is expected to be moved to
/tmp/distro/built_packages/ to prep for reuse when building runtime images.

## Maintenance and test of the build images
Given you have configured rootless podman/buildah environment on dev-pc,
then  to verify changes to the build system images, you can with do so with:

```bash
buildah unshare ./build.sh archlinux  archlinux-build:next
```

After building new image locally, you can test it using it for building the current
version of Shyft.

Example scripts to give an idea, or starting point, given you have 
a directory ~/projects/bbox, where you locate the 

`arch-bbox` script like

```bash
#!/bin/bash
distro=archlinux
ccache_dir=$(pwd)/${distro}-ccache
if [[ ! -e ${ccache_dir} ]]; then
    mkdir -p "$ccache_dir"
fi
podman run -it --rm --network=host --name ${distro}-dev --userns=keep-id \
       --workdir /projects/shyft \
       --env CCACHE_DIR=/projects/ccache \
       -v ${ccache_dir}:/projects/ccache:Z \
       -v $(pwd)/shyft:/projects/shyft:Z \
       archlinux-build:next /bin/bash
```
and then cd to that directory, git clone shyft into it and then
use execute `arch-bbox` to jump into the container and test building and testing shyft with the new container

Similar approach for other distros, and ofcourse, make adaption to your own
preferences regarding mapping of directories, environment variables as needed.
