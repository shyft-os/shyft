import logging

from shyft.energy_market.stm import StmTaskServer, DStmServer, StmServer
from shyft.hydrology import DrmServer, StateServer, ParameterServer, GeoCellDataServer, GoalFunctionServer
from shyft.time_series import DtsServer

from hydrology.service import dtss, gcd, drms, parameter, state, task, dstm, stm, goal_function
from hydrology.task.fx import register_drms_node
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import get_service_arguments
from hydrology.service.utils import run_until_exit
from hydrology.service.setup_logging import setup_logging

class HydroServices:

    def __init__(self, cfg: HydroServiceConfig):
        self.dtss: DtsServer = dtss.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)
        self.gcd_server: GeoCellDataServer = gcd.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)
        self.drms_server: DrmServer = drms.start_service(interface=cfg.listen_interface, base_port=cfg.base_port,drms_port=cfg.drms_port)
        self.parameter_server: ParameterServer = parameter.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)
        self.state_server: StateServer = state.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)
        self.task_server: StmTaskServer = task.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)
        self.dstm_server: DStmServer = dstm.start_service(interface=cfg.listen_interface, base_port=cfg.base_port)
        self.stm_server: StmServer = stm.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)
        self.goal_function_server: GoalFunctionServer = goal_function.start_service(interface=cfg.listen_interface, base_port=cfg.base_port, srv_root=cfg.srv_root)

    def __del__(self):
        # destruct in dependency order
        self.goal_function_server.stop_server()
        self.drms_server.stop_server()
        self.task_server.stop_server()
        self.state_server.stop_server()
        self.parameter_server.stop_server()
        self.dstm_server.stop_server()
        self.stm_server.stop_server()
        self.gcd_server.stop_server()
        self.dtss.stop_server()  # must be the last one.


def start_services(cfg: HydroServiceConfig) -> HydroServices:
    return HydroServices(cfg)

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    setup_logging()
    a = get_service_arguments(description="Start all backend services, using args, or defaults in environment variables")
    # drms_hosts = a.drms_hosts.split(":")
    print(f"Task scaling with drms node auto register (1 node for demo)")
    cfg = HydroServiceConfig(srv_root=a.root_dir, host=a.host, base_port=a.base_port,
                             listen_interface=a.interface,
                             drms_port=a.drms_port)
    s = start_services(cfg)
    tc = cfg.task.client()

    self_host_port = f'{cfg.drms.client().host_port}'


    def self_register() -> bool:
        try:
            return register_drms_node(tc, self_host_port)
        except Exception as e:
            logger.error(f'Failed to register {self_host_port} on Task service {tc.host_port} :Exception {e}')
        return False


    run_until_exit(hart_beat_fx=self_register)
    #run_until_exit()
    print("Closing down")
    del tc
    del s
    print("All services closed")
