import json
from dataclasses import asdict
from shyft.time_series import time
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import get_service_arguments
from hydrology.task.run import RunModelArgs, RunResult

if __name__ == "__main__":
    arg = get_service_arguments(description="Create dummy data for testing purposes.")
    cfg = HydroServiceConfig(listen_interface=arg.interface, base_port=arg.base_port, srv_root=arg.root_dir, host=arg.host)
    from shyft.hydrology import pt_gs_k
    try:
        # (0) some assumptions about the test db data as provided by the fill_dummy_data script
        #     e.g. key-value style lookup on state and parameter
        rm_type_name = "PT_GS_K"
        t0 = time('2010-01-01T00:00:00Z')
        # Now, with the initial content in place,  our first use case:
        # (1) find a task (template)
        task_client = cfg.task.client()
        model_name = 'nea'
        run_task_name = f'{model_name}-run'
        a_nea = [t for t in task_client.get_model_infos(mids=[]) if t.name == run_task_name]
        assert len(a_nea) > 0, f'Did not fined task with name {run_task_name}'
        a = a_nea[0]
        # (2) find model type specific parameter and state to run with
        parameter_model_name = f'{model_name}-{pt_gs_k.PTGSKParameter.__name__}'
        param_models = [pm for pm in cfg.parameter.client().get_model_infos(mids=[]) if pm.name == parameter_model_name]
        assert len(param_models) > 0 , f'Did not find parameter model with name {parameter_model_name}'
        pm_id = param_models[0].id
        # (3) find the state, that is also model type specific
        state_model_name = f'{model_name}-{pt_gs_k.PTGSKState.__name__}'
        state_models = [ sm for sm in cfg.state.client().get_model_infos(mids=[]) if sm.name ==state_model_name]
        assert len(state_models)>0, f'Did not find state model with name {state_model_name}'
        sm_id = state_models[0].id
        # (3) invoke an action on the task to create a new run (with results stored)
        print(f'Task {run_task_name}, task-id={a.id} with param-id={pm_id} state-id={sm_id}')
        run_args = RunModelArgs(rm_type=rm_type_name,
                                t0=int(t0), dt=3600 , n=365*24,
                                parameter_id=pm_id,
                                state_id=sm_id,
                                geo_db='nea_senorge',
                                geo_vars=['temperature', 'precipitation', 'radiation', 'wind_speed', 'rel_hum'],
                                ensembles=[0],
                                dtss_container='hydrology',
                                save_results=[RunResult(variable='discharge', indexes=[], url_suffix=''),
                                              RunResult(variable='charge', indexes=[], url_suffix=''),
                                              RunResult(variable='snow_swe', indexes=[], url_suffix=''),
                                              RunResult(variable='snow_sca', indexes=[], url_suffix=''),
                                              # notice we can take any ts, including input, as they where projected to the catchment
                                              RunResult(variable='temperature', indexes=[], url_suffix=''),
                                              RunResult(variable='precipitation', indexes=[], url_suffix=''),
                                              RunResult(variable='radiation', indexes=[], url_suffix=''),
                                              RunResult(variable='wind_speed', indexes=[], url_suffix=''),
                                              RunResult(variable='rel_hum', indexes=[], url_suffix='')
                                              ]
                                )
        fx_arg = asdict(run_args)
        fx_arg['action'] = 'run'
        ra = task_client.fx(mid=a.id, fx_arg=json.dumps(fx_arg))
        if ra:
            r_cases = task_client.read_model(mid=a.id).cases
            for r_case in r_cases:
                print(f'r_case:{r_case.id} {r_case.created}: {r_case.name}: {r_case.labels=}, {r_case.json=}')
        else:
            print('The task failed to run')

    finally:
        # del svc
        print('Done')
