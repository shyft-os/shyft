from hydrology.service.config import HydroServiceConfig
from hydrology.task.run import RunModelArgs, RunResult
from hydrology.task.calibrate import CalibrateModelArgs
from shyft.time_series import time
from hydrology.service.setup_logging import setup_logging

setup_logging()
def test_run_calib_args():
    r = RunModelArgs(rm_type="PT_GS_K",
                     t0=int(time('2010-01-01T00:00:00Z')), dt=3600, n=365 * 24,
                     parameter_id=1,
                     state_id=1,
                     geo_db='nea_senorge',
                     geo_vars=['temperature', 'precipitation', 'radiation', 'wind_speed', 'rel_hum'],
                     ensembles=[0],
                     dtss_container='hydrology',
                     save_results=[RunResult(variable='discharge', indexes=[], url_suffix=''),
                                   RunResult(variable='charge', indexes=[], url_suffix=''),
                                   RunResult(variable='snow_swe', indexes=[], url_suffix=''),
                                   RunResult(variable='snow_sca', indexes=[], url_suffix=''),
                                   # notice we can take any ts, including input, as they were projected to the catchment
                                   RunResult(variable='temperature', indexes=[], url_suffix=''),
                                   RunResult(variable='precipitation', indexes=[], url_suffix=''),
                                   RunResult(variable='radiation', indexes=[], url_suffix=''),
                                   RunResult(variable='wind_speed', indexes=[], url_suffix=''),
                                   RunResult(variable='rel_hum', indexes=[], url_suffix='y', url_prefix='x')
                                   ]
                     )
    s = r.to_json()
    r2 = RunModelArgs.from_json(s)
    assert r2 == r
    r2.dt = r2.dt + 10
    assert r2 != r
    c = CalibrateModelArgs(run_args=r, goal_function_id=1, optimizer=2, max_iterations=4)
    s = c.to_json()
    c2 = CalibrateModelArgs.from_json(s)
    assert c == c2
    c2.goal_function_id = 3
    assert c != c2


def test_drms_node():
    """ minimal test for drms node management skeleton """
    cfg = HydroServiceConfig()
    drms_host_port_fix = cfg.ensure_drms_host_port("localhost")
    assert f'localhost:{cfg._api_port(relative_port=4)}' == drms_host_port_fix

    n0 = len(cfg.drms_hosts)
    if n0 > 0:  # get rid of initial
        cfg.unregister_drms_node(cfg.drms_hosts[0])
    n1 = "localhost:10000"
    cfg.register_drms_node(n1)
    assert n1 in cfg.drms_hosts
    assert cfg.drms_q
    n2 = "localhost:10001"
    cfg.register_drms_node(n2)
    assert n2 in cfg.drms_hosts
    assert len(cfg.drms_q) == 2
    cfg.register_drms_node(n1)  # should not change anything
    assert len(cfg.drms_hosts) == 2
    cfg.unregister_drms_node(n1)
    assert n1 not in cfg.drms_hosts
    assert len(cfg.drms_q) == 1
    assert n2 in cfg.drms_hosts
    my_n2 = cfg.drms_q.popleft()
    cfg.unregister_drms_node(n2)  # not in the list, but put into the to be removed
    assert n2 in cfg.drms_hosts_to_remove
    cfg.release_drms_client(my_n2)
    assert n2 not in cfg.drms_hosts_to_remove
    assert len(cfg.drms_q) == 0


if __name__ == '__main__':
    test_run_calib_args()
