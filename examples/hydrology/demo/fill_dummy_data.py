import json
from typing import Any, Protocol
from shyft.energy_market.stm import StmTask, StmModelRef
from shyft.hydrology import (GeoCellDataClient, GeoCellData, GeoCellDataVector, GeoCellDataModel, GoalFunctionClient,
                             LandTypeFractions, ParameterClient, ParameterModel, StateModel, StateClient,
                             GoalFunctionModel, TargetSpecificationVector, TargetSpecificationPts, CELL_CHARGE,
                             KLING_GUPTA,
                             CatchmentPropertyType, TargetSpecCalcType)
from shyft.time_series import (time, TimeAxis, TimeSeries, StringVector, ModelInfo, DtsClient, GeoGridSpec,
                               GeoTimeSeriesConfiguration, GeoSlice, UtcTimeVector, POINT_AVERAGE_VALUE, GeoPoint,
                               GeoPointVector, GeoTimeSeriesConfigurationVector,
                               TsVector, shyft_url, IntVector)

from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import get_service_arguments


class ParameterType(Protocol):
    @property
    def map_t(self) -> Any: ...

    def __init__(self) -> None: ...

    @property
    def __name__(self) -> str: ...


class StateType(Protocol):
    @property
    def vector_t(self) -> Any: ...

    def __init__(self) -> None: ...

    @property
    def __name__(self) -> str: ...


class RegionModelSkeleton(Protocol):
    @property
    def parameter_t(self) -> ParameterType: ...

    @property
    def state_t(self) -> StateType: ...

    def __init__(self, gcd: GeoCellDataVector, param: ParameterType) -> None: ...

    @property
    def __name__(self) -> str: ...


def create_initial_content(cfg: HydroServiceConfig, model_type: RegionModelSkeleton) -> int:
    """
    create the initial content for our testing.

    To create and run a shyft.hydrology region model we need:

    - geo cell data model , preferabely a TIN (geo cell data service),
    - parameter_map model (parameter_map service)
    - state at t0 (initial state) t0, is like 2012-01-01T00:00:00Z
    - dtss geo ts database with forcing data for the region model, for the run period

    To orchestrate the run we need:
    - a task with base model reference (e.g. the geo cell data model)

    :param cfg: the services configurations that we can use to create clients.
    :return: the task id for testing
    """
    t0: time = time("2010-01-01T00:00:00Z")
    dt: time = time(3600 * 24)
    n_points: int = 365 * 10  # 10 years, daily data.
    fc_dt: time = dt*n_points # length of the forecast/history we stash into db
    nea_catchment_id: int = 1
    nea_name: str = 'nea'

    def _create_geo_grid(n: int) -> GeoGridSpec:
        points = GeoPointVector(
            [GeoPoint(x=x * 1000, y=y * 1000, z=x * 10 + y * 10 + 100) for x in range(n) for y in range(n)])
        return GeoGridSpec(epsg=32633, points=points)

    def _create_nea_model(c: GeoCellDataClient, n: int) -> int:
        def mk_geo_cell_data_example(n: int) -> GeoCellDataVector:
            """ n x n squares, z="""
            gs = _create_geo_grid(n)
            return GeoCellDataVector([
                GeoCellData(  # describe personality of the cell.
                    # TIN vertexes
                    p1=GeoPoint(x=p.x, y=p.y, z=p.z),
                    p2=GeoPoint(x=p.x + 1000, y=p.y, z=p.z + 100),
                    p3=GeoPoint(x=p.x + 1000, y=p.y + 1000, z=p.z + 50),
                    # the epsg of the tin coordinates
                    epsg_id=gs.epsg,  # utm 33n
                    # catchment grouping id
                    catchment_id=nea_catchment_id,
                    # and the landtypes/fractions
                    land_type_fractions=LandTypeFractions(
                        glacier=0.0,
                        lake=0.1,
                        reservoir=0.2,
                        forest=0.3,
                        unspecified=0.4
                    )
                ) for p in gs.points])

        mis = c.get_model_infos(mids=[])
        nea = [m for m in mis if m.name == f'{nea_name}']
        if not nea:
            m = GeoCellDataModel()
            m.geo_cell_data = mk_geo_cell_data_example(n)
            return c.store_model(m, ModelInfo(id=0, name=f'{nea_name}', created=time.now(), json='{}'))
        return nea[0].id  # pick first.

    def _create_nea_geo_ts(c: DtsClient, n: int) -> str:
        geos: GeoTimeSeriesConfigurationVector = c.get_geo_db_ts_info()
        gdb: GeoTimeSeriesConfiguration | None = None
        for geo in geos:
            if geo.name == nea_name:
                gdb = geo
        variables = StringVector(['temperature', 'precipitation', 'radiation', 'wind_speed', 'rel_hum'])
        if gdb is None:
            gs: GeoGridSpec = _create_geo_grid(n)
            gdb = GeoTimeSeriesConfiguration(
                prefix='shyft://',
                name=f'{nea_name}_senorge',
                description='{"source":"senorge"}',
                grid=gs,
                t0_times=UtcTimeVector([]),
                dt=fc_dt,  # the max length of forecast!
                n_ensembles=1,
                variables=variables)
            c.add_geo_ts_db(gdb)
        # fill with data
        if len(gdb.t0_times) == 0:
            g_slice = GeoSlice(
                v=[i for i in range(len(gdb.variables))],
                e=[i for i in range(gdb.n_ensembles)],
                g=[i for i in range(len(gdb.grid.points))],
                t=UtcTimeVector([t0]),
                ts_dt=fc_dt
            )
            tsm = gdb.create_ts_matrix(g_slice)
            ta = TimeAxis(t0, dt, n_points)
            print(f"Generating data for ta {ta}")
            fv = {'temperature': 20.0, 'precipitation': 3.2, 'rel_hum': 0.8, 'radiation': 300, 'wind_speed': 2.0}
            for t in range(tsm.shape.n_t0):
                for v in range(tsm.shape.n_v):
                    fill_value = fv[variables[v]]
                    for e in range(tsm.shape.n_e):
                        for g in range(tsm.shape.n_g):
                            fc_ts = TimeSeries(ta, fill_value=fill_value, point_fx=POINT_AVERAGE_VALUE)
                            tsm.set_ts(t=t, v=v, e=e, g=g, ts=fc_ts)
            print(f"Storing")
            c.geo_store(geo_ts_db_name=gdb.name, tsm=tsm, replace=True, cache=True)
            print(f"Done with {gdb.name}")
        return nea_name

    def _create_nea_observed_discharg(c: DtsClient, gcd_id: int) -> TimeSeries:
        discharge = TimeSeries(shyft_url("hydrology", f"catchment/{gcd_id}/discharge"),
                               TimeSeries(TimeAxis(t0, dt, n_points), 1.8, POINT_AVERAGE_VALUE)
                               )
        c.store_ts(TsVector([discharge]))
        return discharge

    def _create_nea_goal_function(c: GoalFunctionClient, discharge: TimeSeries) -> int:
        mis = c.get_model_infos(mids=[])
        nea = [m for m in mis if m.name == nea_name]
        if not nea:
            m = GoalFunctionModel()
            cids = IntVector([nea_catchment_id])
            m.goal_functions = TargetSpecificationVector(
                [
                    TargetSpecificationPts(
                        discharge,
                        cids,
                        0.7,
                        TargetSpecCalcType.KLING_GUPTA, 1.0, 1.0, 1.0,
                        CatchmentPropertyType.DISCHARGE,
                        f'{discharge.ts_id()}'
                    )
                ]
            )
            return c.store_model(m, ModelInfo(id=0, name=nea_name, created=time.now(), json='{}'))
        return nea[0].id

    def _create_nea_parameter(c: ParameterClient, param_type: ParameterType) -> int:
        mis = c.get_model_infos(mids=[])
        param_name = f'{nea_name}-{param_type.__name__}'
        nea = [m for m in mis if m.name == param_name]
        if not nea:
            def mk_parameter_data_example(n: int = 10) -> param_type.map_t:
                m = param_type.map_t()
                m[0] = param_type()
                return m

            m = ParameterModel()
            m.parameters = mk_parameter_data_example()
            return c.store_model(m, ModelInfo(id=0, name=param_name, created=time.now(), json='{}'))
        return nea[0].id

    def _create_nea_calibration_parameter_range(c: ParameterClient, param_type: ParameterType) -> int:
        mis = c.get_model_infos(mids=[])
        calib_name = f'{nea_name}-range-{param_type.__name__}'
        nea = [m for m in mis if m.name == calib_name]
        if not nea:
            def mk_parameter_range_example(n: int = 10) -> param_type.map_t:
                p_min = param_type()
                p_max = param_type()
                # just give some slack
                p_min.kirchner.c1 = 0.7 * p_min.kirchner.c1
                p_min.kirchner.c2 = 0.7 * p_min.kirchner.c2
                p_min.p_corr.scale_factor = 0.5 * p_min.p_corr.scale_factor
                p_max.p_corr.scale_factor = 10.0 * p_max.p_corr.scale_factor
                p_max.kirchner.c1 = 1.7 * p_max.kirchner.c1
                p_max.kirchner.c2 = 1.7 * p_max.kirchner.c2
                m = param_type.map_t()
                m[0] = p_min
                m[1] = p_max
                return m

            m = ParameterModel()
            m.parameters = mk_parameter_range_example()
            return c.store_model(m, ModelInfo(id=0, name=calib_name, created=time.now(), json='{}'))
        return nea[0].id

    def _create_nea_state(c: StateClient, gcd_c: GeoCellDataClient, nea_gcd_id: int,
                          region_model_type: RegionModelSkeleton) -> int:
        mis = c.get_model_infos(mids=[])
        # check if it is already there.
        state_name = f'{nea_name}-{region_model_type.state_t.__name__}'
        nea = None
        for m in mis:
            if not m.name == state_name:
                continue
        if not nea:
            gcd = gcd_c.read_model(mid=nea_gcd_id)
            rm = region_model_type(gcd.geo_cell_data, region_model_type.parameter_t())  # any parameter will do.
            s0 = rm.state.extract_state([])  # this is how to get all states (with unique cellid) from model
            for s in s0:
                s.state.kirchner.q = 20.0
            sm = StateModel()
            sm.states = s0
            smi = ModelInfo(id=0, name=state_name, created=time.now(), json=f'{{"t":"{t0}"}}')
            return c.store_model(sm, smi)
        else:
            return nea.id

    # Now check if content is already there(skip), or if missing, create and store it.
    # create what we need for the fundamental region model, e.g. gcd, parameter, state, dtss forcing data
    gcd_client = cfg.gcd.client()
    parameter_client = cfg.parameter.client()
    state_client = cfg.state.client()
    dts_client = cfg.dtss.client()
    containers = dts_client.get_container_names()
    gf_client = cfg.goal_function.client()
    if 'hydrology' not in containers:
        print('Creating the "hydrology" ts container for results')
        dts_client.set_container(name='hydrology', relative_path='hydrology', container_type='ts_rdb')
    n_cells: int = 2
    nea_id = _create_nea_model(gcd_client, n=n_cells)
    _nea_state = _create_nea_state(state_client, gcd_client, nea_id,model_type)
    _nea_parameter = _create_nea_parameter(parameter_client,model_type.parameter_t)
    _nea_range = _create_nea_calibration_parameter_range(parameter_client,model_type.parameter_t)
    _nea_geo_name = _create_nea_geo_ts(c=dts_client, n=n_cells)
    discharge_ts = _create_nea_observed_discharg(dts_client, nea_id)
    _nea_gf_id = _create_nea_goal_function(gf_client, discharge_ts)
    print(f'Goal fx id={_nea_gf_id}')
    # Then check/create the task needed to further orchestrate and work with the above region model(s)
    task_client = cfg.task.client()
    task_mis = task_client.get_model_infos(mids=[])
    run_task_name = f'{nea_name}-run'
    nea_task = [task for task in task_mis if task.name == run_task_name]
    if not nea_task:
        nea_ref = StmModelRef(host=cfg.gcd.cfg.host, port_num=cfg.gcd.cfg.port, api_port_num=0, model_key=str(nea_id))
        a = StmTask(id=0, name=run_task_name, created=time.now(), json="{}", labels=['base'], base_model=nea_ref,
                    task_name=f'dstm {run_task_name}')
        t_id = task_client.store_model(a, ModelInfo(id=0, name=run_task_name, created=time.now(), json='{}'))
    else:
        t_id = nea_task[0].id
    # also create calibration task
    calibrate_task_name = f'{nea_name}-calibration'
    nea_task_calibration = [task for task in task_mis if task.name == f'{nea_name}-calibration']
    if not nea_task_calibration:
        nea_ref = StmModelRef(host=cfg.gcd.cfg.host, port_num=cfg.gcd.cfg.port, api_port_num=0, model_key=str(nea_id))
        a = StmTask(id=0, name=calibrate_task_name, created=time.now(),
                    json="{}",
                    labels=['base', 'calibration'],
                    base_model=nea_ref,
                    task_name= f'{nea_name} calibration')
        c_id = task_client.store_model(a, ModelInfo(id=0, name=calibrate_task_name, created=time.now(), json='{}'))
    else:
        c_id = nea_task_calibration[0].id
    return t_id, c_id


if __name__ == "__main__":
    from shyft.hydrology import pt_gs_k

    print("Notice that services must be up and running prior to starting this script..")
    a = get_service_arguments(description="Create dummy data for testing purposes.")
    cfg = HydroServiceConfig(listen_interface=a.interface, base_port=a.base_port, srv_root=a.root_dir, host=a.host)
    t_id, c_id = create_initial_content(cfg=cfg, model_type=pt_gs_k.PTGSKModel)
    print(f'Done, got task id for run={t_id} and calibration = {c_id}')
