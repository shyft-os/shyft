import logging
from logging import config
import time
import atexit
from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from threading import Thread, Lock
import os

# Define the path to the logging configuration file (current working directory)
def get_logging_root_path()->Path:
    if 'SHYFT_LOGGING_CONFIG_DIR' in os.environ:
        p=Path(os.environ['SHYFT_LOGGING_CONFIG_DIR'])
        if p.exists() and p.is_dir():
            return p
    return Path(__file__).parent

LOGGING_CONFIG_FILE = get_logging_root_path() / 'logging_config.ini'


class ConfigFileChangeHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if Path(event.src_path) == LOGGING_CONFIG_FILE:
            logging.config.fileConfig(LOGGING_CONFIG_FILE)
            logging.info("Re-loaded logging configuration from file")


# Global reference to the observer and the monitor_thread
observer = None
monitor_thread = None
monitor_thread_running = True
logging_initialized = False
logging_lock = Lock()

def setup_logging():
    global observer, monitor_thread, monitor_thread_running, logging_initialized

    with logging_lock:
        if logging_initialized:
            return

        # Load initial logging configuration
        logging.config.fileConfig(LOGGING_CONFIG_FILE)

        # Set up a watchdog observer to monitor the config file for changes
        event_handler = ConfigFileChangeHandler()
        observer = Observer()
        observer.schedule(event_handler, path=LOGGING_CONFIG_FILE.parent, recursive=False)
        observer.start()

        def monitor_config():
            global monitor_thread_running
            try:
                while monitor_thread_running:
                    time.sleep(1)
            except KeyboardInterrupt:
                pass
            observer.stop()
            observer.join()

        monitor_thread = Thread(target=monitor_config, daemon=True)
        monitor_thread.start()

        # Register the cleanup function to run at program termination
        atexit.register(shutdown_logging_monitor)

        logging_initialized = True


def shutdown_logging_monitor():
    global monitor_thread_running
    if observer:
        monitor_thread_running = False
        observer.stop()
        observer.join()

