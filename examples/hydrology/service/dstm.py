from shyft.energy_market.stm import DStmServer
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import run_until_exit, get_service_arguments
from hydrology.service.setup_logging import setup_logging


def start_service(*, interface: str, base_port: int) -> DStmServer:
    c = HydroServiceConfig(listen_interface=interface, base_port=base_port)
    s = c.dstm.start()
    c.dtss.cfg.validate_client_port_or_raise("DSTM: Needs working dtss port")
    s.set_master_slave_mode(ip=c.dtss.cfg.host, port=c.dtss.cfg.port, master_poll_time=0.2, unsubscribe_threshold=10,
                            unsubscribe_max_delay=10.0)
    return s


if __name__ == "__main__":
    setup_logging()
    a = get_service_arguments(
        description="DSTM service, provides in energy-market model server with DTSS engine/backend")
    s = start_service(interface=a.interface, base_port=a.base_port)
    try:
        run_until_exit()
    finally:
        s.stop_server()
        del s
