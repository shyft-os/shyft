from typing import List
from pathlib import Path
from shyft.energy_market.stm import StmTaskServer
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import run_until_exit, get_service_arguments
from hydrology.task.run import fx_run_model
from hydrology.task.calibrate import fx_calibrate_model
from hydrology.task.fx import TaskCallback
from hydrology.service.setup_logging import setup_logging


def start_service(*, interface: str, base_port: int, srv_root: Path) -> StmTaskServer:
    c = HydroServiceConfig(listen_interface=interface, base_port=base_port, srv_root=srv_root)
    callback = TaskCallback(c)
    # register actions here, run, calibrate etc.
    callback.fx_table['run'] = fx_run_model
    callback.fx_table['calibrate'] = fx_calibrate_model
    task_server = c.task.start()
    task_server.fx = callback   # When client or web invokes fx, it is directed to our dispatch
    return task_server


if __name__ == "__main__":
    setup_logging()
    a = get_service_arguments(description="Task service, provides live orchestration and persisted tasks states")
    s = start_service(interface=a.interface, base_port=a.base_port, srv_root=a.root_dir)
    try:
        run_until_exit()
    finally:
        s.stop_server()
        del s
