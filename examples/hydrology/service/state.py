from pathlib import Path
from shyft.hydrology import StateServer
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import run_until_exit, get_service_arguments
from hydrology.service.setup_logging import setup_logging


def start_service(*, interface: str, base_port: int, srv_root: Path) -> StateServer:
    c = HydroServiceConfig(listen_interface=interface, base_port=base_port, srv_root=srv_root)
    return c.state.start()


if __name__ == "__main__":
    setup_logging()
    a = get_service_arguments(description="Hydrology region model state service, provides persisted states")
    s = start_service(interface=a.interface, base_port=a.base_port, srv_root=a.root_dir)
    try:
        run_until_exit()
    finally:
        s.stop_server()
        del s
