from time import sleep
from shyft.hydrology import DrmServer
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import run_until_exit, get_service_arguments
from hydrology.task.fx import register_drms_node, unregister_drms_node
from shyft.time_series import time
from hydrology.service.setup_logging import setup_logging

def start_service(*, interface: str, base_port: int, drms_port: int = 0) -> DrmServer:
    c = HydroServiceConfig(listen_interface=interface, base_port=base_port, drms_port=drms_port)
    return c.drms.start()


if __name__ == "__main__":
    setup_logging()
    a = get_service_arguments(description="DRMS service compute component")
    s = start_service(interface=a.interface, base_port=a.base_port)
    cfg = HydroServiceConfig(srv_root=a.root_dir,
                             host=a.host,
                             base_port=a.base_port,
                             listen_interface=a.interface,
                             drms_port=a.drms_port)
    self_host = a.interface if a.interface != '0.0.0.0' else a.host
    self_host_port = f'{self_host}:{s.get_listening_port()}'
    tc = cfg.task.client()  # alloc it here, once, and ensure to kill at end
    print(f'Drms compute node {self_host_port}, starting up')


    def self_register() -> bool:
        try:
            return register_drms_node(tc, self_host_port)
        except Exception as e:
            print(f'Failed to register {self_host_port} on Task service {tc.host_port} :Exception {e}')
        return False


    initial_register_time = time.now() + time(10.0)
    initial_register = False
    while time.now() < initial_register_time:
        initial_register = self_register()
        if initial_register:
            print("Successfully registered")
            break
        sleep(0.1)

    try:
        run_until_exit(hart_beat_fx=self_register)
    finally:
        try:
            print(f'Unregistering {self_host_port}')
            unregister_drms_node(tc, self_host_port)
        except:  # ignore any error
            pass
        del tc
        s.stop_server()
        del s
