from pathlib import Path
from shyft.time_series import DtsServer
from hydrology.service.config import HydroServiceConfig
from hydrology.service.utils import run_until_exit, get_service_arguments
from hydrology.service.setup_logging import setup_logging


def start_service(*, interface: str, base_port: int, srv_root: Path) -> DtsServer:
    c = HydroServiceConfig(listen_interface=interface, base_port=base_port, srv_root=srv_root)
    return c.dtss.start()


if __name__ == "__main__":
    setup_logging()
    a = get_service_arguments(description="DTSS, The time-series service")
    s = start_service(interface=a.interface, base_port=a.base_port, srv_root=a.root_dir)
    try:
        run_until_exit()
    finally:
        s.stop_server()
        del s
