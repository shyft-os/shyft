import signal
from time import sleep
import argparse
from typing import Callable


class Exit:
    """
    A small helper-class to raise a flag if SIGTERM is received
    """
    now: bool = False  # signal handler sets this to True, and poll loop above terminates in controlled fashion

    def __init__(self):
        signal.signal(signal.SIGTERM, self.exit)
        signal.signal(signal.SIGINT, self.exit)

    @staticmethod
    def exit(signum, frame):
        Exit.now = True


def run_until_exit(sleep_dt: float = 5.0, hart_beat: bool = True, hart_beat_fx:Callable[[],bool]|None=None) -> None:
    ex = Exit()
    s: float = 0.0
    mini_sleep: float = 0.04
    c: int = 0
    while True:
        sleep(mini_sleep)
        if ex.now:
            break
        s += mini_sleep
        if s >= sleep_dt:
            s = 0.0
            if hart_beat:
                if hart_beat_fx:
                    hart_beat_fx()  # ignore bool result for now
                c += 1
                if c == 20:
                    c = 0
                    print('.', flush=True)
                else:
                    print('.', end='', flush=True)


def get_service_arguments(description: str) -> argparse.Namespace:
    a = argparse.ArgumentParser(description=description)
    a.add_argument('--base-port', type=int, default=0, help="Port to listen to or connect to, default 0 to auto port")
    a.add_argument('--interface', type=str, default="",
                   help="network interface, 0.0.0.0 means any, 127.0.0.1 is localhost")
    a.add_argument('--host', type=str, default="",
                   help="network address used to contact shyft services")
    a.add_argument('--task-host', type=str, default="localhost",
                   help="network address used to contact task, needed for drms nodes to register their service")
    a.add_argument('--root-dir', type=str, default="")
    a.add_argument('--drms-port', type=int, default=0, help="Specify explicit drms node port to use")

    a_result=a.parse_args()
    return a_result
