from dataclasses import dataclass
from pathlib import Path
from typing import List
from collections import deque
from time import sleep
from shyft.hydrology import (DrmServer, ParameterServer, StateServer, GeoCellDataServer)
from shyft.hydrology import (DrmClient, ParameterClient, StateClient, GeoCellDataClient)
from shyft.hydrology import (GoalFunctionClient, GoalFunctionServer)
from shyft.energy_market.stm import (StmTaskServer, StmTaskClient, DStmServer, DStmClient, StmServer, StmClient)
from shyft.time_series import (DtsServer, DtsClient, DtssCfg,time)
from os import environ
from tempfile import gettempdir
import logging

logger = logging.getLogger(__name__)

def _default_base_port(port: int | None) -> int:
    if port is None or int(port) == 0:
        return int(environ.get("SHYFT_SERVICE_BASE_PORT", "20000"))
    return port


def _default_srv_root(srv_root: str | Path | None) -> Path:
    if srv_root:
        p = Path(srv_root)
    else:
        p = Path(environ.get("SHYFT_SERVICE_ROOT", str(Path(gettempdir()) / "shyft_root")))
    if not p.exists():
        p.mkdir(exist_ok=True, parents=True)
    elif not p.is_dir():
        raise RuntimeError(f'{p} is not a directory')
    return p


def _default_shyft_host(host: str | None) -> str:
    if host:
        return host
    return environ.get("SHYFT_SERVICE_HOST", "localhost")


def _default_drms_hosts(hosts: List[str] | None) -> List[str]:
    if not hosts:
        return environ.get("SHYFT_DRMS_HOSTS", "localhost").split(':')
    return hosts

def _default_shyft_listen_interface(listen_interface: str | None) -> str:
    if listen_interface:
        return listen_interface
    return environ.get("SHYFT_SERVICE_INTERFACE", "127.0.0.1")


def _default_dtss_cache_target(n: int) -> int:
    if n is None or n < 5000000:
        return int(environ.get("SHYFT_DTSS_CACHE", "5000000"))
    return n


@dataclass
class MicroServiceConfig:
    host: str = 'localhost'  # client side: Where we can find the microservice
    port: int = 0  # client&server side: 0 auto port, >0 auto port of the microservice
    interface: str = '127.0.0.1'  # server side: listening config for the server side, 127.0.0.1 is the local only interface, suitable for test/experiments
    web_port: int = -1  # client&server side:-1 do not start, 0 auto port, >0 fixed port
    db: Path = None  # server db path
    web: Path = None  # server www path

    def validate_client_port_or_raise(self, info: str) -> None:
        if self.port == 0:
            raise RuntimeError(f'{info}: not configured, is server started ?')

    def ensure_db_or_raise(self, info: str) -> None:
        if not self.db.exists():
            self.db.mkdir(exist_ok=True, parents=True)
        elif not self.db.is_dir():
            raise RuntimeError(f'{info}:{self.db} is not a directory')

    def start_server(self,
                     srv: GoalFunctionServer | DrmServer | StmTaskServer | DtsServer | ParameterServer | StateServer | GeoCellDataServer | DStmServer | StmServer) -> None:
        srv.set_listening_ip(self.interface)
        srv.set_listening_port(self.port)
        port = self.port
        self.port = srv.start_server()
        logger.info(f'Started {srv.__class__.__name__} @port {port}->{self.port} listen at {self.interface}, db->{self.db}')


def _set_cache_config(s: DStmServer | DtsServer, *, cache_size_in_bytes: int, initial_ts_points_estimate: int) -> None:
    s.cache_memory_target = cache_size_in_bytes
    s.cache_ts_initial_size_estimate = initial_ts_points_estimate * 8


@dataclass
class DtssMicroService:
    cfg: MicroServiceConfig

    def start(self) -> DtsServer:
        s = DtsServer()
        s.set_can_remove(True)
        mb = 1024 * 1024
        gb = 1024 * mb
        _set_cache_config(s, cache_size_in_bytes=_default_dtss_cache_target(10 * gb),
                          initial_ts_points_estimate=365 * 10)
        self.cfg.ensure_db_or_raise("Dtss")
        s.set_container(name='', root_dir=str(self.cfg.db))
        s.default_geo_db_type = 'ts_rdb'
        s.default_geo_db_config = DtssCfg(ppf=24 * 30,
                                          compress=True,
                                          max_file_size=100 * mb,
                                          write_buffer_size=4 * mb,
                                          log_level=200, test_mode=0,
                                          ix_cache=100 * mb)
        s.configuration_file = str(
            self.cfg.db.parent / "dtss.containers.cfg")  # to preserve containers during cold starts
        self.cfg.start_server(s)
        return s

    def client(self) -> DtsClient:
        self.cfg.validate_client_port_or_raise("DtsClient")
        return DtsClient(f'{self.cfg.host}:{self.cfg.port}')


@dataclass
class DStmMicroService:
    cfg: MicroServiceConfig

    def start(self) -> DStmServer:
        s = DStmServer()
        mb = 1024 * 1024
        gb = 1024 * mb
        _set_cache_config(s, cache_size_in_bytes=_default_dtss_cache_target(10 * gb),
                          initial_ts_points_estimate=365 * 10)
        self.cfg.start_server(s)
        return s

    def client(self) -> DStmClient:
        self.cfg.validate_client_port_or_raise("DStmClient")
        return DStmClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class StmMicroService:
    cfg: MicroServiceConfig

    def start(self) -> StmServer:
        s = StmServer(str(self.cfg.db))
        self.cfg.start_server(s)
        return s

    def client(self) -> StmClient:
        self.cfg.validate_client_port_or_raise("StmClient")
        return StmClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class ParameterMicroService:
    cfg: MicroServiceConfig

    def start(self) -> ParameterServer:
        self.cfg.ensure_db_or_raise("ParameterServer")
        s = ParameterServer(str(self.cfg.db))
        self.cfg.start_server(s)
        return s

    def client(self) -> ParameterClient:
        self.cfg.validate_client_port_or_raise("ParameterClient")
        return ParameterClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class StateMicroService:
    cfg: MicroServiceConfig

    def start(self) -> StateServer:
        self.cfg.ensure_db_or_raise("StateServer")
        s = StateServer(str(self.cfg.db))
        self.cfg.start_server(s)
        return s

    def client(self) -> StateClient:
        self.cfg.validate_client_port_or_raise("StateClient")
        return StateClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class GoalFunctionMicroService:
    cfg: MicroServiceConfig

    def start(self) -> GoalFunctionServer:
        self.cfg.ensure_db_or_raise("GoalFunctionServer")
        s = GoalFunctionServer(str(self.cfg.db))
        self.cfg.start_server(s)
        return s

    def client(self) -> GoalFunctionClient:
        self.cfg.validate_client_port_or_raise("GoalFunctionClient")
        return GoalFunctionClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class GeoCellDataMicroService:
    cfg: MicroServiceConfig

    def start(self) -> GeoCellDataServer:
        self.cfg.ensure_db_or_raise("GeoCellDataServer")
        s = GeoCellDataServer(str(self.cfg.db))
        self.cfg.start_server(s)
        return s

    def client(self) -> GeoCellDataClient:
        self.cfg.validate_client_port_or_raise("GeoCellDataClient")
        return GeoCellDataClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class DrmsMicroService:
    cfg: MicroServiceConfig

    def start(self) -> DrmServer:
        s = DrmServer()
        self.cfg.start_server(s)
        return s

    def client(self) -> DrmClient:
        self.cfg.validate_client_port_or_raise("DrmClient")
        return DrmClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


@dataclass
class TaskMicroService:
    cfg: MicroServiceConfig

    def start(self) -> StmTaskServer:
        self.cfg.ensure_db_or_raise("StmTaskServer")
        s = StmTaskServer(str(self.cfg.db))
        self.cfg.start_server(s)
        return s

    def client(self) -> StmTaskClient:
        self.cfg.validate_client_port_or_raise("StmTaskClient")
        return StmTaskClient(f'{self.cfg.host}:{self.cfg.port}', timeout_ms=1000)


class HydroServiceConfig:
    """
    A HydroServiceConfig provide service related configuration for dealing with a completely service driven
    setup of Shyft's Hydrology stack.

    """


    def __init__(self,*, srv_root: Path | str | None = None, host: str | None = None, base_port: int | None = None,
                 listen_interface: str | None = None, drms_port:int=0):
        """
        Construct a HydroServiceConfig with neatly defined client/server configurations, suitable
        for both testing and 'real world' client/server configurations.

        :param srv_rooot: the root directory for all the persistent stores, each starts with its own subdirectory(dtss etc)
        :param host: the host, or ip, for the (task) service
        :param base_port: the services ip ports are allocated in this range from base_port to 2 x n services
        :param listen_interface: For drms nodes, Use specific ip/host so that it correctly register to task, For other using 0.0.0.0 listens on all interfaces, default to localhost only,
        :param drms_port: For drms node operation, use specific port (allow multiple drms for each ip)
        """

        self.base_port: int = _default_base_port(base_port)
        self.srv_root: Path = _default_srv_root(srv_root)
        self.listen_interface: str = _default_shyft_listen_interface(listen_interface)
        self.host: str = _default_shyft_host(host)
        self.drms_port:int=drms_port
        self.drms_hosts: List[str] = []
        self.drms_q: deque = deque()
        self.drms_hosts_to_remove: List[str] = []

    def ensure_drms_host_port(self, h:str)->str:
        if ':' in h:
            return h
        else:
            return f'{h}:{self._api_port(4)}'


    def _api_port(self, relative_port: int) -> int:
        return self.base_port + 2 * relative_port if self.base_port else 0

    def _web_port(self, relative_port: int) -> int:
        return self.base_port + 2 * relative_port + 1 if self.base_port else 0

    def _db_path(self, db: str) -> Path:
        return self.srv_root / db

    def _web_path(self, web: str) -> Path:
        return self.srv_root / web

    @property
    def dtss(self) -> DtssMicroService:
        return DtssMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(5), web_port=self._web_port(5),
            interface=self.listen_interface, db=self._db_path('dtss/db'))
        )

    @property
    def task(self) -> TaskMicroService:
        return TaskMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(0), web_port=self._web_port(0),
            interface=self.listen_interface, db=self._db_path('task/db'))
        )

    @property
    def gcd(self) -> GeoCellDataMicroService:
        return GeoCellDataMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(1), web_port=self._web_port(1),
            interface=self.listen_interface, db=self._db_path('gcd/db'))
        )

    @property
    def parameter(self) -> ParameterMicroService:
        return ParameterMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(2), web_port=self._web_port(2),
            interface=self.listen_interface, db=self._db_path('parameter/db'))
        )

    @property
    def state(self) -> StateMicroService:
        return StateMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(3), web_port=self._web_port(3),
            interface=self.listen_interface, db=self._db_path('state/db'))
        )

    @property
    def goal_function(self) -> GoalFunctionMicroService:
        return GoalFunctionMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(8), web_port=self._web_port(8),
            interface=self.listen_interface, db=self._db_path('goal_function/db'))
        )

    @property
    def drms(self) -> DrmsMicroService:
        return DrmsMicroService(MicroServiceConfig(
            host=self.host,
            port=self.drms_port if self.drms_port else self._api_port(4),
            web_port=self._web_port(4),  # note: no web yet for drms
            interface=self.listen_interface)
        )

    def register_drms_node(self, drms_host_port: str):
        drms_host_port=self.ensure_drms_host_port(drms_host_port)
        if drms_host_port not in self.drms_hosts:
            self.drms_hosts.append(drms_host_port)
            self.drms_q.append(DrmClient(host_port=drms_host_port, timeout_ms=1000))

        #else:
        #    logger.info(f"Attempt to add {drms_host_port} twice, ignored")
        #    maybe just register it as a ping drms_ping[drms_host_port]=time.now()

    def unregister_drms_node(self, drms_host_port: str):
        if drms_host_port in self.drms_hosts: # do we have it at all?
            self.drms_hosts.remove(drms_host_port)
            got_it: bool = False
            for i in range(len(self.drms_q)):  # try prune element out of the deque
                try:
                    e = self.drms_q.popleft()
                    if e.host_port == drms_host_port:
                        got_it = True
                        try:
                            e.close()
                        finally:
                            del e
                        break
                    else:
                        self.drms_q.append(e)
                finally:
                    pass
            if not got_it:  # it may be in use, so remember to kill it when returned
                self.drms_hosts_to_remove.append(drms_host_port)
        else:
            logger.warning(f"Attempt to remove unknown drms'{drms_host_port}', ignored")

    def acquire_drms_client(self,*, info: str = "",
                            sleep_dt: float = 1.0,
                            report_dt: float = 10.0) -> DrmClient:
        info = info or "acquire_drms_client"
        t_report: time = time.now() + report_dt
        drms_client: DrmClient = None
        while drms_client is None:
            while not self.drms_q:
                if time.now() > t_report:
                    t_report = time.now() + report_dt
                    logger.info(f"Waiting for drms client: {info}")
                sleep(sleep_dt)
            try:
                drms_client = self.drms_q.popleft()
            except IndexError:  # another thread won, just continue to wait.
                continue
            try:
                if drms_client.host_port in self.drms_hosts_to_remove:
                    self.drms_hosts_to_remove.remove(drms_client.host_port)
                    try:
                        drms_client.close()
                    finally:
                        del drms_client
                    drms_client = None
                    continue
                v = drms_client.get_server_version()  # just a health check
            except Exception as e:
                logger.error(
                    f'For info= {info}: Failed to connect to drms client {drms_client.host_port}, exception:{e}, will retry in 1s')
                self.release_drms_client(drms_client)  # stash it back to queue, maybe ok next time
                drms_client = None
                sleep(sleep_dt)  # extra sleep for error, avoid spin on error
        return drms_client

    def release_drms_client(self, c: DrmClient) -> None:
        if c and isinstance(c, DrmClient):
            if c.host_port in self.drms_hosts_to_remove:
                self.drms_hosts_to_remove.remove(c.host_port)
                try:
                    c.close()
                finally:
                    del c
            else:
                self.drms_q.append(c)

    @property
    def dstm(self) -> DStmMicroService:
        return DStmMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(6), web_port=self._web_port(6),
            interface=self.listen_interface
        )
        )

    @property
    def stm(self) -> StmMicroService:
        return StmMicroService(MicroServiceConfig(
            host=self.host,
            port=self._api_port(7), web_port=self._web_port(7),
            interface=self.listen_interface, db=self._db_path('stm/db'))
        )
