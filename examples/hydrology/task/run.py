import json
from dataclasses import dataclass, field, asdict
from typing import List

from shyft.energy_market.stm import StmTaskClient, StmTask, StmCase, StmModelRef, ModelRefList
from shyft.hydrology import RegionModelType, ARegionEnvironment, InterpolationParameter, GeoPointVector, GeoPoint, \
    GeoCellDataModel, DrmClient
from shyft.time_series import (time, TimeAxis, TimeSeries, TsVector, IntVector, shyft_url, __version__ as shyft_version,
                               GeoTsMatrix, GeoEvalArgs, GeoQuery, GeoTimeSeriesConfiguration)

from hydrology.service.config import HydroServiceConfig
from hydrology.task.utils import get_geo_db
from hydrology.task.fx import create_unique_case_for_task
import logging

logger: logging.Logger = logging.getLogger(__name__)
@dataclass
class RunResult:
    variable: str = ""
    indexes: List[int] = field(default_factory=list)
    url_suffix: str = ""
    url_prefix: str = ""


# result extractor, list of:
#  property=discharge indexes=[..] url=task.case.run.discharge 1..n?


@dataclass
class RunModelArgs:
    rm_type: str = ""  # one of RegionModelType.names
    t0: int = 0
    dt: int = 0
    n: int = 0
    parameter_id: int = 0
    state_id: int = 0
    geo_db: str = ""
    geo_vars: List[str] = field(default_factory=list)
    ensembles: List[int] = field(default_factory=list)
    ip_max_members: int = 4
    dtss_container: str = "tmp"
    save_results: List[RunResult] = field(default_factory=list)

    @property
    def time_axis(self):
        return TimeAxis(time(self.t0), time(self.dt), self.n)

    @property
    def interpolation_parameter(self) -> InterpolationParameter:
        ip: InterpolationParameter = InterpolationParameter()
        ip.use_idw_for_temperature = True
        for idw in [ip.temperature_idw, ip.radiation, ip.precipitation, ip.wind_speed, ip.rel_hum]:
            idw.max_members = self.ip_max_members
            idw.max_distance = 1000 * 100
        return ip

    def to_json(self) -> str:
        return json.dumps(asdict(self))

    @staticmethod
    def from_json(s: str) -> 'RunModelArgs':
        def fix_list(dct: dict) -> dict:
            if 'save_results' in dct:
                r = list()
                for sr in dct['save_results']:
                    r.append(RunResult(**sr))
                dct['save_results'] = r
            return dct

        rd = json.loads(s, object_hook=fix_list)
        return RunModelArgs(**rd)


def create_run_eval_args(gdb: GeoTimeSeriesConfiguration, run_args: RunModelArgs, bbox: GeoPointVector) -> GeoEvalArgs:
    ix0 = gdb.t0_time_axis.index_of(run_args.t0)  # give back ix to t0
    if ix0 < 0 or ix0 >= len(gdb.t0_time_axis):
        logger.debug(f'Did not find suitable forecast {run_args.t0} in geo db {run_args.geo_db}')
    t0 = gdb.t0_time_axis.time(ix0)
    skip_dt = run_args.t0 - t0
    ta = TimeAxis(run_args.t0, run_args.dt, run_args.n)
    t0_time_axis = TimeAxis(t0, skip_dt + ta.total_period().timespan(), 1)  # just need to cover t0
    # MAP runargs.geo_vars to  gdb.variables indicies
    gdb_v = {v: i for i, v in enumerate(gdb.variables)}
    # verify
    for v in run_args.geo_vars:
        if not v in gdb_v:
            raise RuntimeError(f"Missing variable {v} , not found in geo db {run_args.geo_db} : {gdb.variables}")

    v_ix = [gdb_v[v] for v in run_args.geo_vars]
    # Check the bbox, that it returns at least one point
    geo_range = GeoQuery(gdb.grid.epsg, bbox)
    bbox_points = gdb.find_geo_match_ix(geo_range)
    if not bbox_points:
        raise RuntimeError(f"Bounding box of the model gcd does intersect with any points in the geo db {gdb.name}")
    # logger.debug(f'Run time-axis is {ta}')
    # logger.debug(f'T0 time-axis is {t0_time_axis}')
    return GeoEvalArgs(geo_ts_db_id=run_args.geo_db,
                       # variables=run_args.geo_vars, # we choose to use clip/average expression
                       ensembles=run_args.ensembles,
                       time_axis=t0_time_axis,
                       ts_dt=ta.total_period().timespan(),
                       geo_range=geo_range,
                       concat=False,
                       cc_dt0=skip_dt,
                       ts_expressions=TsVector(  # use expression to clip exactly what we want
                           [TimeSeries(f"{v_ix[i]}").average(ta) for i in range(len(v_ix))]
                       )
                       )


def extract_time_series_from_run(drms_client: DrmClient, run_args: RunModelArgs, drms_mid: str,
                                 prefix: str) -> TsVector:
    tsv = TsVector()
    for sr in run_args.save_results:
        get_method: str = f'get_{sr.variable}'
        if hasattr(drms_client, get_method):
            ts = getattr(drms_client, get_method)(mid=drms_mid, indexes=sr.indexes)  # f'{m.id}_{case_id}_{run_id}
            ts_url = shyft_url(run_args.dtss_container, f'{sr.url_prefix}{prefix}.{sr.variable}{sr.url_suffix}')
            tsv.append(TimeSeries(ts_url, ts))
            logger.info(f'Storing {ts_url} {ts.time_axis}')
        else:
            logger.error(f'Wrong variable name attempted on extract, "{sr.variable}"')
    return tsv


def fx_run_model(cfg: HydroServiceConfig, task_client: StmTaskClient, m: StmTask, fx_args: str) -> bool:
    """
    Run the model as specified by the base_model.model_key, the geo-cell-data id, that have the
    geo personality of the drms model
    Use the supplied args to select the time-axis, t0, state of t0, the stack-type, the parameters(of type)
    and the ref to dtss. geo database.
    Create a case, to stash the drms model, and start it.
    when it is done update the case with the finished result.
    Maybe extract discharge, and store it using the url-formula task-id, case-id, run-id .property(discharge ..)
    We can also consider the following:
      Create a stm_system, with hps/catchment, and decorate it.
      Then, use it for keeping the parameters, .. , and let the run decorate that model when done.
      In that case, the url will be more like task, stm_model-id, catchment-id and then properties on the catchment.
    """
    logger.debug(f"task.fx_run {fx_args}")
    run_args = RunModelArgs.from_json(fx_args)
    if run_args.rm_type not in RegionModelType.names:
        raise RuntimeError(f'RegionModelType {run_args.rm_type} is not supported')
    gcd_client = cfg.gcd.client()
    drms_client = cfg.acquire_drms_client(info=f'Task {m.name} run')
    if not drms_client:
        raise RuntimeError(f'All Drms clients {cfg.drms_hosts} are busy')
    logger.debug(f"drms_client: {drms_client.host_port}")
    parameter_client = cfg.parameter.client()
    state_client = cfg.state.client()
    dtss_client = cfg.dtss.client()
    drms_mid:str =""
    try:
        m_gcd = gcd_client.read_model(mid=int(m.base_model.model_key))
        bbox = m_gcd.geo_cell_data.polygon_buffer(distance=10 * 1000.0)
        logger.debug(f'Got gcd back {len(m_gcd.geo_cell_data)}')
        case_id = create_unique_case_for_task(task_client, task_id=m.id)
        run_id = 1  # always just one run
        #  STEP 1: create the drms model in drms server.
        drms_mid_tmp = f'T{m.id}_{case_id}_{run_id}'  # we name the drms model id like this, so unique
        rm_type = RegionModelType.names[run_args.rm_type]
        if not drms_client.create_model(mid=drms_mid_tmp, mtype=rm_type, gcd=m_gcd.geo_cell_data):
            logger.error(f'Failed to create drms-model {drms_mid_tmp}')
            return False
        drms_mid=drms_mid_tmp
        # STEP 1.1 update the task.case with the new 'run'
        this_run = StmModelRef(host=cfg.drms.cfg.host, port_num=cfg.drms.cfg.port, api_port_num=cfg.drms.cfg.web_port,
                               model_key=drms_mid)
        case_labels = [run_args.rm_type, run_args.geo_db,
                       shyft_version]  # add useful labels for search/presentation later
        run_case = StmCase(id=case_id, name=f'run_{case_id}', created=time.now(), json=run_args.to_json(),
                           labels=case_labels)
        task_client.update_case(mid=m.id, case=run_case)
        task_client.add_model_ref(mid=m.id, cid=case_id, mr=this_run)
        logger.debug(f'Successfully created drms-model {drms_mid}')
        # STEP 2. Set the parameter(s) for the region model
        pm = parameter_client.read_model(mid=run_args.parameter_id)
        # note this might fail, if there is mismatched rm type vs. parameter type,
        # so we could consider letting the parameters determine the (main) model type
        #  and then just use bool optimized_model to determine if it should be an _OPT or not.
        #  the advantage: will always succeed.
        #  - but the  state represent similar problem
        #  - so the state-id must return a state id set of correct type as well.
        for i in pm.parameters:
            if i.key() == 0:  # notice that id ==0 means the special region model param.
                drms_client.set_region_parameter(drms_mid, i.data())
            else:  # a dedicated parameter for specific catchment id
                drms_client.set_catchment_parameter(drms_mid, i.data(), i.key())
        logger.debug(f'Successfully set parameters drms-model {drms_mid}:{pm.parameters}')
        # STEP 3. Set the state of the model
        smi = state_client.get_model_infos(mids=[run_args.state_id])[0]
        try:
            smi_info = json.loads(smi.json)  # we could check state_type and t0
            logger.debug(f'State provided for t= {smi_info["t"]}')
        except json.decoder.JSONDecodeError as je:
            smi_info = {}
            logger.error(f'Failed to decode json for model info {je.msg} at position {je.pos}')
            return False

        sm = state_client.read_model(mid=run_args.state_id)
        state_ok = drms_client.set_state(drms_mid, sm.states)
        # pdb.set_trace()
        logger.debug(f'Successfully {state_ok=} set state drms-model {drms_mid}: {smi.name} -> {smi_info}')
        # STEP 4. apply/interpolate forcing data.
        gdb = get_geo_db(dtss_client, run_args.geo_db)
        if not gdb:
            logger.error(f'Did not find geo db {run_args.geo_db}')
            return False
        # compute how to extract data from dtss/geo
        geo_args = create_run_eval_args(gdb, run_args, bbox)
        tx = time.now()
        tsm: GeoTsMatrix = dtss_client.geo_evaluate(eval_args=geo_args, use_cache=True, update_cache=True)
        logger.debug(f'dtss geo eval: used {time.now() - tx} s')

        # Step 4.1 create region_env from GeoTsMatrix.
        #
        #   t=0 should be correct since we ask precisely for one t0
        #   e=0 is currently ok, since we just run one ensemble/scenario
        #       FIXME: should we ever support ensemble runs, or just let that be in the outer loop?
        # NOTE: we NEED the tsm variables to be in order temperature,precip,rad,wind,relhum
        # otherwise we must specify the order in the v=
        # thus if run_args variables are specified in this order.. it just works!
        r_env = ARegionEnvironment.create_from_geo_ts_matrix(m=tsm, t=0, e=0, v=IntVector([]))
        logger.debug(f'Interpolation with forcing data points {len(r_env.temperature)}')
        tx = time.now()
        if not drms_client.run_interpolation(mid=drms_mid, ip_parameter=run_args.interpolation_parameter,
                                             ta=run_args.time_axis, r_env=r_env, best_effort=True):
            logger.error(f'Interpolation failed')
            return False
        if not drms_client.is_cell_env_ts_ok(mid=drms_mid):
            logger.error(f'Interpolation worked, but leaved cell environment with one or more nans')
            return False
        logger.debug(f'used {time.now() - tx} s')
        logger.debug(f'Run cells: ')
        tx = time.now()
        if not drms_client.run_cells(mid=drms_mid):
            logger.error(f'Run cells failed {drms_mid}')
            return False
        logger.debug(f'used {time.now() - tx} s')
        logger.debug(f'Extract and store results:')
        tx = time.now()
        tsv = extract_time_series_from_run(drms_client, run_args, drms_mid, f'{m.id}_{case_id}_{run_id}')
        if tsv:
            dtss_client.store_ts(tsv=tsv, overwrite_on_write=False, cache_on_write=True)
        logger.debug(f'used {time.now() - tx} s')
        drms_client.remove_model(mid=drms_mid)  # clean up the model since we are done
        logger.info(f'Done with run {drms_mid}')
        drms_mid=""

    finally:
        if drms_mid:
            drms_client.remove_model(mid=drms_mid)
        cfg.release_drms_client(drms_client)
        gcd_client.close()
        parameter_client.close()
        dtss_client.close()
        del gcd_client
        del parameter_client
        del dtss_client

    return True
