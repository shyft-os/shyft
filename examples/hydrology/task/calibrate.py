import json
from dataclasses import dataclass, field, asdict
from typing import List
from time import sleep
import logging


from shyft.energy_market.stm import StmTaskClient, StmTask, StmCase, StmModelRef, ModelRefList
from shyft.hydrology import RegionModelType, ARegionEnvironment, InterpolationParameter, GeoPointVector, GeoPoint, \
    GeoCellDataModel, DrmClient, CalibrationOption, ParameterModel
from shyft.time_series import (time, TimeAxis, TimeSeries, TsVector, IntVector, shyft_url, __version__ as shyft_version,
                               GeoTsMatrix, GeoEvalArgs, GeoQuery, GeoTimeSeriesConfiguration, ModelInfo)

from hydrology.service.config import HydroServiceConfig
from hydrology.task.utils import get_geo_db
from hydrology.task.fx import create_unique_case_for_task

from hydrology.task.run import RunModelArgs, create_run_eval_args, extract_time_series_from_run

logger:logging.Logger = logging.getLogger(__name__)

@dataclass
class CalibrateModelArgs:
    """

    """
    run_args: RunModelArgs = field(default_factory=lambda: RunModelArgs())
    goal_function_id: int = 0
    parameter_range_id: int = 0
    optimizer: int = 0
    max_iterations: int = 100
    max_seconds: int = 600
    p_name: str = ''
    p_json: str = ''
    tr_start: float = 0.05
    tr_stop: float = 1.0e-16

    @property
    def calibrate_option(self) -> CalibrationOption:
        if self.optimizer == 1:
            c = CalibrationOption.bobyqa(self.max_iterations, self.tr_start, self.tr_stop)
            logger.debug("Selected calibration option: bobyqa")
        elif self.optimizer == 2:
            c = CalibrationOption.dream(max_iterations=self.max_iterations)
            logger.debug("Selected calibration option: dream")
        elif self.optimizer == 3:
            c = CalibrationOption.sceua(max_iterations=self.max_iterations, x_epsilon=0.012, y_epsilon=0.022)
            logger.debug("Selected calibration option: sceua")
        else:
            c = CalibrationOption.global_search(
                max_iterations=self.max_iterations,
                time_limit=self.max_seconds,
                solver_epsilon=0.01
            )
            logger.debug("Selected calibration option: global search")
        return c

    def to_json(self) -> str:
        return json.dumps(asdict(self))

    @staticmethod
    def from_json(s: str) -> 'CalibrateModelArgs':
        r = json.loads(s)
        if 'run_args' in r:
            r['run_args'] = RunModelArgs.from_json(json.dumps(r['run_args']))

        return CalibrateModelArgs(**r)


def fx_calibrate_model(cfg: HydroServiceConfig, task_client: StmTaskClient, m: StmTask, fx_args: str) -> bool:
    cal_args = CalibrateModelArgs.from_json(fx_args)
    gf_client = cfg.goal_function.client()
    gf = gf_client.read_model(cal_args.goal_function_id)
    logger.debug(f"task.fx_calibrate {fx_args}")
    run_args = cal_args.run_args
    if run_args.rm_type not in RegionModelType.names:
        raise RuntimeError(f'RegionModelType {run_args.rm_type} is not supported')
    gcd_client = cfg.gcd.client()

    parameter_client = cfg.parameter.client()
    state_client = cfg.state.client()
    dtss_client = cfg.dtss.client()
    drms_client = cfg.acquire_drms_client(info=f'Task {m.name} calibrate')
    if not drms_client:
        raise RuntimeError(f'All Drms clients {cfg.drms_hosts} are busy')
    logger.debug(f"drms_client: {drms_client.host_port}")
    drms_mid: str = ""
    try:
        m_gcd = gcd_client.read_model(mid=int(m.base_model.model_key))
        bbox = m_gcd.geo_cell_data.polygon_buffer(distance=10 * 1000.0)
        logger.debug(f'Got gcd back {len(m_gcd.geo_cell_data)}')
        case_id = create_unique_case_for_task(task_client,task_id=m.id)
        run_id = 1  # always just one run
        #  STEP 1: create the drms model in drms server.
        drms_mid_tmp = f'T{m.id}_{case_id}_{run_id}'  # we name the drms model id like this, so unique
        rm_type = RegionModelType.names[run_args.rm_type]
        if not drms_client.create_model(mid=drms_mid_tmp, mtype=rm_type, gcd=m_gcd.geo_cell_data):
            logger.error(f'Failed to create drms-model {drms_mid_tmp}')
            return False
        drms_mid = drms_mid_tmp
        # STEP 1.1 update the task.case with the new 'run'
        this_run = StmModelRef(host=cfg.drms.cfg.host, port_num=cfg.drms.cfg.port, api_port_num=cfg.drms.cfg.web_port,
                               model_key=drms_mid)
        case_labels = [run_args.rm_type, run_args.geo_db, shyft_version]
        run_case = StmCase(id=case_id, name=f'calibrate_{case_id}', created=time.now(), json=run_args.to_json(),
                           labels=case_labels)
        task_client.update_case(mid=m.id, case=run_case)
        task_client.add_model_ref(mid=m.id,cid=case_id,mr=this_run)
        logger.debug(f'Successfully created drms-model {drms_mid}')
        # STEP 2. Set the parameter(s) for the region model
        pm = parameter_client.read_model(mid=run_args.parameter_id)
        # note this might fail, if there is mismatched rm type vs. parameter type,
        # so we could consider letting the parameters determine the (main) model type
        #  and then just use bool optimized_model to determine if it should be an _OPT or not.
        #  the advantage: will always succeed.
        #  - but the  state represent similar problem
        #  - so the state-id must return a state id set of correct type as well.
        region_parameter = None
        for i in pm.parameters:
            if i.key() == 0:  # notice that id ==0 means the special region model param.
                region_parameter = i.data()
                drms_client.set_region_parameter(drms_mid, i.data())
            else:  # a dedicated parameter for specific catchment id
                drms_client.set_catchment_parameter(drms_mid, i.data(), i.key())
        logger.debug(f'Successfully set parameters drms-model {drms_mid}:{pm.parameters}')
        # STEP 3. Set the state of the model
        smi = state_client.get_model_infos(mids=[run_args.state_id])[0]
        try:
            smi_info = json.loads(smi.json)  # we could check state_type and t0
        except json.decoder.JSONDecodeError as je:
            smi_info = {}
            logger.error(f'Failed to decode json for model info {je.message} at position {je.pos}')

        sm = state_client.read_model(mid=run_args.state_id)
        state_ok = drms_client.set_state(drms_mid, sm.states)
        # pdb.set_trace()
        logger.debug(f'Successfully {state_ok=} set state drms-model {drms_mid}: {smi.name} -> {smi_info}')
        # STEP 4. apply/interpolate forcing data.
        gdb = get_geo_db(dtss_client, run_args.geo_db)
        if not gdb:
            logger.error(f'Did not find geo db {run_args.geo_db}')
            return False
        # compute how to extract data from dtss/geo
        geo_args = create_run_eval_args(gdb, run_args, bbox)
        tx = time.now()
        tsm: GeoTsMatrix = dtss_client.geo_evaluate(eval_args=geo_args, use_cache=True, update_cache=True)
        logger.debug(f'dtss geo eval: used {time.now() - tx} s')

        # Step 4.1 create region_env from GeoTsMatrix.
        #
        #   t=0 should be correct since we ask precisely for one t0
        #   e=0 is currently ok, since we just run one ensemble/scenario
        r_env = ARegionEnvironment.create_from_geo_ts_matrix(m=tsm, t=0, e=0, v=IntVector([]))
        logger.debug(f'Interpolation with forcing data points {len(r_env.temperature)}')
        tx = time.now()
        if not drms_client.run_interpolation(mid=drms_mid, ip_parameter=run_args.interpolation_parameter,
                                             ta=run_args.time_axis, r_env=r_env, best_effort=True):
            logger.error(f'Interpolation failed')
            return False
        if not drms_client.is_cell_env_ts_ok(mid=drms_mid):
            logger.error(f'Interpolation worked, but leaved cell environment with one or more nans')
            return False
        logger.debug(f'used {time.now() - tx} s')
        logger.debug(f'Calibrate : ')

        cal_pm = parameter_client.read_model(mid=cal_args.parameter_range_id)
        p_min = None
        p_max = None
        for i in cal_pm.parameters:
            if i.key() == 0:
                p_min = i.data()
            elif i.key() == 1:
                p_max = i.data()
        if p_min is None or p_max is None:
            logger.error(f'Calibration parameter set id={cal_args.parameter_range_id}, missing key 0(p_min) or 1(p_max)')
            return False
        if not drms_client.start_calibration(
                mid=drms_mid,
                p_start=region_parameter,
                p_min=p_min,
                p_max=p_max,
                spec=gf.goal_functions,
                opt=cal_args.calibrate_option
        ):
            logger.error(f'Run cells failed {drms_mid}')
            return False
        # for now, poll the calibration, and wait until it ends
        tx = time.now()
        cal_result = drms_client.check_calibration(mid=drms_mid)
        last_i: int = 0
        t_exit = time.now() + cal_args.max_seconds
        while cal_result.running:
            sleep(2)
            cal_result = drms_client.check_calibration(mid=drms_mid)
            if last_i == cal_result.trace_size:
                logger.debug('*')
            else:
                for i in range(last_i, cal_result.trace_size):
                    logger.info(f'{i}-{drms_mid}:{cal_result.trace_goal_function_value(i)}: {cal_result.trace_parameter(i)}')
            last_i = cal_result.trace_size
            if time.now() > t_exit:
                logger.warning("Timeout")
                try:
                    drms_client.cancel_calibration(mid=drms_mid)
                finally:
                    drms_client.cancel_calibration(mid=drms_mid)

        logger.info(f'used {time.now() - tx} s')
        p_result = cal_result.result()
        logger.info(f'Result {drms_mid} is {p_result}')
        if p_result is None:
            logger.error("No result -> Failed calibration")
            return False
        # TODO stash result to the parameter service.
        cal_r = ParameterModel()
        cal_m = p_result.map_t()
        cal_m[0] = p_result
        cal_r.parameters = cal_m

        p_id = parameter_client.store_model(m=cal_r,
                                            mi=ModelInfo(
                                                id=0,
                                                name=cal_args.p_name,
                                                created=time.now(),
                                                json=cal_args.p_json  # add goal fx, value, plus task ref?
                                            )
                                            )
        logger.info(f'Stashed parameters to storage as "{cal_args.p_name}", parameter-id={p_id},\nExtract and store results:')
        tx = time.now()
        tsv = extract_time_series_from_run(drms_client, run_args, drms_mid, f'{m.id}_{case_id}_{run_id}')
        if tsv:
            dtss_client.store_ts(tsv=tsv, overwrite_on_write=False, cache_on_write=True)
        logger.debug(f'used {time.now() - tx} s')
        drms_client.remove_model(mid=drms_mid)  # clean up the model since we are done
        logger.debug(f'Done with calibration {drms_mid}')
        drms_mid=""
    finally:
        if drms_mid:
            drms_client.remove_model(mid=drms_mid)
        cfg.release_drms_client(drms_client)
        gcd_client.close()
        parameter_client.close()
        dtss_client.close()
        gf_client.close()
        del gcd_client
        del drms_client
        del parameter_client
        del dtss_client
        del gf_client

    return True
