import json
from dataclasses import dataclass
from typing import Callable, Dict, Any
import logging

from shyft.time_series import time
from hydrology.service.config import HydroServiceConfig
from shyft.energy_market.stm import StmTaskClient, StmTask, StmCase
from hydrology.task.utils import enable_thread_debug

logger = logging.getLogger(__name__)

@dataclass
class TaskAdmin:
    register_drms_node:str="register_drms_node"
    unregister_drms_node:str="unregister_drms_node"

def register_drms_node(tc: StmTaskClient, host_port: str) -> bool:
    """
    Register a drms compute node
    :param tc: Task client
    :param host_port: where to reach the drms compute node
    :return: true if fx call succeeded
    """
    return tc.fx(mid=0, fx_arg=f'{{"{TaskAdmin.register_drms_node}":"{host_port}"}}')


def unregister_drms_node(tc: StmTaskClient, host_port: str) -> bool:
    """
    Unregister a drms compute node
    :param tc:
    :param host_port:
    :return: true if fx call succeeded
    """
    return tc.fx(mid=0, fx_arg=f'{{"{TaskAdmin.unregister_drms_node}":"{host_port}"}}')

def create_unique_case_for_task(tc: StmTaskClient, task_id:int) -> int:
    while True:
        try:
            m:StmTask = tc.read_model(task_id)  # get a fresh copy
            case_id=1
            if m.cases:
                case_id= 1+ max([c.id for c in m.cases]) # make a unique id..
            new_case: StmCase = StmCase(id=case_id,name=f'{task_id}#{case_id}',created=time.now(),json='')
            tc.add_case(mid=task_id,case=new_case) # will fail if not unique..
            return case_id
        except RuntimeError as re:
            if "was not unique" in re:
                logging.info(f'Failed to create unique case, retrying:{re}') # retry.
            else:
                raise RuntimeError("Failed to create unique case {re}")


class TaskCallback:
    """
    A simple message dispatcher that forwards messages to
    functions registered in the fx_table.
    It is anticipated that the user of client.fx, passes in arguments that
    is json formatted, and contains a key 'action' that are
    registered in the fx_table.
    The remainder of the arguments are passed through to the
    handler.
    The handler has the signature
    Callable[[HydroServiceConfig, StmTaskClient, StmTask, str], bool]]
    like
    ```python
    def my_callback_handler(cfg:HydroServiceConfig, c:StmTaskClient,task:StmTask, fx_args:str)->bool:
       return True
    ```

    The cfg is valid for the system, and can be used to get connections to
    other microservices (the StmTaskClient are already passed as arg).
    The task is the current task(have case, runs..)
    and fx_args are a json like structure, stripped of 'action' keyword.

    Suitable for using along with cls.from_json(..).

    """

    def __init__(self, cfg: HydroServiceConfig):
        self.cfg: HydroServiceConfig = cfg
        self.c: StmTaskClient = cfg.task.client()
        self.fx_table: Dict[str, Callable[[HydroServiceConfig, StmTaskClient, StmTask, Any], bool]] = {}

    @enable_thread_debug
    def __call__(self, mid: str, fx_arg: str) -> bool:
        logger.debug(f'Got callback for {mid=}, {fx_arg=}')
        args = json.loads(fx_arg.strip())
        logger.debug(args)
        if int(mid)==0:
            #print('Task admin cmd received')
            if TaskAdmin.register_drms_node in args:
                self.cfg.register_drms_node(args[TaskAdmin.register_drms_node])
            elif TaskAdmin.unregister_drms_node in args:
                self.cfg.unregister_drms_node(args[TaskAdmin.unregister_drms_node])
            else:
                logger.warning("Unknown cmd for task root")
                return False
            return True
        task = self.c.read_model(mid=mid)
        logger.debug(
            f'Got model {task.name=} and gcd {task.base_model.host}:{task.base_model.port_num} and base model {task.base_model.model_key}')
        if 'action' not in args:
            logger.error(f"Missing 'action' keyword in passed json args {fx_arg}")
            return False

        action = args['action']
        del args['action']  # strip off action, and use clever .from_json to restore a true RunModelArgs
        if action not in self.fx_table.keys():
            logger.error(f'Action {action} not found in fx table {self.fx_table.keys()}')
            return False
        fx_args = json.dumps(args)
        logger.debug(f'Invoking {action} with args {fx_args}')
        try:
            result= self.fx_table[action](self.cfg, self.c, task,fx_args)
        except Exception as e:
            logger.error(f'Exception {e}')
            result=False
        return result
