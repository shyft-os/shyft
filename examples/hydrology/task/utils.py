from shyft.time_series import GeoTimeSeriesConfiguration
from shyft.time_series import DtsClient
import functools

# Note: the support for bg threads breakpoints
#       is a best effort, and flaky, depending
#       on a lot of factors to work.
#

_debugpy_available: bool = False
_pydevd_available: bool = False

if __debug__:
    try:
        import debugpy

        _debugpy_available = True
    except ImportError:
        pass
    if not _debugpy_available:
        try:
            import pydevd

            _pydevd_available = True
        except ImportError:
            pass


def enable_thread_debug(func):
    @functools.wraps(func)
    def thread_debug_enabled_func(*args, **kwargs):
        if _pydevd_available:
            try:
                pydevd.settrace(suspend=False, trace_only_current_thread=True)
            except Exception as e:
                print(f'pydevd enable failed for {func.__qualname__}')
        elif _debugpy_available:
            try:
                debugpy.debug_this_thread()
            except Exception as e:
                print(f'debugpy enabled failed for {func.__qualname__}\n'
                      'for pycharm/clion maybe uninstall system debugpy might resolve the problem')
        return func(*args, **kwargs)

    return thread_debug_enabled_func


def get_geo_db(c: DtsClient, gdb: str) -> GeoTimeSeriesConfiguration:
    geo_cfgs = c.get_geo_db_ts_info()
    gcc = [g for g in geo_cfgs if g.name == gdb]
    if gcc:
        return gcc[0]
    else:
        return None
