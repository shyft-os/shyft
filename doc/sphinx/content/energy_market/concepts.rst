********
Concepts
********

Shyft provides a fully working operationally used descriptive electricity energy-market model framework.

The main physical components that constitutes the essential parts are represented, such as the power-grid,
producers, consumers, and also details relevant for production planning, catchments(runoff),
reservoirs, tunnel and river network, wind farms, and generic power modules for describing
other power sources (nuclear, coal, gas etc.).
The detail level is developed so that we can cover common energy-management processes,
spanning from intra-day up to long term market simulations, and 'what if' scenarios for
renewable energy sources, maintenance planning etc.

In this context, the descriptive model try to capture the essential components, as they
are physically, like river, tunnel, unit, bus-bar, transmission-line,
or for logical items, like contracts, quantity, price etc.

The descriptive model allows us to use common terms, and makes room for the 'algorithmic models',
often created by harvesting information from the descriptive model, and translate it
into data-structures, like matrices, networks, or combination, that can be passed to
solvers to find optimum solutions, or practical/realistic simulation results.

.. note::
    The following content is produced iteratively using questions to open AI

Main component categories
=========================

.. mermaid::

    classDiagram
        direction LR
        StmSystem -- Network :networks
        StmSystem -- HydroPowerSystem : hydro_power_systems
        StmSystem -- WindFarm : wind_farms
        StmSystem -- PowerModule : power_modules
        StmSystem -- MarketArea : market_areas
        StmSystem -- UnitGroup : unit_groups
        StmSystem -- Contract : contracts
        StmSystem -- ContractPortfolio : contract_portfolios



Network, the power grid
-----------------------
The power grid is a complex system responsible for delivering electricity from the power generation units to end consumers. It primarily consists of generating stations, transmission lines, and distribution lines, all interconnected to ensure a continuous and reliable flow of electricity. In this context, two critical components of the power grid are the bus-bar and transmission line.

.. mermaid::

    classDiagram
        direction TB
        Network -- Busbar: busbars
        Network -- TransmissionLine : transmission_lines
        TransmissionLine --> Busbar: to
        TransmissionLine --> Busbar: from


Bus-bar
.......
A bus-bar is a key node in the power grid where multiple paths converge. It's a thick strip made from a highly conductive material such as copper or aluminum that helps in the distribution of electricity. Its main function is to effectively distribute electric power with high efficiency while also reducing the chance of power losses. Bus-bars are usually found in electrical substations, where different electrical inputs are effectively managed and dispatched. It symbolically represents the point in a system where multiple elements can supply power or have power drawn from.

Transmission line
.................
Transmission lines constitute the network that connects power generation units with local distribution systems. These high-capacity carriers efficiently deliver electrical power over long distances. Made from highly conductive materials like aluminum or copper, these lines are designed to minimize losses during transmission. They usually operate at high voltages, in order to reduce the dissipation of energy as heat, and are supported high above the ground on transmission towers. A crucial part of the grid, transmission lines ensure that generated power is reliably delivered to substations, which then distribute the power to end consumers.


Hydro power system
------------------

The Shyft Hydro Power System is a comprehensive network designed to harness the potential of water for electricity generation. The fundamental components of the system include catchments, reservoirs or rivers, a tunnel network, and power generation units comprising turbines and generators.

.. mermaid::

    classDiagram
        direction LR
        HydroPowerSystem -- Catchment: catchments
        HydroPowerSystem -- Reservoir: reservoirs
        HydroPowerSystem -- Waterway: waterways
        HydroPowerSystem -- Gate: gates
        Waterway --> Gate: gates
        HydroPowerSystem -- PowerPlant: power_plants
        HydroPowerSystem -- Unit: units
        PowerPlant --> Unit: units

Catchments
..........

The system begins with catchment areas, also known as drainage basins. These are areas of land where precipitation collects and drains off into a common outlet. In the context of the hydro power system, the outlet is typically a reservoir or a river. The role of the catchment is to provide a consistent source of water, known as discharge or runoff, to fuel the power generation process.

Reservoirs and Rivers
.....................
Upon leaving the catchment, the runoff flows into reservoirs or rivers. These bodies of water serve as natural storage systems for the collected water. In some cases, rivers provide an added input of water to the system through lateral inflow. These large volumes of stored water carry significant potential energy due to their elevation.
Rivers float into creek-inlets, or reservoirs, that keeps the water.

Tunnel Network
..............
The water from the reservoirs or creek-inlets is then channeled through a tunnel network to the power generation units. These tunnels, also known as penstocks in hydroelectric power systems, are designed to guide the water flux towards the turbines effectively. The force of the flowing water through these tunnels helps infringing the high mechanical energy used for power generation.
The complexity of the tunnel network depends on power-plant design. The most complex systems do have multiple
reservoir upstreams, with pressure-balanced flow, as well as similar downstream reservoirs.
The turbines might be sharing tunnels, penstocks and draft in any configuration as designed.
Also note that turbines might be reversible, working as pumps. Depending on physical design a unit can
be both generating, and pumping, but not at the same time, or just one of them.

Power Generation Units
......................

At the heart of the system are the power generation units, each consisting of a turbine and a generator. As water rushes down the tunnels, it strikes the blades of the turbine, causing it to spin. This kinetic energy gets converted into mechanical energy. The rotating turbine is connected to a generator, where this mechanical energy gets further converted into electrical energy. This is the fundamental process through which the hydro power system generates electricity.

Wind farms
----------
A wind farm, is a group of wind turbines in the same location used for the production of electric power. A large wind farm may consist of several hundred individual wind turbines spread over an extensive area.

Wind Turbines
.............
Wind turbines are the critical components of a wind park. They convert the kinetic energy in the wind into mechanical power. This mechanical power can be used for specific tasks, or a generator can convert it into electricity to power homes, businesses, schools, etc.
Each wind turbine consists of a tower, rotor, and nacelle. The rotor has multiple blades (usually three) that capture wind's energy. The nacelle houses a drive train which includes a gearbox and generator.


Substations(planned)
....................
Substations play a crucial role within a wind park. They are intermediary points in the power grid that transform the electrical output from wind turbines to a higher voltage so that it can be transmitted efficiently over long distances. Substations also control and protect the electrical power flows, respond to faults, provide points for grid connection and disconnection, and serve as hubs to redistribute the power to multiple directions.

Grid Connection
...............
The electricity produced by a wind park needs to be transmitted to where it is needed. After the electricity is generated by the turbines and stepped up to a high voltage by the substations, the power is fed into the electric grid. Here, it is combined with electricity from other power plants and distributed to end consumers.
The process for connecting a wind park to the grid is complex and involves extensive planning to ensure the stability of the grid while making the most of the fluctuating and location-dependent nature of wind energy. Grid operators need to balance the supply from the wind park with the demand from consumers while also ensuring the quality of the electricity supply (e.g., maintaining frequency and reliability).


Power modules
-------------
The PowerModule in Shyft is a versatile component that can simulate various entities within a power system. It can represent either power production units like power plants or power consumption entities like towns or cities. The functionality of a PowerModule is dictated by the properties assigned to it.

PowerModule as a Power Producer
...............................
When configured to act like a power plant, the PowerModule can be given properties to simulate any type of power plant,
each with its own unique energy conversion methods.

- **Gas/Coal Fuel, Combined Heat and Power:** To replicate a fossil fuel power plant, the PowerModule can be given properties such as the type of fuel used, the efficiency of the plant at converting that fuel to electricity, the capacity of the plant, and emission factors.
- **Nuclear :** When functioning as a nuclear power plant, the PowerModule may have properties pertaining to the type of nuclear reactor, the level of efficiency in converting thermal energy to electricity, the plant's output capacity, and the associated waste and emission factors. Other energy production methods, like wind, hydro, and solar, can also be simulated by appropriately setting the PowerModule's properties.


PowerModule as a Power Consumer
...............................

The PowerModule can also be configured to mimic a power consumer, such as an urban city or a small town. In this scenario, the PowerModule is given properties to represent characteristics of electricity usage.
For example, it can be assigned features that signify peak load times, average electricity use, factors impacting power use like weather or time of day, population size, and the type of users (residential, commercial, industrial).
Price/temperature sensitivity and seasonal patterns are also common to use for these parts.

Summary
.......
In essence, the PowerModule in Shyft is a powerful tool for simulating diverse elements within a power grid, be they power producers or consumers. The versatility of this module allows for accurate modeling and forecasting in power systems, thereby enhancing the optimization and management of the power grid.

Market areas
------------
The MarketArea class in Shyft plays a crucial role in simulating and managing power markets within a power grid. The boundaries of a MarketArea are defined by a unique set of bus-bars that it contains and the power transmission lines that interconnect one instance of the MarketArea with others.

Bus-Bars and Power Transmission Lines
.....................................
Bus-bars are conductor rails that distribute power from generators to the load. In the context of MarketArea, they serve as the nodes that define the spatial extent of a market area. The interconnected bus-bars form a network that allows power to be transmitted and distributed across different regions within the MarketArea.
Power transmission lines are crucial for interconnecting different market areas. These high-voltage lines carry electricity over long distances from power plants to load centers. In MarketArea, these lines allow for the transfer of electricity between different instances of the market, enabling a more efficient distribution of power resources based on supply and demand conditions in different areas.

Multiple Market Types
.....................
A key feature of the MarketArea class is the support for multiple market types. Depending on the Transmission System Operator's (TSO's) requirements for operational stability and redundancy, different delineations can be defined within a MarketArea.
For example, there could be wholesale electricity markets, real-time markets, day-ahead markets, and ancillary services markets within a MarketArea, each serving specific purposes and enabling different types of transactions. These markets can be structured and designated according to the unique operational and redundancy needs of the TSO.
To summarize, the MarketArea class in Shyft provides a comprehensive tool for representing and operating various aspects of power markets within a power grid. The establishment of different market types and the interconnection of market areas via transmission lines offer flexible and robust ways to ensure a stable, efficient, and reliable power grid.

Contracts and Contract portfolios
---------------------------------
The 'Contract' in Shyft is a fundamental object that represents an agreement between two or more parties to buy or sell a specific product at a predetermined price and time. The properties of the 'Contract' encapsulate the following essential information:

- Time,t: The time element represents the period during which the product will be delivered. This could be a specific point in time or a defined time-window depending on the type of product and market rules.
- Price(t): This represents the cost per unit of the product that has been agreed upon by the parties involved.
- Quantity(t): This represents the volume or amount of the product that will be delivered under the agreement.
- Constraints and Penalties(t): Various constraints with corresponding penalties applicable to the contract

Contract and Hydro Power-Plant
..............................
Contracts can optionally form an association with a hydro power-plant. This feature ensures that a certain volume of energy supplied under the contract is generated using renewable energy from the hydro power-plant. By including this option, Shyft allows for agreements that support renewable energy initiatives and follow environmental compliance rules.
Contract Semantic Relations
In addition to physical relationships, contracts can also have semantic relations with other contracts. These relations can be governed by market rules applicable to the product.  Depending on the defined market rules, this could be:

- Chained Contracts: In this type of relationship, the contract is considered applicable only if the entire chain of contracts is chosen. This is often seen in situations where delivery of the product involves multiple stages or when contracts are used to hedge risks.
- Exclusive Contracts: Here, the semantic relation terms specify that the contract is exclusive, in the sense that one contract can be chosen out of two or more, but not simultaneously.

ContractPortfolio
.................

A 'ContractPortfolio' in Shyft is a collection of 'Contract' objects. It offers a consolidated view of multiple contracts for efficient management and risk assessment. The 'ContractPortfolio' allows for the analysis of aggregated risks, costs, and revenues from the portfolio of contracts. It aids in decision-making, helps manage complexities associated with multiple contracts, and plays a key role in strategizing and planning.
Overall, the 'Contract' and 'ContractPortfolio' classes in Shyft represent a comprehensive system for managing energy contracts, enabling efficient and flexible market operations, promoting renewable energy, and allow for flexible modelling of market rules.

UnitGroup
----------

The UnitGroup class in Shyft is a key component representing a group of
active hydro power units that collectively contribute to the production
of a specific product or generation capability.

The unit group members, are hydro-power units, which in turn are connected to the power-grid via bus-bars.
The membership time-frame can also be controlled explicit by a mask time-series (1 or 0), so that
the contribution also follows the mask time-series.

Obligations and Delivery
........................
The UnitGroup has an obligation, typically defined by contracts or regulatory requirements, that should be met. This can be viewed as the performance metric or output that is expected from the group of units. For example, it can be the total amount of electricity that the unit group is obligated to generate in a specific timeframe.
One of the main functionalities provided by the UnitGroup class is the computed delivery of the product that the unit group provides. This refers to the capability of the UnitGroup to calculate and report the cumulative product or energy output delivered by the group of units at any given time.

In practice, there is often a schedule, a plan for the delivery, then there is the optimized/simulated delivery, and
most important, the realised delivery, that indicates the historical fact from observed measurements.

