*************************************************
Tutorials for the Shyft dashboard for time series
*************************************************

:ref:`setup-shyft`

Prerequisites
=============

This section assumes that you are familiar with :ref:`PackageTimeSeries` and the concepts of visualization library
`Bokeh <https://bokeh.org/>`_.

