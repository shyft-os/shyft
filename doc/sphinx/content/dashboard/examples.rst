********
Examples
********

shyft.dashboard.examples
========================

.. autosummary::
   :toctree: _autosummary
   :recursive:

   shyft.dashboard.examples
   shyft.dashboard.apps
   shyft.dashboard.entry_points