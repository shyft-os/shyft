Examples
========

.. toctree::
    :maxdepth: 1

    examples/convolve.rst
    examples/decode.rst
    examples/derivative.rst
    examples/dtss_krls_container.rst
    examples/inside.rst
    examples/partition_by_and_percentiles.rst
    examples/time_calendar_period.rst