************
Introduction
************

Welcome to the Shyft documentation!

To use this doc effectively:

- Use the **Next** and **Previous** buttons on the bottom of the page
- Use the **search bar** at the top left corner to search the docs
- Click the topics in the content menu to show to content tree, to explore subtopics

The documentation is structured as follows for each category:

- **Installation:** How to install 
- **Concepts:** A description of the concepts inside the category
- **Getting started:** How-to descriptions
- **Examples:** Code examples 
- **API Documentation:** Generated documentation for API lookup and description

