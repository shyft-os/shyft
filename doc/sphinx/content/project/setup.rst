.. _setup-shyft:

===============================
Setup environment for tutorials
===============================

In order to run the tutorials, either install Shyft using any available method for your platform, pip, conda,
or build Shyft from sources.

If you have a working container environment, podman or docker, these can be used to get a quick start
for working with Shyft.

.. note::
    A ready made container image with Shyft, is available at container registry `https://gitlab.com/shyft-os/shyft/container_registry`


Container based setup
---------------------

First, ensure you have the latest sdk image, of available flavors

 - https://gitlab.com/shyft-os/shyft/archlinux-build:latest
 - https://gitlab.com/shyft-os/shyft/fedora-build:latest

If using vscode, create a dev container setup with this image, a similar setup template directory is
available in https://gitlab.com/shyft-os/shyft/build_support/devcontainer, using the development container
above.

In vscode terminal:

.. code-block:: shell

    cd /workspaces
    git clone https://gitlab.com/shyft-os/shyft-data.git
    git clone https://gitlab.com/shyft-os/shyft-doc.git


.. code-block:: shell

    cd /usr/local/lib64
    ln -s /workspaces/shyft-data shyft-data
    cd /workspaces
    pip install ipywidgets

in vscode: Ctrl+Shift+P and select: Jupyter Create Interactive window

**That's it!** 

Example of working in interactive window:

.. image:: setup_interactive.png


Info about packages
-------------------

.. include:: ../_shared/help.rst