import os
import glob
from setuptools import setup, Distribution, find_namespace_packages
from sysconfig import get_platform

shyft_license: str = 'LGPL v3'
shyft_author: str = 'shyft'
shyft_author_email: str = 'sigbjorn.helset@gmail.com'
shyft_url: str = 'https://gitlab.com/shyft-os/shyft'

from shyft.time_series import __version__ as VERSION
from shyft.time_series import __release__ as RELEASE
from shyft.time_series import __release_type__ as RELEASE_TYPE

print(f'Building Shyft version {VERSION}')


class BinaryDistribution(Distribution):
    """Distribution which always forces a binary package with platform name"""

    def has_ext_modules(self):
        return True


# needs to be ran next to cmake install-dir.
package_roots = find_namespace_packages(where="./shyft")
packages = [f'shyft']
packages.extend([f'shyft.{p}' for p in package_roots])
requires = ["numpy"]
install_requires = ["numpy"]
tests_require = ["pytest"]
package_data = {p: [] for p in packages}

for p in package_data.keys():
    package_data[p].extend(['*.so', '*.pyd', '*.pyi', "*.yml", "*.sh", "*.service"])
package_data['shyft'] = ['*.so', '*.pyd', '*.pyi', '*.dll']

extras_require = {
    'shyft.dashboard.time_series': ['bokeh', 'pint', 'pydot'],
    'shyft.hydrology.repositories': ['netcdf4', 'shapely', 'pyyaml', 'pyproj'],
    'shyft.hydrology.viz': ['matplotlib'],
    'shyft.hydrology.notebooks': ['jupyter']
}

entry_points = {
    'console_scripts': [
        'shyft-dashboard-examples = shyft.dashboard.entry_points.start_bokeh_examples:main',
        'shyft-dashboard-visualisation = shyft.dashboard.entry_points.visualize_apps:main'
    ],
    'shyft_dashboard_apps': [
        'dtss_viewer_app = shyft.dashboard.apps.dtss_viewer.dtss_viewer_app:DtssViewerApp'
    ]
}
name = f'shyft'
description = 'Open Shyft, - An framework providing python enabled tools and services for the energy-market, hydrological forecasting and advanced time-series',
if os.name == 'nt':
    # Windows: These are installed directly at shyft/, to allow them to be loaded
    shared_libs = []
    shared_libs_version = []
else:
    # in the case of building a linux wheel, it is possible, although distro pkgs are preferred
    shared_libs = glob.glob(f"../../*.so.{VERSION.split('.')[0]}")
    shared_libs_version = glob.glob(f"../../shyft-{VERSION}/*")
setup(
    name=name,
    version=f'{VERSION}.post{RELEASE}' if RELEASE_TYPE == 1 else f'{VERSION}.dev{RELEASE}',
    author=shyft_author,
    author_email=shyft_author_email,
    url=shyft_url,
    description=description,
    license=shyft_license,
    packages=packages,
    include_package_data=True,
    package_data=package_data,
    requires=requires,
    install_requires=install_requires,
    tests_require=tests_require,
    platforms=[get_platform()],
    zip_safe=False,
    data_files=[('lib', shared_libs), (f'lib/shyft-{VERSION}', shared_libs_version)],
    distclass=BinaryDistribution,
    extras_require=extras_require,
    entry_points=entry_points
)
