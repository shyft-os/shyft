import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ, XyPointCurveWithZList, TurbineOperatingZone, TurbineOperatingZoneList, TurbineDescription
from shyft.energy_market.stm import t_xy, t_xyz, t_turbine_description
from shyft.time_series import time

t0 = time('2000-01-01T00:00:00Z')
t1 = time('2022-01-01T00:00:00Z')


def test_t_xy():
    # Test case:
    t = t_xy()
    t[t0] = XyPointCurve(PointList([]))
    assert str(t)

    # Test case:
    t = t_xy()
    t[t0] = XyPointCurve(PointList([Point(20.0, 96.0)]))
    assert str(t)

    # Test case:
    t = t_xy()
    t[t0] = XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)]))
    str(t)
    print(t)
    assert str(t)


def test_t_xyz():
    # Test case:
    t = t_xyz()
    t[t0] = XyPointCurveWithZ(XyPointCurve(PointList([])), 70.0)
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_xyz()
    t[t0] = XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0)])), 70.0)
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_xyz()
    t[t0] = XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0)
    str(t)
    print(t)
    assert str(t)


def test_t_turbine_description():
    # Test case:
    t = t_turbine_description()
    str(t)
    print(t)
    assert str(t)
    # Expected results:
    # {}

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription()
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList([]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([])), 70.0)]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList(
        [TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList(
        [TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    t[t1] = TurbineDescription(TurbineOperatingZoneList(
        [TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    t[t1] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    str(t)
    print(t)
    assert str(t)


def test_t_turbine_description_for_pelton():
    #
    # Turbin description for pelton turbin, with needle combinations - more than one turbine efficiency object!
    #

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList(
        [TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0)])),
         TurbineOperatingZone(XyPointCurveWithZList([XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0)]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])), TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    str(t)
    print(t)
    assert str(t)

    # Test case:
    t = t_turbine_description()
    t[t0] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])), TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    t[t1] = TurbineDescription(TurbineOperatingZoneList([TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)])), TurbineOperatingZone(XyPointCurveWithZList(
        [XyPointCurveWithZ(XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])), 70.0),
         XyPointCurveWithZ(XyPointCurve(PointList([Point(22.0, 72.0), Point(42.0, 82.0), Point(62.0, 92.0), Point(82.0, 92.0), Point(102.0, 92.0), Point(112.0, 92.0)])), 72.0)]))]))
    str(t)
    print(t)
    assert str(t)
