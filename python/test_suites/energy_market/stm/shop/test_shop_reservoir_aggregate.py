import pytest
import numpy as np

import shyft.time_series as sts
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ
from shyft.energy_market import stm
from shyft.energy_market.stm.shop import shyft_with_shop

if not stm.shyft_with_stm or not shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

from shyft.energy_market.stm.utilities import create_t_turbine_description, create_t_xyz_list
from shyft.energy_market.stm import StmSystem, MarketArea, shop

from .test_river_flow_description import create_t_double


def run_shop(stm_sys: StmSystem, run_ta: sts.TimeAxis) -> shop.ShopCommandList:
    cmd_list = shop.ShopCommandList()
    cmd_list.extend([
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])

    shop_sys = shop.ShopSystem(run_ta)
    shop_sys.emit(stm_sys)
    for cmd in cmd_list:
        shop_sys.commander.execute(cmd)
    return shop_sys


def create_double_reservoir_system():
    hps = stm.HydroPowerSystem(1, "Hydro_power_system")
    t0 = sts.time("2010-01-01T00:00:00Z")

    rsv_up = hps.create_reservoir(1, "rsv_up")
    rsv_vol_head = stm.t_xy()
    rsv_vol_head[t0] = XyPointCurve(PointList([
        Point(0.0e6,    540.04),
        Point(10.1e6,   544.94),
        Point(20e6,   545.94),
    ]))
    rsv_up.volume_level_mapping.value = rsv_vol_head
    rsv_up.volume.static_max.value = create_t_double(t0, 10.1e6)
    rsv_up.level.regulation_max.value = create_t_double(t0, 544.94)
    rsv_up.level.regulation_min.value = create_t_double(t0, 540.04)

    rsv_down = hps.create_reservoir(2, "rsv_down")
    rsv_down_vol_head = stm.t_xy()
    rsv_down_vol_head[t0] = XyPointCurve(PointList([
        Point(0.0e6,    464.54),
        Point(7.8e6,   468.36),
        Point(20.0e6,   468.55)]))
    rsv_down.volume_level_mapping.value = rsv_down_vol_head
    rsv_down.volume.static_max.value = create_t_double(t0, 7.8e6)
    rsv_down.level.regulation_max.value = create_t_double(t0, 468.36)
    rsv_down.level.regulation_min.value = create_t_double(t0, 464.54)

    # Waterways
    f_rsv_up = hps.create_waterway(1, "ww_f_rsv_up")
    f_gate = f_rsv_up.add_gate(1, "gate_f_rsv_up")
    f_rsv_up.input_from(rsv_up, ConnectionRole.flood)
    f_gate.flow_description = create_t_xyz_list(t0, XyPointCurve([
        Point(544.94,   0.0),
        Point(545.27, 7.0),
        Point(545.61, 21.0),
        Point(545.94,   38.0),
    ]), 0)

    ww_rsv_up_to_down = hps.create_waterway(2, "ww_rsv_up_to_down")
    gate_ww_rsv_up_to_down = ww_rsv_up_to_down.add_gate(2, "gate_ww_rsv_up_to_down", "")
    gate_ww_rsv_up_to_down.discharge.static_max.value = create_t_double(t0, 4)
    ww_rsv_up_to_down.input_from(rsv_up, ConnectionRole.main).output_to(rsv_down)

    f_rsv_down = hps.create_waterway(3, 'ww_f_rsv_down')
    f_down_gate = f_rsv_down.add_gate(3, 'gate_f_rsv_down')
    f_rsv_down.input_from(rsv_down, ConnectionRole.flood)
    f_down_gate.flow_description = create_t_xyz_list(t0, XyPointCurve([
        Point(468.36,   0.0),
        Point(468.42, 2.0),
        Point(468.49, 7.0),
        Point(468.55, 12.0),
    ]), 0)

    # Unit
    unit1 = hps.create_unit(1, 'Unit_1')
    unit1.turbine_description.value = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(1.2,  84.14),
            Point(1.4,  84.69),
            Point(1.6,  85.22),
            Point(1.8,  85.74),
            Point(2.0,  86.23),
            Point(2.2,  86.69),
            Point(2.4,  87.11),
            Point(2.6,  87.49),
            Point(2.8,  87.81),
            Point(3.0,  88.09),
            Point(3.2,  88.30),
            Point(3.4,  88.47),
            Point(3.6,  88.57),
            Point(3.8,  88.63),
            Point(4.0,  88.63),
            Point(4.2,  88.58),
            Point(4.4,  88.48),
            Point(4.6,  88.35),
            Point(4.8,  88.19),
            Point(5.0,  87.99),
            Point(5.2,  87.78),
            Point(5.4,  87.58),
            Point(5.6,  87.37),
            Point(5.8,  87.16),
        ])), 455.00),
    ])

    unit1.production.static_max.value = create_t_double(t0, 21.5e6)
    unit1.production.static_min.value = create_t_double(t0, 4.0e6)

    unit1.cost.start.value = sts.TimeSeries(
        sts.TimeAxis(sts.time('2010-01-01T00:00:00Z'), sts.deltahours(8760), 25),
        fill_value=90.0, point_fx=sts.POINT_AVERAGE_VALUE)
    unit1.cost.stop.value = sts.TimeSeries(
        sts.TimeAxis(sts.time('2010-01-01T00:00:00Z'), sts.deltahours(8760), 25),
        fill_value=90.0, point_fx=sts.POINT_AVERAGE_VALUE)

    gen_eff = stm.t_xy()
    gen_eff[t0] = XyPointCurve(PointList([
        Point(3.5e6, 93.48),
        Point(4.5e6, 94.77),
        Point(5.5e6, 95.63),
        Point(6.5e6, 96.23),
        Point(8.5e6, 96.97),
        Point(9.5e6, 97.21),
        Point(10.5e6, 97.41),
        Point(11.5e6, 97.57),
        Point(12.5e6, 97.71),
        Point(13.5e6, 97.81),
        Point(14.5e6, 97.90),
        Point(15.5e6, 97.96),
        Point(16.5e6, 98.01),
        Point(17.5e6, 98.05),
        Point(18.5e6, 98.08),
        Point(19.5e6, 98.11),
        Point(20.5e6, 98.14),
        Point(21.5e6, 98.16),
    ]))

    unit1.generator_description.value = gen_eff

    # PowerPlant
    pp = hps.create_power_plant(1, 'Plant_1')
    pp.add_unit(unit1)
    pp.outlet_level.value = create_t_double(t0, 3)  # Outlet level might to be moved to waterway

    # Downstream Waterways
    wr1 = hps.create_waterway(6, 'ww_rsv_down_to_Unit1')
    pen1 = hps.create_waterway(7, 'Penstock 1 Unit1')
    tr1 = hps.create_waterway(8, 'Tail race for Unit1')

    wr1.input_from(rsv_down).output_to(pen1)
    wr1.add_gate(9, 'ww_rsv_down_to_u1_gate', '{}')
    pen1.input_from(wr1).output_to(unit1)
    tr1.input_from(unit1)

    wr1.head_loss_coeff.value = create_t_double(t0, 0.0074)
    pen1.head_loss_coeff.value = create_t_double(t0, 0.3170)
    return hps

@pytest.mark.skipif(not shop.shyft_with_shop, reason="shyft not build with shop")
def test_reservoir_aggregates(system_to_optimize):
    dt = sts.Calendar.HOUR
    ta = sts.TimeAxis(sts.time("2023-01-01T00:00:00Z"), dt, 24)
    ta_real = sts.TimeAxis(ta.time(0)-dt, dt, 3)

    stm_sys = StmSystem(1, "Two_reservoirs_system", "")
    hps = create_double_reservoir_system()
    stm_sys.hydro_power_systems.append(hps)
    ma = MarketArea(1, "ma", "", stm_sys)
    ma.price.value = create_t_double(sts.time(0), 13.0e-6)
    ma.max_sale.value = create_t_double(sts.time(0), 9999e6)
    ma.max_buy.value = create_t_double(sts.time(0), 0)
    stm_sys.market_areas.append(ma)

    rsv_up, rsv_down = hps.reservoirs
    rsv_up.water_value.endpoint_desc = create_t_double(sts.time(0), 12.1e-6)
    rsv_down.water_value.endpoint_desc = create_t_double(sts.time(0), 12.1e-6)
    rsv_up.inflow.schedule.value = sts.TimeSeries(ta, 0.5, sts.POINT_AVERAGE_VALUE)
    rsv_down.inflow.schedule.value = sts.TimeSeries(ta, 1, sts.POINT_AVERAGE_VALUE)
    rsv_up.level.realised.value = sts.TimeSeries(ta_real, 544.45, sts.POINT_INSTANT_VALUE)
    rsv_down.level.realised.value = sts.TimeSeries(ta_real, 467.02, sts.POINT_INSTANT_VALUE)

    # Volume Aggregate
    ra = hps.create_reservoir_aggregate(1, "Reservoir Aggregate", "")
    ra.add_reservoir(rsv_up)
    ra.add_reservoir(rsv_down)
    ra.volume.constraint.min.value = sts.TimeSeries(ta, 13.0e6, sts.POINT_AVERAGE_VALUE)
    ra.volume.constraint.max.value = sts.TimeSeries(ta, 15.0e6, sts.POINT_AVERAGE_VALUE)
    ra.volume.static_max.value = rsv_up.volume.static_max.value + rsv_down.volume.static_max.value

    shop_sys = run_shop(stm_sys, ta)
    shop_sys.collect(stm_sys)
    shop_sys.complete(stm_sys)

    v_sum = rsv_down.volume.result.value + rsv_up.volume.result.value
    assert all(v_sum.inside(13e6, 15e6, inside_v=1, outside_v=0).values)