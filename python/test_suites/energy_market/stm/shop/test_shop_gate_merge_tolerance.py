import math
import pytest

from shyft.energy_market.stm import shyft_with_stm
from shyft.energy_market.stm.shop import shyft_with_shop

if not shyft_with_stm or not shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

from typing import List
import numpy as np
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ, ConnectionRole
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, shop
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case, DoubleVector, Calendar


def show_ts(title: str, ta: TimeAxis, ts: TimeSeries, scale: float = 1.0, accumulate=False, initial_dev: float = 0.0):
    s = ""
    if not isinstance(ts, TimeSeries):
        ts = ts.value if ts.exists and ts.value else None
    if ts:
        r = TimeSeries(ta, fill_value=0.0, point_fx=stair_case)
        if accumulate:
            ta_acc = TimeAxis(ta.time(0), 3600, len(ta) + 1)
            tmp = initial_dev + ts.accumulate(ta_acc).time_shift(-3600)  # just to present it better
            xx = tmp.use_time_axis_from(r)  # just to resample all values to the run-time-axis
        else:
            xx = ts.use_time_axis_from(r)  # just to resample all values to the run-time-axis

        for v in xx.values:
            if math.isfinite(v):
                s = s + f"{v*scale:5.1f} "
            else:
                s = s + "   .  "
        print(f"{title:20}:{s}")
    else:
        print(f"{title:20}: empty")


def create_optimization_commands(run_id: int, write_files: bool) -> list[shop.ShopCommand]:
    return [
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.set_merge_on(),  # required to have shop take into account merge criteria
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ]


def wave_(ta: TimeAxis, offset: float, amplitude: float, n_periods: float = 3.0) -> DoubleVector:
    """ ts-generator: f(t) = offset + amplitude*sin( w*t) , w= .. """
    n = len(ta)
    return DoubleVector([offset + amplitude*math.sin(n_periods*2*3.14*i/n) for i in range(n)])


def create_const_timeseries(ta, fill_value):
    return TimeSeries(ta, fill_value=fill_value, point_fx=stair_case)


def create_timeseries(ta_points: List, ts_val: List):
    ta = TimeAxis(ta_points)
    ts = TimeSeries(ta, ts_val, stair_case)


def uid_gen():
    i = 0
    while True:
        i += 1
        yield i


def build_simple_stm_system(run_ta: TimeAxis):
    """
    This model is created with the purpose to illustrate/verify
    that constraint  on rivers,
    that imposes limitation on min and max  flow, accumulated flow
    and in this specific case, accumulated deviation up/down from a specific reference flow.
    """
    t_begin = time(int(run_ta.time_points[0]))

    # t_end = run_ta.time_points[-1]
    # dt = run_ta.time_points[1] - run_ta.time_points[0]

    mega = 1000000
    ui = uid_gen()
    stm = StmSystem(next(ui), "stm_system", "")
    # Create power market
    ma = MarketArea(next(ui), "market", '{}', stm)
    ma.price = TimeSeries(run_ta, values=wave_(run_ta, 18.6/mega, 10.0/mega, 2), point_fx=stair_case)  # constructed so that two hours have positive prod values
    ma.max_buy = create_const_timeseries(run_ta, 0*mega)
    ma.max_sale = create_const_timeseries(run_ta, 120*mega)
    # ma.load = TimeSeries(run_ta, values=wave_(run_ta, 50.0 * mega, 20 * mega), point_fx=stair_case)  # no load requirement in this case, for simplicity
    stm.market_areas.append(ma)

    # Create hydro power system
    hps = HydroPowerSystem(next(ui), "hps")

    # Reservoir
    rsv = hps.create_reservoir(next(ui), "rsv")
    volume_mapping = create_t_xy(t_begin, XyPointCurve(PointList(
        [Point(0.0*mega, 80.0), Point(20.0*mega, 90.0),
         Point(30.0*mega, 95.0), Point(50.0*mega, 100.0),
         Point(160.0*mega, 105.0)
         ])))
    rsv.volume_level_mapping = volume_mapping
    rsv.level.regulation_max.value = TimeSeries(run_ta, 100, stair_case)

    rsv.water_value.endpoint_desc = TimeSeries(run_ta, fill_value=23.5/mega, point_fx=stair_case)
    rsv.level.realised = TimeSeries(run_ta, fill_value=80.0, point_fx=stair_case)
    rsv.inflow.schedule = TimeSeries(run_ta, values=wave_(run_ta, 50.0, 15.0, 0.5), point_fx=stair_case)*0.0  # set to 0.0 for simplicity illustration purposes

    # Upstream reservoir, about rsv.
    rsv2 = hps.create_reservoir(next(ui), "rsv2")
    volume_mapping2 = create_t_xy(t_begin, XyPointCurve(PointList(
        [Point(0.0*mega, 100.0), Point(200.0*mega, 110.0),
         Point(300.0*mega, 115.0), Point(500.0*mega, 120.0),
         Point(1600.0*mega, 125.0)
         ])))
    rsv2.volume_level_mapping = volume_mapping2

    rsv2.water_value.endpoint_desc = TimeSeries(run_ta, fill_value=23.2/mega, point_fx=stair_case)
    rsv2.level.realised = TimeSeries(run_ta, fill_value=100.0, point_fx=stair_case)
    rsv2.inflow.schedule = TimeSeries(run_ta, values=wave_(run_ta, 50.0, 15.0, 0.5),
                                      point_fx=stair_case)*1.0  # set to 0.0 for simplicity illustration purposes

    # Plant
    # with indicated outlet level 10 masl, gives 70..90 masl net head for the units.
    plant = hps.create_power_plant(next(ui), 'plant')
    plant.outlet_level = create_t_double(t_begin, 10.0)
    plant.mip = TimeSeries(run_ta, fill_value=1.0, point_fx=stair_case)
    # Unit
    unit = hps.create_unit(next(ui), "unit")
    plant.add_unit(unit)
    # generator and turbine-description (efficiency and operating range)
    ge = 90.0  # if set to 80, there is a corresponding drop in efficiency
    unit.generator_description = create_t_xy(t_begin,
                                             XyPointCurve(PointList(
                                                 [Point(10.0*mega, ge + 6.0), Point(20.0*mega, ge + 7.2),
                                                  Point(40.0*mega, ge + 8.0), Point(60.0*mega, ge + 9.0),
                                                  Point(80.0*mega, ge + 8.0)])))
    unit.turbine_description = create_t_turbine_description(t_begin,
                                                            [XyPointCurveWithZ(
                                                                XyPointCurve([Point(10.0, 60.0), Point(20.0, 70.0),
                                                                              Point(40.0, 85.0), Point(60.0, 92.0),
                                                                              Point(80.0, 94.0), Point(100.0, 92.0),
                                                                              Point(110.0, 90.0)]),
                                                                70.0
                                                            ), XyPointCurveWithZ(
                                                                XyPointCurve([Point(8.0, 60.0), Point(18.0, 70.0),
                                                                              Point(40.0, 86.0), Point(60.0, 93.0),
                                                                              Point(80.0, 95.0), Point(100.0, 93.0),
                                                                              Point(110.0, 89.0)]),
                                                                110.0
                                                            )]

                                                            )

    # Waterways

    ## Tunnels
    inlet = hps.create_tunnel(next(ui), "tunnel")
    penstock = hps.create_tunnel(next(ui), "penstock")
    tailrace = hps.create_tunnel(next(ui), "tailrace")

    ### loss coeffecients in tunnels (it marks them as tunnels as well)
    inlet.head_loss_coeff = create_t_double(t_begin, 0.000030)
    penstock.head_loss_coeff = create_t_double(t_begin, 0.00005)
    tailrace.head_loss_coeff = create_t_double(t_begin, 0.00001)

    ## Rivers
    bypass = hps.create_river(next(ui), "bypass")
    flood = hps.create_river(next(ui), "flood")
    river = hps.create_river(next(ui), "river")
    river2 = hps.create_tunnel(next(ui), "river-r2-r")
    g2 = river2.add_gate(next(ui), "r2_gate", "controls flow between r2 and r")
    g2.discharge.constraint.max = TimeSeries(run_ta, fill_value=999.0, point_fx=stair_case)
    mt = TimeSeries(run_ta, fill_value=-1.0, point_fx=stair_case)  # group together if less than mt m3/s change, -1 to disable
    mt.set(0, -1)  # turn block merge free at first timestep, just to get a value
    mt.set(1, 10)  # then merge next hours
    mt.set(2, 10)
    mt.set(3, 10)
    # next values are -1, and shop deactivate the block merge.

    g2.discharge.merge_tolerance = mt

    # Set gate and river constraints to trigger discharge group
    gate = bypass.add_gate(next(ui), "bypass_gate", "")
    # bypass.head_loss_coeff = create_t_double(t_begin, 0.00001)  # makes it a tunnel, by def.
    # gate.discharge.schedule = create_const_timeseries(run_ta, 0.0) # stop shop from using the bypass
    # bypass.static_max = create_t_double(t_begin,123.0)

    flood_gate = flood.add_gate(next(ui), "flood_gate", "")
    flood_gate.flow_description = create_t_xyz_list(t_begin, XyPointCurve([Point(100.0, 0.0), Point(105.0, 250.0)]), 0)

    ################ NOTE ####################3
    # Setting discharge reference and contraint.accumulated max, triggers shop_api to add a discharge group
    # for the powerstation and bypass gate upstream. C++ does an iterative search upstreams from river (in this example)
    # until it finds the gate in the bypass, and the powerstation owning the unit. Then adds them to the same discharge group.

    # Connect hps components
    #
    #                     /Flood
    #                   |                             power_station
    #                   |                                  ||
    # r2(gate)->river-> rsv = inlet(wtr) = penstock(wtr) = unit = tailrace(wtr)
    #                   |                                                   |
    #                   |                                                   | river(wtr)
    #                   bypass(wtr) = = = = = = = = = = = = = = = = = = = = |

    river2.input_from(rsv2, ConnectionRole.main).output_to(rsv)
    inlet.input_from(rsv, ConnectionRole.main).output_to(penstock)
    unit.input_from(penstock).output_to(tailrace)
    river.input_from(tailrace)
    bypass.input_from(rsv, ConnectionRole.bypass).output_to(river)
    flood.input_from(rsv, ConnectionRole.flood).output_to(river)
    stm.hydro_power_systems.append(hps)

    return stm


@pytest.mark.skipif(not shop.shyft_with_shop, reason="shyft not build with shop")
def test_gate_merge_tolerance():
    cal = Calendar("Europe/Oslo")
    t_start = cal.time(2021, 1, 1)
    t_end = cal.time(2021, 1, 2)
    dt = cal.HOUR

    n = int((t_end - t_start)//dt)
    time_axis_sim = TimeAxis(t_start, dt, n)
    stm = build_simple_stm_system(run_ta=time_axis_sim)

    run_id = 123
    opts = create_optimization_commands(run_id, write_files=False)
    shop_sys = shop.ShopSystem(time_axis_sim)
    shop_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
    shop_sys.emit(stm)
    for opt in opts:
        shop_sys.commander.execute(opt)
    shop_sys.collect(stm)
    shop_sys.complete(stm)

    hps = stm.hydro_power_systems[0]
    rsv = hps.reservoirs[0]
    rsv2 = hps.reservoirs[1]
    r2 = rsv2.downstreams[0].target
    bypass = rsv.downstreams[1].target
    river = bypass.downstreams[0].target
    flood = rsv.downstreams[2].target
    unit = hps.units[0]
    print()
    show_ts(title="rsv2.volume                  ", ta=time_axis_sim, ts=rsv2.volume.result, scale=1/1e6)
    show_ts(title="rsv2.level                   ", ta=time_axis_sim, ts=rsv2.level.result, scale=1)
    show_ts(title="rsv2.inflow   m3/s           ", ta=time_axis_sim, ts=rsv2.inflow.schedule, scale=1)
    show_ts(title="river2.act                   ", ta=time_axis_sim, ts=r2.discharge.result, scale=1)
    show_ts(title="rsv.volume                   ", ta=time_axis_sim, ts=rsv.volume.result, scale=1/1e6)
    show_ts(title="rsv.level                    ", ta=time_axis_sim, ts=rsv.level.result, scale=1)
    show_ts(title="rsv.inflow   m3/s            ", ta=time_axis_sim, ts=rsv.inflow.schedule, scale=1)
    show_ts(title="price                        ", ta=time_axis_sim, ts=stm.market_areas[0].price, scale=1.e6)
    show_ts(title="unit.production schedul MW   ", ta=time_axis_sim, ts=unit.production.schedule, scale=1/1e6)
    show_ts(title="unit.production MW           ", ta=time_axis_sim, ts=unit.production.result, scale=1/1e6)
    show_ts(title="unit.discharge               ", ta=time_axis_sim, ts=unit.discharge.result, scale=1)

    show_ts(title="flood.discharge              ", ta=time_axis_sim, ts=flood.discharge.result, scale=1)
    show_ts(title="flood.discharge   acc K flux ", ta=time_axis_sim, ts=flood.discharge.result, scale=1/1e3, accumulate=True)
    show_ts(title="bypass.discharge             ", ta=time_axis_sim, ts=bypass.discharge.result, scale=1)
    show_ts(title="bypass.discharge acc Mm3     ", ta=time_axis_sim, ts=bypass.discharge.result, scale=1/1e6, accumulate=True)
    show_ts(title="river.act                    ", ta=time_axis_sim, ts=river.discharge.result, scale=1)
    if stm.summary:
        print(f"\n {stm.summary}")


    expected_river_discharge = np.array(
        [50.0, 50.0, 50.0, 50.0, 69.1, 59.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 175.6, 333.0, 64.5, 63.9, 63.0, 61.9, 60.6, 0.0, 0.0, 0.0, 0.0, 278.3]
    )

    assert np.allclose(expected_river_discharge[:4], r2.discharge.result.value.values[:4], atol=0.2)
    # note: these values was computed by shop, notice the flat 50.0 in the beginning that demo's the merge tolerance
    #


@pytest.mark.skipif(not shop.shyft_with_shop, reason="shyft not build with shop")
def test_gate_ramp_cost():

    def river_discharge(ramp_cost: float | None):
        cal = Calendar("Europe/Oslo")
        t_start = cal.time(2021, 1, 1)
        t_end = cal.time(2021, 1, 2)
        dt = cal.HOUR

        n = int((t_end - t_start)//dt)
        time_axis_sim = TimeAxis(t_start, dt, n)
        stm = build_simple_stm_system(run_ta=time_axis_sim)
        if ramp_cost:
            stm.custom[shop.gate_ramp_cost_url] = ramp_cost

        run_id = 123
        opts = create_optimization_commands(run_id, write_files=False)
        shop_sys = shop.ShopSystem(time_axis_sim)
        shop_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
        shop_sys.emit(stm)
        for opt in opts:
            shop_sys.commander.execute(opt)
        shop_sys.collect(stm)
        shop_sys.complete(stm)

        hps = stm.hydro_power_systems[0]
        rsv = hps.reservoirs[0]
        rsv2 = hps.reservoirs[1]
        r2 = rsv2.downstreams[0].target
        bypass = rsv.downstreams[1].target
        river = bypass.downstreams[0].target
        flood = rsv.downstreams[2].target
        unit = hps.units[0]
        return r2.discharge.result.value.values


    d0 = river_discharge(None)
    d1 = river_discharge(2e5)

    assert not np.allclose(d0, d1, atol=0.2)


@pytest.mark.skipif(not shop.shyft_with_shop, reason="shyft not build with shop")
def test_gate_ramping():
    cal = Calendar("Europe/Oslo")
    t_start = cal.time(2021, 1, 1)
    t_end = cal.time(2021, 1, 2)
    dt = cal.HOUR

    n = int((t_end - t_start)//dt)
    time_axis = TimeAxis(t_start, dt, n)
    stm = build_simple_stm_system(run_ta=time_axis)
    hps = stm.hydro_power_systems[0]

    bypass = hps.find_waterway_by_name("bypass")
    bypass_gate = bypass.add_gate(bypass.id, "bypass-gate")
    bypass.discharge.constraint.ramping_up.value = TimeSeries(time_axis, 5, stair_case)
    bypass.discharge.constraint.ramping_down.value = TimeSeries(time_axis, 10, stair_case)

    # Add high filling level and inflow to force bypass
    rsv = hps.reservoirs[0]
    rsv.level.realised.value.set(rsv.level.realised.value.index_of(t_start), rsv.level.regulation_max.value(t_start) - 0.1)
    rsv.inflow.schedule.value = TimeSeries(time_axis, 225, stair_case)

    opts = create_optimization_commands(123, write_files=False)
    shop_sys = shop.ShopSystem(time_axis)
    shop_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
    shop_sys.emit(stm)
    for opt in opts:
        shop_sys.commander.execute(opt)
    shop_sys.collect(stm)
    shop_sys.complete(stm)

    assert np.sum(bypass_gate.discharge.result.value.values) > 0
    tol = 0.01

    # Ramping up:
    assert np.all(bypass_gate.discharge.result.value.values[1:] - bypass_gate.discharge.result.value.values[:-1] < 5 + tol)
    # Ramping down:
    assert np.all(bypass_gate.discharge.result.value.values[:-1] - bypass_gate.discharge.result.value.values[1:] < 10 + tol)
