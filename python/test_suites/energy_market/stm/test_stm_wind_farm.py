import pytest

from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from functools import reduce
from shyft.energy_market.stm import StmSystem, t_xy, WindTurbine
from shyft.time_series import TimeSeries, GeoPoint, time
from shyft.energy_market.core import XyPointCurve, Point, PointList


def test_wind_farm_attributes():
    sys = StmSystem(1, "A", "{}")
    wf = sys.create_wind_farm(1, "Wind Park #1")
    # generics, from id_base
    assert hasattr(wf, "id")
    assert hasattr(wf, "name")
    assert hasattr(wf, "json")
    assert hasattr(wf, "location")
    assert hasattr(wf, "height")
    assert hasattr(wf, "epsg")

    # specifics
    assert hasattr(wf, "production")
    assert hasattr(wf, "reserve")
    assert hasattr(wf, "cost")
    for _attr in ["break_even"]:
        assert hasattr(wf.cost, _attr)

    for _attr in ["constraint", "forecast", "schedule", "realised", "result"]:
        assert hasattr(wf.production, _attr)
    for _attr in ["min", "max"]:
        assert hasattr(wf.production.constraint, _attr)
    for pair in ["fcr_n", "mfrr", "fcr_d"]:
        for spec in ["up", "down"]:
            for _attr in ["min", "max", "cost", "result", "penalty", "realised"]:
                assert hasattr(reduce(getattr, [pair, spec], wf.reserve), _attr)

    assert str(wf)
    assert repr(wf)
    assert wf.tag == "/w1"


def test_wind_farm_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    wf = sys.create_wind_farm(1, "Wind Farm #1")
    assert wf.flattened_attributes()
    for (path, val) in wf.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), wf)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()


def test_wind_farm_equality():
    sys1 = StmSystem(1, "A", "{}")
    wt1 = sys1.create_wind_farm(1, "Wind Farm #1")
    sys2 = StmSystem(2, "B", "{}")
    wt2 = sys2.create_wind_farm(1, "Wind Farm #1")
    wt3 = sys2.create_wind_farm(99, "other Wind Farm #1")

    assert len(sys2.wind_farms) == 2
    assert wt1 == wt1
    assert wt1 == wt2
    assert wt1 != wt3
    wt1.production.forecast = TimeSeries('shyft://stm/windfarm_forecast')
    assert wt1 != wt2
    wt2.production.forecast = wt1.production.forecast
    assert wt1 == wt2
    wt1.location = GeoPoint(1, 2, 3)
    assert wt1 != wt2
    wt2.location = GeoPoint(1, 2, 3)
    assert wt1 == wt2


def test_wind_turbine_attributes():
    sys = StmSystem(1, "A", "{}")
    wf = sys.create_wind_farm(1, "Wind Park #1")
    height = TimeSeries("shyft://some/ts_or_constant")
    t0 = time('2019-01-01T00:00:00Z')
    power_curve = t_xy()
    power_curve[t0] = XyPointCurve(PointList([Point(10, 1), Point(12, 2), Point(13, 2)]))
    wt = wf.create_wind_turbine(id=1, name="1", json="{}", location=GeoPoint(1, 2, 3), height=height,
                                power_curve=power_curve)

    # generics, from id_base
    assert hasattr(wt, "id")
    assert hasattr(wt, "name")
    assert hasattr(wt, "json")
    assert hasattr(wt, "location")
    assert hasattr(wt, "power_curve")
    assert hasattr(wt, "height")

    # specifics
    assert hasattr(wt, "production")
    assert hasattr(wt, "reserve")
    assert hasattr(wt, "cost")
    for _attr in ["start", "stop"]:
        assert hasattr(wt.cost, _attr)

    for _attr in ["schedule", "realised", "result"]:
        assert hasattr(wt.production, _attr)
    assert hasattr(wt.production, "constraint")
    for _attr in ["min", "max"]:
        assert hasattr(wt.production.constraint, _attr)

    for pair in ["fcr_n", "mfrr", "fcr_d"]:
        for spec in ["up", "down"]:
            for _attr in ["min", "max", "cost", "result", "penalty", "realised"]:
                assert hasattr(reduce(getattr, [pair, spec], wt.reserve), _attr)

    assert str(wt)
    assert repr(wt)
    assert wt.tag == "/w1/T1"


def test_wind_farm_add_wind_turbine():
    sys = StmSystem(1, "A", "{}")
    wf = sys.create_wind_farm(1, "Wind Park #1")
    height = TimeSeries("shyft://some/ts_or_constant")
    t0 = time('2019-01-01T00:00:00Z')
    power_curve = t_xy()
    power_curve[t0] = XyPointCurve(PointList([Point(10, 1), Point(12, 2), Point(13, 2)]))
    wt = wf.create_wind_turbine(id=1, name="1", json="{}", location=GeoPoint(1, 2, 3), height=height,
                                power_curve=power_curve)
    assert len(wf.wind_turbines) == 1
    assert wt == wf.wind_turbines[0]
    wt2 = wf.create_wind_turbine(id=2, name="2", json="{}", location=GeoPoint(3, 2, 3), height=height,
                                 power_curve=power_curve)
    assert len(wf.wind_turbines) == 2
    assert wt2 == wf.wind_turbines[1]
    wts = [WindTurbine(uid=idx, name=f'WT-{idx}', json='{}') for idx in [3, 4, 5]]
    assert wts
    wf.add_wind_turbines(wts)
    assert len(wf.wind_turbines) == 5
