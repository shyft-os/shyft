import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)

import shyft.energy_market.stm as stm
from typing import Optional, Any, Union
from .models import create_complete_stm_system


def test_url_planning_in_out():
    """Test that url getters return an expected number of items, and
    that randomly selected urls have the expected format"""

    model: stm.StmSystem = create_complete_stm_system()

    input_urls = stm.url_planning_inputs("42", model)
    assert len(input_urls) == 1114
    assert input_urls[0] == "dstm://M42/H1/R16606.inflow.realised"

    output_urls = stm.url_planning_outputs("42", model)
    assert len(output_urls) == 377
    assert output_urls[0] == "dstm://M42/H1/R16606.inflow.result"


def test_get_set_attr():
    """Test that setting then getting an attribute works as expected"""

    model: stm.StmSystem = create_complete_stm_system()
    id: str = "42"
    url = f'dstm://M{id}.run_params.head_opt'

    head_opt: Union[Any, stm.UrlResolveError] = model.get_attr(url)
    assert head_opt is False

    res: Optional[stm.UrlResovleError] = model.set_attr(url, True)
    assert res is None

    head_opt: Union[Any, stm.UrlResolveError] = model.get_attr(url)
    assert head_opt is True
