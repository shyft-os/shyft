from shyft.energy_market import stm
import pytest


@pytest.fixture(scope="session")
def simple_stm_hps():
    hps = stm.HydroPowerSystem(1, "simple hps")
    r = hps.create_reservoir(1, "simple_res", "")
    u = hps.create_unit(1, "simple_unit")
    tun = hps.create_tunnel(1, "r->u", "")

    tun.input_from(r).output_to(u)

    pp = hps.create_power_plant(2, "simple_pp", "")
    pp.add_unit(u)
    return hps


@pytest.fixture(scope="session")
def simple_stm_system(simple_stm_hps):
    sys = stm.StmSystem(1, "Test STM system", "")
    sys.hydro_power_systems.append(simple_stm_hps)
    yield sys
