import pytest
from shyft.energy_market.stm import shyft_with_stm

if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)

import sys

websockets = pytest.importorskip("websockets")
import asyncio
from shyft.energy_market import stm
from shyft import time_series as sa
from time import sleep

if sys.version_info < (3, 7):
    pytest.skip("requires Python3.7 or higher", allow_module_level=True)


def uri(port_num):
    return f"ws://127.0.0.1:{port_num}"


def get_response(req: str, port_no: int):
    async def wrap(wreq):
        async with websockets.connect(uri(port_no)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(wrap(req))


def test_web_api(tmpdir):
    # ensure there is an event loop for this thread
    loop = asyncio.new_event_loop()
    fx_list = []

    def my_fx(mid: int, args: str):
        fx_list.append((mid, args))  # just record the args for testing it
        # raise RuntimeError(f"Did not work as expected {mid}, args={args}")
        return isinstance(mid, int) and isinstance(args, str)

    case_args = '{"run_case": "case 1"}'
    web_args = "{somestring}"
    root_dir = (tmpdir/"t_web_api")
    srv = stm.StmTaskServer(str(root_dir), sa.ServerConfig(stale_duration=sa.time(60.0),stale_sweep_interval=sa.time(0.5)))
    assert not srv.fx
    srv.fx = my_fx
    port = srv.start_server()
    # web_api_port
    c = stm.StmTaskClient(host_port=f"localhost:{port}", timeout_ms=1000, operation_timeout_ms=2000)
    assert srv
    assert c
    assert c.operation_timeout_ms == 2000
    assert c.timeout_ms == 1000
    c.operation_timeout_ms = 1000
    assert c.operation_timeout_ms == 1000
    # Store a simple session
    # Start web API:
    host = "127.0.0.1"
    web_api_port = srv.start_web_api(host, 0, str(root_dir + "/web"), 10, 10)
    sleep(0.5)
    # do all requests via the web api, including creating fresh task model.
    # please pay attention to the fact that there is strict key ordering in the json
    # when it comes to models,model-info, objects, etc.
    qa_tuples = [
        ("""store_model {
                "request_id":"01",
                "model":{
                    "id":1,
                    "name":"s1",
                    "created":10.0,
                    "json":"",
                    "labels":["test","web"],
                    "base_model": {
                        "host":"superserver.shyft.org",
                        "port_num":1234,
                        "api_port_num":1235,
                        "model_key":"s1",
                        "stm_id":123,
                        "labels":["template","operative"]
                    }
                },
                "model_info":{
                    "id":1,
                    "name":"s1",
                    "created":10.0
                }
            }""",
         """{"request_id":"01","result":1}"""),
        ("""get_model_infos {"request_id": "1"}""", """{"request_id":"1","result":[{"id":1,"name":"s1","created":10.0,"json":""}]}"""),
        ("""read_model {"request_id": "2", "model_id": 1}""",
         """{"request_id":"2","result":{"id":1,"name":"s1","created":10.0,"json":"","labels":["test","web"],"cases":[],"base_model":{"host":"superserver.shyft.org","port_num":1234,"api_port_num":1235,"model_key":"s1","stm_id":123,"labels":["template","operative"]},"task_name":""}}"""),
        ("""fx {"request_id": "3", "model_id": 1, "args":"{somestring}"}""", """{"request_id":"3","result":true}"""),
        ("""read_model {"request_id": "4", "model_id": 17}""",
         """{"request_id":"4","diagnostics":"rocksdb::failed:NotFound: "}""")
    ]
    try:
        for qa in qa_tuples:
            response = get_response(qa[0], web_api_port)
            assert response == qa[1], 'expected response'
    finally:
        srv.stop_web_api()
    c.fx(1, case_args)
    assert len(fx_list) == 2
    assert fx_list == [(1, web_args),(1, case_args)]


def test_web_api_error(tmpdir):
    loop = asyncio.new_event_loop()

    somestr="xczxczxxzczxcpzxzxc-z08s-d8fas"
    def callback(mid: int, args: str):
        raise RuntimeError(somestr)

    root_dir = (tmpdir/"t_web_api")
    srv = stm.StmTaskServer(str(root_dir), sa.ServerConfig(stale_duration=sa.time(60.0),stale_sweep_interval=sa.time(0.5)))
    assert not srv.fx
    srv.fx = callback
    port = srv.start_server()
    assert srv
    host = "127.0.0.1"
    web_api_port = srv.start_web_api(host, 0, str(root_dir + "/web"), 10, 10)
    sleep(0.5)
    try:
        request = """fx {"request_id": "3", "model_id": 1, "args":""}"""
        response = get_response(request, web_api_port)
        assert somestr in response
    finally:
        srv.stop_web_api()
