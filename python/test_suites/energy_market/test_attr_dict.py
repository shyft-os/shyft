from typing import Any

import pytest
import shyft.energy_market as em
import shyft.time_series as sts
from shyft.energy_market import (
    AnyAttrDict,
    TsAttrDict,
    XyzPointCurveDict,
    AreaDict,
    PowerModuleDict,
)


@pytest.fixture(
    params=[
        False,
        True,
        "this is a string",
        1.2345,
        12345,
        sts.TimeSeries("ts-name"),
        sts.TsVector([sts.TimeSeries("ts-name")]),
        sts.TimeAxis([1, 2, 3]),
        sts.GeoPoint(1.0, 2.0, 3.0),
        # em.UnitGroupType(123),  # int type when read?
        em.t_turbine_description(),
        em.t_xy(),
    ],
)
def valid_any_attr_val(request: pytest.FixtureRequest) -> Any:
    """Return a valid custom value for AnyAttrDict."""
    return request.param


@pytest.fixture(
    params=[
        sts.TimeSeries("ts-name"),
    ],
)
def valid_ts_attr_val(request: pytest.FixtureRequest) -> sts.TimeSeries:
    """Return a valid value for TsAttrDict."""
    return request.param


@pytest.fixture(
    params=[
        em.XyPointCurve(),
    ],
)
def valid_xyz_point_curve_val(request: pytest.FixtureRequest) -> Any:
    """Return a valid value for XyzPointCurveDict."""
    return request.param


@pytest.fixture(
    params=[
        em.ModelArea(
            model=em.Model(id=123, name="model name"),
            id=123,
            name="model area name",
        )
    ],
)
def valid_area_val(request: pytest.FixtureRequest) -> Any:
    """Return a valid value for AreaDict."""
    return request.param


@pytest.fixture(
    params=[
        em.PowerModule(
            em.ModelArea(
                model=em.Model(id=123, name="model name"),
                id=123,
                name="model area name",
            ),
            id=456,
            name="power model name",
        )
    ],
)
def valid_power_module_val(request: pytest.FixtureRequest) -> Any:
    """Return a valid value for PowerModuleDict."""
    return request.param


# Fixtures for Invalid Values
@pytest.fixture(
    params=[
        {"ts": sts.TimeSeries("ts-name")},
        ["list", "of", "strings"],
    ],
)
def invalid_any_val(request: pytest.FixtureRequest) -> Any:
    return request.param


@pytest.fixture(
    params=[
        {"ts": sts.TimeSeries("ts-name")},
        ["list", "of", "strings"],
        False,
        True,
        "String",
        123,
        123.456,
    ],
)
def invalid_ts_val(request: pytest.FixtureRequest) -> Any:
    return request.param


@pytest.fixture(
    params=[
        {"curve": "invalid_curve_data"},  # Invalid type for XyzPointCurveDict
        ["invalid", "list"],
        999,
    ],
)
def invalid_xyz_point_curve_val(request: pytest.FixtureRequest) -> Any:
    return request.param


@pytest.fixture(
    params=[
        {"area": "invalid_area_data"},  # Invalid type for AreaDict
        ["invalid", "list"],
        999,
    ],
)
def invalid_area_val(request: pytest.FixtureRequest) -> Any:
    return request.param


@pytest.fixture(
    params=[
        {"module": "invalid_module_data"},  # Invalid type for PowerModuleDict
        ["invalid", "list"],
        999,
    ],
)
def invalid_power_module_val(request: pytest.FixtureRequest) -> Any:
    return request.param


# # To deduplicate the below suite of tests could be simplified with pytest-lazy-fixture:
# # Eg:
# import pytest
#
# @pytest.mark.parametrize(
#     "dict_class, val",
#     [
#         (AnyAttrDict, lazy_fixture("valid_any_attr_val")),
#         (TsAttrDict, lazy_fixture("valid_ts_attr_val")),
#         (XyzPointCurveDict, lazy_fixture("valid_xyz_point_curve_val")),
#         (AreaDict, lazy_fixture("valid_area_val")),
#         (PowerModuleDict, lazy_fixture("valid_power_module_val")),
#     ],
# )
# def test_get_valid(dict_class, val):
#     dict_instance = dict_class()
#     dict_instance["key"] = val
#     assert dict_instance.get("key") == val


# Test cases for AnyAttrDict


def test_set_invalid_any_attr_dict(invalid_any_val):
    dict_instance = AnyAttrDict()
    with pytest.raises(TypeError):
        dict_instance["key"] = invalid_any_val


def test_set_and_get_any_attr_dict(valid_any_attr_val):
    dict_instance = AnyAttrDict()
    dict_instance["key1"] = valid_any_attr_val
    dict_instance["key2"] = valid_any_attr_val
    assert dict_instance.get("key1") == valid_any_attr_val
    assert dict_instance.get("key2") == valid_any_attr_val
    assert dict_instance.get("key3") is None
    assert dict_instance.get("key3", valid_any_attr_val) == valid_any_attr_val


def test_delete_key_any_attr_dict(valid_any_attr_val):
    dict_instance = AnyAttrDict()
    dict_instance["key"] = valid_any_attr_val
    del dict_instance["key"]
    assert "key" not in dict_instance


# Test Cases for TsAttrDict


def test_set_invalid_ts_attr_dict(invalid_ts_val):
    dict_instance = TsAttrDict()
    with pytest.raises(TypeError):
        dict_instance["key"] = invalid_ts_val


def test_set_and_get_ts_attr_dict(valid_ts_attr_val):
    dict_instance = TsAttrDict()
    dict_instance["key1"] = valid_ts_attr_val
    dict_instance["key2"] = valid_ts_attr_val
    assert dict_instance.get("key1") == valid_ts_attr_val
    assert dict_instance.get("key2") == valid_ts_attr_val
    assert dict_instance.get("key3") is None
    assert dict_instance.get("key3", valid_ts_attr_val) == valid_ts_attr_val


def test_delete_key_ts_attr_dict(valid_ts_attr_val):
    dict_instance = TsAttrDict()
    dict_instance["key"] = valid_ts_attr_val
    del dict_instance["key"]
    assert "key" not in dict_instance


# Test Cases for XyzPointCurveDict


def test_set_invalid_xyz_point_curve_dict(invalid_xyz_point_curve_val):
    dict_instance = XyzPointCurveDict()
    with pytest.raises(TypeError):
        dict_instance[1.1] = invalid_xyz_point_curve_val


def test_set_and_get_xyz_point_curve_dict(valid_xyz_point_curve_val):
    dict_instance = XyzPointCurveDict()
    dict_instance[1.1] = valid_xyz_point_curve_val
    dict_instance[1.2] = valid_xyz_point_curve_val
    assert dict_instance.get(1.1) == valid_xyz_point_curve_val
    assert dict_instance.get(1.2) == valid_xyz_point_curve_val
    assert dict_instance.get(1.3) is None
    assert (
        dict_instance.get(1.3, valid_xyz_point_curve_val) == valid_xyz_point_curve_val
    )


def test_delete_key_xyz_point_curve_dict(valid_xyz_point_curve_val):
    dict_instance = XyzPointCurveDict()
    dict_instance[1.1] = valid_xyz_point_curve_val
    del dict_instance[1.1]
    assert 1.1 not in dict_instance


# Test Cases for AreaDict


def test_set_invalid_area_dict(invalid_area_val):
    dict_instance = AreaDict()
    with pytest.raises(TypeError):
        dict_instance[1] = invalid_area_val


def test_set_and_get_area_dict(valid_area_val):
    dict_instance = AreaDict()
    dict_instance[1] = valid_area_val
    dict_instance[2] = valid_area_val
    assert dict_instance.get(1) == valid_area_val
    assert dict_instance.get(2) == valid_area_val
    assert dict_instance.get(3) is None
    assert dict_instance.get(3, valid_area_val) == valid_area_val


def test_delete_key_area_dict(valid_area_val):
    dict_instance = AreaDict()
    dict_instance[1] = valid_area_val
    del dict_instance[1]
    assert 1 not in dict_instance


# Test Cases for PowerModuleDict


def test_set_invalid_power_module_dict(invalid_power_module_val):
    dict_instance = PowerModuleDict()
    with pytest.raises(TypeError):
        dict_instance["key"] = invalid_power_module_val


def test_set_and_get_power_module_dict(valid_power_module_val):
    dict_instance = PowerModuleDict()
    dict_instance[1] = valid_power_module_val
    dict_instance[2] = valid_power_module_val
    assert dict_instance.get(1) == valid_power_module_val
    assert dict_instance.get(2) == valid_power_module_val
    assert dict_instance.get(3) is None
    assert dict_instance.get(3, valid_power_module_val) == valid_power_module_val


def test_delete_key_power_module_dict(valid_power_module_val):
    dict_instance = PowerModuleDict()
    dict_instance[1] = valid_power_module_val
    del dict_instance[1]
    assert 1 not in dict_instance
