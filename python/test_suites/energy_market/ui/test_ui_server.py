import pytest

websockets = pytest.importorskip("websockets")
import asyncio
from time import sleep

from shyft.time_series import time
from shyft.energy_market import ui

if not ui.shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft import time_series as sa
import json

def uri(port_num):
    return f"ws://127.0.0.1:{port_num}"

def get_response(req: str, port_no: int):
    async def wrap(wreq):
        async with websockets.connect(uri(port_no)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(wrap(req))

def generate_json(name: str, args: str):
    if name == "test":
        res = {}
        res["name"] = "generated_layout"
        res["widget"] = {"misc": "data"}
        return json.dumps(res)
    else:
        return ""


def test_server(tmpdir):
    root_dir = (tmpdir/"t_ui_srv")
    s = ui.LayoutServer(str(root_dir), config=sa.ServerConfig(stale_duration=time(60.0),stale_sweep_interval=time(0.5)))
    s.fx = generate_json
    pn = s.start_server()
    c = ui.LayoutClient(host_port="localhost:" + str(pn), timeout_ms=1000, operation_timeout_ms=400)
    assert c.operation_timeout_ms == 400
    assert c.timeout_ms == 1000

    assert len(c.get_model_infos([])) == 0
    # 1. Store a model:
    li = ui.LayoutInfo(1, "Laye oeuitte", "")
    mi = ui.ModelInfo(li.id, li.name, sa.utctime_now(), "")
    c.store_model(li, mi)
    assert len(c.get_model_infos([])) == 1

    # 2. Read model
    li2 = c.read_model(1)
    assert li == li2
    li2v = c.read_models([1])
    assert len(li2v) == 1

    # 4. Generate layout from fx
    li3 = c.read_model_with_args(-1, "test", "{misc.}", True)
    assert li3.id == 2
    assert li3.name == "test"
    assert li3.json == generate_json("test", "{misc.}")
    li4 = c.read_model_with_args(-1, "test", "{misc.}", False)
    assert li4.id == -1
    assert li4.name == li3.name
    assert li4.json == li3.json
    assert len(c.get_model_infos([])) == 2

    # 3. Remove model
    c.remove_model(1)
    assert len(c.get_model_infos([])) == 1


def test_web_api_error(tmpdir):
    loop = asyncio.new_event_loop()

    somestr="xczxczxxzczxcpzxzxc-z08s-d8fas"
    def callback(mid: int, args: str):
        raise RuntimeError(somestr)

    root_dir = (tmpdir/"t_web_api")
    srv = ui.LayoutServer(str(root_dir), sa.ServerConfig(stale_duration=sa.time(60.0),stale_sweep_interval=sa.time(0.5)))
    assert not srv.fx
    srv.fx = callback
    port = srv.start_server()
    assert srv
    host = "127.0.0.1"
    web_api_port = srv.start_web_api(host, 0, str(root_dir + "/web"), 10, 10)
    sleep(0.5)
    try:
        request = """read_layout {"request_id": "3", "layout_id": 1, "args":"vbxcvbxcvbxc"}"""
        response = get_response(request, web_api_port)
        assert somestr in response
    finally:
        srv.stop_web_api()
