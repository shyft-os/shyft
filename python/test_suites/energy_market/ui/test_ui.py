from shyft.energy_market import ui
import pytest
if not ui.shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)

from shyft.energy_market.ui import Application, Aligment
from shyft.energy_market import ui as _ui
from shyft.time_series import time

import random
import json


@pytest.fixture(scope="session")
def get_app():
    return Application()


def test_simple_widget(get_app):
    app = get_app
    window = app.widget()
    window.setObjectName("ObjectName")
    window.setProperty("PropertyName", "PropertyValue")
    window.setToolTip("ToolTip")
    window.setStatusTip("StatusTip")
    window.setWhatsThis("WhatsThis")
    window.setWindowRole("WindowRole")
    window.setWindowTitle('WindowTitle')
    window.setAccessibleName("AccessibleName")
    window.setAccessibleDescription("AccessibleDescription")
    window.setStyleSheet("StyleSheet")
    cfg = _ui.export_qt(window)
    assert cfg
    print(cfg)
    jwindow = json.loads(cfg)
    # json.dumps(jwindow, indent=2  )
    assert jwindow
    assert all(key in ('id', 'properties', 'toolTip', 'statusTip', 'whatsThis', 'caption', 'role', 'accessibleName',
                       'accessibleDescription', 'styleSheet', 'type') for key in jwindow)
    assert jwindow.get('id') == 'ObjectName'
    p = jwindow.get('properties')
    assert p
    assert 'PropertyName' in p
    assert p.get('PropertyName') == 'PropertyValue'
    assert jwindow.get('toolTip') == 'ToolTip'
    assert jwindow.get('statusTip') == 'StatusTip'
    assert jwindow.get('whatsThis') == 'WhatsThis'
    assert jwindow.get('caption') == 'WindowTitle'
    assert jwindow.get('role') == 'WindowRole'
    assert jwindow.get('accessibleName') == 'AccessibleName'
    assert jwindow.get('accessibleDescription') == 'AccessibleDescription'
    assert jwindow.get('styleSheet') == 'StyleSheet'


def test_table_widget(get_app):
    # First create a 10x3 table, without content
    app = get_app
    window = app.widget()
    table = app.tableWidget(window)
    table.setRowCount(10)
    table.setColumnCount(3)
    tableHeader = ["A", "B", "C"]
    table.setHorizontalHeaderLabels(tableHeader)
    table.setVerticalHeaderVisible(False)
    table.setShowGrid(False)
    cfg = _ui.export_qt(window)
    assert cfg
    jwindow = json.loads(cfg)
    # json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtable = jwidgets[0]
    assert all(key in jtable for key in ('type', 'rowCount', 'columnCount'))
    assert jtable.get('type') == 'table'
    assert jtable.get('rowCount') == 10
    assert jtable.get('columnCount') == 3
    # Then fill with some data, but only 2 of 10 rows
    table.setItem(0, 0, app.tableWidgetItem("Item 0.0"))
    table.setItem(0, 1, app.tableWidgetItem("Item 0.1"))
    table.setItem(0, 2, app.tableWidgetItem("Item 0.2"))
    table.setItem(1, 0, app.tableWidgetItem("Item 1.0"))
    table.setItem(1, 1, app.tableWidgetItem("Item 1.1"))
    table.setItem(1, 2, app.tableWidgetItem("Item 1.2"))
    cfg = _ui.export_qt(window)
    assert cfg
    jwindow = json.loads(cfg)
    # json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'widgets' in jwindow
    jtable = jwindow.get('widgets')[0]
    assert all(key in jtable for key in ('type', 'rowCount', 'columnCount', 'rows'))
    assert jtable.get('type') == 'table'
    assert jtable.get('rowCount') == 10
    assert jtable.get('columnCount') == 3
    jrows = jtable.get('rows')
    assert len(jrows) == 2  # Note: Only the 2 rows with data emitted, not all 10 given by rowCount!
    jrow = jrows[0]
    assert len(jrow) == 3
    jcell = jrow[0]
    assert all(key in jcell for key in ('value',))
    assert jcell.get('value') == 'Item 0.0'
    jcell = jrow[1]
    assert all(key in jcell for key in ('value',))
    assert jcell.get('value') == 'Item 0.1'
    jcell = jrow[2]
    assert all(key in jcell for key in ('value',))
    assert jcell.get('value') == 'Item 0.2'


def test_chart_widget(get_app):
    app = get_app
    fill_data = True
    window = app.widget()
    axis_x = app.dateTimeAxis()
    axis_x.setTickCount(10)
    axis_x.setFormat("dd.MM (h:mm)")
    axis_x.setTitleText("This is a datetime axis")
    axis_y = app.valueAxis()
    axis_y.setTickCount(10)
    axis_y.setLabelFormat("%.2f")
    axis_y.setTitleText("This is a value axis")
    axis_y.setMin(10)
    axis_y.setMax(100)
    serie1 = app.lineSeries()
    serie1.setName("This is a series")
    if fill_data:
        t = time()
        for _ in range(24):
            v = random.randrange(10, 100)
            serie1.append(float(_ui.toMSecsSinceEpoch(t)), v)
            t = _ui.addSecs(t, 3600)
    chart = app.chart()
    chart.setTitle("This is a chart")
    chart.addAxis(axis_x, Aligment.AlignBottom)
    chart.addAxis(axis_y, Aligment.AlignLeft)
    chart.addSeries(serie1)
    serie1.attachAxis(axis_x)
    serie1.attachAxis(axis_y)
    chartView = app.chartView(chart, window)
    cfg = _ui.export_qt(window)
    assert cfg
    jwindow = json.loads(cfg)
    # json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtable = jwidgets[0]
    assert all(key in jtable for key in ('type', 'title', 'axes', 'series'))
    assert jtable.get('type') == 'chart'
    assert jtable.get('title') == 'This is a chart'
    jaxes = jtable.get('axes')
    assert len(jaxes) == 2
    jaxis = jaxes[0]
    assert all(key in jaxis for key in ('type', 'title', 'orientation', 'min', 'max', 'tickCount', 'format'))
    assert jaxis.get('type') == 'dateTime'
    assert jaxis.get('orientation') == 'horizontal'
    assert jaxis.get('title') == 'This is a datetime axis'
    jaxis = jaxes[1]
    assert all(
        key in jaxis for key in ('type', 'title', 'orientation', 'min', 'max', 'tickType', 'tickCount', 'format'))
    assert jaxis.get('type') == 'value'
    assert jaxis.get('orientation') == 'vertical'
    assert jaxis.get('title') == 'This is a value axis'
    jseries = jtable.get('series')
    assert len(jseries) == 1
    jserie = jseries[0]
    assert all(key in jserie for key in ('type', 'name', 'attachedAxes'))
    assert jserie.get('type') == 'line'
    assert jserie.get('name') == 'This is a series'
    jaa = jserie.get('attachedAxes')
    assert len(jaa) == 2
    assert jaa[0] == 0
    assert jaa[1] == 1


def test_pushbutton_menu(get_app):
    app = get_app
    window = app.widget()
    layout = app.vBoxLayout()
    window.setLayout(layout)

    # Button 1
    button1 = app.pushButton(window)
    button1.setText("This is a plain PushButton")
    layout.addWidget(button1)

    # Button 2
    button2 = app.pushButton(window)
    button2.setText("This is a PushButton with menu")
    layout.addWidget(button2)

    # Top level menu on button 2
    button2_menu = app.menu()
    button2.setMenu(button2_menu)

    # Top level menu entries
    button2_menu_entry1 = app.action("Meny entry 1", window)
    button2_menu.addAction(button2_menu_entry1)
    button2_menu_entry2 = app.action("Meny entry 2", window)
    button2_menu.addAction(button2_menu_entry2)

    # Sub menu of menu entry 1
    button2_menu_sub = app.menu()
    button2_menu_entry1.setMenu(button2_menu_sub)

    # Sub menu entries
    button2_menu_entry1_sub1 = app.action("Submenu entry A", window)
    button2_menu_sub.addAction(button2_menu_entry1_sub1)
    button2_menu_entry1_sub2 = app.action("Submenu entry B", window)
    button2_menu_sub.addAction(button2_menu_entry1_sub2)
    button2_menu_entry1_sub3 = app.action("Submenu entry C", window)
    button2_menu_sub.addAction(button2_menu_entry1_sub3)

    # Sub-sub menu of sub-menu entry 2
    button2_menu_sub_sub = app.menu()
    button2_menu_entry1_sub2.setMenu(button2_menu_sub_sub)
    button2_menu_entry1_sub2_entry1 = app.action("Subsubmenu entry i", window)
    button2_menu_sub_sub.addAction(button2_menu_entry1_sub2_entry1)

    cfg = _ui.export_qt(window)
    assert cfg
    jwindow = json.loads(cfg)
    # json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'layout' in jwindow
    jlayout = jwindow.get('layout')
    assert all(key in jlayout for key in ('type', 'direction', 'items'))
    jitems = jlayout.get('items')
    assert len(jitems) == 2

    jitem = jitems[0]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'pushButton'
    assert jwidget.get('text') == 'This is a plain PushButton'

    jitem = jitems[1]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text', 'actions'))
    assert jwidget.get('type') == 'pushButton'
    assert jwidget.get('text') == 'This is a PushButton with menu'
    jactions = jwidget.get('actions')
    assert len(jactions) == 1
    jaction = jactions[0]
    assert all(key in jaction for key in ('menu',))
    jmenu = jaction.get('menu')
    assert all(key in jmenu for key in ('actions',))
    jactions = jmenu.get('actions')
    assert len(jactions) == 2
    jaction = jactions[1]  # Index 1 first, because it is the simplest
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Meny entry 2'
    jaction = jactions[0]
    assert all(key in jaction for key in ('text', 'menu'))
    assert jaction.get('text') == 'Meny entry 1'
    jmenu = jaction.get('menu')
    assert all(key in jmenu for key in ('actions',))
    jactions = jmenu.get('actions')
    jaction = jactions[0]
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Submenu entry A'
    jaction = jactions[1]
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Submenu entry B'
    jaction = jactions[2]
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Submenu entry C'


def test_multi_cell_grid_layout_with_chart_and_table(get_app):
    app = get_app
    fill_data = True
    window = app.widget()
    mainGrid = app.gridLayout()
    counter = 0
    for i in range(3):
        for j in range(3):
            if i == 0 and j == 0:
                subGrid = app.gridLayout()
                for k in range(7):
                    subItem = app.pushButton(f"Item {counter} [{i},{j}]/[{k}]")
                    subItem.setFlat(True if k % 2 == 0 else False)
                    subGrid.addWidget(subItem)
                subGridWidget = app.widget()
                subGridWidget.setLayout(subGrid)
                mainGrid.addWidget(subGridWidget, i, j, 0, 0)
            elif i == 1 and j == 0:
                table = app.tableWidget()
                table.setRowCount(10)
                table.setColumnCount(3)
                tableHeader = ["A", "B", "C"]
                table.setHorizontalHeaderLabels(tableHeader)
                table.setVerticalHeaderVisible(False)
                table.setShowGrid(False)
                table.setStyleSheet("QTableView {selection-background-color: red;}")
                if fill_data:
                    table.setItem(0, 0, app.tableWidgetItem("Item 0.0"))
                    table.setItem(0, 1, app.tableWidgetItem("Item 0.1"))
                    table.setItem(0, 2, app.tableWidgetItem("Item 0.2"))
                    table.setItem(1, 0, app.tableWidgetItem("Item 1.0"))
                    table.setItem(1, 1, app.tableWidgetItem("Item 1.1"))
                    table.setItem(1, 2, app.tableWidgetItem("Item 1.2"))
                mainGrid.addWidget(table, i, j, 0, 0)
            elif i == 1 and j == 1:
                axis_x = app.dateTimeAxis()
                axis_x.setTickCount(10)
                axis_x.setFormat("dd.MM (h:mm)")
                axis_x.setTitleText("This is a datetime axis")

                axis_y = app.valueAxis()
                axis_y.setTickCount(10)
                axis_y.setLabelFormat("%.2f")
                axis_y.setTitleText("This is a value axis")
                axis_y.setMin(10)
                axis_y.setMax(100)

                serie1 = app.lineSeries()
                serie1.setName("This is a series")

                if fill_data:
                    t = time()
                    for _ in range(24):
                        v = random.randrange(10, 100)
                        serie1.append(float(_ui.toMSecsSinceEpoch(t)), v)
                        t = _ui.addSecs(t, 3600)

                chart = app.chart()
                chart.setTitle("This is a chart")
                chart.addAxis(axis_x, Aligment.AlignBottom)
                chart.addAxis(axis_y, Aligment.AlignLeft)

                chart.addSeries(serie1)
                serie1.attachAxis(axis_x)
                serie1.attachAxis(axis_y)
                chartView = app.chartView(chart)
                mainGrid.addWidget(chartView, i, j, 0, 0)
            counter += 1

    window.setLayout(mainGrid)
    window.setWindowTitle("This is a window")

    cfg = _ui.export_qt(window)
    assert cfg
    assert json.loads(cfg)


def test_item_data_property_with_0_decimals(get_app):
    """ as pr report https://gitlab.com/shyft-os/shyft/-/issues/976 """
    app = get_app
    table = app.tableWidget()
    row_item = app.tableWidgetItem("dummy")
    row_item.setData(ui.ItemDataProperty.Decimals, 0)
    table.setRowCount(1)
    table.setVerticalHeaderItem(0, row_item)
    x = _ui.export_qt(table)
    assert "decimals" in x


def test_area_series_set_name(get_app: Application):
    area_series = get_app.areaSeries()
    area_series.setName("test_name")
    assert area_series.name() == "test_name"


def test_area_series_set_object_name(get_app: Application):
    area_series = get_app.areaSeries()
    area_series.setObjectName("test_name")
    assert area_series.objectName() == "test_name"


def test_area_series_properties(get_app: Application):
    area_series = get_app.areaSeries()
    area_series.setProperty("upper_series", "url_upper")
    area_series.setProperty("lower_series", "url_lower")
    assert area_series.property("upper_series") == "url_upper"
    assert area_series.property("lower_series") == "url_lower"
