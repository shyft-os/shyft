from shyft.energy_market import ui
import pytest

if not ui.shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)


def test_layout_info():
    li = ui.LayoutInfo(1, "name", "{misc.}")
    assert hasattr(li, "id")
    assert hasattr(li, "name")
    assert hasattr(li, "json")
    assert li.id == 1
    assert li.name == "name"
    assert li.json == "{misc.}"
