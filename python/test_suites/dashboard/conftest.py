from typing import Tuple, List
import logging
import pytest
from tornado.ioloop import IOLoop
import asyncio

@pytest.fixture(autouse=True,scope='session')
def ensure_asyncio_loop():
    loop=asyncio.new_event_loop()
    asyncio.set_event_loop(loop)


@pytest.fixture
def logger_and_list_handler() -> Tuple[logging.Logger, List]:

    class ListHandler(logging.Handler):  # Inherit from logging.Handler
        def __init__(self, log_list):
            # run the regular Handler __init__
            logging.Handler.__init__(self)
            # Our custom argument
            self.log_list = log_list

        def emit(self, record):
            # record.message is the log message
            self.log_list.append(record.msg)

    log_list = []
    lh = ListHandler(log_list)
    lh.setLevel(logging.DEBUG)
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(lh)

    return logger, log_list

@pytest.fixture
def mock_bokeh_document():
    class Document:
        def __init__(self):
            self.callbacks = []
            self.next_tick_callbacks = []

        def add_periodic_callback(self, callback, time):
            if callback not in self.callbacks:
                self.callbacks.append(callback)

        def add_next_tick_callback(self, callback):
            self.next_tick_callbacks.append(callback)

        def next_tick(self):
            if not self.next_tick_callbacks:
                return False
            cb = self.next_tick_callbacks.pop(0)
            io_loop = IOLoop.current()
            if not io_loop:
                io_loop= IOLoop()
            io_loop.run_sync(cb)
            return True

    return Document()
