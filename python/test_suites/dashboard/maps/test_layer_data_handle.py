import pytest

from shyft.dashboard.maps.map_viewer import MapViewer
from shyft.dashboard.maps.layer_data import LayerDataHandle, LayerDataError
from shyft.dashboard.maps.map_layer import MapLayer, MapLayerType
#from shyft.dashboard.test.maps.test_map_fixtures import basemap_factory


def test_layer_data_handle(base_map):
    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer.fig_axes_ranges.set_axes_bounds(26621 + 22373 * 0.5, 6599653 + 18731 * 0.5,
                                               26621 + 22373 * 0.5 + 100, 6599653 + 18731 * 0.5 + 100)
    map_viewer.fig_axes_ranges.padding = 10
    assert not map_viewer.layers
    layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                      glyph_fixed_kwargs={'radius': 5},
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'}, )

    data = {'x': [1,2], 'y': [2,3]}
    layer_data_handle = LayerDataHandle(map_layer=layer1, data=data)

    assert layer1 == layer_data_handle.map_layer
    assert data == layer_data_handle.data

    with pytest.raises(LayerDataError):
        layer_data_handle = LayerDataHandle(map_layer=map_viewer, data=data)
