from shyft.time_series import TimeSeries, TimeAxis, TsVector, POINT_INSTANT_VALUE
import numpy as np


def test_basic_case():
    ta = TimeAxis(0, 10, 6)
    va = np.array([0, 10, 20, 30, 40, 50.])
    ts = TimeSeries(ta, va, POINT_INSTANT_VALUE)

    ts_new = ts.use_time_axis(TimeAxis(5, 5, 3))
    assert ts.point_interpretation() == ts_new.point_interpretation()

    expected = np.array([5., 10, 15.])
    assert np.allclose(ts_new.values, expected)
