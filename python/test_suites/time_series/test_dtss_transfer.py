from typing import Callable

import pytest
import re
from time import sleep
from shyft.time_series import (DtsClient, DtsServer,
                               TimeAxis, TimeSeries, TsVector, point_interpretation_policy as point_fx, time,
                               Calendar, StorePolicy,
                               TransferConfiguration, UtcPeriod, StringVector,
                               TsInfoVector,
                               log_level
                               )
from shyft.time_series import TsInfo


def _make_test_time_series(n: int, value: float, n_values: int = 10000, prefix: str = 'shyft://stm',
                           t0: time = time('2021-10-01T00:00:00Z'), dt: time = time(3600)) -> TsVector:
    ta = TimeAxis(t0, dt, n_values)
    return TsVector(
        [TimeSeries(f'{prefix}/r/{i}.realised',
                    TimeSeries(ta, fill_value=float(i + value), point_fx=point_fx.POINT_AVERAGE_VALUE)) for i in
         range(n)]
    )


@pytest.mark.parametrize("prefix", ["x://stm", "shyft://stm"])
def test_dtss_transfer(tmp_path, prefix):
    """

    Start two dtss servers,
    fill one with some data,
    then use transfer to replicate to the
    other server.

    :param tmp_path:
    :return:
    """
    log_level(log_level=200)
    r1 = tmp_path / "1"
    r2 = tmp_path / "2"
    s1 = DtsServer()
    s1.set_container(name="stm", root_dir=str(r1), container_type="ts_rdb")
    p1 = s1.start_server()
    s2 = DtsServer()
    s2.set_container(name="stm", root_dir=str(r2), container_type="ts_rdb")
    p2 = s2.start_server()
    c1 = DtsClient(f"localhost:{p1}")
    c2 = DtsClient(f"localhost:{p2}")

    ## setup external callbacks
    x_db: dict = {}

    def x_read_callback(ts_ids: StringVector, read_period: UtcPeriod) -> TsVector:
        r = TsVector()
        for ts_id in ts_ids:
            if ts_id in x_db:
                r.append(x_db[ts_id])
            else:
                raise RuntimeError(f"x_db: failed to find:{ts_id}")
        return r

    def x_find_callback(search_expression: str) -> TsInfoVector:
        r = TsInfoVector()
        prog = re.compile(search_expression)
        for tsn in x_db.keys():
            if prog.fullmatch(tsn):
                ts = x_db[tsn]
                tsi = TsInfo(name=tsn,
                             point_fx=ts.point_interpretation(),
                             delta_t=ts.time_axis.fixed_dt.delta_t,
                             olson_tz_id='',
                             data_period=ts.time_axis.total_period(),
                             created=time.now(),
                             modified=time.now())
                r.append(tsi)
        return r

    def x_store_callback(tsv: TsVector) -> None:
        for ts in tsv:
            x_db[ts.ts_id()] = ts

    s1.cb = x_read_callback
    s1.find_cb = x_find_callback
    s1.store_ts_cb = x_store_callback

    ## generate test time-series

    n_ts: int = 100
    n_values: int = 10000
    tsv0 = _make_test_time_series(n_ts, value=0, n_values=n_values, prefix=prefix)
    c1.store(tsv0, StorePolicy(recreate=True, strict=True, cache=True))

    xfers = c1.get_transfers()
    assert len(xfers) == 0

    t = TransferConfiguration()
    t.name = "test-1"
    t.json = '{"info":"Some useful info about the transfer here"}'
    t.what.search_pattern = f"{prefix}/(r/.*realised)"
    t.what.replace_pattern = "shyft://stm/from_a/$1"
    t.where.host = "localhost"
    t.where.port = p2
    t.read_remote = False  # default
    t.read_updates_cache = True  # default
    t.store_policy = StorePolicy(recreate=False, strict=False, cache=True, best_effort=True)
    t.when.changed = False  # default
    t.when.poll_interval = time(0.02)
    t.when.change_linger_time = time(1.0)
    # t.when.ta=TimeAxis()
    t.how.retries = 10
    t.how.sleep_before_retry = time(0.5)
    t.how.partition_size = 15
    c1.start_transfer(t)
    xfers = c1.get_transfers()
    assert len(xfers) == 1
    expected_total_transferred = n_ts * n_values
    s0 = c1.get_transfer_status(name=t.name, clear_status=False)
    while s0.total_transferred < expected_total_transferred:
        sleep(0.1)
        s0 = c1.get_transfer_status(name=t.name, clear_status=False)

    s0 = c1.get_transfer_status(name=t.name, clear_status=True)
    # print(s0)
    assert s0.total_transferred == expected_total_transferred
    assert s0.n_ts_found == n_ts
    assert len(s0.read_errors) == 0
    assert len(s0.write_errors) == 0
    assert len(s0.remote_errors) == 0
    assert s0.read_speed > 0.0
    assert s0.write_speed > 0.0
    assert s0.last_activity > time.now() - Calendar.HOUR
    # would be flaky, since it takes a while for them to finish.
    # assert not s0.reader_alive
    # assert not s0.writer_alive

    c1.stop_transfer(name=t.name, max_wait=time(10.0))
    xfers = c1.get_transfers()
    assert len(xfers) == 0
    c1.close()
    f2 = c2.find("shyft://stm/from_a.*")
    assert len(f2) == n_ts
    c2.close()
    s1.clear()
    s2.clear()
    pass
