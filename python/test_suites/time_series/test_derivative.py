from shyft.time_series import TimeSeries, TimeAxis, TsVector, POINT_INSTANT_VALUE, derivative_method
import math


def test_linear():
    ta = TimeAxis(0, 10, 6)
    tsv = [1, 1, 2, 3, -1.0, 5.0]
    f = TimeSeries(ta, tsv, POINT_INSTANT_VALUE)
    d_f = f.derivative()
    assert len(f) == len(d_f)
    assert round(abs(d_f.value(0) - 0.0), 7) == 0
    assert round(abs(d_f.value(1) - 0.1), 7) == 0
    assert round(abs(d_f.value(2) - 0.1), 7) == 0
    assert round(abs(d_f.value(3) - -0.4), 7) == 0
    assert round(abs(d_f(f.time(3) + 5) - -0.4), 7) == 0
    assert round(abs(d_f.value(4) - 0.6), 7) == 0
    assert not math.isfinite(d_f.value(5))
    assert not math.isfinite(d_f(f.time(5)))
    assert math.isfinite(d_f(f.time(5) - 1))
    v = d_f.values
    assert round(abs(len(v) - len(f)), 7) == 0
    assert round(abs(v[0] - 0.0), 7) == 0
    assert round(abs(v[1] - 0.1), 7) == 0
    assert round(abs(v[2] - 0.1), 7) == 0
    assert round(abs(v[3] - -0.4), 7) == 0
    assert round(abs(v[4] - 0.6), 7) == 0
    assert not math.isfinite(v[5])


def test_linear_vector():
    ta = TimeAxis(0, 10, 6)
    tsv = [1, 1, 2, 3, -1.0, 5.0]
    fv = TsVector([TimeSeries(ta, tsv, POINT_INSTANT_VALUE)])
    d_fv = fv.derivative()
    f = fv[0]
    d_f = d_fv[0]
    assert len(f) == len(d_f)
    assert round(abs(d_f.value(0) - 0.0), 7) == 0
    assert round(abs(d_f.value(1) - 0.1), 7) == 0
    assert round(abs(d_f.value(2) - 0.1), 7) == 0
    assert round(abs(d_f.value(3) - -0.4), 7) == 0
    assert round(abs(d_f(f.time(3) + 5) - -0.4), 7) == 0
    assert round(abs(d_f.value(4) - 0.6), 7) == 0
    assert not math.isfinite(d_f.value(5))
    assert not math.isfinite(d_f(f.time(5)))
    assert math.isfinite(d_f(f.time(5) - 1))
    v = d_f.values
    assert round(abs(len(v) - len(f)), 7) == 0
    assert round(abs(v[0] - 0.0), 7) == 0
    assert round(abs(v[1] - 0.1), 7) == 0
    assert round(abs(v[2] - 0.1), 7) == 0
    assert round(abs(v[3] - -0.4), 7) == 0
    assert round(abs(v[4] - 0.6), 7) == 0
    assert not math.isfinite(v[5])


def test_bind_info():
    ts = TimeSeries("a")
    tsd = ts.derivative(derivative_method.FORWARD)
    bi = tsd.find_ts_bind_info()
    assert len(bi) == 1
