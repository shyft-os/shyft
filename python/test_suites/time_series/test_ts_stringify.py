from shyft.time_series import TimeSeries, ts_stringify, time, TimeAxis


def test_ts_stringify():
    a = TimeSeries('a')
    b = TimeSeries('b')
    c = a + b*4.2
    s_a = ts_stringify(a)
    s_c = ts_stringify(c)
    assert s_a == 'a'
    assert s_c == '(a + (b*4.2))'
    d = c.statistics(TimeAxis(time('2020-03-04T00:00:00Z'), time(3600), 24), 50)
    s_d = ts_stringify(d)
    assert s_d == 'statistics((a + (b*4.2)),TaF[2020-03-04T00:00:00Z,3600,24],50)'
    assert (a*1.23456e-20).stringify() == '(a*1.23456e-20)'