from typing import Callable

from time import sleep
from shyft.time_series import (DtsClient, DtsServer,
                               TimeAxis, TimeSeries, TsVector, point_interpretation_policy as point_fx, time,
                               QueueBridgeConfiguration,
                               log_level
                               )

def _make_test_time_series(n: int, value: float, n_values: int = 10000, prefix: str = 'shyft://stm',
                           t0: time = time('2021-10-01T00:00:00Z'), dt: time = time(3600)) -> TsVector:
    ta = TimeAxis(t0, dt, n_values)
    return TsVector(
        [TimeSeries(f'{prefix}/r/{i}.realised',
                    TimeSeries(ta, fill_value=float(i + value), point_fx=point_fx.POINT_AVERAGE_VALUE)) for i in
         range(n)]
    )


def test_dtss_queue_bridge():
    def wait_until(predicate: Callable[[], bool], timeout: time = 3.0, interval: time = 0.01):
        start = time.now()
        while time.now() - start < timeout:
            if predicate():
                return True
            sleep(float(interval))
        return False

    log_level(log_level=200)
    s1 = DtsServer()
    p1 = s1.start_server()
    s2 = DtsServer()
    p2 = s2.start_server()
    c1 = DtsClient(f"localhost:{p1}")
    c2 = DtsClient(f"localhost:{p2}")
    xfers = c1.get_q_bridges()
    assert len(xfers) == 0
    queue_name = "test_queue"
    t = QueueBridgeConfiguration()
    t.name = "test-1"
    t.what.queue_name = queue_name
    t.what.ack_poll_interval = time(0.01)
    t.where.host = "localhost"
    t.where.port = p2
    t.where.queue_name = queue_name
    t.how.retries = 3
    t.how.sleep_before_retry = time(0.01)
    t.how.queue_pick_up_max_wait = time(0.1)
    t.how.io_operation_timeout = time(0.5)
    c1.start_q_bridge(t)
    assert wait_until(lambda: len(c1.get_q_bridges()) == 1)
    bridges = c1.get_q_bridges()
    assert len(bridges) == 1
    assert bridges[0].name == t.name

    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert wait_until(
        lambda: c1.get_q_bridge_status(name=t.name, clear_status=False).fetch_errors>0 and c1.get_q_bridge_status(name=t.name, clear_status=False).last_activity > s0.last_activity)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=True)  # clear out err.msg
    assert s0.fetch_errors > 0  # should be many missing queue error messages
    assert s0.ack_errors == 0
    assert s0.fetched == 0
    assert s0.completed == 0
    c2.q_add(queue_name)  # add a queue so that mirror process get a working setup
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=True)  # record time now

    assert wait_until(
        lambda: c1.get_q_bridge_status(name=t.name, clear_status=True).last_activity > s0.last_activity)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=True)
    assert s0.fetch_errors == 0  # local queue auto created, so no missing.
    assert s0.ack_errors == 0
    assert s0.fetched == 0
    assert s0.completed == 0
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=True)  # clear status
    assert wait_until(
        lambda: c1.get_q_bridge_status(name=t.name, clear_status=True).last_activity > s0.last_activity)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert s0.ack_errors == 0
    assert s0.fetch_errors == 0
    assert s0.fetched == 0
    assert s0.completed == 0
    # ready for messages:
    n_ts: int = 10
    n_values: int = 100
    tsv0 = _make_test_time_series(n_ts, value=0, n_values=n_values)
    c2.q_put(queue_name, "001", "description", time(10), tsv0)
    assert wait_until(lambda: c1.get_q_bridge_status(name=t.name, clear_status=False).fetched == 1)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert s0.fetch_errors == 0
    assert s0.ack_errors == 0
    assert s0.completed == 0
    assert s0.fetched == 1

    resp = c1.q_get(queue_name, time(10))
    c1.q_ack(queue_name, resp.info.msg_id, "done")
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert wait_until(lambda: c1.get_q_bridge_status(name=t.name, clear_status=False).completed == 1)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert s0.fetch_errors == 0
    assert s0.ack_errors == 0
    assert s0.completed == 1
    assert s0.fetched == 1

    resp1 = c1.q_find(queue_name, resp.info.msg_id)
    resp2 = c2.q_find(queue_name, "001")

    assert resp1.info.msg_id != resp2.info.msg_id
    assert resp1.info.diagnostics == resp2.info.diagnostics
    assert len(resp1.tsv) == len(resp2.tsv)

    c1.q_put(queue_name, "002", "description", time(10), tsv0)
    c2.q_put(queue_name, "002", "description1", time(10), tsv0)
    assert wait_until(lambda: c1.get_q_bridge_status(name=t.name, clear_status=False).fetched == 2)

    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert s0.fetch_errors == 0
    assert s0.ack_errors == 0
    assert s0.completed == 1
    assert s0.fetched == 2
    resp = c1.q_get(queue_name, time(10))

    c1.q_ack(queue_name, resp.info.msg_id, "done")
    assert wait_until(lambda: c1.get_q_bridge_status(name=t.name, clear_status=False).fetched == 2)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert s0.fetch_errors == 0
    assert s0.ack_errors == 0
    assert s0.completed == 1
    assert s0.fetched == 2

    resp = c1.q_get(queue_name, time(10))
    c1.q_ack(queue_name, resp.info.msg_id, "done")
    assert wait_until(lambda: c1.get_q_bridge_status(name=t.name, clear_status=False).completed == 2)
    s0 = c1.get_q_bridge_status(name=t.name, clear_status=False)
    assert s0.fetch_errors == 0
    assert s0.ack_errors == 0
    assert s0.fetched == 2
    assert s0.completed == 2
    c1.stop_q_bridge(name=t.name, max_wait=time(10.0))
