import numpy as np
from shyft.time_series import Calendar, deltahours, TimeAxis, DoubleVector, create_periodic_pattern_ts


def test_periodic_pattern_ts():
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    pattern_values = np.arange(8)
    pattern_dt = deltahours(3)
    pattern_t0 = c.time(2015, 6, 1)
    pattern_ts = create_periodic_pattern_ts(pattern_values, pattern_dt, pattern_t0,
                                            ta)  # this is how to create a periodic pattern ts (used in gridpp/kalman bias handling)
    assert round(abs(pattern_ts.value(0) - 0.0), 7) == 0
    assert round(abs(pattern_ts.value(1) - 0.0), 7) == 0
    assert round(abs(pattern_ts.value(2) - 0.0), 7) == 0
    assert round(abs(pattern_ts.value(3) - 1.0), 7) == 0  # next step in pattern starts here
    assert round(abs(pattern_ts.value(24) - 0.0), 7) == 0  # next day repeats the pattern
