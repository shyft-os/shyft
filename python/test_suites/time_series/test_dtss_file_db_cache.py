import pytest
from typing import Final

import numpy as np
from shyft.time_series import (
    Calendar,
    DtsClient,
    DtsServer,
    TimeAxis,
    TimeSeries,
    TsVector,
    UtcPeriod,
    POINT_AVERAGE_VALUE,
    deltahours,
    time,
)

UTC: Final[Calendar] = Calendar()
T0: Final[time] = UTC.time(2022, 1, 1)
T1: Final[time] = UTC.time(2024, 1, 1)
T2: Final[time] = UTC.time(2024, 4, 1)

N0: Final[int] = int((T1 - T0) // deltahours(1))
N1: Final[int] = int((T2 - T1) // deltahours(1))


def ts_id(num: int):
    return f"shyft://store/series-{num}"


def ts_data(time_axis: TimeAxis) -> TimeSeries:
    return TimeSeries(time_axis, np.random.randn(len(time_axis)), POINT_AVERAGE_VALUE)


@pytest.fixture(params=["ts_rdb", "ts_db"])
def dts_service(tmp_path, request):
    store_dir = tmp_path.as_posix()
    server = DtsServer()
    server.set_container("store", store_dir, request.param)

    try:
        port = server.start_server()
        client = DtsClient(f"localhost:{port}")

        # populate with data for [T0, T1]
        time_axis = TimeAxis(T0, deltahours(1), N0)
        data = TsVector([TimeSeries(ts_id(i), ts_data(time_axis)) for i in range(5)])
        client.store_ts(data, True, False)

        yield client

    finally:
        server.stop_server()


def wrap2tsv(ts_id: str, data: TimeSeries | None = None):
    """Wrap a time series id in a TsVector of size 1."""
    return TsVector([TimeSeries(ts_id) if not data else TimeSeries(ts_id, data)])


def test_cache_empty(dts_service):
    """The result total period should cover requested period."""
    dts_service.cache_flush()

    ts = dts_service.evaluate(wrap2tsv(ts_id(0)), UtcPeriod(T0, T1))[0]
    data_period = ts.time_axis.total_period()

    assert data_period.contains(T0), f"{data_period} does not contain {T0}"


def test_cache_on_read(dts_service):
    """Test that cache on read don't cause false cache hit."""
    dts_service.cache_flush()

    _ = dts_service.evaluate(wrap2tsv(ts_id(1)), UtcPeriod(T1, T2))
    ts = dts_service.evaluate(wrap2tsv(ts_id(1)), UtcPeriod(T0, T2))[0]
    data_period = ts.time_axis.total_period()

    assert data_period.contains(T0), f"{data_period} does not contain {T0}"


@pytest.mark.parametrize("cache_on_write", [True, False])
def test_after_cache_on_store(dts_service, cache_on_write: bool):
    """Test that cache on store don't cause false cache hit."""
    dts_service.cache_flush()

    ta = TimeAxis(T1, deltahours(1), N1)
    dts_service.store_ts(wrap2tsv(ts_id(2), ts_data(ta)), False, cache_on_write)
    ts = dts_service.evaluate(wrap2tsv(ts_id(2)), UtcPeriod(T0, T2))[0]

    data_period = ts.time_axis.total_period()

    assert data_period.contains(T0), f"{data_period} does not contain {T0}"


@pytest.mark.parametrize("cache_on_write", [True, False])
def test_after_cache_on_merge_store(dts_service, cache_on_write: bool):
    """Test that cache on merge-store don't cause false cache hit."""
    dts_service.cache_flush()

    ta = TimeAxis(T1, deltahours(1), N1)
    dts_service.merge_store_ts_points(wrap2tsv(ts_id(3), ts_data(ta)), cache_on_write)

    ts = dts_service.evaluate(wrap2tsv(ts_id(3)), UtcPeriod(T0, T2))[0]
    data_period = ts.time_axis.total_period()

    assert data_period.contains(T0), f"{data_period} does not contain {T0}"
