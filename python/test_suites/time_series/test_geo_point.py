import pytest
from math import isfinite

from shyft import time_series as api

"""Verify and illustrate GeoPoint exposure to python
   The GeoPoint plays an important role when you create
   cells, and when you have geo located time-series that
   you feed into shyft
 """


def test_create():
    p1 = api.GeoPoint(1, 2, 3)

    assert round(abs(p1.x - 1), 7) == 0
    assert round(abs(p1.y - 2), 7) == 0
    assert round(abs(p1.z - 3), 7) == 0
    p2 = api.GeoPoint(p1)
    assert p1 == p2
    p2.x += 2
    assert p1 != p2


def test_create_default():
    p2 = api.GeoPoint()
    assert round(abs(p2.x - 0), 7) == 0
    assert round(abs(p2.y - 0), 7) == 0
    assert round(abs(p2.z - 0), 7) == 0


def test_difference_of_two():
    a = api.GeoPoint(1, 2, 3)
    b = api.GeoPoint(3, 4, 8)
    d = api.GeoPoint.difference(b, a)
    assert round(abs(d.x - (b.x - a.x)), 7) == 0


def test_xy_distance():
    a = api.GeoPoint(1, 2, 3)
    b = api.GeoPoint(3, 4, 8)
    d = api.GeoPoint.xy_distance(b, a)
    assert round(abs(d - 2.8284271247), 7) == 0


def test_create_from_x_y_z_vector():
    x = api.DoubleVector([1.0, 4.0, 7.0])
    y = api.DoubleVector([2.0, 5.0, 8.0])
    z = api.DoubleVector([3.0, 6.0, 9.0])
    gpv = api.GeoPointVector.create_from_x_y_z(x, y, z)
    for i in range(3):
        assert round(abs(gpv[i].x - (3 * i + 1)), 7) == 0
        assert round(abs(gpv[i].y - (3 * i + 2)), 7) == 0
        assert round(abs(gpv[i].z - (3 * i + 3)), 7) == 0


def test_transform_epsg():
    # 32633 -> 32632 (two nearby epsg) that splits norway
    p32633 = api.GeoPoint(254807.03, 6784125.07, 100)
    p32632 = api.GeoPoint(578006.66, 6776452.38, 100)
    a = p32633.transform(from_epsg=32633, to_epsg=32632)
    assert api.GeoPoint.xy_distance(a=a, b=p32632) == pytest.approx(0.0, abs=0.01)
    assert a.z == pytest.approx(p32633.z)
    b = p32633.transform(32633, 32633)
    assert b == p32633
    p32633v = api.GeoPointVector([p32633, p32633])
    p32632v = api.GeoPointVector([p32632, p32632])
    av = p32633v.transform(from_epsg=32633, to_epsg=32632)
    for pa in av:
        assert pa.z == pytest.approx(p32632.z)
        assert api.GeoPoint.xy_distance(a=pa, b=p32632) == pytest.approx(0.0, abs=0.01)
    bv = p32633v.transform(from_epsg=32633, to_epsg=32632)
    assert bv == p32632v


def test_transform_proj4():
    p32633 = api.GeoPoint(254807.03, 6784125.07, 100)
    p32633_ll = api.GeoPoint(10.447590315396154, 61.1149353468646, 100)
    p32632 = api.GeoPoint(578006.66, 6776452.38, 100)
    a = p32633.transform(from_proj4="+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
                         to_proj4="+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    assert api.GeoPoint.xy_distance(a=a, b=p32632) == pytest.approx(0.0, abs=0.01)
    assert a.z == pytest.approx(p32633.z)
    b = p32633.transform(from_proj4="+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
                         to_proj4="+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    assert b == p32633
    p32633v = api.GeoPointVector([p32633, p32633])
    p32632v = api.GeoPointVector([p32632, p32632])
    av = p32633v.transform(from_proj4="+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
                           to_proj4="+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    for pa in av:
        assert pa.z == pytest.approx(p32632.z)
        assert api.GeoPoint.xy_distance(a=pa, b=p32632) == pytest.approx(0.0, abs=0.01)
    bv = p32633v.transform(from_proj4="+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
                           to_proj4="+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    assert bv == p32632v
    bv_ll = p32633v.transform(from_proj4="+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +units=m +no_defs",
                              to_proj4="+proj=longlat +datum=WGS84 +no_defs")
    assert bv_ll == api.GeoPointVector([p32633_ll, p32633_ll])


def test_geo_point_vector_polygon_buffer():
    e = api.GeoPointVector()  # empty gives empty
    assert not e.polygon_buffer(distance=1000.0)
    # just to illustrate, the full test is in c++ part
    p1 = api.GeoPointVector([api.GeoPoint(0, 0, 0)])
    d = 10.0
    pb1 = p1.polygon_buffer(distance=d)
    assert pb1 == api.GeoPointVector(
        [api.GeoPoint(-d, -d, 0),
         api.GeoPoint(-d, d, 0),
         api.GeoPoint(d, d, 0),
         api.GeoPoint(d, -d, 0),
         api.GeoPoint(-d, -d, 0)
         ])


def test_geo_point_vector_polygon_area():
    e = api.GeoPointVector()
    assert not isfinite(e.area())
    e1 = api.GeoPointVector([api.GeoPoint(0.0, 0.0, 0.0)])
    assert not isfinite(e1.area())
    # notice clock wise polygon
    e2 = api.GeoPointVector([api.GeoPoint(0.0, 0.0, 0.0), api.GeoPoint(1.0, 1.0, 0.0), api.GeoPoint(1.0, 0.0, 0.0)])
    assert e2.area() == 0.5
    # and also for a list of polygons
    e2v = api.GeoPointVectorVector([e2])
    assert e2v.areas() == [0.5]


def test_geo_point_vector_polygon_mid_point():
    e = api.GeoPointVector()
    assert not isfinite(e.mid_point().x)
    assert not isfinite(e.mid_point().y)
    assert not isfinite(e.mid_point().z)
    e1 = api.GeoPointVector([api.GeoPoint(0.0, 0.0, 0.0)])
    assert e1.mid_point() == api.GeoPoint(0.0, 0.0, 0.0)
    e2 = api.GeoPointVector([api.GeoPoint(0.0, 0.0, 0.0), api.GeoPoint(1.0, 3.0, 3.0), api.GeoPoint(2.0, 0.0, 0.0)])
    assert e2.mid_point() == api.GeoPoint(1.0, 1.0, 1.0)
    # test polygon vector as well
    e2v = api.GeoPointVectorVector([e2])
    e2v_midpoints = e2v.mid_points()
    assert e2v_midpoints[0] == api.GeoPoint(1.0, 1.0, 1.0)


def test_geo_point_vector_nearest_neighbours():
    p1 = api.GeoPoint(0, 0, 0)
    p2 = api.GeoPoint(1, 1, 1)
    p3 = api.GeoPoint(2, 2, 2)
    p4 = api.GeoPoint(3, 3, 3)
    p5 = api.GeoPoint(4, 4, 4)
    points = api.GeoPointVector([p1, p2, p3, p4, p5])
    nearest = points.nearest_neighbours(p1, 3)
    assert nearest == api.GeoPointVector([p1, p2, p3])
    nearest2 = points.nearest_neighbours(p1, 10)
    assert nearest2 == points
    assert not api.GeoPointVector().nearest_neighbours(p1, 3)
    assert not points.nearest_neighbours(p1, 0)


def test_geo_point_regular_grid_error():
    p1 = api.GeoPoint(24361, 6615293, 0)
    # 24361 6615293, epsg: 32633 blåsjø, sandsavatnet

    d = 0.5
    poly = api.GeoPointVector([p1]).polygon_buffer(distance=d)
    assert poly.area() == 1.0
    # transform to met.no
    poly_met_no = poly.transform(
        from_proj4="+proj=utm +zone=33 +datum=WGS84 +units=m +no_defs +type=crs",
        to_proj4="+proj=lcc +lat_0=63.3 +lon_0=15 +lat_1=63.3 +lat_2=63.3 +x_0=0 +y_0=0 +R=6371000 +units=m +no_defs +type=crs")
    area = poly_met_no.area()
    assert area == pytest.approx(1.0, rel=0.01)
    # case 2: really outside the 32633 definitions,
    # 1111718 7874458 epsg: 32633, kirkenes ca.
    p2 = api.GeoPoint(1111718, 7874458, 0)
    poly2 = api.GeoPointVector([p2]).polygon_buffer(distance=d)
    assert poly2.area() == 1.0
    poly_met_no2 = poly2.transform(
        from_proj4="+proj=utm +zone=33 +datum=WGS84 +units=m +no_defs +type=crs",
        to_proj4="+proj=lcc +lat_0=63.3 +lon_0=15 +lat_1=63.3 +lat_2=63.3 +x_0=0 +y_0=0 +R=6371000 +units=m +no_defs +type=crs")
    a2 = poly_met_no2.area()
    # investigating the sides of new square shows it is up to 1.4% deformed, but the area is within 1%
    assert a2 == pytest.approx(1.0, rel=0.01)


def test_geo_point_vector_list():
    p = api.GeoPointVector([api.GeoPoint(1, 2, 3), api.GeoPoint(4, 5, 6), api.GeoPoint(7, 8, 9)])
    assert len(p) == 3
    pv = api.GeoPointVectorVector([p])
    assert len(pv) == 1
    assert pv[0] == p
    pv[0][0].x = 2.0
    assert pv[0] != p


def test_polygons_intersection_areas():
    p1 = api.GeoPoint(0, 0, 0)
    p2 = api.GeoPoint(0.5, 0, 0)
    p3 = api.GeoPoint(3, 0, 0)
    d = 0.5
    polygon1 = api.GeoPointVector([p1]).polygon_buffer(distance=d)
    polygon2 = api.GeoPointVector([p2]).polygon_buffer(distance=d)
    polygon3 = api.GeoPointVector([p3]).polygon_buffer(distance=d)
    polygon_list = api.GeoPointVectorVector([polygon1, polygon2, polygon3])
    intersection_areas = polygon_list.intersection_areas(polygon2, 8)
    assert intersection_areas == [0.5, 1.0, 0.0]
