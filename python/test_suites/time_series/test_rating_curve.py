import numpy as np
from shyft import time_series as sa
from shyft.time_series import TimeAxis, deltaminutes, TimeSeries, POINT_INSTANT_VALUE, deltahours

from shyft.time_series import RatingCurveSegment, RatingCurveSegments, RatingCurveFunction, RatingCurveTimeFunction, RatingCurveTimeFunctions, RatingCurveParameters, Calendar


def test_easy_rating_curve_construct():
    utc = Calendar()
    rating_curve = RatingCurveParameters(RatingCurveTimeFunctions([
        RatingCurveTimeFunction(
            utc.time(1950, 3, 27), RatingCurveFunction(RatingCurveSegments([
                RatingCurveSegment(lower=0.474, a=5.97489, b=-0.4745, c=2.36997)
            ]))),
        RatingCurveTimeFunction(
            utc.time(1968, 7, 29), RatingCurveFunction(RatingCurveSegments([
                RatingCurveSegment(lower=0.25, a=2.9822, b=-0.45, c=1.5078),
                RatingCurveSegment(lower=0.79, a=3.9513, b=-0.45, c=2.8087),
                RatingCurveSegment(lower=1.38, a=5.7071, b=-0.45, c=2.3503),
                RatingCurveSegment(lower=2.55, a=8.2672, b=-0.45, c=2.052)
            ])))
    ]))
    assert rating_curve is not None
    flow = rating_curve.flow(utc.time(2018, 1, 1), 3.2)
    assert round(abs(flow - 117.8103380205204), 7) == 0


def test_rating_curve_segment():
    lower = 0.0
    a = 1.0
    b = 2.0
    c = 3.0
    rcs = sa.RatingCurveSegment(lower=lower, a=a, b=b, c=c)
    assert rcs.lower == lower
    assert round(abs(rcs.a - a), 7) == 0
    assert round(abs(rcs.b - b), 7) == 0
    assert round(abs(rcs.c - c), 7) == 0
    for level in range(10):
        assert round(abs(rcs.flow(level) - a*pow(level - b, c)), 7) == 0
    flows = rcs.flow([i for i in range(10)])
    for i in range(10):
        assert round(abs(flows[i] - a*pow(float(i) - b, c)), 7) == 0


def test_rating_curve_function():
    rcf = sa.RatingCurveFunction()
    assert rcf.size() == 0
    lower = 0.0
    a = 1.0
    b = 2.0
    c = 3.0
    rcs = sa.RatingCurveSegment(lower=lower, a=a, b=b, c=c)
    rcf.add_segment(rcs)
    rcf.add_segment(lower + 10.0, a, b, c)
    assert rcf.size() == 2
    assert round(abs(rcf.flow(4.0) - 8.0), 7) == 0
    assert round(abs(rcf.flow([4.0])[0] - 8.0), 7) == 0
    s = str(rcf)  # just to check that str works
    assert len(s) > 10
    sum_levels = 0.0
    for rcs in rcf:  # demo iterable
        sum_levels += rcs.lower
    assert round(abs(sum_levels - (lower + 10.0)), 7) == 0


def test_rating_curve_ts():
    t0 = Calendar().time(2018, 7, 1)
    ta = TimeAxis(t0, deltaminutes(30), 48*2)
    data = np.linspace(0, 10, ta.size())
    ts = TimeSeries(ta, data, POINT_INSTANT_VALUE)

    rcf1 = RatingCurveFunction()
    rcf1.add_segment(0, 1, 0, 1)
    rcf1.add_segment(RatingCurveSegment(5, 2, 0, 1))

    rcf2 = RatingCurveFunction()
    rcf2.add_segment(0, 3, 0, 1)
    rcf2.add_segment(RatingCurveSegment(8, 4, 0, 1))

    rcp = RatingCurveParameters()
    rcp.add_curve(t0, rcf1)
    rcp.add_curve(t0 + deltahours(24), rcf2)

    sts = TimeSeries("a")
    rcsts = sts.rating_curve(rcp)

    rcsts_blob = rcsts.serialize()
    rcsts_2 = TimeSeries.deserialize(rcsts_blob)

    assert rcsts_2.needs_bind()
    fbi = rcsts_2.find_ts_bind_info()
    assert len(fbi) == 1
    fbi[0].ts.bind(ts)
    rcsts_2.bind_done()
    assert not rcsts_2.needs_bind()

    assert len(rcsts_2) == len(ts)
    for i in range(rcsts_2.size()):
        expected = (1*ts.get(i).v if ts.get(i).v < 5 else 2*ts.get(i).v) if ts.get(i).t < t0 + deltahours(24) else (
            3*ts.get(i).v if ts.get(i).v < 8 else 4*ts.get(i).v)
        assert rcsts_2.get(i).t == ts.get(i).t
        assert rcsts_2.get(i).v == expected
