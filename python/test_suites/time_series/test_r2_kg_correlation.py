import numpy as np
from shyft.time_series import Calendar, deltahours, TimeAxis, DoubleVector, TimeSeries, point_interpretation_policy, kling_gupta, nash_sutcliffe


def test_kling_gupta_and_nash_sutcliffe():
    """
    Test/verify exposure of the kling_gupta and nash_sutcliffe correlation functions

    """

    def np_nash_sutcliffe(o, p):
        return 1 - (np.sum((o - p)**2))/(np.sum((o - np.mean(o))**2))

    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    from math import sin, pi
    rad_max = 10*2*pi
    obs_values = np.array([sin(i*rad_max/n) for i in range(n)])
    mod_values = np.array([0.1 + sin(pi/10.0 + i*rad_max/n) for i in range(n)])
    obs_ts = TimeSeries(ta=ta, values=obs_values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    mod_ts = TimeSeries(ta=ta, values=mod_values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    assert round(abs(kling_gupta(obs_ts, obs_ts, ta, 1.0, 1.0, 1.0) - 1.0), None) == 0, "1.0 for perfect match"
    assert round(abs(obs_ts.kling_gupta( obs_ts, 1.0, 1.0, 1.0) - 1.0), None) == 0, "1.0 for perfect match"

    assert round(abs(nash_sutcliffe(obs_ts, obs_ts, ta) - 1.0), None) == 0, "1.0 for perfect match"
    assert round(abs(obs_ts.nash_sutcliffe(obs_ts) - 1.0), None) == 0, "1.0 for perfect match"

    # verify some non trivial cases, and compare to numpy version of ns
    mod_inv = obs_ts*-1.0
    kge_inv = kling_gupta(obs_ts, mod_inv, ta)  # also show how to use time-series.method itself to ease use
    ns_inv = nash_sutcliffe(obs_ts, mod_inv, ta)  # similar for nash_sutcliffe, you can reach it directly from a ts
    ns_inv2 = np_nash_sutcliffe(np.array(obs_ts.values), mod_inv.values)
    assert kge_inv <= 1.0, "should be less than 1"
    assert ns_inv <= 1.0, "should be less than 1"
    assert round(abs(ns_inv - ns_inv2), 4) == 0, "should equal numpy calculated value"
    kge_obs_mod = kling_gupta(obs_ts, mod_ts, ta, 1.0, 1.0, 1.0)
    assert kge_obs_mod <= 1.0
    assert round(abs(nash_sutcliffe(obs_ts, mod_ts, ta) - np_nash_sutcliffe(np.array(obs_ts.values), np.array(mod_ts.values))), 7) == 0
    assert round(abs(obs_ts.nash_sutcliffe(mod_ts) - np_nash_sutcliffe(np.array(obs_ts.values), np.array(mod_ts.values))), 7) == 0
