import numpy as np
from numpy.testing import assert_array_almost_equal
from shyft.time_series import TimeSeries, TimeAxis, DoubleVector, POINT_AVERAGE_VALUE, UtcTimeVector


def test_merge_points():
    a = TimeSeries()  # a empty at beginning, we allow that.
    tb = TimeAxis(0, 1, 5)
    b = TimeSeries(tb, values=DoubleVector([1.0, -1.0, 2.0, 3.0, 4.0]), point_fx=POINT_AVERAGE_VALUE)
    a.merge_points(b)  # now a should equal b
    c = TimeSeries(TimeAxis(UtcTimeVector([3, 10, 11]), t_end=12), fill_value=9.0, point_fx=POINT_AVERAGE_VALUE)
    a.merge_points(c)  # now a should have new values for t=3, plus new time-points 11 and 12
    assert len(a) == 7
    assert_array_almost_equal(a.values, np.array([1.0, -1.0, 2.0, 9.0, 4.0, 9.0, 9.0]))
    assert_array_almost_equal(a.time_axis.time_points, np.array([0, 1, 2, 3, 4, 10, 11, 12]))
    xa = TimeSeries("some_unbound_ts")
    xa.merge_points(a)  # now it should be bound, and it's values are from a
    assert len(xa) == 7
    assert_array_almost_equal(xa.values, np.array([1.0, -1.0, 2.0, 9.0, 4.0, 9.0, 9.0]))
    assert_array_almost_equal(xa.time_axis.time_points, np.array([0, 1, 2, 3, 4, 10, 11, 12]))
    d = TimeSeries(TimeAxis(UtcTimeVector([3, 10, 11]), t_end=12), fill_value=10.0, point_fx=POINT_AVERAGE_VALUE)
    xa.merge_points(d)  # now that xa is bound, also check we get updated
    assert len(xa) == 7
    assert_array_almost_equal(xa.values, np.array([1.0, -1.0, 2.0, 10.0, 4.0, 10.0, 10.0]))
    assert_array_almost_equal(xa.time_axis.time_points, np.array([0, 1, 2, 3, 4, 10, 11, 12]))
