from pathlib import Path

import pytest
from netCDF4 import Dataset
import numpy as np
from pyproj import CRS

from shyft.hydrology.repository.netcdf.concat_utils import WindSpeedSources
from shyft.time_series import UtcPeriod, Calendar, GeoPointVector, GeoPoint, GeoMatrixShape
from shyft.hydrology.repository.netcdf.concat_geo_dtss import (DimensionEnsembleParser, CRSVariableEpsgParser,
                                                               CoordinateEpsgParser, VariablesGridParser,
                                                               TimeAxesDimensions, ConcatGeoDtss, ConcatDataParser,
                                                               DataDimensions, EnsembleDimension, GridDimension,
                                                               HeightDimension, TimeAxisDimensions,
                                                               HeightVariableParser, HeightDependentVariableParser)


def test_ensemble_parse_dimension(tmp_path):
    file_path = Path(tmp_path) / 'test_ensembles.nc'
    dimension_name = 'ensemble_member_dimension'
    with Dataset(file_path, 'w') as dataset:
        dataset.createDimension(dimension_name, 3)
    parser = DimensionEnsembleParser(file_path, dimension_name=dimension_name)
    ens_dimension = parser.get()
    assert ens_dimension.n_ensembles == 3
    assert ens_dimension.dimension_name == dimension_name


def test_ensemble_parse_file(netcdf_data_path):
    file_path = netcdf_data_path.ecmwf_ens_merged_vik_red
    parser = DimensionEnsembleParser(file_path, 'ensemble_member')
    ens_dimension = parser.get()
    assert ens_dimension.n_ensembles == 51
    assert ens_dimension.dimension_name == 'ensemble_member'


def test_epsg_code_parse_variable(tmp_path):
    file_path = Path(tmp_path) / 'test_crs.nc'
    variable_name = 'epsg_variable'
    epsg = 4326
    with Dataset(file_path, 'w') as dataset:
        var = dataset.createVariable(variable_name, str, ())
        var.epsg_attribute = CRS.from_epsg(epsg).to_proj4()
    parser = CRSVariableEpsgParser(file_path, variable_name=variable_name, attribute_name='epsg_attribute')
    assert parser.get() == epsg


def test_epsg_code_parse_file(netcdf_data_path):
    file_path = netcdf_data_path.ecmwf_ens_merged_vik_red
    parser = CRSVariableEpsgParser(file_path, 'crs', 'proj4')
    assert parser.get() == 32633


def test_epsg_coordinates_parse_file(netcdf_data_path):
    file_path = netcdf_data_path.arome_default2
    parser = CoordinateEpsgParser(file_path)
    assert parser.get() == 4326


def test_concat_grid_parse_dimension(tmp_path):
    file_path = Path(tmp_path) / 'test_grid.nc'
    with Dataset(file_path, 'w') as dataset:
        dataset.createDimension('grid_points', 4)
        dataset.createVariable("x_var", np.int64, ("grid_points",))
        dataset.createVariable("y_var", np.int64, ("grid_points",))
    parser = VariablesGridParser(file_path, x_variable='x_var', y_variable='y_var', z_variable='z_var')
    grid = parser.get()
    assert list(grid.grid_dimensions.keys()) == ['grid_points']
    assert len(grid.points) == 4


def test_concat_grid_parse_variables(tmp_path):
    file_path = Path(tmp_path) / 'test_grid.nc'
    x_name = 'x_variable'
    y_name = 'y_variable'
    with Dataset(file_path, 'w') as dataset:
        dataset.createDimension(x_name, 4)
        dataset.createDimension(y_name, 4)
        dataset.createVariable(x_name, np.int64, (x_name,))
        dataset.createVariable(y_name, np.int64, (y_name,))
    parser = VariablesGridParser(file_path, x_variable=x_name, y_variable=y_name, z_variable=None)
    grid = parser.get()
    assert list(grid.grid_dimensions.keys()) == [x_name, y_name]
    assert len(grid.points) == 16


def test_concat_grid_parser_1(netcdf_data_path):
    file_path = netcdf_data_path.ecmwf_op_merged_vik
    parser = VariablesGridParser(file_path, x_variable='x', y_variable='y', z_variable='z')
    grid = parser.get()
    assert len(grid.points) == 4


def test_concat_time_axes_parser(netcdf_data_path):
    file_path = netcdf_data_path.ecmwf_op_merged_vik
    parser = TimeAxesDimensions(file_path)
    time_axes = parser.get()

    assert len(time_axes.time_axes) == 8
    assert time_axes.time_axes[0].total_period() == \
           UtcPeriod(Calendar().time(2019, 8, 31), Calendar().time(2019, 9, 10, 6))


def test_data_dimensions(mock_ta):
    gpv = GeoPointVector([GeoPoint(0, 0, 0)])
    dims = DataDimensions(1,
                          EnsembleDimension('ens'),
                          HeightDimension(None),
                          GridDimension({'grid': 1}, gpv),
                          TimeAxisDimensions('time', None, [mock_ta]))
    ordered_dims = dims.get_ordered_dimensions()
    assert ordered_dims == ['ens', 'grid', 'time']


def test_height_variable_parser(tmp_path, mock_ts):
    dims = DataDimensions(1,
                          EnsembleDimension('ens'),
                          HeightDimension('height', [10., 50]),
                          GridDimension({'grid': 1}, GeoPointVector([GeoPoint(0, 0, 0)])),
                          TimeAxisDimensions('time', None, [mock_ts.time_axis]))

    file_path = Path(tmp_path) / 'test_h_var.nc'
    with Dataset(file_path, 'w') as dataset:
        dataset.createDimension('time', len(mock_ts))
        dataset.createDimension('grid', 1)
        dataset.createDimension('height', 2)
        dataset.createVariable('wind', np.int64, ('grid', 'height', 'time', ))
        dataset['wind'][0][0] = mock_ts.values
        dataset['wind'][0][1] = np.array(mock_ts.values) + 1.0

    hvp = HeightVariableParser(file_path, dims, {'wind': WindSpeedSources})
    hvs = hvp.get()
    assert len(hvs) == 2

    hdvp = HeightDependentVariableParser(file_path, dims, hvs)
    var_types = hdvp.get()
    assert len(var_types) == 2


def test_concat_variable_parser(netcdf_data_path):
    for file_path, shape in [(netcdf_data_path.ecmwf_op_merged_vik, GeoMatrixShape(n_t0=8, n_e=1, n_v=9, n_g=4)),
                             (netcdf_data_path.ecmwf_ens_merged_vik_red, GeoMatrixShape(n_t0=5, n_e=51, n_v=7, n_g=1)),
                             (netcdf_data_path.arome_test2, GeoMatrixShape(n_t0=1, n_e=1, n_v=1, n_g=95*24))]:
        data_parser = ConcatDataParser(file_path)
        gdb, gm = data_parser.get_geo_data('test')
        assert gm.shape == shape


def test_concat_geo_dtss_validate(dts_server, netcdf_data_path):
    url = f'localhost:{dts_server.get_listening_port()}'
    valid_parser = ConcatGeoDtss(netcdf_data_path.ecmwf_op_merged_vik, url, 'v1')
    valid_parser.validate()

    invalid_file = netcdf_data_path.repo_path / 'not_a_file.nc'
    parser = ConcatGeoDtss(invalid_file, url, 'not_a_file')
    with pytest.raises(RuntimeError):
        parser.validate()

    invalid_url = f'localhost:{dts_server.get_listening_port() + 10}'
    parser = ConcatGeoDtss(netcdf_data_path.ecmwf_op_merged_vik, invalid_url, 'v2')
    with pytest.raises(RuntimeError):
        parser.validate()




