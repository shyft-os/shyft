from builtins import range

from shyft.time_series import (time)
from shyft.hydrology import (GeoCellData, GeoCellDataVector, LandTypeFractions, GeoPoint, GeoPointVector)
import numpy as np
from numpy.testing import assert_array_almost_equal

"""
Some basic test to ensure that the exposure of c++ vector<T> is working as expected
"""

def test_geo_cell_data_vector():
    gcdv = GeoCellDataVector()
    for i in range(5):
        p = GeoPoint(100, 200, 300)
        ltf = LandTypeFractions()
        ltf.set_fractions(glacier=0.1, lake=0.1, reservoir=0.1, forest=0.1)
        gcd = GeoCellData(p, 1000000.0, i, 0.9, ltf)
        gcdv.append(gcd)
    assert len(gcdv) == 5
    for i in range(5):
        assert gcdv[i].catchment_id() == i
    g2 = GeoCellDataVector(gcdv)  # copy construct a new
    assert g2 == gcdv
    g2[0].set_catchment_id(10)
    assert g2 != gcdv
    # serialize
    gcdv_s = gcdv.serialize()
    assert len(gcdv_s) > 2
    gcdv_deserialized = GeoCellDataVector.deserialize(gcdv_s)
    assert gcdv_deserialized is not None
    assert gcdv_deserialized == gcdv
