from shyft.hydrology import (GoalFunctionServer, GoalFunctionClient, GoalFunctionModel,
                             TargetSpecificationPts, TargetSpecificationVector,
                             TargetSpecCalcType, CatchmentPropertyType)
from shyft.time_series import time, ModelInfo, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE


def mk_goal_function_example(n: int = 10) -> TargetSpecificationVector:
    return TargetSpecificationVector([
        TargetSpecificationPts(
            TimeSeries(TimeAxis(time('2024-02-20T12:00:00Z'), time(3600), 100), float(3), POINT_AVERAGE_VALUE),  # must be bound for now..
            list([i]),
            0.7 + float(i) / 100,
            TargetSpecCalcType.KLING_GUPTA, 1.0, 1.0, 1.0,
            CatchmentPropertyType.DISCHARGE,
            f'shyft://hydrology/catchment/{i}/observed_discharge')
        for i in range(n)]
    )


def test_goal_function_server_basics(tmpdir):
    model_dir = (tmpdir / "t_goal_function_srv_basics")
    if not model_dir.exists():
        model_dir.mkdir()
    s = GoalFunctionServer(root_dir=str(model_dir))  # just tell the Server where to store it's model.
    try:
        # s.set_listening_port(10000) # you can specify the server port in real life, but in test, we autoallocate ports to avoid conflicts
        port = s.start_server()  # if you do not tell  the server, it will find a free port
        assert port > 0
        all_infos = list()  # specify empty to get all infos
        c = GoalFunctionClient(host_port=f'localhost:{port}', timeout_ms=1000)
        m_infos = c.get_model_infos(all_infos)
        assert len(m_infos) == 0
        m = GoalFunctionModel()
        m.goal_functions = mk_goal_function_example(n=100)
        m_info = ModelInfo(id=0, name='example model', created=time.now(), json='{"descript":"any thing usefule for filtering goes here"}')
        mid = c.store_model(m, m_info)  # here we store the model, with the decorated m_info, that we can later use for filtering
        m_infos = c.get_model_infos(all_infos)  # we can specify spesific infos, or empty typed list will give all
        assert len(m_infos) == 1
        assert m_infos[0].id == mid  # because that's the only model we have stored so far
        mx = c.read_model(mid=mid)
        mxv = c.read_models([mid])  # you can read many models in one go(save time!)
        assert len(mxv) == 1
        mx1 = mxv[0]
        assert mx.id == mx1.id  # should give same model in this case
        assert mx == mx1  # can compare the model
        # update the model info (not the model, but associated info)
        m_info.json = '{"labels":["operative","pt_gs_k","nea-nidelv"]}'
        m_info.id = mid  # required, extra check, the mi.id must equal the mid=mid, below
        c.update_model_info(mid=mid, mi=m_info)
        m_infos = c.get_model_infos(all_infos)
        assert m_info.json == m_infos[0].json
        c.remove_model(mid=mid)
        assert len(c.get_model_infos(all_infos)) == 0  # now there should be zero left.
        c.close()
    finally:
        s.stop_server()


if __name__ == '__main__':
    from pathlib import Path

    test_goal_function_server_basics(tmpdir=Path("/tmp"))
