#include <unordered_map>

#include <shyft/dtss/dtss_subscription.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/dtss/ts_subscription_read.h>
#include <shyft/time_series/dd/compute_ts_vector.h>

#include "test_pch.h"

namespace shyft {

  using namespace shyft::core;
  using namespace shyft::time_axis;
  using namespace shyft::time_series::dd;

  using std::vector;
  using std::make_shared;
  using std::forward;
  using std::map;
  using std::string;
  // using std::string_view;
  using std::to_string;
  using std::runtime_error;
  using std::end;
  using std::unordered_map;
  using std::array;
  using std::recursive_mutex;

  using std::scoped_lock;
  using std::begin;
  using std::end;
  using std::atomic_int64_t;

  namespace test::dtss {
    // NOTE: this is just example code to study the mechanisms
    //       that we later implement on time_series::dd ipoint_ts and friends
    //
    string shyft_url(string ts_name) {
      return string{"shyft://test/"} + ts_name;
    }

    ats_vector make_test_tsv(size_t n_ts) {
      ats_vector r;
      calendar utc;
      gta_t ta{utc.time(2019, 10, 1), deltahours(1), 24};
      for (size_t i = 0; i < n_ts; ++i)
        r.emplace_back(3.0 * apoint_ts{shyft_url(to_string(i))});
      return r;
    }

  }

  using namespace test::dtss;
  using namespace shyft::dtss::subscription;

  TEST_SUITE_BEGIN("dtss");
  using shyft::core::subscription::manager;

  TEST_CASE("dtss/subscription_manager/expression_observer") {
    auto tsv = make_test_tsv(3);
    vector<vector<ts_bind_info>> ts_terminals;

    // 0.  using already tested functions to extract the unbound terminals
    for (auto const &ts : tsv)
      ts_terminals.emplace_back(ts.find_ts_bind_info());
    REQUIRE_EQ(ts_terminals.size(), tsv.size());

    for (auto const &bi : ts_terminals) {
      CHECK_EQ(1, bi.size());
    }
    // -- now test the subscription_manager :

    auto sm = make_shared<manager>();
    auto ts_value = 1.0;
    auto fx = [&ts_value](ats_vector expr) -> ats_vector {
      calendar utc;
      gta_t ta{utc.time(2019, 10, 1), deltahours(1), 24};
      for (auto &ts : expr) {
        auto biv = ts.find_ts_bind_info();
        for (auto &bi : biv)
          bi.ts.bind(apoint_ts{ta, ts_value, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE});
        ts.do_bind();
      }
      return ats_vector{deflate_ts_vector<apoint_ts>(expr)}; // compute and flatten expression
    };
    size_t master_notify_count = 0; // we count number of master cb here
    sm->master_changed_cb = [&master_notify_count]() {
      ++master_notify_count;
    }; // each real change should propagate here
    REQUIRE_EQ(0u, sm->total_change_count.load());
    CHECK_EQ(sm->total_unsubscribe_count.load(), 0u);
    string request_id{"1"};
    /* sm and interaction with ts_expression_observer */ {
      auto o = make_shared<ts_expression_observer>(sm, request_id, tsv, fx); // observe these expressions.
      REQUIRE_EQ(true, o->has_changed());
      CHECK_EQ(true, o->recalculate());
      // o->published_version=o->terminal_version();
      REQUIRE_EQ(false, o->has_changed());
      vector<string> ts_c01{shyft_url("0"), shyft_url("1")};
      vector<string> ts_unrelated{shyft_url("9")};
      sm->notify_change(ts_unrelated);
      REQUIRE_EQ(0u, master_notify_count); // nothing should change here
      CHECK_EQ(false, o->has_changed());   // nothing should happen
      sm->notify_change(ts_c01);
      CHECK_EQ(true, o->has_changed());    // now it should be changed.
      REQUIRE_EQ(1u, master_notify_count); // we should have 1 master cb
      ts_value = 2.0;
      CHECK_EQ(true, o->recalculate());
      sm->notify_change(ts_c01);         // propagate a change once more
      CHECK_EQ(false, o->recalculate()); // second time, no change since last published view
      CHECK_EQ(sm->total_unsubscribe_count.load(), 0u);

      REQUIRE_EQ(
        2u, master_notify_count); // we should have 2 master cb, because it was a change(even though not in the values)
      o = nullptr;                //
      CHECK_EQ(sm->active.size(), 0u); // no more subs at this point
      CHECK_EQ(sm->total_unsubscribe_count.load(), tsv.size());
    }
    sm = nullptr; // defuse everything.
  }

  TEST_CASE("dtss/subscription_manager/ts_observer") {

    auto sm = make_shared<manager>();
    size_t master_notify_count = 0; // we count number of master cb here
    sm->master_changed_cb = [&master_notify_count]() {
      ++master_notify_count;
    }; // each real change should propagate here
    REQUIRE_EQ(0u, sm->total_change_count.load());
    CHECK_EQ(sm->total_unsubscribe_count.load(), 0u);
    string request_id{"1"};

    /* sm and interaction with ts_observer */ {
      ts_observer o{sm, request_id};
      vector<string> ts_c01{shyft_url("0"), shyft_url("1")};
      utcperiod p1{from_seconds(10.0), from_seconds(20.0)};
      o.subscribe_to(ts_c01, p1);
      vector<string> ts_unrelated{shyft_url("9")};
      sm->notify_change(ts_unrelated);
      REQUIRE_EQ(0u, master_notify_count); // nothing should change here
      auto read_count = 0u;
      vector<utcperiod> read_period;
      vector<id_vector_t> read_ids;
      auto local_read_ts = [&](id_vector_t const &ids, utcperiod const &p) {
        read_count++;
        read_period.push_back(p);
        read_ids.push_back(ids);
        std::vector<ts_frag> r;
        std::vector<utcperiod> rp;
        shyft::time_axis::generic_dt ta{p.start, p.timespan(), 1};
        for (size_t i = 0u; i < ids.size(); ++i) {
          r.emplace_back(std::make_shared<gpoint_ts>(ta, 1.0, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE));
          rp.emplace_back(p);
        }
        return std::make_pair(r, rp);
      };
      auto [rr_tsv0, rr_p0] = read_changed_subs(o, local_read_ts);
      CHECK_EQ(true, rr_tsv0.empty());
      CHECK_EQ(true, rr_p0.empty());
      // test case of: one changed -> one read
      sm->notify_change(ts_c01[0]); //
      auto [rr_tsv1, rr_p1] = read_changed_subs(o, local_read_ts);
      CHECK_EQ(read_count, 1u);
      REQUIRE_EQ(read_ids.size(), 1u);
      CHECK_EQ(read_ids.back()[0], ts_c01[0]);
      CHECK_EQ(read_period.back(), p1);
      // test case: after read, no more changes.
      auto [rr_tsv2, rr_p2] = read_changed_subs(o, local_read_ts);
      CHECK_EQ(read_count, 1u); // no more calls. and
      CHECK_EQ(rr_tsv2.empty(), true);
      CHECK_EQ(rr_p2.empty(), true);
      // test case: if multiple subs, with different periods, we need requests
      // that are bulked using period as a key.

      utcperiod p2{from_seconds(5.0), utctime::max()}; // open end
      utcperiod p3{};                                  // empty, so entirely open.
      vector<string> ts_23{shyft_url("2"), shyft_url("3")};
      vector<string> ts_45{shyft_url("4"), shyft_url("5")};
      o.subscribe_to(ts_23, p2);
      o.subscribe_to(ts_45, p3);

      // notify
      sm->notify_change(ts_c01);
      sm->notify_change(ts_23);
      sm->notify_change(ts_45);
      // then read changed subs
      auto [rr_tsv3, rr_p3] = read_changed_subs(o, local_read_ts);
      // verify results (at some degree)
      CHECK_EQ(read_count, 1u + 3u);
      CHECK_EQ(rr_tsv3.size(), 6);
      CHECK_EQ(rr_p3.size(), 6);
      // verify behaviour, the order could be different. - but it seems to work out
      REQUIRE_EQ(read_period.size(), 4);
      for (size_t i = 1; i < read_period.size(); ++i) {
        auto px = read_period[i];
        std::ranges::sort(read_ids[i]);
        if (px == p1) {
          CHECK_EQ(read_ids[i], ts_c01);
        } else if (px == p2) {
          CHECK_EQ(read_ids[i], ts_23);
        } else if (px == p3) {
          CHECK_EQ(read_ids[i], ts_45);
        } else {
          MESSAGE("read_changed_subs: failed to partition the read requests");
          CHECK_EQ(px, p3);
        }
      }
    }
    sm = nullptr; // defuse everything.
  }

  TEST_SUITE_END();

}
