#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_client.h>

#include <doctest/doctest.h>
#include <test_utils.h>

namespace shyft::dtss {
  using namespace std::chrono_literals;
  TEST_SUITE_BEGIN("dtss");

  TEST_CASE("dtss/stale_clients") {

    test::utils::temp_dir root{"shyft.dtss.stale_clients"};
    dtss::server_config cfg{
      .config = {.stale_duration = utctime{100ms}, .stale_sweep_interval = utctime{1ms}}
    };
    // we want to ensure that a connection does not become stale as long as server
    // is processing  a request.
    // thus: we use a callback that we control to solve that
    dtss::server_state s;
    auto processing_delay = 200ms;
    s.callbacks.find_ts = [&](auto&&) -> ts_info_vector_t {
      std::this_thread::sleep_for(processing_delay);
      return {};
    };
    dtss::binary_server b{s, cfg};
    auto p = b.start_server();
    dtss::client c{fmt::format("127.0.0.1:{}", p)};
    (void) c.get_server_version(); // make one connection
    while (s.alive_connections.load() == 1u)
      std::this_thread::sleep_for(30ms);
    CHECK_EQ(s.alive_connections.load(), 0u);
    CHECK_EQ(s.failed_connections.load(), 0u);
    CHECK_EQ(b.get_stale_close_count(), 1u);
    (void) c.get_server_version(); // re-connect
    CHECK_EQ(s.alive_connections.load(), 1u);
    //-- now issue a call that takes longer time to process, and verify
    //-- the connection is not killed while processing.
    (void) c.find("x://ternal/find");                         // will hit our callback above, and take some time.
    CHECK_EQ(b.get_stale_close_count(), 1u);                  // we should still have same stale count!
    (void) c.get_server_version();                            // re-connect(should use existing connect, no recon)
    CHECK_EQ(c.srv_con.back().connection.reconnect_count, 1); // and no reconnects
    c.close();
    while (s.alive_connections.load() == 1u) // takes a while to close server side
      std::this_thread::sleep_for(1ms);
    CHECK_EQ(s.alive_connections.load(), 0u);
    CHECK_EQ(b.get_stale_close_count(), 1u); // should still just be one.
    b.clear();
  }

  TEST_SUITE_END;

}
