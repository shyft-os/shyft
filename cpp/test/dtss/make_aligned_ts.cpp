#include <shyft/dtss/detail/make_aligned_ts.h>

#include <doctest/doctest.h>

namespace shyft::dtss::detail {


  using namespace shyft::time_series;
  using namespace shyft::core;
  using ta_type_t = time_axis::generic_dt::generic_type;
  using std::vector;
  using std::shared_ptr;
  using std::make_shared;
  using std::string;
  using std::runtime_error;

  TEST_SUITE_BEGIN("ts");

  namespace {

    auto lookup_calendar = [](string const &tz) {
      return make_shared<calendar>(tz);
    };

    ///< template type for the target requirement, aka rocks/level/file db ts description
    struct target_requirement {
      utctime t0;
      utctime dt;
      time_axis::generic_dt::generic_type ta_type;
      ts_point_fx point_fx{};
      string tz = {};
    };

    /**
     * @brief  make_aligned_ts_fixture
     * @detail
     * To keep tests short and with clear semantics
     */
    struct make_aligned_ts_fixture {
      utctime t0{0l};
      utctime t0_u{t0 + utctime{10000l}}; // unaligned t0
      utctime dt{calendar::DAY};
      utctime dt_u{3 * dt}; // unaligned dt
      gts_t f_ts{
        gta_t{ t0,  dt,   3},
        vector<double>{1.0, 2.0, 1.1},
        POINT_AVERAGE_VALUE
      };
      gts_t c_ts{
        gta_t{calendar::utc(), t0, dt, 3},
        vector<double>{1.0, 2.0, 1.1},
        POINT_AVERAGE_VALUE
      };
      gts_t p_ts{
        gta_t{vector<utctime>{t0, t0 + dt, t0 + dt * 2, t0 + dt * 3}},
        vector<double>{1.0, 2.0, 1.1},
        POINT_AVERAGE_VALUE
      };
      std::shared_ptr<calendar> utc = calendar::utc();
      bool const strict{true};
      bool const relaxd{false};

      // helper to make test readable with clang format
      target_requirement f_req{.t0 = t0, .dt = dt, .ta_type = ta_type_t::FIXED, .point_fx = POINT_AVERAGE_VALUE};

      target_requirement
        c_req{.t0 = t0, .dt = dt, .ta_type = ta_type_t::CALENDAR, .point_fx = POINT_AVERAGE_VALUE, .tz = "UTC"};

      target_requirement
        p_req{.t0 = t0, .dt = dt, .ta_type = ta_type_t::POINT, .point_fx = POINT_AVERAGE_VALUE, .tz = "UTC"};

      static std::optional<gts_t> make_(bool strict_align, target_requirement req, gts_t const &ats) {
        return make_aligned_ts(strict_align, req, lookup_calendar, ats);
      }

      // just to make tests shorter
      ta_type_t const FIXED{ta_type_t::FIXED};
      ta_type_t const CAL{ta_type_t::CALENDAR};
      ta_type_t const POINT{ta_type_t::POINT};
      ts_point_fx const step{POINT_AVERAGE_VALUE};
      ts_point_fx const linear{POINT_INSTANT_VALUE};
    };

  }

  using rte = runtime_error const &; ///< the exception type thrown on misalignemnt

  TEST_CASE_FIXTURE(make_aligned_ts_fixture, "dtss/make_aligned_ts/strict") {
    SUBCASE("same-time_axis") {
      CHECK_FALSE(make_(strict, f_req, f_ts).has_value());
      CHECK_FALSE(make_(strict, c_req, c_ts).has_value());
      CHECK_FALSE(make_(strict, p_req, p_ts).has_value());
    }
    SUBCASE("diff-time_axis-types") {
      CHECK_THROWS_AS(make_(strict, f_req, c_ts), rte);
      CHECK_THROWS_AS(make_(strict, c_req, f_ts), rte);
    }
    SUBCASE("ualigned-same-time_axis-type") {
      CHECK_THROWS_AS(make_(strict, {.t0 = t0, .dt = dt_u, .ta_type = FIXED}, f_ts), rte);
      CHECK_THROWS_AS(make_(strict, {.t0 = t0_u, .dt = dt, .ta_type = FIXED}, f_ts), rte);
      CHECK_THROWS_AS(make_(strict, {.t0 = t0, .dt = dt_u, .ta_type = CAL, .point_fx = step, .tz = "UTC"}, c_ts), rte);
      CHECK_THROWS_AS(make_(strict, {.t0 = t0_u, .dt = dt, .ta_type = CAL, .point_fx = step, .tz = "UTC"}, c_ts), rte);
      CHECK_THROWS_AS(make_(strict, {.t0 = t0, .dt = dt, .ta_type = CAL, .point_fx = step, .tz = "UTC+1"}, c_ts), rte);
    }
    SUBCASE("flexible-convert-to-point") {
      CHECK_FALSE(make_(strict, {.t0 = t0, .dt = dt, .ta_type = POINT, .point_fx = step}, p_ts).has_value());
      CHECK(make_(strict, p_req, f_ts).has_value()); // because it must convert to point.
      CHECK(make_(strict, p_req, c_ts).has_value()); // because it must convert to point.
    }
  }

  TEST_CASE_FIXTURE(make_aligned_ts_fixture, "dtss/make_aligned_ts/relaxed") {
    SUBCASE("same-time_axis") {
      CHECK_FALSE(make_(relaxd, f_req, f_ts).has_value());
      CHECK_FALSE(make_(relaxd, c_req, c_ts).has_value());
      CHECK_FALSE(make_(relaxd, p_req, p_ts).has_value());
    }
    SUBCASE("diff-time_axis-types") {
      CHECK(make_(relaxd, f_req, c_ts).has_value());
      CHECK(make_(relaxd, c_req, f_ts).has_value());
    }
    SUBCASE("ualigned-same-time_axis-type") {
      CHECK(make_(relaxd, {.t0 = t0, .dt = dt_u, .ta_type = FIXED}, f_ts).has_value());
      CHECK(make_(relaxd, {.t0 = t0_u, .dt = dt, .ta_type = FIXED}, f_ts).has_value());
      CHECK(make_(relaxd, {.t0 = t0, .dt = dt_u, .ta_type = CAL, .point_fx = step, .tz = "UTC"}, c_ts).has_value());
      CHECK(make_(relaxd, {.t0 = t0_u, .dt = dt, .ta_type = CAL, .point_fx = step, .tz = "UTC"}, c_ts).has_value());
      CHECK(make_(relaxd, {.t0 = t0, .dt = dt, .ta_type = CAL, .point_fx = step, .tz = "UTC+1"}, c_ts).has_value());
    }
    SUBCASE("flexible-convert-to-point") {
      CHECK_FALSE(make_(relaxd, {.t0 = t0, .dt = dt, .ta_type = POINT, .point_fx = step}, p_ts).has_value());
      CHECK(make_(relaxd, p_req, f_ts).has_value()); // because it must convert to point.
      CHECK(make_(relaxd, p_req, c_ts).has_value()); // because it must convert to point.
    }
  }

  TEST_SUITE_END();
}
