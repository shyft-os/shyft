#include <ranges>
#include <string>
#include <vector>

#include <fmt/format.h>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/common.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include <doctest/doctest.h>
#include <forge/create_message.h>
#include <forge/forged_version.h>
#include <test/dtss/file_path.h>
#include <test/test_utils.h>

TEST_SUITE_BEGIN("dtss");

namespace shyft::dtss {

  TEST_CASE("dtss/protocol") {

    auto versions = std::views::iota(min_version, version_type{min_version + num_versions});
    for (auto v : versions)
      SUBCASE(fmt::format("v{}", v).c_str()) {
        server_state s{};
        s.can_remove = true;
        test::utils::temp_dir default_container("shyft.def.ctr.");
        std::string c1("c1");
        add_container(s, std::string{""}, default_container.string());
        add_container(s, c1, (default_container / c1).string());
        auto ta = time_axis::generic_dt{
          std::vector<core::utctime>{core::utctime{0l}, core::utctime{10l}, core::utctime{20l}},
          core::utctime{30l}
        };
        auto ta_2 = time_axis::generic_dt{
          std::vector<core::utctime>{core::utctime{40l}, core::utctime{50l}, core::utctime{60l}},
          core::utctime{70l}
        };
        time_series::dd::apoint_ts c1a(ta, std::vector<double>{1, 2, 3}, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
        time_series::dd::apoint_ts c2a(
          ta_2, std::vector<double>{4, 5, 6}, time_series::ts_point_fx::POINT_AVERAGE_VALUE);

        std::string ts_name{"shyft://c1/a"};
        time_series::dd::ats_vector tsv{time_series::dd::apoint_ts(ts_name, c1a)};

        do_store_ts(s, tsv, store_policy{.recreate = false, .strict = true, .cache = false});

        binary_server binary{s};
        binary.set_listening_ip("127.0.0.1");
        int port_no = binary.start_server();
        std::string host_port = fmt::format("localhost:{}", port_no);
        client c(host_port, 1000);
        client c_v(host_port, 1000, v);

        CHECK(c.get_server_version() == c_v.get_server_version());
        CHECK(c.get_supported_protocols() == c_v.get_supported_protocols());
        CHECK(c.find("shyft://c1/.*") == c_v.find("shyft://c1/.*"));
        CHECK(c.get_ts_info(ts_name) == c_v.get_ts_info(ts_name));
        core::utcperiod p{core::utctime(0l), core::utctime(999l)};
        std::vector<std::int64_t> ps{22l};
        CHECK_THROWS(std::ignore = c_v.percentiles(tsv, p, ta, ps, true, true));
        CHECK_THROWS(std::ignore = c_v.evaluate(tsv, p, true, true));
        std::string name_manip = "shyft://c1/b";
        time_series::dd::ats_vector tsv_other{time_series::dd::apoint_ts(name_manip, c1a)};
        time_series::dd::ats_vector tsv_merge{time_series::dd::apoint_ts(name_manip, c2a)};
        auto sp = store_policy{.recreate = true, .strict = true, .cache = true};
        auto store_c = c.store_ts(tsv_other, sp);
        CHECK_NOTHROW(c_v.merge_store_ts(tsv_merge, false));
        CHECK_NOTHROW(c_v.remove(name_manip));
        CHECK(store_c == c_v.store_ts(tsv_other, sp));

        CHECK_THROWS(std::ignore = c_v.get_cache_stats());
        CHECK_THROWS(c_v.cache_flush());
        auto rr = c.try_read({ts_name, name_manip, "non_existant"}, ta.total_period(), false, false);
        auto rr_v1 = c_v.try_read({ts_name, name_manip, "non_existant"}, ta.total_period(), false, false);
        CHECK(std::ranges::all_of(std::views::zip(rr.result, rr_v1.result), [](auto&& v) {
          auto& [unv, vers] = v;
          if (unv == nullptr)
            return vers == nullptr;
          return unv->values() == vers->values();
        }));
        CHECK(rr.result_tp == rr_v1.result_tp);
        CHECK(rr.updates.size() == rr_v1.updates.size());
        CHECK(std::ranges::all_of(std::views::zip(rr.updates, rr_v1.updates), [](auto&& v) {
          auto& [unv, vers] = v;
          return unv.values() == vers.values();
        }));
        CHECK(rr.updates_tp == rr_v1.updates_tp);
        CHECK(std::ranges::all_of(std::views::zip(rr.errors, rr_v1.errors), [](auto&& v) {
          auto& [unv, vers] = v;
          return unv == vers;
        }));

        auto subs = c.read_subscription();
        auto subs_v1 = c_v.read_subscription();
        CHECK(std::ranges::all_of(std::views::zip(std::get<0>(subs), std::get<0>(subs_v1)), [](auto&& v) {
          auto& [unv, vers] = v;
          return unv.values() == vers.values();
        }));
        CHECK(std::get<1>(subs) == std::get<1>(subs_v1));
        CHECK_NOTHROW(c_v.unsubscribe({"shyft://c1/b"}));

        CHECK(c.get_container_names() == c_v.get_container_names());

        CHECK_NOTHROW(c_v.set_container("a", "a"));
        CHECK_NOTHROW(c_v.set_container("b", "b"));
        CHECK_NOTHROW(c_v.swap_container("a", "b"));
        CHECK_NOTHROW(c_v.remove_container("a"));
        CHECK_NOTHROW(c_v.remove_container("b"));

        CHECK_THROWS(std::ignore = c_v.geo_evaluate({}, false, false));
        CHECK_THROWS(c_v.geo_store("a", {}, false, false));
        CHECK_THROWS(std::ignore = c_v.get_geo_ts_db_info());
        CHECK_THROWS(c_v.add_geo_ts_db({}));

        std::string q_name = "something";
        std::string msg_id = "something";
        c.q_add(q_name);
        CHECK(c.q_list() == c_v.q_list());
        CHECK_NOTHROW(c_v.q_put(q_name, msg_id, "some descr", core::utctime(2l), tsv));
        CHECK(c.q_msg_info(q_name, msg_id) == c_v.q_msg_info(q_name, msg_id));
        CHECK(c.q_msg_infos(q_name) == c_v.q_msg_infos(q_name));

        auto msg = c.q_get(q_name, core::utctime(10l));
        CHECK_NOTHROW(c_v.q_ack(q_name, msg->info.msg_id, ""));
        CHECK_NOTHROW(c_v.q_maintain(q_name, false, true));
        CHECK_NOTHROW(c_v.q_put(q_name, msg_id, "some descr", core::utctime(2l), tsv));
        auto msg_v1 = c_v.q_get(q_name, core::utctime(10l));
        CHECK(msg->tsv == msg_v1->tsv);
        CHECK_THROWS(c_v.q_add("what"));
        CHECK_THROWS(c_v.q_remove(q_name));
        CHECK_THROWS(c_v.start_transfer({}));
        CHECK_THROWS(std::ignore = c_v.get_transfers());
        CHECK_THROWS(std::ignore = c_v.get_transfer_status("abc",true));
        CHECK_THROWS(c_v.stop_transfer("some",core::utctime(1l)));
        if (v > 1)
          CHECK(c_v.q_find(q_name, msg->info.msg_id));
      }
  }

  namespace detail {
    bool aref_compare(time_series::dd::aref_ts const & lhs, time_series::dd::aref_ts const & rhs) {
      if (lhs.id != rhs.id)
        return false;
      if (lhs.rep.get() == nullptr && rhs.rep.get() == nullptr)
        return true;
      if (lhs.rep.get() == nullptr || rhs.rep.get() == nullptr)
        return false;
      return lhs.rep->rep == rhs.rep->rep;
    }
#if defined(__GNUC__) && !defined(__clang__)
    // Concept to check if a type has static members `tag` and `version`
    template <typename T>
    concept HasTagAndVersion = requires {
      // Verify `tag` and `version` exist AND are static members:
      { T::tag } -> std::convertible_to<decltype(T::tag)>;
      { T::version } -> std::convertible_to<decltype(T::version)>;
    };

    template <typename reply>
    bool deep_compare(reply& lhs, reply& rhs) {
      constexpr auto msg = protocols::get_message(lhs);
      if constexpr (msg.tag == dtss::message_tags<msg.version>::TRY_SLAVE_READ) {
        if (
          std::tie(lhs.periods, lhs.period_updates, lhs.errors)
          != std::tie(rhs.periods, rhs.period_updates, rhs.errors))
          return false;
        if (!std::ranges::all_of(std::views::zip(lhs.fragments, rhs.fragments), [](auto const & p) {
              auto const& [l, r] = p;
              if (l.get() == nullptr && r.get() == nullptr)
                return true;
              if (l.get() == nullptr || r.get() == nullptr)
                return false;
              return l->rep == r->rep;
            }))
          return false;
        return std::ranges::all_of(std::views::zip(lhs.fragment_updates, rhs.fragment_updates), [](auto const & p) {
          auto const& [l, r] = p;
          return aref_compare(l, r);
        });
      } else if constexpr (msg.tag == dtss::message_tags<msg.version>::SLAVE_READ_SUBSCRIPTION) {
        if (lhs.periods != rhs.periods)
          return false;
        return std::ranges::all_of(std::views::zip(lhs.series, rhs.series), [](auto const & p) {
          auto const& [l, r] = p;
          return aref_compare(l, r);
        });
      } else
        return lhs == rhs;
    }
#endif
  }

#ifndef _MSC_VER
  /**
   * Test that previously persisted versions are still serializable,
   * note: ms c++ does not compile this since it fail to deduce that version arg to the lambda is a constexpr
   */
  TEST_CASE("dtss/protocol/forged_versions") {
    auto versions = std::views::iota(min_version, version_type{min_version + num_versions});
    for (auto const v : versions) {
      auto filename = fmt::format("{}/dtss_protocol_{}.bin", SHYFT_CORE_TEST_BINARY_FILES, v);
      std::fstream f(filename, f.binary | f.in);
      REQUIRE_MESSAGE(f.is_open(), fmt::format("File {} for version {} not found", filename, v));
      auto header = version_forge::versioned_protocol_header::read(f);
      protocols::with_version_or<shyft::dtss::protocol>(
        header.version,
        [&](auto version) {
          if constexpr (version != 0) {
            using tags = shyft::dtss::message_tags<version>;
            std::ranges::for_each(std::views::iota(std::size_t(0), header.n_messages), [&](auto t) {
              auto m = version_forge::message::read(f);
              with_enum_or(
                tags(t),
                [&](auto tag) {
                  constexpr auto msg = protocols::versioned_message<protocol, version>{tag.value};
                  {
                    auto request = protocols::request<msg>{};
                    auto exp_request = version_forge::get_request<msg>();
                    std::stringstream req{m.request};
                    auto ia = protocols::protocol_archives<msg>::make_iarchive(req);
                    ia >> request;
                    CHECK_MESSAGE(
                      request == exp_request,
                      fmt::format("Non equal request for tag={} version={}", tag.value, version.value));
                  }
                  {
                    auto reply = protocols::reply<msg>{};
                    auto exp_reply = version_forge::get_reply<msg>();
                    std::stringstream rep{m.reply};
                    auto ia = protocols::protocol_archives<msg>::make_iarchive(rep);
                    ia >> reply;
#if defined(__GNUC__) && !defined(__clang__)
                    CHECK_MESSAGE(
                      detail::deep_compare(reply, exp_reply),
                      fmt::format("Non equal reply for tag={} version={}", tag.value, version.value));
#endif
                  }
                },
                [&] {
                  REQUIRE_MESSAGE(false, fmt::format("tag {} not in tags for version {}", t, version.value));
                });
            });
          }
        },
        [](auto error) {
          auto error_message = [&] {
            if constexpr (get_error_tag(error) == shyft::protocols::protocol_stream_error_tag::invalid_version)
              return fmt::format("Version {} not supported by client", error.read_version);
            else
              return fmt::format("invalid error tag");
          }();
          REQUIRE_MESSAGE(false, error_message);
        });
    }
  }
#endif
}

TEST_SUITE_END();
