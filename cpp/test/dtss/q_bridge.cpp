#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/exchange/managers.h>

#include <test/test_pch.h>
#include <test/test_utils.h>


TEST_SUITE_BEGIN("dtss");
using namespace std::chrono_literals;

namespace shyft::dtss {

  namespace {
    struct q_bridge_fixture {
      server_state local{};
      binary_server local_bin{local};
      server_state remote{};
      binary_server remote_bin{remote};
      test::utils::temp_dir root{"shyft.dtss.xfer"};
      std::string queue_name{"local_q"};
      std::string remote_queue_name{"remote_q"};
      int l_port{0};
      int r_port{0};
      core::calendar utc{};
      utctime t0 = core::from_seconds(30);

      q_bridge_fixture() {
        add_container(local, "stm", (root / "local").string(), "ts_rdb", {});
        add_container(remote, "stm", (root / "remote").string(), "ts_rdb", {});
        l_port = local_bin.start_server();
        t0 = utc.time(2000, 1, 1);
      }

      ~q_bridge_fixture() {
      }

      auto make_test_tsv(size_t n, double value, size_t n_values = 3, utctime dt = core::from_seconds(3600)) const {
        ts_vector_t r;
        time_axis::generic_dt const ta{t0, dt, n_values};
        for (auto i = 0u; i < n; ++i)
          r.emplace_back(
            shyft_url("stm", fmt::format("r/{}.realised", i)),
            apoint_ts{ta, static_cast<double>(i) + value, time_series::POINT_AVERAGE_VALUE});
        return r;
      };
    };
  }

  TEST_CASE_FIXTURE(q_bridge_fixture, "exchange/queue/basics") {
    exchange::manager manager{.server = local};
    r_port = remote_bin.start_server();
    q_bridge::configuration config{
      .name = "test_db",
      .json = "{\"info\":\"test_exchange\"}",
      .where = {.host = "localhost", .port = r_port, .queue_name = remote_queue_name},
      .how =
        {.retries = 1,
                .sleep_before_retry = core::from_seconds(0.01),
                .queue_pick_up_max_wait = core::from_seconds(0.01)},
      .what = {.queue_name = queue_name, .ack_poll_interval = core::from_seconds(0.01)}
    };
    local.queue_manager.add(queue_name); // part of test setup!
    remote.queue_manager.add(remote_queue_name);
    auto local_q = local.queue_manager(queue_name);
    REQUIRE((local_q != nullptr));
    auto remote_q = remote.queue_manager(remote_queue_name);
    REQUIRE((remote_q != nullptr));
    manager.start_q_bridge(config);
    auto mirrors = manager.get_q_bridges();
    REQUIRE_EQ(mirrors.size(), 1);
    auto status = manager.get_q_bridge_status(config.name, true);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.completed, 0);
    CHECK_EQ(status.completed, 0);
    auto vec = make_test_tsv(10, 2);

    queue::tsv_msg tsv_msg;
    tsv_msg.tsv = vec;
    tsv_msg.info.ttl = t0;
    tsv_msg.info.description = "dumb";
    tsv_msg.info.msg_id = "0001";
    status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.fetched, 0);
    CHECK_EQ(status.completed, 0);

    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    while (manager.get_q_bridge_status(config.name, false).fetched == 0)
      std::this_thread::sleep_for(core::from_seconds(0.01));
    status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.completed, 0);
    CHECK_EQ(status.fetched, 1);


    auto tmp_msg = local_q->try_get();
    local_q->done(tmp_msg->info.msg_id, "Picked up msg");
    while (manager.get_q_bridge_status(config.name, false).completed == 0)
      std::this_thread::sleep_for(core::from_seconds(0.01));
    status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.fetched, 1);
    CHECK_EQ(status.completed, 1);

    auto local_remote_tag = fmt::format("[{}:{}:{}]{}", "localhost", r_port, remote_queue_name, tsv_msg.info.msg_id);
    auto remote_q_m = remote_q->find(tsv_msg.info.msg_id);
    auto local_q_m = local_q->find(local_remote_tag);
    REQUIRE((remote_q_m != nullptr));
    REQUIRE((local_q_m != nullptr));
    CHECK_NE(remote_q_m->info.msg_id, local_q_m->info.msg_id);
    CHECK_EQ(remote_q_m->info.diagnostics, local_q_m->info.diagnostics);
    CHECK_EQ(remote_q_m->tsv.size(), local_q_m->tsv.size());
    CHECK_EQ(remote_q_m->tsv.size(), tsv_msg.tsv.size());
    CHECK_EQ(remote_q_m->info.msg_id, tsv_msg.info.msg_id);

    tsv_msg.tsv = vec;
    tsv_msg.info.ttl = t0;
    tsv_msg.info.description = "dumb";
    tsv_msg.info.msg_id = "0002";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);

    while (manager.get_q_bridge_status(config.name, false).fetched == 1)
      std::this_thread::sleep_for(core::from_seconds(0.01));
    status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.completed, 1);
    CHECK_EQ(status.fetched, 2);

    tsv_msg.tsv = vec;
    tsv_msg.info.ttl = t0;
    tsv_msg.info.description = "dumb";
    tsv_msg.info.msg_id = "0001";
    local_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    tmp_msg = local_q->try_get();
    REQUIRE((tmp_msg != nullptr));
    local_q->done(tmp_msg->info.msg_id, "Picked up msg");
    while (manager.get_q_bridge_status(config.name, false).completed == 1)
      std::this_thread::sleep_for(core::from_seconds(0.01));

    status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.completed, 2);
    CHECK_EQ(status.fetched, 2);

    tmp_msg = local_q->try_get();
    REQUIRE((tmp_msg != nullptr));
    local_q->done(tmp_msg->info.msg_id, "Picked up msg");
    auto find = local_q->find(tsv_msg.info.msg_id);
    while (!find && find->info.done == no_utctime)
      std::this_thread::sleep_for(core::from_seconds(0.01));
    status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.completed, 2);
    CHECK_EQ(status.completed, 2);
    manager.stop_q_bridge(config.name, core::from_seconds(5.0));
  }

  TEST_CASE_FIXTURE(q_bridge_fixture, "exchange/queue/multiple_msgs") {
    exchange::manager manager{.server = local};
    r_port = remote_bin.start_server();
    q_bridge::configuration config{
      .name = "test_db",
      .where = {.host = "localhost", .port = r_port, .queue_name = remote_queue_name},
      .how =
        {.retries = 3,
                .sleep_before_retry = core::from_seconds(0.001),
                .queue_pick_up_max_wait = core::from_seconds(0.01)},
      .what = {.queue_name = queue_name, .ack_poll_interval = core::from_seconds(0.01)}
    };

    auto vec = make_test_tsv(10, 2);
    queue::tsv_msg tsv_msg;
    tsv_msg.tsv = vec;
    tsv_msg.info.ttl = t0;
    tsv_msg.info.description = "dumb";
    tsv_msg.info.msg_id = "0001";
    remote.queue_manager.add(remote_queue_name);
    local.queue_manager.add(queue_name);
    auto local_q = local.queue_manager(queue_name);
    REQUIRE((local_q != nullptr));
    auto remote_q = remote.queue_manager(remote_queue_name);
    REQUIRE((remote_q != nullptr));

    manager.start_q_bridge(config);

    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    while (manager.get_q_bridge_status(config.name, false).fetched != 1)
      std::this_thread::sleep_for(core::from_seconds(0.01));

    auto tmp_msg = local_q->try_get();
    local_q->done(tmp_msg->info.msg_id, "Picked up msg");

    tsv_msg.info.msg_id = "0002";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    tsv_msg.info.msg_id = "0003";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    tsv_msg.info.msg_id = "0004";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    tsv_msg.info.msg_id = "0005";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    tsv_msg.info.msg_id = "0006";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    tsv_msg.info.msg_id = "0007";
    remote_q->put(tsv_msg.info.msg_id, tsv_msg.info.description, tsv_msg.info.ttl, vec);
    while (manager.get_q_bridge_status(config.name, false).fetched != 7)
      std::this_thread::sleep_for(core::from_seconds(0.01));

    auto tmp_msg_2 = local.queue_manager(queue_name)->try_get();
    auto tmp_msg_3 = local.queue_manager(queue_name)->try_get();
    auto tmp_msg_4 = local.queue_manager(queue_name)->try_get();
    auto tmp_msg_5 = local.queue_manager(queue_name)->try_get();
    auto tmp_msg_6 = local.queue_manager(queue_name)->try_get();
    auto tmp_msg_7 = local.queue_manager(queue_name)->try_get();

    local_q->done(tmp_msg->info.msg_id, "Picked up msg");
    local_q->done(tmp_msg_3->info.msg_id, "Picked up msg");
    local_q->done(tmp_msg_5->info.msg_id, "Picked up msg");

    while (manager.get_q_bridge_status(config.name, false).completed != 3)
      std::this_thread::sleep_for(core::from_seconds(0.01));


    auto status = manager.get_q_bridge_status(config.name, false);
    CHECK_EQ(status.ack_errors, 0);
    CHECK_EQ(status.fetch_errors, 0);
    CHECK_EQ(status.completed, 3);
    CHECK_EQ(status.fetched, 7);

    auto mk_local_remote_tag = [&](auto msg_id) {
      return fmt::format("[{}:{}:{}]{}", "localhost", r_port, remote_queue_name, msg_id);
    };
    auto msg_id = "0001";
    auto local_remote_tag = mk_local_remote_tag(msg_id);
    auto remote_msg = remote_q->find(msg_id);
    auto local_mirrored_msg = local_q->find(local_remote_tag);
    REQUIRE((remote_msg != nullptr));
    REQUIRE((local_mirrored_msg != nullptr));
    CHECK_NE(remote_msg->info.msg_id, local_mirrored_msg->info.msg_id);
    CHECK_EQ(remote_msg->info.diagnostics, local_mirrored_msg->info.diagnostics);
    CHECK_EQ(remote_msg->tsv.size(), local_mirrored_msg->tsv.size());

    msg_id = "0003";
    local_remote_tag = mk_local_remote_tag(msg_id);
    remote_msg = remote_q->find(msg_id);
    local_mirrored_msg = local_q->find(local_remote_tag);
    REQUIRE((remote_msg != nullptr));
    REQUIRE((local_mirrored_msg != nullptr));
    CHECK_NE(remote_msg->info.msg_id, local_mirrored_msg->info.msg_id);
    CHECK_EQ(remote_msg->info.diagnostics, local_mirrored_msg->info.diagnostics);
    CHECK_EQ(remote_msg->tsv.size(), local_mirrored_msg->tsv.size());

    msg_id = "0005";
    local_remote_tag = mk_local_remote_tag(msg_id);
    remote_msg = remote_q->find(msg_id);
    local_mirrored_msg = local_q->find(local_remote_tag);
    REQUIRE((remote_msg != nullptr));
    REQUIRE((local_mirrored_msg != nullptr));
    CHECK_NE(remote_msg->info.msg_id, local_mirrored_msg->info.msg_id);
    CHECK_EQ(remote_msg->info.diagnostics, local_mirrored_msg->info.diagnostics);
    CHECK_EQ(remote_msg->tsv.size(), local_mirrored_msg->tsv.size());
    manager.stop_q_bridge(config.name, core::from_seconds(5));
  }

  namespace {
    auto make_dummy_ts(size_t n, double value, size_t n_values = 3, utctime dt = core::from_seconds(3600)) {
      ts_vector_t r;
      utctime t0 = core::from_seconds(30);
      time_axis::generic_dt const ta{t0, dt, n_values};
      for (auto i = 0u; i < n; ++i)
        r.emplace_back(
          shyft_url("stm", fmt::format("r/{}.realised", i)),
          apoint_ts{ta, static_cast<double>(i) + value, time_series::POINT_AVERAGE_VALUE});
      return r;
    }

    auto create_dummy_msg(std::size_t unique_id) {
      utctime t0 = core::from_seconds(6000);
      queue::tsv_msg tsv_msg;
      tsv_msg.tsv = make_dummy_ts(10, 2);
      tsv_msg.info.ttl = t0;
      tsv_msg.info.description = "dumb";
      tsv_msg.info.msg_id = fmt::format("000{}", unique_id);
      return tsv_msg;
    }

    auto queue_msg_pusher(server_state &server_state, std::string const &queue_name, std::stop_token token) {
      std::size_t unique_id{0};
      auto queue = server_state.queue_manager(queue_name);
      while (!token.stop_requested()) {
        auto msg = create_dummy_msg(unique_id++);
        queue->put(msg.info.msg_id, msg.info.description, msg.info.ttl, msg.tsv);
        std::this_thread::sleep_for(core::from_seconds(0.01));
      }
    }

    auto queue_msg_acker(
      server_state &server_state,
      bool double_ack,
      std::string const &queue_name,
      std::stop_token token) {
      auto queue = server_state.queue_manager(queue_name);
      while (!token.stop_requested()) {
        auto msg = queue->try_get();
        if (!msg)
          continue;
        std::this_thread::sleep_for(core::from_seconds(0.005));
        queue->done(msg->info.msg_id, "Picked up msg");
        // since we are double adding when running mirror setup, want to do ack twice for proper check
        if (double_ack) {
          auto msg_1 = queue->try_get();
          if (!msg_1)
            continue;
          std::this_thread::sleep_for(core::from_seconds(0.005));
          queue->done(msg_1->info.msg_id, "Picked up msg");
        }
        std::this_thread::sleep_for(core::from_seconds(0.01));
      }
    }
  }

  TEST_CASE("exchange/queue/stress") {
    server_state local{};
    binary_server local_server{local};
    server_state remote{};
    binary_server remote_server{remote};
    server_state local_1;
    test::utils::temp_dir root{"shyft.dtss.xfer"};
    std::stop_source stop_src;
    std::stop_source stop_src2;
    auto remote_port = remote_server.start_server();
    auto local_port = local_server.start_server();
    std::string queue_name{"q1"};
    local.queue_manager.add(queue_name);
    remote.queue_manager.add(queue_name);
    local_1.queue_manager.add(queue_name);

    q_bridge::configuration config{
      .name = "test_db",
      .where = {.host = "localhost", .port = remote_port, .queue_name = queue_name},
      .how =
        {.retries = 1,
                .sleep_before_retry = core::from_seconds(0.01),
                .queue_pick_up_max_wait = core::from_seconds(0.01)},
      .what = {.queue_name = queue_name, .ack_poll_interval = core::from_seconds(0.01)}
    };

    exchange::manager manager{.server = local};

    std::jthread t1{queue_msg_pusher, std::ref(local), queue_name, stop_src.get_token()};
    std::jthread t2{queue_msg_pusher, std::ref(remote), queue_name, stop_src.get_token()};
    std::jthread t3{queue_msg_acker, std::ref(local), true, queue_name, stop_src.get_token()};
    std::jthread t4{queue_msg_pusher, std::ref(local_1), queue_name, stop_src2.get_token()};
    std::jthread t5{queue_msg_acker, std::ref(local_1), false, queue_name, stop_src2.get_token()};
    manager.start_q_bridge(config);
    auto started = core::utctime_now();

    while (local_1.queue_manager(queue_name)->get_full_size() < 100)
      std::this_thread::sleep_for(core::from_seconds(0.01));
    (void) stop_src2.request_stop();
    auto normal_ended = core::utctime_now();


    while (manager.get_q_bridge_status(config.name, false).fetched != 100)
      std::this_thread::sleep_for(core::from_seconds(0.01));

    CHECK_EQ(manager.get_q_bridge_status(config.name, false).fetched, 100);
    while (manager.get_q_bridge_status(config.name, false).completed != 100)
      std::this_thread::sleep_for(core::from_seconds(0.01));
    CHECK_EQ(manager.get_q_bridge_status(config.name, false).completed, 100);
    (void) stop_src.request_stop();
    auto mirror_ended = core::utctime_now();
    manager.stop_q_bridge(config.name, core::from_seconds(5));

    MESSAGE(
      fmt::format(
        "Normal queue with 1 thread to add and ack with 100msg took {}s", core::to_seconds(normal_ended - started)));
    MESSAGE(
      fmt::format(
        "Mirror setup with 1 thread adding to local, 1 thread adding to remote, and 1 thread acking with acking twice "
        "in loop from remote with 100msg took {}s",
        core::to_seconds(mirror_ended - started)));
  }

  TEST_SUITE_END();
}