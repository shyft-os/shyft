#include <algorithm>
#include <cstdint>
#include <future>
#include <mutex>
#include <regex>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/bin_op.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/fx_merge.h>
#include <shyft/time_series/ref.h> // needed for the bind ops.
#include <shyft/time_series/time_axis.h>

#include "test_pch.h"
#include <test/test_utils.h>
#ifdef _WIN32
#include <io.h>
#else
#ifdef __APPLE__
#include <sys/uio.h>
#else
#include <sys/io.h>
#define O_BINARY 0
#define O_SEQUENTIAL 0
#include <sys/stat.h>
#endif
#endif
#include <armadillo>
#include <string_view>

#include <fcntl.h>

namespace shyft::dtss {

  using namespace std;
  using namespace shyft::core;
  using time_series::dd::apoint_ts;
  using time_series::dd::gta_t;
  using namespace std::chrono_literals;

  namespace {
    auto const stair_case = time_series::POINT_AVERAGE_VALUE;

    inline core::utctime _t(int64_t t1970s) {
      return core::utctime{core::seconds(t1970s)};
    }

    apoint_ts mk_expression(utctime t, utctimespan dt, int n) {
      std::vector<double> x;
      x.reserve(n);
      for (int i = 0; i < n; ++i)
        x.push_back(-double(n) / 2.0 + i);
      apoint_ts aa(gta_t(t, dt, n), x);
      auto a = aa * 3.0 + aa;
      return a;
    }

    auto construct_ts_frag = [](auto &&ta, auto &&v, time_series::ts_point_fx pfx) {
      return make_shared<time_series::dd::gpoint_ts const>(SHYFT_FWD(ta), SHYFT_FWD(v), pfx);
    };
    auto gross_period = [](utcperiod const &a, utcperiod const &b) {
      return utcperiod{std::min(a.start, b.start), std::max(a.end, b.end)};
    };

    bool equal_ts_frag(ts_frag const &a, ts_frag const &b) {
      return apoint_ts{a} == apoint_ts{b};
    }

    struct container_wrapper {

      using gta_t = time_axis::generic_dt;
      using gts_t = time_series::point_ts<gta_t>;
      // -----
      using container_adt = std::unique_ptr<its_db>;
      // -----
      container_adt _container;

      container_wrapper() = default;
      ~container_wrapper() = default;

      // -----
      template < class CIMPL >
      container_wrapper(std::unique_ptr<CIMPL> &&c)
        : _container{std::forward<std::unique_ptr<CIMPL>>(c)} {
      }

      container_wrapper(container_wrapper &&) = default;
      container_wrapper &operator=(container_wrapper &&) = default;

      /* Container API
       * ------------- */

     public:
      /** Save a time-series to the container. */
      void save(std::string const &tsid, gts_t const &ts, bool overwrite = true) {
        (void) _container->save(tsid, ts, nullptr, {.recreate = true});
      }

      /** Read a period from a time-series from the container. */
      gts_t read(std::string const &tsid, core::utcperiod period) {
        return std::get<0>(_container->read(tsid, period));
      }

      void remove(std::string const &tsid) {
        return _container->remove(tsid);
      }

      ts_info get_ts_info(std::string const &tsid) {
        return _container->get_ts_info(tsid);
      }

      /** Find minimal information about all time-series stored in the container matching a regex pattern. */
      std::vector<ts_info> find(std::string const &pattern) {
        return _container->find(pattern);
      }
    };
  }

  dlib::logger dlog("dlib.log");

#define TEST_SECTION(x)
  TEST_SUITE_BEGIN("dtss");

  TEST_CASE("dtss/cache_stats") {
    using dtss::cache_stats;
    cache_stats cs;
    CHECK_EQ(cs.id_count, 0);
    CHECK_EQ(cs.point_count, 0);
    CHECK_EQ(cs.fragment_count, 0);
    CHECK_EQ(cs.hits, 0);
    CHECK_EQ(cs.misses, 0);
    CHECK_EQ(cs.coverage_misses, 0);
    CHECK_EQ(cs.size_in_bytes(), 0);
    cs.id_count = 1;
    cs.point_count = 10;
    CHECK_EQ(cs.size_in_bytes(), 8 * cs.point_count);
    cs.hits = 1;
    cs.misses = 2;
    cs.coverage_misses = 4;
    cs.reset_stats();
    CHECK_EQ(cs.hits, 0);
    CHECK_EQ(cs.misses, 0);
    CHECK_EQ(cs.coverage_misses, 0);
    cs.hits = 1;
    cs.misses = 2;
    cs.coverage_misses = 4;
    cs.flush();
    CHECK_EQ(cs.hits, 1);
    CHECK_EQ(cs.misses, 2);
    CHECK_EQ(cs.coverage_misses, 4);
  }

  TEST_CASE("dtss/lru_cache") {
    using dtss::lru_cache;
    using std::map;
    using std::list;
    using std::vector;
    using std::string;
    using std::back_inserter;
    using time_series::dd::apoint_ts;
    using time_series::dd::gta_t;
    vector<apoint_ts> evicted_items;
    lru_cache<string, apoint_ts, map > c(2, [&evicted_items](apoint_ts const &x) {
      evicted_items.push_back(x);
    });

    apoint_ts r;
    vector<string> mru;
    gta_t ta(_t(0), 1s, 10);

    TEST_SECTION("empty_cache") {
      CHECK_UNARY_FALSE(c.try_get_item("a", r));
    }
    TEST_SECTION("add_one_item") {
      c.add_item("a", apoint_ts(ta, 1.0, stair_case));
      CHECK_UNARY(c.try_get_item("a", r));
      CHECK_EQ(ta.size(), r.time_axis().size());
      CHECK_UNARY_FALSE(c.try_get_item("b", r));
    }
    TEST_SECTION("add_second_item") {
      c.add_item("b", apoint_ts(ta, 2.0, stair_case));
      CHECK_UNARY(c.try_get_item("a", r));
      CHECK_UNARY(c.try_get_item("b", r));
      c.get_mru_keys(back_inserter(mru));
      CHECK_EQ(string("b"), mru[0]);
      CHECK_EQ(string("a"), mru[1]);
    }
    TEST_SECTION("mru_item_in_front") {
      c.try_get_item("a", r);
      mru.clear();
      c.get_mru_keys(back_inserter(mru));
      CHECK_EQ(string("a"), mru[0]);
      CHECK_EQ(string("b"), mru[1]);
    }
    TEST_SECTION("excessive_lru_item_evicted_when_adding") {
      c.add_item("c", apoint_ts(ta, 3.0, stair_case));
      CHECK_UNARY_FALSE(c.try_get_item("b", r));
      CHECK_UNARY(c.try_get_item("c", r));
      CHECK_UNARY(c.try_get_item("a", r));
      CHECK_EQ(evicted_items.size(), 1);
    }
    TEST_SECTION("remove_item") {
      c.remove_item("a");
      CHECK_UNARY_FALSE(c.try_get_item("a", r));
      CHECK_EQ(evicted_items.size(), 2);
    }
    TEST_SECTION("ensure_items_added_are_first") {
      c.add_item("d", apoint_ts(ta, 4.0, stair_case));
      mru.clear();
      c.get_mru_keys(back_inserter(mru));
      CHECK_EQ(string("d"), mru[0]);
      CHECK_EQ(string("c"), mru[1]);
      CHECK_EQ(evicted_items.size(), 2);
    }
    TEST_SECTION("update_existing") {
      c.try_get_item("c", r);                          // just to ensure "c" is in first position
      c.add_item("d", apoint_ts(ta, 4.2, stair_case)); // update "d"
      c.try_get_item("d", r);
      CHECK_GT(r.value(0), 4.1);
      mru.clear();
      c.get_mru_keys(back_inserter(mru));
      CHECK_EQ(string("d"), mru[0]);
      CHECK_EQ(string("c"), mru[1]);
      CHECK_EQ(evicted_items.size(), 2);
    }
  }

  TEST_CASE("dtss/ts_cache/apoint_ts_frag/merge") {
    using namespace time_series::dd;
    using time_series::point_ts;

    time_axis::point_dt pta{vector<utctime>{utctime{0l}}, utctime{10l}};
    time_axis::point_dt ptb{vector<utctime>{utctime{2l}}, utctime{4l}};
    time_axis::point_dt ptr{
      vector<utctime>{utctime{0l}, utctime{2l}, utctime{4l}},
      utctime{10l}
    };

    gts_t a(gta_t(pta), 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    gts_t b(gta_t(ptb), 0.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    gts_t r(gta_t(ptr), vector<double>{1.0, 0.0, 1.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    auto rx = time_series::merge(b, a);
    FAST_CHECK_EQ(r, rx);
    auto fb = make_shared<gpoint_ts const>(b.ta, b.v, b.fx_policy);
    auto fa = make_shared<gpoint_ts const>(a.ta, a.v, a.fx_policy);
    auto fr = make_merged(fb, fa);
    FAST_CHECK_EQ(apoint_ts{fr}, apoint_ts{r.ta, r.v, r.point_interpretation()});
  }

  TEST_CASE("dtss/ts_cache") {
    using std::vector;
    using std::string;
    using core::utctime;
    using core::deltahours;
    using dtss::cache_stats;
    using time_series::dd::apoint_ts;
    using time_series::dd::gta_t;
    using time_series::dd::gpoint_ts;
    using dtss_cache = dtss::cache;
    size_t max_ids = 10;
    size_t avg_ts_estimate = 8 * 24 * 365;
    dtss_cache c(max_ids, avg_ts_estimate);

    CHECK_EQ(c.get_ts_size(), avg_ts_estimate);
    CHECK_EQ(c.get_mem_max(), max_ids * avg_ts_estimate);
    CHECK_EQ(c.get_total_evicted_count(), 0u);    // verify we have 0 evicted_items at start.
    c.set_mem_max(10 * c.get_mem_max());          // increase the memory target by 10
    CHECK_EQ(c.get_capacity(), 10 * max_ids);     // verify it grows.
    CHECK_EQ(c.compute_capacity(), 10 * max_ids); // verify it's a stable compute
    CHECK_EQ(c.compute_capacity(), 10 * max_ids); // verify it's a stable compute
    c.set_ts_size(avg_ts_estimate * 2);           // increase estimate, expect the cap in items to decrease
    CHECK_EQ(c.get_ts_size(), avg_ts_estimate * 2);
    CHECK_EQ(c.get_capacity(), 10 * max_ids / 2); // lower cap,purely based on the estimated size

    utcperiod p{_t(0), _t(10)};
    CHECK_EQ(false, c.try_get("a", p).has_value());


    utctime t0{_t(5)};
    utctime t1{_t(10)};

    utctimespan dt{1s};
    size_t n{3};
    auto ts_a = construct_ts_frag(gta_t{t0, dt, n}, 1.0, stair_case);
    auto ts_a2 = construct_ts_frag(gta_t{t1, dt, n}, 1.0, stair_case);

    c.add("a", ts_a, ts_a->total_period());
    auto ox = c.try_get("a", utcperiod{t0, t0 + dt});
    REQUIRE_EQ(true, ox.has_value());
    auto const &[x, x_tp] = *ox;
    CHECK_EQ(1.0, x->value(0));
    CHECK_EQ(x_tp, ts_a->total_period());

    auto ts_tp = gross_period(ts_a->total_period(), ts_a2->total_period());
    c.add("a", ts_a2, ts_tp); // test we can add another fragment

    REQUIRE_EQ(false, c.try_get("a", utcperiod{t0, t0 + 10 * dt}).has_value());
    // the above is correct, since we ask for data covering a gap that is not covered
    //  now,if we ask for a period outside the ts total_period registered in cache
    //  we want to succeed to avoid reading db after data not there(yet)

    auto req_before = c.try_get("a", {t0 - 1 * dt, t0 + 2 * dt});
    REQUIRE_EQ(true, req_before.has_value());
    auto [b_ts, b_tp] = *req_before;
    CHECK_EQ(apoint_ts{b_ts}, apoint_ts{ts_a});
    CHECK_EQ(b_tp, ts_tp);

    auto req_after = c.try_get("a", {t1 + 1 * dt, t1 + 6 * dt});
    REQUIRE_EQ(true, req_after.has_value());
    auto [a_ts, a_tp] = *req_after;
    CHECK_EQ(apoint_ts{a_ts}, apoint_ts{ts_a2});
    CHECK_EQ(a_tp, ts_tp);


    auto s = c.get_cache_stats();
    CHECK_EQ(s.hits, 4);
    CHECK_EQ(s.misses, 1);
    CHECK_EQ(s.coverage_misses, 1);
    CHECK_EQ(s.point_count, 6);
    CHECK_EQ(s.fragment_count, 2);
    CHECK_EQ(s.id_count, 1);
    CHECK_EQ(c.get_total_evicted_count(), 0u); // verify we have 0 evicted_items at start.

    c.clear_cache_stats();
    c.flush();
    s = c.get_cache_stats();
    CHECK_EQ(c.get_total_evicted_count(), 1u); // verify we have 1 evicted_item, even when doing flush

    CHECK_EQ(s.hits, 0);
    CHECK_EQ(s.misses, 0);
    CHECK_EQ(s.coverage_misses, 0);
    CHECK_EQ(s.point_count, 0);
    CHECK_EQ(s.fragment_count, 0);
    CHECK_EQ(s.id_count, 0);

    c.add("a", ts_a2, ts_tp);

    c.remove("a");
    REQUIRE_EQ(false, c.try_get("a", utcperiod{t0, t0 + dt}).has_value());

    //-- test vector operations
    // arrange n_ts
    vector<string> ids;
    vector<ts_frag> tss;
    vector<utcperiod> tss_tp;
    size_t n_ts = 3;
    gta_t mta{t0, dt, n};
    for (size_t i = 0; i < n_ts; ++i) {
      ids.push_back(to_string(i));
      tss.emplace_back(construct_ts_frag(mta, double(i), stair_case));
      tss_tp.emplace_back(mta.total_period());
    }

    c.add(std::views::zip(ids, tss, tss_tp)); // add a vector of ids|tss

    auto mts = c.get(ids, mta.total_period()); // get a vector of  ids back as map[id]=ts
    REQUIRE_EQ(n_ts, mts.size());
    for (size_t i = 0; i < n_ts; ++i) {
      REQUIRE_UNARY(mts.find(ids[i]) != mts.end());
      CHECK_EQ(std::get<0>(mts[ids[i]])->value(0), double(i)); // just check one value unique for ts.
    }

    auto ids2 = ids;
    ids2.push_back("not there"); // ask for something that's not there
    auto mts2 = c.get(ids2, mta.total_period());
    REQUIRE_EQ(n_ts, mts.size());
    for (size_t i = 0; i < n_ts; ++i) {
      REQUIRE_UNARY(mts2.find(ids[i]) != mts2.end());
      CHECK_EQ(std::get<0>(mts2[ids[i]])->value(0), double(i)); // just check one value unique for ts.
    }

    c.remove(ids2); // remove by vector (even with elem not there)
    s = c.get_cache_stats();
    CHECK_EQ(s.point_count, 0);
    CHECK_EQ(s.fragment_count, 0);
    CHECK_EQ(s.id_count, 0);
    //
    //-- verify that the cache-capacity is adaptive
    //-- add items to cache, so that it's filled up, and
    //   force to recompute number of items accoring to
    //   average ts-memory size and the memory target
    //
    auto mem_target = c.get_mem_max(); // this is the current memory target
    // auto avg_ts_sz= c.get_ts_size();// current estimate for ts average size
    auto c_cap = c.get_capacity();                 // current cap in number of ts.
    auto n_cap = 200;                              // we aim for 200 ts in cache
    auto n_ts_points = (mem_target / 8) / (n_cap); // fill up with ts that would give count
    gta_t fill_ta{t1, dt, n_ts_points};
    for (size_t i = 0; i < c_cap / 2; ++i) // 25 time-series is enough to trigger real compute of size
      c.add(std::to_string(i), construct_ts_frag(fill_ta, 1.0, stair_case), fill_ta.total_period());
    s = c.get_cache_stats();
    auto computed_cap = c.compute_capacity();
    auto expected_cap = n_cap;
    // MESSAGE("id_count="<<s.id_count<<",frag_count"<<s.fragment_count<<",point_count="<<s.point_count<<",real_size="<<8*s.point_count);
    // MESSAGE("mem_target="<<mem_target<<",avg_ts_sz="<<avg_ts_sz<<",old_cap="<<c_cap<<",
    // new_cap="<<c.get_capacity()<<", target n_cap="<<n_cap);
    CHECK_EQ(c.get_capacity(), expected_cap);
    CHECK_EQ(computed_cap, expected_cap);
    auto ev0 = c.get_total_evicted_count(); // get evicted count before adding a lot o tseries

    //
    //-- now target cap is 200, based on current  time-series in the cache.
    //-- lets add a lot more time-series, with the double size,
    //-- and observe that the cap is  reduced to 100.
    gta_t fill_ta2{t1, dt, n_ts_points * 2};
    for (auto i = 0; i < n_cap; ++i) // 200 times, will entirely fill up cache with the larger ts
      c.add(std::to_string(i), construct_ts_frag(fill_ta2, 1.0, stair_case), fill_ta2.total_period());
    s = c.get_cache_stats();
    auto computed_cap2 = c.compute_capacity();
    auto expected_cap2 = n_cap / 2;
    // MESSAGE("id_count="<<s.id_count<<",frag_count"<<s.fragment_count<<",point_count="<<s.point_count<<",real_size="<<8*s.point_count);
    // MESSAGE("mem_target="<<mem_target<<",avg_ts_sz="<<avg_ts_sz<<",old_cap="<<computed_cap<<",
    // new_cap="<<c.get_capacity()<<", target n_cap="<<computed_cap2);
    CHECK_EQ(c.get_capacity(), expected_cap2);
    CHECK_EQ(computed_cap2, expected_cap2);
    CHECK_EQ(
      c.get_total_evicted_count(),
      ev0 + n_cap / 2); // empirical set target, imporant thing is that we prove evicted raises steady
  }

  TEST_CASE("dtss/mini_frag") {
    using std::vector;
    using std::string;
    using std::min;
    using std::max;
    using core::utcperiod;
    using core::utctime;
    using dtss::mini_frag;
    using time_axis::continuous_merge;

    mini_frag m;

    CHECK_EQ(m.count_fragments(), 0);
    CHECK_EQ(m.estimate_size(), 0);
    CHECK_EQ(false, m.get_frag(utcperiod(_t(0), _t(10))).has_value());

    auto f0 = construct_ts_frag(gta_t{_t(5), _t(1), 5}, 0.0, stair_case);
    m.add(f0, f0->total_period()); // add first
    CHECK_EQ(m.count_fragments(), 1);
    CHECK_EQ(m.estimate_size(), 5);
    CHECK_EQ(m.get_frag(utcperiod{_t(5), _t(10)}).has_value(), true);

    auto f1 = construct_ts_frag(gta_t{_t(4), _t(1), 1}, 1.0, stair_case);
    auto ts_tp = gross_period(f0->total_period(), f1->total_period());
    m.add(f1, ts_tp); // add merge element
    CHECK_EQ(m.count_fragments(), 1);
    CHECK_EQ(m.estimate_size(), 6);
    auto c01 = m.get_frag(utcperiod{_t(4), _t(6)});
    CHECK_EQ(c01.has_value(), true);
    CHECK_EQ(std::get<1>(*c01), ts_tp); // just ensure we get out the new total period

    auto f2 = construct_ts_frag(gta_t{_t(1), _t(1), 2}, 2.0, stair_case);
    ts_tp = gross_period(ts_tp, f2->total_period());
    m.add(f2, ts_tp); // add before first
    CHECK_EQ(m.count_fragments(), 2);
    CHECK_EQ(m.estimate_size(), 8);

    // with two frags parts, verify we get correct frag part when asking for it
    // ask for something that is in the second frag part
    auto c1 = m.get_frag(utcperiod{_t(4), _t(6)});
    REQUIRE(c1.has_value());
    auto [c1_ts, c1_tp] = *c1;
    CHECK_EQ(equal_ts_frag(c1_ts, std::get<0>(*c01)), true);
    // then first frag part
    auto c2 = m.get_frag(utcperiod{_t(1), _t(2)});
    REQUIRE(c2.has_value());
    auto [c2_ts, c2_tp] = *c2;
    CHECK(equal_ts_frag(c2_ts, f2));
    CHECK_EQ(ts_tp, c2_tp);

    // add a frag that melts the two into one frag part
    auto f3 = construct_ts_frag(gta_t{_t(3), _t(1), 1}, 3.0, stair_case);
    ts_tp = gross_period(ts_tp, f3->total_period());
    m.add(f3, ts_tp); // add a piece that merge [0] [1]
    CHECK_EQ(m.count_fragments(), 1);
    CHECK_EQ(m.estimate_size(), 9);
    CHECK_EQ(m.get_frag(utcperiod{_t(4), _t(6)}).has_value(), true);

    auto f4 = construct_ts_frag(gta_t{_t(11), _t(1), 1}, 4.0, stair_case);
    ts_tp = gross_period(ts_tp, f4->total_period());
    m.add(f4, ts_tp); // append a frag after the end
    CHECK_EQ(m.count_fragments(), 2);
    CHECK_EQ(m.estimate_size(), 10);
    CHECK_EQ(m.get_frag(utcperiod{_t(4), _t(6)}).has_value(), true);
    CHECK_EQ(m.get_frag(utcperiod{_t(11), _t(12)}).has_value(), true);
    CHECK_EQ(m.get_frag(utcperiod{_t(11), _t(13)}).has_value(), true); // because of tp
    CHECK_EQ(m.get_frag(utcperiod{_t(1), _t(12)}).has_value(), false);

    // more add at end
    auto f5 = construct_ts_frag(gta_t{_t(13), _t(1), 2}, 5.0, stair_case);
    ts_tp = gross_period(ts_tp, f5->total_period());
    m.add(f5, ts_tp); // append a frag after the end

    // verify by indication that we got what we expected
    CHECK_EQ(m.count_fragments(), 3);
    CHECK_EQ(m.estimate_size(), 12);
    CHECK_EQ(m.get_frag(utcperiod{_t(13), _t(14)}).has_value(), true);
    CHECK_EQ(m.get_frag(utcperiod{_t(11), _t(12)}).has_value(), true);
    CHECK_EQ(m.get_frag(utcperiod{_t(5), _t(6)}).has_value(), true);


    // m.add(tst_frag{_t(11), _t(14)}); // append a frag that melts [1]..[2] into one

    auto f6 = construct_ts_frag(gta_t{_t(11), _t(1), 3}, 6.0, stair_case);
    ts_tp = gross_period(ts_tp, f6->total_period());
    m.add(f6, ts_tp); // append a frag after the end

    CHECK_EQ(m.count_fragments(), 2);
    CHECK_EQ(m.get_frag(utcperiod{11, 15}).has_value(), true);

    // m.add(tst_frag{_t(0), _t(20)}); // append a frag that melts all into one
    auto f7 = construct_ts_frag(gta_t{_t(0), _t(1), 20}, 7.0, stair_case);
    ts_tp = gross_period(ts_tp, f7->total_period());
    m.add(f7, ts_tp); // append a frag after the end


    CHECK_EQ(m.count_fragments(), 1);
    CHECK_EQ(m.estimate_size(), 20);

    // m.add(tst_frag{_t(0), _t(20), 1}); // append a marked frag that is exactly equal
    auto f8 = construct_ts_frag(gta_t{_t(0), _t(1), 20}, 8.0, stair_case);
    ts_tp = gross_period(ts_tp, f8->total_period());
    m.add(f8, ts_tp); // append a frag after the end

    CHECK_EQ(m.count_fragments(), 1);
    CHECK_EQ(m.estimate_size(), 20);
    // verify we got our values into place
    auto c8 = m.get_frag(utcperiod{0, 10});
    REQUIRE(c8.has_value());
    auto const &[c8_ts, c8_tp] = *c8;
    CHECK_EQ(c8_tp, ts_tp);
    CHECK_EQ(c8_ts->value(0), doctest::Approx(f8->value(0)));

    // finally even more testing that causes traffic to the merge algo
    double f_value = 8.0; // fill value for frags that we add
    auto m_add_tst_frag = [&](utctime t1, utctime t2) {
      f_value += 1.0;
      auto tf = construct_ts_frag(gta_t{t1, _t(1), size_t(to_seconds64(t2 - t1))}, f_value, stair_case);
      ts_tp = gross_period(ts_tp, tf->total_period());
      m.add(tf, ts_tp);
      return tf;
    };
    m_add_tst_frag(_t(21), _t(23)); // two frags
    m_add_tst_frag(_t(25), _t(27)); // three
    m_add_tst_frag(_t(0), _t(23));  // exactly cover second frag
    CHECK_EQ(m.count_fragments(), 2);

    m_add_tst_frag(_t(1), _t(24));
    CHECK_EQ(m.count_fragments(), 2);

    m_add_tst_frag(_t(2), _t(27)); // parts p1, p2.end
    CHECK_EQ(m.count_fragments(), 1);

    auto finale = m_add_tst_frag(_t(-1), _t(27));
    CHECK_EQ(m.count_fragments(), 1);
    auto cf = m.get_frag(utcperiod(_t(-3), _t(50))); // notice that we ask for more, it get clipped to ts_tp
    REQUIRE(cf.has_value());
    auto const &[cf_ts, cf_tp] = *cf;
    CHECK_EQ(cf_tp, ts_tp);
    CHECK_EQ(cf_ts->total_period(), ts_tp);              // the clipped period
    CHECK_EQ(cf_ts->value(0), doctest::Approx(f_value)); // and its equal to last
  }

  TEST_CASE("dtss/default_container") {
    using dts_server = server_state;
    using dtss::ts_db;
    using time_series::ts_point_fx;
    dts_server s{};
    test::utils::temp_dir default_container("shyft.def.ctr.");
    string c2("c2");
    add_container(s, string{""}, default_container.string()); // this one will cover root and down
    add_container(s, c2, (default_container / c2).string());  // add one explicit container c2 from the root
    // -- verify that hitting something that  does not match specific container, will match
    ts_db db_c1(default_container.string());
    string c1{"c1"};
    fs::create_directories(default_container / c1);

    gts_t c1_a(
      gta_t{
        vector<utctime>{utctime{0l}, utctime{10l}, utctime{20l}},
        utctime{30l}
    },
      vector<double>{1, 2, 3},
      ts_point_fx::POINT_AVERAGE_VALUE);
    string ts_name{"a"};
    CHECK_EQ(false, db_c1.save(ts_name, c1_a, nullptr, {.recreate = true}).has_value());
    auto &c = internal(s, c1);
    string pattern(".*");
    auto fr = c.find(pattern);
    FAST_REQUIRE_EQ(1u, fr.size());
    FAST_CHECK_EQ(fr[0].name, ts_name);
    auto f1 = do_find_ts(s, string("shyft://c1/.*"));
    FAST_REQUIRE_EQ(1u, f1.size());
    FAST_CHECK_EQ(f1[0].name, ts_name);
    vector<string> rr;
    rr.push_back(string("shyft://c1/a"));
    auto [rtsv, rtsv_tp, rtsv_err] = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_REQUIRE_EQ(1u, rtsv.size());
    apoint_ts aa(c1_a.time_axis(), c1_a.values(), c1_a.point_interpretation());
    FAST_CHECK_EQ(apoint_ts{rtsv[0]}, aa);
    string c2_a_url("shyft://c2/a");
    string c3_xa_url("shyft://c3/xa"); // add new root at the top
    ts_vector_t stsv;
    stsv.push_back(apoint_ts(c2_a_url, aa));
    stsv.push_back(apoint_ts(c3_xa_url, aa));
    do_store_ts(s, stsv, store_policy{.recreate = false, .strict = true, .cache = false});
    rr.push_back(c2_a_url);
    auto [rtsv2, rtsv2_tp, rtsv2_err] = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_REQUIRE_EQ(2u, rtsv2.size());
    auto f2 = do_find_ts(s, string("shyft://c3/x.*"));
    FAST_REQUIRE_EQ(1u, f2.size()); // ok, got the new ts as well.
    do_merge_store_ts(s, stsv, false);
    auto [rtsv3, rtsv3_tp, rtsv3_err] = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_REQUIRE_EQ(2u, rtsv3.size());
    s.can_remove = true;
    do_remove_ts(s, c3_xa_url); // verify we can remove stuff
    auto f3 = do_find_ts(s, string("shyft://c3/x.*"));
    FAST_CHECK_EQ(f3.size(), 0u);
  }

  TEST_CASE("dtss/read_fail_success") {
    using dts_server = server_state;
    using dtss::ts_db;
    using time_series::ts_point_fx;
    dts_server s{};
    test::utils::temp_dir default_container("shyft.def.ctr.");
    add_container(s, std::string{""}, default_container.string()); // this one will cover root and down
    std::string c1{"c1"};
    std::string c1_dir = fmt::format("{}/{}/", default_container.string(), c1);
    add_container(s, c1, c1_dir); // this one will cover root and down
    ts_db db_c1(c1_dir);
    gts_t c1_a(
      time_axis::generic_dt{
        std::vector<utctime>{utctime(0l), utctime(10l), utctime(20l)},
        utctime(30l)
    },
      std::vector<double>{1, 2, 3},
      ts_point_fx::POINT_AVERAGE_VALUE);
    CHECK_EQ(false, db_c1.save(std::string{"a"}, c1_a, nullptr, {.recreate = true}).has_value());
    CHECK_EQ(false, db_c1.save(std::string{"c"}, c1_a, nullptr, {.recreate = true}).has_value());

    std::vector<std::string> rr;
    rr.push_back(std::string("shyft://c1/a"));
    auto [result, result_tp, errs] = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_CHECK_EQ(errs.size(), 0u);
    rr.push_back(std::string("shyft://c1/b"));
    rr.push_back(std::string("shyft://c1/c"));
    rr.push_back(std::string("shyft://c99/f"));
    auto [result2, result2_tp, errs2] = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_CHECK_EQ(result2.size(), 4u);
    FAST_CHECK_EQ(errs2.size(), 2u);
    FAST_CHECK_EQ(errs2[0].index, 1u);
    FAST_CHECK_EQ(errs2[0].code, lookup_error::container_throws);
    FAST_CHECK_EQ(errs2[1].index, 3u);
    FAST_CHECK_EQ(
      errs2[1].code, lookup_error::container_throws); // because we honor default container, and f is not found
    rr.clear();
    rr.push_back("external://something/something");
    auto [result3, result3_tp, errs3] = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_CHECK_EQ(result3.size(), 1u);
    FAST_CHECK_EQ(errs3.size(), 1u);
    FAST_CHECK_EQ(errs3[0].index, 0u);
    FAST_CHECK_EQ(errs3[0].code, lookup_error::unknown_schema);
  }

  TEST_CASE("dtss/try_read_update_cache") {
    using dts_server = server_state;
    using dtss::ts_db;
    using time_series::ts_point_fx;
    dts_server s{};
    test::utils::temp_dir default_container("shyft.def.ctr.");
    add_container(s, std::string{""}, default_container.string()); // this one will cover root and down
    std::string c1{"c1"};
    std::string c1_dir = fmt::format("{}/{}/", default_container.string(), c1);
    add_container(s, c1, c1_dir); // this one will cover root and down
    ts_db db_c1(c1_dir);
    gts_t c1_a(
      time_axis::generic_dt{
        std::vector<utctime>{utctime{0l}, utctime{10l}, utctime{20l}},
        utctime{30l}
    },
      std::vector<double>{1, 2, 3},
      ts_point_fx::POINT_AVERAGE_VALUE);
    (void) db_c1.save(std::string{"a"}, c1_a, nullptr, {.recreate = true});
    std::vector<std::string> rr;
    rr.push_back(std::string("shyft://c1/a"));
    auto tsv = try_read(s, rr, c1_a.total_period(), false, false);
    // not cached, is no cache allowed:
    auto cc = s.ts_cache.get(rr, c1_a.total_period());
    FAST_CHECK_EQ(cc.size(), 0u);
    try_read(s, rr, c1_a.total_period(), false, true);
    // now it is cached:
    cc = s.ts_cache.get(rr, c1_a.total_period());
    FAST_CHECK_EQ(cc.size(), 1u);
    // flush and check with try
    s.ts_cache.flush();
    // removed from cached:
    cc = s.ts_cache.get(rr, c1_a.total_period());
    FAST_CHECK_EQ(cc.size(), 0u);
    auto [result, result_tp, errs] = try_read(s, rr, c1_a.total_period(), false, false);
    // not cached, is no cache allowed:
    cc = s.ts_cache.get(rr, c1_a.total_period());
    FAST_CHECK_EQ(cc.size(), 0u);
    try_read(s, rr, c1_a.total_period(), false, true);
    // now it is cached:
    cc = s.ts_cache.get(rr, c1_a.total_period());
    FAST_CHECK_EQ(cc.size(), 1u);
  }

  TEST_CASE("dtss/read_different_path") {
    using dts_server = server_state;
    using dtss::ts_db;
    using time_series::ts_point_fx;
    dts_server s{};
    test::utils::temp_dir default_container("shyft.def.ctr.");
    add_container(s, std::string{""}, default_container.string()); // this one will cover root and down
    std::string c1{"directory"};
    std::string c1_dir = fmt::format("{}/{}/", default_container.string(), c1);
    add_container(s, c1, c1_dir); // this one will cover root and down
    ts_db db_c1(c1_dir);
    gts_t c1_a(
      time_axis::generic_dt{
        std::vector<utctime>{utctime{0l}, utctime{10l}, utctime{20l}},
        utctime{30l}
    },
      std::vector<double>{1, 2, 3},
      ts_point_fx::POINT_AVERAGE_VALUE);
    (void) db_c1.save(std::string{"a"}, c1_a, nullptr, {.recreate = true});
    // we now have a ts in default/directory/a, with url "shyft://directory/a"
    std::vector<std::string> rr{"shyft://directory/a"};
    // we can read it:
    auto [res, res_tp, res_err] = try_read(s, rr, c1_a.total_period(), false, false);
    // no error
    FAST_CHECK_EQ(res_err.size(), 0u);
    // can we read it from a different path?
    rr.clear();
    rr.push_back("shyft://other_directory/a");
    std::tie(res, res_tp, res_err) = try_read(s, rr, c1_a.total_period(), false, false);
    FAST_CHECK_EQ(res.size(), 1u);
    // can we read it from a nested path?
    rr.clear();
    rr.push_back("shyft://other_directory/with/a/long/path/that/does/not/end/early/a");
    std::tie(res, res_tp, res_err) = try_read(s, rr, c1_a.total_period(), false, false);
    // no error
    FAST_CHECK_EQ(res.size(), 1u);
  }

  TEST_CASE("dtss/read_dstm_no_cache") {
    using dts_server = server_state;
    using dtss::ts_db;
    using time_series::ts_point_fx;

    dts_server s{};
    test::utils::temp_dir default_container("shyft.def.ctr.");
    add_container(s, std::string{""}, default_container.string()); // this one will cover root and down

    time_axis::generic_dt ta{
      std::vector<utctime>{utctime{0l}, utctime{10l}, utctime{20l}},
      utctime{30l}
    };
    // dummy callback, returns one ts per id
    s.callbacks.bind_ts = [&](auto ids, [[maybe_unused]] auto period) {
      std::vector<apoint_ts> res;
      std::ranges::copy(std::views::repeat(apoint_ts{ta, 1.0, stair_case}, ids.size()), std::back_inserter(res));
      return res;
    };
    // read from dstm with cache true:
    std::vector<std::string> rr{"dstm://something/a"};
    auto [res, res_tp, res_err] = try_read(s, rr, ta.total_period(), true, true);
    // could read, no error:
    FAST_CHECK_EQ(res.size(), 1u);
    // not cached, is dstm:
    auto cc = s.ts_cache.get(rr, ta.total_period());
    FAST_CHECK_EQ(cc.size(), 1u);
    // other externals are cached:
    rr.clear();
    rr.push_back("external://something/a");
    auto [res2, res2_tp, res2_err] = try_read(s, rr, ta.total_period(), true, true);
    // could read, no error:
    FAST_CHECK_EQ(res2.size(), 1u);
    // is cached, is not dstm:
    cc = s.ts_cache.get(rr, ta.total_period());
    FAST_CHECK_EQ(cc.size(), 1u);
    // read both:
    rr.push_back("dstm://something/a");
    auto [res3, res3_tp, res3_err] = try_read(s, rr, ta.total_period(), true, true);
    // could read, no error:
    FAST_CHECK_EQ(res3.size(), 2u);
    cc = s.ts_cache.get(rr, ta.total_period());
    FAST_CHECK_EQ(cc.size(), 2u);
  }

  TEST_CASE("dtss/dlib_server_basics") {
    dlog.set_level(dlib::LALL);
    dlib::set_all_logging_output_streams(std::cout);
    dlib::set_all_logging_levels(dlib::LALL);
    dlog << dlib::LINFO << "Starting dtss test";

    struct make_my_day {
      int x{666};
    };

    try {
      // Tell the server to begin accepting connections.
      calendar utc;
      auto t = utc.time(2016, 1, 1);
      auto dt = deltahours(1);
      auto dt24 = deltahours(24);
      int n = 240;
      int n24 = n / 24;
      time_axis::fixed_dt ta(t, dt, n);
      gta_t ta24(t, dt24, n24);
      bool throw_exception = false;
      bool throw_bad_exception = false;
      auto cb = [ta, &throw_exception, &throw_bad_exception](id_vector_t ts_ids, core::utcperiod) -> ts_vector_t {
        ts_vector_t r;
        r.reserve(ts_ids.size());
        double fv = 1.0;
        for (size_t i = 0; i < ts_ids.size(); ++i)
          r.emplace_back(ta, fv += 1.0);
        if (throw_exception) {
          dlog << dlib::LINFO << "Throw from inside dtss executes!";
          throw std::runtime_error("test exception");
        }
        if (throw_bad_exception) {
          dlog << dlib::LINFO << "Throw bad exception inside dtss";
          throw make_my_day();
        }
        return r;
      };
      std::vector<std::string> ts_names = {string("a.prod.mw"), string("b.prod.mw")};
      auto fcb = [&ts_names](std::string search_expression) -> ts_info_vector_t {
        ts_info_vector_t r;
        dlog << dlib::LINFO << "find-callback with search-string:" << search_expression;
        std::regex re(search_expression);
        auto match_end = std::sregex_iterator();
        for (auto const &tsn : ts_names) {
          if (std::sregex_iterator(tsn.begin(), tsn.end(), re) != match_end) {
            ts_info tsi;
            tsi.name = tsn;
            r.push_back(tsi);
          }
        }
        return r;
      };

      server_state our_server{
        server_callbacks{cb, fcb}
      };
      binary_server our_bin_server(our_server);
      // set up the server object we have made
      our_bin_server.set_listening_ip("127.0.0.1");
      // int port_no = 20000;
      // our_bin_server.set_listening_port(port_no);
      int port_no = our_bin_server.start_server();
      dlog << dlib::LINFO << "Serving connection @" << port_no;
      {
        string host_port = string("localhost:") + to_string(port_no);
        dlog << dlib::LINFO << "sending an expression ts to " << host_port;
        std::vector<apoint_ts> tsl;
        for (size_t kb = 4; kb < 16; kb += 2)
          tsl.push_back(
            mk_expression(t, dt, kb * 1000) * apoint_ts(string("netcdf://group/path/ts") + std::to_string(kb)));
        client dtss(host_port);
        REQUIRE_NOTHROW(std::ignore = dtss.get_server_version());
        CHECK_EQ(dtss.get_supported_protocols(), std::array<version_type, 2>{min_version, latest_version});

        auto ts_b = dtss.evaluate(tsl, ta.total_period(), false, false);
        dlog << dlib::LINFO << "Got vector back, size= " << ts_b.size();
        for (auto const &ts : ts_b)
          dlog << dlib::LINFO << "ts.size()" << ts.size();
        dlog << dlib::LINFO << "testing 2 time:";
        REQUIRE_UNARY(our_bin_server.is_running());
        (void) dtss.evaluate(tsl, ta.period(0), false, false);
        dlog << dlib::LINFO << "done second test";
        // test search functions
        dlog << dlib::LINFO << "test .find function";
        auto found_ts = dtss.find(string("a.*"));
        REQUIRE_EQ(found_ts.size(), 1);
        CHECK_EQ(found_ts[0].name, ts_names[0]);
        dlog << dlib::LINFO << "test .find function done";

        throw_exception = true; // verify server-side exception gets back here.
        TS_ASSERT_THROWS_ANYTHING((void) dtss.evaluate(tsl, ta.period(0), false, false));
        dlog << dlib::LINFO << "exceptions done,testing ordinary evaluate after exception";
        throw_exception = false; // verify server-side exception gets back here.
        throw_bad_exception = true;
        TS_ASSERT_THROWS_ANYTHING((void) dtss.evaluate(tsl, ta.period(0), false, false));
        throw_bad_exception = false;
        (void) dtss.evaluate(tsl, ta.period(0), false, false); // verify no exception here, should still work ok
        REQUIRE_UNARY(our_bin_server.is_running());
        dlog << dlib::LINFO << "ok, -now testing percentiles";
        std::vector<int64_t> percentile_spec{0, 25, 50, -1, 75, 100};
        auto percentiles = dtss.percentiles(tsl, ta.total_period(), ta24, percentile_spec, false, false);
        CHECK_EQ(percentiles.size(), percentile_spec.size());
        CHECK_EQ(percentiles[0].size(), ta24.size());
        dlog << dlib::LINFO << "done with percentiles, stopping localhost server";
        dtss.close();
        our_bin_server.clear();
        dlog << dlib::LINFO << "done";
      }
    } catch (exception &e) {
      cout << e.what() << endl;
    }
    dlog << dlib::LINFO << "done";
  }

  TEST_CASE("dtss/dlib_server_cache_update") {
    //
    // This test is the the very special case that custom-made reader-callbacks returns back
    // time-series with different resolution etc. if the time-series is not found (instead of fail/exception)
    // This was failing in those cases where the next sequent cached write operation ended up in
    // a cache merge, with incompatible time-series.
    // This should however work, given that the user passes the overwrite flag, meaning replace
    // the whole time-series. The cache should then be replaced for those items.
    //
    // this test was constructed so that the initial case reported was failing,
    // then fix added, and now working ok.
    //
    try {
      // Tell the server to begin accepting connections.
      calendar utc;
      auto t = utc.time(2016, 1, 1);
      auto dt = deltahours(1);
      auto dt24 = deltahours(24);
      int n = 240;
      int n24 = n / 24;
      time_axis::fixed_dt ta(t, dt, n);
      gta_t ta24(t, dt24, n24);
      bool return_nan_24 = true;
      map<string, ts_vector_t::value_type> memory_store;

      auto cb = [ta24, &return_nan_24](id_vector_t ts_ids, core::utcperiod p) -> ts_vector_t {
        ts_vector_t r;
        r.reserve(ts_ids.size());
        double fv = 1.0;
        auto dt = return_nan_24 ? deltahours(24) : deltahours(1);
        gta_t rta{p.start, dt, static_cast<size_t>(1u + p.timespan() / dt)};
        for (size_t i = 0; i < ts_ids.size(); ++i) {
          if (return_nan_24)
            r.emplace_back(rta, nan);
          else
            r.emplace_back(rta, fv += 1.0);
        }
        return r;
      };
      auto fcb = [&memory_store](std::string search_expression) -> ts_info_vector_t {
        ts_info_vector_t r;
        std::regex re(search_expression);
        auto match_end = std::sregex_iterator();
        for (auto const &kv : memory_store) {
          string tsn{kv.first};
          if (std::sregex_iterator(tsn.begin(), tsn.end(), re) != match_end) {
            ts_info tsi;
            tsi.name = tsn;
            r.push_back(tsi);
          }
        }
        return r;
      };
      auto scb = [&memory_store](ts_vector_t const &tsv) -> void {
        for (auto const &ts : tsv)
          memory_store[ts.id()] = ts;
      };

      server_state our_server{
        server_callbacks{cb, fcb, scb}
      };
      binary_server our_bin_server(our_server);
      our_bin_server.set_listening_ip("127.0.0.1");
      our_server.cache_all_reads = true; // special for this case
      int port_no = our_bin_server.start_server();
      {
        string host_port = string("localhost:") + to_string(port_no);
        std::vector<apoint_ts> tsl;
        std::vector<apoint_ts> tsv_store;
        for (size_t kb = 4; kb < 16; kb += 2) {
          auto ts_url = string("netcdf://group/path/ts") + std::to_string(kb);
          tsl.push_back(mk_expression(t, dt, kb * 1000) * apoint_ts(ts_url));
          tsv_store.push_back(apoint_ts(ts_url, apoint_ts(ta, double(kb), time_series::POINT_AVERAGE_VALUE)));
        }
        client dtss(host_port);
        auto p_r = ta.period(0);
        p_r.start -= deltahours(
          24); // subtract 1 day so that we read slighly before, next write will cause merge(not replace)
        auto ts_b = dtss.evaluate(tsl, p_r, true, false);
        (void) dtss.store_ts(tsv_store, {.recreate = true, .cache = true}); // store these time-series, cache on store
        CHECK_EQ(memory_store.size(), tsv_store.size());
        return_nan_24 = false; // return other values, not nans
        (void) dtss.evaluate(tsl, ta.period(0), true, false);
        REQUIRE_UNARY(our_bin_server.is_running());
        dtss.close();
        our_bin_server.clear();
      }
    } catch (exception &e) {
      cout << e.what() << endl;
      CHECK_MESSAGE(false, "The server throw exception, test failed");
    }
  }

  TEST_CASE("dtss/dlib_multi_server_basics") {
    dlog.set_level(dlib::LALL);
    dlib::set_all_logging_output_streams(std::cout);
    dlib::set_all_logging_levels(dlib::LALL);
    dlog << dlib::LINFO << "Starting dtss multi-test";
    try {
      calendar utc;
      auto t = utc.time(2016, 1, 1);
      auto dt = deltahours(1);
      auto dt24 = deltahours(24);
      int n = 240;
      int n24 = n / 24;
      time_axis::fixed_dt ta(t, dt, n);
      gta_t ta24(t, dt24, n24);
      auto rcb = [ta](id_vector_t ts_ids, core::utcperiod) -> ts_vector_t {
        ts_vector_t r;
        r.reserve(ts_ids.size());
        double fv = 1.0;
        for (size_t i = 0; i < ts_ids.size(); ++i)
          r.emplace_back(ta, fv += 1.0);
        return r;
      };

      size_t n_servers = 2;
      vector<unique_ptr<server_state>> servers;
      vector<unique_ptr<binary_server>> bin_servers;
      vector<string> host_ports;
      for (size_t i = 0; i < n_servers; ++i) {
        auto srv = std::make_unique<server_state>();
        srv->callbacks = server_callbacks(rcb);
        auto bin_srv = make_unique<binary_server>(*srv);
        bin_srv->set_listening_ip("127.0.0.1");
        // srv->set_listening_port(base_port +i);
        int port_no = bin_srv->start_server();
        servers.emplace_back(std::move(srv));
        bin_servers.emplace_back(std::move(bin_srv));
        host_ports.push_back(string("localhost:") + to_string(port_no));
      }
      {
        dlog << dlib::LINFO << "sending permutation of an expression sizes/vectors ts to " << n_servers << " hosts";
        client c(host_ports, 1000);
        for (size_t n_ts = 1; n_ts < 10; n_ts += 1) {
          std::vector<apoint_ts> tsl;
          for (size_t kb = 1; kb <= n_ts; kb += 1) {
            tsl.push_back(
              mk_expression(t, dt, (kb + 1) * 10) * apoint_ts(string("netcdf://group/path/ts") + std::to_string(kb)));
          }
          // dlog << dlib::LINFO<< "Try with size " << tsl.size();
          auto ts_b = c.evaluate(tsl, ta.total_period(), false, false);
          // dlog << dlib::LINFO << "Got vector back, size= " << ts_b.size();
          // for (const auto& ts : ts_b)
          //     dlog << dlib::LINFO << "ts.size()" << ts.size();
          // dlog << dlib::LINFO << "-now testing percentiles";
          std::vector<int64_t> percentile_spec{0, 25, 50, -1, 75, 100};
          auto percentiles = c.percentiles(tsl, ta.total_period(), ta24, percentile_spec, false, false);
          // dlog<<dlib::LINFO<<"done calc, verify";
          CHECK_EQ(percentiles.size(), percentile_spec.size());
          CHECK_EQ(percentiles[0].size(), ta24.size());
        }
        dlog << dlib::LINFO << "done with multi-server requests, stopping localhost server";
        c.close();
        for (size_t i = 0; i < n_servers; ++i) {
          dlog << dlib::LINFO << "Terminating server " << i;
          bin_servers[i]->clear();
        }
        dlog << dlib::LINFO << "done";
      }
    }

    catch (exception &e) {
      cout << e.what() << endl;
    }

    dlog << dlib::LINFO << "done";
  }

  TEST_CASE("perf/dtss/dlib_server") {
    dlog.set_level(dlib::LALL);
    dlib::set_all_logging_output_streams(std::cout);
    dlib::set_all_logging_levels(dlib::LALL);
    dlog << dlib::LINFO << "Starting dtss server performance test";
    try {
      // Tell the server to begin accepting connections.
      calendar utc;
      auto t = utc.time(2016, 1, 1);
      auto dt = deltahours(1);
      auto dt24 = deltahours(24);
      int n = 24 * 365 * 5; // 5years of hourly data
      int n24 = n / 24;
      int n_ts = 10; // 83;
      gta_t ta(t, dt, n);
      gta_t ta24(t, dt24, n24);
      bool throw_exception = false;
      ts_vector_t from_disk;
      from_disk.reserve(n_ts);
      double fv = 1.0;
      for (int i = 0; i < n_ts; ++i)
        from_disk.emplace_back(ta, fv += 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);

      auto cb = [&from_disk, &throw_exception](id_vector_t, core::utcperiod) -> ts_vector_t {
        if (throw_exception) {
          dlog << dlib::LINFO << "Throw from inside dtss executes!";
          throw std::runtime_error("test exception");
        }
        return from_disk;
      };
      server_state our_server{server_callbacks{cb}};
      binary_server our_bin_server(our_server);
      // set up the server object we have made
      our_bin_server.set_listening_ip("127.0.0.1");
      int port_no = our_bin_server.start_server();
      size_t n_threads = 1;

      vector<future<void>> clients;
      for (size_t i = 0; i < n_threads; ++i) {
        clients.emplace_back(async(
          launch::async,
          [port_no, ta, ta24, i, n_ts]() /** thread this */ {
            string host_port = string("localhost:") + to_string(port_no);
            dlog << dlib::LINFO << "sending an expression ts to " << host_port;
            std::vector<apoint_ts> tsl;
            for (int x = 1; x <= n_ts; ++x) { // just make a  very thin request, that get loads of data back
              auto ts_expr = 10.0 + 3.0 * apoint_ts(string("netcdf://group/path/ts_") + std::to_string(x));
              if (x > 1) {
                ts_expr = ts_expr - 3.0 * apoint_ts(string("netcdf://group/path/ts_") + std::to_string(x - 1));
              }
              tsl.push_back(ts_expr.average(ta));
            }

            client dtss(host_port);
            auto t0 = timing::now();
            size_t eval_count = 0;
            int test_duration_ms = 5000;
            int kilo_points = tsl.size() * ta.size() / 1000;
            while (elapsed_ms(t0, timing::now()) < test_duration_ms) {
              // burn cpu server side, save time on serialization
              std::vector<int64_t> percentile_spec{-1};
              auto percentiles = dtss.percentiles(tsl, ta.total_period(), ta24, percentile_spec, false, false);
              // slower due to serialization:
              // auto ts_b = dtss.evaluate(tsl, ta.total_period());
              ++eval_count;
            }
            auto total_ms = double(elapsed_ms(t0, timing::now()));
            dlog << dlib::LINFO << "Done testing " << i << ": #= " << eval_count
                 << " e/s = " << 1000 * double(eval_count) / total_ms << " [e/s]\n"
                 << " netw throughput = " << kilo_points * eval_count * 8 / (total_ms) << " Mb/sec";
            dtss.close();
          }

          ));
      }
      dlog << dlib::LINFO << "waiting for test to complete";

      for (auto &f : clients)
        f.get();
      our_bin_server.clear();
      dlog << dlib::LINFO << "done";
    } catch (exception &e) {
      cout << e.what() << endl;
      dlog << dlib::LERROR << "exception:" << e.what();
    }
    dlog << dlib::LINFO << "done";
  }

  TEST_CASE("dtss/store/no_tracking") { /*
    Test case to ensure
    dtss.client.store do non-tracking serialization of gpoints_ts
    https://gitlab.com/shyft-os/shyft/-/issues/1228
    It turned out that boost::serialization do track point_ts<ta>,
    failing the purpose of the .store function.
    */
    using namespace time_series::dd;
    using time_series::point_ts;
    using time_series::ts_point_fx;

    auto utc = make_shared<calendar>();
    auto t = utc->time(2016, 1, 1);
    auto dt = deltahours(1);
    size_t n = 2; // 24*365*5;

    // make dtss server
    test::utils::temp_dir tmpdir("shyft.dtss_store.no_tracking");
    server_state our_server{};
    string tc{"tc"};
    add_container(our_server, tc, tmpdir.string());
    binary_server our_bin_server(our_server);
    our_bin_server.set_listening_ip("127.0.0.1");
    int port_no = our_bin_server.start_server();
    string host_port = string("localhost:") + to_string(port_no);
    // make corresponding client that we will use for the test.
    client dtss(host_port);
    SUBCASE("save_w.m.refs") {
      size_t n_ts = 2;
      time_axis::generic_dt gta{t, dt, n};
      ts_vector_t tsv;
      apoint_ts one_ref{gta, 1 * 10.0, stair_case}; // multiple refs.
      for (size_t i = 0; i < n_ts; ++i) {
        tsv.emplace_back(shyft_url(tc, to_string(i)), one_ref); // re-using the one_ref here.
      }
      auto f0 = dtss.find(shyft_url(tc, ".*"));
      CHECK_EQ(f0.size(), 0); // expect zero to start with
      // auto t0 = timing::now();
      (void) dtss.store_ts(tsv, {.recreate = false, .cache = true});
      // auto t1 = timing::now();
      auto f1 = dtss.find(shyft_url(tc, ".*"));
      CHECK_EQ(f1.size(), tsv.size());
      ts_vector_t rtsv;
      for (auto const &ts : tsv) {
        rtsv.push_back(apoint_ts(ts.id()));
      }
      ts_vector_t check_tsv = dtss.evaluate(rtsv, gta.total_period(), true, true);
      if (check_tsv != tsv) {
        MESSAGE("Different");
        for (auto i = 0u; i < n_ts; ++i)
          fmt::print("{} != {}", check_tsv[i], tsv[i]);
      }
      CHECK_EQ(check_tsv, tsv);
    }
  }

  TEST_CASE("dtss/store/and_evaluate") { /*
      This test simply create and host a dtss on auto-port,
      then uses shyft:// prefix to test
      all internal operations that involve mapping to the
      shyft ts-db-store.
      */
    using namespace time_series::dd;
    using time_series::point_ts;
    using time_series::ts_point_fx;

    auto utc = make_shared<calendar>();
    auto t = utc->time(2016, 1, 1);
    auto dt = deltahours(1);
    int n = 24 * 365 * 2; // 24*365*5;

    // make dtss server
    test::utils::temp_dir tmpdir("shyft.dtss_store.");
    server_state our_server{};
    binary_server our_bin_server(our_server);
    string tc{"tc"};
    add_container(our_server, tc, tmpdir.string());
    our_bin_server.set_listening_ip("127.0.0.1");
    int port_no = our_bin_server.start_server();
    string host_port = string("localhost:") + to_string(port_no);
    // make corresponding client that we will use for the test.
    client dtss(host_port);
    SUBCASE("save_find_read") {
      size_t n_ts = 10;
      time_axis::fixed_dt fta(t, dt, n);
      time_axis::generic_dt gta{t, dt * 24, size_t(n / 24)};
      ts_vector_t tsv;
      vector<point_ts<time_axis::fixed_dt>> ftsv;

      for (size_t i = 0; i < n_ts; ++i) {
        tsv.emplace_back(shyft_url(tc, to_string(i)), apoint_ts{fta, i * 10.0, stair_case});
        ftsv.emplace_back(fta, i * 10.0, stair_case);
      }
      auto f0 = dtss.find(shyft_url(tc, ".*"));
      CHECK_EQ(f0.size(), 0); // expect zero to start with
      auto t0 = timing::now();
      (void) dtss.store_ts(tsv, {.recreate = true, .cache = false});
      auto t1 = timing::now();
      auto f1 = dtss.find(shyft_url(tc, ".*"));
      CHECK_EQ(f1.size(), tsv.size());
      ts_vector_t ev;
      for (size_t i = 0; i < tsv.size(); ++i)
        ev.push_back(3.0 * apoint_ts(shyft_url(tc, to_string(i))));
      // activate auto-cache, to prepare for next
      our_server.cache_all_reads = true;
      vector<int> pc{10, 50, 90};
      auto t2 = timing::now();
      auto er = dtss.evaluate(ev, fta.total_period(), true, true); // uncached read
      auto t3 = timing::now();
      auto ec = dtss.evaluate(ev, fta.total_period(), true, true);
      auto t4 = timing::now(); // cached read.
      //-- establish benchmark
      vector<vector<double>> bmr;
      bmr.reserve(n_ts);
      for (auto const &ts : ftsv) {
        auto calc = 3.0 * ts;
        vector<double> r;
        r.reserve(calc.size());
        for (size_t i = 0; i < calc.size(); ++i)
          r.emplace_back(calc.value(i));
        bmr.emplace_back(std::move(r));
      }
      auto t5 = timing::now();
      CHECK_EQ(bmr.size(), n_ts);
      CHECK_EQ(er.size(), ev.size());
      CHECK_EQ(ec.size(), ev.size());
      std::cout << "store mpts/s " << double(n_ts * n) / (double(elapsed_us(t0, t1)) / 1000000.0) / 1e6 << "\n";
      std::cout << "evalr mpts/s " << double(n_ts * n) / (double(elapsed_us(t2, t3)) / 1000000.0) / 1e6 << "\n";
      std::cout << "evalc mpts/s " << double(n_ts * n) / (double(elapsed_us(t3, t4)) / 1000000.0) / 1e6 << "\n";
      std::cout << "bench mpts/s " << double(n_ts * n) / (double(elapsed_us(t4, t5)) / 1000000.0) / 1e6
                << "\t time :" << double(elapsed_ms(t4, t5)) << "\n";
      auto cs = our_server.ts_cache.get_cache_stats();
      std::cout << "cache stats(hits,misses,cover_misses,id_count,frag_count,point_count):\n " << cs.hits << ","
                << cs.misses << "," << cs.coverage_misses << "," << cs.id_count << "," << cs.fragment_count << ","
                << cs.point_count << ")\n";
    }
    SUBCASE("evaluate_clip") {
      // 1. ARRANGE (given the doctest context above)
      size_t n_ts = 2;
      time_axis::generic_dt fta(t, dt, n);

      ts_vector_t tsv;
      auto ts_name = [](size_t i) -> string {
        return string("ec") + to_string(i);
      };
      for (size_t i = 0; i < n_ts; ++i)
        tsv.emplace_back(shyft_url(tc, ts_name(i)), apoint_ts{fta, i * 10.0, stair_case});
      (void) dtss.store_ts(tsv, {.recreate = true, .cache = true}); // store(overwrite) and cache

      ts_vector_t ev;
      for (size_t i = 0; i < tsv.size(); ++i)
        ev.push_back(apoint_ts(shyft_url(tc, ts_name(i))));
      our_server.cache_all_reads = true;
      // 2. ACT
      auto r1 = dtss.evaluate(ev, fta.total_period(), true, false); // should give full result
      auto r2 = dtss.evaluate(
        ev, fta.period(2), true, false); // at least period(2), but in practice full result due to cache
      auto r3 = dtss.evaluate(ev, fta.total_period(), true, false, fta.period(2)); // will clip to 2
      auto r4 = dtss.evaluate(ev, fta.period(1), true, false, fta.period(2));      // will clip to 2, since it's

      // 3. ASSERT
      CHECK_EQ(r1[0].total_period(), fta.total_period());
      CHECK_EQ(r2[0].total_period(), fta.total_period());
      CHECK_EQ(r3[0].total_period(), fta.period(2));
      CHECK_EQ(r4[0].total_period(), fta.period(2));
    }
    SUBCASE("remove") {
      our_server.can_remove = true;
      time_axis::fixed_dt fta(t, dt, n);
      time_axis::generic_dt gta{t, dt * 24, std::size_t(n / 24)};
      auto const url = shyft_url(tc, "blub");
      time_series::dd::ats_vector tsv;
      tsv.emplace_back(url, time_series::dd::apoint_ts{fta, 1.0, stair_case});

      auto f0 = dtss.find(url);
      CHECK_EQ(f0.size(), 0);
      (void) dtss.store_ts(tsv, {.recreate = true, .cache = true});
      auto f1 = dtss.find(url);
      CHECK_EQ(f1.size(), 1);

      dtss.remove(url);
      auto f2 = dtss.find(url);
      CHECK_EQ(f2.size(), 0);


      time_series::dd::ats_vector tsv2;
      tsv2.emplace_back(url);
      CHECK_THROWS((void) dtss.evaluate(tsv2, gta.total_period(), true, true));
    }

    our_bin_server.clear();
  }

  TEST_CASE("dtss/client_container_store") {
    /*
    Use client.set_container to create a container, access it, and remove it with client.remove_container
    */
    using time_axis::generic_dt;
    using time_series::point_ts;
    using time_series::ts_point_fx;

    test::utils::temp_dir tmpdir("shyft.dtss_store.");
    core::calendar utc;

    server_state our_server{};
    binary_server our_bin_server(our_server);
    std::string rc{""};
    add_container(our_server, rc, tmpdir.string());
    our_bin_server.set_listening_ip("127.0.0.1");
    int port_no = our_bin_server.start_server();
    std::string host_port = fmt::format("localhost:{}", port_no);
    // make corresponding client that we will use for the test.
    client dtss(host_port);

    std::string relative_path = "relative_path";
    std::string container_name = "some_container";
    std::string ts_name = "some_ts";


    SUBCASE("add_and_remove") {
      dtss.set_container(container_name, relative_path, "ts_db");
      CHECK_EQ(our_server.container.size(), 2);
      auto f = our_server.container.find(container_name);
      CHECK((f != our_server.container.end()));
      std::string container_dir = f->second->root_dir();
      CHECK_EQ(fs::path(container_dir), tmpdir / relative_path);
      REQUIRE(fs::is_directory(container_dir));
      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};
      (void) f->second->save(ts_name, ts, nullptr, {.recreate = true, .strict = true});

      CHECK_NOTHROW((void) dtss.get_ts_info(fmt::format("shyft://{}/{}", container_name, ts_name)));

      dtss.remove_container(fmt::format("shyft://{}/", container_name), true);
      CHECK_EQ(our_server.container.size(), 1);
      f = our_server.container.find(container_name);
      CHECK_EQ(f, our_server.container.end());
      REQUIRE(!fs::exists(container_dir));

      // adding and removing without disk removal keeps disk untouched
      dtss.set_container(container_name, relative_path, "ts_db");
      dtss.remove_container(fmt::format("shyft://{}/", container_name));
      REQUIRE(fs::exists(container_dir));
    };

    SUBCASE("add_and_remove_rocks") {
      dtss.set_container(container_name, relative_path, "ts_rdb");
      CHECK_EQ(our_server.container.size(), 2);
      auto f = our_server.container.find(container_name);
      CHECK((f != our_server.container.end()));
      std::string container_dir = f->second->root_dir();
      CHECK_EQ(fs::path(container_dir), tmpdir / relative_path);
      REQUIRE(fs::is_directory(container_dir));
      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};
      (void) f->second->save(ts_name, ts, nullptr, {.recreate = true, .strict = true});
      CHECK_NOTHROW((void) dtss.get_ts_info(fmt::format("shyft://{}/{}", container_name, ts_name)));

      dtss.remove_container(fmt::format("shyft://{}/", container_name), true);
      CHECK_EQ(our_server.container.size(), 1);
      f = our_server.container.find(container_name);
      CHECK((f == our_server.container.end()));
      REQUIRE(!fs::exists(container_dir));
      // adding and removing without disk removal keeps disk untouched
      dtss.set_container(container_name, relative_path, "ts_rdb");
      dtss.remove_container(fmt::format("shyft://{}/", container_name));
      REQUIRE(fs::exists(container_dir));
    };

    SUBCASE("add_existing_name_throws") {
      dtss.set_container(container_name, relative_path, "ts_db");
      CHECK_EQ(our_server.container.size(), 2);
      TS_ASSERT_THROWS_ANYTHING((void) dtss.set_container(container_name, "some_other_path", "ts_db"));
    }

    SUBCASE("add_existing_path_throws") {
      dtss.set_container(container_name, relative_path, "ts_db");
      CHECK_EQ(our_server.container.size(), 2);
      TS_ASSERT_THROWS_ANYTHING(dtss.set_container("some_other_name", relative_path, "ts_db"));
    }

    SUBCASE("empty_path_throws") {
      TS_ASSERT_THROWS_ANYTHING(dtss.set_container(container_name, "", "ts_db"));
    }

    SUBCASE("out_of_root_path_throws") {
      TS_ASSERT_THROWS_ANYTHING(dtss.set_container(container_name, "../sneaky_out_of_root", "ts_db"));
    }

    SUBCASE("krls_type_throws") {
      TS_ASSERT_THROWS_ANYTHING(dtss.set_container(container_name, relative_path, "krls"));
    }

    SUBCASE("remove_external_url") {
      std::optional<std::pair<std::string, bool>> remove_called_with;

      our_server.callbacks.remove_external = [&](std::string const &to_remove, bool delete_dir) {
        remove_called_with = std::make_pair(to_remove, delete_dir);
      };
      dtss.remove_container("shyft://some_container/", true);
      CHECK_EQ(remove_called_with, std::nullopt);
      dtss.remove_container("fame://some_container/", true);
      CHECK_EQ(remove_called_with, std::make_pair<std::string, bool>("fame://some_container/", true));
    };

    SUBCASE("empty_url_throws") {
      TS_ASSERT_THROWS_ANYTHING(dtss.remove_container(""));
    }
  }

  TEST_CASE("dtss/baseline") {
    using namespace time_series::dd;
    using time_series::point_ts;
    using time_series::ts_point_fx;
    using std::cout;
    auto utc = make_shared<calendar>();
    auto t = utc->time(2016, 1, 1);
    auto dt = deltahours(1);
    int const n = 24 * 365 * 5 / 3; // 24*365*5;

    vector<point_ts<time_axis::fixed_dt>> ftsv;
    size_t const n_ts = 10 * 83;
    arma::mat a_mat(n, n_ts);
    time_axis::fixed_dt fta(t, dt, n);
    // time_axis::generic_dt gta{t,dt*24,size_t(n/24)};
    ts_vector_t tsv;
    for (size_t i = 0; i < n_ts; ++i) {
      tsv.emplace_back(to_string(i), apoint_ts(fta, i * 10.0, stair_case));
      ftsv.emplace_back(fta, i * 10.0, stair_case);
      for (size_t tx = 0u; tx < n; ++tx)
        a_mat(tx, i) = i * 10.0;
    }
    tsv = 3.0 * tsv;


    //-- establish benchmark core-ts
    auto t0 = timing::now();

    vector<vector<double>> bmr;
    bmr.reserve(n_ts);
    for (auto const &ts : ftsv) {
      auto calc = 3.0 * ts;
      vector<double> r;
      r.reserve(calc.size());
      for (size_t i = 0; i < calc.size(); ++i)
        r.emplace_back(calc.value(i));
      bmr.emplace_back(std::move(r));
    }
    auto t1 = timing::now();

    //-- establish benchmark armadillo
    vector<vector<double>> amr;
    amr.reserve(n_ts);
    auto a_res = (a_mat * 3.0).eval();
    for (size_t i = 0; i < n_ts; ++i) {
      amr.emplace_back(arma::conv_to<vector<double>>::from(a_res.col(i)));
    }
    auto t2 = timing::now();

    //-- establish timing for apoint_ts eval.
    vector<vector<double>> xmr;
    xmr.reserve(n_ts);
    // auto xtsv = deflate_ts_vector<point_ts<time_axis::generic_dt>>(tsv);
    for (auto const &ts : tsv) {
      xmr.emplace_back(ts.values());
    }
    auto t3 = timing::now();

    CHECK_EQ(bmr.size(), n_ts);
    CHECK_EQ(amr.size(), n_ts);
    CHECK_EQ(xmr.size(), n_ts);

    cout << "core-ts base-line n_ts= " << n_ts << ", n=" << n << ", time=" << double(elapsed_us(t0, t1)) / 1000.0
         << "ms ->" << double(n * n_ts) / (elapsed_us(t0, t1) / 1e6) / 1e6 << " mops/s \n";

    cout << "api -ts base-line n_ts= " << n_ts << ", n=" << n << ", time=" << double(elapsed_us(t2, t3)) / 1000.0
         << "ms ->" << double(n * n_ts) / (elapsed_us(t2, t3) / 1e6) / 1e6 << " mops/s \n";

    cout << "armavec base-line n_ts= " << n_ts << ", n=" << n << ", time=" << double(elapsed_us(t1, t2)) / 1000.0
         << "ms ->" << double(n * n_ts) / (elapsed_us(t1, t2) / 1e6) / 1e6 << " mops/s \n";
  }

  TEST_CASE("perf/dtss/ltm") {
    // this is basically just for performance study of
    // for api type of ts-expressions,
    using namespace time_series::dd;
    using time_series::point_ts;
    using time_series::ts_point_fx;
    using std::cout;
    auto utc = make_shared<calendar>();
    auto t = utc->time(2016, 1, 1);
    auto dt = deltahours(1);
    int const n = 24 * 365 * 5 / 3; // 24*365*5;

    size_t const n_scn = 83;
    size_t const n_obj = 2;
    size_t const n_ts = n_obj * 2 * n_scn;

    vector<point_ts<time_axis::fixed_dt>> ftsv;
    arma::mat a_mat(n, n_ts);
    time_axis::fixed_dt fta(t, dt, n);
    time_axis::generic_dt gta{t, dt * 24, size_t(n / 24)};
    map<string, apoint_ts> rtsv;
    ts_vector_t stsv;
    for (size_t i = 0; i < n_ts; ++i) {
      rtsv[to_string(i)] = apoint_ts(fta, i * 10.0, stair_case);
      stsv.emplace_back(to_string(i));
      ftsv.emplace_back(fta, i * 10.0, stair_case);
      for (size_t tx = 0; tx < n; ++tx)
        a_mat(tx, i) = i * 10.0;
    }
    ts_vector_t tsv;
    for (size_t i = 0; i < n_scn; ++i) {
      apoint_ts sum;
      for (size_t j = 0; j < n_obj; ++j) {
        size_t p = i * (2 * n_obj) + (2 * j);
        size_t c = p + 1;
        apoint_ts eff = 3.0 * (stsv[p] - stsv[c]);
        if (j == 0)
          sum = eff;
        else
          sum = sum + eff;
      }
      tsv.emplace_back(sum);
    }
    tsv = 1000.0 * (tsv.average(gta));


    //-- establish compute binding time
    auto t0 = timing::now();
    size_t bind_count{0};
    for (auto &sts : tsv) {
      auto ts_refs = sts.find_ts_bind_info();
      for (auto &bi : ts_refs) {
        bi.ts.bind(rtsv[bi.reference]);
        bind_count++;
      }
    }
    for (auto &sts : tsv)
      sts.do_bind();

    auto t1 = timing::now();
    //-- establish timing for apoint_ts eval.
    auto xmr = deflate_ts_vector<point_ts<time_axis::generic_dt>>(tsv);
    auto t2 = timing::now();

    CHECK_EQ(xmr.size(), n_scn);
    CHECK_EQ(bind_count, n_ts);
    cout << "bind phase n_ts= " << n_ts << ", n=" << n << ", time=" << double(elapsed_us(t0, t1)) / 1000.0 << "ms\n";
    cout << "eval phase n_ts= " << n_ts << ", n=" << n << ", time=" << double(elapsed_us(t1, t2)) / 1000.0 << "ms ->"
         << double(n * n_ts * (2 + 1)) / (elapsed_us(t1, t2) / 1e6) / 1e6 << " mops/s \n";
  }

  TEST_CASE("dtss/container_wrapping") {
    test::utils::temp_dir tmpdir("shyft.dtss.container");
    core::calendar utc;

    using time_axis::generic_dt;
    using time_series::point_ts;
    using time_series::ts_point_fx;
    // -----
    using dtss::ts_db;
    using cwrp_t = container_wrapper;

    SUBCASE("dispatch save through container") {
      std::string ts_name{"test"};

      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};

      cwrp_t container{std::make_unique<ts_db>(tmpdir.string())};

      container.save(ts_name, ts, true);
      REQUIRE_UNARY(fs::is_regular_file(tmpdir / ts_name));
    }

    SUBCASE("dispatch read through container") {
      std::string ts_name{"test"};

      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};

      cwrp_t container{std::make_unique<ts_db>(tmpdir.string())};

      container.save(ts_name, ts, true);
      CHECK_UNARY(fs::is_regular_file(tmpdir / ts_name));

      auto ts_new = container.read(ts_name, ta.total_period());
      TS_ASSERT_EQUALS(ts_new, ts);
    }

    SUBCASE("dispatch remove through container") {
      std::string ts_name{"test"};

      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};

      cwrp_t container{std::make_unique<ts_db>(tmpdir.string())};

      container.save(ts_name, ts, true);
      CHECK_UNARY(fs::is_regular_file(tmpdir / ts_name));

      container.remove(ts_name);
      REQUIRE_UNARY_FALSE(fs::exists(tmpdir / ts_name));
    }

    SUBCASE("dispatch get_ts_info through container") {
      std::string ts_name{"test"};

      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};

      cwrp_t container{std::make_unique<ts_db>(tmpdir.string())};

      container.save(ts_name, ts, true);
      CHECK_UNARY(fs::is_regular_file(tmpdir / ts_name));

      auto info = container.get_ts_info(ts_name);
      REQUIRE_EQ(info.name, ts_name);
    }

    SUBCASE("dispatch find through container") {
      std::string ts_name{"test"};

      generic_dt ta{utc.time(2002, 2, 2), calendar::HOUR, 24};
      point_ts<generic_dt> ts{ta, 15., time_series::ts_point_fx::POINT_INSTANT_VALUE};

      cwrp_t container{std::make_unique<ts_db>(tmpdir.string())};

      container.save(ts_name, ts, true);
      CHECK_UNARY(fs::is_regular_file(tmpdir / ts_name));

      auto info = container.find(ts_name);
      REQUIRE_EQ(info.size(), 1);
      REQUIRE_EQ(info[0].name, ts_name);
    }
  }

  TEST_CASE("dtss/container_store/persist") {
    /* test procedure
     * (1) start server
     * (1.1) set_container_cfg(path) to enable persistent storage
     * (2) add "" as root container.
     * (3) stop//restart server, and setup again.
     * (4) verify it has a root path: -> proves it work with minimal change
     * (5) from client add container, store a ts
     * (6) stop//restart server
     * (7) verify : -> proves it keeps added containers persistent
     * (8) remove container
     * (9) stop//restart ..
     * (10) verify: -> proves it also updates when removing containers.
     */
    using time_axis::generic_dt;
    using namespace time_series::dd;
    using time_series::ts_point_fx;
    std::string rc{""};
    test::utils::temp_dir tmpdir("shyft.dtss_persistent_containers.");
    core::calendar utc;
    auto cfg_file = (tmpdir / ".config").string();
    auto db_root = (tmpdir / "dtss").string();
    std::string c_root{"a"};
    std::string c_name{"c"};
    db_cfg c_cfg;
    std::string c_type{"ts_rdb"};
    fs::create_directories(db_root);

    /* 1..4: set and stop  several times, should work*/
    for (auto i = 0; i < 2; ++i) {
      server_state s{};
      set_container_cfg(s, cfg_file);
      add_container(s, rc, db_root, "ts_db", c_cfg);
      CHECK((s.container.size() == 1));
      CHECK((s.cfg_file == cfg_file));
    }
    // use client to add a container
    auto ts_url = shyft_url(c_name, "a", {});
    apoint_ts ts_a{
      generic_dt{utctime{0l}, 1s, 3},
      15.0, ts_point_fx::POINT_AVERAGE_VALUE
    };
    {
      server_state s{};
      set_container_cfg(s, cfg_file);
      binary_server bin(s);
      bin.set_listening_ip("127.0.0.1");
      int port_no = bin.start_server();
      std::string host_port = fmt::format("localhost:{}", port_no);

      // make corresponding client that we will use for the test.
      {
        client c(host_port);
        c.set_container(c_name, c_root, c_type);
        ats_vector tsv{
          apoint_ts{ts_url, ts_a}
        };
        (void) c.store_ts(tsv, {.recreate = true, .cache = true});
      }
      CHECK_EQ(s.container.size(), 2);
    }
    // verify it persisted, and remove it
    {
      server_state s{};
      set_container_cfg(s, cfg_file);
      CHECK_EQ(s.container.size(), 2);
      binary_server bin(s);
      bin.set_listening_ip("127.0.0.1");
      int port_no = bin.start_server();
      std::string host_port = fmt::format("localhost:{}", port_no);

      // make corresponding client that we will use for the test.
      {
        client c(host_port);
        auto fr = c.find(shyft_url(c_name, ".*"));
        CHECK_EQ(fr.size(), 1);                          // just check it is there!
        c.remove_container(shyft_url(c_name, ""), true); // remove and destroy.
      }
      CHECK_EQ(s.container.size(), 1);
    }
    // now coldstart again and verify we are at zero
    {
      server_state s{};
      set_container_cfg(s, cfg_file);
      CHECK_EQ(s.container.size(), 1);
    }
  }

  TEST_CASE("dtss/container_store/swap") {
    /* test procedure
     * (1) start server(master and slave, so we cover complete fx)
     * (1.1) set_container_cfg(path) to enable persistent storage
     * (2) add "" as root container.
     * (3) add 'a' with some data '1' to cache '2' without cache
     * (4) copy from 'a' to 'a_tmp' (no cache on read/write)
     * (4.1) prove we can maintain backend containers, not harming the db(hopefully optimize them)
     * (5) swap a,a_tmp
     * (6) remove a_tmp
     * (7) read '1' and '2' with use cache and cache update
     * (8) verify 1 cache hit, and one miss
     * (9) verify that 1 and 2 matches expectations
     * (..) close server (to prove we update cfg when doing stuff)
     * (..) open server and read 1 and 2, verify result
     */
    using time_axis::generic_dt;
    using namespace time_series::dd;
    using time_series::ts_point_fx;
    std::string rc{""};
    test::utils::temp_dir tmpdir("shyft.dtss_persistent_swap_containers.");
    core::calendar utc;
    auto cfg_file = (tmpdir / ".config").string();
    auto db_root = (tmpdir / "dtss").string();
    std::string c_root{"a"};
    std::string c_new_root{"a_new_location"};
    std::string c_name{"c"};

    std::string c_tmp_name{"c_tmp"};
    db_cfg c_cfg;
    std::string c_type{"ts_rdb"};
    fs::create_directories(db_root);

    auto ts_url1 = shyft_url(c_name, "1", {});
    apoint_ts ts_1{
      generic_dt{utctime{0l}, 10s, 3},
      15.0, ts_point_fx::POINT_AVERAGE_VALUE
    };
    auto ts_url2 = shyft_url(c_name, "2", {});
    apoint_ts ts_2{
      generic_dt{utctime{0l}, 10s, 3},
      25.0, ts_point_fx::POINT_AVERAGE_VALUE
    };
    // (1) .. (1.1)
    /** scope this */ {
      server_state m{}; // master server
      server_state s{};
      set_container_cfg(m, cfg_file);
      binary_server bin(s);
      bin.set_listening_ip("127.0.0.1");
      binary_server bin_m(m);
      bin_m.set_listening_ip("127.0.0.1");
      add_container(m, rc, db_root, "ts_db", c_cfg); // (2)add root cont.
      int m_port_no = bin_m.start_server();
      set_master(s, "127.0.0.1", m_port_no, 0.01, 10, 1.0, latest_version); // connect slave to master
      int port_no = bin.start_server();
      std::string host_port = fmt::format("localhost:{}", port_no);
      client c(host_port);
      c.set_container(c_name, c_root, c_type);
      c.set_container(c_tmp_name, c_new_root, c_type);
      // (3)
      ats_vector tsv1{
        apoint_ts{ts_url1, ts_1}
      };
      (void) c.store_ts(tsv1, {.recreate = true, .cache = true}); // cache it
      ats_vector tsv2{
        apoint_ts{ts_url2, ts_2}
      };
      (void) c.store_ts(tsv2, {.recreate = true, .cache = false}); // do not cache it
      // (4) copy to a_tmp
      ats_vector tsv_cpy{
        apoint_ts{shyft_url(c_tmp_name, "1", {}), ts_1},
        apoint_ts{shyft_url(c_tmp_name, "2", {}), ts_2}
      };
      (void) c.store_ts(tsv_cpy, {.recreate = true, .cache = false}); // no cache on write here
      // (4.1) do maintenance for the containers.(and thus remaining prove it did not hurt, as a minimum)
      maintain_backend_container(m, c_name, true, true);
      maintain_backend_container(m, c_tmp_name, true, true);
      // (5) finally.. swap
      c.swap_container(c_tmp_name, c_name); // order does not matter here
      // (6) remove the old backend storage
      c.remove_container(shyft_url(c_tmp_name, ""), true); // also mark for deletion.
      // (7) read back
      ats_vector tse{apoint_ts{ts_url1}, apoint_ts{ts_url2}};
      auto rtsv = c.evaluate(tse, ts_2.total_period(), true, true);
      ats_vec expected_rtsv{ts_1, ts_2};
      FAST_CHECK_EQ(rtsv, expected_rtsv);
      auto cs2 = c.get_cache_stats();
      client mc(fmt::format("localhost:{}", m_port_no));
      auto mcs2 = mc.get_cache_stats();
      FAST_CHECK_EQ(cs2.id_count, 2); // on read, the slave read/cache stuff when requested.
      FAST_CHECK_EQ(mcs2.hits, 1);    // mc: because the slave does not cache the write (unless its subscribed to)
      FAST_CHECK_EQ(mcs2.misses, 1);  // so we check on master, will do caching as pr. req. on write.
      c.close();
    }
    // now coldstart again and verify we have one correct container(using just one server)
    {
      server_state s{};
      set_container_cfg(s, cfg_file);
      CHECK_EQ(s.container.size(), 2);
      binary_server bin(s);
      int port_no = bin.start_server();
      std::string host_port = fmt::format("localhost:{}", port_no);
      client c(host_port);
      ats_vector tse{apoint_ts{ts_url1}, apoint_ts{ts_url2}};
      auto rtsv = c.evaluate(tse, ts_2.total_period(), true, true);
      ats_vec expected_rtsv{ts_1, ts_2};
      FAST_CHECK_EQ(rtsv, expected_rtsv);
    }
  }

  TEST_CASE("dtss/server_down_up") {
    server_state s{};
    s.can_remove = true;
    test::utils::temp_dir default_container("shyft.def.ctr.");
    add_container(s, "", default_container.string());
    std::string c1("test");

    add_container(s, c1, (default_container / c1).string());
    binary_server bin(s);
    bin.set_listening_ip("127.0.0.1");
    int port_no = bin.start_server();
    REQUIRE(s.alive_connections == 0);
    calendar utc;
    auto dt = deltahours(1);
    auto n = 365 * 24 / 3;
    auto t = utc.time(2016, 1, 1);
    time_axis::fixed_dt ta(t, dt, n);
    time_series::dd::apoint_ts ts{ta, 1.0, stair_case};
    time_series::dd::ats_vector tsv{
      time_series::dd::apoint_ts{"shyft://test/foo", ts}
    };

    std::string host_port = fmt::format("localhost:{}", port_no);
    client c(host_port);
    REQUIRE(s.alive_connections == 0);
    std::ignore = c.store_ts(tsv, {});
    REQUIRE(s.alive_connections == 1);
    c.close();
    REQUIRE(s.alive_connections == 0);
    std::ignore = c.store_ts(tsv, {});
    REQUIRE(s.alive_connections == 1);
    bin.set_graceful_close_timeout(1000);
    bin.clear();
    REQUIRE(s.alive_connections == 0);
    try {
      std::ignore = c.store_ts(tsv, {});
      CHECK(false);
    } catch (std::exception &) {
    }
    bin.set_listening_port(port_no);
    bin.start_server();

    std::ignore = c.store_ts(tsv, {});
    REQUIRE(s.alive_connections == 1);
    c.close();
    REQUIRE(s.alive_connections == 0);
    bin.clear();
  }

  TEST_CASE("dtss/store_empty_ts") {
    using time_axis::generic_dt;
    using namespace time_series::dd;
    using time_series::ts_point_fx;
    std::string rc{""};
    test::utils::temp_dir tmpdir("shyft.dtss.empty");
    auto db_root = (tmpdir / "dtss").string();
    fs::create_directories(db_root);
    server_state s{};
    add_container(s, "file", (tmpdir / "file").string(), "ts_db");
    add_container(s, "rocks", (tmpdir / "rocks").string(), "ts_rdb");
    binary_server bin_s(s);
    bin_s.set_listening_ip("127.0.0.1");
    auto port = bin_s.start_server();
    client c(fmt::format("127.0.0.1:{}", port));
    auto cal = std::make_shared<calendar>("Europe/Oslo");
    auto t0 = cal->time(2024, 11, 18);
    utctime f_dt{10s};
    utctime c_dt{calendar::DAY};
    apoint_ts f_empty{
      generic_dt{t0, f_dt, 0},
      shyft::nan, ts_point_fx::POINT_AVERAGE_VALUE
    };

    apoint_ts c_empty{
      generic_dt{cal, t0, c_dt, 0},
      shyft::nan, ts_point_fx::POINT_AVERAGE_VALUE
    };

    apoint_ts p_empty{generic_dt{std::vector<utctime>{}}, 0.0, ts_point_fx::POINT_AVERAGE_VALUE};
    for (auto container : std::vector<std::string>{"file", "rocks"}) {
      CAPTURE(container);
      ats_vector tsv{
        {apoint_ts(shyft_url(container, "f"), f_empty),
         apoint_ts(shyft_url(container, "c"), c_empty),
         apoint_ts(shyft_url(container, "p"), p_empty)}
      };
      // verify we  can indeed save empty
      auto diags = c.store_ts(tsv, {});
      CHECK(!diags.has_value());
      auto fe = c.find(shyft_url(container, ".*"));
      CHECK_EQ(fe.size(), 3u);
      // verify that the saved def. keeps t0
      for (auto name : std::vector<std::string>{"f", "c"}) {
        auto info_f = std::find_if(std::begin(fe), std::end(fe), [&](auto const &e) {
          return e.name == name;
        });
        CHECK_EQ(info_f->data_period.start, t0);
        CHECK_EQ(info_f->data_period.end, t0);
      }
      // notice, we can only save empty  point frag, so no  t0  in this case, def. to min_utctime is reasonable(enough)
      auto info = std::find_if(std::begin(fe), std::end(fe), [&](auto const &e) {
        return e.name == "p";
      });
      CHECK_EQ(info->data_period.start, min_utctime); // break point/empty  gives  min..min empty period
      CHECK_EQ(info->data_period.end, min_utctime);

      //
      // then prove we can save new frags to existing ts.
      //
      ats_vector fragv{
        {apoint_ts(
           shyft_url(container, "f"),
         apoint_ts(generic_dt{t0 + 10 * f_dt, f_dt, 1}, 10.0, ts_point_fx::POINT_AVERAGE_VALUE)),
         apoint_ts(
           shyft_url(container, "c"),
         apoint_ts(generic_dt{cal, cal->add(t0, c_dt, 10), c_dt, 1}, 10.0, ts_point_fx::POINT_AVERAGE_VALUE)),
         apoint_ts(
           shyft_url(container, "p"), apoint_ts(generic_dt{{t0}, t0 + f_dt}, 10.0, ts_point_fx::POINT_AVERAGE_VALUE))}
      };
      diags = c.store_ts(fragv, {.recreate = false});
      CHECK(!diags.has_value());
      fe = c.find(shyft_url(container, ".*"));
      CHECK_EQ(fe.size(), 3u);
      // verify that the saved def. keeps t0
      for (auto e : std::vector<std::tuple<std::string, utcperiod>>{
             std::make_tuple("f", utcperiod(t0 + 10 * f_dt, t0 + 11 * f_dt)),
             std::make_tuple("c", utcperiod(cal->add(t0, c_dt, 10), cal->add(t0, c_dt, 11)))}) {
        auto const &[name, dp] = e;
        auto info_f = std::find_if(std::begin(fe), std::end(fe), [&](auto const &e) {
          return e.name == name;
        });
        CHECK_EQ(info_f->data_period, dp);
      }

      info = std::find_if(std::begin(fe), std::end(fe), [&](auto const &e) {
        return e.name == "p";
      });
      CHECK_EQ(info->data_period, fragv.back().total_period());
      utcperiod rp{t0, t0 + cal->add(t0, c_dt, 100)};
      auto rtsv = c.evaluate(

        ats_vector{
          apoint_ts(shyft_url(container, "f")),
          apoint_ts(shyft_url(container, "c")),
          apoint_ts(shyft_url(container, "p"))},

        rp,
        true,
        true);
      for (auto i = 0u; i < rtsv.size(); ++i) {
        CHECK_EQ(rtsv[i], fragv[i]);
      }
    }


    c.close();
    bin_s.clear();
  }

  TEST_CASE("dtss/operation_timeout") {
    /*
     * Test procedure
     * 1. run a case that succeeds with margin, using find_cb
     * 1.1 wait a while and observe the socket is closed
     * 1.2 repeat the operation, and verify it succeeds (auto connect)
     * 2. set operation timeout so that it will fail
     * 2.1 verify the result of the failed operation (find_cb, should throw)
     * 3. set operation timeout so it will succeed
     * 3.1 verify it now works
     * 3.2 verify communication counters
     */
    utctime reply_delay{from_seconds(0.0001)};
    server_callbacks callbacks{.find_ts = [&]([[maybe_unused]] std::string search_expression) {
      sleep_for(reply_delay);
      return ts_info_vector_t{
        ts_info("a", time_series::POINT_AVERAGE_VALUE, from_seconds(1), "", {}, from_seconds(0), from_seconds(0))};
    }};
    server_state s{callbacks};
    s.can_remove = true;
    binary_server bin(s);
    bin.set_listening_ip("127.0.0.1");
    int port_no = bin.start_server();

    std::string host_port = fmt::format("localhost:{}", port_no);
    client c(host_port, 1000, internal_version, 100);
    auto found = c.find("x://abc/.*"); // will hit the find_ts
    REQUIRE_EQ(found.size(), 1);
    REQUIRE(s.alive_connections == 1);
    CHECK_EQ(c.reconnect_count(), 0);
    FAST_CHECK_EQ(true, test::utils::wait_until( [&]() {
                    return s.alive_connections == 0;
                  }));
    std::ignore = c.find("x://abc/.*");
    REQUIRE_EQ(s.alive_connections, 1);
    CHECK_EQ(c.reconnect_count(), 1); // auto connected
    FAST_CHECK_EQ(true, test::utils::wait_until( [&]() {
                return s.alive_connections == 0;
              }));
    c.set_operation_timeout(10);
    reply_delay = from_seconds(0.1);
    CHECK_THROWS_AS((void)c.find("x://abc/.*"), std::runtime_error);
    FAST_CHECK_EQ(true, test::utils::wait_until( [&]() {
                return s.alive_connections == 0;
              }));
    CHECK_EQ(c.reconnect_count(), 4); // auto connected
    // just verify it works if conditions are met
    reply_delay = from_seconds(0.0001);
    std::ignore = c.find("x://abc/.*");

    bin.set_graceful_close_timeout(500);
    bin.clear();
  }

  TEST_SUITE_END();
}
