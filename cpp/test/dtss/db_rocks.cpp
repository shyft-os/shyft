#include <algorithm>
#include <future>
#include <unordered_set>

#include <shyft/dtss/detail/ts_db_rocks_impl.h>
#include <shyft/dtss/dtss_db.h>
#include <shyft/dtss/dtss_db_rocks.h>
#include <shyft/dtss/geo.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/fx_merge.h>

#include "test_pch.h"
#include <test/test_utils.h>

namespace shyft {
  using std::exception;
  using std::vector;
  using std::shared_ptr;
  using std::make_shared;
  using std::string;

  using namespace shyft::dtss;
  using namespace shyft::core;
  using namespace shyft;
  using shyft::time_series::POINT_AVERAGE_VALUE;
  namespace ta = shyft::time_axis;
  namespace ts = shyft::time_series;
  using shyft::time_series::dd::gta_t;
  using namespace shyft::time_series::dd;
  using time_series::point_ts;
  using namespace std::chrono_literals;

  namespace { /* tools to help testing */

    inline utctime _t(int64_t t1970s) {
      return utctime{seconds(t1970s)};
    }

    /**
     * @brief simple concurrent exec fx(i), i [0..n)
     * @param n number of fx(i) calls
     * @param fx threadsafe callable fx(size_t i)->void, there is no guarantee of order.
     * @param n_threads default 0, uses std::thread::hardware_concurrency()
     */
    constexpr auto concurrent_exec = [](size_t n, auto &&fx, size_t n_threads = 0u) -> void {
      vector<std::future<void>> workers;
      std::atomic_int64_t ai; // ensure secure access
      n_threads = n_threads != 0 ? n_threads : std::thread::hardware_concurrency();
      for (size_t i = 0; i < n_threads; ++i) {
        workers.emplace_back(std::async(std::launch::async, [&]() {
          for (size_t i = ai++; i < n; i = ai++) { // pick initial atomic ++ i, then pick another atomic ++i until done
            fx(i);
          }
        }));
      }
      for (auto &w : workers)
        w.get(); // wait until all done
    };

    using namespace shyft::time_series::dd;

    gts_t join_ts(gts_t const &a, gts_t const &b) {
      auto r = apoint_ts{a.ta, a.v, a.fx_policy}.extend(
        apoint_ts{b.ta, b.v, b.fx_policy},
        extend_ts_split_policy::EPS_RHS_FIRST,
        extend_ts_fill_policy::EPF_NAN,
        no_utctime,
        0.0);
      return {r.time_axis(), r.values(), a.fx_policy};
    }

    gts_t store_to_ts(gts_t const &a, gts_t const &b) {
      if (time_axis::continuous_merge(a.ta.total_period(), b.ta.total_period())) {
        return time_series::merge(b, a); // first have priority
      } else {
        if (a.total_period().end < b.total_period().start) { // a before b
          return join_ts(a, b);
        } // else { // b before a.
        return join_ts(b, a);
        //}
      }
    }

    bool verify_same_value(double value, double expected) {
      if (std::isfinite(value) && std::isfinite(expected))
        FAST_CHECK_EQ(value, doctest::Approx(expected));
      else {
        if (!std::isfinite(expected) && !std::isfinite(value))
          return true;
        if (std::isfinite(expected)) {
          DOCTEST_FAIL("expected was finite, but value was nan");
        } else {
          DOCTEST_FAIL("expected was nan, but value was finite value");
        }
      }
      return true;
    }

    bool semantically_equal(gts_t const &a /*real ts*/, gts_t const &b /* expected ts */) {
      if (a.total_period() != b.total_period())
        return false;
      if (a.fx_policy != b.fx_policy)
        return false;
      for (size_t i = 0; i < a.size(); ++i) {
        auto av = a.value(i);
        auto bv = b(a.time_axis().time(i));
        verify_same_value(av, bv);
      }
      return true;
    }

    constexpr auto const validate_values_with_nans = [](auto const &res, auto const &old_ts, auto const &new_ts) {
      for (size_t i = 0; i < res.size(); ++i) {
        auto t = res.time(i);
        auto v = res.value(i);
        if (new_ts.total_period().contains(t)) {
          verify_same_value(v, new_ts(t));
        } else if (old_ts.total_period().contains(t)) {
          verify_same_value(v, old_ts(t));
        } else if (
          (t >= new_ts.total_period().end && t < old_ts.total_period().start)
          || (t >= old_ts.total_period().end && t < new_ts.total_period().start)) {
          CHECK_UNARY(std::isnan(v));
        } else {
          FAST_CHECK_UNARY(false); // should never get here..
        }
      }
    };

    constexpr auto const validate_values = [](auto const &read, auto const &written) {
      return validate_values_with_nans(read, written, written);
    };

    /** test fixture for basics test, to ease making testcases visible to cmake, (subcases are not) */
    struct _basics_ {
      shared_ptr<calendar> utc;
      shared_ptr<calendar> osl;

      utctime t;
      utctimespan
        dt; // note that dt must be >= day to ensure calendar ta works (so that it does not get simplified to pure dt)
      utctimespan dt_half;
      uint64_t const ppf{5};
      uint64_t const n{2 * ppf + 1};
      // construct time-axis that we want to test.
      time_axis::fixed_dt fta;
      time_axis::calendar_dt cta1;
      time_axis::calendar_dt cta2;

      // construct vector with values
      vector<double> v;

      vector<utctime> tp;
      time_axis::point_dt pta;
      test::utils::temp_dir tmpdir;
      ts_db_rocks db;

      _basics_()
        : tmpdir{"shyft.ts.db.rocks"}
        , db{tmpdir.string(), db_cfg{ppf}} {
        utc = std::make_shared<calendar>();
        osl = std::make_shared<calendar>("Europe/Oslo");

        t = utc->time(2016, 1, 1);
        dt = deltahours(24); // note that dt must be >= day to ensure calendar ta works (so that it does not get
                             // simplified to pure dt)
        dt_half = deltahours(12);
        // construct time-axis that we want to test.
        fta = time_axis::fixed_dt(t, dt, n);
        cta1 = time_axis::calendar_dt(utc, t, dt, n);
        cta2 = time_axis::calendar_dt(osl, t, dt, n);

        // construct vector with values
        for (size_t i = 0; i < n; i++) {
          v.push_back(double(i));
        }
        for (std::size_t i = 0; i < fta.size(); ++i)
          tp.push_back(fta.time(i));
        pta = time_axis::point_dt(tp, fta.total_period().end);
      }
    };
  }

  TEST_SUITE_BEGIN("dtss");

  TEST_CASE("dtss/rocks_db/test_tools") {
    utctime t0{0l}, dt{from_seconds(10)}, c_dt{calendar::DAY};
    size_t n = 1;
    double v1{1.0}, v2{2.0};
    auto c = make_shared<calendar>();
    SUBCASE("store_to_ts/join_ts/fixed") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{t0, dt, n},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{t0 + (n + 1) * dt, dt, n}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(t0, dt, n * 2 + 1), {1.0, shyft::nan, 2.0},
               shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, e);
    }
    SUBCASE("store_to_ts/join_ts/calendar") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{c, t0, c_dt, n},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{c, c->add(t0, c_dt, std::int64_t(n) + 1), c_dt, n}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(c, t0, c_dt, n * 2 + 1), {1.0, shyft::nan, 2.0},
                shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_UNARY(semantically_equal(r, e));
    }
    SUBCASE("store_to_ts/join_ts/point") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{{t0}, t0 + dt},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{{t0 + 2 * dt}, t0 + 3 * dt}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt({ t0,    t0 + dt, t0 + 2 * dt},
        t0 + 3 * dt),
        {1.0, shyft::nan,         2.0},
        shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_UNARY(semantically_equal(r, e));
    }
    SUBCASE("store_to_ts/merge/fixed/interior") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{t0, dt, 3},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{t0 + 1 * dt, dt, 1}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(t0, dt, 3), {v1, v2, v1},
           shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, e);
    }
    SUBCASE("store_to_ts/merge/fixed/extend") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{t0, dt, 3},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{t0 + 3 * dt, dt, 1}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(t0, dt, 4), {v1, v1, v1, v2},
           shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, e);
    }
    SUBCASE("store_to_ts/merge/fixed/prepend") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{t0 + 1 * dt, dt, 3},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{t0, dt, 1}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(t0, dt, 4), {v2, v1, v1, v1},
           shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, e);
    }
    SUBCASE("store_to_ts/merge/fixed/cover") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{t0, dt, 3},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{t0, dt, 3}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(t0, dt, 3), {v2, v2, v2},
           shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, e);
    }
    SUBCASE("store_to_ts/merge/calendar") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{c, t0, c_dt, 3},
          v1, POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{c->add(t0, c_dt, 1), c_dt, 1}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt(c, t0, c_dt, 3), {v1, v2, v1},
            shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_UNARY(semantically_equal(r, e));
    }
    SUBCASE("store_to_ts/merge/point") {
      auto r = store_to_ts(
        gts_t{
          time_axis::generic_dt{{t0, t0 + dt, t0 + 2 * dt}, t0 + 3 * dt},
          {v1, v1, v1},
          POINT_AVERAGE_VALUE
      },
        gts_t{time_axis::generic_dt{{t0 + dt}, t0 + 2 * dt}, v2, POINT_AVERAGE_VALUE});
      auto e = gts_t{
        time_axis::generic_dt({t0, t0 + dt, t0 + 2 * dt},
        t0 + 3 * dt),
        {v1,      v2,          v1},
        shyft::time_series::POINT_AVERAGE_VALUE
      };
      FAST_CHECK_EQ(r, e);
    }
  }

  TEST_CASE_FIXTURE(_basics_, "dtss/rocks_db/basics/store_fixed_dt") {
    gts_t o(gta_t(fta), v, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    string fn("measurements/tssf.db"); // verify we can have path-parts
    CHECK_EQ(false, db.save(fn, o, nullptr, {.recreate = true}).has_value());
    // read all
    auto [r1, r1p] = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r1.point_interpretation());
    CHECK_EQ(o.time_axis(), r1.time_axis());
    validate_values(r1, o);

    // read inner slice trimming the beginning
    auto te = o.total_period().end;
    for (size_t i = 0; i < 2 * n; i++) {
      auto tb = o.total_period().start + i * dt_half;
      auto [r2, r2p] = db.read(fn, utcperiod{tb, te});
      CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
      CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t + (i / 2) * dt, dt, n - i / 2));
      validate_values(r2, o);
    }

    // read inner slice trimming the end
    auto tb = o.total_period().start;
    for (size_t i = 0; i < 2 * n; i++) {
      auto [r2, r2p] = db.read(fn, utcperiod{tb, o.total_period().end - i * dt_half});
      CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
      auto exp_n = std::min(n, n - i / 2 + 1); // +1 because sourround read
      CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t, dt, exp_n));
      validate_values(r2, o);
    }
    // check that it reads both points surrounding read period if boundaries are not exactly on range
    SUBCASE("read_surrounding_points") {
      core::utcperiod rp{fta.time(0) + dt_half, fta.time(0) + dt_half + 1s};
      auto [r3, r3_tp] = db.read(fn, rp);
      CHECK_EQ(r3.size(), 2);
      CHECK_EQ(r3.value(0), doctest::Approx(o.value(0)));
      if (r3.size() > 1)
        CHECK_EQ(r3.value(1), doctest::Approx(o.value(1)));
    }
    // read outside stored range
    auto [ro, ro_tp] = db.read(fn, utcperiod{tb - 3 * dt, tb - dt});
    FAST_CHECK_EQ(ro.size(), 0);
    // read interior within a frag
    auto [ri, ri_tp] = db.read(fn, utcperiod{tb + dt, tb + 2 * dt});
    FAST_CHECK_EQ(ri.size(), 2); // should be two.. ref.linear case..
    validate_values(ri, o);

    //// write  a new frag after the previous last, that is not filled up, this will cover a specific case in the
    /// read-logic, filling in with nans.
    gta_t ext_ta{fta.total_period().end + ppf * dt, dt, 1}; // sufficient with one value
    gts_t o_ext{ext_ta, 33, shyft::time_series::POINT_AVERAGE_VALUE};
    auto expected_ts = store_to_ts(o, o_ext);
    CHECK_EQ(false, db.save(fn, o_ext, nullptr, {.recreate = false}).has_value());
    auto [re, re_tp] = db.read(fn, utcperiod{}); // read, so we get the extend period
    FAST_CHECK_UNARY(semantically_equal(re, expected_ts));

    auto tsi = db.get_ts_info(fn);
    CHECK_EQ(tsi.name, fn);

    db.remove(fn);
    auto fr = db.find(string("measurements/tssf\\.db")); // should match our ts.
    CHECK_EQ(fr.size(), 0);
  }

  TEST_CASE_FIXTURE(_basics_, "dtss/rocks_db/basics/store_calendar_utc_dt") {
    gts_t o(gta_t(cta1), v, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    string fn("tssf1.db");
    CHECK_EQ(false, db.save(fn, o, nullptr, {.recreate = true}).has_value());

    // read all
    auto [r, r_tp] = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r.point_interpretation());
    CHECK_EQ(o.time_axis(), r.time_axis());
    validate_values(r, o);

    // read inner slice trimming the beginning
    auto te = o.total_period().end;
    for (size_t i = 0; i < 2 * n; i++) {
      auto tb = o.total_period().start + i * dt_half;
      auto [r2, r2p] = db.read(fn, utcperiod{tb, te});
      CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
      CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t + (i / 2) * dt, dt, n - i / 2));
      validate_values(r2, o);
    }
    // SUBCASE("read_surrounding_points")
    {
      core::utcperiod rp{cta1.time(0) + dt_half, cta1.time(0) + dt_half + 1s};
      auto [r3, r3_tp] = db.read(fn, rp);
      CHECK_EQ(r3.size(), 2);
      CHECK_EQ(r3.value(0), doctest::Approx(o.value(0)));
      if (r3.size() > 1)
        CHECK_EQ(r3.value(1), doctest::Approx(o.value(1)));
    }

    // read inner slice trimming the end
    auto tb = o.total_period().start;
    for (size_t i = 0; i < 2 * n; i++) {
      auto [r2, r2p] = db.read(fn, utcperiod{tb, o.total_period().end - i * dt_half});
      CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
      auto exp_n = std::min(n, n - i / 2 + 1); //+1 to get surround read
      CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t, dt, exp_n));
      validate_values(r2, o);
    }
    // read outside stored range
    auto [ro, ro_tp] = db.read(fn, utcperiod{tb - 3 * dt, tb - dt});
    FAST_CHECK_EQ(ro.size(), 0);
    // read interior within a frag
    auto [ri, ri_tp] = db.read(fn, utcperiod{tb + dt, tb + 2 * dt});
    FAST_CHECK_EQ(ri.size(), 2); // should be two.. ref.linear case..
    validate_values(ri, o);

    auto i = db.get_ts_info(fn);
    CHECK_EQ(i.name, fn);
    CHECK_EQ(i.data_period, o.total_period());
    CHECK_EQ(i.point_fx, o.point_interpretation());
    CHECK_LE(i.modified, utctime_now());
    CHECK_EQ(i.delta_t, cta1.dt);

    auto fr = db.find(string(".ss.1\\.db")); // should match our ts.
    CHECK_EQ(fr.size(), 1);

    db.remove(fn);
    try {
      auto [rx, rx_tp] = db.read(fn + ".not.there", utcperiod{});
      CHECK_UNARY(rx.size() == 3);
    } catch (exception const &) {
      CHECK_UNARY(true);
    }
  }

  TEST_CASE_FIXTURE(_basics_, "dtss/rocks_db/basics/store_calendar_osl_dt") {
    gts_t o(gta_t(cta2), v, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    string fn("tssf2.db");
    CHECK_EQ(false, db.save(fn, o, nullptr, {.recreate = true}).has_value());
    auto [r, r_tp] = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r.point_interpretation());
    CHECK_EQ(o.time_axis(), r.time_axis());
    validate_values(r, o);
    SUBCASE("read-null-frag") {
      utcperiod rp{osl->time(2000, 1, 1), osl->time(2000, 1, 2)};
      auto [rn, rn_tp] = db.read(fn, rp);
      CHECK_EQ(rn.size(), 0u);
      CHECK_EQ(rn.time_axis().gt(), time_axis::generic_dt::CALENDAR);
      REQUIRE_NE(rn.time_axis().c().cal, nullptr);
      CHECK_EQ(rn.time_axis().c().cal->get_tz_name(), cta2.cal->get_tz_name());
      CHECK_EQ(rn.time_axis().c().dt, cta2.dt); // ensure we get dt filled in
    }
    db.remove(fn);
  }

  TEST_CASE_FIXTURE(_basics_, "dtss/rocks_db/basics/get_ts_info_with_tz") {
    gts_t o(gta_t(osl, osl->time(2020, 3, 3), calendar::DAY, 10), 10.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    string fn("tssf2_tz.db");
    CHECK_EQ(false, db.save(fn, o, nullptr, {.recreate = true}).has_value());
    auto i = db.get_ts_info(fn);
    CHECK_EQ(i.name, fn);
    CHECK_EQ(i.data_period, o.total_period());
    CHECK_EQ(i.point_fx, o.point_interpretation());
    CHECK_LE(i.modified, utctime_now());
    CHECK_EQ(i.delta_t, o.ta.c().dt);
    CHECK_EQ(i.olson_tz_id, osl->get_tz_name());
    db.remove(fn);
  }

  TEST_CASE_FIXTURE(_basics_, "dtss/rocks_db/basics/store_point_dt") {
    gts_t o(gta_t(pta), v, time_series::ts_point_fx::POINT_INSTANT_VALUE);
    string fn("tssf3.db");
    CHECK_EQ(false, db.save(fn, o, nullptr, {.recreate = true}).has_value());
    // read all
    auto [r, r_tp] = db.read(fn, utcperiod{});
    CHECK_EQ(o.point_interpretation(), r.point_interpretation());
    CHECK_EQ(o.time_axis(), r.time_axis());
    validate_values(r, o);

    // read inner slice trimming the beginning
    auto te = o.total_period().end;
    for (size_t i = 0; i < 2 * n; i++) {
      // std::cout << i << std::endl;
      auto tb = o.total_period().start + i * dt_half;
      auto [r2, r2p] = db.read(fn, utcperiod{tb, te});
      CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
      CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t + (i / 2) * dt, dt, n - i / 2));
      validate_values(r2, o);
    }

    // read inner slice trimming the end
    auto tb = o.total_period().start;
    for (size_t i = 0; i < 2 * n; i++) {
      auto [r2, r2p] = db.read(fn, utcperiod{tb, o.total_period().end - i * dt_half});
      CHECK_EQ(o.point_interpretation(), r2.point_interpretation());
      auto exp_n = std::min(n, n - i / 2 + 1); //+1 because of surround read
      CHECK_EQ(r2.time_axis(), time_axis::generic_dt(t, dt, exp_n));
      validate_values(r2, o);
    }
    SUBCASE("read_surrounding_points") {
      core::utcperiod rp{pta.time(0) + dt_half, pta.time(0) + dt_half + 1s};
      auto [r3, r3_tp] = db.read(fn, rp);
      CHECK_EQ(r3.size(), 2);
      CHECK_EQ(r3.value(0), doctest::Approx(o.value(0)));
      if (r3.size() > 1)
        CHECK_EQ(r3.value(1), doctest::Approx(o.value(1)));
    }
    auto i = db.get_ts_info(fn);
    // calendar utc;
    // MESSAGE(" modified time :"<<utc.to_string(i.modified));
    CHECK_EQ(i.name, fn);
    CHECK_EQ(i.data_period, o.total_period());
    CHECK_EQ(i.point_fx, o.point_interpretation());
    CHECK_LE(i.modified, utctime_now() + _t(2));
    CHECK_GE(i.modified, utctime_now() - _t(300));

    db.remove(fn);
  }

  TEST_CASE("perf/dtss/rocks_db/basics") {
    test::utils::temp_dir tmpdir;
    size_t const ppf = 10 * 1024 * 1024;
    bool compression = false; // always slower with compression in these tests., so off.
    ts_db_rocks ldb{
      (tmpdir / "rocks").string(), db_cfg{ppf, compression}
    };
    ts_db fdb{(tmpdir / "file").string()};

    // parameterized function to perform the speedtest
    constexpr auto const mk_ta_fixed = [](size_t n_values) {
      return gta_t{utctime{0l}, utctime{10l}, n_values};
    };

    constexpr auto const mk_ta_point = [](size_t n_values) {
      vector<utctime> t;
      t.reserve(n_values);
      for (size_t i = 0; i < n_values; ++i)
        t.emplace_back(i * 10);
      return gta_t{t, utctime{n_values * 10}};
    };

    constexpr auto const mk_ts_ = [](string const &ts_prefix, size_t n_ts, gta_t const &ta) {
      vector<gts_t> tsv;
      tsv.reserve(n_ts);
      vector<string> tsn;
      tsn.reserve(n_ts);
      double fv = 1.0;
      vector<double> v;
      v.reserve(ta.size());
      for (size_t i = 0; i < ta.size(); ++i)
        v.push_back(fv + 0.001 * double(i));
      for (size_t i = 0; i < n_ts; ++i) {
        for (auto &x : v)
          x += fv;
        tsv.emplace_back(ta, v, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE);
        tsn.emplace_back(ts_prefix + std::to_string(i));
      }
      return std::make_tuple(tsn, tsv);
    };

    auto const speed_test =
      [&](
        auto &db,
        bool fixed_dt,
        string const &ts_prefix,
        bool recreate_ts,
        size_t n_values,
        size_t n_ts,
        auto &&fx_save,
        auto &&fx_read) {
        auto [tsn, tsv] = mk_ts_(ts_prefix, n_ts, fixed_dt ? mk_ta_fixed(n_values) : mk_ta_point(n_values));
        auto t0 = timing::now();
        fx_save(db, tsn, tsv, recreate_ts);
        auto t1 = timing::now();
        auto rv = fx_read(db, tsn);
        FAST_CHECK_EQ(rv.size(), tsn.size()); // enforce usage of rv.
        auto t2 = timing::now();
        auto w_mb_s = double(n_ts * n_values) / (double(elapsed_us(t0, t1)) / 1e6) / 1e6;
        auto r_mb_s = double(n_ts * n_values) / (double(elapsed_us(t1, t2)) / 1e6) / 1e6;

        MESSAGE(
          "rocks_db/speed " << ts_prefix << ", n_ts = " << n_ts << " n = " << n_values << " write Mpts/s = " << w_mb_s
                            << ", read Mpts/s = " << r_mb_s << " Mpts = " << n_ts * n_values / 1e6
                            << ", roundtrip s=" << double(elapsed_us(t0, t2)) / 1e6);
      };

    constexpr auto single_save = [](auto &db, auto const &tsn, auto const &tsv, bool recreate_ts) {
      for (size_t i = 0; i < tsn.size(); ++i) {
        (void) db.save(tsn[i], tsv[i], nullptr, {.recreate = recreate_ts});
      }
    };

    auto const mthread_save = [&](auto &db, auto const &tsn, auto const &tsv, bool recreate_ts) {
      concurrent_exec(tsn.size(), [&](size_t i) {
        (void) db.save(tsn[i], tsv[i], nullptr, {.recreate = recreate_ts});
      });
    };

    auto const multi_save = [](auto &db, auto const &tsn, auto const &tsv, bool recreate_ts) {
      (void) db.save(
        tsn.size(),
        [&](size_t i) {
          return ts_item_t{tsn[i], tsv[i]};
        },
        nullptr,
        {.recreate = recreate_ts});
    };

    auto const single_read = [](auto &db, auto const &tsn) {
      vector<gts_t> r;
      r.reserve(tsn.size());
      utcperiod rp;
      for (size_t i = 0; i < tsn.size(); ++i)
        r.emplace_back(std::get<0>(db.read(tsn[i], rp)));
      return r;
    };
    auto const mthread_read = [&](auto &db, auto const &tsn) {
      vector<gts_t> r(tsn.size()); // make space so we can move in safely during multi thread exec
      utcperiod rp;
      concurrent_exec(tsn.size(), [&](auto i) {
        r[i] = std::move(std::get<0>(db.read(tsn[i], rp)));
      });
      return r;
    };


    int n_repeats = 1; // if we need to make sure it repeats, notice that overwrite/append to same ts is not measured
                       // here.(less performance, approx 1/2 )
    size_t ts_arome{24 * 10},
      n_arome{
        3 * 100 * 1000
        / 8}; // n_xx scaled down to allow tests to run fast, yet providing the same results as in 'real life'
    size_t ts_ltm{3 * 10000}, n_ltm{10 * 1000 / 8};
    bool fixed_dt = true; // using break-point ts impact performance as expected 2..3x the fixed-dt performance.

    for (int i = 0; i < n_repeats; ++i)
      speed_test(
        ldb, fixed_dt, "rdb.arome_swsr_" + std::to_string(i), false, ts_arome, n_arome, single_save, single_read);
    for (int i = 0; i < n_repeats; ++i)
      speed_test(
        ldb,
        fixed_dt,
        "rdb.arome_mwxr_" + std::to_string(i),
        false,
        ts_arome,
        n_arome,
        multi_save,
        mthread_read); // faster write&reads(!) with multi_save
    for (int i = 0; i < n_repeats; ++i)
      speed_test(
        ldb,
        fixed_dt,
        "rdb.arome_xwxr_" + std::to_string(i),
        false,
        ts_arome,
        n_arome,
        mthread_save,
        mthread_read); // faster write&reads(!) with multi_save

    for (int i = 0; i < n_repeats; ++i)
      speed_test(
        fdb, fixed_dt, "fdb.arome_xwxr_" + std::to_string(i), false, ts_arome, n_arome, mthread_save, mthread_read);
    /// for larger ts, we always benefit from multithreading.
    for (int i = 0; i < n_repeats; ++i)
      speed_test(
        ldb,
        fixed_dt,
        "rdb.ltm_xwxr" + std::to_string(i),
        false,
        ts_ltm,
        n_ltm,
        mthread_save,
        mthread_read); ////takes longer with multi_save..
    for (int i = 0; i < n_repeats; ++i)
      speed_test(fdb, fixed_dt, "fdb.ltm_xwxr" + std::to_string(i), false, ts_ltm, n_ltm, mthread_save, mthread_read);
  }

  TEST_CASE("dtss/rocks_db/max_id") {
    test::utils::temp_dir tmpdir("ts.db.test.max_id");
    uint64_t ppf = 5;
    uint64_t const uid_pr_sync = 10000;
    db_cfg cfg{ppf};
    auto db = make_unique<ts_db_rocks>(tmpdir.string(), cfg);
    FAST_CHECK_EQ(db->current_uid(), 0);
    FAST_CHECK_EQ(db->clean_shutdown(), 1);
    FAST_CHECK_EQ(db->sync_count(), 0);
    gts_t a{
      gta_t{utctime{0l}, utctime{1l}, 1},
      1.0, POINT_AVERAGE_VALUE
    };
    gts_t b{
      gta_t{utctime{0l}, utctime{1l}, 1},
      2.0, POINT_AVERAGE_VALUE
    };
    gts_t c{
      gta_t{utctime{0l}, utctime{1l}, 1},
      3.0, POINT_AVERAGE_VALUE
    };
    CHECK_EQ(false, db->save("a", a, nullptr, {.recreate = true}).has_value());
    FAST_CHECK_EQ(db->sync_count(), 1);
    db = nullptr;      // close db.
    cfg.test_mode = 1; // next close, we make it dirty
    db = make_unique<ts_db_rocks>(tmpdir.string(), cfg);
    FAST_CHECK_EQ(db->current_uid(), 1);
    FAST_CHECK_EQ(db->clean_shutdown(), 1);
    auto [ra, ra_tp] = db->read("a", utcperiod{});
    FAST_CHECK_EQ(a, ra);
    CHECK_EQ(false, db->save("b", b, nullptr, {.recreate = true}).has_value());
    ra = std::get<0>(db->read("a", utcperiod{}));
    auto [rb, rb_tp] = db->read("b", utcperiod{});
    FAST_CHECK_EQ(ra, a);
    FAST_CHECK_EQ(rb, b);
    db = nullptr; // an unclean shutdown here since we enabled test-mode
    cfg.test_mode = 0;
    db = make_unique<ts_db_rocks>(tmpdir.string(), cfg); // reopen a dirty db.
    FAST_CHECK_EQ(db->current_uid(), uid_pr_sync + 2);   // we jump 10k
    FAST_CHECK_EQ(db->clean_shutdown(), 1);              // and now its clean
    FAST_CHECK_EQ(db->sync_count(), 1);                  // because we synced in startup
    CHECK_EQ(
      false,
      db->save("c", c, nullptr, {.recreate = true})
        .has_value()); // if recovery of max _ts id, was not working, this would overwrite 'b'
    ra = std::get<0>(db->read("a", utcperiod{}));
    rb = std::get<0>(db->read("b", utcperiod{}));
    FAST_CHECK_EQ(db->clean_shutdown(), uid_pr_sync); // after first write, we stash safety limit 10k in there
    FAST_CHECK_EQ(db->sync_count(), 2);               // because we synced the stash above

    auto [rc, rc_tp] = db->read("c", utcperiod{});
    FAST_CHECK_EQ(ra, a);
    FAST_CHECK_EQ(rb, b);
    FAST_CHECK_EQ(rc, c);
    db = nullptr;
    db = make_unique<ts_db_rocks>(tmpdir.string(), cfg); // reopen a dirty db.
    FAST_CHECK_EQ(db->clean_shutdown(), 1);              // after first write, we stash safety limit 10k in there
    FAST_CHECK_EQ(db->current_uid(), uid_pr_sync + 3);   //
    for (auto i = 0u; i < 2 * uid_pr_sync; ++i)
      CHECK_EQ(false, db->save(fmt::format("ts{}", i), b, nullptr, {.recreate = false}).has_value());
    FAST_CHECK_EQ(db->sync_count(), 4); // one for start and three after that
  }

  TEST_CASE("dtss/rocks_db/merge_write/error_handling") {
    // setup db
    test::utils::temp_dir tmpdir("ts.db.test.error_handling");
    uint64_t ppf = 5;
    ts_db_rocks db(tmpdir.string(), db_cfg{ppf});
    std::shared_ptr<calendar> utc_ptr = std::make_shared<calendar>();
    CHECK_EQ(tmpdir.string(), db.root_dir());
    SUBCASE("store/zero/elements") {
      (void) db.save(
        0,
        [](size_t) -> ts_item_t {
          throw std::runtime_error("never get here!");
        },
        nullptr,
        {.recreate = false});
    }

    SUBCASE("extending with different ta") {
      // data
      utctime const t0 = utctime_now();
      utctimespan const dt_h = calendar::HOUR;
      utctimespan const dt_d = calendar::DAY;
      std::size_t const n = 1000;
      // -----
      ta::fixed_dt f_ta_h{t0, dt_h, n};
      ta::calendar_dt c_ta_d{utc_ptr, t0, dt_d, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(c_ta_d), 0.};
      // -----
      string fn("dtss_save_merge/ext_diff_ta.db");

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_d, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_diff_ta\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        (void) db.save(fn, pts_h, nullptr, {.recreate = false}),
        std::runtime_error,
        "dtss_store: cannot merge with different ta type");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_diff_ta\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("extending with different point interpretation") {
      // data
      utctime const t0 = utctime_now();
      utctimespan const dt_h = calendar::HOUR;
      std::size_t const n = 1000;
      // -----
      ta::fixed_dt f_ta_h_1{t0, dt_h, n};
      ta::fixed_dt f_ta_h_2{t0 + n * dt_h, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h_1{gta_t(f_ta_h_1), 0., ts::POINT_INSTANT_VALUE};
      ts::point_ts<ta::generic_dt> pts_h_2{gta_t(f_ta_h_2), 0., ts::POINT_AVERAGE_VALUE};
      // -----
      string fn("dtss_save_merge/ext_diff_fx.db");

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_h_1, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_diff_fx\\.db"));
      REQUIRE_EQ(find_res.size(), 1);

      // add data to the same path

      CHECK_EQ(false, db.save(fn, pts_h_2, nullptr, {.recreate = false}).has_value());

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_diff_fx\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("fixed_dt old_dt != new_dt") {
      // data
      utctime const t0 = utctime_now();
      utctimespan const dt_h = calendar::HOUR;
      utctimespan const dt_d = calendar::DAY;
      std::size_t const n = 1000;
      // -----
      ta::fixed_dt f_ta_h{t0, dt_h, n};
      ta::fixed_dt f_ta_d{t0, dt_d, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      string fn("dtss_save_merge/ext_fixed_diff_dt.db"); // verify we can have path-parts

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_d, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_fixed_diff_dt\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        (void) db.save(fn, pts_h, nullptr, {.recreate = false}),
        std::runtime_error,
        "dtss_store: cannot merge unaligned fixed_dt");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_fixed_diff_dt\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("fixed_dt unaligned axes") {
      // data
      calendar utc{};
      utctime const t0_1 = utc.time(2000, 1, 1, 0, 0);
      utctime const t0_2 = utc.time(2000, 1, 1, 0, 13); // 13 minutes shifted
      utctimespan const dt_h = calendar::HOUR;
      std::size_t const n = 1000;
      // -----
      ta::fixed_dt f_ta_h{t0_1, dt_h, n};
      ta::fixed_dt f_ta_d{t0_2, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      string fn("dtss_save_merge/ext_fixed_unaligned.db"); // verify we can have path-parts

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_d, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_fixed_unaligned\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        (void) db.save(fn, pts_h, nullptr, {.recreate = false}),
        std::runtime_error,
        "dtss_store: cannot merge unaligned fixed_dt");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_fixed_unaligned\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("calendar_dt old_dt != new_dt") {
      // data
      utctime const t0 = utctime_now();
      utctimespan const dt_h = calendar::WEEK;
      utctimespan const dt_d = calendar::DAY;
      std::size_t const n = 1000;
      // -----
      ta::calendar_dt f_ta_h{utc_ptr, t0, dt_h, n};
      ta::calendar_dt f_ta_d{utc_ptr, t0, dt_d, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      string fn("dtss_save_merge/ext_cal_diff_dt.db"); // verify we can have path-parts

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_d, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_cal_diff_dt\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        (void) db.save(fn, pts_h, nullptr, {.recreate = false}),
        std::runtime_error,
        "dtss_store: cannot merge unaligned calendar_dt");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_cal_diff_dt\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("calendar_dt unaligned axes") {
      // data
      utctime const t0_1 = utc_ptr->time(2000, 1, 1, 0, 0);
      utctime const t0_2 = utc_ptr->time(2000, 1, 1, 0, 13); // 13 minutes shifted
      utctimespan const dt_h = calendar::DAY;
      std::size_t const n = 1000;
      // -----
      ta::calendar_dt f_ta_h{utc_ptr, t0_1, dt_h, n};
      ta::calendar_dt f_ta_d{utc_ptr, t0_2, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      string fn("dtss_save_merge/ext_cal_unaligned.db"); // verify we can have path-parts

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_d, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_cal_unaligned\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        (void) db.save(fn, pts_h, nullptr, {.recreate = false}),
        std::runtime_error,
        "dtss_store: cannot merge unaligned calendar_dt");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_cal_unaligned\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("calendar_dt different calendars") {
      // data
      std::shared_ptr<calendar> osl_ptr = std::make_shared<calendar>("Europe/Oslo");
      utctime const t0_1 = utc_ptr->time(2000, 1, 1, 0, 0);
      utctime const t0_2 = osl_ptr->time(2000, 1, 1, 0, 13); // 13 minutes shifted
      utctimespan const dt_h = calendar::DAY;
      std::size_t const n = 1000;
      // -----
      ta::calendar_dt f_ta_h{utc_ptr, t0_1, dt_h, n};
      ta::calendar_dt f_ta_d{osl_ptr, t0_2, dt_h, n};
      ts::point_ts<ta::generic_dt> pts_h{gta_t(f_ta_h), 0.};
      ts::point_ts<ta::generic_dt> pts_d{gta_t(f_ta_d), 0.};
      // -----
      string fn("dtss_save_merge/ext_cal_diff_cal.db"); // verify we can have path-parts

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_d, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find(string("dtss_save_merge/ext_cal_diff_cal\\.db"));
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path
      CHECK_THROWS_AS_MESSAGE(
        (void) db.save(fn, pts_h, nullptr, {.recreate = false}),
        std::runtime_error,
        "dtss_store: cannot merge calendar_dt with different calendars");

      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/ext_cal_diff_cal\\.db")); // should match our ts.
      CHECK_EQ(find_res.size(), 0);
    }
    SUBCASE("remove/non-existing") {
      CHECK_THROWS_AS(db.remove("rm-nonexisting-ts"), runtime_error const &);
    }
    SUBCASE("construct/w/wrong-args") {
      CHECK_THROWS_AS(ts_db_rocks(tmpdir.string(), db_cfg{0u}), runtime_error const &);
    }
  }

  TEST_CASE("dtss/rocks_db/merge_write") {

    // setup db
    test::utils::temp_dir tmpdir("ts.db.test");
    uint64_t ppf = 5;
    ts_db_rocks db(tmpdir.string(), db_cfg{ppf});
    std::shared_ptr<calendar> utc_ptr = std::make_shared<calendar>();

    SUBCASE("fixed_dt") {
      SUBCASE("exact") { // this is a full overwrite case
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 100;
        utctime const t0 = utc_ptr->time(2016, 1, 1);
        // -----
        ta::fixed_dt f_ta{t0, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta), 10.};
        // -----
        string fn("dtss_save_merge/fixed_exact.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_exact\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/fixed_exact.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());
        CHECK_UNARY(i.delta_t == dt);

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_exact\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old") {
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/fixed_new_in_old.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_new_in_old\\.db");
        CHECK_EQ(find_res.size(), 1);
        for (size_t drop = 4 * ppf; drop > 0; drop--) {
          // add data to the same path
          utctime const t0_new = t0_old + dt * int(drop);
          ta::fixed_dt f_ta_new{t0_new, dt, n - 2 * drop};
          ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), double(drop)};
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/fixed_new_in_old.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
          CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
          CHECK_EQ(res.total_period().end, f_ta_old.total_period().end);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }
        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new") { // this is a full overwrite case
        // data
        std::size_t const extra = 10u; // points to drop from start/end of old
        // -----
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 100;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        utctime const t0_new = t0_old - dt * extra;
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ta::fixed_dt f_ta_new{t0_new, dt, n + extra};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
        // -----
        string fn("dtss_save_merge/fixed_old_in_new.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/fixed_old_in_new.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);

        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap start of old") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);

        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/fixed_new_over_start.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_new_over_start\\.db");
        CHECK_EQ(find_res.size(), 1);

        for (size_t j = 0; j < 3 * ppf; j++) {
          utctime const t0_new = t0_old - dt * (n / 2 + j);
          ta::fixed_dt f_ta_new{t0_new, dt, n + 2 * j};
          ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), double(j)};
          REQUIRE_LT(t0_new, t0_old);
          REQUIRE_LT(t0_old, pts_new.total_period().end);
          REQUIRE_LT(pts_new.total_period().end, pts_old.total_period().end);
          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());
          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/fixed_new_over_start.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
          CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
          CHECK_EQ(res.total_period().end, f_ta_old.total_period().end);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_new_over_start\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap end of old") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);

        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 100.};

        // -----
        string fn("dtss_save_merge/fixed_new_over_end.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_new_over_end\\.db");
        CHECK_EQ(find_res.size(), 1);
        for (size_t j = 0; j < 3 * ppf; j++) {
          utctime const t0_new = t0_old + dt * (n / 2 - j);
          ta::fixed_dt f_ta_new{t0_new, dt, n + 2 * j};
          ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), double(j)};
          REQUIRE_LT(t0_old, t0_new);
          REQUIRE_LT(t0_new, pts_old.total_period().end);
          REQUIRE_LT(pts_old.total_period().end, pts_new.total_period().end);
          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/fixed_new_over_end.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
          CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
          CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }
        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_new_over_end\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("consecutive without gap") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/fixed_consec.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_consec\\.db");
        CHECK_EQ(find_res.size(), 1);
        // new after old
        utctime const t0_new = t0_old + dt * n;
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 1.};
        CHECK_EQ(pts_old.total_period().end, t0_new);
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/fixed_consec.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // new before old
        utctime const t0_new2 = t0_old - dt * n;
        ta::fixed_dt f_ta_new2{t0_new2, dt, n};
        ts::point_ts<ta::generic_dt> pts_new2{gta_t(f_ta_new2), 2.};
        CHECK_EQ(pts_new2.total_period().end, t0_old);
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new2, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res2, res2_tp] = db.read("dtss_save_merge/fixed_consec.db", utcperiod{});
        // time-axis
        CHECK_EQ(res2.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res2.total_period().start, f_ta_new2.total_period().start);
        CHECK_EQ(res2.total_period().end, res.total_period().end);
        // values
        validate_values_with_nans(res2, res, pts_new2);
        i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res2.total_period());


        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_consec\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new after old with gap") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/fixed_gap_after.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_gap_after\\.db");
        CHECK_EQ(find_res.size(), 1);

        utctime const t0_new = t0_old + 2 * n * dt;
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 1.};
        REQUIRE_LT(pts_old.total_period().end, t0_new);
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/fixed_gap_after.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, f_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_gap_after\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new before old with gap") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::fixed_dt f_ta_old{t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/fixed_gap_before.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/fixed_gap_before\\.db");
        CHECK_EQ(find_res.size(), 1);

        utctime const t0_new = t0_old - 2 * n * dt;
        ta::fixed_dt f_ta_new{t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 1.};
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/fixed_gap_before.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, f_ta_old.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/fixed_gap_before\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
    }
    SUBCASE("calendar_dt") {
      SUBCASE("exact") { // this is a full overwrite case
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 100;
        utctime const t0 = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta{utc_ptr, t0, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta), 10.};
        // -----
        string fn("dtss_save_merge/calendar_exact.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_exact\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/calendar_exact.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_exact\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old") {
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/calendar_new_in_old.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_new_in_old\\.db");
        CHECK_EQ(find_res.size(), 1);
        for (size_t drop = 4 * ppf; drop > 0; drop--) {
          utctime const t0_new = t0_old + dt * int(drop);
          ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n - 2 * drop};
          ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), double(drop)};
          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/calendar_new_in_old.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
          CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
          CHECK_EQ(res.total_period().end, c_ta_old.total_period().end);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }
        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new") { // this is a full overwrite case
        // data
        std::size_t const extra = 10u; // points to drop from start/end of old
        // -----
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 100;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        utctime const t0_new = t0_old - dt * extra;
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n + extra};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 10.};
        // -----
        string fn("dtss_save_merge/calendar_old_in_new.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/calendar_old_in_new.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap start of old") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/calendar_new_over_start.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_new_over_start\\.db");
        CHECK_EQ(find_res.size(), 1);
        for (size_t j = 0; j < 3 * ppf; j++) {
          utctime const t0_new = t0_old - dt * (n / 2 + j);
          ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n + 2 * j};
          ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), double(j)};
          REQUIRE_LT(t0_new, t0_old);
          REQUIRE_LT(t0_old, pts_new.total_period().end);
          REQUIRE_LT(pts_new.total_period().end, pts_old.total_period().end);
          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/calendar_new_over_start.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
          CHECK_EQ(res.total_period().start, c_ta_new.total_period().start);
          CHECK_EQ(res.total_period().end, c_ta_old.total_period().end);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }
        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_new_over_start\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap end of old") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 1.};
        // -----
        string fn("dtss_save_merge/calendar_new_over_end.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_new_over_end\\.db");
        CHECK_EQ(find_res.size(), 1);
        for (size_t j = 0; j < 3 * ppf; j++) {
          utctime const t0_new = t0_old + dt * (n / 2 - j);
          ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n + 2 * j};
          ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), double(j)};
          REQUIRE_LT(t0_old, t0_new);
          REQUIRE_LT(t0_new, pts_old.total_period().end);
          REQUIRE_LT(pts_old.total_period().end, pts_new.total_period().end);
          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/calendar_new_over_end.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
          CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
          CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_new_over_end\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("consecutive without gap") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/calendar_consec.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_consec\\.db");
        CHECK_EQ(find_res.size(), 1);
        // new after old
        utctime const t0_new = t0_old + dt * n;
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 1.};
        CHECK_EQ(pts_old.total_period().end, t0_new);
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/calendar_consec.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // new before old
        utctime const t0_new2 = t0_old - dt * n;
        ta::calendar_dt c_ta_new2{utc_ptr, t0_new2, dt, n};
        ts::point_ts<ta::generic_dt> pts_new2{gta_t(c_ta_new2), 1.};
        CHECK_EQ(pts_new2.total_period().end, t0_old);
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new2, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res2, res2_tp] = db.read("dtss_save_merge/calendar_consec.db", utcperiod{});
        // time-axis
        CHECK_EQ(res2.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res2.total_period().start, c_ta_new2.total_period().start);
        CHECK_EQ(res2.total_period().end, res.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res2.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_consec\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new after old with gap") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/calendar_gap_after.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_gap_after\\.db");
        CHECK_EQ(find_res.size(), 1);

        utctime const t0_new = t0_old + 2 * n * dt;
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 1.};
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/calendar_gap_after.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, c_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_new.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());


        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_gap_after\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new before old with gap") {
        // data
        utctimespan const dt = calendar::DAY;
        std::size_t const n = 10 * ppf;
        utctime const t0_old = utc_ptr->time(2016, 1, 1);
        // -----
        ta::calendar_dt c_ta_old{utc_ptr, t0_old, dt, n};
        ts::point_ts<ta::generic_dt> pts_old{gta_t(c_ta_old), 100.};
        // -----
        string fn("dtss_save_merge/calendar_gap_before.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/calendar_gap_before\\.db");
        CHECK_EQ(find_res.size(), 1);

        utctime const t0_new = t0_old - (2 * n) * dt;
        ta::calendar_dt c_ta_new{utc_ptr, t0_new, dt, n};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(c_ta_new), 1.};
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/calendar_gap_before.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::CALENDAR);
        CHECK_EQ(res.ta.size(), 3 * n);
        CHECK_EQ(res.total_period().start, c_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, c_ta_old.total_period().end);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/calendar_gap_before\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
    }
    SUBCASE("point_dt") {
      // setup db
      test::utils::temp_dir tmpdir_sub("ts.db.test");
      // data
      std::shared_ptr<calendar> utc_ptr = std::make_shared<calendar>();
      std::shared_ptr<calendar> osl_ptr = std::make_shared<calendar>("Europe/Oslo");

      utctimespan const dtx{10l};
      std::size_t const n = 10 * ppf;
      utctime const t0_old{0l};
      utctimespan const offset{5l}; // to misalign old and new timepoints
      // -----
      std::vector<utctime> old_timepoints{};
      std::vector<double> const old_values(n, 1.);
      // -----
      old_timepoints.reserve(n + 1);
      for (std::size_t i = 0; i <= n; ++i) {
        old_timepoints.emplace_back(t0_old + dtx * i);
      }
      ta::point_dt p_ta_old{old_timepoints};
      ts::point_ts<ta::generic_dt> pts_old{gta_t(p_ta_old), old_values};

      SUBCASE("exact") { // this is a full overwrite case
        std::vector<double> new_values(n, 10.);
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_old), new_values};
        // -----
        string fn("dtss_save_merge/point_exact.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_exact\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_exact.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period(), p_ta_old.total_period());
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());
        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_exact\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old with offset") {
        string fn("dtss_save_merge/point_new_in_old.db");
        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_new_in_old\\.db");
        REQUIRE_EQ(find_res.size(), 1);

        for (size_t drop = n / 2; drop > 0; drop--) {
          utctime const t0_new = t0_old + dtx * drop - offset;
          std::vector<utctime> new_timepoints{};
          new_timepoints.reserve(n - 2 * drop + 2);
          for (std::size_t i = 0; i <= n - 2 * drop + 1; ++i) {
            new_timepoints.emplace_back(t0_new + i * dtx);
          }
          ta::point_dt p_ta_new{new_timepoints};
          std::vector<double> new_values(new_timepoints.size() - 1, double(drop) * 10.);
          ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};

          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/point_new_in_old.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
          CHECK_EQ(res.total_period(), p_ta_old.total_period());
          CHECK_EQ(res.ta.size(), n + 1);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old without offset") {
        string fn("dtss_save_merge/point_new_in_old.db");
        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_new_in_old\\.db");
        REQUIRE_EQ(find_res.size(), 1);

        for (size_t drop = n / 2 - 1; drop > 0; drop--) {
          utctime const t0_new = t0_old + dtx * drop;
          std::vector<utctime> new_timepoints{};
          new_timepoints.reserve(n - 2 * drop);
          for (std::size_t i = 0; i <= n - 2 * drop; ++i) {
            new_timepoints.emplace_back(t0_new + i * dtx);
          }
          ta::point_dt p_ta_new{new_timepoints};
          std::vector<double> new_values(new_timepoints.size() - 1, double(drop) * 10.);
          ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};

          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/point_new_in_old.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
          CHECK_EQ(res.total_period(), p_ta_old.total_period());
          CHECK_EQ(res.ta.size(), n);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new contained in old/fixed_dt") {
        std::size_t const drop = 10u; // points to drop from start/end of old
        utctime const t0_new = t0_old + offset + dtx * drop;
        ts::point_ts<ta::generic_dt> pts_new{gta_t(t0_new, dtx, n - 2 * drop), 10};
        // -----
        string fn("dtss_save_merge/point_new_in_old.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_new_in_old\\.db");
        REQUIRE_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_new_in_old.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period(), p_ta_old.total_period());
        CHECK_EQ(res.ta.size(), n + 1);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_in_old\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new") { // this is a full overwrite case
        // data
        std::size_t const extra = 10u; // points to drop from start/end of old
        utctime const t0_new = t0_old + offset - dtx * extra;
        // -----
        std::vector<utctime> new_timepoints{};
        new_timepoints.reserve(n + 2 * extra + 1);
        for (std::size_t i = 0; i <= n + 2 * extra; ++i) {
          new_timepoints.emplace_back(t0_new + i * dtx);
        }
        std::vector<double> new_values(new_timepoints.size() - 1, 10.);
        // -----
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        string fn("dtss_save_merge/point_old_in_new.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_old_in_new.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period(), p_ta_new.total_period());
        // values
        validate_values_with_nans(res, pts_old, pts_new);

        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("old contained in new/fixed") {
        std::size_t const extra = 10u; // points to add from start/end of old
        utctime const t0_new = t0_old + offset - dtx * extra;
        ts::point_ts<ta::generic_dt> pts_new{gta_t(t0_new, dtx, n + 2 * extra), 10};
        // -----
        string fn("dtss_save_merge/point_old_in_new.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_old_in_new\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_old_in_new.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period(), pts_new.total_period());
        // values
        validate_values_with_nans(res, pts_old, pts_new);

        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_old_in_new\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }

      SUBCASE("new overlap start of old") {
        string fn("dtss_save_merge/point_new_over_start.db");
        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_new_over_start\\.db");
        CHECK_EQ(find_res.size(), 1);

        for (size_t j = 0; j < 10 * ppf; j++) {
          utctime const t0_new = t0_old - offset - dtx * j;
          std::vector<utctime> new_timepoints{};
          for (std::size_t i = 0; i <= 2 * j + 1; ++i) {
            new_timepoints.emplace_back(t0_new + i * dtx);
          }
          ta::point_dt p_ta_new{new_timepoints};
          std::vector<double> new_values(new_timepoints.size() - 1, double(j) * 10.);
          ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
          REQUIRE_LT(t0_new, t0_old);
          REQUIRE_LT(pts_new.total_period().end, pts_old.total_period().end);

          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/point_new_over_start.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
          CHECK_EQ(res.total_period().start, p_ta_new.total_period().start);
          CHECK_EQ(res.total_period().end, p_ta_old.total_period().end);
          CHECK_EQ(res.size(), n + j + 1);

          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }
        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_over_start\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new overlap end of old") {
        string fn("dtss_save_merge/point_new_over_end.db");
        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_new_over_end\\.db");
        CHECK_EQ(find_res.size(), 1);

        for (size_t j = 0; j < 10 * ppf; j++) {
          utctime const t0_new = t0_old - offset + dtx * (n - j);
          std::vector<utctime> new_timepoints{};
          for (std::size_t i = 0; i <= 2 * j + 1; ++i) {
            new_timepoints.emplace_back(t0_new + i * dtx);
          }
          ta::point_dt p_ta_new{new_timepoints};
          std::vector<double> new_values(new_timepoints.size() - 1, double(j) * 10.);
          ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
          REQUIRE_LT(t0_old, t0_new);
          REQUIRE_LT(pts_old.total_period().end, pts_new.total_period().end);
          // add data to the same path
          CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

          // check merged data
          auto [res, res_tp] = db.read("dtss_save_merge/point_new_over_end.db", utcperiod{});
          // time-axis
          CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
          CHECK_EQ(res.total_period().start, p_ta_old.total_period().start);
          CHECK_EQ(res.total_period().end, p_ta_new.total_period().end);
          CHECK_EQ(res.size(), n + j + 1);
          // values
          validate_values_with_nans(res, pts_old, pts_new);
          auto i = db.get_ts_info(fn);
          CHECK_UNARY(i.data_period == res.total_period());
        }

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_new_over_end\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("consecutive without gap") {
        string fn("dtss_save_merge/point_consec.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_consec\\.db");
        CHECK_EQ(find_res.size(), 1);

        // new before old
        utctime const t0_new = t0_old - n * dtx;
        std::vector<utctime> new_timepoints{};
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dtx);
        }
        // -----
        ta::point_dt p_ta_new{new_timepoints};
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), 10};
        REQUIRE_LT(t0_new, t0_old);
        REQUIRE_EQ(pts_new.total_period().end, t0_old);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_consec.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.total_period().start, p_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_old.total_period().end);
        CHECK_EQ(res.size(), 2 * n);
        // values
        validate_values_with_nans(res, pts_old, pts_new);
        auto tsi = db.get_ts_info(fn);
        CHECK_UNARY(tsi.data_period == res.total_period());

        // new after old
        utctime const t0_new2 = t0_old + n * dtx;
        std::vector<utctime> new_timepoints2{};
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints2.emplace_back(t0_new2 + i * dtx);
        }
        // -----
        ta::point_dt p_ta_new2{new_timepoints2};
        ts::point_ts<ta::generic_dt> pts_new2{gta_t(p_ta_new2), 20};
        REQUIRE_EQ(t0_new2, res.total_period().end);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new2, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res2, res2_tp] = db.read("dtss_save_merge/point_consec.db", utcperiod{});
        // time-axis
        CHECK_EQ(res2.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res2.total_period().start, res.total_period().start);
        CHECK_EQ(res2.total_period().end, pts_new2.total_period().end);
        CHECK_EQ(res2.size(), 3 * n);
        // values
        validate_values_with_nans(res2, res, pts_new2);
        tsi = db.get_ts_info(fn);
        CHECK_UNARY(tsi.data_period == res2.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_consec\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new after old with gap") {
        // data
        utctime const t0_new = t0_old + n * dtx + offset;
        std::vector<utctime> new_timepoints{};
        new_timepoints.reserve(n + 1);
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dtx);
        }
        // -----
        ta::point_dt p_ta_new{new_timepoints};
        std::vector<double> new_values(new_timepoints.size() - 1, 10.);
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        string fn("dtss_save_merge/point_gap_after.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_gap_after\\.db");
        CHECK_EQ(find_res.size(), 1);

        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_gap_after.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        CHECK_EQ(res.ta.size(), 2 * n + 1);
        CHECK_EQ(res.total_period().start, p_ta_old.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_new.total_period().end);
        // values
        REQUIRE_GE(res.size(), 2 * n + 1);
        CHECK_EQ(res.v.at(0), 1.);
        CHECK_EQ(res.v.at(n - 1), 1.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_EQ(res.v.at(n + 1), 10.);
        CHECK_EQ(res.v.at(2 * n), 10.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_gap_after\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
      SUBCASE("new before old with gap") {
        // data
        utctime const t0_new = t0_old - n * dtx - offset;
        std::vector<utctime> new_timepoints{};
        new_timepoints.reserve(n + 1);
        for (std::size_t i = 0; i <= n; ++i) {
          new_timepoints.emplace_back(t0_new + i * dtx);
        }
        // -----
        ta::point_dt p_ta_new{new_timepoints};
        std::vector<double> new_values(new_timepoints.size() - 1, 10.);
        ts::point_ts<ta::generic_dt> pts_new{gta_t(p_ta_new), new_values};
        // -----
        string fn("dtss_save_merge/point_gap_before.db");

        // save initital data
        CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
        auto find_res = db.find("dtss_save_merge/point_gap_before\\.db");
        CHECK_EQ(find_res.size(), 1);
        // add data to the same path
        CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = false}).has_value());

        // check merged data
        auto [res, res_tp] = db.read("dtss_save_merge/point_gap_before.db", utcperiod{});
        // time-axis
        CHECK_EQ(res.ta.gt(), time_axis::generic_dt::POINT);
        REQUIRE_EQ(res.ta.size(), 2 * n + 1);
        CHECK_EQ(res.total_period().start, p_ta_new.total_period().start);
        CHECK_EQ(res.total_period().end, p_ta_old.total_period().end);
        // values
        CHECK_EQ(res.v.at(0), 10.);
        CHECK_EQ(res.v.at(n - 1), 10.);
        CHECK_UNARY(std::isnan(res.v.at(n)));
        CHECK_EQ(res.v.at(n + 1), 1.);
        CHECK_EQ(res.v.at(2 * n), 1.);
        auto i = db.get_ts_info(fn);
        CHECK_UNARY(i.data_period == res.total_period());

        // cleanup
        db.remove(fn);
        find_res = db.find(string("dtss_save_merge/point_gap_before\\.db"));
        CHECK_EQ(find_res.size(), 0);
      }
    }
    SUBCASE("force overwrite") {
      // data
      std::size_t const drop = 10u; // points to drop from start/end of old
      // -----
      utctimespan const dt = calendar::DAY;
      std::size_t const n = 100;
      utctime const t0_old = utc_ptr->time(2016, 1, 1);
      utctime const t0_new = t0_old + dt * drop;
      // -----
      ta::fixed_dt f_ta_old{t0_old, dt, n};
      ta::fixed_dt f_ta_new{t0_new, dt, n - 2 * drop};
      ts::point_ts<ta::generic_dt> pts_old{gta_t(f_ta_old), 1.};
      ts::point_ts<ta::generic_dt> pts_new{gta_t(f_ta_new), 10.};
      // -----
      string fn("dtss_save_merge/force_overwrite.db");

      // save initital data
      CHECK_EQ(false, db.save(fn, pts_old, nullptr, {.recreate = false}).has_value());
      auto find_res = db.find("dtss_save_merge/force_overwrite\\.db");
      CHECK_EQ(find_res.size(), 1);

      // add data to the same path with overwrite
      CHECK_EQ(false, db.save(fn, pts_new, nullptr, {.recreate = true}).has_value());

      // check merged data
      auto [res, res_tp] = db.read(fn, utcperiod{});
      // time-axis
      CHECK_EQ(res.ta.gt(), time_axis::generic_dt::FIXED);
      CHECK_EQ(res.total_period().start, f_ta_new.total_period().start);
      CHECK_EQ(res.total_period().end, f_ta_new.total_period().end);
      // values
      CHECK_EQ(res.v.at(0), 10.);
      CHECK_EQ(res.v.at(n - 2 * drop - 1), 10.);
      auto i = db.get_ts_info(fn);
      CHECK_UNARY(i.data_period == res.total_period());
      // cleanup
      db.remove(fn);
      find_res = db.find(string("dtss_save_merge/force_overwrite\\.db"));
      CHECK_EQ(find_res.size(), 0);
    }
  }

  TEST_CASE("dtss/rocks_db/minimal_point_merge") {
    using namespace shyft::dtss;
    using namespace shyft::time_series::dd;
    using time_series::point_ts;

    time_axis::point_dt pta{vector<utctime>{utctime{0l}}, utctime{10l}};
    time_axis::point_dt ptb{vector<utctime>{utctime{2l}}, utctime{4l}};
    time_axis::point_dt ptr{
      vector<utctime>{utctime{0l}, utctime{2l}, utctime{4l}},
      utctime{10l}
    };

    gts_t a(gta_t(pta), 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    gts_t b(gta_t(ptb), 0.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    gts_t r(gta_t(ptr), vector<double>{1.0, 0.0, 1.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    std::string fn("p1.db");
    test::utils::temp_dir tmpdir("ts.db.p1");
    ts_db_rocks db(tmpdir.string(), db_cfg{});
    CHECK_EQ(false, db.save(fn, a, nullptr, {.recreate = true}).has_value());
    CHECK_EQ(false, db.save(fn, b, nullptr, {.recreate = false}).has_value());
    auto [rx, rx_tp] = db.read(fn, utcperiod{});
    FAST_CHECK_EQ(rx, r);
  }

  TEST_CASE("dtss/rocks_db/header") {
    using namespace shyft::dtss::detail;
    using namespace shyft::time_axis;
    using namespace shyft::time_series;
    SUBCASE("default_ct") {
      ts_db_rocks_header h;
      FAST_CHECK_EQ(h.t0, no_utctime);
      FAST_CHECK_EQ(h.dt, utctime{0l});
      FAST_CHECK_EQ(h.n, 0);
      FAST_CHECK_EQ(h.tff, no_utctime);
      FAST_CHECK_EQ(h.ts_id, 0);
      FAST_CHECK_EQ(h.modified, no_utctime);
      FAST_CHECK_EQ(h.tz, string(""));
      FAST_CHECK_EQ(h.version, 0);
    }
    SUBCASE("std_ct") {
      ts_db_rocks_header h(
        ts_point_fx::POINT_INSTANT_VALUE,
        generic_dt::generic_type::CALENDAR,
        utctime{10l},
        utctime{20l},
        30,
        utctime{40l},
        50,
        utctime{60l},
        "UTC");
      FAST_CHECK_EQ(h.t0, utctime{10l});
      FAST_CHECK_EQ(h.dt, utctime{20l});
      FAST_CHECK_EQ(h.n, 30);
      FAST_CHECK_EQ(h.tff, utctime{40l});
      FAST_CHECK_EQ(h.ts_id, 50);
      FAST_CHECK_EQ(h.modified, utctime{60l});
      FAST_CHECK_EQ(h.tz, string("UTC"));
      FAST_CHECK_EQ(h.version, 0);
    }
    SUBCASE("total_period/fixed_dt") {
      ts_db_rocks_header h(
        ts_point_fx::POINT_INSTANT_VALUE,
        generic_dt::generic_type::FIXED,
        utctime{10l},
        utctime{20l},
        30,
        utctime{40l},
        50,
        utctime{60l},
        "UTC");
      FAST_CHECK_EQ(
        h.total_period([](char const *) {
          return calendar();
        }),
        utcperiod(utctime{10l}, utctime{10l} + utctime{20l} * 30));
    }
    SUBCASE("total_period/calendar_dt") {
      auto const tz_id = "Europe/Oslo";
      calendar cal(tz_id);
      auto const cal_fx = [&cal](char const *) {
        return cal;
      };
      auto t0 = cal.time(2022, 1, 1);
      auto dt = calendar::MONTH;
      size_t n = 12;
      auto tff = t0;
      ts_db_rocks_header h(
        ts_point_fx::POINT_AVERAGE_VALUE, generic_dt::generic_type::CALENDAR, t0, dt, n, tff, 50, utctime{60l}, tz_id);
      FAST_CHECK_EQ(h.total_period(cal_fx), utcperiod{t0, cal.add(t0, dt, n)});
    }
    SUBCASE("total_period/point_dt") {
      calendar cal;
      auto const cal_fx = [&cal](char const *) {
        return cal;
      };
      auto t0 = cal.time(2022, 1, 1);
      auto dt = calendar::MONTH;
      size_t n = 120;
      auto tff = t0;
      ts_db_rocks_header h(
        ts_point_fx::POINT_AVERAGE_VALUE, generic_dt::generic_type::POINT, t0, dt, n, tff, 50, utctime{60l}, "");
      FAST_CHECK_EQ(h.total_period(cal_fx), utcperiod{t0, t0 + dt});
    }
  }

  TEST_CASE("dtss/rocks_db/frag_key") {
    using namespace shyft::dtss::detail;
    SUBCASE("default_ct") {
      frag_key f;
      CHECK_UNARY(f.empty());
      CHECK_EQ(f.ts_id(), 0);
      CHECK_EQ(f.time(), min_utctime);
      // verify setters..
      f.set_frag_type('v');
      CHECK_UNARY(!f.empty());
      FAST_CHECK_EQ(f.frag_type(), 'v');
      f.set_ts_id(2);
      FAST_CHECK_EQ(f.ts_id(), 2);
      f.set_time(utctime{3l});
      FAST_CHECK_EQ(f.time(), utctime{3l});
    }
    SUBCASE("useful_ct") {
      frag_key f{1, 'v', utctime{2l}};
      CHECK_UNARY(!f.empty());
      FAST_CHECK_EQ(f.ts_id(), 1);
      FAST_CHECK_EQ(f.time(), utctime{2l});
      // verify we can construct from view
      frag_key f2{frag_key_view{f}};
      FAST_CHECK_EQ(f, f2); // verify comparison
      // verify we can construct slice
      rocksdb::Slice s{f}; // slice from frag-key proven here
      frag_key_view v1{s}; // frag-key view from slice proven here
      frag_key f3{v1};
      FAST_CHECK_EQ(f, f3);
    }
    SUBCASE("comparison") {
      // vital for us: we need to have strict memcmp order, and notice that memcmp do compare unsigned bytes
      //               so we really need to take care when storing signed number, like the timestamp.
      calendar utc; // make numbers with a lot of digits
      auto t1 = utc.time(
        1948, 1, 1); // make sure to include a timestamp before 1970, neg number, two complementary form
      auto t2 = utc.time(2022, 2, 1);
      uint64_t id1 = 1000000002Lu;
      uint64_t id2 = 2000000001Lu;

      frag_key f11v{id1, 'v', t1};
      frag_key f21v{id2, 'v', t1};
      frag_key f12v{id1, 'v', t2};
      frag_key f12t{id1, 't', t2};
      FAST_CHECK_UNARY(f11v < f12v); // order by t
      FAST_CHECK_UNARY(f12v < f21v); // order by tsid
      FAST_CHECK_UNARY(f12t < f12v); // order by timestamps first, then value keys.
    }
    SUBCASE("zero_copy_partial_slice") {
      frag_key f{1, 'v', utctime{2l}};
      auto ts_id_only = f.slice_ts_id();
      FAST_CHECK_EQ(ts_id_only.size(), sizeof(uint64_t));
      FAST_CHECK_EQ(ts_id_only.data(), f.data()); // should be same memory zero copy
      auto ts_id_type_only = f.slice_ts_id_type();
      FAST_CHECK_EQ(ts_id_type_only.size(), sizeof(uint64_t) + 1);
      FAST_CHECK_EQ(ts_id_type_only.data(), f.data()); // should be same memory zero copy
    }
  }

  TEST_CASE("dtss/rocks_db/exists_at") {
    test::utils::temp_dir tmpdir;
    auto r = (tmpdir / "e").string();
    CHECK(!ts_db_rocks::exists_at(r));
    ts_db_rocks ldb{r, db_cfg{}};
    CHECK(ts_db_rocks::exists_at(r));
  }

  TEST_CASE("dtss/rocks_db/frag_v") {
    using namespace shyft::dtss::detail;
    double d[2]{1.0, 2.0};
    rocksdb::Slice s(reinterpret_cast<char const *>(d), 2 * sizeof(double));
    frag_v<double> f{s};
    FAST_CHECK_EQ(f.size(), 2);
    FAST_CHECK_EQ(f.data()[0], doctest::Approx(d[0]));
    FAST_CHECK_EQ(f.data()[1], doctest::Approx(d[1]));
    vector<double> dv;
    f.copy(dv);
    FAST_CHECK_EQ(dv.size(), 2);
    FAST_CHECK_EQ(dv[0], doctest::Approx(d[0]));
    FAST_CHECK_EQ(dv[1], doctest::Approx(d[1]));
    vector<double> sv;
    f.copy_slice(1, 1, sv); // skip 1, take 1
    FAST_CHECK_EQ(sv.size(), 1);
    FAST_CHECK_EQ(sv[0], doctest::Approx(d[1]));
  }

  TEST_CASE("dtss/rocks_db/bugs") {
    SUBCASE("0") {
      test::utils::temp_dir tmpdir("ts.db.test");

      db_cfg cfg{.ppf = 4u};
      ts_db_rocks db(tmpdir.string(), cfg);

      REQUIRE((cfg.ppf >= 4 && ((cfg.ppf % 4) == 0)));

      auto const fn = "dtss/rocks_db/bug/0.db";
      utctimespan const dt{1l};

      utctime const t0{0l};
      auto const n0 = cfg.ppf / 2;
      ta::fixed_dt const ta0{t0, dt, (std::size_t) n0};
      ts::point_ts<ta::generic_dt> const ts0{ta::generic_dt(ta0), shyft::nan};

      CHECK_EQ(false, db.save(fn, ts0, nullptr, {.recreate = false}).has_value());

      utctime const t1{cfg.ppf + cfg.ppf / 2};
      auto const n1 = cfg.ppf / 2;
      ta::fixed_dt const ta1{t1, dt, (std::size_t) n1};
      ts::point_ts<ta::generic_dt> const ts1{ta::generic_dt(ta1), 100.0};

      REQUIRE_LT(ta0.total_period().end, t1);
      CHECK_EQ(false, db.save(fn, ts1, nullptr, {.recreate = false}).has_value());

      utctime const t2{3 * cfg.ppf / 4};
      auto const n2 = cfg.ppf / 2;
      utcperiod const p{t2, t2 * n2};
      auto [ts2, ts2_tp] = db.read(fn, p);
      validate_values_with_nans(ts2, ts0, ts1);

      db.remove(fn);
    }
  }

  TEST_CASE("dtss/rocks_db/best_effort") {
    test::utils::temp_dir tmpdir("ts.rdb.best_effort");
    ts_db_rocks db(tmpdir.string(), {});
    utctime const t0{0l};
    utctime const dt1{1l};
    utctime const dt2{2l};
    auto const n0 = 10;
    ts::point_ts<ta::generic_dt> const ts0{ta::generic_dt(t0, dt1, n0), 1.0, ts::POINT_AVERAGE_VALUE};
    // save two time-series
    CHECK_EQ(false, db.save("a", ts0, nullptr, {.recreate = false}).has_value());
    CHECK_EQ(false, db.save("b", ts0, nullptr, {.recreate = false}).has_value());

    // make a ts that is unaligned,
    ts::point_ts<ta::generic_dt> const ts1{ta::generic_dt(t0, dt2, n0), 2.0, ts::POINT_AVERAGE_VALUE};
    // do a best effort save, and instead of exception, make it continue saving next
    size_t cache_requests = 0;
    auto cache_req = [&](size_t, std::optional<gts_t>, utcperiod) {
      ++cache_requests;
    };
    auto diags = db.save(
      2,
      [&](size_t i) {
        return i == 0 ? ts_item_t{"a", ts1} : ts_item_t{"b", ts0};
      },
      cache_req,
      {.recreate = false, .best_effort = true});
    // now verify we got behaviour that we want.
    REQUIRE(diags.has_value());
    REQUIRE_EQ(diags->size(), 1u);
    CHECK_EQ(cache_requests, 1u);
    CHECK_EQ((*diags)[0], diagnostics{.ix = 0, .diag = ts_diagnostics::miss_matched_resolution});
  }

  TEST_CASE("dtss/rocks_db/migration") {
    fs::path test_path;
    if (auto data_dir = getenv("SHYFT_DATA"))
      test_path = data_dir;
    if (test_path.empty() || !fs::exists(test_path / "dtss")) {
      MESSAGE("SHYFT_DATA not set or missing dtss dir, skipping migration test");
    } else {
      auto dtss_root_template = test_path / "dtss";
      test::utils::temp_dir tmpdir("ts.db.migration");
      auto dtss_root = tmpdir / "dtss";
      fs::create_directories(dtss_root);
      fs::copy(dtss_root_template, dtss_root, fs::copy_options::recursive);
      auto verify_content_of_migrated_db = [&]() {
        ts_db_rocks db(dtss_root.string(), {});
        auto find_res = db.find(string(".*"));
        CHECK_EQ(find_res.size(), 3u); // the test db contains 3 items exactly
        calendar cal("Europe/Oslo");
        utcperiod expected_data_period{cal.time(2024, 11, 9), cal.time(2024, 11, 10)};
        std::unordered_set<std::string> expected_names{"f", "c", "p"};
        for (auto const &i : find_res) { // verify each of them are complete
          CHECK_EQ(i.point_fx, POINT_AVERAGE_VALUE);
          CHECK_EQ(cal.calendar_units(i.modified).year, 2024);
          CHECK_EQ(i.data_period, expected_data_period);
          CHECK_EQ(expected_names.contains(i.name), true);
          expected_names.erase(i.name);
        }
        CHECK_EQ(expected_names.empty(), true); // only one of each
        CHECK_EQ(fs::exists(dtss_root / "rdb_h.migrated"), true);
      };
      verify_content_of_migrated_db();
      //--verify we can rollback migration
      fs::remove_all(dtss_root / "rdb_h.migrated"); // get rid of it.
      CHECK_EQ(true, ts_db_rocks::rollback_migration_at(dtss_root.string()));
      CHECK_EQ(true, fs::exists(dtss_root / "rdb_h"));
      verify_content_of_migrated_db(); // creative part: we re-migrate and verify content
    }
  }

  TEST_CASE("dtss/rocks_db/maintain") {
    test::utils::temp_dir tmpdir("ts.rdb.maintain");
    db_cfg cfg{
      .ppf = 1024,
      .compression = false,
      .max_file_size = 10 * 1024,
      .write_buffer_size = 1024 * 1024,
      .log_level = 200};
    ts_db_rocks db(tmpdir.string(), cfg);
    utctime const t0{0l};
    utctime const dt1{1l};
    utctime const dt2{2l};
    auto const n0 = 10000;
    ts::point_ts<ta::generic_dt> const ts0{ta::generic_dt(t0, dt1, n0), 1.0, ts::POINT_AVERAGE_VALUE};
    size_t cache_requests = 0;
    auto cache_req = [&](size_t, std::optional<gts_t>, utcperiod) {
      ++cache_requests;
    };
    auto const n_ts = 1000;
    std::vector<std::string> ts_names;
    auto diags = db.save(
      n_ts,
      [&](size_t i) {
        ts_names.emplace_back(fmt::format("ts{}", i));
        return ts_item_t{ts_names.back(), ts0};
      },
      cache_req,
      {.recreate = false, .best_effort = true});
    // now verify we got behaviour that we want.
    CHECK_EQ(cache_requests, n_ts);
    CHECK_EQ(diags.has_value(), false);
    db.maintain(true, true);
    db.maintain(false, true);
    db.maintain(true, false);
    auto find_res = db.find(string("ts.*"));
    CHECK_EQ(find_res.size(), n_ts); // just verify it still works after maintain
    // further testing done
  }

  TEST_CASE("dtss/rocks_db/fixed_fragment_ends") {
    test::utils::temp_dir tmpdir("ts.rdb.best_effort");
    ts_db_rocks db(tmpdir.string(), {});
    utctime const t0 = utctime_now();
    utctimespan const dt_m = calendar::MINUTE;
    std::size_t const n = 2;
    // -----
    ts::point_ts<ta::generic_dt> const ts0{ta::generic_dt(t0, dt_m, n), 1.0, ts::POINT_AVERAGE_VALUE};
    ts::point_ts<ta::generic_dt> const ts1{ta::generic_dt(t0 + calendar::DAY, dt_m, n), 1.0, ts::POINT_AVERAGE_VALUE};
    std::string name{"a"};
    CHECK_EQ(false, db.save(name, ts0, nullptr, {.recreate = false}).has_value());
    CHECK_EQ(false, db.save(name, ts1, nullptr, {.recreate = false}).has_value());

    auto [r1, r1p] = db.read(name, utcperiod{t0, t0 + calendar::MINUTE * 4});
    ts::point_ts<ta::generic_dt> expected{
      ta::generic_dt(t0, dt_m, 5), {1.0, 1.0, shyft::nan, shyft::nan, shyft::nan},
         ts::POINT_AVERAGE_VALUE
    };
    FAST_CHECK_UNARY(semantically_equal(r1, expected));
  }

  TEST_CASE_FIXTURE(_basics_, "dtss/rocks_db/basics/fixed_dt_issue") {
    /**
     * Issue detected by Svein Elgstøen and Marius Munthe-Kaas,
     * We got a description precise enough to
     * carv up the test-case close enough to expose
     * the problem.
     */
    auto t0 = from_seconds(0);
    auto dt = from_seconds(60);
    // ppf is 5, so we arrange the test for that.
    gta_t f0_ta{t0, dt, 2};
    ts::point_ts<gta_t> f0{f0_ta, 1.0, ts::POINT_AVERAGE_VALUE};
    gta_t f1_t{t0 + dt * (ppf + 2), dt, 2};
    ts::point_ts<gta_t> f1{f1_t, 2.0, ts::POINT_AVERAGE_VALUE};
    gta_t f2_t{t0 + dt * (2 * ppf + 1), dt, 2};
    ts::point_ts<gta_t> f2{f2_t, 3.0, ts::POINT_AVERAGE_VALUE};
    // clang-format off
    string fn("mffr/x1");
    CHECK_EQ(false, db.save(fn, f0, nullptr, {.recreate = true}).has_value());
    // read all
    auto [r1, r1p] = db.read(fn, utcperiod{});
    CHECK_EQ(f0.point_interpretation(), r1.point_interpretation());
    CHECK_EQ(f0.time_axis(), r1.time_axis());
    validate_values(r1, f0);
    CHECK_EQ(false, db.save(fn, f1, nullptr, {.recreate = false}).has_value());
    auto [r2, r2p] = db.read(fn, utcperiod{});
    std::vector<double> expected_r2{
      1.0,1.0,nan,nan,nan,
      nan,nan,2.0,2.0
    };
    CHECK_EQ(nan_equal(r2.values(), expected_r2), true);
    CHECK_EQ(false, db.save(fn, f2, nullptr, {.recreate = false}).has_value());
    auto [r3, r3p] = db.read(fn, utcperiod{});
    std::vector<double> expected_r3{
      1.0,1.0,nan,nan,nan,
      nan,nan,2.0,2.0,nan,
      nan,3.0,3.0
    };
    CHECK_EQ(nan_equal(r3.values(), expected_r3), true);
    utcperiod rpp{t0+(5+1)*dt,t0+dt*(2*5+1)};
    auto [r4, r4p] = db.read(fn, rpp);
    std::vector<double> expected_r4{
      //1.0,1.0,nan,nan,nan,
      //nan,
      nan,2.0,2.0,nan,
      nan,3.0
      //3.0
    };
    CHECK_EQ(nan_equal(r4.values(), expected_r4), true);
    // clang-format on
  }

  TEST_SUITE_END();

}
