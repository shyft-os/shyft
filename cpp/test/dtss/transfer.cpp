
#include <shyft/dtss/exchange/managers.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/transfer/config.h>
#include <shyft/dtss/transfer/engine.h>
#include <shyft/dtss/transfer/logger.h>
#include <shyft/dtss/ts_subscription.h>

#include <test/test_pch.h>
#include <test/test_utils.h>

using namespace std::chrono_literals;

namespace shyft::dtss {
  namespace {
    struct transfer_fixture {
      server_state local{};
      binary_server local_bin{local};
      server_state remote{};
      binary_server remote_bin{remote};
      test::utils::temp_dir root{"shyft.dtss.xfer"};
      int l_port{0};
      int r_port{0};
      core::calendar utc{};
      utctime t0;

      transfer_fixture() {
        add_container(local, "stm", (root / "local").string(), "ts_rdb", {});
        add_container(remote, "stm", (root / "remote").string(), "ts_rdb", {});
        l_port = local_bin.start_server();
        t0 = utc.time(2000, 1, 1);
      }

      ~transfer_fixture() {
      }

      auto make_test_tsv(size_t n, double value, size_t n_values = 3, utctime dt = core::from_seconds(3600)) {
        ts_vector_t r;
        time_axis::generic_dt ta{t0, dt, n_values};
        for (auto i = 0u; i < n; ++i)
          r.emplace_back(
            shyft_url("stm", fmt::format("r/{}.realised", i)),
            apoint_ts{ta, double(i) + value, time_series::POINT_AVERAGE_VALUE});
        return r;
      };
    };
  }

  TEST_SUITE_BEGIN("dtss");

  TEST_CASE_FIXTURE(transfer_fixture, "dtss/transfer/push_to_remote") {
    //
    // Arrange the test (a bit verbose yet)
    //
    // params that we would like to permutate
    size_t n_ts{0};                       // = 100; // 1, 10, 100
    std::string remote_extra = "from/b/"; // or "", no translation.
    bool with_subscription{true};
    bool ts_exists_before_start{true};
    bool remote_missing_at_start{false};

    SUBCASE("with_subcription") {
      SUBCASE("ts/1") {
        n_ts = 1;
        SUBCASE("no_transform") {
          remote_extra = "";
        }
        SUBCASE("w_transform") {
        }
      }
      SUBCASE("ts/100") {
        n_ts = 100;
        SUBCASE("no_transform") {
          remote_extra = "";
        }
        SUBCASE("w_transform") {
        }
      }
    }
    SUBCASE("no_subcription") {
      with_subscription = false;
      SUBCASE("ts/1") {
        n_ts = 1;
        SUBCASE("no_transform") {
          remote_extra = "";
        }
        SUBCASE("w_transform") {
        }
      }
      SUBCASE("ts/100") {
        n_ts = 100;
        SUBCASE("no_transform") {
          remote_extra = "";
        }
        SUBCASE("w_transform") {
        }
      }
    }
    SUBCASE("missing-ts-when-started") {
      ts_exists_before_start = false;
      n_ts = 10;
    }
    SUBCASE("remote-down-when-started") {
      remote_missing_at_start = true;
      n_ts = 10;
    }
    constexpr auto bool_str = [](bool x, std::string_view t, std::string_view f) {
      return std::string(x ? t : f);
    };
    MESSAGE(
      "Case with " << bool_str(with_subscription, "subscription", "oneshot") << " n_ts=" << n_ts
                   << ",ts_exists_before_start:" << bool_str(ts_exists_before_start, "yes", "no")
                   << ",remote_missing_at_start:" << bool_str(remote_missing_at_start, "yes", "no")
                   << ", remote-name transform:" << remote_extra);
    utctime dt{core::from_seconds(3600)};
    auto tsv0 = make_test_tsv(n_ts, 0.1);
    // one extra to prove we do not replicate non-matching ts.
    time_axis::generic_dt ta1{
      std::vector<utctime>{t0, t0 + 1 * dt, t0 + 2 * dt, t0 + 3 * dt}
    };
    time_series::dd::ats_vector last_written{tsv0}; // keep initial write as last_written
    tsv0.push_back(
      apoint_ts{
        shyft_url("stm", "r/b.realised.plan"), apoint_ts{ta1, 2.0, time_series::POINT_AVERAGE_VALUE}
    });
    auto c_l = client(fmt::format("localhost:{}", l_port));
    if (ts_exists_before_start) {
      (void) c_l.store_ts(tsv0, store_policy{.recreate = true, .cache = true});
      auto l_tsinfos = c_l.find(shyft_url("stm", "r/.*realised.*"));
      REQUIRE_EQ(l_tsinfos.size(), n_ts + 1); // the plus one is to demo we have full pattern match only
    }

    r_port = remote_bin.start_server();

    if (remote_missing_at_start) {
      remote_bin.clear();                    // stop listener
      remote_bin.set_listening_port(r_port); // but keep the port when restarting
    }
    //
    // Act, doing what we want
    //
    std::string path_match("(r/.*realised)");
    transfer::configuration xfer_cfg{
      .name = "test",
      .what =
        {.search_pattern = "shyft://stm/"
                         + path_match, // notice that it is full pattern match,so realised.xyz is not match
         .replace_pattern = remote_extra.empty()
                            ? ""
                            : "shyft://stm/" + remote_extra + "$1"}, // notice parenthesis above to group the match

      .where = {.host = "localhost", .port = r_port},
      .xfer_period = {.spec = {}}, // means complete time-series (all data, whatever period)
      .read_remote = false,
      .read_updates_cache = true,
      .write_policy = {.recreate = false, .strict = false, .cache = true, .best_effort = true},
      .when =
        {.ta = time_axis::generic_dt{},
               .changed = with_subscription,
               .poll_interval = core::from_seconds(0.02),
               .change_linger_time = core::from_seconds(0.5)},
      .how =
        {
               .retries = 10,
               .sleep_before_retry = core::from_seconds(0.1),
               .partition_size = 13u,
               },
      .protocol_version = latest_version
    };
    auto remote_search_pattern =
      remote_extra.empty() ? xfer_cfg.what.search_pattern : std::string("shyft://stm/") + remote_extra + path_match;
    // reasonable : fmt::print("Test using cfg={}",xfer_cfg);
    // to watch messages: log.dlog.set_level(dlib::LTRACE);
    {
      transfer::engine pt{local, xfer_cfg};
      // auto & log=pt.get_logger();
      // to watch messages:
      // log.dlog.set_level(dlib::LTRACE);
      pt.start();
      MESSAGE("push transfer started");
      // let reader run and search for ts
      while (pt.get_status(false).last_activity < core::utctime_now() - core::from_seconds(10.0))
        std::this_thread::sleep_for(core::from_seconds(0.01));

      if (!ts_exists_before_start) { // if we have not created series to transfer,
        (void) c_l.store_ts(tsv0, store_policy{.recreate = true, .cache = true}); // then do it now as it is missing
      }
      // if the remote is not started, ensure it tells us about it
      if (remote_missing_at_start) {
        while (pt.get_status(false).remote_errors.empty() && pt.get_status(false).write_errors.empty()) {
          std::this_thread::sleep_for(core::from_seconds(0.01));
        }
        // ok, it reported errors
        remote_bin.start_server(); // now start the server, using r_port
      }
      // now the transfer is running active, and it will automagically do a first transfer, then subscribe to changes
      // lets ensure the initial transfer does happen.
      while (pt.get_status(false).total_transferred == 0)
        std::this_thread::sleep_for(core::from_seconds(0.01));
      MESSAGE("push transfer: reading first batch");

      auto cr = client(fmt::format("localhost:{}", r_port));
      auto r_tsinfo_some = cr.find(remote_search_pattern);
      CHECK_GE(r_tsinfo_some.size(), 1);

      if (xfer_cfg.when.changed) {
        auto max_times = 3;
        auto delta = 1 / double(max_times) / 10.0;
        double value = delta;
        auto n_values = 3;
        while (pt.is_running() && --max_times > 0) { // let it boil for a while
          std::this_thread::sleep_for(core::from_seconds(0.1));
          value += delta; // make some noise, store values, subs should update.
          ++n_values;
          last_written = make_test_tsv(n_ts, value, n_values); // increase size of ts
          MESSAGE("push transfer: n_values=" << n_values << ", values:" << value);
          (void) c_l.store_ts(last_written, store_policy{.recreate = false, .cache = true});
        }
      }
      MESSAGE("Stopping");
      std::this_thread::sleep_for(core::from_seconds(0.5)); // let last change get picked up by reader.
      // then wait for empty queue and writer done.
      pt.stop(core::from_seconds(10.0)); //
      MESSAGE("Check results");
      //
      // Assert that we are ok with results/effects.
      //
      REQUIRE_EQ(false, pt.is_running());
      auto status = pt.get_status(true);
      CHECK_EQ(true, status.write_errors.empty());
      CHECK_EQ(true, status.read_errors.empty());
      CHECK_EQ(!remote_missing_at_start, status.remote_errors.empty());
      for (auto const &re : status.read_errors) {
        MESSAGE("Read error:" << re.ts_url << " was :" << int(re.code));
      }
      CHECK_GE(status.total_transferred, 1); // not accurate, because timing might influence everything, we hope for >1
      CHECK_GE(status.read_speed, 0.001);
      CHECK_GE(status.write_speed, 0.001);
      CHECK((status.last_activity >= (core::utctime_now() - core::calendar::HOUR)));
    }
    auto c_r = client(fmt::format("localhost:{}", r_port));

    MESSAGE("Check remote time-series are present");
    auto r_tsinfos = c_r.find(remote_search_pattern);
    REQUIRE_EQ(r_tsinfos.size(), n_ts);

    // even read and verify they are all there:

    MESSAGE("Check remote time-series are equal to last transferred");
    time_series::dd::ats_vector tsids;
    for (auto const &i : last_written) {
      auto ts_url = i.id();
      if (!xfer_cfg.what.replace_pattern.empty()) {
        std::regex re_match(xfer_cfg.what.search_pattern);
        ts_url = std::regex_replace(
          ts_url, re_match, xfer_cfg.what.replace_pattern, std::regex_constants::match_default);
      }
      tsids.push_back(apoint_ts(ts_url));
    }
    auto r_tsv = c_r.evaluate(tsids, r_tsinfos[0].data_period, true, false);
    for (size_t i = 0; i < last_written.size(); ++i) {
      CHECK_EQ(r_tsv[i], last_written[i]);
    }


    c_l.close();
    c_r.close();
    MESSAGE("Leaving test");
  }

  TEST_CASE("transfer/period_spec") {
    core::calendar utc;
    auto h_now = utc.trim(core::utctime_now(), core::calendar::HOUR);
    SUBCASE("hour_now/relative") {
      transfer::configuration::period_spec ps{
        .now_relative = {core::calendar::HOUR},
          .spec = utcperiod{-3600s, +3600s}
      };
      auto p = ps.real(h_now);
      CHECK_EQ(true, p.start == h_now - core::calendar::HOUR);
      CHECK_EQ(true, p.end == h_now + core::calendar::HOUR);
    }
    SUBCASE("full_period_spec") {
      transfer::configuration::period_spec ps{
        .now_relative = {0s},
          .spec = utcperiod{utc.time(2010, 1, 1), utc.time(2010, 1, 1, 1)}
      };
      utcperiod p = ps.real(h_now);

      CHECK_EQ(p, ps.spec);
    }
    SUBCASE("everything") {
      transfer::configuration::period_spec ps;
      utcperiod p = ps.real(core::utctime_now());
      CHECK_EQ(p, utcperiod{});
    }
    SUBCASE("relative-start/open-end") {
      transfer::configuration::period_spec ps{
        .now_relative = {core::calendar::HOUR},
          .spec = utcperiod{-core::calendar::DAY, core::max_utctime}
      };
      utcperiod p = ps.real(h_now);
      CHECK_EQ(p, utcperiod{h_now - core::calendar::DAY, core::max_utctime});
    }
  }

  TEST_SUITE_END();
}
