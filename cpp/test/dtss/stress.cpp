#include <cstdint>
#include <cstdlib>
#include <future>
#include <shared_mutex>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/fx_merge.h>
#include <shyft/time_series/time_axis.h>

#include "test_pch.h"
#include <test/test_utils.h>

namespace shyft {

  using namespace std;
  using namespace shyft;
  using namespace shyft::core;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::gta_t;

  namespace {

    string ts_url(string const &c, string const ts) {
      return "shyft://" + c + "/" + ts;
    }

    string server_addr(string const &host, int port) {
      return host + ":" + to_string(port);
    }

    string mk_ts_url(string const &container, string const &ts_path, int i) {
      return ts_url(container, ts_path + "/" + to_string(i));
    }

    struct config {
      string host{"127.0.0.1"};
      int port{20000};
      utctimespan dt_min_read{seconds(10000l)};
      utctimespan dt_read_var{seconds(1000l)};
      string container{"test"};
      string ts_path{"a"};
      size_t min_ts{10};
      size_t max_ts{100};
      size_t min_n{10};
      size_t max_n{100};
      utctimespan dt{seconds(10l)};
      size_t n_pts_min{1};
      int n_pts_var{10000};
      //--reader parameters
      bool reader_use_cache{false};    // false-> force reads to hit db
      bool reader_update_cache{false}; // true->force maximum cache update concurrency
      //-- writer parameters
      bool writer_cache_on_write{true};            // stash to cache on writes
      bool writer_replace_all{false};              // rewrites ts every time
      bool writer_covered_complete_periods{false}; // let update of ts cover complete period
      chrono::milliseconds writer_sleep{20l};
    };

    struct reader {
      config c{};

      dtss::ts_vector_t create_read_expression() {
        dtss::ts_vector_t r;
        int n = c.min_ts + rand() % (c.max_ts - c.min_ts);
        r.reserve(n);
        int i0 = std::min(c.max_ts - n, rand() % c.max_ts);
        for (size_t i = 0; int(i) < n; ++i)
          r.push_back(apoint_ts(mk_ts_url(c.container, c.ts_path, i0 + i)) * 1000.0 + 42 * double(i));
        return r;
      }

      utcperiod create_read_period(utctime t_now) {
        utctime t_end = t_now - seconds(static_cast<long>(rand() % 1));
        utctime t_start = t_end - (c.dt_min_read + seconds(rand() % to_seconds64(c.dt_read_var)));
        return utcperiod{t_start, t_end};
      }

      size_t do_work(utctime t_exit) {
        auto t_now = utctime_now();
        dtss::client d(server_addr(c.host, c.port));
        size_t rr = 0;
        while (t_now < t_exit) {
          t_now = utctime_now();
          auto rp = create_read_period(t_now);
          int n = c.min_n + rand() % (c.max_n - c.min_n);
          gta_t ta(rp.start, utctimespan{seconds(1l) + rp.timespan() / n}, n);
          auto rexpr = create_read_expression();
          try {
            auto x_update_cache = c.reader_update_cache * (rand() % 100) > 50; // randomize
            // auto r = d.percentiles(
            //   rexpr, rp, ta, vector<int64_t>{0, 10, 50, 75, 100}, c.reader_use_cache, x_update_cache);
            auto r = d.evaluate(rexpr, rp, c.reader_use_cache, x_update_cache);

            rr++;
            this_thread::sleep_for(chrono::milliseconds(10l));
          } catch (runtime_error const &re) {
            throw runtime_error(
              fmt::format(
                "reader: t_now={},at attempt {},rp={},rexpr.size={}, rexpr.first={}, use_cache={}, re.what= {}",
                t_now,
                rr,
                rp,
                rexpr.size(),
                rexpr.front().id(),
                c.reader_use_cache,
                re.what()));
          }
        }
        return rr;
      }
    };

    struct writer {
      size_t volatile n{0u};
      promise<bool> done;
      config c{};
      calendar utc;

      gta_t create_time_axis(utctime t_now) {
        size_t n_steps = c.n_pts_min + c.n_pts_var;
        auto t0 = utc.trim(t_now, c.dt) - n_steps * c.dt;
        return gta_t{t0, c.dt, n_steps};
      }

      gta_t create_random_slice(gta_t const &ta) {
        auto i0 = rand() % (ta.size() - c.n_pts_min);
        auto n_steps = std::min(ta.size() - i0, rand() % ta.size());
        return ta.slice(i0, n_steps);
      }

      dtss::ts_vector_t mk_write_ts(size_t n_ts, gta_t const &ta) {
        dtss::ts_vector_t r;
        for (size_t i = 0u; i < ta.size(); ++i) {
          vector<double> v(ta.size(), double(i));
          apoint_ts ts_v(ta, std::move(v), time_series::POINT_AVERAGE_VALUE);
          r.push_back(apoint_ts(mk_ts_url(c.container, c.ts_path, i), ts_v));
        }
        return r;
      }

      size_t do_work(utctime t_exit) {
        auto t_now = utctime_now();
        dtss::client d(server_addr(c.host, c.port));
        dtss::store_policy policy{.recreate = c.writer_replace_all, .strict = true, .cache = c.writer_cache_on_write};
        gta_t ta = create_time_axis(t_now);
        (void) d.store_ts(mk_write_ts(c.max_ts, ta), policy);
        n = 1;

        fmt::print("Initial write of all time-series done, signal start to readers\n");
        done.set_value(true);

        while (t_now < t_exit) {
          try {
            if (!c.writer_covered_complete_periods) {
              ta = create_time_axis(t_now);
              ta = create_random_slice(ta);
            }
            auto tsw = mk_write_ts(1u + rand() % c.max_ts, ta);
            (void) d.store_ts(tsw, policy);
          } catch (runtime_error const &ex) {
            throw runtime_error(string("writer:") + ex.what());
          }
          n = n + 1;
          cout << ".";
          cout.flush();
          this_thread::sleep_for(c.writer_sleep);
          t_now = utctime_now();
        }
        return n;
      }
    };

    struct single_writer_multi_reader {
      vector<reader> r{};
      writer w{};

      void setup(config const &c, size_t n_readers) {
        w.c = c;
        for (size_t i = 0; i < n_readers; ++i)
          r.push_back(reader{c});
      }

      vector<size_t> run(utctime t_exit) {
        vector<future<size_t>> x;
        //
        // w.do_work(t_exit);
        auto wm = std::async(std::launch::async, [t_exit, this]() -> size_t {
          return w.do_work(t_exit);
        });
        auto ready = w.done.get_future().wait_for(chrono::seconds(55l)) != future_status::timeout;
        if (!ready && w.n < 1)
          throw runtime_error("still missing writer ready");
        std::cout << "Ok, launch readers\n";
        for (size_t i = 0; i < r.size(); ++i) {
          x.push_back(std::async(std::launch::async, [t_exit, this, i]() -> size_t {
            return r[i].do_work(t_exit);
          }));
        }
        std::cout << "wait for readers\n";

        vector<size_t> c;

        for (auto &f : x)
          c.push_back(f.get());
        std::cout << "FINALE WRITER\n";
        c.push_back(wm.get());
        std::cout << "DONE\n";
        std::cout.flush();
        return c;
      }
    };
  }

  TEST_SUITE_BEGIN("dtss");

  namespace {
    void do_dtss_perf_test(std::string db_type, config &c) {
      auto root_dir = "shyft.dtss_stress." + db_type;
      test::utils::temp_dir tmpdir(root_dir.c_str());
      dtss::server_state srv{};
      dtss::add_container(srv, c.container, tmpdir.string(), db_type);
      srv.ts_cache.set_capacity(50 * 1000 * 1000); //* was 50
      dtss::binary_server bin{srv};
      bin.set_listening_ip(c.host);
      // bin.set_listening_port(c.port);
      bin.set_graceful_close_timeout(30); // just 10 ms ?
      c.port = bin.start_server();
      bool rr = bin.is_running();
      REQUIRE_EQ(rr, true);
      vector<size_t> x;
      /* */ {
        single_writer_multi_reader swmr{};
        size_t n_readers = 5;
        long n_seconds = 10; // seems to be sufficient to expose bugs seen so far
        if (getenv("SHYFT_DTSS_STRESS")) {
          std::istringstream params ( getenv("SHYFT_DTSS_STRESS"));
          char delimiter;
          if (!(params >> n_readers>>delimiter >> n_seconds) || delimiter != ',') {
            FAIL("Invalid SHYFT_DTSS_STRESS environment variable");
          };
        }
        MESSAGE("Stress test " << db_type << " w " << n_readers << " readers for " << n_seconds << " seconds");
        swmr.setup(c, n_readers);
        x = swmr.run(utctime_now() + seconds(n_seconds));
        auto sum_read_operations = 0u;
        for (auto i = 0u; i < x.size() && i < n_readers; ++i) {
          sum_read_operations += x[i];
        }
        auto cs = srv.ts_cache.get_cache_stats();
        MESSAGE(
          "Test finished, with " << sum_read_operations << " read operations and " << x.back() << " write operations");
        MESSAGE(
          "Cache stats " << fmt::format(
            "Cached ts {} hits={} misses={} total size={} MB",
            cs.id_count,
            cs.hits,
            cs.misses,
            cs.point_count * 8.0 / 1e6));
      }
      bin.clear();
      CHECK_GT(x.size(), 0);
    }
  }

  TEST_CASE("stress/dtss/basic/ts_db") {
    config c;
    SUBCASE("standard_operation") {
      c.writer_covered_complete_periods = false;
      c.writer_replace_all = false;
    }
    SUBCASE("writer_recreate") {
      c.writer_replace_all = true;
    }
    SUBCASE("writer_cover_period") {
      c.writer_covered_complete_periods = true;
    }
    do_dtss_perf_test("ts_db", c);
  }

  TEST_CASE("stress/dtss/basic/ts_rdb") {
    config c;
    SUBCASE("standard_operation") {
      c.writer_covered_complete_periods = false;
      c.writer_replace_all = false;
    }
    SUBCASE("writer_recreate") {
      c.writer_replace_all = true;
    }
    SUBCASE("writer_cover_period") {
      c.writer_covered_complete_periods = true;
    }
    do_dtss_perf_test("ts_rdb", c);
  }

  TEST_CASE("stress/dtss/connection") {
    config c;
    c.port = 20004;
    test::utils::temp_dir tmpdir("shyft.dtss_conn_stress");
    dtss::server_state srv{};
    add_container(srv, c.container, tmpdir.string());
    dtss::binary_server bin{srv};
    bin.set_listening_ip(c.host);
    // bin.set_listening_port(c.port);
    c.port = bin.start_server();
    bool rr = bin.is_running();
    REQUIRE_EQ(rr, true);
#ifdef _WIN32
    int const n_connects = 300;
#else
    int const n_connects = 100;
#endif
    dtss::client client{server_addr(c.host, c.port), 1000}; // just one client...
    dtss::cache_stats cs;
    for (size_t i = 0; i < n_connects; ++i) { // this will exhaust local ports due to linger/TIME_WAIT sockets, and fail
                                              // the test unless we use smart connect/reuse
      cs = client.get_cache_stats();
    }
    bin.clear();
    bin.set_listening_ip(c.host);
    bin.set_listening_port(c.port);
    bin.start_async();
    bool ok = true;
    try {
      cs = client.get_cache_stats(); // should work! with retry
    } catch (runtime_error const &) {
      ok = false;
    }
    CHECK_EQ(ok, true);
    bin.clear();
  }

  TEST_SUITE_END();

}
