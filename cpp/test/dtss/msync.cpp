#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/ts_subscription.h>

#include "test_pch.h"
#include <test/test_utils.h>

namespace shyft {
  using namespace shyft::dtss;
  using namespace shyft::time_series;
  using std::make_unique;

  namespace test {
    geo::ts_matrix create_forecast(geo::ts_db_config const &cfg, utctime fc_length, gta_t const &ta);
  }

  namespace {
    // to make life easier
    utctime _t(int s) noexcept {
      return shyft::core::from_seconds(s);
    }

    utctime _ms(int s) noexcept {
      return shyft::core::from_seconds(s / 1000.0);
    }

    utcperiod _p(int b, int e) noexcept {
      return utcperiod(_t(b), _t(e));
    }

    /**
     * Arrange the test case with a
     * (1) master dtss, (with shyft://test container setup)
     * (2) master client to work at the master side
     * (3) slave dtss with connect through msync to the master
     * (4) slave client to work from outside to the slave dtss
     */

    struct msync_setup {
      static constexpr auto stair_case = ts_point_fx::POINT_AVERAGE_VALUE;
      string const local_only{"127.0.0.1"};

      test::utils::temp_dir tmpdir{"shyft.msync.store."};

      unique_ptr<server_state> master;
      unique_ptr<binary_server> master_binary;
      int master_port_no;
      unique_ptr<client> mc;

      unique_ptr<server_state> slave;
      unique_ptr<binary_server> slave_binary;
      int slave_port_no;
      unique_ptr<client> sc;


      size_t dstm_count{0};

      msync_setup(::shyft::dtss::version_type version = latest_version) {

        master = make_unique<server_state>();
        add_container(*master, "test", (tmpdir / "test").string());
        add_container(*master, "", tmpdir.string()); // prep for geo
        master->can_remove = true;
        master_binary = make_unique<binary_server>(*master);
        master_binary->set_listening_ip(local_only);
        master_port_no = master_binary->start_server();
        mc = make_unique<client>("localhost:" + std::to_string(master_port_no));

        slave = std::make_unique<server_state>(server_callbacks{[this](id_vector_t const &tsv, utcperiod p) {
          return dstm_resolve(tsv, p);
        }});
        slave->can_remove = true;
        set_master(*slave, "localhost", master_port_no, 0.001, 10, 0.050, version);
        slave_binary = make_unique<binary_server>(*slave);
        slave_binary->set_listening_ip(local_only);
        slave_port_no = slave_binary->start_server();
        sc = make_unique<client>("localhost:" + std::to_string(slave_port_no));
      }

      ///< callback from the slave dtss to resolve dstm:// queries
      ts_vector_t dstm_resolve(id_vector_t const &ts_ids, utcperiod p) {
        ++dstm_count;
        ts_vector_t r;
        for (auto id : ts_ids)
          r.push_back(
            apoint_ts{
              gta_t{p.start, _t(10),   3},
              vector{    2.0,    1.0, 3.0},
              stair_case
          });
        return r;
      };

      ~msync_setup() {
        // controlled tear down in order
        sc.reset();
        mc.reset();
        slave_binary->clear();
        master_binary->clear();
      }
    };

    struct msync_internal : public msync_setup {
      msync_internal()
        : msync_setup(shyft::dtss::internal_version) {
      }
    };
  }

  TEST_SUITE_BEGIN("dtss");

  TEST_CASE("dtss/ts_observer") {
    using shyft::core::subscription::manager;
    using shyft::dtss::subscription::ts_observer;
    auto sm = make_shared<manager>();
    auto o = make_shared<ts_observer>(sm, "subscription_request_id");

    FAST_CHECK_EQ(false, o->recalculate());
    FAST_CHECK_EQ(0u, o->find_changed_ts().size());

    auto p0 = _p(0, 10);
    auto p1 = _p(5, 15);
    auto p2 = _p(0, 15);

    o->subscribe_to(id_vector_t{"a", "b"}, p0);
    o->subscribe_to(id_vector_t{"b", "c"}, p1); // should extend first set, and then create a new  set for c in p1

    FAST_CHECK_EQ(3u, o->terminals.size());
    FAST_CHECK_EQ(3u, sm->active.size()); // ensure the sm also got it
    FAST_CHECK_EQ(0u, o->find_changed_ts().size());

    sm->notify_change(id_vector_t{"a"});
    auto cs1 = o->find_changed_ts();                // there should be only one change
    FAST_CHECK_EQ(0u, o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(1u, cs1.size());
    FAST_CHECK_EQ(1u, cs1[p0].size());      // for this specific period,
    FAST_CHECK_EQ(string("a"), cs1[p0][0]); // and exactly equal to a

    sm->notify_change(id_vector_t{"c"});
    cs1 = o->find_changed_ts();
    FAST_CHECK_EQ(0u, o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(1u, cs1.size());
    FAST_CHECK_EQ(1u, cs1[p1].size());      // for this specific period,
    FAST_CHECK_EQ(string("c"), cs1[p1][0]); // and exactly equal to c

    sm->notify_change(id_vector_t{"a", "c", "b"});
    cs1 = o->find_changed_ts();
    FAST_CHECK_EQ(0u, o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(3u, cs1.size());

    FAST_CHECK_EQ(1u, cs1[p0].size());      // for this specific period,
    FAST_CHECK_EQ(string("a"), cs1[p0][0]); // and exactly equal to c

    FAST_CHECK_EQ(1u, cs1[p1].size());      // for this specific period,
    FAST_CHECK_EQ(string("c"), cs1[p1][0]); // and exactly equal to c

    FAST_CHECK_EQ(1u, cs1[p2].size());      // for this specific period,.. extended!
    FAST_CHECK_EQ(string("b"), cs1[p2][0]); // and exactly equal to b

    o->unsubscribe(id_vector_t{"b"});                      // unsub one
    FAST_CHECK_EQ(1u, sm->total_unsubscribe_count.load()); // ensure we can detect this event

    sm->notify_change(id_vector_t{"a", "c", "b"});
    cs1 = o->find_changed_ts();
    FAST_CHECK_EQ(0u, o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(2u, cs1.size());
    FAST_CHECK_EQ(1u, cs1[p0].size());      // for this specific period,
    FAST_CHECK_EQ(string("a"), cs1[p0][0]); // and exactly equal to c
    FAST_CHECK_EQ(1u, cs1[p1].size());      // for this specific period,
    FAST_CHECK_EQ(string("c"), cs1[p1][0]); // and exactly equal to c

    o->unsubscribe(id_vector_t{"a", "b", "c"}); // unsub all ++
    sm->notify_change(id_vector_t{"a", "c", "b", "d"});
    cs1 = o->find_changed_ts();
    FAST_CHECK_EQ(0u, cs1.size());
    FAST_CHECK_EQ(3u, sm->total_unsubscribe_count.load()); // ensure we can detect unsubs
  }

  TEST_CASE("dtss/ts_sub_item") {
    ts_sub_item a(_p(2, 4), "a");
    FAST_CHECK_EQ(a.p, _p(2, 4));
    FAST_CHECK_EQ(a.id, "a");
    a.extend_period(_p(2, 3));
    FAST_CHECK_EQ(a.p, _p(2, 4)); // the extend was within existing period
    a.extend_period(_p(3, 6));
    FAST_CHECK_EQ(a.p, _p(2, 6)); // the extend increased upper
    a.extend_period(_p(1, 2));
    FAST_CHECK_EQ(a.p, _p(1, 6)); // the extend lower bound
    a.extend_period(_p(0, 7));
    FAST_CHECK_EQ(a.p, _p(0, 7)); // both upper and lower
    a.extend_period(_p(10, 12));
    FAST_CHECK_EQ(a.p, _p(0, 12)); // handling extend with disjoint periods
  }

  TEST_CASE("dtss/ts_sub_map/make_read_partitions") {

    ts_sub_map m;

    std::vector<ts_sub_item> subs{
      {_p(0, 4), "a"},
      {_p(2, 5), "b"},
      {_p(0, 4), "c"}
    };
    for (auto e : subs) {
      auto i = std::make_unique<ts_sub_item>(e.p, e.id);
      m[&i->id] = std::move(i);
    }
    auto rp = make_same_period_partitions(m);
    REQUIRE_EQ(rp.size(), 2);
    CHECK_EQ(rp[subs[0].p].size(), 2u);
    CHECK_EQ(rp[subs[1].p].size(), 1u);
    CHECK_EQ(*rp[subs[1].p][0], subs[1]);
    auto const &rpx = rp[subs[0].p];
    auto fa = std::ranges::find_if(rpx, [&](auto const &e) {
      return e->id == subs[0].id;
    });
    auto fc = std::ranges::find_if(rpx, [&](auto const &e) {
      return e->id == subs[2].id;
    });
    CHECK_EQ(true, fa != std::end(rpx));
    CHECK_EQ(true, fc != std::end(rpx));
  }

  TEST_CASE("dtss/slave_mode_messages") {
    using namespace shyft::dtss;
    using dts_server = server_state;
    using shyft::dtss::ts_db;
    using shyft::time_series::ts_point_fx;
    dts_server s{};

    test::utils::temp_dir default_container("slave");
    string c2("c2"), c3("c3");
    add_container(s, c2, (default_container / c2).string());
    add_container(s, c3, (default_container / c3).string());

    binary_server bin{s};
    bin.set_listening_ip("127.0.0.1");
    auto port = bin.start_server();
    // MESSAGE("Starting dtss server on port: " << port);
    // MESSAGE("Starting dtss client port: " << port);
    client c("127.0.0.1:" + std::to_string(port));

    constexpr auto check = [](auto const &v, auto const &c) {
      return std::any_of(v.begin(), v.end(), [&c](auto const &e) {
        return e == c;
      });
    };

    // MESSAGE("Get container names on server");
    auto names = do_get_container_names(s);
    FAST_CHECK_UNARY(check(names, c2));
    FAST_CHECK_UNARY(check(names, c3));

    // Now test method from client side
    // MESSAGE("Get container names from client request");
    names = c.get_container_names();
    FAST_CHECK_UNARY(check(names, c2));
    FAST_CHECK_UNARY(check(names, c3));

    // Do slave read directly on "master" server
    gta_t ta(utctime{0l}, utctime{10l}, 3);
    apoint_ts c2_a(ta, vector<double>{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    apoint_ts c3_a(ta, vector<double>{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    string c2_name = "shyft://c2/a";
    string c3_name = "shyft://c3/a";
    vector<apoint_ts> a_tsv{apoint_ts(c2_name, c2_a), apoint_ts(c3_name, c3_a)};
    ts_vector_t tsv{a_tsv};
    // MESSAGE("Store tsv to server");
    (void) c.store_ts(tsv, {.recreate = true, .cache = true});
    // MESSAGE("Read tsv to server");
    auto r = c.try_read(id_vector_t{c2_name, c3_name}, ta.total_period(), true, true);
    ts_vector_t expected_r{
      vector<apoint_ts>{c2_a, c2_a}
    };
    auto r_view = std::views::transform(r.result, [&](auto &&gts) {
      return apoint_ts{gts};
    });
    ts_vector_t r_tsv{r_view.begin(), r_view.end()};
    FAST_CHECK_EQ(r_tsv, expected_r);

    // Subscribe
    // MESSAGE("Client read subscription");
    auto [sub_r, sub_r_tp] = c.read_subscription();
    FAST_CHECK_UNARY(sub_r.size() == 0u); // ensure that if there are no changes, we get zero back

    c2_a.set(0, 3);
    ts_vector_t tsv_2{vector<apoint_ts>{apoint_ts(c2_name, c2_a)}};
    (void) c.store_ts(
      tsv_2,
      {.recreate = false, .cache = true}); // now store something, that will change the stuff we are subscribing to.
    auto rr = c.try_read(
      id_vector_t{c3_name}, ta.total_period(), true, true); // read c3, but get c2 changes as piggyback!
    sub_r = rr.updates;                                     // these are the updates piggybacked to the read response
    FAST_CHECK_EQ(sub_r.size(), 1u);
    FAST_CHECK_EQ(sub_r[0].id, c2_name);
    FAST_CHECK_EQ(apoint_ts{sub_r[0].rep}, c2_a);
    FAST_REQUIRE_EQ(rr.result.size(), 1u); // just ensure we also got the stuff we tried to read.
    FAST_CHECK_EQ(apoint_ts{rr.result[0]}, c3_a);

    std::tie(sub_r, sub_r_tp) = c.read_subscription();
    FAST_CHECK_EQ(sub_r.size(), 0u);


    c2_a.set(0, 4);
    (void) c.store_ts(ts_vector_t{vector<apoint_ts>{apoint_ts(c2_name, c2_a)}}, {.recreate = false, .cache = true});
    std::tie(sub_r, sub_r_tp) = c.read_subscription();
    FAST_CHECK_UNARY(sub_r.size() == 1);
    // MESSAGE("Client unsubscribe");
    c.unsubscribe(vector<string>{c2_name});
    // MESSAGE("Before set");
    c2_a.set(0, 5);
    // MESSAGE("Client change and store ts");
    (void) c.store_ts(ts_vector_t{vector<apoint_ts>{apoint_ts(c2_name, c2_a)}}, {.recreate = false, .cache = true});
    // MESSAGE("Client read");
    std::tie(sub_r, sub_r_tp) = c.read_subscription();
    FAST_CHECK_UNARY(sub_r.size() == 0);

    // MESSAGE("Closing client and server");
    c.close();
    bin.clear();
  }

  TEST_CASE_FIXTURE(msync_setup, "dtss/master_slave_ops") {
    using namespace shyft::time_series;

    string fpattern{"shyft://test/a.*"};
    FAST_CHECK_EQ(0u, sc->find(fpattern).size()); // find should be redirected to master, and return 0

    // store some time_series so that we have something to work on.
    gta_t ta1(_t(0), _t(10), 3);
    apoint_ts a1("shyft://test/a1");
    apoint_ts a2("shyft://test/a2");
    apoint_ts a1_f1(ta1, vector<double>{1.0, 2.0, 3.0}, stair_case);
    apoint_ts a2_f1(ta1, vector<double>{4.0, 4.0, 4.0}, stair_case);

    ts_vector_t tsv1;
    tsv1.push_back(apoint_ts(a1.id(), a1_f1));
    tsv1.push_back(apoint_ts(a2.id(), a2_f1));
    (void) sc->store_ts(tsv1, {.recreate = true, .cache = true}); // overwrite, and cache it
    FAST_CHECK_EQ(2u, sc->find(fpattern).size());                 // find should be redirected to master, and return 2

    // now read back the timeseries through the slave dtss
    ts_vector_t e1;
    e1.push_back(a1);
    e1.push_back(a2);
    ts_vector_t r1 = sc->evaluate(
      e1,
      ta1.total_period(),
      true,
      true,
      ta1.total_period()); // read and cache it! (note: both at master and slave dtss)
    FAST_REQUIRE_EQ(2, r1.size());
    FAST_CHECK_EQ(r1[0], a1_f1);
    FAST_CHECK_EQ(r1[1], a2_f1);

    FAST_CHECK_EQ(2u, slave->msync->subs.size()); // we should have two subscriptions by now

    // verify get_ts_info
    auto tsi = sc->get_ts_info(a1.id());
    FAST_CHECK_EQ(tsi.name, "a1");

    // make changes for a1 at the server (without slave knowing about it!), and prove it propagates to the slave dtss
    ts_vector_t tsv2 = vector<apoint_ts>{apoint_ts(a1.id(), a2_f1)};
    (void) mc->store_ts(tsv2, {.recreate = false, .cache = true}); // no overwrite, and cache it on the master server
    auto cs0 = sc->get_cache_stats();                              // so that we can detect that we are hitting locally

    // wait for a while, to let the msync worker update it
    std::this_thread::sleep_for(_ms(30)); // wait .. should be enough
    ts_vector_t r2 = sc->evaluate(
      e1,
      ta1.total_period(),
      true,
      true,
      ta1.total_period());             // read and cache it! (note: both at master and slave dtss)
    auto cs1 = sc->get_cache_stats();  // get cache stats after, we expect more hits.
    FAST_CHECK_GE(cs1.hits, cs0.hits); //
    FAST_REQUIRE_EQ(2, r2.size());
    FAST_CHECK_EQ(r2[0], a2_f1); // they should both be equal now, ref. storage.
    FAST_CHECK_EQ(r2[1], a2_f1);
    // flush slave cache, and verify that msync worker reduces the subs to minimum
    auto obs = make_shared<dtss::subscription::ts_observer>(slave->sm, "xtra"); //
    obs->subscribe_to(id_vector_t{a2.id()}, _p(0, 10)); // now we simulate that there is a sub for the a2.
    slave->ts_cache.flush();               // get rid of the cache, this should remove all need for a2 locally
    std::this_thread::sleep_for(_ms(300)); // wait more than minium defer time
    FAST_CHECK_EQ(1u, slave->msync->subs.size());
    obs->unsubscribe(id_vector_t{a2.id()});
    FAST_CHECK_EQ(0u, obs->terminals.size());
    FAST_CHECK_EQ(0u, obs->size());
    slave->ts_cache.flush();                      // a1 could be in cache
    std::this_thread::sleep_for(_ms(300));        // wait more than minium defer time
    FAST_CHECK_EQ(0u, slave->msync->subs.size()); // now there should be no need for timeseries
    obs.reset();                                  // causes release of resources/subs
    // verify we can merge store
    sc->merge_store_ts(ts_vector_t{vector<apoint_ts>{apoint_ts{a2.id(), a1_f1}}}, true);
    // verify we can remove stuff
    sc->remove(a1.id());
    FAST_CHECK_EQ(1u, sc->find(fpattern).size()); // find should be redirected to master, and return 1

    // add container through the slave client:
    sc->set_container("some_container", "some_path");
    // not added to the slave:
    FAST_CHECK_EQ(slave->container.size(), 0);
    // added to the master:
    FAST_CHECK_EQ(master->container.size(), 3);
    apoint_ts asome("shyft://some_container/some_ts");
    apoint_ts asome_f1(ta1, vector<double>{1.0, 2.0, 3.0}, stair_case);
    ts_vector_t tsvsome;
    tsvsome.push_back(apoint_ts(asome.id(), asome_f1));
    (void) sc->store_ts(tsvsome, {.recreate = true, .cache = true}); // overwrite, and cache it
    FAST_CHECK_EQ(
      1u, sc->find("shyft://some_container/some_ts").size()); // find should be redirected to master, and return 1
    (void) sc->try_read(id_vector_t{"shyft://some_container/some_ts"}, ta1.total_period(), true, true);
    // should have one sub:
    FAST_CHECK_EQ(1u, slave->msync->subs.size()); // we should have one subscriptions by now

    // active sub, throws:
    CHECK_THROWS_AS(sc->remove_container("shyft://some_container/", true), runtime_error const &);

    // unsub and flush
    sc->unsubscribe(id_vector_t{"shyft://some_container/some_ts"});
    slave->ts_cache.flush();                      // delete cache
    std::this_thread::sleep_for(_ms(300));        // wait more than minium defer time
    FAST_CHECK_EQ(0u, slave->msync->subs.size()); // we should have no subscriptions by now
    sc->remove_container("shyft://some_container/", true);
    // removed from master:
    FAST_CHECK_EQ(master->container.size(), 2);
  }

  TEST_CASE_FIXTURE(msync_setup, "dtss/master_slave/rm_ts_master") {
    using namespace shyft::time_series;

    string fpattern{"shyft://test/a.*"};
    FAST_CHECK_EQ(0u, sc->find(fpattern).size()); // find should be redirected to master, and return 0

    // store some time_series so that we have something to work on.
    gta_t ta1(_t(0), _t(10), 3);
    apoint_ts a1("shyft://test/a1");
    apoint_ts a2("shyft://test/a2");
    apoint_ts a1_f1(ta1, vector<double>{1.0, 2.0, 3.0}, stair_case);
    apoint_ts a2_f1(ta1, vector<double>{4.0, 4.0, 4.0}, stair_case);

    ts_vector_t tsv1;
    tsv1.push_back(apoint_ts(a1.id(), a1_f1));
    tsv1.push_back(apoint_ts(a2.id(), a2_f1));
    (void) sc->store_ts(tsv1, {.recreate = true, .cache = true}); // overwrite, and cache it
    FAST_CHECK_EQ(2u, sc->find(fpattern).size());                 // find should be redirected to master, and return 2

    // now read back the timeseries through the slave dtss
    ts_vector_t e1;
    e1.push_back(a1);
    e1.push_back(a2);
    ts_vector_t r1 = sc->evaluate(
      e1,
      ta1.total_period(),
      true,
      true,
      ta1.total_period()); // read and cache it! (note: both at master and slave dtss)
    FAST_REQUIRE_EQ(2, r1.size());
    FAST_CHECK_EQ(r1[0], a1_f1);
    FAST_CHECK_EQ(r1[1], a2_f1);

    FAST_CHECK_EQ(2u, slave->msync->subs.size()); // we should have two subscriptions by now

    mc->remove(a2.id()); // remove a ts on the master side, that we do have in client cache, and subs.

    // make changes for a1 at the server (without slave knowing about it!), and prove it propagates to the slave dtss
    ts_vector_t tsv2 = vector<apoint_ts>{apoint_ts(a1.id(), a2_f1)};
    (void) mc->store_ts(tsv2, {.recreate = false, .cache = true}); // no overwrite, and cache it on the master server
    auto cs0 = sc->get_cache_stats();                              // so that we can detect that we are hitting locally

    // wait for a while, to let the msync worker update it
    std::this_thread::sleep_for(_ms(30)); // wait .. should be enough
    ts_vector_t e2;
    e2.push_back(a1);
    ts_vector_t r2 = sc->evaluate(
      e2,
      ta1.total_period(),
      true,
      true,
      ta1.total_period());            // read and cache it! (note: both at master and slave dtss)
    auto cs1 = sc->get_cache_stats(); // get cache stats after, we expect more hits.
    // trigger a repair subs?
    // will fail, and leave subs unchanged: slave->msync->repair_subs();
    FAST_CHECK_EQ(2u, slave->msync->subs.size()); // currently two subs
    master->sm->notify_change(a2.id());           // fake a notify on master, will trigger msync fetc.
    // important: now msync should have cleaned out the stuff we removed at master
    std::this_thread::sleep_for(_ms(300)); // wait .. should be enough
    FAST_CHECK_EQ(1u, slave->msync->subs.size());

    FAST_CHECK_GE(cs1.hits, cs0.hits); //
    FAST_REQUIRE_EQ(1, r2.size());
    FAST_CHECK_EQ(r2[0], a2_f1); // they should both be equal now, ref. storage.
    // flush slave cache, and verify that msync worker reduces the subs to minimum
    auto obs = make_shared<dtss::subscription::ts_observer>(slave->sm, "xtra"); //
    obs->subscribe_to(id_vector_t{a1.id()}, _p(0, 10)); // now we simulate that there is a sub for the a1.
    slave->ts_cache.flush();               // get rid of the cache, this should remove all need for a1 locally
    std::this_thread::sleep_for(_ms(300)); // wait more than minium defer time
    FAST_CHECK_EQ(1u, slave->msync->subs.size());
    // due to msync the slave cache is also updated.
    FAST_CHECK_EQ(1u, master->ts_cache.get_cache_stats().id_count);
    obs->unsubscribe(id_vector_t{a1.id()});
    FAST_CHECK_EQ(0u, obs->terminals.size());
    FAST_CHECK_EQ(0u, obs->size());
    slave->ts_cache.flush();                      // a1 could be in cache
    std::this_thread::sleep_for(_ms(300));        // wait more than minium defer time
    FAST_CHECK_EQ(0u, slave->msync->subs.size()); // now there should be no need for timeseries
    obs.reset();                                  // causes release of resources/subs
  }

  TEST_CASE_FIXTURE(msync_setup, "dtss/slave_read") {
    shyft::time_axis::generic_dt ta(_t(0), _t(10), 3);
    auto get_ts_with_val = [&](double val) {
      return apoint_ts(ta, std::vector<double>{val, val, val}, stair_case);
    };
    std::string cached_on_master{"shyft://test/cached_on_master"};
    std::string cached_on_slave{"shyft://test/cached_on_slave"};
    std::string missing_shyft("shyft://test/missing_ts");
    std::string missing_external("external://test/missing_ext");

    std::unordered_map<std::string, std::optional<double>> values;
    values[cached_on_master] = 1.0;
    values[cached_on_slave] = 2.0;
    values[missing_shyft] = std::nullopt;
    values[missing_external] = std::nullopt;

    master->callbacks.bind_ts = [&]([[maybe_unused]] auto ids, [[maybe_unused]] auto period) -> std::vector<apoint_ts> {
      throw std::runtime_error("something");
    };
    // store on the correct places:
    ts_vector_t tsv{apoint_ts(cached_on_master, get_ts_with_val(*values[cached_on_master]))};
    (void) mc->store_ts(tsv, {.recreate = true, .cache = true}); // cache on master
    tsv.clear();
    tsv.push_back(apoint_ts(cached_on_slave, get_ts_with_val(*values[cached_on_slave])));
    (void) sc->store_ts(tsv, {.recreate = true, .cache = true}); // cache on slave

    auto check_apoint = [&](std::string name, ts_frag ts) {
      auto val = values[name];
      if (val) {
        FAST_REQUIRE_NE(ts, nullptr);
        // should be close to the value expected
        FAST_REQUIRE_LT(ts->values()[0] - *val, 0.01);
      } else {
        FAST_REQUIRE_EQ(ts, nullptr);
      }
    };
    auto check_err_has_index = [](auto errors_range, std::size_t index) {
      FAST_REQUIRE_UNARY(std::ranges::any_of(errors_range, [&](auto &err) {
        return err.index == index;
      }));
    };

    std::vector<std::string> rr;
    rr.push_back(cached_on_master);
    auto [rtsv, rtsv_tp, rtsv_err] = try_read(*slave, rr, ta.total_period(), false, false);
    auto try_res = try_read(*slave, rr, ta.total_period(), false, false);
    auto &[result, result_tp, errs] = try_res;
    FAST_REQUIRE_EQ(rtsv.size(), 1u);
    check_apoint(cached_on_master, rtsv[0]);
    FAST_REQUIRE_EQ(result.size(), 1u);
    check_apoint(cached_on_master, result[0]);
    FAST_REQUIRE_EQ(errs.size(), 0u);
    // read from slave with one failing:
    rr.push_back(missing_external);
    rr.push_back(cached_on_slave);
    rr.push_back(missing_shyft);
    // read, using cache on slave:
    try_res = try_read(*slave, rr, ta.total_period(), true, false);
    FAST_REQUIRE_EQ(result.size(), 4u); // 2 present, two missing
    check_apoint(cached_on_master, result[0]);
    check_apoint(missing_external, result[1]);
    check_apoint(cached_on_slave, result[2]);
    check_apoint(missing_shyft, result[3]);
    FAST_REQUIRE_EQ(errs.size(), 2u);
    check_err_has_index(errs, 1u);
    check_err_has_index(errs, 3u);
    // remove ts on master, to stress that subs and cache m
  }

  TEST_CASE_FIXTURE(msync_setup, "dtss/master_slave_cache_semi_hit") {
    shyft::time_axis::generic_dt ta(_t(0), _t(1), 10);
    apoint_ts underlying_series{
      ta, std::vector{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0},
       stair_case
    };
    std::vector urls{std::string{"shyft://test/time_series"}};
    // store on the correct places:
    ts_vector_t tsv{
      apoint_ts{urls[0], underlying_series}
    };
    (void) mc->store_ts(tsv, {.recreate = true, .cache = false}); // no cache on master
    REQUIRE_EQ(mc->get_cache_stats().fragment_count, 0);
    REQUIRE_EQ(sc->get_cache_stats().fragment_count, 0);
    // read period is longer then the underlying series and clips beginning:
    shyft::core::utcperiod readp{_t(1), _t(11)};
    auto res = std::get<0>(try_read(*slave, urls, readp, true, true))[0];
    REQUIRE(std::ranges::equal(res->values(), std::views::drop(underlying_series.values(), 1)));
    // cached only on slave
    REQUIRE_EQ(mc->get_cache_stats().fragment_count, 0);
    REQUIRE_EQ(sc->get_cache_stats().hits, 0);
    REQUIRE_EQ(sc->get_cache_stats().fragment_count, 1);
    REQUIRE_EQ(sc->get_cache_stats().misses, 1);
    REQUIRE_EQ(sc->get_cache_stats().coverage_misses, 0);
    // read again, get hit:
    auto res_2 = std::get<0>(try_read(*slave, urls, readp, true, true))[0];
    REQUIRE(std::ranges::equal(res_2->values(), std::views::drop(underlying_series.values(), 1)));
    REQUIRE_EQ(sc->get_cache_stats().hits, 1);
    REQUIRE_EQ(sc->get_cache_stats().fragment_count, 1);
    REQUIRE_EQ(sc->get_cache_stats().misses, 1);
    REQUIRE_EQ(sc->get_cache_stats().coverage_misses, 0);
    // updating on master creates update in slave with cache update:
    shyft::time_axis::generic_dt ta_2(_t(0), _t(1), 11);
    apoint_ts underlying_series_2{
      ta_2, std::vector{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0},
       stair_case
    };
    ts_vector_t tsv_2{
      apoint_ts{urls[0], underlying_series_2}
    };
    (void) mc->store_ts(tsv_2, {.recreate = false, .cache = false});
    // allow for sync
    std::this_thread::sleep_for(_ms(30));
    auto res_3 = std::get<0>(try_read(*slave, urls, readp, true, true))[0];
    REQUIRE(std::ranges::equal(res_3->values(), std::views::drop(underlying_series_2.values(), 1)));
    // from cache:
    REQUIRE_EQ(sc->get_cache_stats().hits, 2);
    REQUIRE_EQ(sc->get_cache_stats().misses, 1);
    REQUIRE_EQ(sc->get_cache_stats().coverage_misses, 0);
  }

  TEST_CASE_FIXTURE(msync_setup, "dtss/master_slave/issue_1233") {
    /*
     * From jeh team:
     * time-series that caused the issue was a break point ts.
     * initial condition:
     *  cache ok, subs ok with a
     *  - read-period [t3 ..t4) that gives a surrounding read, that is cached
     *  - reply period [t1 ... t5), total period [t0 .. t7)
     * event at server:
     *  - new frag write [t1,t2 ..t2) notify change done
     *  dstm/slave
     *   - subs update read [ t3 .. t4)
     *   - reply frag [t2 .. t5 )
     *   - cache update on dstm/slave will now have the old t1/value in cache.
     *
     */
    shyft::time_axis::generic_dt ta({_t(2), _t(4), _t(11)});
    apoint_ts a_ts{
      ta, std::vector{2.0, 4.0},
       stair_case
    };
    std::vector urls{std::string{"shyft://test/a"}};
    // store on the correct places:
    ts_vector_t tsv{
      apoint_ts{urls[0], a_ts}
    };
    (void) sc->store_ts(tsv, {.recreate = true, .cache = true}); // save from slave, and cache it.
    REQUIRE_EQ(mc->get_cache_stats().fragment_count, 1);
    REQUIRE_EQ(sc->get_cache_stats().fragment_count, 0); // store via slave never caches (or subscribes..)
    // read period is longer then the underlying series and clips beginning:
    shyft::core::utcperiod readp{_t(3), _t(7)};
    auto res = std::get<0>(try_read(*slave, urls, readp, true, true))[0]; // will use client cache, if avail
    REQUIRE(std::ranges::equal(res->values(), a_ts.values()));            // surrounding read give all, inclusive t(2)
    // cached only on slave
    REQUIRE_EQ(mc->get_cache_stats().fragment_count, 1);
    REQUIRE_EQ(sc->get_cache_stats().hits, 0);
    REQUIRE_EQ(sc->get_cache_stats().fragment_count, 1);
    REQUIRE_EQ(sc->get_cache_stats().misses, 1);
    REQUIRE_EQ(sc->get_cache_stats().coverage_misses, 0);
    // read again, get hit:
    auto res_2 = std::get<0>(try_read(*slave, urls, readp, true, true))[0];
    REQUIRE(std::ranges::equal(res_2->values(), a_ts.values()));
    REQUIRE_EQ(sc->get_cache_stats().hits, 1);
    REQUIRE_EQ(sc->get_cache_stats().fragment_count, 1);
    REQUIRE_EQ(sc->get_cache_stats().misses, 1);
    REQUIRE_EQ(sc->get_cache_stats().coverage_misses, 0);
    // updating on master creates update in slave with cache update:
    shyft::time_axis::generic_dt ta_patch({_t(2), _t(3), _t(4), _t(5)});
    apoint_ts a_patch{
      // t2     t3    t4
      ta_patch,
      std::vector{-2.0, -3.0, -4.0},
      stair_case
    };
    ts_vector_t tsv_2{
      apoint_ts{urls[0], a_patch}
    };
    (void) mc->store_ts(tsv_2, {.recreate = false, .cache = true});
    // use dtss cache make_merge, just to illustrate how its applied in the cache.
    auto a_merged = apoint_ts{dtss::make_merged(
      std::dynamic_pointer_cast<gpoint_ts const>(a_patch.ts), std::dynamic_pointer_cast<gpoint_ts const>(a_ts.ts))};
    // allow for sync
    std::this_thread::sleep_for(_ms(30));
    auto res_3 = std::get<0>(try_read(*slave, urls, readp, true, true))[0];
    for (auto i = 0u; i < res_3->size(); ++i) {
      // fails on t(2) != -2 (e.g. the cache on slave is stale on the old value in this case
      CHECK_EQ(res_3->value(i), doctest::Approx(a_merged(res_3->time(i))));
    }
    // from cache:
    REQUIRE_EQ(sc->get_cache_stats().hits, 2);
    REQUIRE_EQ(sc->get_cache_stats().misses, 1);
    REQUIRE_EQ(sc->get_cache_stats().coverage_misses, 0);
  }

  TEST_CASE_FIXTURE(msync_internal, "dtss/master_slave_geo_ops") {
    using namespace shyft::time_series;

    string fpattern{"shyft://test/a.*"};
    string prefix = "shyft://";
    string tc{"geo"}; // important in our case, since we are using this as prefix for all geo stuff.
    geo::grid_spec gs{
      32633, {{0, 0, 0}, {1000, 0, 1}, {1000, 1000, 2}, {0, 1000, 3}}
    };       // 4 points, a grid.
    vector<utctime> t0_times{};                                               // initially empty
    vector<string> variables{string("temperature"), string("precipitation")}; // two variables
    utctime fc_dt = from_seconds(2 * 24 * 3600);                              // forecast length 2x 24hours
    utctime max_fc_dt = fc_dt;
    int64_t n_ens = 2;
    geo::ts_db_config_ gdb = make_shared<geo::ts_db_config>(
      prefix, tc, "a test geo container using shyft native time-series", gs, t0_times, max_fc_dt, n_ens, variables);
    // MESSAGE("add geo ts db");
    sc->add_geo_ts_db(gdb);

    auto t0_dt = from_seconds(24 * 3600); // new forecast everty 24 hour
    gta_t t0_ta{from_seconds(0), t0_dt, 3};
    auto tsm1 = test::create_forecast(*gdb, fc_dt, t0_ta);


    // MESSAGE("store to geo ts db");

    sc->geo_store(gdb->name, tsm1, true, false); // replace,and no cache
    auto p = gs.points[2];
    auto d = 1;
    geo::query g_q1{
      gs.epsg, vector<geo_point>{{p.x - d, p.y - d, p.z}, {p.x + d, p.y - d, p.z}, {p.x, p.y + d, p.z}}
    };
    gta_t q1_ta{t0_ta.time(1), t0_dt, 1}; // pick the second forecast
    geo::eval_args q1{
      tc,
      vector<string>{variables[1]},
      vector<int64_t>{1},
      q1_ta,
      utctime{0l},
      g_q1,
      false,
      utctime{0l}}; // pick precipitation,second geo-point, second ens, and second time.

    // MESSAGE("evaluate on geo ts db");

    auto r1 = sc->geo_evaluate(q1, false, false); // read query 1.
                                                  // MESSAGE("r1.shape,
    // n_t0="<<r1.shape.n_t0<<",n_v="<<r1.shape.n_v<<",n_e="<<r1.shape.n_e<<",n_g="<<r1.shape.n_g<<"\n");
    auto xdb = sc->get_geo_ts_db_info()[0];
    auto s1 = xdb->compute(q1); // compute the slice of the query
    // MESSAGE("s1., t="<<s1.t.size()<<",n_v="<<s1.v.size()<<",n_e="<<s1.e.size()<<",n_g="<<s1.g.size()<<"\n");
    FAST_CHECK_EQ(r1.shape, geo::detail::ix_calc{1, 1, 1, 1}); // verify we got exactly the right dim back.
    FAST_CHECK_EQ(r1.tsv.size(), 1u);
    FAST_CHECK_EQ(r1.tsv[0].mid_point, p); // should hit exactly this point.
    auto s1_t0_ix = t0_ta.index_of(s1.t[0]);
    auto q1_ts = tsm1.ts(
      s1_t0_ix, s1.v[0], s1.e[0], s1.g[0]); // use the computed slice to get out the ts from the source
    FAST_CHECK_EQ(q1_ts.time_axis() == r1.tsv[0].ts.time_axis(), true);
    FAST_CHECK_EQ(q1_ts == r1.tsv[0].ts, true);
  }

  TEST_CASE_FIXTURE(msync_setup, "dtss/master_slave_repair_connections" * doctest::skip(true) * doctest::timeout(4.0)) {
    // TODO: this one need to be fixed to prove we can repair connections.
    using namespace shyft::time_series;
    string fpattern{"shyft://test/a.*"};

    FAST_CHECK_EQ(0u, sc->find(fpattern).size()); // find should be redirected to master, and return 0

    // store some time_series so that we have something to work on.
    gta_t ta1(_t(0), _t(10), 3);
    apoint_ts a1("shyft://test/a1");
    apoint_ts a2("shyft://test/a2");
    apoint_ts a1_f1(ta1, vector<double>{1.0, 2.0, 3.0}, stair_case);
    apoint_ts a2_f1(ta1, vector<double>{4.0, 4.0, 4.0}, stair_case);

    ts_vector_t tsv1;
    tsv1.push_back(apoint_ts(a1.id(), a1_f1));
    tsv1.push_back(apoint_ts(a2.id(), a2_f1));
    (void) sc->store_ts(tsv1, {.recreate = true, .cache = true}); // overwrite, and cache it
    FAST_CHECK_EQ(2u, sc->find(fpattern).size());                 // find should be redirected to master, and return 2
    ts_vector_t e1;
    e1.push_back(a1);
    e1.push_back(a2);
    ts_vector_t r1 = sc->evaluate(
      e1,
      ta1.total_period(),
      true,
      true,
      ta1.total_period()); // read and cache it! (note: both at master and slave dtss)
    FAST_REQUIRE_EQ(2, r1.size());
    FAST_CHECK_EQ(r1[0], a1_f1);
    FAST_CHECK_EQ(r1[1], a2_f1);

    FAST_CHECK_EQ(2u, slave->msync->subs.size()); // we should have two subscriptions by now
    //-- now break the connection to the master,
    //// forcing the master to stop
    MESSAGE("stop dtss.master");
    master->terminate = true;
    // master->set_graceful_close_timeout(2);
    clear_master(*master);                 // now it stops communciation
    master_binary->clear();                //
    std::this_thread::sleep_for(_ms(500)); // wait more than minium defer time
    master_binary->set_listening_ip(local_only);
    master_binary->set_listening_port(master_port_no);
    master->terminate = false;
    MESSAGE("restart dtss.master@" << master_port_no << " is running " << master_binary->is_running());
    auto unsafe_start = std::async(std::launch::async, [&]() {
      master_binary->start_server();
    }); /// master->start_server();
    if (unsafe_start.wait_for(_ms(15000)) == std::future_status::ready) {
      std::this_thread::sleep_for(_ms(500)); // wait more than minium defer time
      MESSAGE("dtss.master, test it's reconnected..");
      if (master->alive_connections != 1) { // bail out, avoid hang.
        MESSAGE("test hickup, bail out");
        sc->close();
        // mc->clear();
        master_binary->clear();
      }
      FAST_REQUIRE_EQ(
        (std::size_t) 1,
        (std::size_t) master->alive_connections); // Note: Casting from atomic_size_t to size_t avoids error "undefined
                                                  // reference to symbol quadmath_snprintf" without having to link with
                                                  // quadmath (it is related to Boost.Math and its Float128 feature) on
                                                  // Arch Linux with GCC 12.2 and Boost 1.80 (not sure what triggers it)
      tsv1.clear();
      tsv1.push_back(apoint_ts(a1.id(), a2_f1));
      tsv1.push_back(apoint_ts(a2.id(), a1_f1));                     // flip content
      (void) mc->store_ts(tsv1, {.recreate = false, .cache = true}); // update, and cache it, at the master..
      std::this_thread::sleep_for(_ms(100)); // wait more than minium defer time, allow poll master to run
      // then read
      r1 = sc->evaluate(
        e1,
        ta1.total_period(),
        true,
        true,
        ta1.total_period()); // read and cache it! (note: both at master and slave dtss)
      FAST_REQUIRE_EQ(3, r1.size());
      FAST_CHECK_EQ(r1[0], a2_f1);
      FAST_CHECK_EQ(r1[1], a1_f1);
    } else {
      MESSAGE("dtss failed to start due to some issues related to dlib and ports available");
    }
  }

  TEST_SUITE_END();

}
