#include <sstream>
#include <string_view>

#include <shyft/core/boost_serialization_std_string_view.h>
#include <shyft/core/core_archive.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/srv/model_info.h>

#include <doctest/doctest.h>

namespace shyft::core {
  // externals for testing only


  TEST_SUITE_BEGIN("serialization");

  TEST_CASE("serialization/string_view_into_string") {
    std::ostringstream xmls(std::ios::binary);
    core_oarchive oa(xmls, core::arch_info_flags);
    std::string_view s{"some string_view that contains a full string with null term"};
    // non null term
    auto s2 = s.substr(0, 4);
    oa << s << s2;
    xmls.flush();
    auto ss = xmls.str();
    std::istringstream xmli(ss, std::ios::binary);
    core_iarchive ia(xmli, core::arch_info_flags);
    std::string s_des, s2_des;
    ia >> s_des >> s2_des;
    CHECK(s == s_des);
    CHECK(s2 == s2_des);
  }

  TEST_CASE("serialization/archive/header/primitives") {
    /* verify archive_info it self */
    {
      archive_info ai;
      CHECK_EQ(true, ai.is_valid());
      CHECK_EQ(false, ai.is_same_as_current_version());
      ai.set_shyft_version(_version);
      ai.boost_version = BOOST_VERSION;
      CHECK_EQ(true, ai.is_same_as_current_version());
    }
    /* verify we can peek out magic with no mods to stream */
    {
      std::string x1_base{"SHYFT"};
      for (auto i = 0; i < 5; ++i) {
        std::string x1 = x1_base.substr(0, i);
        std::istringstream x1i(x1, std::ios::binary);
        CHECK_EQ(0, detail::read_magic(x1i));
        std::string x1_des;
        x1i >> x1_des;
        CHECK_EQ(x1, x1_des);
      }
    }
    /* verify we can peek out magic */
    {
      std::string x1{"SHFTabcd"};
      std::istringstream x1i(x1, std::ios::binary);
      CHECK_EQ(archive_info::magic_value, detail::read_magic(x1i));
      std::string x1_tail;
      x1i >> x1_tail;
      CHECK_EQ("abcd", x1_tail);
    }
    /* verify we can roundtrip archive_info */
    {
      archive_info ai;
      ai.set_shyft_version(_version);
      ai.boost_version = BOOST_VERSION;
      std::ostringstream x1o(std::ios::binary);
      detail::save_archive_info(x1o, ai);
      CHECK_EQ(x1o.str().size(), 4 * 4); // 16 bytes exactly in stream
      std::istringstream x1i(x1o.str(), std::ios::binary);
      archive_info ai_des;
      detail::load_archive_info(x1i, ai_des);
      CHECK_EQ(ai.boost_version, ai_des.boost_version);
      CHECK_EQ(ai.shyft_version, ai_des.shyft_version);
      CHECK_EQ(ai.magic, ai_des.magic);
      CHECK_EQ(ai.optionals, ai_des.optionals);
    }
    /* verify throw if read valid magic, but lacks remainder */
    {
      std::string x1{"SHFT123412341234"};
      for (auto i = 4; i < 16; ++i) {
        std::istringstream x1i(x1.substr(0, i), std::ios::binary);
        archive_info ai;
        CHECK_THROWS_AS(detail::load_archive_info(x1i, ai), boost::archive::archive_exception);
      }
    }
  }

  TEST_CASE("serialization/archive/header") {
    std::ostringstream xmls(std::ios::binary);
    std::string s{"info"};
    bool use_arch_info_flags = true;
    SUBCASE("with-header") {
      core_oarchive oa(xmls, arch_info_flags);
      oa << s;
      xmls.flush();
    }
    SUBCASE("with-no-header") {
      core_oarchive oa(xmls, core_arch_flags);
      oa << s;
      xmls.flush();
      use_arch_info_flags = false;
    }
    auto ss = xmls.str();
    std::istringstream xmli(ss, std::ios::binary);
    core_iarchive ia(xmli, arch_info_flags);
    std::string s_des;
    ia >> s_des;
    CHECK(use_arch_info_flags == ia.archive_info.is_valid());
    if (use_arch_info_flags)
      CHECK_EQ(true, ia.archive_info.is_same_as_current_version());
    CHECK_EQ(s, s_des);
  }

  TEST_CASE("serialization/archive/can-read-old-archive") {
    auto m1 = srv::model_info{};
    m1.name = "m1";
    m1.id = 1;
    m1.created = utctime_now();
    m1.json = "{\"a\":1}";
    auto m2 = srv::model_info{m1};
    CHECK_EQ(m1, m2);
    std::ostringstream xmls(std::ios::binary);
    {
      core_oarchive oa(xmls, core_arch_flags); // no header here
      oa << core_nvp("o", m1);
      xmls.flush();
    }
    std::istringstream xmli(xmls.str(), std::ios::binary);
    {
      core_iarchive ia(xmli, arch_info_flags);       // potential header here
      REQUIRE_EQ(false, ia.archive_info.is_valid()); // ensure naked archive
      srv::model_info m3;
      ia >> core_nvp("o", m3);
      CHECK_EQ(m1, m3);
    }
  }

  TEST_SUITE_END();
}