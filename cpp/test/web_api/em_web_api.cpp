#include <csignal>
#include <memory>

#include <boost/asio/connect.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <dlib/logger.h>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/wind_turbine.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/web_api/targetver.h>

#include <test/test_pch.h>
#include <test/test_utils.h>
#include <test/web_api/build_stm_system.h>
#include <test/web_api/mocks.h>
#include <test/web_api/request_sender.h>

namespace shyft::energy_market::stm {
  using test_server = mocks::dstm_server;
  auto constexpr mega = 1e6;
  auto constexpr eur_mega = 1 / 1e6;

  void add_market(std::shared_ptr<stm_system> const & mdl) {
    core::utctime t0{0l};
    core::utctime dt = core::calendar::HOUR;
    time_axis::generic_dt ta(t0, dt, 6);
    auto mkt = std::make_shared<energy_market_area>(1, "NOX", "{}", mdl);
    mkt->price = time_series::dd::apoint_ts(
      ta,
      std::vector{30.1 * eur_mega, 30.2 * eur_mega, 31.3 * eur_mega, 31.4 * eur_mega, 31.5 * eur_mega, 31.6 * eur_mega},
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    mkt->load = time_series::dd::apoint_ts(
      ta,
      std::vector{1.1 * mega, 1.2 * mega, 1.3 * mega, 1.4 * mega, 1.5 * mega, 1.6 * mega},
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    mkt->sale = time_series::dd::apoint_ts(
      ta,
      std::vector{1.1 * mega, 1.2 * mega, 1.3 * mega, 1.4 * mega, 1.5 * mega, 1.6 * mega},
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    mkt->tsm["planned_revenue"] = 10.0 * mkt->sale; // add user speficied attribute
    mdl->market.push_back(mkt);
  }

  void add_unit_group(std::shared_ptr<stm_system> const & mdl) {
    auto ug = mdl->add_unit_group(1, "ug1", "{}");
    core::utctime t0{0l};
    core::utctime dt = calendar::HOUR;
    time_axis::generic_dt ta(t0, dt, 6);
    auto u0 = std::dynamic_pointer_cast<unit>(mdl->hps[0]->find_unit_by_id(1));
    FAST_REQUIRE_UNARY(u0 != nullptr);
    u0->production.result = time_series::dd::apoint_ts(
      ta,
      std::vector{0.0, 20.0 * mega, 30.0 * mega, 40.0 * mega, 0.0, 0.0},
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    u0->discharge.result = time_series::dd::apoint_ts(
      ta, std::vector{0.0, 21.0, 31.0, 41.0, 0.0, 0.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    ug->add_unit(
      u0,
      time_series::dd::apoint_ts(
        ta, std::vector{0.0, 1.0, 1.0, 1.0, 1.0, 0.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE));
    ug->group_type = unit_group_type::fcr_n_up;
    ug->obligation.schedule = time_series::dd::apoint_ts(
      ta,
      std::vector{0.0, 2.0 * mega, 3.0 * mega, 4.0 * mega, 0.0, 0.0},
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
  }

  void add_network_and_power_modules(std::shared_ptr<stm_system> const & mdl) {
    auto sys_builder = stm_builder(mdl);
    auto n = sys_builder.create_network(50, "net", "{}");
    auto net_builder = network_builder(n);
    auto t1 = net_builder.create_transmission_line(61, "t1", "{}");
    auto t2 = net_builder.create_transmission_line(62, "t2", "{}");
    auto b1 = net_builder.create_busbar(52, "b1", "{}");
    auto b2 = net_builder.create_busbar(53, "b2", "{}");
    b1->add_to_start_of_transmission_line(t1);
    b2->add_to_end_of_transmission_line(t1);
    auto p1 = sys_builder.create_power_module(54, "p1", "{}");
    auto p2 = sys_builder.create_power_module(55, "p2", "{}");
    b1->add_power_module(p1, time_series::dd::apoint_ts{});
    b1->add_power_module(p2, time_series::dd::apoint_ts{});
    core::utctime t0{0l};
    time_axis::generic_dt ta(t0, core::calendar::HOUR, 6);
    auto u0 = std::dynamic_pointer_cast<unit>(mdl->hps[0]->find_unit_by_id(1));
    b1->add_unit(
      u0,
      time_series::dd::apoint_ts(
        ta, std::vector{0.0, 1.0, 1.0, 1.0, 1.0, 0.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE));
  }

  void add_contracts(std::shared_ptr<stm_system> const & mdl) {
    using shyft::energy_market::stm::contract;
    auto c1 = make_shared<contract>(1, "c1", "{\"x\":1.2}", mdl);
    auto c2 = make_shared<contract>(2, "c2", "{}", mdl);
    mdl->contracts.push_back(c1);
    mdl->contracts.push_back(c2);
    c1->add_relation(11, c2, 1);
    c1->power_plants.push_back(
      dynamic_pointer_cast<shyft::energy_market::stm::power_plant>(mdl->hps.front()->power_plants.front()));
    mdl->market.front()->contracts.push_back(c1); // just to add a relation to energy_market_area
  }

  void add_windfarms(std::shared_ptr<stm_system> const & mdl) {
    auto sys_builder = stm_builder(mdl);
    auto wf = sys_builder.create_wind_farm(99, "wp", "");
    core::utctime t0{0l};
    time_axis::generic_dt ta(t0, calendar::HOUR, 6);
    wf->production.result = time_series::dd::apoint_ts(
      ta,
      vector<double>{
        22.8,
        22.8,
        22.8,
        22.8,
        22.8,
        22.8,
      },
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    time_series::dd::apoint_ts height(
      ta,
      vector<double>{
        1.8,
        2.8,
        3.8,
        4.8,
        5.8,
        6.8,
      },
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    using shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::point;
    auto xy_0 = std::make_shared<xy_point_curve>(std::vector<point>{
      { 0.0,   0.0},
      { 5.0, 0.1e6},
      {15.0, 2.0e6},
      {25.0, 2.1e6}
    });
    auto power_curve = std::make_shared<std::map<utctime, std::shared_ptr<xy_point_curve>>>();
    (*power_curve)[t0] = xy_0;
    auto wt1 = wf->create_wind_turbine(1, "WTG-1", "{}", core::geo_point{1.0, 2.0, 3.0}, height, power_curve);
    wt1->production.realised = height;
    auto wt2 = wf->create_wind_turbine(2, "WTG-2", "{}", core::geo_point{100.0, 200.0, 3.0}, height, power_curve);
    wt2->production.result = height;
  }

  void add_reservoir_aggregates(std::shared_ptr<stm_system> const & mdl) {
    auto& hps = mdl->hps[0];
    auto ra = std::make_shared<reservoir_aggregate>(3, "ra", "", hps);
    utctime t0{0l};
    shyft::time_axis::generic_dt ta(t0, calendar::HOUR, 6);
    ra->tsm["water_value"] = time_series::dd::apoint_ts(
      ta,
      std::vector{
        1.09e-5,
        1.09e-5,
        1.09e-5,
        1.09e-5,
        1.09e-5,
        1.09e-5,
      },
      time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    hps->reservoir_aggregates.push_back(std::move(ra));
  }

  auto run_websocket(std::string host_ip, int port, auto const & requests) {
    auto request = std::ranges::begin(requests);
    std::vector<std::string> responses;
    if (request == std::ranges::end(requests))
      return responses;
    boost::asio::io_context ioc;
    auto s1 = std::make_shared<shyft::test::session>(ioc);
    s1->run(host_ip, port, *request, [&](string const & web_response) -> std::string {
      responses.push_back(web_response);
      ++request;
      return request == std::ranges::end(requests) ? "" : *request;
    });
    ioc.run();
    s1.reset();
    return responses;
  }

  TEST_SUITE_BEGIN("web_api");

  TEST_CASE("web_api/stm/read_model") {
    dlib::set_all_logging_levels(dlib::LNONE);
    auto dirname = "em.web_api.test." + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    test_server a;
    std::string host_ip{"127.0.0.1"};
    a.set_listening_ip(host_ip);
    a.start_server();
    // Store some models:
    auto mdl = test::web_api::create_stm_system();
    a.do_add_model("sorland", mdl);

    auto mdl2 = test::web_api::create_simple_system(2, "simple");
    mdl2->run_params.n_inc_runs = 2;
    mdl2->run_params.n_full_runs = 3;
    mdl2->run_params.head_opt = true;
    add_market(mdl2);
    add_unit_group(mdl2);
    add_network_and_power_modules(mdl2);
    add_contracts(mdl2);
    add_windfarms(mdl2);
    add_reservoir_aggregates(mdl2);
    auto u0 = std::dynamic_pointer_cast<unit>(mdl2->hps[0]->find_unit_by_id(1));
    u0->best_profit.production = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
    auto t_begin = core::seconds(3600l);
    u0->best_profit.production.get()->emplace(
      t_begin,
      std::make_shared<shyft::energy_market::hydro_power::xy_point_curve>(
        shyft::energy_market::hydro_power::xy_point_curve::make(
          std::vector{20.0 * mega, 40.0 * mega, 60.0 * mega, 80.0 * mega}, std::vector{96.0, 98.0, 99.0, 98.0})));

    a.do_add_model("simple", mdl2);

    auto port = a.start_web_api(host_ip, 0, doc_root, 1, 1);

    REQUIRE_EQ(true, a.web_api_running());
    std::vector requests{
      R"_(read_model {"request_id": "1", "model_key": "simple"})_",
      R"_(read_attributes {
                    "request_id": "2",
                    "model_key": "simple",
                    "read_period":[ 0, 10000],
                    "subscribe":false,
                    "cache":false,
                    "time_axis": {"t0":0.0,"dt": 3600,"n": 2},
                    "hps":[{
                            "hps_id": 1,
                                "reservoirs": [{
                                    "component_id": 1,
                                    "attribute_ids": ["level.result"]
                                }],
                                "units": [{
                                    "component_id": 1,
                                    "attribute_ids": ["discharge.constraint.max","best_profit.production"]
                                }],
                                "catchments": [{
                                    "component_id": 12,
                                    "attribute_ids": ["component", "doesnt", "exist",""]
                                }],
                                "waterways":[{
                                    "component_id": 1,
                                    "attribute_ids": ["discharge.result", "discharge.realised"]
                                }],
                                "gates": [{
                                    "component_id": 1,
                                    "attribute_ids": ["discharge.result","discharge.realised"]
                                }],
                                "power_plants": [{
                                    "component_id": 2,
                                    "attribute_ids": [ "" ]
                                }],
                                "reservoir_aggregates": [{
                                    "component_id": 3,
                                    "attribute_ids": [ "ts.water_value" ]                                
                                }]
                            }
                            ],
                    "markets":[{
                            "component_id": 1,
                            "attribute_ids": ["price","load","sale","buy","ts.planned_revenue"]
                        }],
                    "contracts":[{
                            "component_id": 1,
                            "attribute_ids": ["price","json"],
                            "relations": [{
                                "component_id":11,
                                "attribute_ids":["relation_type"]
                            }]
                        }],
                    "unit_groups":[
                        {
                            "component_id":1,
                            "attribute_ids":["group_type","obligation.schedule","flow","production"],
                            "members": [{
                                "component_id":1,
                                "attribute_ids":["active"]
                            }]
                        }
                    ],
                    "networks":[
                        {
                            "component_id":50,
                            "transmission_lines":[{
                                "component_id":61,
                                "attribute_ids":["capacity"]
                            },
                            {
                                "component_id":62,
                                "attribute_ids":["capacity"]
                            }],
                            "busbars":[{
                                "component_id":52,
                                "attribute_ids":["price.result"],
                                "units": [{
                                    "component_id":1,
                                    "attribute_ids":["active"]
                                }],
                                "power_modules": [{
                                    "component_id":54,
                                    "attribute_ids":["active"]
                                },
                                {
                                    "component_id":55,
                                    "attribute_ids":["active"]
                                }]
                            },
                            {
                                "component_id":53,
                                "attribute_ids":["price.result"]
                            }]
                        }
                    ],
                    "wind_farms":[
                        {
                            "component_id":99,
                            "attribute_ids":["production.result","location","epsg"],
                            "wind_turbines":[
                              {
                                 "component_id": 1,
                                 "attribute_ids": ["location","power_curve","production.realised"]
                              }
                            ]
                        }
                    ],
                    "contract_portfolios": null,
                    "power_modules":[{
                        "component_id":54,
                        "attribute_ids":["power.schedule"]
                    }]
                })_",
      R"_(get_model_infos {"request_id": "3"})_",
      R"_(get_hydro_components {"request_id": "4",
                    "model_key": "simple",
                    "hps_id": 1
            })_",
      R"_(get_hydro_components {"request_id": "5",
                    "model_key": "simple",
                    "hps_id": 1,
                    "available_data": true
            })_",
      R"_(run_params {"request_id" : "6", "model_key" : "simple"})_",
      R"_(get_log {"request_id": "7", "model_key": "simple"})_",
      R"_(get_contract_components {"request_id": "8",
                    "model_key": "simple",
                    "available_data": true
            })_",
      R"_(get_wind_farm_components {"request_id": "9",
                    "model_key": "simple",
                    "wf_id": 99,
                    "available_data": true
            })_",
    };
    auto responses = run_websocket(host_ip, port, requests);
    std::vector<std::string> expected{
      R"_({"request_id":"1","result":{"model_key":"simple","id":2,"name":"simple","json":"","hps":[{"id":1,"name":"simple"}],"unit_groups":[{"id":1,"name":"ug1"}],"markets":[{"id":1,"name":"NOX"}],"contracts":[{"id":1,"name":"c1"},{"id":2,"name":"c2"}],"contract_portfolios":[],"networks":[{"id":50,"name":"net"}],"power_modules":[{"id":54,"name":"p1"},{"id":55,"name":"p2"}],"wind_farms":[{"id":99,"name":"wp"}]}})_",
      R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.result","data":{"id":"","pfx":false,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1,1]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"discharge.constraint.max","data":"not found"},{"attribute_id":"best_profit.production","data":{"3600.0":[[2.0e07,96.0],[4.0e07,98.0],[6.0e07,99.0],[8.0e07,98.0]]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":"","data":"attribute not found"}],"component_id":2}],"waterways":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":false,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[5,5]}},{"attribute_id":"discharge.realised","data":"not found"}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[20,21]}}],"component_id":1}],"catchments":[{"component_data":"Unable to find component","component_id":12}],"reservoir_aggregates":[{"component_data":[{"attribute_id":"ts.water_value","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1.09e-05,1.09e-05]}}],"component_id":3}]}],"markets":[{"component_data":[{"attribute_id":"price","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[3.01e-05,3.02e-05]}},{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1100000,1200000]}},{"attribute_id":"buy","data":"not found"},{"attribute_id":"sale","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1100000,1200000]}},{"attribute_id":"ts.planned_revenue","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[11000000,12000000]}}],"component_id":1}],"contracts":[{"component_id":1,"component_data":[{"attribute_id":"price","data":"not found"},{"attribute_id":"json","data":"{\"x\":1.2}"}],"relations":[{"component_data":[{"attribute_id":"relation_type","data":1}],"component_id":11}]}],"power_modules":[{"component_data":[{"attribute_id":"power.schedule","data":"not found"}],"component_id":54}],"networks":[{"component_id":50,"transmission_lines":[{"component_data":[{"attribute_id":"capacity","data":"not found"}],"component_id":61},{"component_data":[{"attribute_id":"capacity","data":"not found"}],"component_id":62}],"busbars":[{"component_id":52,"component_data":[{"attribute_id":"price.result","data":"not found"}],"units":[{"component_data":[{"attribute_id":"active","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0,1]}}],"component_id":1}],"power_modules":[{"component_data":[{"attribute_id":"active","data":"not found"}],"component_id":54},{"component_data":[{"attribute_id":"active","data":"not found"}],"component_id":55}]},{"component_id":53,"component_data":[{"attribute_id":"price.result","data":"not found"}]}]}],"unit_groups":[{"component_id":1,"component_data":[{"attribute_id":"group_type","data":1},{"attribute_id":"obligation.schedule","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0,2000000]}},{"attribute_id":"production","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0,20000000]}},{"attribute_id":"flow","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0,21]}}],"members":[{"component_data":[{"attribute_id":"active","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0,1]}}],"component_id":1}]}],"wind_farms":[{"component_id":99,"component_data":[{"attribute_id":"location","data":{"x":0,"y":0,"z":0}},{"attribute_id":"epsg","data":0},{"attribute_id":"production.result","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[22.8,22.8]}}],"wind_turbines":[{"component_data":[{"attribute_id":"location","data":{"x":1,"y":2,"z":3}},{"attribute_id":"power_curve","data":{"0.0":[[0.0,0.0],[5.0,1.0e05],[15.0,2.0e06],[25.0,2.1e06]]}},{"attribute_id":"production.realised","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1.8,2.8]}}],"component_id":1}]}]}})_",
      R"_({"request_id":"3","result":[{"model_key":"simple","id":2,"name":"simple"},{"model_key":"sorland","id":1,"name":"stm_system"}]})_",
      R"_({"request_id":"4","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"id":1,"name":"simple_res"}],"units":[{"id":1,"name":"simple_unit"}],"power_plants":[{"id":2,"name":"simple_pp","units":[1]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]}]}})_",
      R"_({"request_id":"5","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"id":1,"name":"simple_res","set_attrs":["level.result","inflow.schedule"]}],"units":[{"id":1,"name":"simple_unit","set_attrs":["production.result","discharge.result","discharge.constraint.min","best_profit.production"]}],"power_plants":[{"id":2,"name":"simple_pp","units":[1],"set_attrs":[]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}],"set_attrs":["discharge.result"]}]}})_",
      R"_({"request_id":"6","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":2},{"attribute_id":"n_full_runs","data":3},{"attribute_id":"head_opt","data":true},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
      R"_({"request_id":"7","log_version":0,"bookmark":0,"result":[]})_",
      R"_({"request_id":"8","result":{"model_key":"simple","contracts":[{"id":1,"name":"c1","power_plants":[{"id":2,"name":"simple_pp","units":[1],"set_attrs":[]}],"wind_farms":[],"energy_market_areas":[{"id":1,"name":"NOX","set_attrs":["price","load","sale"]}],"relations":[{"id":11,"relation_type":1,"contract":2}],"portfolios":[],"set_attrs":[]},{"id":2,"name":"c2","power_plants":[],"wind_farms":[],"energy_market_areas":[],"relations":[],"portfolios":[],"set_attrs":[]}]}})_",
      R"_({"request_id":"9","result":{"model_key":"simple","wf_id":99,"wind_turbines":[{"id":1,"name":"WTG-1","set_attrs":["height","location","power_curve","production.realised"]},{"id":2,"name":"WTG-2","set_attrs":["height","location","power_curve","production.result"]}]}})_"};
    REQUIRE_EQ(expected.size(), responses.size());

    for (auto i = 0u; i < expected.size(); i++)
      CHECK_EQ(responses[i], expected[i]);
    a.stop_web_api();
  }

  TEST_CASE("web_api/stm/set_attributes") {
    dlib::set_all_logging_levels(dlib::LNONE);
    auto dirname = "em.web_api.test." + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    test_server a;
    string host_ip{"127.0.0.1"};
    a.set_listening_ip(host_ip);
    a.start_server();
    auto mdl = test::web_api::create_stm_system();
    a.do_add_model("sorland", mdl);

    auto mdl2 = test::web_api::create_simple_system(2, "simple");
    add_market(mdl2);
    add_unit_group(mdl2);
    add_network_and_power_modules(mdl2);
    add_contracts(mdl2);
    add_windfarms(mdl2);
    add_reservoir_aggregates(mdl2);
    a.do_add_model("simple", mdl2);

    int port = a.start_web_api(host_ip, 0, doc_root, 1, 1);

    std::this_thread::sleep_for(std::chrono::milliseconds(700l));
    REQUIRE_EQ(true, a.web_api_running());
    std::vector requests{
      R"_(set_attributes {"request_id": "1",
        "model_key": "simple",
        "hps": [
                {
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_min",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": true,
                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                        "values": [0.1, 0.2, null, 0.4]
                                    }
                                },
                                {
                                    "attribute_id": "volume_level_mapping",
                                    "value": {
                                        "1969-12-31T23:00:00Z": [[1.0, 3.0], [1.5, 2.5], [2.0, 2.0], [2.5, 1.5], [3.0, 1.0]],
                                        "1970-01-01T00:00:02Z": [[1.0, 1.0], [1.5, 2.0]]
                                    }
                                }
                            ]
                        }
                    ],
                    "units": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "production.result",
                                    "value": {
                                        "1970-01-01T00:00:00Z": [[1.0, 3.0], [1.5, 2.5], [2.0, 2.0], [2.5, 1.5], [3.0, 1.0]],
                                        "1970-01-01T00:00:01Z": [[1.0, 1.0], [1.5, 2.0]]
                                    }
                                }
                            ]
                        }
                    ],
                    "gates": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "opening.schedule",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": true,
                                        "time_axis": {"time_points": [0, 2, 3, 4, 5]},
                                        "values": [0.1, 0.2, null, 0.4]
                                    }
                                }
                            ]
                        }
                    ],
                    "reservoir_aggregates": [
                        {
                            "component_id": 3,
                            "attribute_data": [
                                {
                                    "attribute_id": "ts.water_value",
                                    
                                    "value": {
                                        "id":"",
                                        "pfx":true,
                                        "time_axis":{"t0":0.0,"dt":3600.0,"n":2},
                                        "values":[13.09e-05,13.09e-05]}
                                }
                            ]
                        }
                    ]
                }
            ],
    "markets": [
        {
            "component_id": 1,
            "attribute_data": [
                {
                    "attribute_id": "price",
                    "value": {
                        "id": "abcd",
                        "pfx": false,
                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                        "values": [1000.0, 2000.0, 3000.0, 4000.0]
                    }
                }
            ]
        }
    ],
    "contracts":[
        {
            "component_id": 1,
            "attribute_data": [
                {
                    "attribute_id": "price",
                    "value": {
                        "id": "abcd",
                        "pfx": false,
                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                        "values": [1000.0, 2000.0, 3000.0, 4000.0]
                    }
                },
                {
                    "attribute_id": "json",
                    "value": "a_string"
                }
            ],
            "relations" : [
            {
                "component_id": 11,
                "attribute_data": [
                    {
                            "attribute_id": "relation_type",
                            "value": 2
                    }
                ]
                }
            ]
        }
        ],
    "power_modules": [
        {
            "component_id": 54,
            "attribute_data": [
                {
                    "attribute_id": "power.schedule",
                    "value": {
                        "id": "abcd",
                        "pfx": false,
                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                        "values": [1111.0, 2222.0, 3333.0, 4444.0]
                    }
                }
            ]
        }
    ],
    "networks": [
        {
            "component_id": 50,
            "transmission_lines" : [
            {
                "component_id": 61,
                "attribute_data": [
                    {
                            "attribute_id": "capacity",
                            "value": {
                                "id": "abcd",
                                "pfx": false,
                                "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                "values": [1.0, 2.0, 3.0, 4.0]
                            }
                    }
                ]
                }
            ],
            "busbars" : [
            {
                "component_id": 52,
                "attribute_data": [
                    {
                            "attribute_id": "price.result",
                            "value": {
                                "id": "abcd",
                                "pfx": false,
                                "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                "values": [1.0, 2.0, 3.0, 4.0]
                            }
                    }
                ],
                "units" : [
                    {
                        "component_id": 1,
                        "attribute_data": [
                            {
                                    "attribute_id": "active",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": false,
                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                        "values": [1.0, 1.0, 2.0, 3.0]
                                    }
                            }
                        ]
                    }
                ],
                "power_modules" : [
                    {
                        "component_id": 54,
                        "attribute_data": [
                            {
                                    "attribute_id": "active",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": false,
                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                        "values": [1.0, 1.0, 0.0, 1.0]
                                    }
                            }
                        ]
                    },
                    {
                        "component_id": 55,
                        "attribute_data": [
                            {
                                    "attribute_id": "active",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": false,
                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                        "values": [1.0, 0.0, 1.0, 1.0]
                                    }
                            }
                        ]
                    }
                ]
                }
            ]
        }
    ],
    "unit_groups": [
        {
            "component_id": 1,
            "attribute_data": [
                {
                    "attribute_id": "obligation.schedule",
                    "value": {
                        "id": "abcd",
                        "pfx": false,
                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                        "values": [1000.0, 2000.0, 3000.0, 4000.0]
                    }
                }
            ],
            "members" : [
            {
                "component_id": 1,
                "attribute_data": [
                    {
                            "attribute_id": "active",
                            "value": {
                                "id": "abcd",
                                "pfx": false,
                                "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                "values": [1.0, 1.0, 0.0, 1.0]
                            }
                    }
                ]
                }
            ]
        }
    ],
    "wind_farms": [
        {
            "component_id":99,
            "attribute_data": [
                {
                    "attribute_id": "production.realised",
                    "value": {
                        "id": "some",
                        "pfx": false,
                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                        "values": [22.0, 33.0, 44.0, 55.0]
                    }
                }
            ],
            "wind_turbines":[
               {
                  "component_id": 1,
                  "attribute_data": [
                    {
                      "attribute_id": "production.realised",
                      "value": {
                        "id": "some",
                        "pfx": false,
                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                        "values": [22.0, 33.0, 44.0, 55.0]
                      }
                    }
                  ]
               }
            ]
        }
    ]
  }
)_",
      R"_(read_attributes {"request_id": "2",
        "model_key": "simple",
        "time_axis": {"time_points" : [ 1, 2, 3, 4, 5 ]},
        "hps": [{
            "hps_id": 1,
            "reservoirs": [{
                "component_id": 1,
                "attribute_ids": ["level.regulation_min", "volume_level_mapping"]
            }]
        }]
})_",
      R"_(set_attributes {"request_id": "A4",
        "model_key": "simple",
        "hps":[{
        "hps_id": 1,
        "reservoirs": [
            {
                "component_id": 1,
                "attribute_data": [
                    {
                        "attribute_id": "production.schedule",
                        "value": {
                        "id": "abcd",
                        "pfx" : true,
                        "time_axis": {"time_points": [946684800]},
                        "values" : [ 210 ]
                    }
                ]
            }
        ]}]
})_",
    };
    auto responses = run_websocket(host_ip, port, requests);
    std::vector expected{
      R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"volume_level_mapping","status":"OK"},{"attribute_id":"level.regulation_min","status":"OK"}]}],"units":[{"component_id":1,"status":[{"attribute_id":"production.result","status":"type mismatch"}]}],"gates":[{"component_id":1,"status":[{"attribute_id":"opening.schedule","status":"OK"}]}],"reservoir_aggregates":[{"component_id":3,"status":[{"attribute_id":"ts.water_value","status":"OK"}]}]}],"markets":[{"component_id":1,"status":[{"attribute_id":"price","status":"OK"}]}],"contracts":[{"component_id":1,"status":[{"attribute_id":"price","status":"OK"},{"attribute_id":"json","status":"OK"}],"relations":[{"component_id":11,"status":[{"attribute_id":"relation_type","status":"immutable attribute"}]}]}],"power_modules":[{"component_id":54,"status":[{"attribute_id":"power.schedule","status":"OK"}]}],"networks":[{"component_id":50,"transmission_lines":[{"component_id":61,"status":[{"attribute_id":"capacity","status":"OK"}]}],"busbars":[{"status":[{"attribute_id":"price.result","status":"OK"}],"units":[{"component_id":1,"status":[{"attribute_id":"active","status":"OK"}]}],"power_modules":[{"component_id":54,"status":[{"attribute_id":"active","status":"OK"}]},{"component_id":55,"status":[{"attribute_id":"active","status":"OK"}]}]}]}],"unit_groups":[{"component_id":1,"status":[{"attribute_id":"obligation.schedule","status":"OK"}],"members":[{"component_id":1,"status":[{"attribute_id":"active","status":"OK"}]}]}],"wind_farms":[{"component_id":99,"status":[{"attribute_id":"production.realised","status":"OK"}],"wind_turbines":[{"component_id":1,"status":[{"attribute_id":"production.realised","status":"OK"}]}]}]}})_",
      R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"-3600.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]],"2.0":[[1.0,1.0],[1.5,2.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"time_points":[1.0,2.0,3.0,4.0,5.0]},"values":[0.1,0.2,null,0.4]}}],"component_id":1}]}]}})_",
      R"_({"request_id":"A4","diagnostics":"exception: time_axis::point_dt() needs at least two time-points"})_"};
    REQUIRE_EQ(expected.size(), responses.size());

    for (size_t i = 0; i < expected.size(); i++)
      CHECK_EQ(responses[i], expected[i]);
    a.stop_web_api();
  }

  TEST_CASE("web_api/dstm_fx") {
    dlib::set_all_logging_levels(dlib::LNONE);
    auto dirname = "em.web_api.test.fx." + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    test_server a;
    std::string host_ip{"127.0.0.1"};
    a.set_listening_ip(host_ip);
    a.start_server();

    auto port = a.start_web_api(host_ip, 0, doc_root, 1, 1);

    std::this_thread::sleep_for(std::chrono::milliseconds(700l));
    REQUIRE_EQ(true, a.web_api_running());
    std::vector requests{
      R"_(fx {"request_id": "1", "model_key": "simple","fx_arg":"{'optimize':'this'}"})_",
    };
    auto responses = run_websocket(host_ip, port, requests);
    std::vector expected{
      R"_({"request_id":"1","diagnostics":""})_",
    };
    REQUIRE_EQ(expected.size(), responses.size());
    FAST_CHECK_EQ(a.fx_mid(), "simple");
    FAST_CHECK_EQ(a.fx_arg(), "{'optimize':'this'}");
    for (auto i = 0u; i < expected.size(); i++)
      CHECK_EQ(responses[i], expected[i]);
    a.stop_web_api();
  }

  TEST_CASE("web_api/merge_set") {
    dlib::set_all_logging_levels(dlib::LNONE);
    auto dirname = "em.web_api.test.merge." + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    test_server a(doc_root);
    std::string host_ip{"127.0.0.1"};
    a.set_listening_ip(host_ip);
    a.start_server();

    auto mdl = test::web_api::create_simple_system(2, "simple");
    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
    auto ts1 = time_series::dd::apoint_ts(
      "shyft://test/ts1",
      time_series::dd::apoint_ts(
        time_axis::generic_dt(core::from_seconds(0), core::from_seconds(1), 5),
        1.0,
        time_series::ts_point_fx::POINT_AVERAGE_VALUE));
    auto ts2 = time_series::dd::apoint_ts(
      "shyft://test/ts2",
      time_series::dd::apoint_ts(
        time_axis::generic_dt(
          {core::from_seconds(0.), core::from_seconds(1.), core::from_seconds(5.), core::from_seconds(6.)}),
        2.0,
        time_series::ts_point_fx::POINT_AVERAGE_VALUE));
    std::vector tsv{ts1, ts2};
    rsv->level.regulation_min = time_series::dd::apoint_ts(ts1.id());
    rsv->level.regulation_max = time_series::dd::apoint_ts(ts2.id());
    a.dtss_do_store_ts(tsv, false, true); // no cache
    a.do_add_model("simple", mdl);
    auto port = a.start_web_api(host_ip, 0, doc_root, 1, 1);

    std::this_thread::sleep_for(std::chrono::milliseconds(700l));
    REQUIRE_EQ(true, a.web_api_running());
    std::vector requests{
      R"_(set_attributes {"request_id": "1",
                "model_key": "simple",
                "merge": true,
                "cache_on_write": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.result",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": true,
                                        "time_axis": {"time_points": [1,2,3,4,5]},
                                        "values": [0.1, 0.2, null, 0.4]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
      R"_(read_attributes {"request_id": "2",
                    "model_key": "simple",
                    "time_axis": {"t0" : 0, "dt": 1,"n":0},
                    "read_period":[0,20000],
                    "cache": false,
                    "hps":[{
                            "hps_id": 1,
                            "reservoirs": [{
                                "component_id": 1,
                                "attribute_ids": ["level.result"]
                            }]
                    }]
            })_",
      R"_(set_attributes {"request_id": "faulty_merge",
                "model_key": "simple",
                "merge": true,
                "cache_on_write": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_min",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": true,
                                        "time_axis": {"time_points": [1.5, 2]},
                                        "values": [0.]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
      R"_(set_attributes {"request_id": "flex_merge",
                "model_key": "simple",
                "merge": false,
                "cache_on_write": true,
                "strict": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_min",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": true,
                                        "time_axis": {"time_points": [1.5, 3.2]},
                                        "values": [0.]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
      R"_(set_attributes {
                "request_id": "merge_proper",
                "model_key": "simple",
                "merge": true,
                "cache_on_write": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_max",
                                    "value": {
                                        "id"        : "abcd",
                                        "pfx"       : true,
                                        "time_axis" : {"time_points": [3.5, 4.5, 5.5]},
                                        "values"    : [-1., -1.]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
      R"_(read_attributes {
                "request_id": "read_merged",
                "model_key": "simple",
                "time_axis": {"time_points": [3.5, 4.0, 4.5, 5.0, 5.5, 6.0]},
                "cache": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [{
                        "component_id": 1,
                        "attribute_ids": ["level.regulation_max"]
                    }]
               }]
            })_",
      R"_(set_attributes {
                "request_id": "no_merge",
                "model_key": "simple",
                "merge": false,
                "recreate": false,
                "cache_on_write":false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_max",
                                    "value": {
                                        "id"        : "abcd",
                                        "pfx"       : true,
                                        "time_axis" : {"time_points": [3.5, 4.5, 5.5]},
                                        "values"    : [3.0, 3.0]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
      R"_(read_attributes {
                "request_id": "read_unmerged",
                "model_key": "simple",
                "time_axis": {"time_points": [2.0, 3.5, 5.5, 6.0]},
                "cache": false,
                "hps":[{
                        "hps_id": 1,
                        "reservoirs": [{
                            "component_id": 1,
                            "attribute_ids": ["level.regulation_max"]
                        }]
                }]
            })_"};
    auto responses = run_websocket(host_ip, port, requests);
    std::vector expected{
      R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.result","status":"OK"}]}]}]}})_",
      R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.result","data":{"id":"","pfx":false,"time_axis":{"time_points":[0.0,1.0,2.0,3.0,4.0,3600.0,7200.0,10800.0,14400.0,18000.0,21600.0]},"values":[1,0.1,0.2,null,0.4,1,1,1,1,1]}}],"component_id":1}]}]}})_",
      R"_({"request_id":"faulty_merge","diagnostics":"exception: dtss_store: cannot merge with different ta type)_",
      R"_({"request_id":"flex_merge","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.regulation_min","status":"stored to dtss"}]}]}]}})_",
      R"_({"request_id":"merge_proper","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.regulation_max","status":"stored to dtss"}]}]}]}})_",
      R"_({"request_id":"read_merged","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.regulation_max","data":{"id":"","pfx":true,"time_axis":{"time_points":[3.5,4.0,4.5,5.0,5.5,6.0]},"values":[-1,-1,-1,2,2]}}],"component_id":1}]}]}})_",
      R"_({"request_id":"no_merge","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.regulation_max","status":"stored to dtss"}]}]}]}})_",
      R"_({"request_id":"read_unmerged","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.regulation_max","data":{"id":"","pfx":true,"time_axis":{"time_points":[2.0,3.5,5.5,6.0]},"values":[2,3,2]}}],"component_id":1}]}]}})_"};
    REQUIRE_EQ(expected.size(), responses.size());
    for (size_t i = 0; i < expected.size(); i++) {
      if (i == 2) {
        CHECK_EQ(true, responses[i].starts_with(expected[i]));
      } else {
        CHECK_EQ(responses[i], expected[i]);
      }
    }
    a.stop_web_api();
  }

  TEST_CASE("web_api/attributes_from_dstm") {
    dlib::set_all_logging_levels(dlib::LNONE);
    auto dirname = "em.web_api.test.dtss" + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    test_server srv(doc_root);
    std::string host_ip{"127.0.0.1"};
    srv.set_listening_ip(host_ip);
    srv.start_server();
    auto mdl = test::web_api::create_simple_system(2, "simple");
    auto hps = mdl->hps[0];
    auto tsv = mocks::mk_expressions("test");
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
    rsv->level.realised = time_series::dd::apoint_ts(tsv[0].id()); // A terminal
    rsv->level.schedule = time_series::dd::apoint_ts(tsv[1].id()); // An expression
    std::string prefix = "dstm://Msimple";
    std::string lvl_url;
    lvl_url.reserve(20);
    lvl_url += prefix;

    {
      auto rbi = std::back_inserter(lvl_url);
      rsv->generate_url(rbi);
    }
    lvl_url += ".level.result";

    std::string inflow_url;
    inflow_url.reserve(20);
    inflow_url += prefix;
    {
      auto rbi = std::back_inserter(inflow_url);
      rsv->generate_url(rbi);
    }
    inflow_url += ".inflow.schedule";

    time_series::dd::apoint_ts inflow(inflow_url);
    time_series::dd::apoint_ts lvl(lvl_url);
    rsv->volume.result = inflow + lvl;

    auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
    auto u = std::dynamic_pointer_cast<unit>(pp->units[0]);
    u->production.result = time_series::dd::apoint_ts(tsv[2].id());    // A terminal
    pp->production.schedule = time_series::dd::apoint_ts(tsv[3].id()); // An expression
    srv.dtss_do_store_ts(tsv, true, true);

    srv.do_add_model("simple", mdl);


    auto port = srv.start_web_api(host_ip, 0, doc_root, 1, 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(700l));
    REQUIRE_EQ(true, srv.web_api_running());
    std::vector<std::string> requests{
      R"_(read_attributes {"request_id": "1",
                    "model_key": "simple",
                    "time_axis": {"t0":1800,"dt": 3600, "n": 2},
                    "hps":[{
                            "hps_id": 1,
                            "reservoirs": [{
                                "component_id": 1,
                                "attribute_ids": ["level.realised","level.schedule","volume.result"]
                            }],
                            "units": [{
                                "component_id": 1,
                                "attribute_ids": ["production.result"]
                            }],
                            "power_plants": [{
                                "component_id": 2,
                                "attribute_ids": ["production.schedule"]
                            }]
                    }]
                })_",
      R"_(set_attributes {"request_id": "2",
                    "model_key": "simple",
                    "merge": false,
                    "recreate": true,
                    "hps":[{
                        "hps_id": 1,
                        "reservoirs": [
                            {
                                "component_id": 1,
                                "attribute_data": [
                                    {
                                        "attribute_id": "level.realised",
                                        "value": {
                                            "id": "",
                                            "pfx": true,
                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                            "values"    : [ 0.1, 0.1, 0.1, 0.1 ]
                                        }
                                    },
                                    {
                                        "attribute_id": "level.result",
                                        "value": {
                                            "id": "",
                                            "pfx": true,
                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                            "values"    : [ 0.2, 0.2, 0.2, 0.2 ]
                                        }
                                    },
                                    {
                                        "attribute_id": "inflow.schedule",
                                        "value" : {
                                            "id": "",
                                            "pfx": true,
                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                            "values"    : [ 0.3, 0.3, 0.3, 0.3 ]
                                        }
                                    },
                                    {
                                        "attribute_id": "volume.result",
                                        "value" : {
                                            "id": "",
                                            "pfx": true,
                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                            "values"    : [ 0.4, 0.4, 0.4, 0.4 ]
                                        }
                                    }
                                ]
                            }
                        ]
                    }]
            })_",
      R"_(read_attributes {"request_id": "3",
                    "model_key": "simple",
                    "time_axis": {"t0":1,"dt": 1,"n": 2},
                    "hps":[{
                        "hps_id": 1,
                        "reservoirs": [{
                            "component_id": 1,
                            "attribute_ids": ["level.realised", "level.result", "volume.result"]
                        }]
                    }]
            })_",
    };
    auto responses = run_websocket(host_ip, port, requests);
    std::vector expected{
      R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-2000,-2000]}},{"attribute_id":"level.schedule","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-8000,-8000]}},{"attribute_id":"volume.result","data":{"id":"","pfx":false,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[2.5,3]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"production.result","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-3000,-3000]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":"production.schedule","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-12000,-12000]}}],"component_id":2}]}]}})_",
      R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.realised","status":"stored to dtss"},{"attribute_id":"level.result","status":"OK"},{"attribute_id":"volume.result","status":"Time series is an expression. Cannot be set."},{"attribute_id":"inflow.schedule","status":"OK"}]}]}]}})_",
      R"_({"request_id":"3","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":2},"values":[0.1,0.1]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":2},"values":[0.2,0.2]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":2},"values":[0.5,0.5]}}],"component_id":1}]}]}})_"

    };
    REQUIRE_EQ(expected.size(), responses.size());
    for (size_t i = 0; i < expected.size(); i++)
      CHECK_EQ(responses[i], expected[i]);
    srv.stop_web_api();
  }

  TEST_SUITE_END();
}
