#pragma once
#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <string_view>

#include <boost/asio/connect.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/buffers_to_string.hpp>
#include <boost/beast/core/multi_buffer.hpp>
#include <boost/beast/websocket.hpp>
#include <fmt/core.h>

#include <shyft/core/utility.h>

namespace shyft::test {
  /**
   * Sends a Websocket message, from examples made by boost.beast/Vinnie Falco
   * Provide a function to recieve responses.
   * If the string returned by response function is continue_reading! , anonther read is scheduled, any other non
   * empty response is going to be sent as the next request.
   * If wait_for_reply is false, the socket will not try to read the reply.
   */
  class session : public std::enable_shared_from_this<session> {
    boost::asio::ip::tcp::resolver resolver_;
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws_;
    boost::beast::multi_buffer buffer_;
    std::string host_;
    std::string port_;
    std::string text_;
    std::string response_;
    std::string fail_;
    bool wait_for_reply_ = true;
    std::function<std::string(std::string const &)> report_response;

    void fail(boost::system::error_code ec, char const * what) {
      fail_ = fmt::format("{}: {}\n", what, ec.message());
    }

   public:
    explicit session(boost::asio::io_context& ioc, bool wait_for_reply = true)
      : resolver_(ioc)
      , ws_(ioc)
      , wait_for_reply_(wait_for_reply) {
    }

    ~session() {
      if (!fail_.empty()) {
        fmt::print("Session failed with {}", fail_);
      }
    }

    void run(std::string_view host, int port, std::string_view text, auto&& rep_response) {
      host_ = host;
      text_ = text;
      port_ = std::to_string(port);
      report_response = SHYFT_FWD(rep_response);
      resolver_.async_resolve(
        host_,
        port_,
        [me = shared_from_this()](boost::system::error_code ec, boost::asio::ip::tcp::resolver::results_type results) {
          me->on_resolve(ec, std::move(results));
        });
    }

    void on_resolve(boost::system::error_code ec, boost::asio::ip::tcp::resolver::results_type results) {
      if (ec) {
        fail(ec, "resolve");
        return;
      }
      boost::asio::async_connect(
        ws_.next_layer(), results.begin(), results.end(), [me = shared_from_this()](auto ec, auto&&...) {
          return me->on_connect(ec);
        });
    }

    void on_connect(boost::system::error_code ec) {
      if (ec) {
        fail(ec, "connect");
        return;
      }
      ws_.async_handshake(host_, "/", [me = shared_from_this()](boost::system::error_code ec) {
        me->on_handshake(ec);
      });
    }

    void on_handshake(boost::system::error_code ec) {
      if (ec) {
        fail(ec, "handshake");
        return;
      }
      if (!text_.empty()) {
        ws_.async_write(
          boost::asio::buffer(text_),
          [me = shared_from_this()](boost::system::error_code ec, size_t bytes_transferred) {
            me->on_write(ec, bytes_transferred);
          });
      } else {
        ws_.async_close(
          boost::beast::websocket::close_code::normal, [me = shared_from_this()](boost::system::error_code ec) {
            me->on_close(ec);
          });
      }
    }

    void on_write(boost::system::error_code ec, std::size_t) {
      if (ec) {
        fail(ec, "write");
        return;
      }
      if (wait_for_reply_)
        ws_.async_read(buffer_, [me = shared_from_this()](boost::system::error_code ec, size_t bytes_transferred) {
          me->on_read(ec, bytes_transferred);
        });
    }

    void on_read(boost::system::error_code ec, std::size_t) {
      if (ec) {
        fail(ec, "read");
        return;
      }
      response_ = boost::beast::buffers_to_string(buffer_.data());
      buffer_.consume(buffer_.size());
      text_ = report_response(response_);
      if (!text_.empty()) {
        if (text_.starts_with("continue_reading!"))
          ws_.async_read(
            buffer_, [me = shared_from_this()](boost::system::error_code ec, std::size_t bytes_transferred) {
              me->on_read(ec, bytes_transferred);
            });
        else
          ws_.async_write(
            boost::asio::buffer(text_),
            [me = shared_from_this()](boost::system::error_code ec, size_t bytes_transferred) {
              me->on_write(ec, bytes_transferred);
            });
      } else {
        ws_.async_close(
          boost::beast::websocket::close_code::normal, [me = shared_from_this()](boost::system::error_code ec) {
            me->on_close(ec);
          });
      }
    }

    void on_close(boost::system::error_code ec) {
      if (ec) {
        fail(ec, "close");
        return;
      }
    }
  };

}