#include <csignal>
#include <cstdint>
#include <future>
#include <memory>
#include <stdexcept>
#include <string>
#include <thread>
#include <utility>

#include <boost/asio/io_context.hpp>
#include <fmt/core.h>

#include <shyft/web_api/bg_work_result.h>

#include <doctest/doctest.h>
#include <test/test_pch.h>
#include <test/test_utils.h>
#include <test/web_api/request_sender.h>

namespace shyft::test {
  using namespace std::chrono_literals;
  using std::literals::string_literals::operator""s;

  struct mock_request_handler final : web_api::base_request_handler {
    bool throw_on_req = true;

    web_api::bg_work_result do_the_work(std::string const &input) override {
      if (throw_on_req) {
        throw std::runtime_error("ERROR");
      }
      return web_api::bg_work_result{fmt::format("Response to {}", input)};
    }

    web_api::bg_work_result do_subscription_work(web_api::observer_base_ const &) override {
      // unused
      return {};
    }
  };

  std::int64_t terminal_version_s = 1;

  struct test_subscription : core::subscription::observer_base {
    std::atomic_int n_threads_processing = 0;

    test_subscription(core::subscription::manager_ const &sm, std::string const &request_id)
      : core::subscription::observer_base(sm, request_id) {
    }

    std::int64_t terminal_version() const noexcept override {
      auto tv = ++terminal_version_s;
      // make sure we trigger every check:
      return tv;
    }

    bool recalculate() override {
      ++n_threads_processing;
      std::this_thread::sleep_for(20ms);
      if (n_threads_processing > 1) {
        FAIL("More then one thread is running the subscription at the same time");
      }
      --n_threads_processing;
      return true;
    }
  };

  struct request_handler_creates_subscription final : web_api::base_request_handler {
    core::subscription::manager_ manager;

    web_api::bg_work_result do_the_work([[maybe_unused]] std::string const &input) override {
      web_api::bg_work_result result;
      result.subscription = std::make_shared<test_subscription>(manager, "some_id");
      return result;
    }

    web_api::bg_work_result do_subscription_work(web_api::observer_base_ const &s) override {
      web_api::bg_work_result result;
      result.unsubscribe_id = "some_id";
      if (s->recalculate()) {
        result.copy_response("some_response");
      }
      return result;
    }
  };

  TEST_SUITE_BEGIN("web_api");

  TEST_CASE("web_api/throw_does_not_consume_bg_worker") {
    mock_request_handler h{};
    std::string host_ip{"127.0.0.1"};
    auto dirname = "em.web_api.test." + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();
    auto web_srv = std::async(std::launch::async, [&]() {
      return web_api::run_web_server(h, host_ip, 0, std::make_shared<std::string>(doc_root), 1, 1);
    });
    auto port = (int) h.wait_for_real_port();
    REQUIRE(web_srv.valid());
    std::string response;
    { // run once with throwing:
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<session>(ioc, false);
      s1->run(host_ip, port, "REQUEST", []([[maybe_unused]] std::string const &web_response) {
        return ""s;
      });
      // Run the I/O service. The call will return when
      // the socket is closed.
      ioc.run();
      s1.reset();
    }
    // turn off throwing:
    h.throw_on_req = false;
    { // run without throwing:
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<session>(ioc);
      s1->run(host_ip, port, "REQUEST", [&](std::string const &web_response) {
        response = web_response;
        return "";
      });
      // Run the I/O service. The call will return when
      // the socket is closed.
      ioc.run();
      s1.reset();
    }
    CHECK(response == "Response to REQUEST");
    // kills websocket:
    if (web_srv.valid()) {
      std::raise(SIGINT);
      (void) web_srv.get();
    }
  }

  TEST_CASE("web_api/subscription_handling_slow_no_rc") {
    request_handler_creates_subscription h{};
    std::string host_ip{"127.0.0.1"};
    auto dirname = "em.web_api.test." + std::to_string(core::to_seconds64(core::utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();

    auto web_srv = std::async(std::launch::async, [&]() {
      // two bg, so we can have race conditions:
      return web_api::run_web_server(
        h, host_ip, static_cast<unsigned short>(0), std::make_shared<std::string>(doc_root), 1, 20);
    });
    auto port = (int) h.wait_for_real_port();
    REQUIRE(web_srv.valid());
    std::string response;
    { // creates the sub:
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<session>(ioc, true);
      s1->run(host_ip, port, "REQUEST", [&](std::string const &web_response) {
        response = web_response;
        return ""s;
      });
      // Run the I/O service. The call will return when
      // the socket is closed.
      ioc.run();
      s1.reset();
    }
    CHECK(response == "some_response");
    // kills websocket:
    if (web_srv.valid()) {
      std::raise(SIGINT);
      (void) web_srv.get();
    }
  }

  TEST_SUITE_END();

}
