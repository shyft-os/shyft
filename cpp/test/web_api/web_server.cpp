#include <csignal>

#include <boost/asio/connect.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/buffers_to_string.hpp>
#include <boost/beast/core/multi_buffer.hpp>
#include <boost/beast/websocket.hpp>

#include <test/test_pch.h>
#include <test/test_utils.h>
#include <test/web_api/request_sender.h>
#include <test/web_api/test_server.h>

namespace shyft {
  namespace test { //

    namespace {
      using std::vector;
      using std::string;
      using std::string_view;

      using namespace shyft::dtss;
      using namespace shyft::core;
      using namespace shyft;

      //-- test client
      using tcp = boost::asio::ip::tcp;              // from <boost/asio/ip/tcp.hpp>
      namespace websocket = boost::beast::websocket; // from <boost/beast/websocket.hpp>
      using boost::system::error_code;

      /** engine that perform a publish-subscribe against as specified host
       *
       * Since this is async, I found it easier to just use that code-flow
       * and make a custom class that do the needed minimal work at high speed.
       */
      class pub_sub_session : public std::enable_shared_from_this<pub_sub_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        bool waiting_first_read = true;

        // Report a failure
        void fail(error_code ec, char const * what) {
          fail_ = string(what) + ": " + ec.message() + "\n";
        }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));
       public:
        // Resolver and socket require an io_context
        explicit pub_sub_session(boost::asio::io_context& ioc)
          : resolver_(ioc)
          , ws_(ioc) {
        }

        vector<string> responses_;

        string diagnostics() const {
          return fail_;
        }

        // Start the asynchronous operation
        void run(string_view host, int port) {
          // Save these for later
          host_ = host;
          port_ = std::to_string(port);
          resolver_.async_resolve(
            host_,
            port_, // Look up the domain name
            [me = shared_from_this()](error_code ec, tcp::resolver::results_type results) {
              me->on_resolve(ec, results);
            });
        }

        void on_resolve(error_code ec, tcp::resolver::results_type results) {
          fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(
              ws_.next_layer(),
              results.begin(),
              results.end(),
              // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
              std::bind(&pub_sub_session::on_connect, shared_from_this(), std::placeholders::_1));
        }

        void on_connect(error_code ec) {
          fail_on_error(ec, "connect") ws_.async_handshake(
            host_,
            "/", // Perform the websocket handshake
            [me = shared_from_this()](error_code ec) {
              me->store_first_ts(ec);
            });
        }

        void store_first_ts(error_code ec) {
          fail_on_error(ec, "store_first_ts") ws_.async_write( // start storing the first time-series
            boost::asio::buffer(
              R"_(store_ts {
                        "request_id"  : "1w",
                        "merge_store" : false,
                        "recreate_ts" : false,
                        "cache"       : true,
                        "tsv"         : [
                                        {
                                            "id": "shyft://test/a1",
                                            "pfx":true,
                                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                            "values": [1,2,3]
                                        }
                                        ,
                                        {
                                            "id": "shyft://test/a2",
                                            "pfx":false,
                                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                            "values": [4,5,6]
                                        }
                                    ]
                    })_"),
            [me = shared_from_this()](error_code /*ec*/, size_t /*bytes_transferred*/) {
              // fail_on_error(ec,"failed to send first write");
            });
          ws_.async_read(
            buffer_, // and also start read incoming messages
            [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
              me->on_read(ec, bytes_transferred);
            });
        }

        void send_read_and_subscribe(error_code ec, std::size_t bytes_transferred) {
          boost::ignore_unused(bytes_transferred);
          fail_on_error(ec, "send_read_and_subscribe")
            ws_.async_write( // once we have stored the time-series, we just follow up with subscribe_and_read
              boost::asio::buffer(R"_(read {
                                    "request_id": "subscribe_and_read",
                                    "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "clip_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "cache": true,
                                    "ts_ids":["shyft://test/a1","shyft://test/a2"],
                                    "subscribe": true,
                                    "ts_fmt": true
                                })_"),
              [me = shared_from_this()](error_code /*ec*/, size_t /*bytes_transferred*/) {
                // me->wait_for_first_read_response(ec,bytes_transferred);
              });
        }

        void on_read(error_code ec, std::size_t bytes_transferred) {
          boost::ignore_unused(bytes_transferred);
          fail_on_error(ec, "read") string response = boost::beast::buffers_to_string(buffer_.data());
          responses_.push_back(response);
          buffer_.consume(buffer_.size());
          // MESSAGE("got:"<<response);
          if (response.find("finale") != string::npos) {
            ws_.async_close(websocket::close_code::normal, [me = shared_from_this()](error_code ec) {
              me->on_close(ec);
            });
          } else {
            if (response.find("1w") != string::npos) {
              ws_.async_write( // once we have stored the time-series, we just follow up with subscribe_and_read
                boost::asio::buffer(R"_(read {
                                    "request_id": "subscribe_and_read",
                                    "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "clip_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "cache": true,
                                    "ts_ids":["shyft://test/a1","shyft://test/a2"],
                                    "subscribe": true,
                                    "ts_fmt":true
                                })_"),
                [me = shared_from_this()](error_code /*ec*/, size_t /*bytes_transferred*/) {
                  // maybe fail here if ec is not ok
                });
            } else if (response.find("subscribe_and_read") != string::npos) {
              if (waiting_first_read) { // first time we send an update to the time-series
                waiting_first_read = false;
                ws_.async_write( // send and update to the time-series, that should trigger a publish on this socket
                  boost::asio::buffer(
                    R"_(store_ts {
                                    "request_id"  : "2w",
                                    "merge_store" : false,
                                    "recreate_ts" : false,
                                    "cache"       : true,
                                    "tsv"         : [
                                                    {
                                                        "id": "shyft://test/a1",
                                                        "pfx":true,
                                                        "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                                        "values": [3,2,1]
                                                    }
                                                    ,
                                                    {
                                                        "id": "shyft://test/a2",
                                                        "pfx":false,
                                                        "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                                        "values": [6,5,4]
                                                    }
                                                ]
                                })_"),
                  [me = shared_from_this()](error_code /*ec*/, size_t /*bytes_transferred*/) {
                    // nothing to do here, just wait for the subscription update
                  });
              } else { // second time, we unsubscribe the read-request
                ws_.async_write( // when we receive the answer to this, finale, we close the socket.
                  boost::asio::buffer(
                    R"_(unsubscribe {"request_id":"finale","subscription_id":"subscribe_and_read"})_"),
                  [me = shared_from_this()](error_code /*ec*/, size_t /*bytes_transferred*/) {
                    // nothing to do here, just wait for the subscription update
                  });
              }
            }
            //--anyway, always continue to read (unless we hit the finale request-id sent with the unsubscribe message
            ws_.async_read(
              buffer_, // Read next message into our buffer
              [me = shared_from_this()](error_code ec, size_t bytes_transferred) {
                me->on_read(ec, bytes_transferred);
              });
          }
        }

        void on_close(error_code ec) {
          fail_on_error(ec, "close")
        }

#undef fail_on_error
      };
    } //
  }

  using namespace test;
  using std::string;

  TEST_SUITE_BEGIN("web_api");

  TEST_CASE("web_api/server_basics") {
    test_server a;
    string host_ip{"127.0.0.1"};
    utils::temp_dir tmp_dir("shyft.web_api.");
    string doc_root = (tmp_dir / "doc_root").string();
    string ts_root = (tmp_dir / "ts_root").string();
    add_container(a, "test", ts_root);
    a.can_remove = true;
    int port = a.start_web_api(host_ip, 0, doc_root, 1, 1);

    REQUIRE_EQ(true, a.web_api_running());

    vector<string> responses;
    vector<string> requests{
      R"_(info {"request_id":"1"})_",
      R"_(info {"request_id":"2"})_",
      R"_(store_ts {
                "request_id"  : "3",
                "merge_store" : false,
                "recreate_ts" : false,
                "cache"       : true,
                "tsv"         : [
                                {
                                    "id": "shyft://test/a1",
                                    "pfx":true,
                                    "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                    "values": [1,2,3]
                                }
                             ,
                                {
                                    "id": "shyft://test/a2",
                                    "pfx":false,
                                    "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                    "values": [4,5,6]
                                }
                            ]
            })_",
      R"_(find { "request_id": "4", "find_pattern": "shyft://test/a.*"})_",
      R"_(read {
            "request_id": "5",
            "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
            "clip_period": ["2018-01-01T01:00:00Z", "2018-01-01T03:00:00Z"],
            "cache": true,
            "ts_ids":["shyft://test/a1","shyft://test/a2"]
        })_",
      R"_(store_ts {
            "request_id"  : "6",
            "merge_store" : false,
            "recreate_ts" : false,
            "cache"       : true,
            "tsv"         : [
                            {
                                "id": "shyft://test/a1",
                                "pfx":true,
                                "time_axis": { "t0": "2018-01-01T00:30:00Z","dt": 3610,"n":3 },
                                "values": [1,2,3]
                            }
                            ,
                            {
                                "id": "shyft://test/a2",
                                "pfx":false,
                                "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3640,"n":3 },
                                "values": [4,5,6]
                            }
                        ]
        })_",
      R"_(store_ts {"request_id"  : "7","merge_store" : True, misspelled, should be true})_",
      R"_(storets {"requestid"  : "7","merge_store" : True, misspelled, should be true})_",
      R"_(remove {"request_id" : "8","ts_urls": ["shyft://test/a2"]})_",
      R"_(find { "request_id": "9", "find_pattern": "shyft://test/a2"})_",
      R"_(read {
        "request_id": "10",
        "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
        "clip_period": ["2018-01-01T01:00:00Z", "2018-01-01T03:00:00Z"],
        "cache": true,
        "ts_ids":["shyft://test/ax","shyft://test/a2"]
    })_",
      R"_(info {"request_id":"11"})_",


    };
    size_t r = 0;

    {
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<shyft::test::session>(ioc);
      s1->run(
        host_ip,
        port,
        requests[r], //
        [&](string const & web_response) {
          responses.push_back(web_response);
          ++r;
          return r >= requests.size() ? string("") : requests[r];
        });
      // Run the I/O service. The call will return when
      // the socket is closed.
      ioc.run();
      s1.reset();
    }
    a.stop_web_api();
    REQUIRE_GT(responses.size(), 0);
    CHECK_EQ(responses.size(), requests.size());
    vector<string> e{
      R"_({"request_id":"1","diagnostics":"not implemented"})_",
      R"_({"request_id":"2","diagnostics":"not implemented"})_",
      R"_({"request_id":"3","diagnostics":""})_",
      // R"_({"request_id":"4","result":[{"name":"a2","pfx":false,"delta_t":null,"olson_tz_id":"","data_period":[1546300800.0,1546311600.0],"created":null,"modified":1555932102.0},{"name":"a1","pfx":true,"delta_t":null,"olson_tz_id":"","data_period":[1514764800.0,1514775600.0],"created":null,"modified":1555932102.0}]})_",
      R"_()_", // response vary with the time we run the test
      R"_({"request_id":"5","tsv":[{"pfx":true,"data":[[1514768400.0,2],[1514772000.0,3]]},{"pfx":false,"data":[[1514768400.0,5],[1514772000.0,6]]}]})_",

      // R"_({ "request_id" : "6","diagnostics": "can not merge time-series" })_",
      R"_(cannot merge)_",
      R"_({"request_id":"7","diagnostics":"syntax error! expecting <boolean> here: \"True, misspelled, should be true}\"\n"})_",
      R"_(not understood:storets {"requestid"  : "7","merge_store" : True, misspelled, should be true})_",
      R"_({"request_id":"8","diagnostics":""})_",
      R"_({"request_id":"9","result":[]})_",
      R"_({"request_id":"10","diagnostics":"shyft-read failed to read)_",
      R"_({"request_id":"11","diagnostics":"not implemented"})_"};
    REQUIRE_EQ(e.size(), responses.size());
    for (size_t i = 0; i < e.size(); ++i) {
      if (e[i].size()) {
        if (i == 5 || i == 10) {
          // MESSAGE("Got response '"<<responses[i]<<"' expect to find"<<e[i]);
          CHECK_EQ(responses[i].find(e[i]) != std::string::npos, true);
        } else {
          CHECK_EQ(responses[i], e[i]);
        }
      } else { // only require response to be larger than 10
        CHECK_GT(responses[i].size(), 10);
      }
    }
  }

  TEST_CASE("stress/web_api/server") {
    test_server a;
    string host_ip{"127.0.0.1"};
    utils::temp_dir tmp_dir("shyft.web_api.stress.");
    string doc_root = (tmp_dir / "doc_root").string();
    string ts_root = (tmp_dir / "ts_root").string();
    add_container(a, "test", ts_root);
    int port = a.start_web_api(host_ip, 0, doc_root, 4, 4);
    REQUIRE_EQ(true, a.web_api_running());

    vector<string> responses;
    string store_ts_request =
      R"_(store_ts {
        "request_id"  : "3",
        "merge_store" : false,
        "recreate_ts" : false,
        "cache"       : true,
        "tsv"         : [
                        {
                            "id": "shyft://test/a1",
                            "pfx":true,
                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                            "values": [1,2,3]
                        }
                        ,
                        {
                            "id": "shyft://test/a2",
                            "pfx":false,
                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                            "values": [4,5,6]
                        }
                    ]
    })_";

    // string info_request=   R"_(info {"request_id":"1"})_";
    string find_request = R"_(find { "request_id": "4", "find_pattern": "shyft://test/a.*"})_";
    string read_request =
      R"_(read {
        "request_id": "5",
        "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
        "clip_period": ["2018-01-01T01:00:00Z", "2018-01-01T03:00:00Z"],
        "cache": true,
        "ts_ids":["shyft://test/a1","shyft://test/a2"]
    })_";
    size_t r = 0;
    size_t max_requests = 1000;
    auto t0 = timing::now();
    {
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<shyft::test::session>(ioc);
      s1->run(
        host_ip,
        port,
        store_ts_request, //
        [&](std::string const & web_response) {
          responses.push_back(web_response);
          ++r;
          if (r >= max_requests)
            return string("");                        //
          return r % 2 ? read_request : find_request; // alternate read/find request.
        });
      // Run the I/O service. The call will return when
      // the socket is closed.
      ioc.run();
      s1.reset();
    }
    auto t1 = timing::now();
    a.stop_web_api();
    REQUIRE_GT(responses.size(), 0);
    CHECK_EQ(responses.size(), max_requests);
    auto us = elapsed_us(t0, t1);
    MESSAGE(
      "web_api server " << max_requests << " done in " << double(us) / 1000.0 << "ms ~ "
                        << double(max_requests) / (double(us) / 1e3) << " kilo req/sec\n");
  }

  TEST_CASE("web_api/server_subscription") {
    test_server a;
    string host_ip{"127.0.0.1"};
    utils::temp_dir tmp_dir("shyft.web_api.sub.");
    string doc_root = (tmp_dir / "doc_root").string();
    string ts_root = (tmp_dir / "ts_root").string();
    add_container(a, "test", ts_root);
    int port = a.start_web_api(host_ip, 0, doc_root, 4, 4);
    REQUIRE_EQ(true, a.web_api_running());
    boost::asio::io_context ioc;
    auto s1 = std::make_shared<pub_sub_session>(ioc);
    s1->run(host_ip, port);
    ioc.run();
    vector<string> expected_responses{
      string(R"_({"request_id":"1w","diagnostics":""})_"),
      string(
        R"_({"request_id":"subscribe_and_read","tsv":[{"id":"","pfx":true,"time_axis":{"t0":1514764800.0,"dt":3600.0,"n":3},"values":[1,2,3]},{"id":"","pfx":false,"time_axis":{"t0":1514764800.0,"dt":3600.0,"n":3},"values":[4,5,6]}]})_"),
      string(R"_({"request_id":"2w","diagnostics":""})_"),
      string(
        R"_({"request_id":"subscribe_and_read","tsv":[{"id":"","pfx":true,"time_axis":{"t0":1514764800.0,"dt":3600.0,"n":3},"values":[3,2,1]},{"id":"","pfx":false,"time_axis":{"t0":1514764800.0,"dt":3600.0,"n":3},"values":[6,5,4]}]})_"),
      string(R"_({"request_id":"finale","diagnostics":""})_")};
    auto responses = s1->responses_;
    for (auto i = 0u; i < responses.size(); ++i)
      REQUIRE_EQ(responses[i], expected_responses[i]);
    // REQUIRE_EQ(responses,expected_responses);
    s1.reset();
    a.stop_web_api();
  }

  TEST_SUITE_END();

}
