#pragma once

#include <shyft/mp/parser_utils.h>

namespace shyft::test {
  using shyft::mp::grammar::phrase_parser; // just redirect to the parser used everywhere
}
