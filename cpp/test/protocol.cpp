#include <cstdint>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

#include <boost/mp11/algorithm.hpp>
#include <boost/serialization/string.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/core/boost_wrapped_binary_archive.h>
#include <shyft/core/protocol.h>

#include <doctest/doctest.h>

namespace shyft {

  namespace model {
#define MESSAGES (a, b)

    SHYFT_DEFINE_ENUM(message_tag, std::uint8_t, MESSAGES)
    inline constexpr auto test_protocol = protocols::basic_protocol<message_tag>{};
  }

  namespace protocols {
    template <auto msg>
    requires(msg.protocol == model::test_protocol)
    struct request<msg> {
      std::string data;

      SHYFT_DEFINE_STRUCT(request, (), (data));
      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg.protocol == model::test_protocol)
    struct reply<msg> {

      std::string data;

      SHYFT_DEFINE_STRUCT(reply, (), (data));
      auto operator<=>(reply const &) const = default;
    };

    template <auto message>
    requires(message.protocol == model::test_protocol)
    void serialize(auto &archive, reply<message> &m, unsigned int const v) {
      using archive_type=std::decay_t<decltype(archive)>;
      if constexpr (archive_type::mode == core::serialization_choice::all)
        archive & m.data;
    }

    template <auto message>
    requires(message.protocol == model::test_protocol)
    void serialize(auto &archive, request<message> &m, unsigned int const v) {
      using archive_type=std::decay_t<decltype(archive)>;
      if constexpr (archive_type::mode == core::serialization_choice::all)
        archive & m.data;
    }

    template <auto msg>
    requires(msg.tag == model::message_tag::b)
    struct protocol_archives<msg> {
      static auto make_oarchive(std::ostream &stream) {
        return core::core_oarchive_stripped{stream, core::core_arch_flags};
      }

      static auto make_iarchive(std::istream &stream) {
        return core::core_iarchive_stripped{stream, core::core_arch_flags};
      }
    };
  }

  namespace model {
#define SHYFT_LAMBDA(r, data, elem) \
  using elem##_request = protocols::request<protocols::message<model::test_protocol>{model::message_tag::elem}>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(MESSAGES))
#undef SHYFT_LAMBDA

#define SHYFT_LAMBDA(r, data, elem) \
  using elem##_reply = protocols::reply<protocols::message<model::test_protocol>{model::message_tag::elem}>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(MESSAGES))
#undef SHYFT_LAMBDA

    template <model::message_tag t>
    using test_request = protocols::request<protocols::message<model::test_protocol>{t}>;
    template <model::message_tag t>
    using test_reply = protocols::reply<protocols::message<model::test_protocol>{t}>;

    using any_request = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, test_request>>;
    using any_reply = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, test_reply>>;

    struct server_dispatch {
      std::vector<any_request> requests;
      std::vector<any_reply> replies;

      struct handler {
        server_dispatch &dispatch;

        template <auto M>
        auto operator()(protocols::request<M> r) {
          dispatch.requests.push_back({r});
          protocols::reply<M> s{r.data};
          dispatch.replies.push_back(s);
          return s;
        }

        void log(dlib::log_level, std::string_view) const {
        }
      };

      handler operator()(std::string const &, unsigned short) & {
        return {*this};
      }
    };

    using client = protocols::basic_client<>;
    using server = protocols::basic_server<test_protocol, server_dispatch>;

  }


}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::model::message_tag);

namespace shyft::model {
#define SHYFT_LAMBDA(r, data, elem) \
  elem##_reply elem(client &c, elem##_request request) { \
    return c.send(std::move(request)); \
  }
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(MESSAGES))
#undef SHYFT_LAMBDA
}

namespace shyft {
  TEST_SUITE_BEGIN("protocol");

  TEST_CASE("protocol") {
    model::server server;
    constexpr auto server_ip = "127.0.0.1";
    server.set_listening_ip(server_ip);
    auto server_port = server.start_server();

    model::client c{.connection=core::srv_connection{fmt::format("{}:{}", server_ip, server_port)}};

    model::a(c, {"ok1"});
    model::b(c, {"ok2"});
    model::a(c, {"ok3"});

    std::vector<model::any_request> expected_requests{
      model::a_request{"ok1"}, model::b_request{}, model::a_request{"ok3"}};
    std::vector<model::any_reply> expected_replies{model::a_reply{"ok1"}, model::b_reply{}, model::a_reply{"ok3"}};

    CHECK((server.dispatch.requests == expected_requests));
    CHECK((server.dispatch.replies == expected_replies));
  }

  TEST_SUITE_END();

}
