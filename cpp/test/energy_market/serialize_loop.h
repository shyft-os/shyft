#pragma once
#include <boost/serialization/serialization.hpp>

#include <shyft/core/core_archive.h>

//-- notice that boost serialization require us to
//   include shared_ptr/vector .. etc.. wherever it's needed

#include <sstream>
#include <string>

#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/weak_ptr.hpp>

namespace shyft::test {
  template <class T>
  static T serialize_loop(T const & o) {
    std::ostringstream xmls(std::ios::binary);
    /* scope this, ensure archive is flushed/destroyd */ {
      core::core_oarchive oa(xmls,core::arch_info_flags);
      oa << BOOST_SERIALIZATION_NVP(o);
    }
    xmls.flush();
    std::string ss = xmls.str();

    T o2;
    std::istringstream xmli(ss,std::ios::binary);
    {
      core::core_iarchive ia(xmli,core::arch_info_flags);
      ia >> BOOST_SERIALIZATION_NVP(o2);
    }
    return o2;
  }
}
