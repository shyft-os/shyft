#include <shyft/srv/client.h>
#include <shyft/srv/db.h>
#include <shyft/srv/db_rocks.h>
#include <shyft/srv/server.h>

#include <doctest/doctest.h>
#include <energy_market/build_test_model.h>
#include <test/test_utils.h>

namespace shyft {

  using std::to_string;
  using std::string;
  using std::vector;
  using core::utctime;
  using core::utctime_now;
  using core::utcperiod;
  using core::to_seconds64;
  using core::from_seconds;

  TEST_SUITE_BEGIN("em");

  //-- this is what we use and test:
  using srv::model_info;
  using srv::db;
  using srv::db_rocks;
  using srv::server;
  using srv::client;
  using energy_market::market::model;

  //-- keep test directory, unique, and with auto-cleanup.
  auto dirname = "srv.db.test." + to_string(to_seconds64(utctime_now()));
  test::utils::temp_dir tmpdir(dirname.c_str());

  //-- now ready for testing:


  TEST_CASE("em/server/rocksdb") {
    //
    // 0. test we can create a db
    db<model> dbm{tmpdir.string() + "rocks_db"};
    vector<int64_t> mids;

    CHECK_EQ(0u, dbm.get_model_infos(mids).size());

    // 1. test we can store one model into db
    auto m = test::build_model();
    m->id = 0; // create new model, require 0 here
    model_info mi{0, "name", from_seconds(7200), "{a:'key',b:1.23}"};
    auto mid = dbm.store_model(m, mi);
    CHECK_GE(1u, mid);
    // update mi&m to new id: ref todo in db.h, we might change the db-layer to mutable mi and m, to avoid this
    mi.id = mid;
    m->id = mid;
    // verify model-info is there
    auto mis = dbm.get_model_infos(mids);
    CHECK_EQ(1u, mis.size());
    CHECK_EQ(mi, mis[0]);
    vector<int64_t> midv{mid};

    // Get_model_infos with created_in period:
    auto mis2 = dbm.get_model_infos(mids, utcperiod(0, 10000));
    CHECK_EQ(1u, mis2.size());
    CHECK_EQ(mi, mis2[0]);
    mis2 = dbm.get_model_infos(midv, utcperiod(0, 10000)); // also test with several in vector.
    CHECK_EQ(1u, mis2.size());
    CHECK_EQ(mi, mis2[0]);
    dbm.io.flush_cache();                                  // evict items, force reread.
    mis2 = dbm.get_model_infos(midv, utcperiod(0, 10000)); // also test with several in empty cache case.
    CHECK_EQ(1u, mis2.size());
    CHECK_EQ(mi, mis2[0]);

    mis2 = dbm.get_model_infos(mids, utcperiod(10000, 20000));
    CHECK_EQ(0u, mis2.size());

    mis = dbm.get_model_infos(midv);
    CHECK_EQ(1u, mis.size());
    CHECK_EQ(mi, mis[0]);
    CHECK_EQ(1u, dbm.find_max_model_id()); // verify we correctly can find max model-id
    // and that we can update it:
    mi.json = string("{Updated info}");
    dbm.update_model_info(mid, mi);

    mis = dbm.get_model_infos(midv);
    CHECK_EQ(mi, mis[0]); // verify we now are aligned with the new stored info

    // and that we can get back the model
    auto mr = dbm.read_model(mid);
    CHECK_UNARY(mr != nullptr);
    CHECK_UNARY(*mr == *m);
    //
    // store another model, and check it can handle more than one:
    auto m2 = test::build_model();
    m2->id = 0;
    model_info mi2 = {0, "name2", utctime_now(), "{b:'vv',b:3.21}"};
    auto mid2 = dbm.store_model(m2, mi2);
    // verify model-infos and models are available
    mi2.id = mid2;
    m2->id = mid2;
    CHECK_EQ(2u, dbm.find_max_model_id()); // verify we correctly can find max model-id

    mis = dbm.get_model_infos(mids);
    CHECK_EQ(2u, mis.size());
    auto ix2 = mis[0].id == mid2 ? 0 : 1;
    CHECK_EQ(mi2, mis[ix2]);

    auto mr2 = dbm.read_model(mid2);
    CHECK_NE(*mr2, *mr);
    CHECK_EQ(*mr2, *m2);

    // remove model
    dbm.remove_model(mid);

    // verify we got just one model left
    mis = dbm.get_model_infos(mids);
    REQUIRE_EQ(1u, mis.size());
    CHECK_EQ(mi2, mis[0]); // and that we got the right model left
    CHECK_EQ(
      2u, dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 2 since we removed 1'

    // remove last model..
    dbm.remove_model(mid2);
    mis = dbm.get_model_infos(mids);
    REQUIRE_EQ(0u, mis.size());
    CHECK_EQ(
      0u,
      dbm.find_max_model_id()); // verify we correctly can find max model-id, it's still 0 since we removed all models
  }

  TEST_CASE("em/server/io_rocksdb") {
    using srv_t = server<db<model>>;
    using client_t = client<model>;
    //
    // 0. arrange with a server and a client running on localhost, tempdir.
    //
    srv_t s0(tmpdir.string() + "rocks_db2");
    string host_ip = "127.0.0.1";
    s0.set_listening_ip(host_ip);
    auto s_port = s0.start_server();
    CHECK_GE(s_port, 1024);
    client_t c{host_ip + ":" + to_string(s_port)};
    vector<int64_t> mids;
    auto mis = c.get_model_infos(mids);
    CHECK_EQ(mis.size(), 0);
    //
    // 1.  arrange with one model
    //
    auto m = test::build_model();
    model_info mi{0, "name2", from_seconds(600), "{b:'vv',b:3.21}"};
    m->id = 0;

    auto mid = c.store_model(m, mi);
    CHECK_EQ(mid, 1u);
    //
    // 2. with one model, verify minfo and m is ok
    //
    m->id = mid;
    mi.id = mid;
    auto mr = c.read_model(mid);
    CHECK_EQ(*mr, *m);
    mis = c.get_model_infos(mids);
    CHECK_EQ(mis.size(), 1u);
    CHECK_EQ(mi, mis[0]);

    auto mis2 = c.get_model_infos(mids, utcperiod(0, 1000));
    CHECK_EQ(mis2.size(), 1u);
    CHECK_EQ(mis2[0], mi);
    mis2 = c.get_model_infos(mids, utcperiod(1000, 2000));
    CHECK_EQ(mis2.size(), 0u);
    c.close(); // just to force auto-open on next call
    //
    // 3. check we can update minfo of existing model
    //
    mi.json = string("{something:else}");
    c.update_model_info(mi.id, mi);
    mis = c.get_model_infos(mids);
    CHECK_EQ(mis.size(), 1u);
    CHECK_EQ(mi, mis[0]);

    //
    // 4. finally, remove the model, and verify we are back to zero
    //
    c.remove_model(mid);
    mis = c.get_model_infos(mids);
    CHECK_EQ(mis.size(), 0);
    c.close(); // should work here
    s0.clear();
  }

  TEST_SUITE_END();

}
