#include <csignal>
#include <memory>
#include <vector>

#include <boost/asio/io_context.hpp>

#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/web_api/ui/request_handler.h>

#include "test_pch.h"
#include <test/energy_market/ui/layout_info_grammar.h>
#include <test/energy_market/ui/ui_test_server.h>
#include <test/test_utils.h>

namespace shyft {

  using std::make_shared;
  using std::vector;
  using std::string;
  using std::string_view;
  using std::to_string;

  using shyft::core::utctime;
  using shyft::core::utctime_now;
  using shyft::core::to_seconds64;

  namespace {
    static string generate_json_api(string const &layout_name, string const &) {
      if (layout_name == "test_ok") {
        return R"_({"key1": "value1", "key2": [4,5,6] })_";
      } else if (layout_name == "test_not_ok") {
        return "invalid json string";
      } else
        return "";
    }
  }

  using test_server = test::ui::server;
  using session = test::ui::session;
  using boost::system::error_code;

  using shyft::srv::model_info;

  using namespace shyft::energy_market::ui;
  using namespace shyft::web_api::grammar;
  using namespace test::ui;

  TEST_SUITE_BEGIN("em/ui");

  TEST_CASE("em/ui/grammar_stuff") {
    layout_info a = parse_layout_info(R"_({"layout_id":3,"name":"x"})_");
    layout_info b{3, "x", ""};
    CHECK_EQ(a, b);
  }

  TEST_CASE("em/ui/layout_response") {
    layout_info_response a = parse_layout_info_response(
      R"_({"request_id":"a","result": [{"layout_id":3,"name":"x"},{"layout_id":2,"name":"y"}]})_");
    layout_info_response b{
      "a", vector<layout_info>{layout_info{3, "x", ""}, layout_info{2, "y", ""}}
    };
    CHECK_EQ(a, b);
    a = parse_layout_info_response(R"_({"request_id":"a","result": [{"layout_id":2,"name":"y"}]})_");
    CHECK_EQ(a, layout_info_response{"a", vector<layout_info>{layout_info{2, "y", ""}}});
    a = parse_layout_info_response(R"_({"request_id":"a","result": []})_");
    CHECK_EQ(a, layout_info_response{"a", vector<layout_info>{}});
  }

  TEST_CASE("em/ui/store/extract_json_unmodified") {
    using shyft::web_api::ui::request_handler;
    CHECK_EQ(std::string_view("{}"), request_handler::extract_json_part(R"_({"some":{},"json" : {}})_"));
    CHECK_EQ(
      std::string_view("{\"a\":null,\"b\":[{},null]}"),
      request_handler::extract_json_part(R"_({"sjson":{},"json":{"a":null,"b":[{},null]}})_"));
    CHECK_THROWS(request_handler::extract_json_part(R"_({"some":{},"json"; {}})_")); // miss :
    CHECK_THROWS(request_handler::extract_json_part(R"_({"some":{},"jzon": {}})_")); // miss kw
    CHECK_THROWS(request_handler::extract_json_part(R"_({"some":{},"json": {{{)_")); // miss }
  }

  TEST_CASE("em/ui/basic_requests") {
    auto dirname = "ui.web_api.test." + to_string(to_seconds64(utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    string doc_root = (tmpdir / "doc_root").string();

    // Set up test server
    test_server a(tmpdir.string());
    string host_ip{"127.0.0.1"};
    a.set_listening_ip(host_ip);
    a.set_read_cb(generate_json_api);
    a.start_server();
    // Store some models:
    auto li1 = make_shared<layout_info>(1, "li1", R"_({"a":"some info","b":[1,2,3]})_");
    model_info mi1(li1->id, li1->name, utctime_now(), "");
    a.db_store_model(li1, mi1);

    auto li2 = make_shared<layout_info>(2, "li2", R"_({"c": "layoutson", "d": false})_");
    model_info mi2(li2->id, li2->name, utctime_now(), "");
    a.db_store_model(li2, mi2);

    auto port = a.start_web_api(host_ip, 0, doc_root, 1, 1);
    REQUIRE_EQ(true, a.web_api_running());

    vector<string> requests{
      R"_(get_layouts {"request_id":"1"})_",
      R"_(read_layout {"request_id":"2", "layout_id": 1})_",
      R"_(read_layout {"request_id":"3", "layout_id": 0, "name": "test_not_ok"})_",
      R"_(read_layout {"request_id":"4", "layout_id": 0, "name": "test_ok", "store": true})_",
      R"_(get_layouts {"request_id":"5"})_",
      R"_(store_layout {"request_id": "6","layout_id":6,"name": "my_layout","json":{"foo": true,"option":null,"object":{"a":{},"b":3.2}}})_"};

    vector<string> responses;
    size_t r = 0;
    {
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<session>(ioc);
      s1->run(host_ip, int(port), requests[r], [&responses, &r, &requests](string const &web_response) -> string {
        responses.push_back(web_response);
        ++r;
        auto next_req = r >= requests.size() ? string("") : requests[r];
        // MESSAGE("Got:"<<web_response);
        // MESSAGE("next is"<<next_req);
        return next_req;
      });
      ioc.run();
      s1.reset();
    }

    // verify the results

    REQUIRE_EQ(requests.size(), responses.size());
    // make life easier here:
    auto find_response = [&](string request_id) {
      for (auto const &r : responses)
        if (r.find(request_id) != string::npos)
          return r;
      CHECK(request_id == " not found ");
      return string("");
    };
    auto check_layout_reponse = [&](string request_id, layout_info_response const &x) -> void {
      auto rx1 = find_response(request_id);
      auto r1 = parse_layout_info_response(rx1);
      CHECK_EQ(r1, x);
    };
    auto check_string_reponse = [&](string request_id, string const &x) -> void {
      auto rx1 = find_response(request_id);
      CHECK_EQ(rx1, x);
    };

    check_layout_reponse(
      R"_("1")_",
      layout_info_response{
        "1", vector<layout_info>{layout_info{1, "li1", ""}, layout_info{2, "li2", ""}}
    });
    check_string_reponse(
      R"_("2")_",
      R"_({"request_id":"2","result":{"layout_id":1,"name":"li1","layout":{"a":"some info","b":[1,2,3]}}})_");
    check_string_reponse(
      R"_("3")_",
      R"_({"request_id":"3","result":{"layout_id":-1,"name":"test_not_ok","layout":invalid json string}})_");
    check_string_reponse(
      R"_("4")_",
      R"_({"request_id":"4","result":{"layout_id":3,"name":"test_ok","layout":{"key1": "value1", "key2": [4,5,6] }}})_");
    check_layout_reponse(
      R"_("5")_",
      layout_info_response{
        "5", vector<layout_info>{layout_info{1, "li1", ""}, layout_info{2, "li2", ""}, layout_info{3, "test_ok", ""}}
    });
    check_string_reponse(R"_("6")_", R"_({"request_id":"6","result":true})_");

    auto stored_mdl_request = a.db_read_model(6);
    CHECK_EQ(stored_mdl_request->id, 6);
    CHECK_EQ(stored_mdl_request->name, std::string{"my_layout"});
    CHECK_EQ(stored_mdl_request->json, std::string{R"_({"foo": true,"option":null,"object":{"a":{},"b":3.2}})_"});
    a.stop_web_api();
  }

  TEST_SUITE_END();

}
