#include <memory>
#include <string>
#include <string_view>

#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/url_parse.h>
#include <shyft/energy_market/stm/urls.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/build_test_system.h>
#include <test/energy_market/stm/utilities.h>

namespace shyft::energy_market::stm {

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/urls") {

    static_assert(url_max_path_length(identity_v<stm_system>) == 3ul);
    auto model = std::make_shared<stm_system>(0, "0", "0");

    static_assert(url_step_type<&stm_system::hps>() != url_step_type_null);

    CHECK(url_fmt_root("abc") == "dstm://Mabc/");
    CHECK(url_peek_model_id("a").empty());
    CHECK(url_peek_model_id("aabbccddeeff").empty());
    CHECK(url_peek_model_id("xgbl://Mm4/G0.fdd").empty());
    CHECK(url_peek_model_id("dstm://Mm4") == std::string_view{"m4"});
    CHECK(url_peek_model_id(url_format("m4", {}, "xx")) == std::string_view{"m4"});

    SUBCASE("hps") {

      static_assert(url_max_path_length(identity_v<reservoir>) == 0ul);
      static_assert(url_max_path_length(identity_v<stm_hps>) == 2ul);
      static_assert(url_max_path_length(identity_v<waterway>) == 1ul);

      static_assert(url_step_type<&stm_system::hps>() != url_step_type_null);
      static_assert(url_step_type<&waterway::gates>() != url_step_type_null);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto reservoir = std::make_shared<stm::reservoir>(20, std::string{}, std::string{}, hps);
      auto gate = std::make_shared<stm::gate>(40, std::string{}, std::string{});
      auto waterway = std::make_shared<stm::waterway>(30, std::string{}, std::string{}, hps);
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);
      auto power_plant = std::make_shared<stm::power_plant>(60, std::string{}, std::string{}, hps);
      auto reservoir_aggregate = std::make_shared<stm::reservoir_aggregate>(70, std::string{}, std::string{}, hps);
      auto catchment = std::make_shared<stm::catchment>(80, std::string{}, std::string{}, hps);

      waterway->gates.push_back(gate);

      model->hps.push_back(hps);

      hps->reservoirs.push_back(reservoir);
      hps->waterways.push_back(waterway);
      hps->units.push_back(unit);
      hps->power_plants.push_back(power_plant);
      hps->reservoir_aggregates.push_back(reservoir_aggregate);
      hps->catchments.push_back(catchment);

      auto const H_step = url_make_step<&stm_system::hps>(hps->id);
      auto const H_misstep = url_make_step<&stm_system::hps>(hps->id + 1);
      auto const R_step = url_make_step<&stm_hps::reservoirs>(reservoir->id);
      auto const R_misstep = url_make_step<&stm_hps::reservoirs>(reservoir->id - 1);
      url_step const r_misstep{'r', 0};
      auto const W_step = url_make_step<&stm_hps::waterways>(waterway->id);
      auto const W_misstep = url_make_step<&stm_hps::waterways>(waterway->id - 2);
      auto const G_step = url_make_step<&waterway::gates>(gate->id);
      auto const G_misstep = url_make_step<&waterway::gates>(gate->id - 3);
      auto const G_mislink = url_make_step<&stm_hps::gates>(gate->id - 3);
      auto const U_step = url_make_step<&stm_hps::units>(unit->id);
      auto const U_misstep = url_make_step<&stm_hps::units>(0);
      auto const P_step = url_make_step<&stm_hps::power_plants>(power_plant->id);
      auto const P_misstep = url_make_step<&stm_hps::power_plants>(0);
      auto const C_step = url_make_step<&stm_hps::catchments>(catchment->id);
      auto const C_misstep = url_make_step<&stm_hps::catchments>(catchment->id + 9);
      auto const A_step = url_make_step<&stm_hps::reservoir_aggregates>(reservoir_aggregate->id);
      auto const A_misstep = url_make_step<&stm_hps::reservoir_aggregates>(reservoir_aggregate->id + 1);


      {
        auto was_invalid = url_with_path(
          model,
          {
            {.tp = url_max_path_length(identity_v<stm_system>), .id = hps->id}
        },
          [&](auto const &...) {
            return false;
          },
          [&](auto const &) {
            return true;
          });
        CHECK(was_invalid);
      }

      CHECK(url_check_subcomp<stm_hps>(*model, H_step));
      CHECK(!url_check_subcomp<stm_hps>(*model, H_misstep));
      CHECK(url_check_subcomp<stm::reservoir>(*hps, R_step));
      CHECK(url_check_subcomp<stm::waterway>(*hps, W_step));
      CHECK(url_check_subcomp<stm::reservoir const>((stm_hps const &) *hps, R_step));
      CHECK(!url_check_subcomp<stm::waterway>(*hps, W_misstep));
      CHECK(!url_check_subcomp<stm::unit>(*hps, R_step));
      CHECK(url_check_subcomp<stm::unit>(*hps, U_step));
      CHECK(!url_check_subcomp<stm::unit>(*hps, U_misstep));
      CHECK(!url_check_subcomp<stm::reservoir>(*hps, R_misstep));
      CHECK(!url_check_subcomp<stm::reservoir>(*hps, r_misstep));

      CHECK(url_check_path<stm::reservoir>(*hps, {R_step}));
      CHECK(url_check_path<stm::waterway>(*hps, {W_step}));
      CHECK(url_check_path<stm::gate>(*hps, {W_step, G_step}));
      CHECK(!url_check_path<stm::gate>(*hps, {W_step, G_misstep}));
      CHECK(url_check_path<stm::gate>(*hps, {G_step}));
      CHECK(!url_check_path<stm::gate>(*hps, {G_mislink}));
      CHECK(url_check_path<stm_hps>(*model, {H_step}));
      CHECK(url_check_path<stm::waterway const>((stm_system const &) *model, {H_step, W_step}));
      CHECK(!url_check_path<stm_hps>(*model, {H_step, H_step}));
      CHECK(url_check_path<stm::reservoir>(*model, {H_step, R_step}));
      CHECK(url_check_path<stm::reservoir const>((stm_system const &) *model, {H_step, R_step}));
      CHECK(url_check_path<stm::unit>(*model, {H_step, U_step}));
      CHECK(!url_check_path<stm::unit>(*model, {H_step, U_misstep}));
      CHECK(url_check_path<stm::power_plant>(*model, {H_step, P_step}));
      CHECK(!url_check_path<stm::power_plant>(*model, {H_step, P_misstep}));
      CHECK(url_check_path<stm::catchment>(*model, {H_step, C_step}));
      CHECK(!url_check_path<stm::catchment>(*model, {H_step, C_misstep}));
      CHECK(url_check_path<stm::reservoir_aggregate>(*model, {H_step, A_step}));
      CHECK(!url_check_path<stm::reservoir_aggregate>(*model, {H_step, A_misstep}));

      CHECK(url_check_attr<time_series::dd::apoint_ts>(*reservoir, "level.regulation_min"));
      CHECK(url_check_attr<time_series::dd::apoint_ts const>(
        (stm::reservoir const &) *reservoir, "volume.constraint.tactical.min.penalty"));
      CHECK(url_check_attr<time_series::dd::apoint_ts>(*reservoir, "level.constraint.min"));

      CHECK(url_check_attr<time_series::dd::apoint_ts>(*power_plant, "fees.feeding_fee"));

      reservoir->tsm["constraint.min"] = apoint_ts{};
      reservoir->custom["my.attribute"] = apoint_ts{};
      CHECK(url_check_attr<time_series::dd::apoint_ts>(*reservoir, "ts.constraint.min"));
      CHECK(url_check_attr<any_attr>(*reservoir, "custom.my.attrtibute"));
      CHECK(!url_check_attr<time_series::dd::apoint_ts>(
        *(stm::reservoir const *) reservoir.get(), "ts.constraint.max")); // notice const here
      CHECK(url_check_attr<time_series::dd::apoint_ts>(
        *reservoir, "ts.constraint.max")); // notice non const, will create the entry


      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {H_step, R_step}, "xlevel.schedule"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, R_step}, "volume.slack.lower"));
      CHECK(url_check<time_series::dd::apoint_ts const>(
        (stm_system const &) *model, {H_step, R_step}, "volume.slack.lower"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, U_step}, "discharge.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, G_step}, "discharge.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, W_step, G_step}, "discharge.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, U_step}, "discharge.constraint.min"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, W_step}, "discharge.static_max"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {H_step, W_step}, "discharge.result"));
    }

    SUBCASE("unit_group") {

      static_assert(url_max_path_length(identity_v<unit_group>) == 1ul);
      static_assert(url_max_path_length(identity_v<unit_group_member>) == 0ul);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);

      hps->units.push_back(unit);
      model->hps.push_back(hps);

      auto unit_group = std::make_shared<stm::unit_group>(model.get());
      auto unit_group_member = std::make_shared<stm::unit_group_member>(unit_group.get(), unit, apoint_ts{});

      auto const u_step = url_make_step<&stm_system::unit_groups>(unit_group->id);
      auto const u_misstep = url_make_step<&stm_system::unit_groups>(unit_group->id + 1);
      auto const M_step = url_make_step<&unit_group::members>(unit_group_member->id());
      auto const M_misstep = url_make_step<&unit_group::members>(unit_group_member->id() + 1);

      unit_group->members.push_back(unit_group_member);
      model->unit_groups.push_back(unit_group);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {u_step}, "obligation.cost"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {u_step}, "obligation.schedule"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step}, "obligation.scheduxxxle"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_misstep}, "obligation.cost"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {u_step, M_step}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step, M_step}, "actixxxve"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step, M_misstep}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {u_step, M_misstep}, "active"));
    }

    SUBCASE("market_area") {

      static_assert(url_max_path_length(identity_v<energy_market_area>) == 0ul);

      auto market = std::make_shared<energy_market_area>(2, "", "", model);

      model->market.push_back(market);

      auto const m_step = url_make_step<&stm_system::market>(market->id);
      auto const m_misstep = url_make_step<&stm_system::market>(market->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {m_step}, "price"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_step}, "psxxrixce"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_misstep}, "price"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {m_step}, "ts.price-alternative"));
    }
    SUBCASE("contract") {

      static_assert(url_max_path_length(identity_v<contract>) == 0ul);

      auto contract = std::make_shared<stm::contract>(4, "", "", model);

      model->contracts.push_back(contract);

      auto const c_step = url_make_step<&stm_system::contracts>(contract->id);
      auto const c_misstep = url_make_step<&stm_system::contracts>(contract->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {c_step}, "revenue"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {c_step}, "quantity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {c_step}, "rexxevenuex"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {c_misstep}, "revenue"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, "dstm://M0/c4.quantity"));
      CHECK(!url_check<std::string>(*model, "dstm://M0/c4.quantity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, "dstm://M0/c4.parent_id"));
      CHECK(url_check<std::string>(*model, "dstm://M0/c4.parent_id"));

      {
        auto ok = url_invoke(
          *model,
          "dstm://M0/c4.parent_id",
          []<typename T>(T const &) {
            return std::is_same_v<T, std::string>;
          },
          [] {
            return false;
          });
        CHECK(ok);
      }
      {
        auto ok = url_invoke<std::string>(
          *model,
          "dstm://M0/c4.parent_id",
          [](std::string const &) {
            return true;
          },
          [] {
            return false;
          });
        CHECK(ok);
      }
      {
        auto ok = url_invoke<time_series::dd::apoint_ts>(
          *model,
          "dstm://M0/c4.parent_id",
          [](time_series::dd::apoint_ts const &) {
            return false;
          },
          [] {
            return true;
          });
        CHECK(ok);
      }
      CHECK(url_get_ts(*model, "dstm://M0/c4.quantity"));
      CHECK(!url_get_ts(*model, "dstm://M0/c4.parent_id"));
    }
    SUBCASE("contract_portfolio") {

      static_assert(url_max_path_length(identity_v<contract_portfolio>) == 0ul);

      auto contract_portfolio = std::make_shared<stm::contract_portfolio>(5, "", "", model);

      model->contract_portfolios.push_back(contract_portfolio);

      auto const p_step = url_make_step<&stm_system::contract_portfolios>(contract_portfolio->id);
      auto const p_misstep = url_make_step<&stm_system::contract_portfolios>(contract_portfolio->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {p_step}, "revenue"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {p_step}, "quantity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {p_step}, "rexxevenuex"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {p_misstep}, "quantity"));
    }
    SUBCASE("network") {

      static_assert(url_max_path_length(identity_v<network>) == 2ul);
      static_assert(url_max_path_length(identity_v<busbar>) == 1ul);
      static_assert(url_max_path_length(identity_v<transmission_line>) == 0ul);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);

      hps->units.push_back(unit);
      model->hps.push_back(hps);

      auto network = std::make_shared<stm::network>(6, "", "", model);

      auto power_module = std::make_shared<stm::power_module>(10, "", "", model);
      auto busbar = std::make_shared<stm::busbar>(8, "", "", network);

      model->power_modules.push_back(power_module);

      auto power_module_member = std::make_shared<stm::power_module_member>(busbar.get(), power_module, apoint_ts{});
      busbar->power_modules.push_back(power_module_member);

      auto unit_member = std::make_shared<stm::unit_member>(busbar.get(), unit, apoint_ts{});
      busbar->units.push_back(unit_member);

      auto transmission_line = std::make_shared<stm::transmission_line>(9, "", "", network);

      network->busbars.push_back(busbar);
      network->transmission_lines.push_back(transmission_line);

      model->networks.push_back(network);

      auto const n_step = url_make_step<&stm_system::networks>(network->id);
      auto const n_misstep = url_make_step<&stm_system::networks>(network->id + 1);
      auto const t_step = url_make_step<&network::transmission_lines>(transmission_line->id);
      auto const t_misstep = url_make_step<&network::transmission_lines>(transmission_line->id + 1);
      auto const b_step = url_make_step<&network::busbars>(busbar->id);
      auto const b_misstep = url_make_step<&network::busbars>(busbar->id + 1);
      auto const M_step = url_make_step<&busbar::units>(unit_member->id());
      auto const M_misstep = url_make_step<&busbar::units>(unit_member->id() + 1);
      auto const P_step = url_make_step<&busbar::power_modules>(power_module->id);
      auto const P_misstep = url_make_step<&busbar::power_modules>(power_module->id + 1);

      CHECK(url_check_path<stm::transmission_line>(*model, {n_step, t_step}));
      CHECK(!url_check_path<stm::transmission_line>(*model, {n_step, t_misstep}));
      CHECK(url_check_path<stm::busbar>(*model, {n_step, b_step}));
      CHECK(!url_check_path<stm::busbar>(*model, {n_step, b_misstep}));
      CHECK(!url_check_path<stm::busbar>(*model, {n_misstep, b_step}));
      CHECK(url_check_path<stm::unit_member>(*model, {n_step, b_step, M_step}));
      CHECK(!url_check_path<stm::unit_member>(*model, {n_step, b_step, M_misstep}));
      CHECK(url_check_path<stm::power_module_member>(*model, {n_step, b_step, P_step}));
      CHECK(!url_check_path<stm::power_module_member>(*model, {n_step, b_step, P_misstep}));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, t_step}, "capacity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, t_misstep}, "capacity"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, t_step}, "capsassity"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step}, "price.result"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step}, "flow.realised"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_misstep}, "flow.realised"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step}, "flab.blub"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, M_step}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_misstep, M_misstep}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, M_step}, "flab.fleb"));
      CHECK(url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, P_step}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_misstep, P_misstep}, "active"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {n_step, b_step, P_step}, "flab.fleb"));
    }
    SUBCASE("power_module") {

      static_assert(url_max_path_length(identity_v<power_module>) == 0ul);

      auto power_module = std::make_shared<stm::power_module>(2, "", "", model);

      model->power_modules.push_back(power_module);

      auto const m_step = url_make_step<&stm_system::power_modules>(power_module->id);
      auto const m_misstep = url_make_step<&stm_system::power_modules>(power_module->id + 1);

      CHECK(url_check<time_series::dd::apoint_ts>(*model, {m_step}, "power.schedule"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_step}, "powsxxxr.erexx"));
      CHECK(!url_check<time_series::dd::apoint_ts>(*model, {m_misstep}, "power.result"));
    }

    SUBCASE("planning") {

      auto const model_id = fmt::format("{}", model->id);

      auto hps = std::make_shared<stm_hps>(10, "");
      auto unit = std::make_shared<stm::unit>(50, std::string{}, std::string{}, hps);

      hps->units.push_back(unit);
      model->hps.push_back(hps);
      auto market = std::make_shared<energy_market_area>(30, "", "", model);

      model->market.push_back(market);

      auto url_custom_input_attr = fmt::format("{}{}", url_planning_custom_input_prefix, ".x");
      auto url_custom_input = url_format(model_id, {}, fmt::format("{}{}", custom_attr_prefix, url_custom_input_attr));
      auto url_custom_output_attr = url_planning_custom_output_prefix;
      auto url_custom_output = url_format(
        model_id, {}, fmt::format("{}{}", custom_attr_prefix, url_custom_output_attr));

      model->custom[url_custom_input_attr] = any_attr{2};
      model->custom[url_custom_output_attr] = any_attr{4};

      {
        auto urls = url_planning_inputs(model_id, *model);

        std::vector<std::string> also_urls;
        url_with_planning_inputs(model_id, *model, [&](ignore_t, ignore_t, std::string_view url) {
          also_urls.push_back(std::string(url));
        });

        CHECK(!urls.empty());
        CHECK(std::ranges::equal(urls, also_urls));

        auto url_market_area_price = url_format(model_id, {url_make_step<&stm_system::market>(market->id)}, "price");

        CHECK(std::ranges::count(urls, url_market_area_price) == 1);
        CHECK(std::ranges::count(urls, url_custom_input) == 1);

        for (auto const &url : urls) {
          auto parse_result = url_parse_to_result(url);
          auto checker = [&]<typename T>(T const &d) {
            if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_tuple>) {
              CHECK(std::get<0>(d) == model_id);
            } else if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_parse_error>) {
              CHECK(false);
            }
          };
          std::visit(checker, parse_result);
        }
      }
      {
        auto urls = url_planning_outputs(model_id, *model);

        CHECK(!urls.empty());
        CHECK(std::ranges::count(urls, url_custom_output) == 1);

        url_parse_buffer buffer;
        for (auto const &url : urls) {
          auto parse_result = url_parse_to_result(url);
          auto checker = [&]<typename T>(T const &d) {
            if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_tuple>) {
              CHECK(std::get<0>(d) == model_id);
            } else if constexpr (std::is_same_v<T, shyft::energy_market::stm::url_parse_error>) {
              CHECK(false);
            }
          };
          std::visit(checker, parse_result);
        }
      }
    }
  }

  TEST_CASE("stm/urls/all") {
    auto model_id = "Mmm";
    auto model = std::make_shared<stm_system>(0, "k");
    auto hps = std::make_shared<stm_hps>(10, "");
    model->hps.push_back(hps);
    auto reservoir = std::make_shared<stm::reservoir>(20, std::string{}, std::string{}, hps);
    hps->reservoirs.push_back(reservoir);
    auto actual_reservoir_urls = url_all(model_id, *model);

    auto url_level_constraint_min = url_format(
      model_id,
      {url_make_step<&stm_system::hps>(hps->id), url_make_step<&stm_hps::reservoirs>(reservoir->id)},
      "level.constraint.min");
    REQUIRE_MESSAGE(
      std::ranges::find(actual_reservoir_urls, url_level_constraint_min) != std::ranges::end(actual_reservoir_urls),
      actual_reservoir_urls);
  }

  TEST_CASE("stm/get_attr") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    auto const hps = mdl->hps[0];
    auto const u = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(1));
    auto const model_id = "mdl";

    // prepare known attrs to check against
    auto const test_bool = true;
    auto const test_double = 900.3;


    auto const test_time_axis = shyft::time_axis::generic_dt{
      std::vector<utctime>{
                           core::from_seconds(0),
                           core::from_seconds(1),
                           core::from_seconds(2),
                           core::from_seconds(3),
                           core::from_seconds(4),
                           core::from_seconds(5)}
    };
    auto const test_apoint = time_series::dd::apoint_ts{test_time_axis, 1.1, shyft::time_series::POINT_INSTANT_VALUE};
    auto const test_txy = test::create_t_xy(12, std::vector<double>{1, 2});
    auto const test_txyz = test::create_t_xyz(1, std::vector<double>{3, 4, 5});
    auto const test_turbine_description = test::create_t_turbine_description(1, std::vector<double>{9, 10, 11});
    auto const ctr = std::make_shared<shyft::energy_market::stm::contract>(1, "ctr", "{}", mdl);
    auto const test_str = "SomeString";
    auto const test_unit_group_type = shyft::energy_market::stm::unit_group_type::afrr_up;
    auto const ug = mdl->add_unit_group(1, "ug1", "{}");

    u->best_profit.production = test_txy;
    u->pump_description = test_txyz;
    u->turbine_description = test_turbine_description;
    ctr->seller = test_str;
    ug->group_type = test_unit_group_type;

    mdl->run_params.head_opt = test_bool;
    mdl->summary->grand_total = test_double;
    mdl->market[0]->price = test_apoint;
    mdl->contracts.push_back(ctr);
    mdl->run_params.run_time_axis = test_time_axis;

    SUBCASE("get_error_illegal_path") {
      auto const res = get_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id + 10), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "reserve.droop.cost"));
      CHECK(std::get_if<stm::url_resolve_error>(&res) != nullptr);
    }
    SUBCASE("get_error_illegal_attr") {
      auto const res = get_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "reservex.droop.cost"));
      CHECK(std::get_if<stm::url_resolve_error>(&res) != nullptr);
    }
    SUBCASE("get_error_unrepresented_type") {
      // std::uint16_t is not part of stm::any_attr
      auto const res = get_attr(*mdl, stm::url_format(model_id, {}, "run_params.n_inc_runs"));
      CHECK(std::get_if<stm::url_resolve_error>(&res) != nullptr);
    }
    SUBCASE("get_bool") {
      auto const res = get_attr(*mdl, stm::url_format(model_id, {}, "run_params.head_opt"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_bool = std::get_if<bool>(as_any);
      REQUIRE(as_bool != nullptr);
      CHECK_EQ(*as_bool, test_bool);
    }
    SUBCASE("get_double") {
      auto const res = get_attr(
        *mdl,
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::summary>(mdl->summary->id)}, "grand_total"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_double = std::get_if<double>(as_any);
      REQUIRE(as_double != nullptr);
      CHECK_EQ(*as_double, doctest::Approx(test_double));
    }
    // there are no int64_t attrs
    // there are no uint64_t attrs
    SUBCASE("get_apoint") {
      auto const m = mdl->market[0];
      auto const res = get_attr(
        *mdl, stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::market>(m->id)}, "price"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_apoint = std::get_if<time_series::dd::apoint_ts>(as_any);
      REQUIRE(as_apoint != nullptr);
      CHECK_EQ(*as_apoint, test_apoint);
      // unequal shared ptr
      CHECK(as_apoint->ts.get() != test_apoint.ts.get());
    }
    SUBCASE("get_t_xy") {
      auto const res = get_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "best_profit.production"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_txy = std::get_if<t_xy_>(as_any);
      REQUIRE(as_txy != nullptr);
      CHECK(equal_attributes(*as_txy, test_txy));
      // unequal shared ptr
      CHECK(*as_txy != test_txy);
    }
    // there are no t_xyz_ attrs
    SUBCASE("get_txyz_list") {
      auto const res = get_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "pump_description"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_txyz = std::get_if<t_xyz_list_>(as_any);
      REQUIRE(as_txyz != nullptr);
      CHECK(equal_attributes(*as_txyz, test_txyz));
      // unequal shared ptr
      CHECK(*as_txyz != test_txyz);
    }
    SUBCASE("get_turbine_description") {
      auto const res = get_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "turbine_description"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_turb = std::get_if<t_turbine_description_>(as_any);
      REQUIRE(as_turb != nullptr);
      CHECK(equal_attributes(*as_turb, test_turbine_description));
      // unequal shared ptr
      CHECK(*as_turb != test_turbine_description);
    }
    SUBCASE("get_string") {
      auto const res = get_attr(
        *mdl, stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::contracts>(ctr->id)}, "seller"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_str = std::get_if<std::string>(as_any);
      REQUIRE(as_str != nullptr);
      CHECK_EQ(*as_str, test_str);
    }
    // there are no time_series::dd::ats_vector attrs
    SUBCASE("get_ugtype") {
      auto const res = get_attr(
        *mdl, stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "group_type"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_unit_group_type = std::get_if<unit_group_type>(as_any);
      REQUIRE(as_unit_group_type != nullptr);
      CHECK_EQ(*as_unit_group_type, test_unit_group_type);
    }
    SUBCASE("get_time_axis") {
      auto const res = get_attr(*mdl, stm::url_format(model_id, {}, "run_params.run_time_axis"));
      auto const *as_any = std::get_if<stm::any_attr>(&res);
      REQUIRE(as_any != nullptr);
      auto const *as_time_axis = std::get_if<time_axis::generic_dt>(as_any);
      REQUIRE(as_time_axis != nullptr);
      CHECK_EQ(*as_time_axis, test_time_axis);
    }
  }

  TEST_CASE("stm/set_attr") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    auto const model_id = "mdl";

    // prepare known attrs to check against
    auto const test_bool = true;
    auto const test_double = 900.3;
    auto const test_apoint = time_series::dd::apoint_ts{"test_series"};
    auto const test_txy = test::create_t_xy(12, std::vector<double>{1, 2});
    auto const test_txyz = test::create_t_xyz(1, std::vector<double>{3, 4, 5});
    auto const test_turbine_description = test::create_t_turbine_description(1, std::vector<double>{9, 10, 11});
    auto const ctr = std::make_shared<shyft::energy_market::stm::contract>(1, "ctr", "{}", mdl);
    auto const test_str = "SomeString";
    auto const test_unit_group_type = shyft::energy_market::stm::unit_group_type::afrr_up;
    auto const ug = mdl->add_unit_group(1, "ug1", "{}");
    auto const test_time_axis = shyft::time_axis::generic_dt{
      std::vector<utctime>{
                           core::from_seconds(0),
                           core::from_seconds(1),
                           core::from_seconds(2),
                           core::from_seconds(3),
                           core::from_seconds(4),
                           core::from_seconds(5)}
    };

    mdl->contracts.push_back(ctr);
    auto const hps = mdl->hps[0];
    auto const u = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(1));

    SUBCASE("set_error_illegal_path") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id + 10), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "reserve.droop.cost"),
        test_apoint);
      CHECK(failed);
    }
    SUBCASE("set_error_illegal_attr") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "reservex.droop.cost"),
        test_apoint);
      CHECK(failed);
    }
    SUBCASE("set_error_wrong_type") {
      // droop cost is apoint, set bool:
      auto const failed = set_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "reserve.droop.cost"),
        test_bool);
      CHECK(failed);
    }
    SUBCASE("set_bool") {
      auto const failed = set_attr(*mdl, stm::url_format(model_id, {}, "run_params.head_opt"), test_bool);
      CHECK_FALSE(failed);
      CHECK(mdl->run_params.head_opt == test_bool);
    }
    SUBCASE("set_double") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::summary>(mdl->summary->id)}, "grand_total"),
        test_double);
      CHECK_FALSE(failed);
      CHECK(mdl->summary->grand_total == test_double);
    }
    // there are no int64_t attrs
    // there are no uint64_t attrs
    SUBCASE("set_apoint") {
      auto m = mdl->market[0];
      auto const failed = set_attr(
        *mdl, stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::market>(m->id)}, "price"), test_apoint);
      CHECK_FALSE(failed);
      CHECK(m->price == test_apoint);
    }
    SUBCASE("set_t_xy") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "best_profit.production"),
        test_txy);
      CHECK_FALSE(failed);
      CHECK(u->best_profit.production == test_txy);
    }
    // there are no t_xyz_ attrs
    SUBCASE("set_txyz_list") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "pump_description"),
        test_txyz);
      CHECK_FALSE(failed);
      CHECK(u->pump_description == test_txyz);
    }
    SUBCASE("get_turbine_description") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
          "turbine_description"),
        test_turbine_description);
      CHECK_FALSE(failed);
      CHECK(u->turbine_description == test_turbine_description);
    }
    SUBCASE("get_string") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::contracts>(ctr->id)}, "seller"),
        test_str);
      CHECK_FALSE(failed);
      CHECK(ctr->seller == test_str);
    }
    // there are no time_series::dd::ats_vector attrs
    SUBCASE("get_ugtype") {
      auto const failed = set_attr(
        *mdl,
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "group_type"),
        test_unit_group_type);
      CHECK_FALSE(failed);
      CHECK(ug->group_type == test_unit_group_type);
    }
    SUBCASE("get_time_axis") {
      auto const failed = set_attr(*mdl, stm::url_format(model_id, {}, "run_params.run_time_axis"), test_time_axis);
      CHECK_FALSE(failed);
      CHECK(mdl->run_params.run_time_axis == test_time_axis);
    }
  }

  TEST_SUITE_END();

}
