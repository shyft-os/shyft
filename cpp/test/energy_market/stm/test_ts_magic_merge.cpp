#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>

#include <doctest/doctest.h>

namespace shyft::energy_market::stm {

  using namespace shyft::core;
  using namespace shyft::energy_market::stm::srv::dstm;
  using std::string;
  using std::to_string;
  using shyft::dtss::store_policy;
  using shyft::time_series::dd::apoint_ts;
  using std::shared_ptr;
  using std::make_shared;
  using time_axis = shyft::time_axis::generic_dt;
  using shyft::time_series::ts_point_fx;
  using shyft::energy_market::stm::srv::dstm::ts_magic_merge_values;
  using shyft::energy_market::stm::srv::dstm::ts_merge_result;

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/srv/ts_magic_merge") {
    /**
     * The magic merge deserves a separate test to pinpoint the
     * semantics needed for this function.
     */
    // helper
    struct dtss_mock {
      ats_vector ms_x;
      ats_vector s_x;
      bool cow{false};
      bool recreate{false};
      bool strict{true};
      size_t call_count{0};
      ats_vector existing_tsv;

      void do_merge_store_ts(ats_vector const &x, bool cow) {
        ms_x = x;
        this->cow = cow;
        ++call_count;
      }

      void do_store_ts(ats_vector const &x, store_policy p) {
        s_x = x;
        this->cow = p.cache;
        this->recreate = p.recreate;
        this->strict = p.strict;
        dtss_mock *null_mock{nullptr};
        if (!p.recreate && !p.strict && existing_tsv.size() == x.size()) {
          // to a aligned store here, using magic merge simple-assign
          for (size_t i = 0; i < x.size(); ++i) {
            ts_magic_merge_values(
              [&](ats_vector const &x, bool cow) {
                this->do_merge_store_ts(x, cow);
              },
              [&](ats_vector const &x, store_policy p) {
                this->do_store_ts(x, p);
              },
              existing_tsv[i],
              x[i],
              false,
              p.recreate,
              p.cache,
              p.strict);
          }
        }
        ++call_count;
      }
    };

    auto s = std::make_unique<dtss_mock>();
    dtss_mock *null_dtss{nullptr};
    time_axis ta3{from_seconds(0), from_seconds(10), 3};
    time_axis ta4{from_seconds(10), from_seconds(5), 4};
    apoint_ts lhs{
      ta3, {1.0, 2.0, 3.0},
       ts_point_fx::POINT_AVERAGE_VALUE
    };
    apoint_ts rhs{
      ta4, {4.0, 4.1, 4.2, 4.3},
       ts_point_fx::POINT_AVERAGE_VALUE
    };
    apoint_ts unbound{"shyft://stm/U1.production.schedule"};
    apoint_ts bound{"shyft://stm/U1.production.schedule", lhs};
    apoint_ts dstm_unbound{"dstm://M1/H1/U1.production.schedule"};
    apoint_ts dstm_bound{"dstm://M1/H1/U1.production.schedule", lhs};
    apoint_ts expr = lhs * rhs * unbound; // expression
    apoint_ts empty{};
    SUBCASE("strict") {
      SUBCASE("plain_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            lhs,
            rhs,
            false,
            true,
            true));
        FAST_CHECK_EQ(s->call_count, 0);
        FAST_CHECK_EQ(lhs, rhs); // simple assign, should be exactly equal
      }
      SUBCASE("plain_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            lhs,
            rhs,
            true,
            false,
            true));
        FAST_CHECK_EQ(s->call_count, 0);
        auto expected{lhs};
        expected.merge_points(rhs);
        FAST_CHECK_EQ(lhs, expected); // merge assign
      }

      SUBCASE("unbound_ref_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::saved_to_dtss,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            unbound,
            rhs,
            false,
            true,
            true));
        FAST_CHECK_EQ(s->call_count, 1);
        FAST_CHECK_EQ(s->cow, true);
        FAST_CHECK_EQ(s->recreate, true);
        FAST_REQUIRE_EQ(s->s_x.size(), 1);
        FAST_REQUIRE_EQ(s->s_x[0], rhs);
      }
      SUBCASE("unbound_ref_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::saved_to_dtss,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            unbound,
            rhs,
            true,
            true,
            true));
        FAST_CHECK_EQ(s->call_count, 1);
        FAST_CHECK_EQ(s->cow, true);
        FAST_CHECK_EQ(s->recreate, false);
        FAST_REQUIRE_EQ(s->ms_x.size(), 1);
        FAST_REQUIRE_EQ(s->ms_x[0], rhs);
      }
      SUBCASE("bound_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            bound,
            rhs,
            false,
            true,
            true));
        FAST_CHECK_EQ(s->call_count, 0);
        FAST_CHECK_EQ(bound, rhs); // simple assign, should be exactly equal
      }
      SUBCASE("bound_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            bound,
            rhs,
            true,
            false,
            true));
        FAST_CHECK_EQ(s->call_count, 0);
        auto expected{bound};
        expected.merge_points(rhs);
        FAST_CHECK_EQ(bound, expected); // merge assign
      }
      SUBCASE("dstm_unbound_ref_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            dstm_unbound,
            rhs,
            false,
            true,
            true));
        FAST_CHECK_EQ(s->call_count, 0);
        FAST_CHECK_EQ(dstm_unbound, rhs);
      }
      SUBCASE("dstm_bound_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            dstm_bound,
            rhs,
            true,
            false,
            true));
        FAST_CHECK_EQ(s->call_count, 0);
        auto expected{dstm_bound};
        expected.merge_points(rhs);
        FAST_CHECK_EQ(dstm_bound, expected); // merge assign
      }
      SUBCASE("lhs_is_expression") {
        FAST_CHECK_EQ(
          ts_merge_result::fail_expression,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            expr,
            rhs,
            true,
            false,
            true));
        FAST_CHECK_EQ(
          ts_merge_result::fail_expression,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            expr,
            rhs,
            false,
            false,
            true));
        FAST_CHECK_EQ(
          ts_merge_result::fail_expression,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            expr,
            rhs,
            false,
            true,
            true));
      }
      SUBCASE("lhs_is_empty") {
        apoint_ts e1, e2;
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            e1,
            rhs,
            true,
            false,
            true));
        FAST_CHECK_EQ(e1, rhs);
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            e2,
            rhs,
            false,
            true,
            true));
        FAST_CHECK_EQ(e2, rhs);
      }
    }
    SUBCASE("non-strict") {
      constexpr bool no_merge{false}, merge{false}, recreate{true}, keep{false}, cache{true}, no_strict{false};
      SUBCASE("plain_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            lhs,
            rhs,
            no_merge,
            recreate,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 0);
        FAST_CHECK_EQ(lhs, rhs); // simple assign, should be exactly equal
      }
      SUBCASE("plain_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            lhs,
            rhs,
            merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 0);
        auto expected{lhs};
        expected.merge_points(rhs);
        FAST_CHECK_EQ(lhs, expected); // merge assign
      }

      SUBCASE("unbound_ref_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::saved_to_dtss,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            unbound,
            rhs,
            no_merge,
            recreate,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 1);
        FAST_CHECK_EQ(s->cow, true);
        FAST_CHECK_EQ(s->recreate, true);
        FAST_CHECK_EQ(s->strict, false);
        FAST_REQUIRE_EQ(s->s_x.size(), 1);
        FAST_REQUIRE_EQ(s->s_x[0], rhs);
      }
      SUBCASE("unbound_ref_ts_assign_keep") {
        s->existing_tsv.push_back(rhs); // fine resolution, then assign with lhs coarse resolution
        FAST_CHECK_EQ(
          ts_merge_result::saved_to_dtss,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            unbound,
            lhs,
            no_merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 1);
        FAST_CHECK_EQ(s->cow, true);
        FAST_CHECK_EQ(s->recreate, false);
        FAST_CHECK_EQ(s->strict, false);
        FAST_REQUIRE_EQ(s->s_x.size(), 1);
        FAST_REQUIRE_EQ(s->s_x[0], lhs);
        // then test that result is semantically same. so s->existing_tsv
        FAST_REQUIRE_EQ(s->existing_tsv.size(), 1);
        auto vv = s->existing_tsv[0].values();
        auto ta = s->existing_tsv[0].time_axis();
        FAST_CHECK_EQ(s->existing_tsv[0].time_axis().dt(), rhs.time_axis().dt());
        FAST_CHECK_NE(rhs.time_axis().dt(), lhs.time_axis().dt()); // just ensure the above counts
        FAST_CHECK_EQ(ta.total_period(), lhs.total_period());      //
      }

      SUBCASE("unbound_ref_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::saved_to_dtss,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            unbound,
            rhs,
            merge,
            recreate,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 1);
        FAST_CHECK_EQ(s->cow, true);
        FAST_CHECK_EQ(s->recreate, true);
        FAST_CHECK_EQ(s->strict, false);
        FAST_REQUIRE_EQ(s->s_x.size(), 1);
        FAST_REQUIRE_EQ(s->s_x[0], rhs);
      }
      SUBCASE("bound_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            bound,
            rhs,
            no_merge,
            recreate,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 0);
        FAST_CHECK_EQ(bound, rhs); // simple assign, should be exactly equal
      }
      SUBCASE("bound_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            bound,
            rhs,
            merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 0);
        auto expected{bound};
        expected.merge_points(rhs);
        FAST_CHECK_EQ(bound, expected); // merge assign
      }
      SUBCASE("dstm_unbound_ref_ts_assign") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            dstm_unbound,
            rhs,
            no_merge,
            recreate,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 0);
        FAST_CHECK_EQ(dstm_unbound, rhs);
      }
      SUBCASE("dstm_bound_ts_merge") {
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            dstm_bound,
            rhs,
            merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(s->call_count, 0);
        auto expected{dstm_bound};
        expected.merge_points(rhs);
        FAST_CHECK_EQ(dstm_bound, expected); // merge assign
      }
      SUBCASE("lhs_is_expression") {
        FAST_CHECK_EQ(
          ts_merge_result::fail_expression,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            expr,
            rhs,
            merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(
          ts_merge_result::fail_expression,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            expr,
            rhs,
            no_merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(
          ts_merge_result::fail_expression,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            expr,
            rhs,
            no_merge,
            recreate,
            cache,
            no_strict));
      }
      SUBCASE("lhs_is_empty") {
        apoint_ts e1, e2;
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            e1,
            rhs,
            merge,
            keep,
            cache,
            no_strict));
        FAST_CHECK_EQ(e1, rhs);
        FAST_CHECK_EQ(
          ts_merge_result::merged,
          ts_magic_merge_values(
            [&](ats_vector const &x, bool cow) {
              s->do_merge_store_ts(x, cow);
            },
            [&](ats_vector const &x, store_policy p) {
              s->do_store_ts(x, p);
            },
            e2,
            rhs,
            no_merge,
            recreate,
            cache,
            no_strict));
        FAST_CHECK_EQ(e2, rhs);
      }
    }
  }

  TEST_SUITE_END();
}
