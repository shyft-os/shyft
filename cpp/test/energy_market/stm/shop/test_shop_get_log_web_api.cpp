#include <memory>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/shop/model_simple.h>
#include <test/energy_market/stm/shop/test_server_dstm.h>
#include <test/test_utils.h>
#include <test/web_api/request_sender.h>

namespace shyft::energy_market::stm::shop {

  using core::utctime;
  using core::utctime_now;
  using core::to_seconds64;
  using core::from_seconds;

  TEST_SUITE_BEGIN("stm/shop");
  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
  auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
  auto const t_step = core::deltahours(1);
  auto const n_steps = (std::size_t)((t_end - t_begin) / t_step);
  time_axis::generic_dt const ta{t_begin, t_step, n_steps};

  TEST_CASE("stm/get_log") {
    using namespace std::chrono_literals;
    dlib::set_all_logging_levels(dlib::LNONE);
    //-- keep test directory, unique, and with auto-cleanup.
    auto dirname = fmt::format("shop.web_api.test.{}", to_seconds64(utctime_now()));
    test::utils::temp_dir tmpdir(dirname.c_str());
    std::string doc_root = (tmpdir / "doc_root").string();

    auto a = std::make_unique<test_server_dstm>();
    std::string host_ip{"127.0.0.1"};
    a->set_listening_ip(host_ip);
    a->start_server();

    // Store some models:
    auto mdl = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    srv::dstm::add_model(*a, "simple", mdl);

    // Add some messages:
    a->dispatch.state->models.mutate_or_throw("simple", [](auto&& view) {
      view.info->log.push_back(log_entry{log_severity::information, "An informational message", 1145, from_seconds(0)});
      view.info->log.push_back(log_entry{log_severity::warning, "A warning message", 3301, from_seconds(1)});
    });
    int port = a->start_web_api(host_ip, 0, doc_root, 1, 1);

    std::this_thread::sleep_for(700ms);
    REQUIRE(a->web_api_running());
    std::vector<std::string> requests{
      R"_(get_log {"request_id": "1", "model_key": "simple","log_version":0,"bookmark":1})_",
      R"_(get_state {"request_id": "2", "model_key": "simple"})_",
      R"_(get_log {"request_id": "3", "model_key": "simple","log_version":1,"bookmark":2,"severity_level":0})_",
      R"_(get_log {"request_id": "4", "model_key": "simple","log_version":0,"bookmark":0,"severity_level":1})_",
      R"_(get_log {"request_id": "5", "model_key": "simple","log_version":0,"bookmark":2,"severity_level":0})_"};
    std::vector<std::string> responses(requests.size());
    std::size_t r = 0;
    {
      boost::asio::io_context ioc;
      std::atomic<bool> session_has_finished = false;
      auto s1 = std::make_shared<::shyft::test::session>(ioc);
      s1->run(host_ip, port, requests[r], [&responses, &r, &requests](std::string const & web_response) -> std::string {
        responses[r] = web_response;
        ++r;
        return r >= requests.size() ? std::string("") : requests[r];
      });
      ioc.run();
      s1.reset();
      a->stop_web_api();
    }
    REQUIRE_EQ(requests.size(), r);
    std::vector<std::string> expected{
      R"_({"request_id":"1","log_version":0,"bookmark":2,"result":[{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]})_",
      R"_({"request_id":"2","result":{"state":"idle"}})_",
      R"_({"request_id":"3","log_version":0,"bookmark":2,"result":[{"time":0.0,"severity":"INFORMATION","code":1145,"message":"An informational message"},{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]})_",
      R"_({"request_id":"4","log_version":0,"bookmark":2,"result":[{"time":1.0,"severity":"WARNING","code":3301,"message":"A warning message"}]})_",
      R"_({"request_id":"5","log_version":0,"bookmark":2,"result":[]})_"};
    REQUIRE_EQ(expected.size(), responses.size());
    std::ranges::for_each(
      std::views::zip(expected, responses), boost::hof::unpack([](auto const & ex, auto const & res) {
        CHECK_EQ(res, ex);
      }));
    clear_manager(a.get());
    a.reset();
  }

}

TEST_SUITE_END();
