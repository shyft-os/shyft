#pragma once
#include <cstdint>
#include <future>
#include <string>

#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/web_api/energy_market/request_handler.h>

namespace shyft::energy_market::stm {

  struct test_server_dstm : srv::dstm::server {
    web_api::energy_market::request_handler bg_server;
    std::future<int> web_srv;

    //-- to verify fx-callback
    std::string fx_mid;
    std::string fx_arg;

    test_server_dstm();
    test_server_dstm(std::string const & root_dir);

    std::int64_t start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads);
    bool web_api_running() const;
    void stop_web_api();
    bool fx_handler(std::string mid, std::string json_arg);
  };

  void clear_manager(srv::dstm::server* dstm);
}