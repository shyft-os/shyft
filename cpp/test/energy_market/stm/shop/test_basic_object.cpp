#include <array>
#include <cmath>
#include <vector>

#include <shyft/energy_market/stm/shop/api/shop_proxy.h>

#include <test/energy_market/stm/shop/fixture.h>

#ifndef NAME_MAX
// not defined on win/mingw, so def it here same value as for linux
#define NAME_MAX 255
#endif
namespace shyft::energy_market::stm::shop {
  using ::shop::data::shop_time;

  TEST_SUITE_BEGIN("stm/shop");

  // NOTE: In general, time resolution must be set before any objects are added, since time series
  // will be initialized. Adding reservoirs and plants works though, but adding generators
  // crashes (segmentation violation).

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/add_reservoir") {
    auto const reservoir_type_index{0};
    SUBCASE("Adding reservoir by type index") {
      auto reservoir_name{"reservoir by index"};
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_index, reservoir_name);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, shop_type::reservoir), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectName, reservoir_name), 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
    }

    SUBCASE("Adding reservoir by type name") {
      auto reservoir_name{"reservoir by name"};
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto const oix = ShopAddObject(shopSystem_.get(), shop_type::reservoir, reservoir_name);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, shop_type::reservoir), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectName, reservoir_name), 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/add_generator") {
    auto const generator_type_index{2};
    auto generator_type_name = shop_type::generator;
    shop_time t_start{"20180910000000000", true}, t_end{"20180911000000000", true};
    ShopSetTimeResolution(shopSystem_.get(), t_start, t_end, ::shop::data::shop_time_unit::hour);
    SUBCASE("Adding generator by type index") {
      auto generator_name{"generator by index"};
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto const oix = ShopAddObject(shopSystem_.get(), generator_type_index, generator_name);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, generator_type_name), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectName, generator_name), 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
    }

    SUBCASE("Adding generator by type name") {
      auto generator_name{"generator by name"};
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto const oix = ShopAddObject(shopSystem_.get(), generator_type_name, generator_name);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, generator_type_name), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectName, generator_name), 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/reservoir_object_and_id") {
    auto const reservoir_type_index{0};
    auto reservoir_type_name = shop_type::reservoir;
    auto id{"id"};
    SUBCASE("Adding reservoir by type index without id") {
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto reservoir_name{"reservoir by index"};
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_index, reservoir_name);
      CHECK_GT(oix, 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
      auto const oid = ShopGetObjectId(shopSystem_.get(), oix);
      CHECK_EQ(oid, -1); // Object _id_ is not set, ShopAddObject returns object _index_!
      auto const attr_id_index = ShopGetAttributeIndex(shopSystem_.get(), oix, id);
      CHECK_EQ(attr_id_index, 0);                                             // There is a valid attribute actually
      CHECK_FALSE(ShopCheckAttributeToGet(shopSystem_.get(), attr_id_index)); // But it is not allowed to get
      CHECK_FALSE(ShopCheckAttributeToSet(shopSystem_.get(), attr_id_index)); // Nor set
      int const attr_id_value = 123;
      CHECK_FALSE(
        ShopSetIntAttribute(shopSystem_.get(), oix, attr_id_index, attr_id_value)); // And trying to set it fails
      int attr_id_value_read = -999;
      CHECK_FALSE(ShopGetIntAttribute(
        shopSystem_.get(), oix, attr_id_index, attr_id_value_read)); // And also trying to get it fails
      auto const oid2 = ShopGetObjectId(shopSystem_.get(), oix);
      CHECK_EQ(oid2, -1); // Nada
    }
    SUBCASE("Adding reservoir by type name with id") {
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto reservoir_name{"reservoir by index"};
      auto const reservoir_id{123};
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_name, reservoir_name, reservoir_id);
      CHECK_GT(oix, 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
      auto const oid = ShopGetObjectId(shopSystem_.get(), oix);
      CHECK_EQ(oid, reservoir_id);
      auto const attr_id_index = ShopGetAttributeIndex(shopSystem_.get(), oix, id);
      CHECK_EQ(attr_id_index, 0);                                             // There is a valid attribute actually
      CHECK_FALSE(ShopCheckAttributeToGet(shopSystem_.get(), attr_id_index)); // But it is not allowed to get
      CHECK_FALSE(ShopCheckAttributeToSet(shopSystem_.get(), attr_id_index)); // Nor set
      int attr_id_value_read = -999;
      CHECK_FALSE(
        ShopGetIntAttribute(shopSystem_.get(), oix, attr_id_index, attr_id_value_read)); // Trying to get it fails
    }
  }

  TEST_CASE("stm/shop/object_long_name") {
    {
      test_fixture fixture; // Scoped fixture, because log file test below needs it
      // Pushing the limits of object names.
      // When crossing the limit the test will crash and burn with access violation!
      // Findings: Reservoirs, plants and gates can mostly be set with 109 characters, although sometimes that also
      // fails, generators 111, junctions 364, junction_gate and creek_intake 367, etc.
      shop_time t_start{"20180910000000000", true}, t_end{"20180911000000000", true};
      ShopSetTimeResolution(fixture.shopSystem_.get(), t_start, t_end, ::shop::data::shop_time_unit::hour);
      char const * shop_types[]{
        shop_type::reservoir,
        shop_type::generator,
        shop_type::plant,
        shop_type::gate,
        shop_type::market,
        shop_type::scenario,
        shop_type::junction,
        shop_type::junction_gate,
        shop_type::creek_intake};
      for (size_t i = 0; i < 9; ++i) {
        auto a_type = shop_types[i];
        char const a_name[]{
          "99 characters long random string jklfjdsfkljdsklf fkdlsjf sdkfjs dlkfsd jflkds fjsdkl f charactersc"};
        CHECK_GT(ShopAddObject(fixture.shopSystem_.get(), a_type, a_name), 0);
        // Crashes and burns with access violation for reservoirs, plants and gates, activate for check:
#ifdef CHECK_SHOP_LIMITS
        char const a_name2[]{
          "110 characters long random string jklfjdsfkljdsklf fkdlsjf sdkfjs dlkfsd jflkds fjsdkl "
          "fcharactersjffdsfdsjdx"};
        CHECK_GT(ShopAddObject(fixture.shopSystem_.get(), a_type, a_name2), 0);
        // Find limit (but will end with access violation crash):
        char strbuf[1024]{};
        for (size_t i = sizeof(strbuf) - 1; i > 0; --i) {
          auto a_name2 = &strbuf[i - 1];
          *a_name2 = 'a';
          CHECK_GT(ShopAddObject(fixture.shopSystem_.get(), a_type, (char const *) a_name2), 0);
        }
#endif
      }
    }
    {
      auto dir = std::filesystem::current_path().string();
      char fullname[PATH_MAX]{};
      size_t i = 0, j = 0;
      for (; i < dir.size(); ++i)
        fullname[i] = dir[i];
      for (; i < PATH_MAX - 2; ++i, ++j) {
        if (j % NAME_MAX == 0)
          fullname[i] = std::filesystem::path::preferred_separator;
        else
          fullname[i] = 'a';
      }
      if (j % NAME_MAX != 0) // else, if at name max but only 1 character left in path, we need to just leave it at that
        fullname[i] = 'a';
      char* objectList[]{fullname};
      auto filepath = std::filesystem::path(fullname);
      // MESSAGE(filepath);
      REQUIRE(!std::filesystem::exists(filepath));
      {
        test_fixture fixture; // Scoped fixture, because we need to free shop system before file can be deleted
        CHECK(ShopExecuteCommand(fixture.shopSystem_.get(), "log file", 0, nullptr, 1, objectList));
      }
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/add_object_failure") {
    auto const invalid_type_index{45325423};
    char invalid_type_name[] = {"frakjhlrgadfds"};
    SUBCASE("Add object with invalid type index") {
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
#if 1 // Version 13.2.1.f (2020.09.15) throws exception: Unknown object type index: 45325423

      CHECK_THROWS(ShopAddObject(shopSystem_.get(), invalid_type_index, "invalid by index"));
#else // OLD: ShopAddObject with invalid type index crashes in shop.lib version from 03.10.2018 (still the case
      // in 13.2.1.d from 2020.06.04)
      auto const oix = ShopAddObject(shopSystem_.get(), invalid_type_index, "invalid by index");
      CHECK_EQ(oix, -1);
#endif
      CHECK_EQ(ShopGetObjectCount(shopSystem_.get()), before_count);
    }
    SUBCASE("Add object with invalid type name") {
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
#if 1 // Version 13.2.1.d (2020.06.04) throws exception, though a bit misleading: Unknown object type index: -1
      // Version 13.2.1.f (2020.09.15) throws exception: Unknown object type: "frakjhlrgadfds"
      CHECK_THROWS(ShopAddObject(shopSystem_.get(), invalid_type_name, "invalid by name"));
#else
      auto const oix = ShopAddObject(shopSystem_.get(), invalid_type_name, "invalid by name");
      CHECK_EQ(oix, -1);
#endif
      CHECK_EQ(ShopGetObjectCount(shopSystem_.get()), before_count);
    }

    SUBCASE("Add object with empty name") {
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto a_name{""};
#if 1 // Version 13.2.1.d (2020.06.04) throws exception
      CHECK_THROWS(ShopAddObject(shopSystem_.get(), invalid_type_name, a_name));
#else
      auto const oix = ShopAddObject(shopSystem_.get(), invalid_type_name, a_name);
      CHECK(
        oix == -1); // CHECK(oix == -2); // Note: Shop API version 13.1.1.d returns -1, previous versions returned -2
#endif
      CHECK_EQ(ShopGetObjectCount(shopSystem_.get()), before_count);
    }
    SUBCASE("Adding objects with same name") {
      auto reservoir_name{"reservoir by name"};
      auto const before_count = ShopGetObjectCount(shopSystem_.get());
      auto const oix = ShopAddObject(shopSystem_.get(), shop_type::reservoir, reservoir_name);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, shop_type::reservoir), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectName, reservoir_name), 0);
      auto const after_count = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count, before_count + 1);
#if 1 // Version 13.2.1.d (2020.06.04) throws exception
      // Version 16.5.0 (2025.31.01) throws exception
      CHECK_THROWS(ShopAddObject(shopSystem_.get(), shop_type::reservoir, reservoir_name));
#else
      // 15.3.3.0 ??
      auto const oix2 = ShopAddObject(shopSystem_.get(), shop_type::reservoir, reservoir_name);
      CHECK_EQ(oix2, 6); // so 15.3.3., just add the new object
#endif
      auto const after_count2 = ShopGetObjectCount(shopSystem_.get());
      CHECK_EQ(after_count2, before_count + 1); // again, 16.5.0, not added
    }
  }

  TEST_CASE("stm/shop/init_gives_new_instance") {
    auto const reservoir_type_index{0};
    // auto reservoir_type_name=shop_type::reservoir;
    auto reservoir_name{"reservoir test 1"};
    // First run
    ShopSystem* shopSystem = shop_init(SHYFT_SHOP_LICENSEDIR);
    CHECK_NE(shopSystem, nullptr);
    auto const initial_count = ShopGetObjectCount(shopSystem);
    auto const oix1 = ShopAddObject(shopSystem, reservoir_type_index, reservoir_name);
    CHECK_GT(oix1, 0);
    auto const after_first_run_count = ShopGetObjectCount(shopSystem);
    CHECK_EQ(after_first_run_count, initial_count + 1);
    CHECK(ShopFree(shopSystem)); // This destructs the instance of ShopSystem
    // Second run - should get new instance of ShopSystem with no traces of first run
    shopSystem = shop_init(SHYFT_SHOP_LICENSEDIR);
    CHECK_NE(shopSystem, nullptr);
    auto const before_second_run_count = ShopGetObjectCount(shopSystem);
    CHECK_EQ(before_second_run_count, initial_count);
    auto const oix2 = ShopAddObject(shopSystem, reservoir_type_index, reservoir_name);
    CHECK_GT(oix2, 0);
    auto const after_second_run_count = ShopGetObjectCount(shopSystem);
    CHECK_EQ(after_second_run_count, initial_count + 1);
    // Third run - another instance of ShopSystem before freeing the previous, still should have no traces of first run
    ShopSystem* shopSystem2 = shop_init(SHYFT_SHOP_LICENSEDIR);
    CHECK_NE(shopSystem2, nullptr);
    auto const before_third_run_count = ShopGetObjectCount(shopSystem2);
    CHECK_EQ(before_third_run_count, initial_count);
    auto const oix3 = ShopAddObject(shopSystem2, reservoir_type_index, reservoir_name);
    CHECK_GT(oix3, 0);
    auto const after_third_run_count = ShopGetObjectCount(shopSystem2);
    CHECK_EQ(after_third_run_count, initial_count + 1);
    CHECK(ShopFree(shopSystem2));
    CHECK(ShopFree(shopSystem));
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/reservoir_with_attributes") {
    auto const reservoir_type_index{0};
    auto reservoir_type_name = shop_type::reservoir;
    auto reservoir_name{"alta"};
    auto const reservoir_id{123};

    SUBCASE("Add reservoir by type name with name and id") {
      auto typeName = ShopGetObjectTypeName(shopSystem_.get(), reservoir_type_index);
      CHECK_EQ(strcmp(typeName, reservoir_type_name), 0);
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_name, reservoir_name, reservoir_id);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, reservoir_type_name), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(reservoir_name, objectName), 0);
      int objectId = ShopGetObjectId(shopSystem_.get(), oix);
      CHECK_EQ(objectId, reservoir_id);
    }

    SUBCASE("Add reservoir by type index with name") {
      auto typeName = ShopGetObjectTypeName(shopSystem_.get(), reservoir_type_index);
      CHECK_EQ(strcmp(typeName, reservoir_type_name), 0);
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_index, reservoir_name);
      CHECK_GT(oix, 0);
      char const * objectTypeName = ShopGetObjectType(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(objectTypeName, reservoir_type_name), 0);
      char const * objectName = ShopGetObjectName(shopSystem_.get(), oix);
      CHECK_EQ(strcmp(reservoir_name, objectName), 0);
    }

    SUBCASE("Set hrl by name for reservoir") {
      auto hrl_tag{"hrl"};
      auto const hrl{200.};
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_name, reservoir_name);
      auto const attribute_index = ShopGetAttributeIndex(shopSystem_.get(), oix, hrl_tag);
      CHECK(ShopAttributeIsDefault(shopSystem_.get(), oix, attribute_index));
      CHECK(ShopCheckAttributeToSet(shopSystem_.get(), attribute_index));
      CHECK(ShopSetDoubleAttribute(shopSystem_.get(), oix, attribute_index, hrl));
      CHECK(!ShopAttributeIsDefault(shopSystem_.get(), oix, attribute_index));
      double read_hrl;
      ShopGetDoubleAttribute(shopSystem_.get(), oix, attribute_index, read_hrl);
      CHECK_EQ(read_hrl, hrl);
    }
    SUBCASE("Set hrl by index for reservoir") {
      auto hrl_tag{"hrl"};
      auto const hrl{200.};
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_name, reservoir_name);
      int hrl_index = ShopGetAttributeIndex(shopSystem_.get(), oix, hrl_tag);
      CHECK(ShopAttributeIsDefault(shopSystem_.get(), oix, hrl_index));
      CHECK(ShopCheckAttributeToSet(shopSystem_.get(), hrl_index));
      CHECK(ShopSetDoubleAttribute(shopSystem_.get(), oix, hrl_index, hrl));
      CHECK(!ShopAttributeIsDefault(shopSystem_.get(), oix, hrl_index));
      double read_hrl;
      ShopGetDoubleAttribute(shopSystem_.get(), oix, hrl_index, read_hrl);
      CHECK_EQ(read_hrl, hrl);
    }

    SUBCASE("Set p_min by name for reservoir") {
      auto pmin_tag{"p_min"};
      auto const oix = ShopAddObject(shopSystem_.get(), reservoir_type_name, reservoir_name);
#if 1 // Version 13.2.1.d (2020.06.04) throws exception
      CHECK_THROWS(ShopGetAttributeIndex(shopSystem_.get(), oix, pmin_tag));
#else
      auto const attribute_index = ShopGetAttributeIndex(shopSystem_.get(), oix, pmin_tag);
      CHECK_EQ(attribute_index, -1);
#endif
    }

    SUBCASE("Set reservoir curve") {
      auto const id = ShopAddObject(shopSystem_.get(), reservoir_type_name, reservoir_name);

      double a[] = {10, 20, 30};
      double b[] = {11, 22, 33};
      auto flow_descr{"flow_descr"};
      auto const attribute_index = ShopGetAttributeIndex(shopSystem_.get(), id, flow_descr);
      CHECK(ShopCheckAttributeToSet(shopSystem_.get(), attribute_index));
      auto const success_set = ShopSetXyAttribute(shopSystem_.get(), id, attribute_index, 123, 3, a, b);

      // dummy, will just try to get the values out again
      int number_of_points{};
      ShopGetXyAttributeNPoints(shopSystem_.get(), id, attribute_index, number_of_points);

      double ref_value{};

      std::vector<double> abba(number_of_points);
      auto const x = &abba[0];

      std::vector<double> babba(number_of_points);
      auto const y = &babba[0];

      auto const success_get = ShopGetXyAttribute(
        shopSystem_.get(), id, attribute_index, ref_value, number_of_points, x, y);

      CHECK(success_set);
      CHECK(success_get);
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/generator_with_attributes") {
    auto generator_name{"alta g1"};
    auto generator_type_name = shop_type::generator;
    shop_time t_start{"20180910000000000", true}, t_end{"20180911000000000", true};
    ShopSetTimeResolution(shopSystem_.get(), t_start, t_end, ::shop::data::shop_time_unit::hour);
    SUBCASE("Add generator by type name with attributes") {
      auto pmin_tag{"p_min"};
      auto pmax_tag{"p_max"};
      auto pnom_tag{"p_nom"};
      auto fcr_n_discrete_droop_values{
        "fcr_n_discrete_droop_values"}; // fcr_n_discrete_droop_values fcr_d_up_discrete_droop_values
      auto const pmin{2.};
      auto const pnom{180.0};
      auto const pmax{200.};
      std::vector<double> d;
      d.push_back(3.0);
      d.push_back(4.0);
      d.push_back(5.0);
      int n = static_cast<int>(d.size());
      auto const id = ShopAddObject(shopSystem_.get(), generator_type_name, generator_name);
      auto const pmin_index = ShopGetAttributeIndex(shopSystem_.get(), id, pmin_tag);
      auto const pmax_index = ShopGetAttributeIndex(shopSystem_.get(), id, pmax_tag);
      auto const pnom_index = ShopGetAttributeIndex(shopSystem_.get(), id, pnom_tag);
      auto const droops_tag = ShopGetAttributeIndex(shopSystem_.get(), id, fcr_n_discrete_droop_values);
      CHECK(ShopSetDoubleAttribute(shopSystem_.get(), id, pmin_index, pmin));
      CHECK(ShopSetDoubleAttribute(shopSystem_.get(), id, pnom_index, pnom));
      CHECK(ShopSetDoubleAttribute(shopSystem_.get(), id, pmax_index, pmax));
      CHECK(!ShopAttributeExists(shopSystem_.get(), id, droops_tag));
      CHECK(ShopCheckAttributeToSet(shopSystem_.get(), droops_tag));
      CHECK(ShopSetDoubleArrayAttribute(shopSystem_.get(), id, droops_tag, n, const_cast<double*>(d.data())));

      double pmin_read = -DBL_MAX;
      double pmax_read = -DBL_MAX;
      std::vector<double> v;
      v.resize(n, 0.0);
      CHECK(ShopGetDoubleAttribute(shopSystem_.get(), id, pmin_index, pmin_read));
      CHECK_EQ(pmin, pmin_read);
      CHECK(ShopGetDoubleAttribute(shopSystem_.get(), id, pmax_index, pmax_read));
      CHECK_EQ(pmax, pmax_read);
      CHECK(ShopGetDoubleArrayAttribute(shopSystem_.get(), id, droops_tag, n, v.data()));
      CHECK_EQ(v, d);
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/hourly_fixed_timeseries") {
    // Hourly time resolution, hour is base unit
    shop_time start_time{"20180101000000000", true};
    shop_time end_time{"20180101180000000", true};
    constexpr int n_time_steps{18};
    CHECK(ShopSetTimeResolution(shopSystem_.get(), start_time, end_time, ::shop::data::shop_time_unit::hour));

    constexpr auto const market_type_index{11};
    auto market_name{"TheMarket"};
    auto const market_ix = ShopAddObject(shopSystem_.get(), market_type_index, market_name);
    auto buy_price{"buy_price"};
    int const attr_ix = ShopGetAttributeIndex(shopSystem_.get(), market_ix, buy_price);

    CHECK_FALSE(ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
    CHECK(ShopCheckAttributeToSet(shopSystem_.get(), attr_ix));

    constexpr int n_scen = 1;
    constexpr int n_points = 3;
    auto t = std::to_array({0, 6, 12});
    auto y = std::to_array({48.1, 40.1, 35.1});

    int* t_ptr = &t[0];
    double* y_ptr = &y[0];
    double** y_pptr = &y_ptr;

    CHECK(ShopSetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));

    CHECK(ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
    CHECK(ShopCheckAttributeToGet(shopSystem_.get(), attr_ix));

    char start_time_get[18];
    int n_scen_get, n_points_get;
    CHECK(
      ShopGetTxyAttributeDimensions(shopSystem_.get(), market_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
    CHECK_EQ(strncmp(start_time_get, start_time, strlen(start_time)), 0);
    CHECK_EQ(n_scen_get, n_scen);
    CHECK_EQ(
      n_points_get,
      n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!

    int* t_get = new int[n_time_steps];
    double* y_get[n_scen];
    y_get[0] = new double[n_time_steps];
    double** y_get_pptr = &y_get[0];
    CHECK(ShopGetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

    // Compare the expanded time series data we got with the compressed break point data we set
    for (int get_t = 0, set_i = 0; get_t < n_points_get; ++get_t) {
      if (set_i < n_points - 1) {
        if (get_t >= t[set_i + 1]) {
          ++set_i;
        }
      }
      CHECK_EQ(y_get[0][get_t], y[set_i]);
    }

    delete[] t_get;
    delete[] y_get[0];

#if 0
        // Additional test of re-setting attribute with 0 values: As of Shop API v0.3.0 this leads to ts filled with garbled data (values like -7.8459077161097183e+298)
        CHECK(ShopSetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, start_time, 0, n_scen, nullptr, nullptr));

        CHECK(ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shopSystem_.get(), attr_ix));

        t_get = new int[n_time_steps];
        y_get[0] = new double[n_time_steps];
        y_get_pptr = &y_get[0];
        CHECK(ShopGetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_t = 0, set_i = 0; get_t < n_points_get; ++get_t) {
            if (set_i < n_points - 1) {
                if (get_t >= t[set_i + 1]) {
                    ++set_i;
                }
            }
            CHECK_EQ(y_get[0][get_t], y[set_i]);
        }

        delete[] t_get;
        delete[] y_get[0];

#endif
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/timeseries_with_nan") {
    // Similar to above, but now with nan values.
    // Documenting current behavior of shop api:
    //  - Accepts std::nan as input.
    //  - Uses the number 1e40 internally to represent nan values,
    //    so an input of std::nan and an input of 1e40 is equivalent.
    //  - Returns the number 1e40 as a representation of nan in output.
    shop_time start_time{"20180101000000000", true};
    shop_time end_time{"20180101180000000", true};
    constexpr int n_time_steps{18};

    CHECK(ShopSetTimeResolution(shopSystem_.get(), start_time, end_time, ::shop::data::shop_time_unit::hour));

    constexpr auto const market_type_index{11};
    auto market_name{"TheMarket"};
    auto const market_ix = ShopAddObject(shopSystem_.get(), market_type_index, market_name);
    auto buy_price{"buy_price"};
    int const attr_ix = ShopGetAttributeIndex(shopSystem_.get(), market_ix, buy_price);

    CHECK_FALSE(ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
    CHECK(ShopCheckAttributeToSet(shopSystem_.get(), attr_ix));

    constexpr int n_scen = 1;
    constexpr int n_points = 4;
    std::array t{0, 2, 4, 6};
    std::array<std::array<double, n_points>, 1> y_input{{{1.23, std::nan(""), 1e40, 9.87}}};
    // Note: Where we put nan shop will actually use 1e40 internally.
    std::array y_output{1.23, 1e40, 1e40, 9.87};
    int* t_ptr = &t[0];
    double* y_ptr = &y_input[0][0];
    double** y_pptr = &y_ptr;

    CHECK(ShopSetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));

    CHECK(ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
    CHECK(ShopCheckAttributeToGet(shopSystem_.get(), attr_ix));

    char start_time_get[18];
    int n_scen_get, n_points_get;
    CHECK(
      ShopGetTxyAttributeDimensions(shopSystem_.get(), market_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
    CHECK_EQ(strncmp(start_time_get, start_time, strlen(start_time)), 0);
    CHECK_EQ(n_scen_get, n_scen);
    CHECK_EQ(
      n_points_get,
      n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!

    std::vector<int> t_get(n_time_steps);
    std::vector<double> y_get(n_time_steps);
    std::vector<double*> y_get_pptr(n_scen_get);
    y_get_pptr[0] = y_get.data();
    CHECK(ShopGetTxyAttribute(
      shopSystem_.get(), market_ix, attr_ix, n_points_get, n_scen_get, t_get.data(), y_get_pptr.data()));

    // Compare the expanded time series data we got with the compressed break point data we set
    for (int get_t = 0, set_i = 0; get_t < n_points_get; ++get_t) {
      if (set_i < n_points - 1) {
        if (get_t >= t[set_i + 1]) {
          ++set_i;
        }
      }
      CHECK_EQ(y_get[get_t], y_output[set_i]);
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/hourly_point_timeseries") {
    // Hourly time resolution, minute is base unit
    shop_time start_time{"20180101000000000", true};
    shop_time end_time{"20180101180000000", true};
    constexpr int n_time_steps{18};

    constexpr int timeres_base_units = 60; // Resolution is 1 hour with 60 minute as base unit (set below)!
    constexpr int timeres_n_points_set = 1;
    int timeres_t_set[timeres_n_points_set]{0};
    double timeres_y_set[timeres_n_points_set]{(double) timeres_base_units}; // 60 minutes resolution!
    ShopSetTimeResolution(
      shopSystem_.get(),
      start_time,
      end_time,
      ::shop::data::shop_time_unit::minute, // Minute as base unit!
      timeres_n_points_set,
      &timeres_t_set[0],
      &timeres_y_set[0]);

    constexpr auto const market_type_index{11};
    auto market_name{"TheMarket"};
    auto const market_ix = ShopAddObject(shopSystem_.get(), market_type_index, market_name);
    auto buy_price{"buy_price"};
    int const attr_ix = ShopGetAttributeIndex(shopSystem_.get(), market_ix, buy_price);

    CHECK(!ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
    CHECK(ShopCheckAttributeToSet(shopSystem_.get(), attr_ix));

    constexpr int n_scen = 1;
    constexpr int n_points = 3;
    std::array t{
      0 * timeres_base_units,
      6 * timeres_base_units,
      12 * timeres_base_units}; // Minute is base unit: 6*60 is after 6 hours, 12*60 is after 12 hours!
    std::array<std::array<double, n_points>, 1> y{{{48.1, 40.1, 35.1}}};
    int* t_ptr = &t[0];
    double* y_ptr = &y[0][0];
    double** y_pptr = &y_ptr;

    CHECK(ShopSetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));

    CHECK(ShopAttributeExists(shopSystem_.get(), market_ix, attr_ix));
    CHECK(ShopCheckAttributeToGet(shopSystem_.get(), attr_ix));

    char start_time_get[18];
    int n_scen_get, n_points_get;
    CHECK(
      ShopGetTxyAttributeDimensions(shopSystem_.get(), market_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
    CHECK_EQ(strncmp(start_time_get, start_time, strlen(start_time)), 0);
    CHECK_EQ(n_scen_get, n_scen);
    CHECK_EQ(
      n_points_get,
      n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!

    int* t_get = new int[n_time_steps];
    double* y_get[n_scen];
    y_get[0] = new double[n_time_steps];
    double** y_get_pptr = &y_get[0];
    CHECK(ShopGetTxyAttribute(shopSystem_.get(), market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

    // Compare the expanded time series data we got with the compressed break point data we set
    for (int get_t = 0, set_i = 0; get_t < n_points_get; get_t += timeres_base_units) {
      if (set_i < n_points - 1) {
        if (get_t >= t[set_i + 1]) {
          ++set_i;
        }
      }
      CHECK_EQ(y_get[0][get_t], y[0][set_i]);
    }
    delete[] t_get;
    delete[] y_get[0];
  }

  TEST_SUITE_END();
}