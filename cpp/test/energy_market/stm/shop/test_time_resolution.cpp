#include <shyft/energy_market/stm/shop/api/shop_proxy.h>

#include <test/energy_market/stm/shop/fixture.h>

namespace shyft::energy_market::stm::shop {

  TEST_SUITE_BEGIN("stm/shop");

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/no_time_resolution_set") {
    char startTimeOutput[18]{}, endTimeOutput[18]{};
    char timeUnitOutput[18]{};
    int timeResNPoints = 10;
    int timeResTOutput[10]{};
    double timeResYOutput[10]{};
    int nPoints = -1;
    CHECK(!GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput, nPoints));
    CHECK(!ShopGetTimeResolution(shopSystem_.get(), startTimeOutput, endTimeOutput, timeUnitOutput));
    CHECK(!ShopGetTimeResolution(
      shopSystem_.get(),
      startTimeOutput,
      endTimeOutput,
      timeUnitOutput,
      timeResNPoints,
      &timeResTOutput[0],
      &timeResYOutput[0]));
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/time_resolution_static") {
    auto startTimeInput = "20010101000000000";
    auto endTimeInput = "20020101000000000";

    // Setting a time resolution of one year, unit as lowercase "hours"
    CHECK(ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, ::shop::data::shop_time_unit::hour));
    // Retrieving fixed time resolution from Shop should give us same as our input.
    std::string startTimeOutput(::shop::data::shop_time_size, '\0'), endTimeOutput(::shop::data::shop_time_size, '\0');
    std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::hour, timeUnitOutput.c_str()) == 0);
    // Retrieving it as a dynamic resolution (with only one point at start of period) should also work
    int nPoints{-1};
    startTimeOutput[0] = 0;
    CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
    CHECK(
      strcmp("", startTimeOutput.c_str())
      == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
    REQUIRE(nPoints == 1);
    startTimeOutput[0] = 0;
    endTimeOutput[0] = 0;
    int timeResNPoints{1};
    int timeResTOutput{0};
    double timeResYOutput{0};
    CHECK(ShopGetTimeResolution(
      shopSystem_.get(),
      startTimeOutput.data(),
      endTimeOutput.data(),
      timeUnitOutput.data(),
      timeResNPoints,
      &timeResTOutput,
      &timeResYOutput));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::hour, timeUnitOutput.c_str()) == 0);
    CHECK(timeResTOutput == 0);   // First time step
    CHECK(timeResYOutput == 1.0); // One hour resolution

    // Setting the same time period but minute or second as unit
    CHECK(ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, ::shop::data::shop_time_unit::minute));
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::minute, timeUnitOutput.c_str()) == 0);

    CHECK(ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, ::shop::data::shop_time_unit::second));
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::second, timeUnitOutput.c_str()) == 0);

    // Setting any other units should fail, and retrieving
    // should give our previous setting - no change!

    CHECK_THROWS(
      ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, ::shop::data::shop_time_unit::week));

    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::second, timeUnitOutput.c_str()) == 0);

    CHECK_THROWS(
      ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, ::shop::data::shop_time_unit::month));
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::second, timeUnitOutput.c_str()) == 0);

    CHECK_THROWS(
      ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, ::shop::data::shop_time_unit::year));
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::second, timeUnitOutput.c_str()) == 0);

    auto timeUnitInput = "HOUR";
    CHECK_THROWS(ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, timeUnitInput));
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::second, timeUnitOutput.c_str()) == 0);

    auto timeUnitInputX = "!df_dx"; // Something random...
    CHECK_THROWS(ShopSetTimeResolution(shopSystem_.get(), startTimeInput, endTimeInput, timeUnitInputX));
    CHECK(
      ShopGetTimeResolution(shopSystem_.get(), startTimeOutput.data(), endTimeOutput.data(), timeUnitOutput.data()));
    CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
    CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
    CHECK(strcmp(::shop::data::shop_time_unit::second, timeUnitOutput.c_str()) == 0);
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/time_resolution_dynamic_constant") {
    auto startTimeInput = "20010101000000000";
    auto endTimeInput = "20010102000000000";
    auto timeUnitHour = ::shop::data::shop_time_unit::hour;
    auto timeUnitMinute = ::shop::data::shop_time_unit::minute;
    SUBCASE("Constant one hour resolution") {
      constexpr int timeResNPointsInput = 1;
      int timeResTInput[timeResNPointsInput]{0};
      double timeResYInput[timeResNPointsInput]{1.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitHour,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps = 24; // 1 hour intervals in 1 day period gives 24 steps
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      fmt::print("{}\n", startTimeOutput);
      fmt::print("{}\n", startTimeOutput.c_str());
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitHour, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      for (int i = 0; i < timeResNPointsOutput; ++i) {
        CHECK(timeResTOutput[i] == i);
        CHECK(timeResYOutput[i] == timeResYInput[0]);
      }
    }
    SUBCASE("Constant three hour resolution") {
      constexpr int timeResNPointsInput = 1;
      int timeResTInput[timeResNPointsInput]{0};
      double timeResYInput[timeResNPointsInput]{3.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitHour,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps = 8; // 3 hour interval in 1 day period gives 8 steps
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitHour, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      for (int i = 0; i < timeResNPointsOutput; ++i) {
        CHECK(timeResTOutput[i] == i * timeResYInput[0]);
        CHECK(timeResYOutput[i] == timeResYInput[0]);
      }
    }
    SUBCASE("Constant ten hour resolution") {
      constexpr int timeResNPointsInput = 1;
      int timeResTInput[timeResNPointsInput]{0};
      double timeResYInput[timeResNPointsInput]{10.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitHour,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps =
        3; // 10 hour interval in 1 day gives 3 steps (two 10 hour steps and one with the four remaining hours)
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitHour, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      CHECK(timeResTOutput[0] == 0);
      CHECK(timeResYOutput[0] == 10.);
      CHECK(timeResTOutput[1] == 4);
      CHECK(timeResYOutput[1] == 10.);
      CHECK(timeResTOutput[2] == 14);
      CHECK(timeResYOutput[2] == 10.);
      CHECK(timeResTOutput[3] == 24);
      CHECK(timeResYOutput[3] == 10.);
    }
    SUBCASE("Constant fifteen minutes resolution") {
      constexpr int timeResNPointsInput = 1;
      int timeResTInput[timeResNPointsInput]{0};
      double timeResYInput[timeResNPointsInput]{15.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitMinute,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps = 4 * 24; // 15 minute interval in 1 day period gives 96 steps
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitMinute, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      for (int i = 0; i < timeResNPointsOutput; ++i) {
        CHECK(timeResTOutput[i] == i * timeResYInput[0]);
        CHECK(timeResYOutput[i] == timeResYInput[0]);
      }
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/time_resolution_dynamic_changing") {
    auto startTimeInput = "20010101000000000";
    auto endTimeInput = "20010102000000000";
    auto timeUnitHour = ::shop::data::shop_time_unit::hour;
    auto timeUnitMinute = ::shop::data::shop_time_unit::minute;
    SUBCASE("One hour then three hours") {
      constexpr int timeResNPointsInput = 2;
      int timeResTInput[timeResNPointsInput]{0, 6};
      double timeResYInput[timeResNPointsInput]{1., 3.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitHour,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps = 12; // 1 hour intervals in 6 steps then 3 hour intervals in 18 hours
                                            // corresponding to 6 steps, gives total 12 steps
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitHour, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      int i = 0;
      for (; i < 6; ++i) {
        CHECK(timeResTOutput[i] == i * timeResYInput[0]);
        CHECK(timeResYOutput[i] == timeResYInput[0]);
      }
      for (; i < timeResNPointsOutput; ++i) {
        CHECK(timeResTOutput[i] == timeResTInput[1] + (i - 6) * timeResYInput[1]);
        CHECK(timeResYOutput[i] == timeResYInput[1]);
      }
    }
    SUBCASE("One hour then three hours then six hours") {
      constexpr int timeResNPointsInput = 3;
      int timeResTInput[timeResNPointsInput]{0, 6, 12};
      double timeResYInput[timeResNPointsInput]{1., 3., 6.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitHour,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps =
        10; // 1 hour intervals in 6 steps then 3 hour intervals in 6 hours corresponding to 2 steps, then 6 hour
            // intervals in 12 hours corresponding to 2 steps, gives total 10 steps.
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitHour, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      int i = 0;
      for (; i < 6; ++i) {
        CHECK(timeResTOutput[i] == i * timeResYInput[0]);
        CHECK(timeResYOutput[i] == timeResYInput[0]);
      }
      for (; i < 8; ++i) {
        CHECK(timeResTOutput[i] == timeResTInput[1] + (i - 6) * timeResYInput[1]);
        CHECK(timeResYOutput[i] == timeResYInput[1]);
      }
      for (; i < timeResNPointsOutput; ++i) {
        CHECK(timeResTOutput[i] == timeResTInput[2] + (i - 8) * timeResYInput[2]);
        CHECK(timeResYOutput[i] == timeResYInput[2]);
      }
    }
    SUBCASE("15 minutes then one hour then 3 hours") {
      constexpr int timeResNPointsInput = 3;
      int timeResTInput[timeResNPointsInput]{0, 6 * 60, 12 * 60};
      double timeResYInput[timeResNPointsInput]{15., 60., 180.};
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(),
        startTimeInput,
        endTimeInput,
        timeUnitMinute,
        timeResNPointsInput,
        &timeResTInput[0],
        &timeResYInput[0]));
      // const int expectedTimeSteps2 = calculateTimeSteps(24*60, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps =
        34; // 15 minute intervals in 6 hours (24 steps) then 1 hour intervals in 6 hours,
            // then 3 hour intervals in 12 hours corresponding, gives total 34 steps.
      int nPoints{-1};
      std::string startTimeOutput(::shop::data::shop_time_size, '\0');
      CHECK(GetTimeResolutionDimensions(shopSystem_.get(), startTimeOutput.data(), nPoints));
      CHECK(
        strcmp("", startTimeOutput.c_str())
        == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
      REQUIRE(nPoints == expectedTimeSteps + 1);
      startTimeOutput[0] = 0;
      std::string endTimeOutput(::shop::data::shop_time_size, '\0');
      std::string timeUnitOutput(::shop::data::time_unit_sz, '\0');
      constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
      int timeResTOutput[timeResNPointsOutput]{};
      double timeResYOutput[timeResNPointsOutput]{};
      CHECK(ShopGetTimeResolution(
        shopSystem_.get(),
        startTimeOutput.data(),
        endTimeOutput.data(),
        timeUnitOutput.data(),
        timeResNPointsOutput,
        &timeResTOutput[0],
        &timeResYOutput[0]));
      CHECK(strcmp(startTimeInput, startTimeOutput.c_str()) == 0);
      CHECK(strcmp(endTimeInput, endTimeOutput.c_str()) == 0);
      CHECK(strcmp(timeUnitMinute, timeUnitOutput.c_str()) == 0);
      CHECK(nPoints == timeResNPointsOutput);
      int i = 0;
      for (; i < 24; ++i) {
        CHECK(timeResTOutput[i] == i * timeResYInput[0]);
        CHECK(timeResYOutput[i] == timeResYInput[0]);
      }
      for (; i < 30; ++i) {
        CHECK(timeResTOutput[i] == timeResTInput[1] + (i - 24) * timeResYInput[1]);
        CHECK(timeResYOutput[i] == timeResYInput[1]);
      }
      for (; i < timeResNPointsOutput; ++i) {
        CHECK(timeResTOutput[i] == timeResTInput[2] + (i - 30) * timeResYInput[2]);
        CHECK(timeResYOutput[i] == timeResYInput[2]);
      }
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/time_index_lookups") {
    auto oneYearBeforeStartTime = "20010101000000000";
    auto startTime = "20010102000000000";
    auto startTimePlusOneHour = "20010102010000000";
    auto startTimePlusTenHours = "20010102100000000";
    auto startTimePlusTenHoursAndThirtyMinutes = "20010102103000000";
    auto startTimePlusTenHoursAndFiftyNine = "20010102105959999";
    auto startTimePlusOneDay = "20010103000000000";
    auto startTimePlusOneDayAndSixHours = "20010103060000000";
    auto startTimePlusFourDays = "20010106000000000";
    auto endTime = "20010109000000000"; // 1 week (168 hours) after startTime
    auto oneDayAfterEndTime = "20010110000000000";
    auto timeUnit = "hour";
    constexpr int timePeriodUnits = 24 * 7;
    SUBCASE("Fixed resolution") {
      // Setting fixed time resolution of one year
      REQUIRE(ShopSetTimeResolution(shopSystem_.get(), startTime, endTime, timeUnit));
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), oneYearBeforeStartTime) == -1);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTime) == 0);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusOneHour) == 1);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusTenHours) == 10);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusTenHoursAndThirtyMinutes) == 10);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusTenHoursAndFiftyNine) == 10);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), endTime) == timePeriodUnits);
      CHECK(
        ShopConvertStringToTimeIndex(shopSystem_.get(), oneDayAfterEndTime)
        == -2); // Returns -2 in v13.2.1.d; -1 in previous versions.
    }
    SUBCASE("Dynamic resolution") {
      constexpr int timeResNPointsInput = 3;
      int timeResTInput[timeResNPointsInput]{0, 24, 72};
      double timeResYInput[timeResNPointsInput]{1., 3., 6.};
      // const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
      constexpr int expectedTimeSteps = 24 + 48 / 3 + 96 / 6;
      REQUIRE(ShopSetTimeResolution(
        shopSystem_.get(), startTime, endTime, timeUnit, timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), oneYearBeforeStartTime) == -1);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTime) == 0);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusOneHour) == 1);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusTenHours) == 10);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusTenHoursAndThirtyMinutes) == 10);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusTenHoursAndFiftyNine) == 10);
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusOneDay) == 24);
      CHECK(
        ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusOneDayAndSixHours)
        == 26); // On day 2 we are running 3 hour steps, so 6 hours is 2 steps in day 2
      CHECK(
        ShopConvertStringToTimeIndex(shopSystem_.get(), startTimePlusFourDays)
        == 44); // 24 steps on day one, 16 steps total on day 2 + 3, 4 steps on day 4 = 44
      CHECK(ShopConvertStringToTimeIndex(shopSystem_.get(), endTime) == expectedTimeSteps);
      CHECK(
        ShopConvertStringToTimeIndex(shopSystem_.get(), oneDayAfterEndTime)
        == -2); // Returns -2 in v13.2.1.d; -1 in previous versions.
    }
  }

  TEST_SUITE_END();
}