#include <chrono>
#include <memory>
#include <vector>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

#include <doctest/doctest.h>

namespace shyft::energy_market::stm::shop {
  TEST_SUITE_BEGIN("stm/shop");

  using core::deltahours;
  using core::deltaminutes;
  using core::to_seconds64;
  using core::utctime_from_seconds64;
  using core::create_from_iso8601_string;
  using time_axis::generic_dt;
  using time_series::dd::apoint_ts;
  using time_series::dd::ats_vector;
  using time_series::POINT_AVERAGE_VALUE;
  using time_series::POINT_INSTANT_VALUE;
  using timing = std::chrono::high_resolution_clock;

  auto elapsed_ms = [](timing::time_point t0, timing::time_point t1) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
  };
  auto elapsed_us = [](timing::time_point t0, timing::time_point t1) {
    return std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();
  };

  template <int scale>
  struct unit { // Simple stand-alone version of shop::proxy::unit

    template <typename V>
    static constexpr V to_base(V v) {
      return v * scale;
    };

    template <typename V>
    static constexpr V from_base(V v) {
      return v / scale;
    };
  };

  TEST_CASE("stm/shop/test_ts_generation") {
    core::calendar const calendar{"Europe/Oslo"};
    auto const t_begin = create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_step = deltahours(1);
    auto const shop_step_resolution = deltaminutes(1);
    auto const start_time_expected = static_cast<::shop::data::shop_time>(to_seconds64(t_begin));

    auto data_for_test = [&]<typename v_unit>(int const n, double const v_begin, std::type_identity<v_unit>) {
      std::vector<core::utctime> ta_utc_points(n + 1);
      std::vector<time_t> t_axis(n + 1);
      std::vector<int> t_shop(n);
      std::vector<double> v(n);
      auto y_expected = std::vector<double>(n);
      auto targets = std::views::zip(ta_utc_points, t_axis, t_shop, v, y_expected);
      std::ranges::transform(std::views::iota(0, n), std::ranges::begin(targets), [&](auto const i) {
        auto t_now = t_begin + i * t_step;
        auto v_now = v_begin * i;
        return std::make_tuple(
          t_now, to_seconds64(t_now), i * (t_step / shop_step_resolution), v_now, v_unit::from_base(v_now));
      });
      t_axis[n] = to_seconds64(t_begin + n * t_step);
      ta_utc_points[n] = t_begin + n * t_step;
      return std::make_tuple(generic_dt{ta_utc_points}, t_axis, t_shop, v, y_expected);
    };


    SUBCASE("basic") {
      // Configuration
      int const n{10};
      double const v_begin{3.0};
      int const v_scale{1};
      using v_unit = unit<v_scale>;
      auto [ta, t_axis, t_shop, v, y_expected] = data_for_test(n, v_begin, std::type_identity<v_unit>{});
      auto expected_ts = apoint_ts(ta, v, POINT_AVERAGE_VALUE);
      // Test conversion of shyft ts to shop data input
      auto [start_time, t_ts, y_ts] = from_ts<v_unit>(expected_ts, t_axis);
      CHECK(start_time == start_time_expected);
      CHECK(t_ts.size() == n);
      for (int i = 0; i < n; ++i) {
        CHECK(t_ts[i] == t_shop[i]);
        CHECK(y_ts[i] == y_expected[i]);
      }
      // Test conversion back to shyft ts
      std::time_t minute = 60L;
      auto ts = to_ts<v_unit>(t_axis, std::move(t_ts), std::move(y_ts), minute);
      CHECK(ts == expected_ts);
    }

    SUBCASE("scaling") {
      // Configuration
      int const n{10};
      double const v_begin{3000000.0};
      int const v_scale{1000000}; // Mega, as in MW
      using v_unit = unit<v_scale>;
      auto [ta, t_axis, t_shop, v, y_expected] = data_for_test(n, v_begin, std::type_identity<v_unit>{});
      auto expected_ts = apoint_ts(ta, v, POINT_AVERAGE_VALUE);
      // Test conversion of shyft ts to shop data
      auto [start_time, t_ts, y_ts] = from_ts<v_unit>(expected_ts, t_axis);
      CHECK(start_time == start_time_expected);
      for (std::size_t i = 0; i < t_shop.size(); ++i) {
        CHECK(t_ts[i] == t_shop[i]);
        CHECK(y_ts[i] == y_expected[i]);
      }
      // Test conversion back to shyft ts
      std::time_t minute = 60L;
      auto ts = to_ts<v_unit>(t_axis, std::move(t_ts), std::move(y_ts), minute);
      CHECK(ts == expected_ts);
    }

    SUBCASE("t_axis_unit") {
      // Configuration
      int const n{10};
      double const v_begin{3000000.0};
      int const v_scale{1000000}; // Mega, as in MW
      using v_unit = unit<v_scale>;
      auto [ta, t_axis, t_shop, v, y_expected] = data_for_test(n, v_begin, std::type_identity<v_unit>{});
      auto expected_ts = apoint_ts(ta, v, POINT_AVERAGE_VALUE);
      auto [start_time, t_ts, y_ts] = from_ts<v_unit>(expected_ts, t_axis);
      std::time_t second = 1L;
      std::time_t minute = 60L;
      std::vector<int> t_ts_seconds(n);
      std::ranges::copy(
        std::views::transform(
          t_ts,
          [&](auto v) {
            return v * minute;
          }),
        t_ts_seconds.data());

      // have to match the incoming t_axis in times after unit conversion
      CHECK_THROWS(to_ts<v_unit>(t_axis, t_ts, y_ts, second));
      CHECK(expected_ts == to_ts<v_unit>(t_axis, t_ts_seconds, y_ts, second));
    }

    SUBCASE("expression_to_shop") {
      // Configuration
      int const n{10000};
      double const v_fill{99.0};
      int const v_scale{1};
      using v_unit = unit<v_scale>;
      auto [ta, t_axis, t_shop, v, y_expected] = data_for_test(n, v_fill, std::type_identity<v_unit>{});

      // Create shyft ts
      auto work_ts = apoint_ts{
        generic_dt{t_begin, t_step, n},
        nan, POINT_AVERAGE_VALUE
      };
      auto override_ts = apoint_ts{
        generic_dt{t_begin, t_step, n},
        v_fill, POINT_AVERAGE_VALUE
      };
      auto qac_ts = work_ts.min_max_check_ts_fill(nan, nan, core::max_utctime, override_ts);
      // Test conversion of shyft ts to shop data txy
      auto [start_time, t_ts, y_ts] = from_ts<v_unit>(qac_ts, t_axis);
      CHECK(start_time_expected == start_time);
      for (std::size_t i = 0; i < t_ts.size(); ++i) {
        CHECK(t_ts[i] == t_shop[i]);
        CHECK(y_ts[i] == v_unit::from_base(v_fill));
      }
    }

    SUBCASE("complex_expression_to_shop") {
      // Configuration
      int const n{5000};
      int const n_tsv{4};
      int const v_scale{1};
      using v_unit = unit<v_scale>;

      // Generate data
      // Making sure to create a point time axis, which gives the more heavy evaluation.
      std::vector<core::utctime> ta_utc_points(n + 1);
      std::vector<time_t> t_axis(n + 1);
      std::vector<int> t_shop(n);
      auto target = std::views::zip(ta_utc_points, t_axis, t_shop);
      std::ranges::transform(std::views::iota(0, n), std::ranges::begin(target), [&](auto const i) {
        auto t_now = t_begin + i * t_step;
        return std::make_tuple(t_now, to_seconds64(t_now), i * (t_step / shop_step_resolution));
      });
      t_axis[n] = to_seconds64(t_begin + n * t_step);
      ta_utc_points[n] = t_begin + n * t_step;
      auto const ta = generic_dt(ta_utc_points);

      // Ccreate a vector of time series, each of them a qac ts,
      // on a source ts which has use_time_axis_from from another qac ts,
      // using replacement values from a third qac ts for replacement.
      auto tsv = ats_vector{};
      tsv.reserve(n_tsv);
      for (auto i = 0; i < n_tsv; ++i) {
        auto work_ts_1 = apoint_ts{ta, nan, POINT_AVERAGE_VALUE};
        auto override_ts_1 = apoint_ts{ta, i + 0.11, POINT_AVERAGE_VALUE};
        auto qac_ts_1 = work_ts_1.min_max_check_ts_fill(nan, nan, core::max_utctime, override_ts_1);

        auto work_ts_2 = apoint_ts{ta, nan, POINT_AVERAGE_VALUE};
        auto override_ts_2 = apoint_ts{ta, i + 0.22, POINT_AVERAGE_VALUE};
        auto qac_ts_2 = work_ts_2.min_max_check_ts_fill(nan, nan, core::max_utctime, override_ts_2);

        auto work_ts_3 = apoint_ts{ta, nan, POINT_AVERAGE_VALUE};
        auto override_ts_3 = apoint_ts{ta, i + 0.33, POINT_AVERAGE_VALUE};
        auto qac_ts_3 = work_ts_2.min_max_check_ts_fill(nan, nan, core::max_utctime, override_ts_3);

        auto qac_ts_123 = qac_ts_3.use_time_axis_from(qac_ts_2).min_max_check_ts_fill(
          nan, nan, core::max_utctime, qac_ts_1);
        auto qac_ts_123_mask = qac_ts_123.inside(0.0, nan, 0.0, 1.0, 0.0);
        auto ts = qac_ts_123 * qac_ts_123_mask;

        tsv.push_back(ts);
      }

      // Create a sum-expression of the time series in the vector,
      // and then a "mask series" as an inside-expression around that sum.
      auto sum_ts = tsv.sum();
      auto sum_ts_mask = sum_ts.inside(0.0, nan, 0.0, 1.0, 0.0);

      // Create another qac ts.
      auto work_ts = apoint_ts{ta, nan, POINT_AVERAGE_VALUE};
      auto override_ts = apoint_ts{ta, 999, POINT_AVERAGE_VALUE};
      auto qac_ts = work_ts.min_max_check_ts_fill(nan, nan, core::max_utctime, override_ts);

      // Create the final ts by "applying" the mask on the above qac ts.
      auto ts = qac_ts * sum_ts_mask;

      auto [start_time, t_ts, y_ts] = from_ts<v_unit>(ts, t_axis);

      // Check results
      CHECK(start_time_expected == start_time);
      for (std::size_t i = 0; i < t_shop.size(); ++i) {
        CHECK(t_ts[i] == t_shop[i]);
        CHECK(y_ts[i] == 999.0);
      }
    }
  }

  TEST_SUITE_END();
}
