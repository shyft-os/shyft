#pragma once
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <functional>
#include <shared_mutex>
#include <string>

#include <shyft/energy_market/stm/shop/shop_bundle_path.h>
#include <shyft/energy_market/stm/shop/shop_init.h>

#include <doctest/doctest.h>
#include <float.h>
#include <limits.h>
#include <shop_lib_interface.h>
#include <string.h>

namespace shyft::energy_market::stm::shop {
  //
  // Additional definitions for unit tests
  //

  struct shop_relation {
    // static constexpr shop_relation_str generator_of_plant{ "generator_of_plant" }; // Note: Deprecated in Shop v14,
    // should use connection_standard instead. static constexpr shop_relation_str pump_of_plant{ "pump_of_plant" }; //
    // Note: Deprecated in Shop v14, should use connection_standard instead.
    static constexpr auto needle_combination_of_generator{"needle_combination_of_generator"};
    static constexpr auto main{"connection_standard"};
    static constexpr auto spill{"connection_spill"};
    static constexpr auto bypass{"connection_bypass"};
  };

  struct shop_type {
    static constexpr auto reservoir{"reservoir"};
    static constexpr auto generator{"generator"};
    static constexpr auto plant{"plant"};
    static constexpr auto thermal{"thermal"};
    static constexpr auto gate{"gate"};
    static constexpr auto market{"market"};
    static constexpr auto scenario{"scenario"};
    static constexpr auto junction{"junction"};
    static constexpr auto junction_gate{"junction_gate"};
    static constexpr auto creek_intake{"creek_intake"};
    static constexpr auto tunnel{"tunnel"};
    static constexpr auto cut{"cut"};
  };

  template <class T> // T = shop::api from shop_proxy.h
  static void set_shop_library_path_for_test(T& api_handle) {
    api_handle.set_library_path(SHYFT_SHOP_LIBDIR);
  }

  static void set_shop_library_path_for_test(ShopSystem* shopSystem) {
    ShopAddDllPath(shopSystem, SHYFT_SHOP_LIBDIR);
  }

  //
  // Common test fixture
  //

  struct test_fixture {
    // Test fixture class used to create a centralized place of calling ShopInit at beginning
    // and ShopFree at end of each test case (that uses this fixture). Could have achived
    // the same by doing this at start and end of a test case and then implement subcases
    // of this, but since we have a lot of cases we avoid the extra nesting level by refactoring
    // it out to a fixture.
    std::unique_ptr<ShopSystem, shyft::energy_market::stm::shop::ShopSystemDeleter> shopSystem_;
    bool silent_;

    test_fixture()
      : shopSystem_(shyft::energy_market::stm::shop::shop_init(SHYFT_SHOP_LICENSEDIR)) {
      silent_ = ShopSetSilentConsole(shopSystem_.get(), true);
      set_shop_library_path_for_test(shopSystem_.get());
    }
  };

}
