#include <test/energy_market/stm/shop/test_server_dstm.h>

namespace shyft::energy_market::stm {

  test_server_dstm::test_server_dstm()
    : srv::dstm::server()
    , bg_server{dispatch.state->models, dispatch.state->sm, dispatch.state->callback, dispatch.state->dtss.get()} {
    dispatch.state->callback = [this](std::string m, std::string a) {
      return fx_handler(m, a);
    };
  }

  test_server_dstm::test_server_dstm(std::string const & root_dir)
    : srv::dstm::server()
    , bg_server{dispatch.state->models, dispatch.state->sm, dispatch.state->callback, dispatch.state->dtss.get()} {
    add_container(*dispatch.state->dtss, "test", root_dir);
  }

  std::int64_t test_server_dstm::start_web_api(
    std::string host_ip,
    int port,
    std::string doc_root,
    int fg_threads,
    int bg_threads) {
    if (!web_srv.valid()) {
      web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads] {
        return web_api::run_web_server(
          bg_server,
          host_ip,
          static_cast<unsigned short>(port),
          std::make_shared<std::string>(doc_root),
          fg_threads,
          bg_threads);
      });
    }
    return bg_server.wait_for_real_port();
  }

  bool test_server_dstm::web_api_running() const {
    return web_srv.valid();
  }

  void test_server_dstm::stop_web_api() {
    if (web_srv.valid()) {
      std::raise(SIGINT);
      (void) web_srv.get();
    }
  }

  bool test_server_dstm::fx_handler(std::string mid, std::string json_arg) {
    fx_mid = mid;
    fx_arg = json_arg;
    return true;
  }

  void clear_manager(srv::dstm::server* dstm) {
    auto& manager = dstm->dispatch.state->compute_manager;
    std::scoped_lock lock{manager.mutex};
    std::ranges::for_each(manager.servers, [](auto& s) {
      if (s->watchdog.valid())
        s->watchdog.get();
    });
    manager.servers.clear();
  }

}
