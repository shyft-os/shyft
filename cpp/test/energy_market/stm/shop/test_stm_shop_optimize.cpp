#include <algorithm>
#include <cstdint>
#include <functional>
#include <ranges>

#include <fmt/core.h>

#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/time/utctime_utilities.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/shop/model_ole_pump.h>
#include <test/energy_market/stm/shop/model_shop_pump_example.h>
#include <test/energy_market/stm/shop/model_simple.h>

namespace shyft::energy_market::stm::shop {

  using time_axis::generic_dt;

  TEST_SUITE_BEGIN("stm/shop");

  auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
  auto const t_step = core::deltahours(1);
  size_t const n_step = 18;
  generic_dt const time_axis{t_begin, t_step, n_step};

  core::utcperiod const t_period{time_axis.total_period()};
  auto const t_end = t_period.end;

  static std::size_t run_id = 1; // Unique number, dont overwrite files from other tests when write_file_commands==true.

  TEST_CASE(
    "stm/shop/optimize_simple_model_without_inlet"
    * doctest::description("building and optmimizing simple model without inlet segment: "
                           "aggregate-penstock-maintunnel-reservoir")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;

    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with expected results
    shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
    check_results(stm, rstm, t_begin, t_end, t_step);
  }

  TEST_CASE(
    "stm/shop/optimize_simple_model_with_inlet"
    * doctest::description("building and optmimizing simple model with inlet segment: "
                           "aggregate-inlet-penstock-maintunnel-reservoir")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);
    shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
    check_results(stm, rstm, t_begin, t_end, t_step);
  }

  TEST_CASE(
    "stm/shop/optimize_simple_model_with_discharge_group"
    * doctest::description("building and optmimizing simple model without explicit values for "
                           "lrl/hrl/maxvol/p_min/p_max/p_nom")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = true;

    // Build models
    auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    // Fetch stm objects connected to shop objects
    auto rsv_ = std::dynamic_pointer_cast<reservoir>(stm->hps.front()->find_reservoir_by_name("reservoir"));
    auto unit_ = std::dynamic_pointer_cast<unit>(stm->hps.front()->find_unit_by_name("aggregate"));
    auto flood_river_ = std::dynamic_pointer_cast<waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute flood river"));
    auto bypass_river_ = std::dynamic_pointer_cast<waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute bypass"));
    auto bypass_gate_ = std::dynamic_pointer_cast<gate>(bypass_river_->gates.front());
    auto river_ = std::dynamic_pointer_cast<waterway>(stm->hps.front()->find_waterway_by_name("waterroute river"));

    // Decorate connection with attrs to enforce discharge group
    river_->discharge.reference = make_constant_ts(t_begin, t_end, 0);
    river_->discharge.constraint.accumulated_max = make_constant_ts(t_begin, t_end, 1e6);
    river_->discharge.penalty.cost.accumulated_max = make_constant_ts(t_begin, t_end, 10);
    river_->deviation.realised = make_constant_ts(t_begin, t_end, 1e6);
    //
    // Set buypass schedule, to check if river_ gets input from both production and bypass
    bypass_gate_->discharge.schedule = make_constant_ts(t_begin, t_end, 10);

    generic_dt time_axis{t_begin, t_step, n_step};
    generic_dt time_axis_accumulate{t_begin, t_step, n_step + 1};
    shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
    shop_system::optimize(*rstm, time_axis, optimization_commands(run_id));
    check_results_with_discharge_group(stm, rstm, t_begin, t_end, t_step);
  }

  TEST_CASE("stm/shop/optimize_contract") {

    bool const always_inlet_tunnels = false;
    bool const use_defaults = true;
    double const mega = 1e6;

    // Build models
    auto stm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model_discharge_group(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    // Fetch stm objects connected to shop objects
    auto mkt_ = stm->market.front();
    auto rsv_ = std::dynamic_pointer_cast<reservoir>(stm->hps.front()->find_reservoir_by_name("reservoir"));
    auto unit_ = std::dynamic_pointer_cast<unit>(stm->hps.front()->find_unit_by_name("aggregate"));
    auto flood_river_ = std::dynamic_pointer_cast<waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute flood river"));
    auto bypass_river_ = std::dynamic_pointer_cast<waterway>(
      stm->hps.front()->find_waterway_by_name("waterroute bypass"));
    auto bypass_gate_ = std::dynamic_pointer_cast<gate>(bypass_river_->gates.front());
    auto river_ = std::dynamic_pointer_cast<waterway>(stm->hps.front()->find_waterway_by_name("waterroute river"));

    SUBCASE("valid") {
      auto ctr_ = std::make_shared<contract>(1, "c1", "{}", stm);
      ctr_->options = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
      // TODO: Not sure what input data is realistic here, have just set something that produced result!
      ctr_->options.get()->emplace(
        time_axis.time(6),
        std::make_shared<hydro_power::xy_point_curve>(hydro_power::xy_point_curve::make(
          {-4.4 * mega,
           -3.3 * mega,
           5.5 * mega,
           6.6 * mega}, // X: Energy volume, negative is sale, positive is buy, unit W in STM, unit MW in Shop
          {32.0 / mega, 33.0 / mega, 34.0 / mega, 50.0 / mega}))); // Y: Price, unit NOK/W in STM, unit NOK/MW in Shop
      ctr_->options.get()->emplace(
        time_axis.time(12),
        std::make_shared<hydro_power::xy_point_curve>(hydro_power::xy_point_curve::make(
          {-4.0 * mega,
           -2.0 * mega,
           5.0 * mega,
           6.0 * mega}, // X: Energy volume, negative is sale, positive is buy, unit W in STM, unit MW in Shop
          {32.0 / mega, 33.0 / mega, 34.0 / mega, 40.0 / mega}))); // Y: Price, unit NOK/W in STM, unit NOK/MW in Shop
      stm->contracts.push_back(ctr_);
      mkt_->contracts.push_back(ctr_);

      shop_system::optimize(*stm, time_axis, optimization_commands(run_id));

      CHECK(exists(ctr_->quantity));
      auto expected_quantity = apoint_ts{
        time_axis::point_dt{{t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
        {6.6 * mega, 6.6 * mega, 6.0 * mega, 6.0 * mega}, // NOTE: The 6.6 value set at time(6) has retroactive effect!?
        time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      check_time_series_at_points(ctr_->quantity, expected_quantity, t_begin, t_step, {0, 6, 12, 17});
    }
    SUBCASE("invalid") {

      SUBCASE("0") {
        auto ctr_ = std::make_shared<contract>(1, "c1", "{}", stm);
        ctr_->options = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
        stm->contracts.push_back(ctr_);
        mkt_->contracts.push_back(ctr_);
        shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
        CHECK(!exists(ctr_->quantity));
      }
      SUBCASE("1") {
        auto ctr_ = std::make_shared<contract>(1, "c1", "{}", stm);
        apoint_ts ctr_ts{
          time_axis::point_dt{{t_begin, t_begin + 1 * t_step}}, 4.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE};
        ctr_->quantity = ctr_ts;
        stm->contracts.push_back(ctr_);
        mkt_->contracts.push_back(ctr_);
        shop_system::optimize(*stm, time_axis, optimization_commands(run_id));
        CHECK(exists(ctr_->quantity));
        CHECK(ctr_->quantity.ts == ctr_ts.ts);
      }
    }
  }

  TEST_CASE(
    "stm/shop/optimize_model_w_3_units_and_reserves"
    * doctest::description("building and optmimizing model with 3 units")) {
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto stm = build_simple_model_n_units_with_reserves(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);

    shop_system shop{time_axis};
    shop.emit(*stm);
    auto cmds = optimization_commands(run_id);
    cmds.insert(cmds.begin(), shop_command::set_universal_mip_on()); // let shop find the ideal combination
    for (auto const &cmd : cmds)
      shop.commander.execute(cmd);
    shop.collect(*stm);
    shop.complete(*stm);
    for (auto &wx : stm->hps.front()->waterways) {
      auto w = std::dynamic_pointer_cast<waterway>(wx);
      if (w) {
        CHECK_UNARY(
          w->discharge.result.size()
          > 0); // verify all water way discharges are filled (done by shop.update call above)
      }
    }

    apoint_ts run_ts(time_axis, 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    auto m = stm->market.front();

    // Verify reserve results: Sum of unit group schedules vs unit results for each reserve type.
    // Assuming the schedule will be followed 100% in correct test, not sure if this will always be true.
    REQUIRE_EQ(stm->unit_groups.size(), 4);
    REQUIRE_EQ(stm->hps.front()->units.size(), 3);
    std::vector reserve_types{unit_group_type::fcr_n_up, unit_group_type::afrr_up};
    for (auto reserve_type : reserve_types) {
      apoint_ts schedule{time_axis, 0.0, time_series::POINT_AVERAGE_VALUE};
      for (auto const &ug : stm->unit_groups) {
        if (ug->group_type == reserve_type) {
          schedule = schedule + ug->obligation.schedule;
        }
      }
      apoint_ts result{time_axis, 0.0, time_series::POINT_AVERAGE_VALUE};
      for (auto const &u_ : stm->hps.front()->units) {
        auto u = std::dynamic_pointer_cast<unit>(u_);
        if (reserve_type == unit_group_type::fcr_n_up) {
          result = result + u->reserve.fcr_n.up.result;
        } else if (reserve_type == unit_group_type::afrr_up) {
          result = result + u->reserve.afrr.up.result;
        }
      }
      CHECK_MESSAGE(schedule == result, fmt::format("Unexpected sum of unit reserves for {}", reserve_type));
    }
  }

  TEST_CASE("stm/shop/optimize_multi_market" * doctest::description("multimarket demand/supply 3 units")) {
    using xy_point = hydro_power::point;
    using xy_points = hydro_power::xy_point_curve;
    using namespace time_series::dd;

    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto stm = build_simple_model_n_units_with_reserves(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, 3);
    // modify the system to use demand/supply tables:
    double const mega = 1e6;
    auto ema = stm->market[0];
    ema->demand.bids = std::make_shared<t_xy_::element_type>( // shop sale opportunities part
      t_xy_::element_type{
        { time_axis.time(0),std::make_shared<xy_points>(vector<xy_point>{{48.0 / mega, 10 * mega}})                            },
        { time_axis.time(6),           std::make_shared<xy_points>(vector<xy_point>{{40.0 / mega, 220.0 * mega}})},
        { time_axis.time(7),
         std::make_shared<xy_points>(
         vector<xy_point>{{30.0 / mega, 120.0 * mega}, {50.0 / mega, 80.0 * mega}, {55.0 / mega, 20.0 * mega}})  },
        { time_axis.time(8),           std::make_shared<xy_points>(vector<xy_point>{{30.0 / mega, 220.0 * mega}})},
        {time_axis.time(12),           std::make_shared<xy_points>(vector<xy_point>{{48.0 / mega, 220.0 * mega}})},
        {             t_end,           std::make_shared<xy_points>(vector<xy_point>{{35.0 / mega, 220.0 * mega}})},
    });
    double const buy = 1.0;                                   // set 0 to close shop ability to buy,
    ema->supply.bids = std::make_shared<t_xy_::element_type>( // shop buy oppportunities part
      t_xy_::element_type{
        { time_axis.time(0),std::make_shared<xy_points>(vector<xy_point>{{148.0 / mega, buy * 10 * mega}})                            },
        { time_axis.time(1),
         std::make_shared<xy_points>(vector<xy_point>{{10.0 / mega, buy * 5 * mega}, {15.0 / mega, buy * 20 * mega}})},
        { time_axis.time(2),            std::make_shared<xy_points>(vector<xy_point>{{50.0 / mega, buy * 10 * mega}})},
        { time_axis.time(6),          std::make_shared<xy_points>(vector<xy_point>{{45.0 / mega, buy * 30.0 * mega}})},
        { time_axis.time(7),          std::make_shared<xy_points>(vector<xy_point>{{37.0 / mega, buy * 30.0 * mega}})},
        {time_axis.time(12),          std::make_shared<xy_points>(vector<xy_point>{{50.0 / mega, buy * 30.0 * mega}})},
        {             t_end,          std::make_shared<xy_points>(vector<xy_point>{{36.0 / mega, buy * 30.0 * mega}})},
    });
    // close primary market object sale/buy parts. zero price, keep load
    ema->max_buy = apoint_ts{time_axis, 0.0, POINT_AVERAGE_VALUE};
    ema->max_sale = apoint_ts{time_axis, 0.0, POINT_AVERAGE_VALUE};

    for (auto const &u_ : stm->hps.front()->units) {
      auto u = std::dynamic_pointer_cast<unit>(u_);
      u->reserve.droop_steps = std::make_shared<t_xy_::element_type>(t_xy_::element_type{
        {time_axis.time(0), std::make_shared<xy_points>(vector<xy_point>{{1, 4}, {2, 6}, {3, 8}, {4, 10}})}
      });
    }
    // modify the market, so that we have
    shop_system shop{time_axis};
    shop.emit(*stm);
    std::vector<shop_command> cmds{
      shop_command::set_universal_mip_on(),
      shop_command::set_droop_discretization_limit(6.0), // verify it works, needs license, otherwise it will fail.
      shop_command::set_method_primal(),
      shop_command::set_code_full(),
      shop_command::start_sim(3),
      shop_command::set_code_incremental(),
      shop_command::start_sim(3),
    };

    shop_global_settings globs = shop.get_global_settings();
    auto lpen = globs.load_penalty_flag.get();
    auto lpenv = globs.load_penalty_cost.get();
    auto g = globs.gravity.get();
    for (auto const &cmd : cmds)
      shop.commander.execute(cmd);
    shop.collect(*stm);

    apoint_ts run_ts(time_axis, 1.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    auto m = stm->market.front();

    // Verify reserve results: Sum of unit group schedules vs unit results for each reserve type.
    // Assuming the schedule will be followed 100% in correct test, not sure if this will always be true.
    REQUIRE_EQ(stm->unit_groups.size(), 4);
    REQUIRE_EQ(stm->hps.front()->units.size(), 3);
    // verify plant instant_max
    ats_vector u_prods;
    for (auto const &u_ : stm->hps.front()->units) {
      u_prods.push_back(std::dynamic_pointer_cast<unit>(u_)->production.result);
    }
    auto pp = std::dynamic_pointer_cast<power_plant>(stm->hps.front()->power_plants.front());
    auto ok_instant_max = (pp->production.instant_max - u_prods.sum()).inside(0.0, 1e9, 0.0, 1.0, 0.0).evaluate();
    CHECK_GE(ok_instant_max.size(), 1);
    for (auto i = 0u; i < ok_instant_max.size(); ++i) {
      CHECK_GT(ok_instant_max.value(i), 0.1);
    }

    std::vector reserve_types{unit_group_type::fcr_n_up, unit_group_type::afrr_up};
    for (auto reserve_type : reserve_types) {
      apoint_ts schedule{time_axis, 0.0, time_series::POINT_AVERAGE_VALUE};
      for (auto const &ug : stm->unit_groups) {
        if (ug->group_type == reserve_type) {
          schedule = schedule + ug->obligation.schedule;
        }
      }
      apoint_ts result{time_axis, 0.0, time_series::POINT_AVERAGE_VALUE};
      for (auto const &u_ : stm->hps.front()->units) {
        auto u = std::dynamic_pointer_cast<unit>(u_);
        if (reserve_type == unit_group_type::fcr_n_up) {
          result = result + u->reserve.fcr_n.up.result;
        } else if (reserve_type == unit_group_type::afrr_up) {
          result = result + u->reserve.afrr.up.result;
        }
      }
      CHECK_MESSAGE(schedule == result, fmt::format("Unexpected sum of unit reserves for {}", reserve_type));
    }
  }

  TEST_CASE("stm/shop/optimize_pumps") {
    SUBCASE("reversible") {
      shop_system shop{time_axis};
      auto system = test_models::build_shop_pump_example(t_begin, t_end);

      CHECK_NOTHROW(shop.emit(*system));
      {
        using sc = shop_command;
        for (auto const &cmd : std::vector{
               sc::set_method_primal(),
               sc::set_code_full(),
               sc::start_sim(3),
               sc::set_code_incremental(),
               sc::start_sim(3)})
          shop.commander.execute(cmd);
      }

      {
        auto log_entries = shop.get_log_buffer();
        auto is_error = [&](auto const &l) {
          return l.severity == log_severity::error;
        };
        auto error_log_entries = std::views::filter(log_entries, is_error);
        REQUIRE(std::ranges::empty(error_log_entries));
      }
      CHECK_NOTHROW(shop.collect(*system));

      REQUIRE(system->hps.size() == 1);
      auto hps = system->hps[0];

      REQUIRE(hps->power_plants.size() == 1);
      auto p0 = std::dynamic_pointer_cast<power_plant>(hps->power_plants[0]);
      REQUIRE(p0);

      auto const &r = p0->production.result;

      auto production_result = std::views::transform(std::views::iota(std::size_t{0}, n_step), [&](auto i) {
        return r(t_begin + i * t_step);
      });
      auto const n_low_price_steps = 6;

      // FIXME: use bind_back & swap greater / less when we are compiling with C++23.
      //        less confusing to read. - jeh
      auto low_price_production_result = std::views::take(production_result, n_low_price_steps);
      CHECK(std::ranges::all_of(low_price_production_result, std::bind_front(std::ranges::greater_equal{}, 0.0)));
      CHECK(std::ranges::any_of(low_price_production_result, std::bind_front(std::ranges::greater{}, 0.0)));

      auto high_price_production_result = std::views::drop(production_result, n_low_price_steps);
      CHECK(std::ranges::all_of(high_price_production_result, std::bind_front(std::ranges::less_equal{}, 0.0)));
      CHECK(std::ranges::any_of(high_price_production_result, std::bind_front(std::ranges::less{}, 0.0)));
    }

    SUBCASE("ole") {

      generic_dt const time_axis{t_begin, core::deltahours(1), 23};

      shop_system shop{time_axis};
      auto system = test_models::build_ole_pump(t_begin);

      CHECK_NOTHROW(shop.emit(*system));


      {
        using sc = shop_command;
        for (auto const &cmd : std::vector{
               sc::set_mipgap(true, 300),
               sc::set_nseg_all(25),
               sc::set_dyn_seg_on(),
               sc::set_fcr_n_equality(true),
               sc::set_power_head_optimization(true),
               sc::set_max_num_threads(8),
               sc::start_sim(3),
               sc::set_code_incremental(),
               sc::start_sim(3)})
          shop.commander.execute(cmd);
      }


      {
        auto log_entries = shop.get_log_buffer();
        auto is_error = [&](auto const &l) {
          return l.severity == log_severity::error;
        };
        auto error_log_entries = std::views::filter(log_entries, is_error);
        REQUIRE(std::ranges::empty(error_log_entries));
      }
      REQUIRE_NOTHROW(shop.collect(*system));
      REQUIRE_NOTHROW(shop.complete(*system));

      REQUIRE(system->hps.size() == 1);
      auto hps = system->hps[0];

      REQUIRE(hps->power_plants.size() == 1);
      auto p0 = std::dynamic_pointer_cast<power_plant>(hps->power_plants[0]);
      REQUIRE(p0);

      REQUIRE(p0->production.result);

      REQUIRE(p0->units.size() == 1);
      auto u0 = std::dynamic_pointer_cast<unit>(p0->units[0]);
      REQUIRE(u0);

      REQUIRE(hps->reservoirs.size() == 2);
      auto r0 = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
      REQUIRE(r0);
      auto r1 = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[1]);
      REQUIRE(r1);

      CHECK(u0->production.result);
      CHECK(u0->discharge.result);

      auto ts_view = [&](auto &&ts) {
        return std::views::transform(std::views::iota(std::size_t{0}, n_step), [&](auto i) {
          return ts(t_begin + i * t_step);
        });
      };
      auto production_result = ts_view(u0->production.result);
      auto discharge_result = ts_view(u0->discharge.result);

      // NOTE:
      //     that pumps are running for at least one timestep
      //     - jeh
      CHECK_MESSAGE(
        std::ranges::all_of(production_result, std::bind_front(std::ranges::greater_equal{}, 0.0)),
        fmt::format("{}", production_result));
      CHECK_MESSAGE(
        std::ranges::any_of(production_result, std::bind_front(std::ranges::greater{}, 0.0)),
        fmt::format("{}", production_result));
      CHECK_MESSAGE(
        std::ranges::all_of(discharge_result, std::bind_front(std::ranges::greater_equal{}, 0.0)),
        fmt::format("{}", discharge_result));
      CHECK_MESSAGE(
        std::ranges::any_of(discharge_result, std::bind_front(std::ranges::greater{}, 0.0)),
        fmt::format("{}", discharge_result));
    }
  }

  TEST_CASE("stm/shop/initial_state") {

    auto const t_early = core::create_from_iso8601_string("2017-01-01T01:00:00Z");
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const dt = core::deltahours(1);
    std::size_t const n_t = (t_end - t_begin) / dt;
    time_axis::generic_dt const ta(t_begin, dt, n_t);

    auto model = std::make_shared<stm_system>(1, "");

    auto hps = std::make_shared<stm_hps>(1, "h0", "", model);
    model->hps.push_back(hps);

    auto rsv = std::make_shared<reservoir>(1, "r0", "", hps);

    rsv->volume_level_mapping = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
    rsv->volume_level_mapping->emplace(
      t_begin,
      std::make_shared<hydro_power::xy_point_curve>(xy_point_curve{
        .points{{1.0e6, 10.0}, {10.0e6, 100.0}}
    }));
    rsv->level.realised = make_constant_ts(t_early, t_end, 50.0);

    hps->reservoirs.push_back(rsv);

    auto penstock0 = std::make_shared<waterway>(1, "t0", "", hps);
    penstock0->head_loss_coeff = make_constant_ts(t_begin, t_end, 1.0e-6);
    hps->waterways.push_back(penstock0);

    auto inlet0 = std::make_shared<waterway>(2, "t1", "", hps);
    inlet0->head_loss_coeff = make_constant_ts(t_begin, t_end, 1.0e-6);
    hps->waterways.push_back(inlet0);

    auto gen0 = std::make_shared<unit>(1, "g0", "", hps);
    hps->units.push_back(gen0);

    connect(penstock0).input_from(rsv);
    connect(inlet0).input_from(penstock0).output_to(gen0);

    gen0->generator_description = std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>();
    gen0->generator_description->emplace(
      t_begin,
      std::make_shared<hydro_power::xy_point_curve>(hydro_power::xy_point_curve{
        .points{{2.0e6, 96.0}, {3.0e6, 97.0}, {4.0e6, 98.0}, {5.0e6, 96.0}}
    }));

    {
      auto gen0_desc = std::make_shared<hydro_power::turbine_description>();
      gen0_desc->operating_zones.push_back(hydro_power::turbine_operating_zone{{xy_point_curve_with_z{
        xy_point_curve{.points{{20.0, 70.0}, {40.0, 80.0}, {60.0, 95.0}, {80.0, 80.0}}}, 50.0}}});
      gen0->turbine_description =
        std::make_shared<std::map<utctime, std::shared_ptr<hydro_power::turbine_description>>>();

      gen0->turbine_description->emplace(t_begin, gen0_desc);
    }


    auto draft_tube0 = std::make_shared<waterway>(3, "t3", "", hps);
    draft_tube0->head_loss_coeff = make_constant_ts(t_begin, t_end, 1.0e-6);
    hps->waterways.push_back(draft_tube0);
    auto outlet0 = std::make_shared<waterway>(4, "r0", "", hps);
    hps->waterways.push_back(outlet0);
    connect(draft_tube0).input_from(gen0).output_to(outlet0);
    connect(outlet0).input_from(draft_tube0);

    auto plant0 = std::make_shared<power_plant>(1, "P0", "", hps);
    hps->power_plants.push_back(plant0);
    power_plant::add_unit(plant0, gen0);

    auto market = std::make_shared<energy_market_area>(1, "m1", "", model);
    model->market.push_back(market);
    market->price = make_constant_ts(t_early, t_end, 10.0e-6);
    {
      auto market_units = model->add_unit_group(1, "mg", "", unit_group_type::production);
      market->set_unit_group(market_units);
      market_units->add_unit(gen0, make_constant_ts(t_begin, t_end, 1.0));
      market_units->obligation.cost = make_constant_ts(t_begin, t_end, 10.0);
    }
    // lower than price, should produce.
    rsv->inflow.schedule = make_constant_ts(t_begin, t_end, 60);
    rsv->water_value.endpoint_desc = make_constant_ts(t_begin, t_end, 5e-6);

    gen0->cost.start = make_constant_ts(t_early, t_end, 10e9);

    auto run = [&] {
      shop_system::optimize(
        *model,
        time_axis,
        {
          shop_command::set_code_full(),
          shop_command::start_sim(1),
          shop_command::set_code_incremental(),
          shop_command::start_sim(1),
        });
    };

    SUBCASE("not_set") {
      run();
      CHECK(gen0->production.result(t_begin) == 0);
    }
    SUBCASE("set_and_running") {
      gen0->production.realised = make_constant_ts(t_early, t_end, 1e6);
      run();
      CHECK(gen0->production.result(t_begin) > 0);
    }
    SUBCASE("set_and_not_running") {
      gen0->production.realised = make_constant_ts(t_early, t_end, 0);
      run();
      CHECK(gen0->production.result(t_begin) == 0);
    }
    SUBCASE("set_and_no_data") {
      gen0->production.realised = make_constant_ts(t_early, t_begin - core::deltahours(24), 0);
      run();
      CHECK(gen0->production.result(t_begin) == 0);
    }
  }

  TEST_SUITE_END();
}
