#include <fstream>
#include <memory>
#include <string>

#include <shyft/energy_market/stm/shop/shop_bundle_path.h>
#include <shyft/energy_market/stm/shop/shop_init.h>

#include <shop_cxx_api_generator.h>
#include <test/energy_market/stm/shop/fixture.h>

namespace shyft::energy_market::stm::shop {

  using namespace std::string_literals;

  TEST_SUITE_BEGIN("stm/shop");

  /**
   * The intention here is to generate the new api, then compare it with the existing one, and fail if different.
   * currently we just generate the new shop_enums.new.h
   */
  TEST_CASE("stm/shop/generate_shop_cxx_api_headers") {
    std::string generated_file_name = "shop_enums.new.h";
    std::string proxy_file_name = "api.new.h";
    std::ofstream ofs(generated_file_name, std::ofstream::out);
    std::ofstream pofs(proxy_file_name, std::ofstream::out);
    REQUIRE(ofs);
    REQUIRE(pofs);
    std::unique_ptr<ShopSystem, bool (*)(ShopSystem*)> shop_safe(shop_init(SHYFT_SHOP_LICENSEDIR), ShopFree);
    ShopSystem* shop = shop_safe.get(); // for convinience calling the next functions
    ShopSetSilentConsole(shop, true);
    ofs << "#pragma once" << std::endl
        << "#include <tuple>" << std::endl
        << "// NOTE: auto generated file" << std::endl
        << "namespace shop::enums {" << std::endl;
    shop_cxx_api_generator::print_object_types(shop, ofs);
    ofs << std::endl;
    shop_cxx_api_generator::print_attribute_types(shop, ofs, pofs, false);
    ofs << std::endl;
    shop_cxx_api_generator::print_relation_types(shop, ofs);
    ofs << std::endl;
    shop_cxx_api_generator::print_command_types(shop, ofs);
    ofs << std::endl;
    shop_cxx_api_generator::print_other(shop, ofs);
    ofs << "\n}" << std::endl;
  }

  TEST_SUITE_END();
}