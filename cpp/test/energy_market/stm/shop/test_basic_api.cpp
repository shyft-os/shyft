// #define _CRT_SECURE_NO_WARNINGS
#include <algorithm>
#include <cstdlib>
#include <map>
#include <string>
#include <vector>

#include <test/energy_market/stm/shop/fixture.h>

namespace shyft::energy_market::stm::shop {
  using namespace std::string_literals;
  TEST_SUITE_BEGIN("stm/shop");

  TEST_CASE("stm/shop/init_and_free") {

    SUBCASE("Init gives valid ShopSystem and free succeeds") {
      ShopSystem* ptr = shop_init(SHYFT_SHOP_LICENSEDIR); // This constructs an instance of ShopSystem
      CHECK(ptr);
      CHECK(ShopFree(ptr)); // This destructs the instance of ShopSystem
    }
    SUBCASE("Multiple inits gives multiple separate ShopSystem") {
      ShopSystem* ptr1 = shop_init(SHYFT_SHOP_LICENSEDIR);
      CHECK(ptr1);
      ShopSystem* ptr2 = shop_init(SHYFT_SHOP_LICENSEDIR);
      CHECK(ptr2);
      ShopSystem* ptr3 = shop_init(SHYFT_SHOP_LICENSEDIR);
      CHECK(ptr3);
      CHECK_NE(ptr3, ptr2);
      CHECK_NE(ptr2, ptr1);
      CHECK(ShopFree(ptr1));
      CHECK(ShopFree(ptr2));
      CHECK(ShopFree(ptr3));
    }

    auto test_override_works = [&](auto env_var) {
      setenv(env_var, SHYFT_SHOP_LICENSEDIR, 1);
      auto* shop_api = shop_init("/tmp/hint/not/considered");
      ShopFree(shop_api);
      CHECK(shop_api);
      setenv(env_var, "/run/proc/bad_attempt", 1); // security wise, we want a throw here
      CHECK_THROWS_AS((void) shop_init("/tmp/hint/not/considered"), std::runtime_error);
      unsetenv(env_var);
    };

    SUBCASE("Works with SHYFT_SHOP_BUNDLE_DIR") {
      test_override_works("SHYFT_SHOP_BUNDLE_DIR");
    }

    SUBCASE("Works with ICC_COMMAND_PATH") {
      test_override_works("ICC_COMMAND_PATH");
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/version_info") {
    // Testing ShopGetVersionInfo, new API function with improved string allocation logic.
    // First: nullptr buffer should give us required length.
    char* s1 = nullptr;
    int l1 = 0;
    CHECK(ShopGetVersionInfo(shopSystem_.get(), s1, l1));
    CHECK((l1 > 0));
    CHECK_EQ(s1, nullptr);

    // Sufficiently sized buffer should give us the version string.
    int l2 = l1;
    auto s2 = std::string(l2, '0');
    CHECK(ShopGetVersionInfo(shopSystem_.get(), s2.data(), l2));
    CHECK_EQ(l1, l2);


    // Insufficiently sized buffer should fail and give the required length.
    int l3 = l2 - 1;
    auto s3 = std::string(l3, '0');
    CHECK(!ShopGetVersionInfo(shopSystem_.get(), s3.data(), l3));
    CHECK_EQ(l3, l2);
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/numobjects_and_atrs") {
    auto const ExpectedNumberOfObjectTypes = 40;
    auto const ExpectedNumberOfAttributes = 1398;
    auto nObjectTypes = ShopGetObjectTypeCount(shopSystem_.get());
    CHECK_EQ(nObjectTypes, ExpectedNumberOfObjectTypes);
    auto nAttributes = ShopGetAttributeCount(shopSystem_.get());
    CHECK_EQ(nAttributes, ExpectedNumberOfAttributes);
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/object_type_names") {
    int nObjectTypes = ShopGetObjectTypeCount(shopSystem_.get());
    for (int i = 0; i < nObjectTypes; ++i) {
      char const * objectTypeName = ShopGetObjectTypeName(shopSystem_.get(), i);
      CHECK(objectTypeName);
    }
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/object_type_index_and_name") {
    // Testing ShopGetObjectName
    int const reservoir_type_index = 0;
    char const * objectTypeName = ShopGetObjectTypeName(shopSystem_.get(), reservoir_type_index);
    CHECK_EQ(strcmp(objectTypeName, shop_type::reservoir), 0);
    int objectTypeIndex = ShopGetObjectTypeIndex(shopSystem_.get(), objectTypeName);
    CHECK_EQ(reservoir_type_index, objectTypeIndex);
    int const plant_type_index = 1;
    objectTypeName = ShopGetObjectTypeName(shopSystem_.get(), plant_type_index);
    CHECK_EQ(strcmp(objectTypeName, shop_type::plant), 0);
    objectTypeIndex = ShopGetObjectTypeIndex(shopSystem_.get(), objectTypeName);
    CHECK_EQ(plant_type_index, objectTypeIndex);
    int const generator_type_index = 2;
    objectTypeName = ShopGetObjectTypeName(shopSystem_.get(), generator_type_index);
    CHECK_EQ(strcmp(objectTypeName, shop_type::generator), 0);
    objectTypeIndex = ShopGetObjectTypeIndex(shopSystem_.get(), objectTypeName);
    CHECK_EQ(generator_type_index, objectTypeIndex);
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/get_attribute_info") {
    int hrl_attribute_index = 8; // "hrl"
    char reservoir_type_name[] = "reservoir";
    char aname[64];
    char afunc[64];
    char atype[64];
    char xunit[64];
    char yunit[64];
    bool aparam, ain, aout;
    ShopGetAttributeInfo(
      shopSystem_.get(), hrl_attribute_index, reservoir_type_name, aparam, aname, afunc, atype, xunit, yunit, ain, aout);
    CHECK_EQ(aparam, false);
    CHECK_EQ(ain, true);
    CHECK_EQ(aout, false);
    CHECK_EQ(strcmp(aname, "hrl"), 0);
    CHECK_EQ(strcmp(atype, "double"), 0);
    CHECK_EQ(strcmp(xunit, "METER"), 0);
    CHECK_EQ(strcmp(yunit, "METER"), 0);
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/default_objects") {
    // There are initially one object of type "scenario" with name "S1",
    // and one object of type "objective" with name "average_objective"
    auto nObjects = ShopGetObjectCount(shopSystem_.get());
    int index = 0;
    CHECK_EQ(nObjects, 5); // 5 as of 2022.02.11 (v14.3.0.2); 3 in older versions
    auto typeName = ShopGetObjectType(shopSystem_.get(), index);
    auto const typeNameScenario = "scenario"s;
    CHECK_EQ(typeName, typeNameScenario);
    auto objectName = ShopGetObjectName(shopSystem_.get(), index);
    auto const objectNameScenario = "S1"s;
    CHECK_EQ(objectName, objectNameScenario);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    auto const typeNameObjective = "objective"s; // ShopGetObjectTypeName(system, (int)shop_objects::objective);
    CHECK_EQ(typeName, typeNameObjective);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    auto const objectNameObjective = "average_objective"s;
    CHECK_EQ(objectName, objectNameObjective);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    auto const typeNameGlobalSettings = "global_settings"s;
    CHECK_EQ(typeName, typeNameGlobalSettings);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    auto const objectNameGlobalSettings = "global_settings"s;
    CHECK_EQ(objectName, objectNameGlobalSettings);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    auto const typeNameLpModel = "lp_model"s;
    CHECK_EQ(typeName, typeNameLpModel);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    auto const objectNameLpModel = "lp_model"s;
    CHECK_EQ(objectName, objectNameLpModel);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    auto const typeNameSystem = "system"s; // ShopGetObjectTypeName(system, (int)shop_objects::objective);
    CHECK_EQ(typeName, typeNameSystem);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    auto const objectNameSystem = "system"s;
    CHECK_EQ(objectName, objectNameSystem);

    // Then there are API methods for creating these default objects
    // CHECK(ShopAddFirstScenario(shopSystem_.get())); // NOTE: Removed in Shop v14
    // CHECK(ShopAddAverageObjective(shopSystem_.get()));// removed in 15.3.30

    // But calling them just results in duplicates, so should not be used?
    nObjects = ShopGetObjectCount(shopSystem_.get());
    CHECK_EQ(nObjects, 5); // 5 as of 15.3.3.0, then 6 as of 2022.02.11 (v14.3.0.2); 5 in older versions

    // Original objects at same indices
    index = 0;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    CHECK_EQ(typeName, typeNameScenario);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    CHECK_EQ(objectName, objectNameScenario);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    CHECK_EQ(typeName, typeNameObjective);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    CHECK_EQ(objectName, objectNameObjective);
    ++index;
    // NOTE: Shop v14
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    CHECK_EQ(typeName, typeNameGlobalSettings);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    CHECK_EQ(objectName, objectNameGlobalSettings);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    CHECK_EQ(typeName, typeNameLpModel);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    CHECK_EQ(objectName, objectNameLpModel);
    ++index;
    typeName = ShopGetObjectType(shopSystem_.get(), index);
    CHECK_EQ(typeName, typeNameSystem);
    objectName = ShopGetObjectName(shopSystem_.get(), index);
    CHECK_EQ(objectName, objectNameSystem);
    ++index;
    // 15.3.3.0 coredumps, so no extra objects here.
    // Added duplicate objects at following indices
    // typeName = ShopGetObjectType(shopSystem_.get(), index);
    // CHECK_EQ(typeName, typeNameObjective);
    // objectName = ShopGetObjectName(shopSystem_.get(), index);
    // CHECK_EQ(objectName, objectNameObjective);
  }

  TEST_CASE_FIXTURE(test_fixture, "stm/shop/object_type_attribute_indices") {
    // Testing the changed logic in version 03.10.2018
    int const reservoir_type_index = 0;
    // If attributeIndexList != NULL and nAttributes < 0:
    // Write all attribute indices into attributeIndexList and the count into nAttributes.
    { // By object type name
      int nAttributes = -1;
      int attributeIndexList[1024]{};
      CHECK(
        ShopGetObjectTypeAttributeIndices(shopSystem_.get(), shop_type::reservoir, nAttributes, attributeIndexList));
      CHECK((nAttributes > 0));
      CHECK((nAttributes <= 1024));
    }
    { // By object type index
      int nAttributes = -1;
      int attributeIndexList[1024]{};
      CHECK(
        ShopGetObjectTypeAttributeIndices(shopSystem_.get(), reservoir_type_index, nAttributes, attributeIndexList));
      CHECK((nAttributes > 0));
      CHECK((nAttributes <= 1024));
    }
    // If attributeIndexList != NULL and nAttributes >= 0:
    // Write minimum of nAttributes and total number of attribute indices into attributeIndexList.
    // Write the count into nAttributes.
    { // By object type name
      int nAttributes = 10;
      int attributeIndexList[1024]{};
      CHECK(
        ShopGetObjectTypeAttributeIndices(shopSystem_.get(), shop_type::reservoir, nAttributes, attributeIndexList));
      CHECK_EQ(nAttributes, 10);
    }
    { // By object type index
      int nAttributes = 10;
      int attributeIndexList[1024]{};
      CHECK(
        ShopGetObjectTypeAttributeIndices(shopSystem_.get(), reservoir_type_index, nAttributes, attributeIndexList));
      CHECK_EQ(nAttributes, 10);
    }
    // If attributeIndexList==NULL:
    // Write no attribute indices into, but only the count of into nAttributes.
    { // By object type name
      int nAttributes;
      CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_.get(), shop_type::reservoir, nAttributes, nullptr));
      CHECK_EQ(nAttributes, 101); // NB: Hard-coded for current version! 101 as of 5.7.0.0 91 as of 5.2.0.0 // as of
                                  // 2022.12.13 (v14.5.0.3); 72 as of 2022.02.11 (v14.3.0.2); 62 in older versions
    }
    { // By object type index
      int nAttributes;
      CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_.get(), reservoir_type_index, nAttributes, nullptr));
      CHECK_EQ(nAttributes, 101); // NB: Hard-coded for current version! 101 as of 5.7.0.0 91 as of 5.2.0.0. 75 as of
                                  // 2022.12.13 (v14.5.0.3); 72 as of 2022.02.11 (v14.3.0.2); 62 in older versions
    }

    // Additional test with invalid object type, return value should now be false.
    {
      int nAttributes = -1;
      int attributeIndexList[1024]{};
      auto not_valid_type{"fdkafhjdsalfklds"};
      CHECK(!ShopGetObjectTypeAttributeIndices(shopSystem_.get(), not_valid_type, nAttributes, attributeIndexList));
    }
  }

  TEST_CASE_FIXTURE(
    test_fixture,
    "stm/shop/methods_for_getting_attribute_types"
      * doctest::description("Testing and comparing different methods for retrieving attribute types")
      * doctest::should_fail(true) * doctest::skip(true)) {
    // Testing if ShopGetObjectTypeAttributeIndices for each object type gives us all the same attributes
    // as looping on all attribute indices from 0 to ShopGetAttributeCount.
    // NO: Conclusion is that ShopGetObjectTypeAttributeIndices gives a precise list of attributes that
    //     are current and relevant for clients! It excludes internal attributes such as id, water_course,
    //     and number attributes describing other array attributes (num_inputs for junction, num_penstock
    //     for plant, etc). This matches the official documentation (SHOP API specification), although that
    //     describes an WCF service interface and not this C/C++ library interface.
    char objectTypeName[64];
    char attributeName[64];
    char attributeDataFuncName[1024];
    char attributeDatatype[64];
    char xUnit[64];
    char yUnit[64];
    bool isObjectParam, isInput, isOutput;
    std::vector<std::string> objectTypes;
    std::map<int, std::string> attributes;
    auto checkAttributes = [&attributes, &objectTypes](ShopSystem* system) {
      INFO("Object type " << objectTypes.back());
      int nAttributeIndices;
      int attributeIndices[1024];
      ShopGetObjectTypeAttributeIndices(
        system, const_cast<char*>(objectTypes.back().c_str()), nAttributeIndices, attributeIndices);
      size_t nAttributes = attributes.size();
      CHECK_MESSAGE(
        (nAttributes == static_cast<std::size_t>(nAttributeIndices)),
        "Different number of attributes returned by ShopGetObjectTypeAttributeIndices and ShopGetAttributeCount");
      std::string attributeNames;
      for (auto const & it : attributes) {
        attributeNames += it.second + " (" + std::to_string(it.first) + ");";
      }
      INFO("All attributes found for object type by looping until ShopGetAttributeCount: " << attributeNames);
      for (int i = 0; i < nAttributeIndices; ++i) {
        int attributeIndex = attributeIndices[i];
        std::string attributeName = ShopGetAttributeName(system, attributeIndex);
        auto it = std::ranges::find_if(attributes, [&attributeName](auto const & entry) {
          return entry.second == attributeName;
        });
        if (it != std::ranges::end(attributes)) {
          attributes.erase(it);
        } else {
          FAIL_CHECK(
            "Attribute " << attributeName << " (" << attributeIndex
                         << ") was returned by ShopGetObjectTypeAttributeIndices but not within ShopGetAttributeCount");
        }
      }
      size_t nAttributesRemaining = attributes.size();
      std::string missingAttributeNames;
      for (auto const & it : attributes) {
        missingAttributeNames += it.second + " (" + std::to_string(it.first) + ");";
      }
      CHECK_MESSAGE(
        (nAttributesRemaining == 0),
        "" << nAttributesRemaining
           << " of the attributes was not returned by ShopGetObjectTypeAttributeIndices: " << missingAttributeNames);
    };
    for (int i = 0, n = ShopGetAttributeCount(shopSystem_.get()); i < n; ++i) {
      ShopGetAttributeInfo(
        shopSystem_.get(),
        i,
        objectTypeName,
        isObjectParam,
        attributeName,
        attributeDataFuncName,
        attributeDatatype,
        xUnit,
        yUnit,
        isInput,
        isOutput);
      if (!objectTypes.empty() && objectTypes.back() != objectTypeName) {
        checkAttributes(shopSystem_.get());
        attributes.clear();
      }
      if (!objectTypes.empty() || objectTypes.back() != objectTypeName) {
        if (std::find(begin(objectTypes), end(objectTypes), objectTypeName) != end(objectTypes)) {
          FAIL_CHECK(
            "Object type " << objectTypeName
                           << " found in multiple separate sections of attributes within ShopGetAttributeCount");
        }
        objectTypes.emplace_back(objectTypeName);
      }
      auto it = std::ranges::find_if(attributes, [&attributeName](auto const & entry) {
        return entry.second == attributeName;
      });
      if (it != std::ranges::end(attributes)) {
        FAIL_CHECK("Attribute " << attributeName << " with index " << i << " already exists with index " << it->first);
      }
      attributes[i] = attributeName;
    }
    if (!attributes.empty()) {
      checkAttributes(shopSystem_.get());
    }
  }

  TEST_SUITE_END();
}
