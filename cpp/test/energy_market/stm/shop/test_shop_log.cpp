#include <cstdint>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/shop/model_simple.h>

namespace shyft::energy_market::stm::shop {

  TEST_SUITE_BEGIN("stm/shop");

  bool const always_inlet_tunnels = false;
  bool const use_defaults = false;
  auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
  auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
  auto const dt = core::deltahours(1);
  std::size_t const n_t = (t_end - t_begin) / dt;
  time_axis::generic_dt const ta(t_begin, dt, n_t);

  TEST_CASE("stm/shop/log_no_term") {
    auto stm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults);
    // Create shop system
    shop_system shop(ta);
    shop.emit(*stm);
    auto const commands = optimization_commands(1, false);
    for (auto const &command : commands)
      shop.commander.execute(command);
    auto messages = shop.get_log_buffer();
    REQUIRE_GT(messages.size(), 0);
    std::ranges::for_each(messages, [](auto &m) {
      CHECK(std::ranges::none_of(m.message, [](auto const c) {
        return c == '\0';
      }));
    });
  }

  TEST_CASE("stm/shop/log" * doctest::description("usage of shop_system::get_log_buffer")) {
    // Set up system to optimize and expected solution
    auto stm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults);
    auto rstm = build_simple_model(t_begin, t_end, dt, always_inlet_tunnels, use_defaults, 1, true);
    // Create shop system
    shop_system shop(ta);

    // 0. Check that log is empty:
    auto messages = shop.get_log_buffer();
    CHECK_EQ(messages.size(), 0);
    // Do optimization:
    auto const commands = optimization_commands(1, false);
    shop.emit(*stm);
    // 1. Check log after emit:
    auto new_messages = shop.get_log_buffer();
    messages.insert(messages.end(), new_messages.begin(), new_messages.end());
    CHECK_EQ(messages.size(), 0);
    for (auto const &command : commands)
      shop.commander.execute(command);
    // 2. Check log after commands:
    new_messages = shop.get_log_buffer();
    messages.insert(messages.end(), new_messages.begin(), new_messages.end());
    auto N = messages.size();
    CHECK_GE(N, 0);
    CHECK_EQ(shop.get_log_buffer().size(), 0);

    shop.collect(*stm);
    // 3. Check log after collecting results
    CHECK_EQ(shop.get_log_buffer().size(), 0);

    check_results(stm, rstm, t_begin, t_end, dt);
  }

  TEST_SUITE_END();

}
