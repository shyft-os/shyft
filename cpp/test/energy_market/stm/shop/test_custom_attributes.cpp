#include <optional>
#include <string>
#include <vector>

#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_custom_attributes.h>

#include <doctest/doctest.h>

namespace shyft::energy_market::stm::shop {

  TEST_SUITE_BEGIN("stm/shop");

  TEST_CASE("stm/shop/custom_attributes") {
    SUBCASE("gate_ramp_cost") {
      stm_system s{};
      SUBCASE("get") {
        CHECK(custom_attributes::gate_ramp_cost(s) == std::nullopt);
        SUBCASE("custom") {
          s.custom[custom_attributes::gate_ramp_cost_url] = double{};
          CHECK(custom_attributes::gate_ramp_cost(s) != std::nullopt);
        }
        SUBCASE("invalid") {
          SUBCASE("0") {
            s.custom[custom_attributes::gate_ramp_cost_url] = time_series::dd::apoint_ts{};
            CHECK(custom_attributes::gate_ramp_cost(s) == std::nullopt);
          }
          SUBCASE("1") {
            s.tsm[custom_attributes::gate_ramp_cost_url] = time_series::dd::apoint_ts{};
            CHECK(custom_attributes::gate_ramp_cost(s) == std::nullopt);
          }
        }
      }
    }
    SUBCASE("river_discharge_result_downstream") {
      waterway w{};
      CHECK(custom_attributes::river_discharge_result_downstream(w) == std::nullopt);
      SUBCASE("get") {
        SUBCASE("ts") {
          w.tsm[custom_attributes::river_discharge_result_downstream_url] = time_series::dd::apoint_ts{};
          CHECK(custom_attributes::river_discharge_result_downstream(w) != std::nullopt);
        }
        SUBCASE("custom") {
          w.custom[custom_attributes::river_discharge_result_downstream_url] = time_series::dd::apoint_ts{};
          CHECK(custom_attributes::river_discharge_result_downstream(w) != std::nullopt);
        }
        SUBCASE("invalid") {
          w.custom[custom_attributes::river_discharge_result_downstream_url] = double{};
          CHECK(custom_attributes::river_discharge_result_downstream(w) == std::nullopt);
        }
      }
      SUBCASE("with") {
        CHECK(std::ranges::empty(custom_attributes::output_attributes<unit>()));
        {
          auto w = custom_attributes::output_attributes<waterway>();
          CHECK(std::ranges::contains(
            w, custom_attributes::attribute<custom_attributes::tag::river_discharge_result_downstream>()));
        }
        {
          auto s = std::make_shared<stm_system>(1, "ff", "{}");
          auto h = std::make_shared<stm_hps>(1, "gg", "{}", s);
          h->waterways.push_back(std::make_shared<waterway>(2, "gg", "{}", h));
          s->hps.push_back(h);
          std::vector<std::string> urls;
          custom_attributes::with_output_urls(
            [&](auto url) {
              urls.push_back(std::string(url));
            },
            "whatever",
            *s);
          REQUIRE(urls.size() == 2);
          for (auto url : urls)
            CHECK(std::ranges::contains_subrange(url, custom_attributes::river_discharge_result_downstream_url));
        }
      }
    }
  }

  TEST_SUITE_END();
}
