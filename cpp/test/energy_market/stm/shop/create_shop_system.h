#pragma once
#include <memory>

#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm::shop {
  using namespace std::string_literals;

  auto create_shop_system(auto hps, auto time_axis) {
    auto mdl = std::make_shared<stm_system>(0, "dummy_stm_system"s, ""s);
    auto mkt = std::make_shared<energy_market_area>(0, "dummy_stm_market"s, ""s, mdl);
    mdl->hps.push_back(hps);
    mdl->market.push_back(mkt);
    return std::make_shared<shop_system>(time_axis);
  }
}