#include <algorithm>
#include <cmath>
#include <filesystem>
#include <string>
#include <vector>

#include <boost/hof/unpack.hpp>

#include <shyft/energy_market/stm/shop/shop_api.h>
#include <shyft/energy_market/stm/shop/shop_data.h>

#include <test/energy_market/stm/shop/fixture.h>
#include <test/test_utils.h>

namespace shyft::energy_market::stm::shop {
  using ::shop::data::shop_time;
  using ::shop::proxy::unit::no_unit;

  auto axis(std::vector<time_t> t_axis) {
    std::vector<core::utctime> utc_axis(t_axis.size());
    std::ranges::transform(t_axis, utc_axis.data(), core::utctime_from_seconds64);
    return time_axis::generic_dt{utc_axis};
  }

  auto from_points(
    time_t t0,
    std::vector<std::pair<time_t, double>> points,
    core::utctime last_step = core::utctime(1l),
    time_series::ts_point_fx fx = time_series::POINT_AVERAGE_VALUE) {
    if (points.size() < 2)
      throw std::runtime_error("Converter requires at least two points on ts");
    auto t_end = core::utctime_from_seconds64(t0 + std::get<0>(points.back())) + last_step;
    std::vector<core::utctime> t(points.size());
    std::vector<double> v(points.size());

    {
      auto target = std::views::zip(t, v);
      std::ranges::transform(
        points, std::ranges::begin(target), boost::hof::unpack([&](auto const & dt, auto const & value) {
          return std::make_pair(core::utctime_from_seconds64(t0 + dt), value);
        }));
    }
    return time_series::dd::apoint_ts{
      time_axis::point_dt{t, t_end},
      v, fx
    };
  }

  TEST_SUITE_BEGIN("stm/shop");

  TEST_CASE("stm/shop/time") {
    shop_time t1(0L);
    shop_time t2("19700101000000000", true);
    CHECK((time_t(0) == time_t(t1)));
    CHECK((time_t(0) == time_t(t2)));
    CHECK((t1 == t2));
    shop_time t3(3600 + 60 + 1);
    CHECK((t1 != t3));
    shop_time t4("19700101010101000", true);
    CHECK((t3 == t4));
    char* t3_s = static_cast<char*>(t3);
    char* t3_ss = t3; // implicit cast works
    CHECK((t3_s == t3_ss));
    CHECK_EQ(t3_ss, std::string("19700101010101000"));
    shop_time t5(-3600L);
    shop_time t6("19691231230000000", true);
    CHECK_EQ(t5, t6);
    CHECK_THROWS_AS(shop_time("", true), std::runtime_error);
    CHECK_THROWS_AS(shop_time("2001", true), std::runtime_error);
    CHECK_THROWS_AS(shop_time("2001020304050", true), std::runtime_error);
    CHECK_THROWS_AS(shop_time("20019903040506", true), std::runtime_error);
    CHECK_THROWS_AS(shop_time("20010203040506", true), std::runtime_error);
    shop_time t7("20010203040506000", true);
    CHECK_EQ(t7.rep, std::string("20010203040506000"));
    core::calendar utc;
    shop_time t8(core::to_seconds64(utc.time(2001, 2, 3, 4, 5, 6)));
    CHECK_EQ(t8.rep, std::string("20010203040506000"));
  }

  TEST_CASE("stm/shop/time_axis_fixed_hour") {
    api s{SHYFT_SHOP_LICENSEDIR};
    shop_time start_time("20180101000000000", true);
    shop_time end_time("20180101180000000", true);
    time_t t_step = 3600;
    s.set_time_axis(time_t(start_time), time_t(end_time), t_step);
    CHECK(s.time_axis_defined);
    CHECK((s.time_axis.size() == 19));
    time_t t_begin = (time_t) start_time;
    time_t t_end = (time_t) end_time;
    size_t i = 0;
    for (time_t t = t_begin; t < t_end; t += t_step) {
      CHECK((t == s.time_axis[i++]));
    }
    CHECK((i == 18));
  }

  TEST_CASE("stm/shop/time_axis_fixed_quarter") {
    api s{SHYFT_SHOP_LICENSEDIR};
    shop_time start_time("20180101000000000", true);
    shop_time end_time("20180101110000000", true);
    time_t t_step = 900;
    s.set_time_axis(time_t(start_time), time_t(end_time), t_step);
    CHECK(s.time_axis_defined);
    time_t t_begin = (time_t) start_time;
    time_t t_end = (time_t) end_time;
    size_t i = 0;
    for (time_t t = t_begin; t < t_end; t += t_step) {
      CHECK((t == s.time_axis[i++]));
    }
    CHECK((i == 44));
  }

  TEST_CASE("stm/shop/time_axis_dynamic") {
    api s{SHYFT_SHOP_LICENSEDIR};
    shop_time t_keypoints[]{
      {"20180101000000000", true},
      {"20180101010000000", true},
      {"20180101030000000", true},
      {"20180101070000000", true},
      {"20180101190000000", true},
    };
    time_t step_lengths[]{900, 1800, 3600, 10800};
    size_t n_step_changes = std::extent<decltype(step_lengths)>::value;
    std::vector<time_t> t_axis;
    std::vector<shop_time> t_axis_s;
    time_t t{0};
    for (size_t i = 0; i < n_step_changes; ++i) {
      time_t step{step_lengths[i]};
      t = time_t(t_keypoints[i]);
      time_t t_next{t_keypoints[i + 1]};
      for (; t < t_next; t += step) {
        t_axis.push_back(t);
        t_axis_s.emplace_back(t);
      }
    }
    t_axis.push_back(t);
    t_axis_s.emplace_back(t);
    s.set_time_axis(t_axis);
    CHECK(s.time_axis_defined);
    CHECK((t_axis == s.time_axis));
  }

  TEST_CASE("stm/shop/XY") {
    hydro_power::xy_point_curve_with_z a;
    a.z = 123.0;
    a.xy_curve.points = {
      {0.0, 0.0},
      {1.0, 1.0}
    };
    auto [x, y] = from_curve<no_unit, no_unit>(a);
    auto a_ = to_curve<no_unit, no_unit>(a.z, std::move(x), std::move(y));
    CHECK((a == a_));
  }

  TEST_CASE("stm/shop/TXY") {
    using time_series::dd::apoint_ts;

    time_t const t_start = 3600;
    time_t const t_step = 3600;
    size_t const n = 13;
    auto a = from_points(
      t_start,
      {
        { 0 * t_step, 60.0},
        { 2 * t_step, 64.0},
        { 4 * t_step, 68.0},
        { 6 * t_step, 62.0},
        { 8 * t_step, 60.0},
        {12 * t_step, 50.0}
    },
      core::utctime_from_seconds64(t_step));

    std::vector<time_t> t_points;
    for (size_t i = 0; i <= n; ++i) {
      t_points.push_back(t_start + i * t_step);
    }

    auto [start_time, t, y] = from_ts<no_unit>(a, t_points);
    std::time_t minute = 60L;
    auto a_ = to_ts<no_unit>(t_points, std::move(t), std::move(y), minute);
    CHECK(a.average(axis(t_points)).evaluate() == a_);
  }

  TEST_CASE("stm/shop/api_scaling") {
    using reservoir = ::shop::reservoir<api>;

    api s{SHYFT_SHOP_LICENSEDIR};
    set_shop_library_path_for_test(s);
    shop_time start_time("20180101000000000", true);
    shop_time end_time("20180101180000000", true);
    s.set_time_axis(time_t(start_time), time_t(end_time), 3600);
    auto r1 = s.create<reservoir>("r1");

    // lrl is in m: no scaling (factor=1)
    CHECK(decltype(r1.lrl)::y_unit::is_base);
    CHECK((decltype(r1.lrl)::y_unit::to_base(1.0) == 1.0));
    double vin = decltype(r1.lrl)::y_unit::to_base(87.6);
    r1.lrl = vin;
    double vout = r1.lrl;
    CHECK((vin == vout));

    // max_vol is in MM3: scaling from/to mega (factor=1000000)
    CHECK((decltype(r1.max_vol)::y_unit::is_base == false));
    CHECK((decltype(r1.max_vol)::y_unit::to_base(1.0) == 1000000.0));
    vin = decltype(r1.max_vol)::y_unit::to_base(12.3);
    r1.max_vol = vin;
    vout = r1.max_vol;
    CHECK((vin == vout));
  }

  TEST_CASE("stm/shop/api_mini_sys") {
    using time_series::dd::apoint_ts;
    using xy = hydro_power::xy_point_curve_with_z;
    using reservoir = ::shop::reservoir<api>;
    using market = ::shop::market<api>;
    using gate = ::shop::gate<api>;
    using power_station = ::shop::power_plant<api>;
    using aggregate = ::shop::unit<api>;

    int const mega = 1000000;
    // using power_station=shop_test::pow
    api s{SHYFT_SHOP_LICENSEDIR};
    set_shop_library_path_for_test(s);
    shop_time start_time("20180101000000000", true);
    shop_time end_time("20180101180000000", true);
    time_t delta_t = 3600;
    s.set_time_axis(time_t(start_time), time_t(end_time), delta_t);
    auto ta_shyft = axis(s.time_axis);
    auto t0 = static_cast<time_t>(start_time);
    auto m1 = s.create<market>("m1");
    m1.buy_price = from_points(
      t0,
      {
        { 0 * delta_t, 48.1 / mega},
        { 6 * delta_t, 40.1 / mega},
        {12 * delta_t, 35.1 / mega}
    },
      core::utctime_from_seconds64(6 * delta_t));

    m1.sale_price = from_points(
      t0,
      {
        { 0 * delta_t, 48.0 / mega},
        { 6 * delta_t, 40.0 / mega},
        {12 * delta_t, 35.0 / mega}
    },
      core::utctime_from_seconds64(6 * delta_t));
    m1.max_buy = from_points(
      t0,
      {
        {0 * delta_t,   10.0 * mega},
        {6 * delta_t, 9999.0 * mega}
    },
      core::utctime_from_seconds64(12 * delta_t));
    m1.max_sale = from_points(
      t0,
      {
        {0 * delta_t,   10.0 * mega},
        {6 * delta_t, 9999.0 * mega}
    },
      core::utctime_from_seconds64(12 * delta_t));
    m1.load = from_points(
      t0,
      {
        {0 * delta_t, 90.0 * mega},
        {1 * delta_t, 80.0 * mega},
        {4 * delta_t, 60.0 * mega},
        {5 * delta_t, 10.0 * mega},
        {6 * delta_t,  0.0 * mega}
    },
      core::utctime_from_seconds64(12 * delta_t));

    auto r1 = s.create<reservoir>("r1");
    auto f1 = s.create<gate>("flood1");
    auto b1 = s.create<gate>("bypass1");
    auto ps1 = s.create<power_station>("ps1");
    auto a1 = s.create<aggregate>("a1");
    s.connect_objects(ps1, ps1.connection_standard, a1.id);
    s.connect_objects(r1, r1.connection_standard, ps1.id);
    s.connect_objects(r1, r1.connection_spill, f1.id);
    s.connect_objects(r1, r1.connection_bypass, b1.id);
    r1.lrl = 80.0;
    r1.hrl = 100.0;
    r1.max_vol = 16.0 * mega;
    r1.vol_head = xy{
      {{{0.0 * mega, 80.0}, {2.0 * mega, 90.0}, {3.0 * mega, 95.0}, {5.0 * mega, 100.0}, {16.0 * mega, 105.0}}}, 0.0};
    r1.flow_descr = xy{{{{100.0, 0.0}, {101.5, 25.0}, {103.0, 80.0}, {104.0, 150.0}}}, 0.0};
    r1.start_head = 90.0;
    r1.energy_value_input = 36.5 / mega;
    auto r1_inflow = from_points(
      t0,
      {
        { 0 * delta_t, 60.0},
        { 2 * delta_t, 64.0},
        { 4 * delta_t, 68.0},
        { 6 * delta_t, 62.0},
        { 8 * delta_t, 60.0},
        {12 * delta_t, 50.0}
    },
      core::utctime_from_seconds64(6 * delta_t));
    r1.inflow = r1_inflow;
    apoint_ts r1_inflow_q = r1.inflow;
    CHECK_EQ(r1_inflow.average(ta_shyft).evaluate(), r1_inflow_q);

    ps1.outlet_line = 10.0;
    ps1.main_loss = std::vector<double>{0.0003};
    ps1.penstock_loss = std::vector<double>{0.00005};

    a1.penstock = 1;
    a1.p_min = 20.0 * mega;
    a1.p_max = 80.0 * mega;
    a1.p_nom = 80.0 * mega;
    xy a1_gen_eff{{{{20.0 * mega, 96.0}, {40.0 * mega, 98.0}, {60.0 * mega, 99.0}, {80.0 * mega, 98.0}}}, 0.0};
    a1.gen_eff_curve = a1_gen_eff;

    CHECK_EQ(a1_gen_eff, a1.gen_eff_curve);
    std::vector<xy> a1_t_eff_curves{
      xy{{{{20.0, 70.0}, {40.0, 85.0}, {60.0, 92.0}, {80.0, 94.0}, {100.0, 92.0}, {110.0, 90.0}}}, 70.0}
    };
    a1.turb_eff_curves = a1_t_eff_curves;
    CHECK_EQ(a1_t_eff_curves, a1.turb_eff_curves.get()); // verify we get back what we inserted
    f1.max_discharge = 150.0;
    b1.max_discharge = 1000.0;
    CHECK_UNARY(b1.max_discharge.exists());
    CHECK_UNARY(a1.gen_eff_curve.exists());

    s.execute_cmd("set method", "primal");
    s.execute_cmd("set code", "full");
    s.start_sim("3");
    s.execute_cmd("set code", "incremental");
    s.start_sim("3");

    apoint_ts a1_discharge = a1.discharge;
    apoint_ts r1_storage = r1.storage;
    apoint_ts r1_head = r1.head;
    CHECK(!a1_discharge.empty());
    CHECK(!r1_storage.empty());
    CHECK(!r1_head.empty());

    auto a1_discharge_expected = from_points(
      t0,
      {
        { 0 * delta_t, 109.9999},
        { 1 * delta_t, 106.2111},
        { 2 * delta_t, 108.0305},
        { 3 * delta_t, 109.8440},
        { 4 * delta_t,  90.0005},
        { 5 * delta_t,  34.5954},
        { 6 * delta_t,  64.7958},
        { 7 * delta_t,  64.8015},
        { 8 * delta_t,  64.8073},
        { 9 * delta_t,  80.0000},
        {10 * delta_t,  80.0000},
        {11 * delta_t,  80.0000},
        {12 * delta_t,   0.0000},
        {13 * delta_t,   0.0000},
        {14 * delta_t,   0.0000},
        {15 * delta_t,  64.6341},
        {16 * delta_t,  64.6634},
        {17 * delta_t,  64.6929},
    },
      core::utctime_from_seconds64(delta_t));
    CHECK(test::utils::approx_equal(a1_discharge, a1_discharge_expected));
    auto r1_head_expected = from_points(
      t0,
      {
        { 0 * delta_t, 90.0000},
        { 1 * delta_t, 89.1000},
        { 2 * delta_t, 88.2682},
        { 3 * delta_t, 87.4757},
        { 4 * delta_t, 86.6505},
        { 5 * delta_t, 86.2544},
        { 6 * delta_t, 86.8557},
        { 7 * delta_t, 86.8054},
        { 8 * delta_t, 86.7550},
        { 9 * delta_t, 86.6684},
        {10 * delta_t, 86.3084},
        {11 * delta_t, 85.9484},
        {12 * delta_t, 85.5884},
        {13 * delta_t, 86.4884},
        {14 * delta_t, 87.3884},
        {15 * delta_t, 88.2884},
        {16 * delta_t, 88.0250},
        {17 * delta_t, 87.7611},
        {18 * delta_t, 87.4966}
    },
      core::utctime(1l),
      time_series::POINT_INSTANT_VALUE);
    CHECK(test::utils::approx_equal(r1_head, r1_head_expected));
    auto r1_storage_expected = from_points(
      t0,
      {
        { 0 * delta_t, 2000000.0000},
        { 1 * delta_t, 1820000.0000},
        { 2 * delta_t, 1653639.9036},
        { 3 * delta_t, 1495130.0883},
        { 4 * delta_t, 1330091.6024},
        { 5 * delta_t, 1250889.8346},
        { 6 * delta_t, 1371146.3614},
        { 7 * delta_t, 1361081.5845},
        { 8 * delta_t, 1350996.0335},
        { 9 * delta_t, 1333689.6429},
        {10 * delta_t, 1261689.6429},
        {11 * delta_t, 1189689.6429},
        {12 * delta_t, 1117689.6429},
        {13 * delta_t, 1297689.6429},
        {14 * delta_t, 1477689.6429},
        {15 * delta_t, 1657689.6429},
        {16 * delta_t, 1605006.8937},
        {17 * delta_t, 1552218.6965},
        {18 * delta_t, 1499324.2408}
    },
      core::utctime(1l),
      time_series::POINT_INSTANT_VALUE);
    CHECK(test::utils::approx_equal(r1_storage, r1_storage_expected));
  }

  TEST_SUITE_END();
}