#include <thread>

#include <boost/asio/io_context.hpp>
#include <boost/beast/core.hpp>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/shop/model_simple.h>
#include <test/energy_market/stm/shop/test_server_dstm.h>
#include <test/test_utils.h>
#include <test/web_api/request_sender.h>

namespace shyft::energy_market::stm {
  using namespace std::chrono_literals;

  namespace {
    constexpr bool not_done(model_state state) {
      return state != model_state::finished && state != model_state::failed;
    }

    auto poll_done_for(srv::dstm::client &client, std::string const &model_id, auto max_duration) {
      auto timeout = core::utctime_now() + max_duration;
      model_state state{};
      std::this_thread::sleep_for(100ms);
      while ((state = client.get_state({model_id}).state) == model_state::idle && core::utctime_now() < timeout)
        std::this_thread::sleep_for(10ms);
      while ((state = client.get_state({model_id}).state) == model_state::running && core::utctime_now() < timeout)
        std::this_thread::sleep_for(10ms);
      return state;
    }

    std::optional<model_state> synch_optimize(
      srv::dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands,
      auto max_duration) {
      if (!client.optimize({model_id, time_axis, commands}).status)
        return std::nullopt;
      return poll_done_for(client, model_id, max_duration);
    }

    auto synch_optimize(
      srv::dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands) {
      return synch_optimize(client, model_id, time_axis, commands, stm::shop::calc_suggested_timelimit(commands));
    }

    std::optional<model_state> synch_tune(
      srv::dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands,
      auto max_duration) {
      if (!client.tune({model_id, time_axis, commands}).status)
        return std::nullopt;
      return poll_done_for(client, model_id, max_duration);
    }

    auto synch_tune(
      srv::dstm::client &client,
      std::string const &model_id,
      generic_dt const &time_axis,
      std::vector<shop::shop_command> &commands) {
      return synch_tune(client, model_id, time_axis, commands, stm::shop::calc_suggested_timelimit(commands));
    }

    /**
     * @brief repeat stop tune cmd until accepted
     * @details
     * Since we talk to dstm, and dstm to compute node,
     * there is possible time needed to compute node to get out of
     * the un-interruptible 'running' state, before it can accept
     * an stop request (compute node does not accept any cmd in 'running' state).
     * As state is observed from the dstm-server side, and not checked, or waited for
     * prior to the stop command sent, we will get exceptions.
     * Which is particular the case during testing , with many threads, or valgrind
     * or otherwise instrumented code.
     * @param c
     * @param mdl_id
     * @param max_duration
     * @return model_state
     */
    auto try_stop_tune(srv::dstm::client &c, std::string const &mdl_id, utctime max_duration) {
      auto t_exit = core::utctime_now() + max_duration;
      model_state ms = c.get_state({mdl_id}).state;
      while (ms != model_state::finished && core::utctime_now() < t_exit) {
        try {
          (void) c.stop_tune({mdl_id});
          ms = c.get_state({mdl_id}).state;
        } catch (std::runtime_error const &ex) {
          std::this_thread::sleep_for(core::from_seconds(0.3));
        }
      }
      return ms;
    }
  }

  TEST_SUITE_BEGIN("stm/shop");

  static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_files==true.

  using namespace test;
  using core::utctime_now;

  TEST_CASE(
    "stm/shop/dstm_simple_model_opt/1"
    * doctest::description(
      "building and optmimizing simple model without inlet segment: "
      "aggregate-penstock-maintunnel-reservoir")) {
    dlib::set_all_logging_levels(dlib::LNONE);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = core::deltahours(1);
    auto const n_steps = (std::size_t) ((t_end - t_begin) / t_step);
    time_axis::generic_dt const ta{t_begin, t_step, n_steps};
    int const mega = 1000000;

    auto stm = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = shop::build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with results:
    auto s = std::make_unique<srv::dstm::server>();
    s->set_listening_ip("127.0.0.1");
    auto port_no = s->start_server();

    auto cs = std::make_unique<srv::compute::server>();
    cs->set_listening_ip("127.0.0.1");
    auto cs_port_no = cs->start_server();
    REQUIRE_GT(cs_port_no, 0);

    std::this_thread::sleep_for(10ms);

    auto cs_addr = fmt::format("127.0.0.1:{}", cs_port_no);

    REQUIRE_GT(port_no, 0);
    try {
      auto host_port = fmt::format("127.0.0.1:{}", port_no);
      srv::dstm::client c{{.connection{host_port}}};
      c.add_compute_server({cs_addr});
      {
        auto [cs_status] = c.compute_server_status({});
        REQUIRE_EQ(cs_status.size(), 1u);
        REQUIRE_EQ(cs_status[0].address, cs_addr);
      }

      // get version info
      auto [result] = c.get_version_info();
      CHECK_EQ(result, get_version_info(*s).version);

      // get model ids
      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      char const mdl_id[] = {"test_stm_model"};
      CHECK_EQ(c.add_model({mdl_id, stm}).status, true);
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);

      // Arrange for subscription checks
      // ADD subscription to ALL results for this model, because we would like to see the notification
      auto all_result_ts_urls = url_planning_outputs(mdl_id, *stm);
      auto subs = s->dispatch.state->dtss->sm->add_subscriptions(all_result_ts_urls);
      auto sum_subs = 0;
      for (auto const &sub : subs)
        sum_subs += sub->v;
      // RUN the optimization, that should fire notifications when done(so subs above should increment)
      auto cmd = shop::optimization_commands(run_id);
      // also add subs for the state of the model, since we want to ensure its working as well.
      auto model_state_subs = s->dispatch.state->sm->add_subscription(sub_model_state_topic(mdl_id));
      auto model_state_sum = model_state_subs[0]->v.load();
      SUBCASE("optimize") {
        REQUIRE_EQ(synch_optimize(c, mdl_id, ta, cmd), model_state::finished);
      }
      SUBCASE("tune") {
        REQUIRE(c.start_tune({mdl_id}).status);
        REQUIRE_EQ(synch_tune(c, mdl_id, ta, cmd), model_state::tuning);
        REQUIRE_EQ(synch_tune(c, mdl_id, ta, cmd), model_state::tuning);
        REQUIRE_EQ(try_stop_tune(c, mdl_id, core::from_seconds(20)), model_state::finished);
      }
      auto [stm2] = c.get_model({mdl_id});
      shop::check_results(stm2, rstm, t_begin, t_end, t_step);

      auto attr = stm2->market.front()->buy;
      REQUIRE_EQ(shop::exists(attr), true);
      auto ts = attr;
      auto values = ts.values();
      REQUIRE_EQ(values[0], 10.0 * mega);

      attr = stm2->market.front()->sale;
      REQUIRE_EQ(shop::exists(attr), true);
      ts = attr;
      values = ts.values();
      REQUIRE_EQ(values[5], -10.0 * mega);

      auto sum_subs_after = 0;
      for (auto const &sub : subs)
        sum_subs_after += sub->v;
      CHECK_GT(sum_subs_after, sum_subs);
      auto n_state_notifications = model_state_subs[0]->v.load() - model_state_sum;
      CHECK_GT(n_state_notifications, 1);
      c.close();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
    }
    cs.reset();
    clear_manager(s.get());
    s.reset();
  }

  TEST_CASE(
    "stm/shop/dstm_simple_model_opt/2"
    * doctest::description(
      "optmimizing simple model without inlet segment with dstm: "
      "aggregate-penstock-maintunnel-reservoir")) {
    dlib::set_all_logging_levels(dlib::LNONE);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = core::deltahours(1);
    auto const n_steps = (std::size_t) ((t_end - t_begin) / t_step);
    time_axis::generic_dt const ta{t_begin, t_step, n_steps};
    int const mega = 1000000;

    auto stm = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto rstm = shop::build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with results:
    auto s = std::make_unique<srv::dstm::server>();
    s->set_listening_ip("127.0.0.1");
    auto port_no = s->start_server();
    auto const n_compute_servers = 1;
    std::vector<srv::compute::server> compute_servers(n_compute_servers);
    std::vector<std::string> cn_addr;
    for (auto &sl : compute_servers) {
      sl.set_listening_ip("127.0.0.1");
      auto ps = sl.start_server();
      REQUIRE_GT(ps, 0);
      cn_addr.push_back(fmt::format("127.0.0.1:{}", ps));
    }
    REQUIRE_GT(port_no, 0);
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{{.connection{host_port}}};

      auto [result] = c.get_version_info();
      CHECK_EQ(result, get_version_info(*s).version);

      for (auto const &cn : cn_addr)
        c.add_compute_server({cn}); // add compute servers

      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      std::string mdl_id = "test_stm_model";
      CHECK_EQ(c.add_model({mdl_id, stm}).status, true);
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);

      // Arrange for subscription checks
      // ADD subscription to ALL results for this model, because we would like to see the notification
      auto all_result_ts_urls = url_planning_outputs(mdl_id, *stm);
      auto subs = s->dispatch.state->dtss->sm->add_subscriptions(all_result_ts_urls);
      auto sum_subs = 0;
      for (auto const &sub : subs)
        sum_subs += sub->v;

      auto cmd = shop::optimization_commands(run_id);
      SUBCASE("optimize") {
        REQUIRE(synch_optimize(c, mdl_id, ta, cmd) == model_state::finished);
      }
      SUBCASE("tune") {
        REQUIRE(c.start_tune({mdl_id}).status);
        REQUIRE_EQ(synch_tune(c, mdl_id, ta, cmd), model_state::tuning);
        REQUIRE_EQ(try_stop_tune(c, mdl_id, core::from_seconds(10)), model_state::finished);
      }

      REQUIRE_EQ(c.get_state({mdl_id}).state, model_state::finished);
      auto [stm2] = c.get_model({mdl_id});
      shop::check_results(stm2, rstm, t_begin, t_end, t_step);
      CHECK_EQ(stm2->run_params.run_time_axis, ta); // ensure time-axis is updated
      CHECK_NE(stm2->summary, nullptr);
      CHECK_EQ(std::isfinite(stm2->summary->total), true);
      auto attr = stm2->market.front()->buy;
      REQUIRE_EQ(shop::exists(attr), true);
      auto ts = attr;
      auto values = ts.values();
      REQUIRE_EQ(values[0], 10.0 * mega);

      attr = stm2->market.front()->sale;
      REQUIRE_EQ(shop::exists(attr), true);
      ts = attr;
      values = ts.values();
      REQUIRE_EQ(values[5], -10.0 * mega);

      // -- verify that notifications was done (that is: that the version number on the subscribed  servers have ticked
      // up
      auto sum_subs_after = 0;
      for (auto const &sub : subs)
        sum_subs_after += sub->v;
      CHECK_GT(sum_subs_after, sum_subs);

      c.close();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      REQUIRE(false);
    }
    compute_servers.clear();
    clear_manager(s.get());
    s.reset();
  }

  TEST_CASE("stm/shop/get_log" * doctest::description("getting log from model, before and after running shop")) {
    dlib::set_all_logging_levels(dlib::LNONE);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = core::deltahours(1);
    auto const n_steps = (std::size_t) ((t_end - t_begin) / t_step);
    time_axis::generic_dt const ta{t_begin, t_step, n_steps};

    auto stm = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
    auto rstm = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);

    test::utils::temp_dir tmpdir{"stm_srv_shop_optimize.test.get_shop_logger."};
    auto s = std::make_unique<srv::dstm::server>();
    s->set_listening_ip("127.0.0.1");
    auto port_no = s->start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    auto cs = std::make_unique<srv::compute::server>();
    cs->set_listening_ip("127.0.0.1");
    auto cs_port_no = cs->start_server();
    std::this_thread::sleep_for(10ms);

    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{{.connection{host_port}}};
      c.add_compute_server({fmt::format("127.0.0.1:{}", cs_port_no)});

      auto [result] = c.get_version_info();
      CHECK_EQ(result, get_version_info(*s).version);

      // get model ids
      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      std::string const mdl_id = "test_stm_model";
      CHECK_EQ(c.add_model({mdl_id, stm}).status, true);
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);

      // Check log before anything else:
      auto log = c.get_log({mdl_id}).log;
      CHECK_EQ(log.size(), 0);

      auto cmd = shop::optimization_commands(run_id);
      REQUIRE(c.optimize({mdl_id, ta, cmd}).status);
      auto t_exit = utctime_now() + 30s; // reasonable limit
      while (not_done(c.get_state({mdl_id}).state) && utctime_now() < t_exit) {
        std::this_thread::sleep_for(2ms);
        log = c.get_log({mdl_id}).log; // just to check that we can get log while optimizing
      }

      auto [stm2] = c.get_model({mdl_id});
      shop::check_results(stm2, rstm, t_begin, t_end, t_step);
      log = c.get_log({mdl_id}).log;

      CHECK_GT(log.size(), 15); // We expect a few log messages

      c.close();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
    }
    cs.reset();
    clear_manager(s.get());
    s.reset();
  }

  TEST_CASE(
    "stm/shop/optimize_with_unbound_attributes"
    * doctest::description("Testing that dtss handles unbound time series properly before sending to SHOP")) {
    dlib::set_all_logging_levels(dlib::LNONE);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = core::deltahours(1);
    auto const n_steps = (std::size_t) ((t_end - t_begin) / t_step);
    time_axis::generic_dt const ta{t_begin, t_step, n_steps};

    test::utils::temp_dir tmpdir{"stm_srv_shop_optimize.test.optimize_with_unbound_attributes."};
    auto s = std::make_unique<srv::dstm::server>();

    auto port_no = s->start_server();
    REQUIRE_GT(port_no, 0);
    add_container(*s->dispatch.state->dtss, "test", (tmpdir / "ts").string());

    auto cs = std::make_unique<srv::compute::server>();
    cs->set_listening_ip("127.0.0.1");
    auto cs_port_no = cs->start_server();
    REQUIRE_GT(cs_port_no, 0);
    std::this_thread::sleep_for(10ms);

    auto stm = shop::build_simple_model_with_dtss(
      *(s->dispatch.state->dtss), t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto stm2 = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto rstm = shop::build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400, true); // Expected results
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{{.connection{host_port}}};
      c.add_compute_server({fmt::format("127.0.0.1:{}", cs_port_no)});
      c.add_model({"dtss_optimize_unbound", stm});
      c.add_model({"dtss_optimize_bound", stm2});
      stm = c.get_model({"dtss_optimize_unbound"}).model;
      // CHECK that stuff is unbound:
      auto hps = stm->hps[0];
      auto market = stm->market[0];
      CHECK_EQ(market->price.needs_bind(), true);
      CHECK_EQ(market->max_buy.needs_bind(), true);
      CHECK_EQ(market->max_sale.needs_bind(), true);
      CHECK_EQ(market->load.needs_bind(), true);
      CHECK_EQ(market->tsm["planned_revenue"].needs_bind(), true);
      auto ug = stm->unit_groups[0];
      CHECK_EQ(ug->obligation.cost.needs_bind(), true);
      CHECK_EQ(ug->obligation.schedule.needs_bind(), true);
      CHECK_EQ(ug->members[0]->active.needs_bind(), true);
      auto rsv = std::dynamic_pointer_cast<stm::reservoir>(hps->find_reservoir_by_name("reservoir"));
      CHECK_EQ(rsv->level.regulation_min.needs_bind(), true);
      CHECK_EQ(rsv->level.regulation_max.needs_bind(), true);
      CHECK_EQ(rsv->volume.static_max.needs_bind(), true);
      CHECK_EQ(rsv->water_value.endpoint_desc.needs_bind(), true);
      CHECK_EQ(rsv->level.realised.needs_bind(), true);
      CHECK_EQ(rsv->inflow.schedule.needs_bind(), true);

      auto wtr_flood = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute flood river"));
      CHECK_EQ(wtr_flood->discharge.static_max.needs_bind(), true);

      auto wtr_tunnel = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute input tunnel"));
      CHECK_EQ(wtr_tunnel->head_loss_coeff.needs_bind(), true);

      auto wtr_penstock = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute penstock"));
      CHECK_EQ(wtr_penstock->head_loss_coeff.needs_bind(), true);

      auto ps = std::dynamic_pointer_cast<stm::power_plant>(hps->find_power_plant_by_name("plant"));
      CHECK_EQ(ps->outlet_level.needs_bind(), true);

      auto gu = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_name("aggregate"));
      CHECK_EQ(gu->production.static_min.needs_bind(), true);
      CHECK_EQ(gu->production.static_max.needs_bind(), true);
      CHECK_EQ(gu->production.nominal.needs_bind(), true);

      // CHECK that we cannot do optimization on unbound model:
      auto cmd = shop::optimization_commands(run_id);
      REQUIRE_EQ(synch_optimize(c, "dtss_optimize_unbound", ta, cmd), std::nullopt);

      // Evaluate all unbound time series in the model:
      CHECK_EQ(true, c.evaluate_model({"dtss_optimize_unbound", ta.total_period(), true, false, {}}).status);
      stm = c.get_model({"dtss_optimize_unbound"}).model;
      market = stm->market[0];
      CHECK_EQ(market->tsm["planned_revenue"].needs_bind(), false);
      auto planned_revenue = market->price * 100.0;
      CHECK_EQ(planned_revenue, market->tsm["planned_revenue"]);
      hps = stm->hps[0];
      rsv = std::dynamic_pointer_cast<stm::reservoir>(hps->find_reservoir_by_name("reservoir"));
      wtr_flood = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute flood river"));
      wtr_tunnel = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute input tunnel"));
      wtr_penstock = std::dynamic_pointer_cast<stm::waterway>(hps->find_waterway_by_name("waterroute penstock"));
      ps = std::dynamic_pointer_cast<stm::power_plant>(hps->find_power_plant_by_name("plant"));
      gu = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_name("aggregate"));
      ug = stm->unit_groups[0];
      CHECK_EQ(ug->obligation.cost.needs_bind(), false);
      CHECK_EQ(ug->obligation.schedule.needs_bind(), false);
      CHECK_EQ(ug->members[0]->active.needs_bind(), false);

      auto m2 = stm2->market[0];
      auto hps2 = stm2->hps[0];
      CHECK_EQ(market->price, m2->price);
      CHECK_EQ(market->max_buy, m2->max_buy);
      CHECK_EQ(market->max_sale, m2->max_sale);
      CHECK_EQ(market->load, m2->load);

      auto rsv2 = std::dynamic_pointer_cast<stm::reservoir>(hps2->find_reservoir_by_name("reservoir"));
      CHECK_EQ(rsv->level.regulation_min, rsv2->level.regulation_min);
      CHECK_EQ(rsv->level.regulation_max, rsv2->level.regulation_max);
      CHECK_EQ(rsv->volume.static_max, rsv2->volume.static_max);
      CHECK_EQ(rsv->water_value.endpoint_desc, rsv2->water_value.endpoint_desc);
      CHECK_EQ(rsv->level.realised, rsv2->level.realised);
      CHECK_EQ(rsv->inflow.schedule, rsv2->inflow.schedule);

      auto wtr_flood2 = std::dynamic_pointer_cast<stm::waterway>(hps2->find_waterway_by_name("waterroute flood river"));
      auto wtr_tunnel2 = std::dynamic_pointer_cast<stm::waterway>(
        hps2->find_waterway_by_name("waterroute input tunnel"));
      auto wtr_penstock2 = std::dynamic_pointer_cast<stm::waterway>(hps2->find_waterway_by_name("waterroute penstock"));
      CHECK_EQ(wtr_flood->discharge.static_max, wtr_flood2->discharge.static_max);
      CHECK_EQ(wtr_tunnel->head_loss_coeff, wtr_tunnel2->head_loss_coeff);
      CHECK_EQ(wtr_penstock->head_loss_coeff, wtr_penstock2->head_loss_coeff);

      auto ps2 = std::dynamic_pointer_cast<stm::power_plant>(hps2->find_power_plant_by_name("plant"));
      auto gu2 = std::dynamic_pointer_cast<stm::unit>(hps2->find_unit_by_name("aggregate"));
      CHECK_EQ(ps->outlet_level, ps2->outlet_level);
      CHECK_EQ(gu->production.static_min, gu2->production.static_min);
      CHECK_EQ(gu->production.static_max, gu2->production.static_max);
      CHECK_EQ(gu->production.nominal, gu2->production.nominal);

      SUBCASE("optimize") {
        REQUIRE_EQ(synch_optimize(c, "dtss_optimize_unbound", ta, cmd), model_state::finished);
      }
      SUBCASE("tune") {
        REQUIRE(c.start_tune({"dtss_optimize_unbound"}).status);
        REQUIRE_EQ(synch_tune(c, "dtss_optimize_unbound", ta, cmd), model_state::tuning);
        REQUIRE_EQ(try_stop_tune(c, "dtss_optimize_unbound", core::from_seconds(10)), model_state::finished);

        REQUIRE_EQ(c.get_state({"dtss_optimize_unbound"}).state, model_state::finished);
      }

      stm = c.get_model({"dtss_optimize_unbound"}).model;
      shop::check_results(stm, rstm, t_begin, t_end, t_step);
      c.close();

    } catch (std::exception const &e) {
      DOCTEST_MESSAGE(e.what());
      CHECK_EQ(true, false);
    }
    cs.reset();
    clear_manager(s.get());
    s.reset();
  }

  TEST_CASE(
    "stress/stm/shop/dstm_optimize"
    * doctest::description("Testing that dtss handles multiple unbound time series properly before sending to SHOP")) {
    dlib::set_all_logging_levels(dlib::LNONE);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = core::deltahours(1);
    auto const n_steps = (std::size_t) ((t_end - t_begin) / t_step);
    time_axis::generic_dt const ta{t_begin, t_step, n_steps};

    test::utils::temp_dir tmpdir{"stm_srv_shop_optimize.test.dstm_stress_optimize."};
    auto s = std::make_unique<srv::dstm::server>();

    auto port_no = s->start_server();
    REQUIRE_GT(port_no, 0);
    auto host_port = string("localhost:") + std::to_string(port_no);
    add_container(*s->dispatch.state->dtss, "test", (tmpdir / "ts").string());

    auto cs = std::make_unique<srv::compute::server>();
    cs->set_listening_ip("127.0.0.1");
    auto cs_port_no = cs->start_server();
    REQUIRE_GT(cs_port_no, 0);

    std::this_thread::sleep_for(10ms);
    add_compute_server(*s, fmt::format("127.0.0.1:{}", cs_port_no));

    auto stm = shop::build_simple_model_with_dtss(
      *(s->dispatch.state->dtss), t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto stm2 = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto rstm = shop::build_simple_model(
      t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400, true); // Expected results
    int const n_connects = 20;

    try {
      // Result for each thread:
      auto cmd = shop::optimization_commands(run_id);
      std::vector<std::future<bool>> res;
      res.reserve(n_connects);

      for (std::size_t i = 0; i < n_connects; ++i) {
        res.emplace_back(std::async(std::launch::async, [i, ta, dt = t_step, &stm, &rstm, &cmd, &host_port]() -> bool {
          srv::dstm::client c{{.connection{host_port}}};
          string mid = "m" + std::to_string(i);
          c.add_model({mid, stm});
          c.evaluate_model({mid, ta.total_period(), false, false, {}});
          auto t_exit = utctime_now() + 30s; // reasonable limit
          while (!c.optimize({mid, ta, cmd}).status)
            std::this_thread::sleep_for(50ms);
          poll_done_for(c, mid, stm::shop::calc_suggested_timelimit(cmd) * 2);

          auto t0 = ta.total_period().start;
          auto [stmx] = c.get_model({mid});
          c.remove_model({mid});
          auto tN = ta.total_period().end;
          shop::check_results(stmx, rstm, t0, tN, dt);
          return true;
        }));
      }
      std::ranges::for_each(res, [](auto &el) {
        CHECK(el.get());
      });

    } catch (std::exception const &e) {
      FAIL(e.what());
    }
    cs.reset();
    clear_manager(s.get());
    s.reset();
  }

  TEST_CASE(
    "stm/shop/optimize_with_run_params_and_state_subscription"
    * doctest::description("Optimizing, while holding a subscription to the state of the system.")) {
    dlib::set_all_logging_levels(dlib::LNONE);
    bool const always_inlet_tunnels = false;
    bool const use_defaults = false;
    bool const with_results = true;
    auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    auto const t_step = core::deltahours(1);
    auto const n_steps = (std::size_t) ((t_end - t_begin) / t_step);
    generic_dt const ta{t_begin, t_step, n_steps};

    auto stm = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
    auto pa = proxy_attr(stm->run_params, "n_inc_runs", stm->run_params.n_inc_runs);
    std::string mdl_id("simple");
    auto url_n_inc = pa.url(fmt::format("dstm://M{}", mdl_id));
    auto rstm = shop::build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, with_results);

    std::string host_ip{"127.0.0.1"};
    test::utils::temp_dir tmp{"shyft.shop.subtst"};

    std::string doc_root = (tmp / "doc_root").string();
    auto srv = std::make_unique<test_server_dstm>(doc_root);
    srv->set_listening_ip(host_ip);
    auto port_no = srv->start_server();
    REQUIRE_GT(port_no, 0);

    auto cs = std::make_unique<srv::compute::server>();
    cs->set_listening_ip("127.0.0.1");
    auto cs_port_no = cs->start_server();
    REQUIRE_GT(cs_port_no, 0);
    auto host_port = fmt::format("localhost:{}", port_no);

    std::this_thread::sleep_for(10ms);

    // Set up expected responses and comparisons.
    std::vector<std::string> expected_responses{
      R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":0},{"attribute_id":"n_full_runs","data":0},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
      R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":33},{"attribute_id":"n_full_runs","data":0},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
      R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":0},{"attribute_id":"n_full_runs","data":0},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
      R"_({"request_id":"sr1","result":{"state":"idle"}})_",
      R"_({"request_id":"gl1","log_version":0,"bookmark":0,"result":[]})_",
      R"_({"request_id":"sr1","result":{"state":"running"}})_",
      R"_({"request_id":"gl1","log_version":1,"bookmark":0,"result":[]})_",
      R"_({"request_id":"gl1","log_version":1,"bookmark":168)_",
      R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":0},{"attribute_id":"n_full_runs","data":0},{"attribute_id":"head_opt","data":false},{"attribute_id":"run_time_axis","data":{"t0":1514768400.0,"dt":3600.0,"n":18}},{"attribute_id":"fx_log","data":[]}]}})_",
      R"_({"request_id":"sr1","result":{"state":"finished"}})_",
      R"_({"request_id":"gl1","log_version":1,"bookmark":249)_",
      R"_({"request_id":"sr1-unsub","subscription_id":"sr1","diagnostics":""})_",
      R"_({"request_id":"gl1-unsub","subscription_id":"gl1","diagnostics":""})_",
      R"_({"request_id":"finale","subscription_id":"initial","diagnostics":""})_"};

    std::vector<std::string> responses(expected_responses.size());
    std::size_t num_replies = 0;
    try {
      srv::dstm::client c{{.connection = core::srv_connection{host_port}}};
      c.add_compute_server({fmt::format("127.0.0.1:{}", cs_port_no)});
      CHECK_EQ(c.add_model({mdl_id, stm}).status, true);

      auto port = srv->start_web_api(host_ip, 0, doc_root, 1, 1);
      REQUIRE(srv->web_api_running());
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<::shyft::test::session>(ioc);
      s1->run(
        host_ip,
        int(port),
        R"_(run_params {"request_id": "initial", "model_key": "simple", "subscribe": true})_",
        [&](std::string const &response) {
          responses[num_replies++] = response;
          // MESSAGE("Got response: "<<response);
          if (response.find("finale") != std::string::npos)
            return "";
          if (num_replies == 1) { // First time we update a model.
            c.set_attrs({.attrs{{url_n_inc, 33}}});
            return "continue_reading!";
          }
          if (num_replies == 2) { // First time we update a model.
            c.set_attrs({.attrs{{url_n_inc, 0}}});
            return "continue_reading!";
          }
          if (num_replies == 3) {
            return R"_(get_state {"request_id": "sr1", "model_key": "simple", "subscribe": true})_";
          }
          if (num_replies == 4) {
            return R"_(get_log {"request_id": "gl1", "model_key": "simple", "subscribe": true})_";
          }

          if (num_replies == 5) { // Update model via optimization
            std::this_thread::sleep_for(10ms);
            auto cmd = shop::optimization_commands(1, false);
            auto const t_begin = core::create_from_iso8601_string("2018-01-01T01:00:00Z");
            auto const t_end = core::create_from_iso8601_string("2018-01-01T19:00:00Z");
            auto const t_step = core::deltahours(1);
            std::size_t const n_steps = (t_end - t_begin) / t_step;
            generic_dt const ta{t_begin, t_step, n_steps};

            REQUIRE_EQ(c.optimize({mdl_id, ta, cmd}).status, true);

            auto t_exit = core::utctime_now() + 30s; // reasonable limit
            while (not_done(c.get_state({mdl_id}).state) && core::utctime_now() < t_exit)
              std::this_thread::sleep_for(10ms);
            REQUIRE_EQ(c.get_state({mdl_id}).state, model_state::finished);
            std::this_thread::sleep_for(20ms);

            auto [stm_now] = c.get_model({mdl_id});
            shop::check_results(stm_now, rstm, t_begin, t_end, t_step);
            return "continue_reading!";
          }
          if (num_replies == 6)
            return R"_(unsubscribe {"request_id": "sr1-unsub", "subscription_id": "sr1"})_";
          if (num_replies == 7)
            return R"_(unsubscribe {"request_id": "gl1-unsub", "subscription_id": "gl1"})_";
          return R"_(unsubscribe {"request_id":"finale", "subscription_id":"initial"})_";
        });

      ioc.run();
      s1.reset();
      std::this_thread::sleep_for(700ms);
      srv->stop_web_api();

      REQUIRE_EQ(responses.size(), expected_responses.size());
      for (std::size_t i = 0; i < responses.size(); ++i) {
        bool found_match = false; // order of responses might differ for the two last
        for (std::size_t j = 0; j < responses.size() && !found_match; ++j) {
          found_match = responses[j].starts_with(expected_responses[i]);
        }
        CHECK_MESSAGE(found_match, "failed for the ", i, "th response: ", expected_responses[i], "!=", responses[i]);
      }

    } catch (...) {
    }
    cs.reset();
    clear_manager(srv.get());
    srv.reset();
  }

  TEST_SUITE_END();

}
