#include <csignal>
#include <cstddef>
#include <functional>
#include <future>
#include <memory>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

#include <boost/asio/buffer.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/buffers_to_string.hpp>
#include <boost/beast/core/multi_buffer.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/websocket/rfc6455.hpp>
#include <boost/beast/websocket/stream.hpp>
#include <boost/core/ignore_unused.hpp>
#include <boost/system/detail/error_code.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/srv/application/message_handler.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/energy_market/stm/srv/application/request_handler.h>
#include <shyft/energy_market/stm/srv/application/server.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/web_api/bg_work_result.h>

#include <test/test_pch.h>
#include <test/test_utils.h>

namespace shyft::energy_market::srv::experimental::application::test {

  struct test_server : stm::srv::experimental::application::server {
    test_server()
      : rh(this->manager) {
    }

    web_api::energy_market::srv::experimental::application::request_handler rh;
    std::future<int> web_srv;

    auto start_web_api(std::string const &host_ip, int port, std::string const &docs, int fg_threads, int bg_threads) {
      if (!web_srv.valid()) {
        web_srv = std::async(std::launch::async, [this, host_ip, port, docs, fg_threads, bg_threads]() -> int {
          return web_api::run_web_server(
            rh,
            host_ip,
            static_cast<unsigned short>(port),
            std::make_shared<std::string>(docs),
            fg_threads,
            bg_threads);
        });
      }
      return rh.wait_for_real_port();
    }

    bool web_api_running() const {
      return web_srv.valid();
    }

    void stop_web_api() {
      if (web_srv.valid()) {
        std::raise(SIGINT);
        (void) web_srv.get();
      }
    }
  };

  struct session : std::enable_shared_from_this<session> {

    boost::asio::ip::tcp::resolver resolver;
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws;
    boost::beast::multi_buffer buffer;
    std::string host;
    std::string text;
    std::string response;
    std::string diagnostics;
    std::function<std::string(std::string const &)> report_response;

    void fail(boost::system::error_code er, char const *what) {
      diagnostics = fmt::format("{}: {}\n", what, er.message());
    }

#define fail_on_error(ec, diag) \
  if ((ec)) \
    return fail((ec), (diag));


   public:
    session(boost::asio::io_context &ioc)
      : resolver(ioc)
      , ws(ioc) {
    }

    template <typename T>
    void run(std::string_view host, int port, std::string_view text, T &&rep_response) {
      this->host = host;
      this->text = text;
      report_response = rep_response;
      resolver.async_resolve(
        host,
        std::to_string(port),
        [me = shared_from_this()](boost::system::error_code er, boost::asio::ip::tcp::resolver::results_type results) {
          me->on_resolve(er, results);
        });
    }

    void on_resolve(boost::system::error_code ec, boost::asio::ip::tcp::resolver::results_type results) {
      fail_on_error(ec, "resolve");

      boost::asio::async_connect(
        ws.next_layer(),
        results.begin(),
        results.end(),
        std::bind(&session::on_connect, shared_from_this(), std::placeholders::_1));
    }

    void on_connect(boost::system::error_code ec) {
      fail_on_error(ec, "connect");
      ws.async_handshake(host, "/", [me = shared_from_this()](boost::system::error_code ec) {
        me->on_handshake(ec);
      });
    }

    void on_handshake(boost::system::error_code ec) {
      fail_on_error(ec, "handshake");

      if (text.empty()) {
        ws.async_close(
          boost::beast::websocket::close_code::normal, [me = shared_from_this()](boost::system::error_code ec) {
            me->on_close(ec);
          });
      } else {
        ws.async_write(
          boost::asio::buffer(text), [me = shared_from_this()](boost::system::error_code ec, size_t bytes_transferred) {
            me->on_write(ec, bytes_transferred);
          });
      }
    }

    void on_write(boost::system::error_code ec, std::size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "write");

      ws.async_read(buffer, [me = shared_from_this()](boost::system::error_code ec, size_t bytes_transferred) {
        me->on_read(ec, bytes_transferred);
      });
    }

    void on_read(boost::system::error_code ec, size_t bytes_transferred) {
      boost::ignore_unused(bytes_transferred);
      fail_on_error(ec, "read");

      response = boost::beast::buffers_to_string(buffer.data());
      buffer.consume(buffer.size());
      text = report_response(response);
      if (text.size()) {
        ws.async_write(
          boost::asio::buffer(text), [me = shared_from_this()](boost::system::error_code ec, size_t bytes_transferred) {
            me->on_write(ec, bytes_transferred);
          });
      } else {
        ws.async_close(
          boost::beast::websocket::close_code::normal, [me = shared_from_this()](boost::system::error_code ec) {
            me->on_close(ec);
          });
      }
    }

    void on_close(boost::system::error_code ec) {
      fail_on_error(ec, "close");
    }

#undef fail_on_error
  };

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/application_web_api/basic_requests") {
    test_server server;
    std::string host_ip{"127.0.0.1"};
    server.set_listening_ip(host_ip);
    server.start_server();

    auto session1 = stm::srv::stm_task(1, "session1", core::from_seconds(3600));

    auto const run1 = std::make_shared<stm::srv::stm_case>(1, "run1", core::from_seconds(3600));
    session1.add_case(run1);
    shyft::srv::model_info mi1(session1.id, session1.name, session1.created, "");
    server.manager.mutate<stm::srv::experimental::application::model_tag::task>([&](auto &&models) {
      models.try_emplace(mi1.id, mi1, session1);
    });

    auto port = server.start_web_api(host_ip, 0, "", 1, 1);
    REQUIRE(server.web_api_running());

    std::vector<std::string> requests{
      R"_(get_task_infos {"request_id": "1"})_",
      R"_(keyword {"some": "json structure"})_",
      R"_(read_task {"request_id": "2", "model_id": 1})_",
      R"_(update_task_info {
                      "request_id": "3",
                      "model_info": {
                          "id": 1,
                          "name": "updated model_info",
                          "created": 4100.0,
                          "json": "This mi has been updated"
                      }
                  })_",
      R"_(get_task_infos {"request_id": "4"})_",
      R"_(store_task {"request_id": "5",
                            "model": {
                                "id": 0,
                                "name": "stored1",
                                "created": 4600.0,
                                "labels": ["test", "web API"]
                            }
                        })_",
      R"_(get_task_infos {"request_id": "6", "model_ids": [2]})_",
      R"_(read_task {"request_id": "7", "model_id": 2})_",
      R"_(store_task {
                "request_id": "8",
                "model": {
                    "id": 3,
                    "name": "stored2",
                    "created": 10.0,
                    "labels": ["test", "web API"]
                },
                "model_info": {
                    "id": 3,
                    "name": "something else",
                    "json": "misc."
                }
            })_",
      R"_(get_task_infos {"request_id":"9", "model_ids": [2,3]})_",
      R"_(read_task {"request_id": "10", "model_id": 3})_",
      R"_(remove_task {"request_id":"11", "model_id": 3})_",
      R"_(get_task_infos {"request_id": "12", "model_ids": [3]})_",
      R"_(get_task_infos {"request_id": "13", "period": [4000.0, 4101.0]})_",
      R"_(get_task_infos {"request_id": "14", "period": [5000.0, 5010.0]})_",
      R"_(add_case {"request_id": "15", "model_id": 1,
                           "cse": {
                               "id": 2,
                               "name": "testrun",
                               "created": 10.0,
                               "labels": ["test"],
                               "model_refs": []
                           }
                   })_",
      R"_(get_case_by_id {"request_id": "16", "model_id": 1, "case_id": 2})_",
      R"_(remove_case_by_id {"request_id": "17", "model_id": 1, "case_id": 2})_",
      R"_(add_task_ref {"request_id": "18", "model_id": 1, "case_id": 1,
                           "model_ref": {
                               "host": "testhost",
                               "port_num": 12,
                               "api_port_num": 34,
                               "model_key": "mkey"
                           }
                   })_",
      R"_(get_task_ref {"request_id": "19", "model_id": 1, "case_id": 1, "model_key": "mkey"})_",
      R"_(remove_task_ref {"request_id": "20", "model_id": 1, "case_id": 1, "model_key": "mkey"})_",
    };

    std::vector<std::string> responses;
    std::size_t r = 0;
    {
      boost::asio::io_context ioc;
      auto s1 = std::make_shared<session>(ioc);
      s1->run(host_ip, port, requests[r], [&](std::string const &web_response) {
        responses.push_back(web_response);
        ++r;
        return r >= requests.size() ? std::string("") : requests[r];
      });
      ioc.run();
      s1.reset();
    }

    std::vector<std::string> expectations{
      R"_({"request_id":"1","result":[{"id":1,"name":"session1","created":3600.0,"json":""}]})_",
      R"_({"request_id":"__request_id_not_found__","diagnostics":"Unknown keyword: keyword"})_",
      R"_({"request_id":"2","result":{"id":1,"name":"session1","created":3600.0,"json":"","labels":[],"cases":[{"id":1,"name":"run1","created":3600.0,"json":"","labels":[],"model_refs":[]}],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":"","stm_id":0,"labels":[]},"task_name":""}})_",
      R"_({"request_id":"3","result":true})_",
      R"_({"request_id":"4","result":[{"id":1,"name":"updated model_info","created":4100.0,"json":"This mi has been updated"}]})_",
      R"_({"request_id":"5","result":2})_",
      R"_({"request_id":"6","result":[{"id":2,"name":"stored1","created":4600.0,"json":""}]})_",
      R"_({"request_id":"7","result":{"id":2,"name":"stored1","created":4600.0,"json":"","labels":["test","web API"],"cases":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":"","stm_id":0,"labels":[]},"task_name":""}})_",
      R"_({"request_id":"8","result":3})_",
      R"_({"request_id":"9","result":[{"id":2,"name":"stored1","created":4600.0,"json":""},{"id":3,"name":"something else","created":10.0,"json":"misc."}]})_",
      R"_({"request_id":"10","result":{"id":3,"name":"stored2","created":10.0,"json":"","labels":["test","web API"],"cases":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":"","stm_id":0,"labels":[]},"task_name":""}})_",
      R"_({"request_id":"11","result":false})_",
      R"_({"request_id":"12","result":[]})_",
      R"_({"request_id":"13","result":[{"id":1,"name":"updated model_info","created":4100.0,"json":"This mi has been updated"}]})_",
      R"_({"request_id":"14","result":[]})_",
      R"_({"request_id":"15","result":true})_",
      R"_({"request_id":"16","result":{"id":2,"name":"testrun","created":10.0,"json":"","labels":["test"],"model_refs":[]}})_",
      R"_({"request_id":"17","result":true})_",
      R"_({"request_id":"18","result":true})_",
      R"_({"request_id":"19","result":{"host":"testhost","port_num":12,"api_port_num":34,"model_key":"mkey","stm_id":0,"labels":[]}})_",
      R"_({"request_id":"20","result":true})_",
    };
    REQUIRE_EQ(expectations.size(), responses.size());

    for (auto const &[expected, actual] : std::views::zip(expectations, responses)) {
      CHECK(!expected.empty());
      CHECK_EQ(expected, actual);
    }
    server.stop_web_api();
  }

  TEST_SUITE_END();
}
