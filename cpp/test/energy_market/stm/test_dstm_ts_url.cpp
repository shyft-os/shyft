#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/url_parse.h>

#include "build_test_system.h"
#include <doctest/doctest.h>
#include <test/test_pch.h>

namespace shyft::energy_market::stm {

  using gta_t = shyft::time_axis::generic_dt;
  using shyft::time_series::dd::apoint_ts;
  using shyft::core::utctime;
  using shyft::core::deltahours;
  using shyft::core::calendar;

  namespace {
    struct dstm_fixture {

      std::unique_ptr<srv::dstm::server> dstm;
      std::shared_ptr<stm_system> sys;

      dstm_fixture() {
        dstm = std::make_unique<srv::dstm::server>();
        sys = test::create_simple_system(1, "sys");
        add_model(*dstm, "sys", sys);
      }

      ~dstm_fixture() {
        dstm->clear();
        dstm.reset();
        sys.reset();
      }
    };
  }

  void verify_dstm_resolve(stm_system const &sys, std::string_view url, std::string_view expected_ts_id) {
    auto ts = url_get_ts(sys, url).value();
    FAST_CHECK_EQ(expected_ts_id, ts.id());
  }

  void verify_dstm_resolve_sz(stm_system const &sys, std::string_view url, std::size_t expected_ts_size) {
    auto ts = url_get_ts(sys, url).value();
    FAST_CHECK_EQ(expected_ts_size, ts.size());
  }

  void verify_dstm_resolve_throws(stm_system const &sys, std::string_view url) {
    url_get_ts(sys, url).value();
  }

  std::string path_resolver(url_tuple const &d) {
    auto &&[mid, comps, attr] = d;
    if (comps.size() > 0) {
      return fmt::format("dstm://M{}/{}.{}", mid, fmt::join(comps, "/"), attr);
    }
    return fmt::format("dstm://M{}.{}", mid, attr);
  }

  void verify_resolve_url_path(std::string_view url) {
    auto check = [&]<typename T>(T const &d) {
      if constexpr (std::is_same_v<T, url_parse_error>) {
        fmt::print("Failed with error:: {}", d.what);
        FAST_CHECK_EQ(true, false);
      } else if constexpr (std::is_same_v<T, url_tuple>) {
        auto res_url = path_resolver(d);
        FAST_CHECK_EQ(url, res_url);
      }
    };
    auto r = url_parse_to_result(url);
    std::visit(check, r);
  }

  void bad_syntax_url_path(std::string_view url) {
    auto check = [&]<typename T>(T const &) {
      if constexpr (std::is_same_v<T, url_parse_error>) {
        throw std::runtime_error("failed to parse");
      }
    };
    auto r = url_parse_to_result(url);
    std::visit(check, r);
    MESSAGE("incredible, it succeded");
  }

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/url/parser") {
    SUBCASE("basic") {
      // allows urls with and without components:
      verify_resolve_url_path("dstm://Msys/H1/G1.discharge.result");
      verify_resolve_url_path("dstm://Msys.discharge.result");
      // allows ts args with any char:
      verify_resolve_url_path("dstm://Msys.ts.@-#");
      // does not allow ts as sub arg:
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.any.ts.@@@@@"));
      // does not allow ts arg with empty name:
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.ts."));

      // allows valid c++ args of different lengths:
      verify_resolve_url_path("dstm://Msys.arg0_11.SomeRegularArg00001325499.Is.Here.And.Goes.On.For.A.Long.Time");
      // also starting with an _:
      verify_resolve_url_path("dstm://Msys._regular");

      // does not allow args starting with a number:
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.0some.arg"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.also.for.subarg.0some.arg"));
      // does not allow args ending with other chars
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.also.for.subarg.0some.arg-"));
      // does not allow empty args (urls ending with a . or containing two in a row):
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.0some."));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys.also.for..0some.arg"));
    }
    SUBCASE("permutations") {
      int g_id = 100, c_id = 203;
      std::map<char, std::string> subs;
      subs['H'] = std::string{"RUWPCGA"};
      subs['u'] = std::string{"M"};
      // subs['m']=string{"A"};
      for (auto g : std::string{"Humcp"}) { // these are the current main types
        for (auto c : subs[g]) {            // these are the current component types
          std::string ts_url(fmt::format("dstm://Mabc/{}{}/{}{}.some.attribute", g, g_id, c, c_id));
          verify_resolve_url_path(ts_url);
        }
      }
    }
    SUBCASE("syntax") {
      CHECK_THROWS(bad_syntax_url_path("suppe//Msys/H1/G1.discharge.result"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Zsys/H1/G1.discharge.result"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys/F1/G1x.discharge.result"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys/Hx1/G1.discharge.result"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys/H1/xx1.discharge.result"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys/H1/Ra1.discharge.result"));
      CHECK_THROWS(bad_syntax_url_path("dstm://Msys/H1/R1."));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/hps") {
    SUBCASE("reservoir") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/H1/R1.level.schedule", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/H1/R1.volume.result", "shyft://test/r.volume.result");
    }
    SUBCASE("gate") {
      verify_dstm_resolve(*sys, "dstm://Msys/H1/G1.discharge.result", "shyft://test/g.discharge.result");
    }
    SUBCASE("unit") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/H1/U1.discharge.result", 0);
      verify_dstm_resolve_sz(*sys, "dstm://Msys/H1/U1.discharge.constraint.min", 6);
    }
    SUBCASE("waterway") {
      verify_dstm_resolve(*sys, "dstm://Msys/H1/W1.discharge.static_max", "shyft://test/w.discharge.static_max");
      verify_dstm_resolve_sz(*sys, "dstm://Msys/H1/W1.discharge.result", 6);
    }
    SUBCASE("powerplant") {
      verify_dstm_resolve(*sys, "dstm://Msys/H1/P2.production.schedule", "shyft://test/pp.production.schedule");
    }
    SUBCASE("reservoir_aggregate") {
      verify_dstm_resolve(*sys, "dstm://Msys/H1/A2.volume.schedule", "shyft://test/ra.volume.schedule");
    }
    SUBCASE("catchment") {
      verify_dstm_resolve(*sys, "dstm://Msys/H1/C1.inflow_m3s", "shyft://test/ca.inflow_m3s");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/H2/R1.level.schedule"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/H1/R2.level.schedule"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/H1/R1.xlevel.schedule"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/H1.schedule"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/unit_group") {
    SUBCASE("group_level") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/u3.obligation.cost", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/u3.obligation.schedule", "shyft://test/ug.obligation.schedule");
    }
    SUBCASE("member_level") {
      verify_dstm_resolve(*sys, "dstm://Msys/u3/M1.active", "shyft://test/ug/m.active");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/u1.obligation.cost"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/u3/M1/X1.active"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/u3/X1.active"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/market_area") {
    SUBCASE("group_level") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/m2.load", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/m2.price", "shyft://test/market.price");
      verify_dstm_resolve(*sys, "dstm://Msys/m2.ts.price-alternative", "shyft://test/market.price-alternative");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/m1.load"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/m2/X1.active"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/m2.price-alternative"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/contract") {
    SUBCASE("group_level") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/c4.revenue", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/c4.quantity", "shyft://test/contract.volume");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/c1.quantity"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/c4/X1.active"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/contract_portfolio") {
    SUBCASE("group_level") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/p5.revenue", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/p5.quantity", "shyft://test/contract_portfolio.volume");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/p1.volume"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/p5/X1.active"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/network") {
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6.busbars"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/busbar") {
    SUBCASE("group_level") {
      verify_dstm_resolve(*sys, "dstm://Msys/n6/b8.price.result", "shyft://test/busbar.price.result");
    }
    SUBCASE("unit_member") {
      verify_dstm_resolve(*sys, "dstm://Msys/n6/b8/M1.active", "shyft://test/ug/m.active");
    }
    SUBCASE("power_module_member") {
      verify_dstm_resolve(*sys, "dstm://Msys/n6/b8/P9.active", "shyft://test/power_module/m.active");
    }
    SUBCASE("wind_farm_member") {
      verify_dstm_resolve(*sys, "dstm://Msys/n6/b8/w99.active", "shyft://test/wind_farm/m.active");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6/b8.test"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6/B7.price.result"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6/b8/X9.active"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6/b8/P9/X5.active"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/transmission_line") {
    SUBCASE("group_level") {
      verify_dstm_resolve(*sys, "dstm://Msys/n6/t7.capacity", "shyft://test/transmission_line.capacity");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6/t7.test"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/n6/x7.capacity"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/power_module") {
    SUBCASE("group_level") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/P9.power.result", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/P9.power.schedule", "shyft://test/power_module.power.schedule");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/P9.power.test"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/P9/X1.power.result"));
    }
  }

  TEST_CASE_FIXTURE(dstm_fixture, "stm/url/wind_farm") {
    SUBCASE("group_level") {
      verify_dstm_resolve_sz(*sys, "dstm://Msys/w99.production.realised", 0u);
      verify_dstm_resolve(*sys, "dstm://Msys/w99.production.result", "shyft://test/wind_farm.production.result");
    }
    SUBCASE("bad_lookups") {
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/w99.production.test"));
      CHECK_THROWS(verify_dstm_resolve_throws(*sys, "dstm://Msys/w9/X1.production.result"));
    }
  }

  TEST_SUITE_END();

}
