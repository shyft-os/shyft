// notice that the include order matters in serialization,
// so take care when clang tidy reorders include
#include <fstream>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/serialization_tools.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/wind_turbine.h>
#include <shyft/mp.h>
// since we do local serialization here, we need these:
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
// then finally the tests.
#include <doctest/doctest.h>
#include <test/energy_market/stm/utilities.h>

namespace hana = boost::hana;

namespace doctest {

  // template specialization to generate readable error messages
  template <>
  struct StringMaker<std::vector<char const *>> {
    static String convert(std::vector<char const *> const &vec) {
      std::ostringstream oss;
      oss << "[ ";
      std::copy(vec.begin(), vec.end(), std::ostream_iterator<char const *>(oss, " "));
      oss << "]";
      return oss.str().c_str();
    }
  };
}

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = mp;
  namespace sts = time_series;

  using hydro_power::xy_point_curve;
  using hydro_power::xy_point_curve_with_z;
  using hydro_power::turbine_operating_zone;
  using hydro_power::turbine_description;
  using core::utctime;
  using core::geo_point;
  using time_axis::fixed_dt;
  using time_axis::generic_dt;
  using time_axis::generic_dt;
  using time_series::dd::apoint_ts;
  using time_series::POINT_INSTANT_VALUE;

  namespace {
    template <typename T>
    auto to_blob(std::shared_ptr<T> &t) -> std::string {
      std::ostringstream oss(std::ios::binary);
      core::core_oarchive archive(oss,core::arch_info_flags);

      serialize_stm_attributes(*t, archive);
      return oss.str();
    }

    template <>
    auto to_blob<stm_system>(std::shared_ptr<stm_system> &t) -> std::string {
      return stm_system::to_blob(t);
    }

    template <typename T>
    auto from_blob(std::string istring) -> std::shared_ptr<T> {
      std::istringstream iss(istring, std::ios::binary);
      core::core_iarchive archive(iss, core::arch_info_flags);

      auto t = std::make_shared<T>();
      serialize_stm_attributes(*t, archive);
      return t;
    }

    // Serializing only sub_attrs from stm_system without serializing the system leads to memory leak as
    // boost creates the memory for the raw pointer stm_system in optimization result, which will not be deallocd
    // using this helper works as we deserialize the whole system
    template <>
    auto from_blob<stm_system>(std::string istring) -> std::shared_ptr<stm_system> {
      return stm_system::from_blob(istring);
    }

    template <typename T>
    auto serialize_deserialize(std::shared_ptr<T> &t) -> std::shared_ptr<T> {
      return from_blob<T>(to_blob(t));
    }
  }

  auto create_fx_log = []() {
    std::vector<std::pair<utctime, std::string>> log;
    log.emplace_back(11, "test");
    return log;
  };

  auto create_time_axis_generic_dt = []() {
    using namespace std::chrono_literals;
    return generic_dt{1000s, 4600s, 8200};
  };

  auto create_hps_vector = []() {
    std::vector<std::shared_ptr<stm_hps>> hps_vec;
    hps_vec.push_back(std::make_shared<stm_hps>(1, "hps_1"));
    hps_vec.push_back(std::make_shared<stm_hps>(2, "hps_2"));
    return hps_vec;
  };

  auto create_market_vector = []() {
    std::vector<std::shared_ptr<energy_market_area>> markets;
    markets.push_back(std::make_shared<energy_market_area>());
    markets.back()->id = 2;
    markets.back()->name = "test_market";
    return markets;
  };

  auto create_contract_vector = []() {
    std::vector<std::shared_ptr<contract>> contracts;
    contracts.push_back(std::make_shared<contract>());
    contracts.back()->id = 3;
    contracts.back()->name = "test_contract";
    return contracts;
  };

  auto create_contract_portfolio_vector = []() {
    std::vector<std::shared_ptr<contract_portfolio>> contracts;
    contracts.push_back(std::make_shared<contract_portfolio>());
    contracts.back()->id = 4;
    contracts.back()->name = "test_contract_portfolio";
    return contracts;
  };

  auto create_network_vector = []() {
    std::vector<std::shared_ptr<network>> networks;
    networks.push_back(std::make_shared<network>());
    networks.back()->id = 3;
    networks.back()->name = "test_network";
    return networks;
  };

  auto create_power_module_vector = []() {
    std::vector<std::shared_ptr<power_module>> power_modules;
    power_modules.push_back(std::make_shared<power_module>());
    power_modules.back()->id = 3;
    power_modules.back()->name = "test_power_modules";
    return power_modules;
  };

  auto create_run_parameters = []() -> std::shared_ptr<run_parameters> {
    auto rp = std::make_shared<run_parameters>();
    rp->n_inc_runs = 1;
    rp->n_full_runs = 1;
    return rp;
  };
  auto create_unit_groups = []() -> std::vector<unit_group_> {
    std::vector<unit_group_> r;
    r.push_back(std::make_shared<unit_group>());
    r.back()->id = 1;
    r.back()->name = "UG1";
    return r;
  };

  auto create_wind_farms = []() -> std::vector<std::shared_ptr<wind_farm>> {
    std::vector<std::shared_ptr<wind_farm>> wind_farms;
    wind_farms.push_back(std::make_shared<wind_farm>());
    wind_farms.back()->id = 3;
    wind_farms.back()->name = "test_wind_farm";
    wind_farms.back()->location = geo_point{0.0, 1.0, 2.0};
    // TODO: add wind-turbines
    return wind_farms;
  };


  // To automate the tests we need a non-default constructed instances of the value types
  // used in the attribute structs.
  auto values = hana::make_tuple(
    (uint16_t) 5,
    (bool) true,
    geo_point{1.0, 2.0, 3.0},
    test::create_t_double(11, 42.0),
    test::create_t_xy(12, std::vector<double>{1, 2}),
    test::create_t_xyz(13, std::vector<double>{1, 2, 3}),
    test::create_t_turbine_description(14, std::vector<double>{1, 2, 3}),

    create_fx_log(),
    create_time_axis_generic_dt(),
    create_hps_vector(),
    create_market_vector(),
    create_contract_vector(),
    create_contract_portfolio_vector(),
    create_network_vector(),
    create_power_module_vector(),
    create_run_parameters(),
    create_unit_groups(),
    create_wind_farms());

  auto const type_value_map = hana::fuse(hana::make_map)(
    hana::transform(hana::zip(hana::transform(values, hana::typeid_), values), hana::fuse(hana::make_pair)));

  // assign to each struct leaf the value of the matching type.
  template <typename T>
  void assign_values(T &t) {
    auto key = hana::compose(mp::accessor_ptr_type, mp::leaf_accessor);
    auto assign_val = [&](auto a) {
      auto v = hana::find(type_value_map, key(a));
      if constexpr (!hana::is_nothing(v))
        mp::leaf_access(t, a) = v.value();
    };
    hana::for_each(mp::leaf_accessors(hana::type_c<T>), assign_val);
  }

  // list ids of attributes that are equal between o1 and o2.
  template <typename T>
  std::vector<char const *> equal_attrs(T &o1, T &o2) {
    std::vector<char const *> res;
    hana::for_each(mp::leaf_accessors(hana::type_c<T>), [&o1, &o2, &res](auto a) {
      auto v1 = mp::leaf_access(o1, a);
      auto v2 = mp::leaf_access(o2, a);
      if (equal_attributes(v1, v2))
        res.push_back(mp::leaf_accessor_id_str(a));
    });
    return res;
  }

  // list ids of attributes that are different between o1 and o2.
  template <typename T>
  std::vector<char const *> not_equal_attrs(T &o1, T &o2) {
    std::vector<char const *> res;
    hana::for_each(mp::leaf_accessors(hana::type_c<T>), [&o1, &o2, &res](auto a) {
      auto v1 = mp::leaf_access(o1, a);
      auto v2 = mp::leaf_access(o2, a);
      if (!equal_attributes(v1, v2))
        res.push_back(mp::leaf_accessor_id_str(a));
    });
    return res;
  }

  // identify attributes we have not handled (missing from the values-tuple).
  template <typename T>
  std::vector<char const *> unhandled_attrs() {
    auto o1 = std::make_shared<T>();
    auto o2 = std::make_shared<T>();
    assign_values(*o1);
    return equal_attrs(*o1, *o2);
  };

  // Custom template for stm_system, due too optimization_summary
  // is created in constructors, hence not a nullptr
  template <>
  std::vector<char const *> unhandled_attrs<stm_system>() {
    auto o1 = std::make_shared<stm_system>();
    auto o2 = std::make_shared<stm_system>();
    o2->summary = nullptr;
    assign_values(*o1);
    return equal_attrs(*o1, *o2);
  }

  // identify attributes that did not pass the serialize-deserialize cycle.
  template <typename T>
  std::vector<char const *> unserialized_attrs() {
    auto o1 = std::make_shared<T>();
    assign_values(*o1);
    auto o2 = serialize_deserialize(o1);
    return not_equal_attrs(*o1, *o2);
  };

  auto const empty_list = std::vector<char const *>{};


  TEST_SUITE_BEGIN("stm");

  // For each struct, we want to test that all its attributes survive
  // a serialization-deserialization cycle in a mostly automated way.
  //
  // The for each object we:
  //  - test that we can assign to each leaf member a non-default value.
  //  - test that attributes are the same after passing the object
  //    through a serialize-deserialize cycle.
  //
  // The non-default values are required in order to make a meaningful
  // comparison between the original and the deserialized object.
  //
  // The assigned values are taken from a tuple according to their type.
  // When adding a new attribute to an object make sure that its value
  // type is found in the tuple of known value types.


  // stm hydro components

  TEST_CASE("stm/reservoir") {
    CHECK_EQ(unhandled_attrs<reservoir>(), empty_list);
    CHECK_EQ(unserialized_attrs<reservoir>(), empty_list);
  }

  TEST_CASE("stm/reservoir_aggregate") {
    CHECK_EQ(unhandled_attrs<reservoir_aggregate>(), empty_list);
    CHECK_EQ(unserialized_attrs<reservoir_aggregate>(), empty_list);
  }

  TEST_CASE("stm/unit") {
    CHECK_EQ(unhandled_attrs<unit>(), empty_list);
    CHECK_EQ(unserialized_attrs<unit>(), empty_list);
  }

  TEST_CASE("stm/power_plant") {
    CHECK_EQ(unhandled_attrs<power_plant>(), empty_list);
    CHECK_EQ(unserialized_attrs<power_plant>(), empty_list);
  }

  TEST_CASE("stm/waterway") {
    CHECK_EQ(unhandled_attrs<waterway>(), empty_list);
    CHECK_EQ(unserialized_attrs<waterway>(), empty_list);
  }

  TEST_CASE("stm/gate") {
    CHECK_EQ(unhandled_attrs<gate>(), empty_list);
    CHECK_EQ(unserialized_attrs<gate>(), empty_list);
  }

  TEST_CASE("stm/wind_farm") {
    CHECK_EQ(unhandled_attrs<wind_farm>(), empty_list);
    CHECK_EQ(unserialized_attrs<wind_farm>(), empty_list);
  }

  //

  TEST_CASE("stm/system") {
    CHECK_EQ(unhandled_attrs<stm_system>(), empty_list);
    CHECK_EQ(unserialized_attrs<stm_system>(), empty_list);
  }

  TEST_CASE("stm/run_parameters") {
    CHECK_EQ(unhandled_attrs<run_parameters>(), empty_list);
    CHECK_EQ(unserialized_attrs<run_parameters>(), empty_list);

    // no check for attribute 'mdl', since it is pointer to unowned object
  }

  TEST_SUITE_END();

} // end namespace
