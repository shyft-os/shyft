#include <chrono>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <thread>
#include <utility>
#include <variant>
#include <vector>

#include <boost/asio/thread_pool.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reflection.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit_group_type.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/common.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/build_test_system.h>
#include <test/energy_market/stm/utilities.h>

namespace shyft::energy_market::stm {

  using shyft::core::from_seconds;
  using shyft::core::deltahours;

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/context_aref_ts_id_replace") {
    time_series::dd::apoint_ts ats("dstm://Mold_model_key/H1/R1/A1");
    time_series::dd::aref_ts *rts = time_series::dd::ts_as_mutable<time_series::dd::aref_ts>(ats.ts);
    CHECK_EQ(rts != nullptr, true);
    CHECK_EQ(rts->id, "dstm://Mold_model_key/H1/R1/A1");

    auto rpos = rts->id.find("/", 8);
    rts->id = rts->id.replace(8, rpos - 8, "new_mkey");
    CHECK_EQ(rts->id, "dstm://Mnew_mkey/H1/R1/A1");

    // Check that the id was changed for ats as well:
    auto arts = time_series::dd::ts_as<time_series::dd::aref_ts>(ats.ts);
    CHECK_EQ(arts->id, "dstm://Mnew_mkey/H1/R1/A1");
  }

  TEST_CASE("stm/context_replace_model_key_function") {
    // Main case:
    time_series::dd::apoint_ts ts1("dstm://Momk/H2/R3/A14");
    CHECK_EQ(replace_model_key_in_id(ts1, "new_mkey"), true);
    auto rts = time_series::dd::ts_as<time_series::dd::aref_ts>(ts1.ts);
    CHECK_EQ(rts->id, "dstm://Mnew_mkey/H2/R3/A14");

    // Non-dstm ID
    time_series::dd::apoint_ts ts2("shyft://test/not_a_dstm_series");
    CHECK_EQ(replace_model_key_in_id(ts2, "new_mkey"), false);

    // Non-ref ts:
    time_series::dd::apoint_ts ts3(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    CHECK_EQ(replace_model_key_in_id(ts3, "new_mkey"), false);
  }

  TEST_CASE("stm/context_rebind_expression") {
    // Case 1: All nodes needs rebind:
    time_series::dd::apoint_ts ts1("dstm://Momk/H1/R1/A14");
    time_series::dd::apoint_ts ts2("dstm://Momk/H1/R2/A14");
    auto ts3 = ts1 + ts2;
    CHECK_EQ(rebind_ts(ts3, "new_mkey"), true);
    CHECK_EQ(ts1.id(), "dstm://Mnew_mkey/H1/R1/A14");
    CHECK_EQ(ts2.id(), "dstm://Mnew_mkey/H1/R2/A14");

    // Case 2: Not every node needs rebind:
    time_series::dd::apoint_ts ts4("dstm://Momk/H1/R1/A3");
    time_series::dd::apoint_ts ts5("shyft://test/no_rebind");
    auto ts6 = ts4 * ts5;
    CHECK_EQ(rebind_ts(ts6, "new_mkey"), true);
    CHECK_EQ(ts4.id(), "dstm://Mnew_mkey/H1/R1/A3");
    CHECK_EQ(ts5.id(), "shyft://test/no_rebind");

    // Case 3: None of the nodes needs rebind:
    time_series::dd::apoint_ts ts7("shyft://test/no_rebind2");
    auto ts8 = ts7 - ts5;
    CHECK_EQ(rebind_ts(ts8, "new_mkey"), false);

    // Case 4: A single ts:
    time_series::dd::apoint_ts ts9("dstm://Momk/H1");
    CHECK_EQ(rebind_ts(ts9, "new_mkey"), true);
    CHECK_EQ(ts9.id(), "dstm://Mnew_mkey/H1");

    // Case 5: A single ts without rebind:
    time_series::dd::apoint_ts ts10("shyft://test/no_rebind3");
    CHECK_EQ(rebind_ts(ts10, "new_mkey"), false);
  }

  TEST_CASE("stm/context_rebind_stm_system") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    // Let's add a market to it
    auto mkt = std::make_shared<energy_market_area>(1, "test-market", "", mdl);
    // In ids:
    mkt->price = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    mkt->load = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
    mkt->tsm["price-alternative-1"] = time_series::dd::apoint_ts(
      time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 1.1);
    mkt->tsm["price-alternative-2"] = time_series::dd::apoint_ts(
      time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 1.2);
    // In rds:
    mkt->buy = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);

    mdl->market.push_back(mkt);

    // Case 1: Nothing to rebind
    CHECK_EQ(false, rebind_ts(*mdl, "new_mkey"));

    // Case 2: Rebind for market:
    mkt->sale = time_series::dd::apoint_ts("dstm://Momk/M1/A5");
    mkt->max_buy = 4.0 + time_series::dd::apoint_ts("dstm://Momk/M1/A2");
    mkt->tsm["price-alternative-2"] = 3.0 * time_series::dd::apoint_ts("dstm://Momk/M1/A12");
    CHECK_EQ(true, rebind_ts(*mdl, "new_mkey"));
    CHECK_EQ(mkt->sale.id(), "dstm://Mnew_mkey/M1/A5");
    // expressions
    CHECK_EQ(mkt->max_buy.stringify().contains("dstm://Mnew_mkey/M1/A2"), true);
    CHECK_EQ(mkt->tsm["price-alternative-2"].stringify().contains("dstm://Mnew_mkey/M1/A12"), true);
    mkt->sale = time_series::dd::apoint_ts();
    mkt->max_buy = time_series::dd::apoint_ts();
    mkt->tsm["price-alternative-2"] = time_series::dd::apoint_ts();
    CHECK_EQ(false, rebind_ts(*mdl, "new_mkey"));

    // Case 3: Rebind for hps:
    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    rsv->level.regulation_max = time_series::dd::apoint_ts("dstm://Momk/H1/R1/A1");
    CHECK_EQ(true, rebind_ts(*mdl, "new_mkey"));
    CHECK_EQ(rsv->level.regulation_max.id(), "dstm://Mnew_mkey/H1/R1/A1");
  }

  TEST_CASE("stm/context_set_get") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    auto mkt = mdl->market[0];
    mkt->price = time_series::dd::apoint_ts("shyft://test/old_id");
    boost::asio::thread_pool execution_context;
    auto exec = execution_context.executor();
    auto models = make_shared_models(
      exec,
      {
        {"old_mdl", mdl}
    });

    SUBCASE("get_clones") {
      auto from_get = models.get("old_mdl").get();
      REQUIRE(from_get.get() != mdl.get());
      REQUIRE(from_get->market[0]->price.id() == "shyft://test/old_id");
      mkt->price = time_series::dd::apoint_ts("shyft://test/new_id");
      REQUIRE(from_get->market[0]->price.id() == "shyft://test/old_id");
    }

    SUBCASE("set_sets") {
      auto mdl_2 = test::create_simple_system(1, "test-mdl");
      mdl_2->market[0]->price = time_series::dd::apoint_ts("shyft://test/new_id");
      REQUIRE(mdl_2 != mdl);
      models.set("old_mdl", mdl_2);
      auto from_get = models.get("old_mdl").get();
      REQUIRE(from_get->market[0]->price.id() == "shyft://test/new_id");
    }
  }

  TEST_CASE("stm/context_copy") {

    auto mdl = test::create_simple_system(1, "test-mdl");
    auto mkt = mdl->market[0];

    // In ids:
    mkt->price = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    mkt->load = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
    mkt->max_buy = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A2");
    // In rds:
    mkt->buy = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);
    mkt->sale = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A5");

    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    rsv->level.regulation_max = time_series::dd::apoint_ts("dstm://Mold_mdl/H1/R1/A1");

    boost::asio::thread_pool execution_context;

    auto exec = execution_context.executor();
    auto models = make_shared_models(
      exec,
      {
        {"old_mdl", mdl}
    });

    CHECK_EQ(models.container.size(), 1);
    CHECK_NOTHROW(models.copy("old_mdl", "new_mdl", false).get());
    CHECK_EQ(models.container.size(), 2);

    auto nsmdl = models.find("new_mdl").get();
    REQUIRE(nsmdl);
    auto nmdl = nsmdl->model.load();
    REQUIRE(nmdl);
    auto info = nsmdl->info.load();
    REQUIRE(info);
    hps = nmdl->hps[0];
    rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    CHECK_EQ(rsv->level.regulation_max.id(), "dstm://Mnew_mdl/H1/R1/A1");
    mkt = nmdl->market[0];
    CHECK_EQ(mkt->max_buy.id(), "dstm://Mnew_mdl/M1/A2");
    CHECK_EQ(mkt->sale.id(), "dstm://Mnew_mdl/M1/A5");

    CHECK_NOTHROW(models.copy("old_mdl", "new_mdl_str", true).get());
    CHECK_EQ(models.container.size(), 3);
    auto nsmdl_str = models.find("new_mdl_str").get();
    REQUIRE(nsmdl_str);
    auto nmdl_str = nsmdl_str->model.load();
    REQUIRE(nmdl_str);
    auto info_str = nsmdl_str->info.load();
    REQUIRE(info_str);
    hps = nmdl_str->hps[0];
    rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    CHECK_EQ(rsv->level.regulation_max.ts, nullptr);
    mkt = nmdl_str->market[0];
    CHECK_EQ(mkt->max_buy.ts, nullptr);
    CHECK_EQ(mkt->sale.ts, nullptr);
  }

  TEST_CASE("stm/context_remove") {

    auto mdl = test::create_simple_system(1, "test-mdl");
    auto mkt = mdl->market[0];

    // In ids:
    mkt->price = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    mkt->load = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
    mkt->max_buy = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A2");
    // In rds:
    mkt->buy = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);
    mkt->sale = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A5");

    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    rsv->level.regulation_max = time_series::dd::apoint_ts("dstm://Mold_mdl/H1/R1/A1");

    boost::asio::thread_pool execution_context;
    auto exec = execution_context.executor();

    auto models = make_shared_models(
      exec,
      {
        {"old_mdl", mdl}
    });

    CHECK(models.container.size() == 1);

    auto nsmdl = models.find("old_mdl").get();
    REQUIRE(nsmdl);

    REQUIRE(models.remove("old_mdl").get());
    CHECK(models.container.size() == 0);
    CHECK(models.find("old_mdl").get() == nullptr);
  }

  TEST_CASE("stm/context_multi_access") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    auto mdl_2 = test::create_simple_system(2, "test-mdl_2");
    boost::asio::thread_pool execution_context;
    std::string model_id_1 = "mdl_1";
    std::string model_id_2 = "mdl_2";

    std::mutex ordering;
    int for_check = 1;
    auto exec = execution_context.executor();
    auto models = make_shared_models(
      exec,
      {
        {model_id_1,   mdl},
        {model_id_2, mdl_2}
    });
    auto shared_1 = models.find(model_id_1).get();
    auto shared_2 = models.find(model_id_2).get();
    auto t1 = std::thread([&] {
      shared_1
        ->mutate([&](auto &&view) {
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
          {
            std::lock_guard g(ordering);
            for_check += 1;
          }
        })
        .get();
    });
    auto t2 = std::thread([&] {
      shared_2
        ->mutate([&](auto &&view) {
          {
            std::lock_guard g(ordering);
            for_check *= 2;
          }
        })
        .get();
    });

    t1.join();
    t2.join();
    CHECK(for_check == 3);
  }

  TEST_CASE("stm/context_get_attrs") {
    auto mdl1 = test::create_simple_system(1, "test-mdl");
    auto mdl2 = test::create_simple_system(2, "test-mdl2");

    auto const id1 = "mdl";
    auto const id2 = "mdl2";
    auto execution_context = boost::asio::thread_pool{};
    auto models = make_shared_models(
      execution_context.executor(),
      {
        {id1, mdl1},
        {id2, mdl2}
    });

    // prepare known attrs to check against
    auto const test_bool = true;
    auto const test_double = 900.3;
    auto const test_time_axis = shyft::time_axis::generic_dt{
      std::vector<utctime>{
                           from_seconds(0), from_seconds(1), from_seconds(2), from_seconds(3), from_seconds(4), from_seconds(5)}
    };
    auto const test_apoint = time_series::dd::apoint_ts{test_time_axis, 1.1, shyft::time_series::POINT_INSTANT_VALUE};

    mdl1->run_params.head_opt = test_bool;
    mdl1->summary->grand_total = test_double;
    mdl2->market[0]->price = test_apoint;

    auto const urls = std::vector{
      stm::url_format("wrong_id", {}, "run_params.head_opt"),
      stm::url_format(id1, {}, "run_params.head_opt"),
      stm::url_format(id1, {stm::url_make_step<&stm::stm_system::summary>(mdl1->summary->id)}, "grand_total"),
      stm::url_format(id2, {stm::url_make_step<&stm::stm_system::market>(mdl2->market[0]->id)}, "price"),
      stm::url_format(id2, {stm::url_make_step<&stm::stm_system::contracts>(42)}, "seller")};

    auto const res = get_attrs(models, urls);
    REQUIRE(res.size() == 5);

    // Check incorrect model_id and incorrect contract id
    CHECK(std::get_if<url_resolve_error>(&res[0]) != nullptr);
    CHECK(std::get_if<url_resolve_error>(&res[4]) != nullptr);

    // Check that the rest is ok
    { // head_opt
      auto as_any = std::get_if<stm::any_attr>(&res[1]);
      REQUIRE(as_any != nullptr);
      auto as_bool = std::get_if<bool>(as_any);
      REQUIRE(as_bool != nullptr);
      CHECK_EQ(*as_bool, test_bool);
    }
    { // grand_total
      auto as_any = std::get_if<stm::any_attr>(&res[2]);
      REQUIRE(as_any != nullptr);
      auto as_double = std::get_if<double>(as_any);
      REQUIRE(as_double != nullptr);
      CHECK_EQ(*as_double, doctest::Approx(test_double));
    }
    { // price
      auto as_any = std::get_if<stm::any_attr>(&res[3]);
      REQUIRE(as_any != nullptr);
      auto as_apoint = std::get_if<time_series::dd::apoint_ts>(as_any);
      REQUIRE(as_apoint != nullptr);
      CHECK_EQ(*as_apoint, test_apoint);
    }
  }

  TEST_CASE("stm/context_get_attrs") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    auto hps = mdl->hps[0];
    auto u = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(1));
    auto const model_id = "mdl";

    // prepare known attrs to check against
    auto const test_bool = true;
    auto const test_double = 900.3;
    auto const test_time_axis = shyft::time_axis::generic_dt{
      std::vector<utctime>{
                           from_seconds(0), from_seconds(1), from_seconds(2), from_seconds(3), from_seconds(4), from_seconds(5)}
    };
    auto const test_apoint = time_series::dd::apoint_ts{test_time_axis, 1.1, shyft::time_series::POINT_INSTANT_VALUE};
    auto const test_txy = test::create_t_xy(12, std::vector<double>{1, 2});
    auto const test_txyz = test::create_t_xyz(1, std::vector<double>{3, 4, 5});
    auto const test_turbine_description = test::create_t_turbine_description(1, std::vector<double>{9, 10, 11});
    auto const ctr = std::make_shared<shyft::energy_market::stm::contract>(1, "ctr", "{}", mdl);
    auto const test_str = "SomeString";
    auto const test_unit_group_type = shyft::energy_market::stm::unit_group_type::afrr_up;
    auto const ug = mdl->add_unit_group(1, "ug1", "{}");

    u->best_profit.production = test_txy;
    u->pump_description = test_txyz;
    u->turbine_description = test_turbine_description;
    ctr->seller = test_str;
    ug->group_type = test_unit_group_type;

    mdl->run_params.head_opt = test_bool;
    mdl->summary->grand_total = test_double;
    mdl->market[0]->price = test_apoint;
    mdl->contracts.push_back(ctr);
    mdl->run_params.run_time_axis = test_time_axis;

    auto execution_context = boost::asio::thread_pool{};
    auto models = make_shared_models(
      execution_context.executor(),
      {
        {model_id, mdl}
    });

    SUBCASE("get_error_wrong_mid") {
      std::vector attr_urls{
        stm::url_format(fmt::format("{}wr", model_id), {}, "run_params.run_time_axis"),
      };
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      CHECK(std::get_if<stm::url_resolve_error>(&res[0]) != nullptr);
    }
    SUBCASE("get_error_illegal_path") {
      std::vector attr_urls{stm::url_format(
        model_id,
        {stm::url_make_step<&stm::stm_system::hps>(hps->id + 10), stm::url_make_step<&stm::stm_hps::units>(u->id)},
        "reserve.droop.cost")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      CHECK(std::get_if<stm::url_resolve_error>(&res[0]) != nullptr);
    }
    SUBCASE("get_error_illegal_attr") {
      std::vector attr_urls{stm::url_format(
        model_id,
        {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
        "reservex.droop.cost")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      CHECK(std::get_if<stm::url_resolve_error>(&res[0]) != nullptr);
    }
    SUBCASE("get_error_unrepresented_type") {
      // std::uint16_t is not part of stm::any_attr
      std::vector attr_urls{
        stm::url_format(model_id, {}, "run_params.n_inc_runs"),
      };
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      CHECK(std::get_if<stm::url_resolve_error>(&res[0]) != nullptr);
    }
    SUBCASE("get_bool") {
      std::vector attr_urls{
        stm::url_format(model_id, {}, "run_params.head_opt"),
      };
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_bool = std::get_if<bool>(as_any);
      REQUIRE(as_bool != nullptr);
      CHECK(*as_bool == test_bool);
    }
    SUBCASE("get_double") {
      std::vector attr_urls{
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::summary>(mdl->summary->id)}, "grand_total"),
      };
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_double = std::get_if<double>(as_any);
      REQUIRE(as_double != nullptr);
      CHECK(*as_double == doctest::Approx(test_double));
    }
    // there are no int64_t attrs
    // there are no uint64_t attrs
    SUBCASE("get_apoint") {
      auto m = mdl->market[0];
      std::vector attr_urls{
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::market>(m->id)}, "price"),
      };
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_apoint = std::get_if<time_series::dd::apoint_ts>(as_any);
      REQUIRE(as_apoint != nullptr);
      CHECK(*as_apoint == test_apoint);
      // unequal shared ptr
      CHECK(as_apoint->ts.get() != test_apoint.ts.get());
    }
    SUBCASE("get_t_xy") {
      std::vector attr_urls{stm::url_format(
        model_id,
        {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
        "best_profit.production")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_txy = std::get_if<t_xy_>(as_any);
      REQUIRE(as_txy != nullptr);
      CHECK(equal_attributes(*as_txy, test_txy));
      // unequal shared ptr
      CHECK(*as_txy != test_txy);
    }
    // there are no t_xyz_ attrs
    SUBCASE("get_txyz_list") {
      std::vector attr_urls{stm::url_format(
        model_id,
        {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
        "pump_description")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_txyz = std::get_if<t_xyz_list_>(as_any);
      REQUIRE(as_txyz != nullptr);
      CHECK(equal_attributes(*as_txyz, test_txyz));
      // unequal shared ptr
      CHECK(*as_txyz != test_txyz);
    }
    SUBCASE("get_turbine_description") {
      std::vector attr_urls{stm::url_format(
        model_id,
        {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u->id)},
        "turbine_description")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_turb = std::get_if<t_turbine_description_>(as_any);
      REQUIRE(as_turb != nullptr);
      CHECK(equal_attributes(*as_turb, test_turbine_description));
      // unequal shared ptr
      CHECK(*as_turb != test_turbine_description);
    }
    SUBCASE("get_string") {
      std::vector attr_urls{
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::contracts>(ctr->id)}, "seller")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_str = std::get_if<std::string>(as_any);
      REQUIRE(as_str != nullptr);
      CHECK(*as_str == test_str);
    }
    // there are no time_series::dd::ats_vector attrs
    SUBCASE("get_ugtype") {
      std::vector attr_urls{
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "group_type")};
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_unit_group_type = std::get_if<unit_group_type>(as_any);
      REQUIRE(as_unit_group_type != nullptr);
      CHECK(*as_unit_group_type == test_unit_group_type);
    }
    SUBCASE("get_time_axis") {
      std::vector attr_urls{
        stm::url_format(model_id, {}, "run_params.run_time_axis"),
      };
      auto const res = get_attrs(models, attr_urls);
      REQUIRE(res.size() == 1);
      auto as_any = std::get_if<stm::any_attr>(&res[0]);
      REQUIRE(as_any != nullptr);
      auto as_time_axis = std::get_if<time_axis::generic_dt>(as_any);
      REQUIRE(as_time_axis != nullptr);
      CHECK(*as_time_axis == test_time_axis);
    }
  }

  TEST_CASE("stm/context_get_ts") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    auto hps = mdl->hps[0];
    auto u = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(1));
    boost::asio::thread_pool execution_context;
    std::string model_id = "mdl";

    // prepare known attrs to check against
    bool test_bool = true;
    mdl->run_params.head_opt = test_bool;
    utctimespan dt{deltahours(1)};
    time_axis::fixed_dt ta{test::_t(0), dt, 6};
    time_series::dd::apoint_ts test_apoint{
      ta, std::vector<double>{1.0, 1.2, 1.3, 1.4, 1.5, 1.6},
       time_series::ts_point_fx::POINT_INSTANT_VALUE
    };
    mdl->market[0]->price = test_apoint;
    auto exec = execution_context.executor();
    auto models = make_shared_models(
      exec,
      {
        {model_id, mdl}
    });
    SUBCASE("gets_ts_ptr") {
      auto m = mdl->market[0];
      std::string attr_url = stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::market>(m->id)}, "price");
      auto ts_ptr = get_ts(models, attr_url);
      REQUIRE(ts_ptr);
      CHECK(ts_ptr->values() == test_apoint.ts->values());
    }
    SUBCASE("gets_nullptr_on_other") {
      auto m = mdl->market[0];
      std::string attr_url = stm::url_format(model_id, {}, "run_params.head_opt");
      auto ts_ptr = get_ts(models, attr_url);
      REQUIRE(ts_ptr == nullptr);
    }
    SUBCASE("gets_nullptr_on_empty") {
      auto m = mdl->market[0];
      std::string attr_url = stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::market>(m->id)}, "load");
      auto ts_ptr = get_ts(models, attr_url);
      REQUIRE(ts_ptr == nullptr);
    }
  }

  TEST_CASE("stm/context_set_attrs") {
    auto mdl1 = test::create_simple_system(1, "test-mdl");
    auto mdl2 = test::create_simple_system(2, "test-mdl2");

    auto const id1 = "mdl";
    auto const id2 = "mdl2";
    auto execution_context = boost::asio::thread_pool{};
    auto models = make_shared_models(
      execution_context.executor(),
      {
        {id1, mdl1},
        {id2, mdl2}
    });

    // prepare known attrs to check against
    auto const test_bool = true;
    auto const test_double = 900.3;
    auto const test_apoint = time_series::dd::apoint_ts{"test_series"};

    auto const m = mdl2->market[0];

    // clang-format off
    auto const attrs = std::vector<std::pair<std::string, stm::any_attr>>{
      {stm::url_format("wrong_id",                                                          {}, "run_params.head_opt"),   test_bool},
      {stm::url_format(id1,                                                                 {}, "run_params.head_opt"),   test_bool},
      {stm::url_format(id1, {stm::url_make_step<&stm::stm_system::summary>(mdl1->summary->id)},         "grand_total"), test_double},
      {stm::url_format(id2,              {stm::url_make_step<&stm::stm_system::market>(m->id)},               "price"), test_apoint},
      {stm::url_format(id2,              {stm::url_make_step<&stm::stm_system::contracts>(42)},              "seller"),     "actor"}
    };
    // clang-format on

    auto const res = set_attrs(models, attrs);
    REQUIRE(res.size() == 5);

    // Check incorrect model_id and incorrect contract id
    CHECK(res[0] != std::nullopt);
    CHECK(res[4] != std::nullopt);

    // Check that the rest is OK
    CHECK_EQ(res[1], std::nullopt);
    CHECK_EQ(res[2], std::nullopt);
    CHECK_EQ(res[3], std::nullopt);

    CHECK_EQ(mdl1->run_params.head_opt, test_bool);
    CHECK_EQ(mdl1->summary->grand_total, test_double);
    CHECK_EQ(m->price, test_apoint);
  }

  TEST_CASE("stm/context_causes_cycle") {
    std::string model_id = "mdl";
    auto mdl = test::create_simple_system(1, model_id);
    auto u = std::dynamic_pointer_cast<stm::unit>(mdl->hps[0]->find_unit_by_id(1));
    boost::asio::thread_pool execution_context;
    auto exec = execution_context.executor();
    auto models = make_shared_models(
      exec,
      {
        {model_id, mdl}
    });
    SUBCASE("no_cycle") {
      CHECK(!causes_cycle(models, "other_mdl_id", mdl));
    }
    SUBCASE("create_new_cycle_in_existing_model") {
      auto patch_mdl = std::make_shared<stm_system>(1, model_id, "");
      auto ug = patch_mdl->add_unit_group(1, "ug1", "{}");
      ug->delivery.schedule = apoint_ts(
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "delivery.result"));
      ug->delivery.result = apoint_ts(
        stm::url_format(model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "delivery.schedule"));
      CHECK(causes_cycle(models, model_id, patch_mdl));
    }
    SUBCASE("new_model_with_cycle") {
      auto const new_model_id = "other_model_id";
      auto new_mdl = std::make_shared<stm_system>(2, new_model_id, "");
      auto ug = new_mdl->add_unit_group(1, "ug1", "{}");
      ug->delivery.schedule = apoint_ts(
        stm::url_format(new_model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "delivery.result"));
      ug->delivery.result = apoint_ts(stm::url_format(
        new_model_id, {stm::url_make_step<&stm::stm_system::unit_groups>(ug->id)}, "delivery.schedule"));
      CHECK(causes_cycle(models, new_model_id, new_mdl));
    }
  }

  TEST_SUITE_END;
}
