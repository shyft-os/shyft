#include <future>
#include <map>
#include <memory>

#include <boost/serialization/vector.hpp>
#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/urls.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/build_test_system.h>
#include <test/test_utils.h>

namespace shyft::energy_market::stm {

  using shyft::test::utils::temp_dir;

// simple utility to make ts url for one time-series
#define mk_ts_url(prefix, o, attr) proxy_attr(o, #attr, o.attr).url(prefix)

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/srv_basics") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server s;
    FAST_CHECK_EQ(s.dispatch.state->shared_lock_timeout, std::chrono::milliseconds{200});
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{core::srv_connection{host_port}};

      // get version info
      auto [result] = c.get_version_info();
      CHECK_EQ(result, get_version_info(s).version);
      // get model ids
      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      // create model server-side
      CHECK(c.create_model({"m1"}));
      CHECK(!c.create_model({"m1"}));
      CHECK(s.dispatch.state->models.container.size() == 1);

      // get model ids
      mids = c.get_model_ids();
      REQUIRE_EQ(mids.size(), 1);
      CHECK_EQ(mids[0], "m1");

      // Get model infos
      auto [mifs] = c.get_model_infos();
      CHECK_EQ(mifs.size(), 1);
      auto mif = mifs["m1"];
      CHECK_EQ(mif.id, 0);
      CHECK_EQ(mif.name, "");
      CHECK_EQ(mif.json, "");

      // rename model
      CHECK(!c.rename_model("m2", "m0"));
      CHECK(!c.rename_model("m1", "m1"));
      CHECK(c.rename_model("m1", "m0"));
      mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 1);
      CHECK_EQ(mids[0], "m0");

      // get model
      CHECK_THROWS_AS(c.get_model({"m2"}), std::runtime_error); // attempting get non-existent model
      CHECK_THROWS_AS(c.get_model({"m1"}), std::runtime_error); // attempting get non-existent model
      auto mdl = c.get_model({"m0"});

      // add model created client-side
      auto mdl2 = std::make_shared<stm::stm_system>();
      {
        mdl2->market.push_back(std::make_shared<stm::energy_market_area>(1, "test market", "", mdl2));
        auto hps = std::make_shared<stm::stm_hps>(1, "test hps");
        stm_hps_builder builder(hps);
        auto rsv = builder.create_reservoir(2, "test rsv", "");
        auto unit = builder.create_unit(3, "test unit", "");
        std::string large_json(100000, 'a');
        auto plant = builder.create_power_plant(4, "test plant", large_json);
        apoint_ts large_ts(
          generic_dt(core::from_seconds(0), core::from_seconds(1), 100000),
          std::vector<double>(100000, 1.0),
          time_series::POINT_AVERAGE_VALUE);
        plant->production.realised = large_ts; // just to make stripped archive really smaller
        auto tun = builder.create_tunnel(5, "rsv-unit", "");
        power_plant::add_unit(plant, unit);
        hydro_power::connect(tun).input_from(rsv).output_to(unit);
        mdl2->hps.push_back(hps);
      }
      CHECK_EQ(c.add_model({"m1", mdl2}).status, true);
      CHECK_EQ(s.dispatch.state->models.container.size(), 2);

      CHECK_EQ(c.set_model({"m1", mdl2}).status, true);
      CHECK_EQ(c.set_model({"mxxx1", mdl2}).status, false);

      // verify model stripped
      auto mdl_strip = c.get_model_stripped({"m1"}).model;
      REQUIRE(mdl_strip != nullptr);
      REQUIRE(mdl2->hps.size() > 0);
      REQUIRE(mdl_strip->hps.size() > 0);

      CHECK_EQ(mdl2->hps.size(), mdl_strip->hps.size());
      CHECK_EQ(mdl_strip->hps[0]->equal_structure(*mdl2->hps[0]), true);
      auto mdl2_blob = stm_system::to_blob(mdl2);
      auto mdl_strip_blob = stm_system::to_blob(mdl_strip);
      CHECK_GT(mdl2_blob.size(), mdl_strip_blob.size());


      // remove model
      CHECK(!c.remove_model({"m2"}).status);
      CHECK(c.remove_model({"m0"}).status);
      mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 1);

      // clone model
      CHECK(!c.clone_model({"m0", "m3"}).status);
      CHECK(!c.clone_model({"m1", "m1"}).status);
      CHECK(c.clone_model({"m1", "m1c"}).status);
      mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 2);
      auto [mdl1] = c.get_model({"m1"});
      auto [mdl1c] = c.get_model({"m1c"});
      CHECK_EQ(mdl1->hps.size(), 1);
      CHECK_EQ(mdl1c->hps.size(), 1);
      CHECK_UNARY(mdl1->hps.front()->equal_structure(*mdl1c->hps.front()));
      // check callback fx
      // (with none on the server side)
      CHECK(!c.fx({"m1", "optimize_this"}).status); // we should have false when no cb set.
      // then rig a callback
      size_t cb_count = 0;
      s.dispatch.state->callback = [&cb_count](std::string mid, std::string arg) {
        cb_count++;
        return mid == "m1" && arg == "optimize_this"; // verify we got the correct args.
      };
      CHECK(c.fx({"m1", "optimize_this"}).status); // we should have false when no cb set.
      CHECK_EQ(cb_count, 1);                       // really, it's done
      s.dispatch.state->callback = nullptr;        // ensure that lambda capture is zeroed out before terminating test.
      // Get state:
      CHECK_THROWS_AS(c.get_state({"m3"}), std::runtime_error); // attempting to get state of nonexistent model
      auto [state] = c.get_state({"m1"});
      CHECK_EQ(state, model_state::idle);
      // remove models
      CHECK(!c.remove_model({"m0"}).status);
      CHECK(c.remove_model({"m1"}).status);
      mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 1);
      CHECK(!c.remove_model({"m1"}).status);
      CHECK(c.remove_model({"m1c"}).status);
      mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0);

      c.close();
      s.clear();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
      s.clear();
    }
  }

  TEST_CASE("stress/stm/dstm") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here
    int const n_connects = 100;
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{core::srv_connection{host_port}};

      // get version info
      auto [result] = c.get_version_info();
      CHECK_EQ(result, get_version_info(s).version);

      // get model ids
      auto mids = c.get_model_ids();
      CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

      // create model server-side
      CHECK(c.create_model({"m1"}));
      CHECK(!c.create_model({"m1"}));
      CHECK_EQ(s.dispatch.state->models.container.size(), 1);
      for (size_t i = 0; i < n_connects; ++i) {
        mids = c.get_model_ids();
        CHECK_EQ(1, mids.size());
      }
      s.clear();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
      s.clear();
    }
  }

  TEST_CASE("stress/stm/dstm_evaluate") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server s;
    s.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_evaluate.test."};
    add_container(*s.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here
    int const n_connects = 20;
    try {
      auto host_port = fmt::format("localhost:{}", port_no);

      for (std::size_t i = 0; i < n_connects; ++i) {
        add_model(s, fmt::format("m{}", i), test::create_simple_system_with_dtss(*(s.dispatch.state->dtss)));
      }
      CHECK_EQ(s.dispatch.state->models.container.size(), n_connects);
      // Do several evaluates in paralell:
      std::vector<std::future<bool>> res;

      utcperiod p(0, 2400);
      for (std::size_t i = 0; i < n_connects; ++i) {
        res.emplace_back(std::async(std::launch::async, [port_no, &p, i]() -> bool {
          auto t = p.start;
          auto host_port = fmt::format("localhost:{}", port_no);
          srv::dstm::client c{core::srv_connection{host_port}};
          auto mid = fmt::format("m{}", i);
          c.evaluate_model({mid, p, false, false});
          auto [mdl] = c.get_model({mid});
          auto hps = mdl->hps[0];
          auto rsv = std::dynamic_pointer_cast<stm::reservoir>(hps->find_reservoir_by_id(1));
          CHECK_EQ(rsv->level.regulation_min(t), doctest::Approx(1.0));
          CHECK_EQ(rsv->level.regulation_max(t), doctest::Approx(2.0));
          CHECK_EQ(rsv->volume.static_max(t), doctest::Approx(3.0));
          CHECK_EQ(rsv->volume.result(t), doctest::Approx(2.5));

          auto u = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(1));
          CHECK_EQ(u->cost.start(t), doctest::Approx(4.0));
          CHECK_EQ(u->cost.stop(t), doctest::Approx(5.0));
          CHECK_EQ(u->production.result(t), doctest::Approx(20.0));

          auto pp = std::dynamic_pointer_cast<stm::power_plant>(hps->find_power_plant_by_id(2));
          CHECK_EQ(pp->mip(t), doctest::Approx(6.0));
          CHECK_EQ(pp->production.schedule(t), doctest::Approx(20.5));

          auto market = mdl->market[0];
          CHECK_EQ(market->price(t), doctest::Approx(7.0));
          CHECK_EQ(market->sale(t), doctest::Approx(8.0));
          CHECK_EQ(market->load(t), doctest::Approx(164.0));
          return true;
        }));
      }
      CHECK_EQ(res.size(), n_connects);

      // Check that each thread exits successfully:
      for (std::size_t i = 0; i < n_connects; ++i) {
        CHECK(res[i].get());
      }
      s.clear();
    } catch (std::exception const &ex) {
      DOCTEST_MESSAGE(ex.what());
      CHECK_EQ(true, false);
      s.clear();
    }
  }

  TEST_CASE("stm/dstm_get_set_ts") {
    dlib::set_all_logging_levels(dlib::LNONE);
    using ta_t = shyft::time_axis::generic_dt;

    srv::dstm::server s;
    s.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_get_set_ts.test."};
    add_container(*s.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0);
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{core::srv_connection{host_port}};
      std::string mid{"m1"};
      auto mdl_prefix = "dstm://M" + mid;
      utcperiod p(0, 2400);
      auto stm_sys = test::create_simple_system_with_dtss(*(s.dispatch.state->dtss));
      // we want to create an expression with dstm attr. reference here
      auto hpsx = stm_sys->hps[0];
      auto ppx = std::dynamic_pointer_cast<stm::power_plant>(hpsx->find_power_plant_by_id(2));
      time_series::dd::apoint_ts pp_production_realised{
        ta_t{core::from_seconds(0), core::from_seconds(10), 5},
        {1.0, 2.0, 3.0, 4.0, 5.0},
        time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      ppx->production.realised = pp_production_realised;
      ppx->production.result = time_series::dd::apoint_ts(mk_ts_url(mdl_prefix, ppx->production, realised)) * 10.0; //
      add_model(s, mid, stm_sys);
      c.evaluate_model({mid, p, false, false});
      auto [mdl] = c.get_model({mid});
      auto hps = mdl->hps[0];
      auto rsv = std::dynamic_pointer_cast<stm::reservoir>(hps->find_reservoir_by_id(1));
      auto u = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(1));
      auto pp = std::dynamic_pointer_cast<stm::power_plant>(hps->find_power_plant_by_id(2));
      auto market = mdl->market[0];

      std::vector<std::string> ts_urls;

      ts_urls.push_back(mk_ts_url(mdl_prefix, rsv->volume, result));
      ts_urls.push_back(mk_ts_url(mdl_prefix, u->production, result));
      ts_urls.push_back(mk_ts_url(mdl_prefix, (*market), sale));
      ts_urls.push_back(mk_ts_url(mdl_prefix, pp->production, result));

      auto [rts] = c.get_ts({mid, ts_urls});

      CHECK_EQ(rts.size(), ts_urls.size());
      CHECK_EQ(rts[0], rsv->volume.result);
      CHECK_EQ(rts[1], u->production.result);
      CHECK_EQ(rts[2], market->sale);
      CHECK_EQ(rts[3], pp_production_realised * 10.0);

      auto all_result_ts_urls = [&] {
        auto urls = url_planning_inputs(mid, *mdl);
        std::erase_if(urls, [&](std::string_view url) {
          return !url_check<time_series::dd::apoint_ts>(*mdl, url);
        });
        return urls;
      }();
      rts = c.get_ts({mid, all_result_ts_urls}).time_series;
      CHECK(rts.size() > 0);

      // lets empty one of the series, so that we subscribe on an empty ts.
      std::vector<std::string> empty_ts_urls;
      empty_ts_urls.push_back(ts_urls[0]);
      time_series::dd::ats_vector empty_ts;
      empty_ts.push_back(time_series::dd::apoint_ts(ts_urls[0]));
      c.set_ts({mid, empty_ts});
      rts = c.get_ts({mid, ts_urls}).time_series;
      CHECK(!rts[0].ts);
      CHECK_EQ(rts[0].id(), "");

      auto subs = s.dispatch.state->dtss->sm->add_subscriptions(ts_urls);

      auto sum_subs = 0;
      for (auto const &sub : subs)
        sum_subs += sub->v;
      time_series::dd::ats_vector sts;
      ta_t ta{core::from_seconds(0), core::from_seconds(10), 5};
      time_series::dd::apoint_ts rsv_volume_result{
        ta, {1.0, 2.0, 3.0, 4.0, 5.0},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      time_series::dd::apoint_ts u_production_result{
        ta, {1.0, 2.2, 3.0, 2.0, 5.0},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      time_series::dd::apoint_ts market_sale{
        ta, {1.1, 2.0, 3.3, 4.0, 5.0},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      time_series::dd::apoint_ts pp_production_realised2{
        ta, {0.1, 0.2, 0.3, 0.4, 0.5},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      sts.push_back(time_series::dd::apoint_ts{ts_urls[0], rsv_volume_result});
      sts.push_back(time_series::dd::apoint_ts{ts_urls[1], u_production_result});
      sts.push_back(time_series::dd::apoint_ts{ts_urls[2], market_sale});
      sts.push_back(
        time_series::dd::apoint_ts{mk_ts_url(mdl_prefix, pp->production, realised), pp_production_realised2});
      c.set_ts({mid, sts});
      // prove two things:
      // (1) values in set_ts is updated
      // (2) that notification/subscription was updated so that web-ui get the changes
      // (3) that the other derived expressions on the model is re-evaluated, .e.g. pp.production.result = 10*.realised
      //     .. as discovered by Roar Emaus, this is really needed and wanted feature, since
      //     .. subscription over web-api would render correct values in these cases, so the model it self should.
      rts = c.get_ts({mid, ts_urls}).time_series;
      CHECK_EQ(rts.size(), ts_urls.size());
      CHECK_EQ(rts[0], rsv_volume_result);
      // set fails, because production result is an expression, and it is
      // currently not allowed to write to it..
      CHECK_NE(rts[1], u_production_result);
      CHECK_EQ(rts[2], market_sale);
      CHECK_EQ(rts[3], (pp_production_realised2 * 10.0));
      auto sum_subs2 = 0;
      for (auto const &sub : subs)
        sum_subs2 += sub->v;
      CHECK_EQ(sum_subs2 - sum_subs, 3);
      s.dispatch.state->dtss->sm->remove_subscriptions(subs);
      subs.clear();

      c.close();
      s.clear();
    } catch (std::exception const &ex) {
      FAIL(std::string{ex.what()});
      s.clear();
    }
  }

  TEST_CASE("stm/dstm_ts_internal_expression_w_subs") {
    dlib::set_all_logging_levels(dlib::LNONE);
    using ta_t = shyft::time_axis::generic_dt;

    srv::dstm::server s;
    s.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_get_set_ts.subtest."};
    add_container(*s.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{core::srv_connection{host_port}};
      std::string mid{"m1"};
      auto mdl_prefix = fmt::format("dstm://M{}", mid);
      utcperiod p(0, 2400);
      auto mdl = test::create_stm_system();
      auto hps = mdl->hps[0];
      std::string u2_prod_url{"shyft://test/u2.production"};
      do_store_ts(
        *s.dispatch.state->dtss,
        {
          time_series::dd::apoint_ts{
                                     u2_prod_url, time_series::dd::apoint_ts{
              ta_t{core::from_seconds(0), core::from_seconds(10), 3},
              {1.0, 2.0, 3.0},
              time_series::ts_point_fx::POINT_AVERAGE_VALUE}}
      },
        shyft::dtss::store_policy{.recreate = true, .strict = true, .cache = true});
      //
      // We want to study pp.production.result = u1.production.result + u2.production.result
      //
      auto u1 = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(51));
      auto u2 = std::dynamic_pointer_cast<stm::unit>(hps->find_unit_by_id(52));
      auto pp = std::dynamic_pointer_cast<stm::power_plant>(u1->pwr_station_());
      pp->production.result = time_series::dd::apoint_ts(mk_ts_url(mdl_prefix, u1->production, result))
                            + time_series::dd::apoint_ts(mk_ts_url(mdl_prefix, u2->production, result));
      u1->production.result = time_series::dd::apoint_ts(
        ta_t{core::from_seconds(0), core::from_seconds(10), 3}, 3.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
      u2->production.result = time_series::dd::apoint_ts(u2_prod_url) * 1e6;
      add_model(s, mid, mdl);
      //
      web_api::energy_market::json request;
      request["model_key"] = mid;
      std::string request_id{"1"};
      auto bg_callback = [](web_api::energy_market::json const &) {
        return web_api::bg_work_result{};
      };
      stm::subscription::proxy_attr_observer pa_obs(
        s.dispatch.state->models, s.dispatch.state->sm, s.dispatch.state->dtss->sm, request_id, request, bg_callback);
      pa_obs.add_ts_subscription(mk_ts_url(mdl_prefix, pp->production, result), pp->production.result);

      FAST_CHECK_EQ(0, pa_obs.terminal_version());
      // act, store to dtss, via dstm, and the set.ts will notify the attribute.
      c.set_ts(
        {mid,
         {time_series::dd::apoint_ts{
           mk_ts_url(mdl_prefix, u1->production, result),
           time_series::dd::apoint_ts{
             ta_t{core::from_seconds(0), core::from_seconds(10), 3},
             {3.0, 2.0, 1.0},
             time_series::ts_point_fx::POINT_AVERAGE_VALUE}}}});
      // assert we got notify..
      FAST_CHECK_EQ(1, pa_obs.terminal_version());
    } catch (std::runtime_error const &e) {
      FAIL("Got exception:" << std::string(e.what()));
    }
  }

  TEST_CASE("stm/dstm_evaluate_ts") {
    dlib::set_all_logging_levels(dlib::LNONE);
    using time_series::ts_point_fx;

    srv::dstm::server server;
    server.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_evaluate_ts.test."};
    add_container(*server.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = server.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    core::utcperiod bind_period{core::from_seconds(0), core::from_seconds(10)};
    time_axis::generic_dt time_axis{core::from_seconds(0), core::from_seconds(10), 3};
    std::string u2_prod_url{"shyft://test/u2.production"};
    std::string bound_url{
      "shyft://test/u2.production"}; // not anymore: .no_lookup"}; // to ensure we cover case bound ts, should not end
                                     //  up in lookup(will fail if attempted)
    std::string failing_ts_name("unkown_url");
    std::string failing_url(fmt::format("shyft://test/{}", failing_ts_name));
    do_store_ts(
      *server.dispatch.state->dtss,
      {
        time_series::dd::apoint_ts{
                                   u2_prod_url, time_series::dd::apoint_ts{time_axis, {1.0, 2.0, 3.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE}}
    },
      shyft::dtss::store_policy{.recreate = true, .strict = true, .cache = true});

    constexpr auto model_id0 = "m1", model_id1 = "m2";
    auto model_prefix0 = fmt::format("dstm://M{}", model_id0), model_prefix1 = fmt::format("dstm://M{}", model_id1);

    auto model0 = test::create_stm_system();
    auto hps0 = model0->hps[0];

    auto g0 = dynamic_cast<stm::gate *>(hps0->find_gate_by_id(1).get());
    g0->discharge.schedule = time_series::dd::apoint_ts(time_axis, 3.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);

    auto u01 = dynamic_cast<stm::unit *>(hps0->find_unit_by_id(51).get());
    auto u02 = dynamic_cast<stm::unit *>(hps0->find_unit_by_id(52).get());
    auto pp0 = dynamic_cast<stm::power_plant *>(u01->pwr_station_().get());

    pp0->production.result =
      time_series::dd::apoint_ts(mk_ts_url(model_prefix0, u01->production, result))
      + time_series::dd::apoint_ts(mk_ts_url(model_prefix0, u02->production, result)) / 2.0
      + time_series::dd::apoint_ts(mk_ts_url(model_prefix0, u02->production, result))
          / 2.0; // just to ref same url twice, to cover that specific case.
    u01->production.result = time_series::dd::apoint_ts(time_axis, 3.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    u02->production.result = time_series::dd::apoint_ts(u2_prod_url) * 1e6
                           + time_series::dd::apoint_ts{bound_url, u01->production.result}; // use a ready bound ts

    add_model(server, model_id0, model0);
    clone_model(server, model_id0, model_id1);

    auto model1 = server.dispatch.state->models.find(model_id1).get();
    REQUIRE(model1);
    auto hps1 = model1->model.load()->hps[0];

    auto u11 = dynamic_cast<stm::unit *>(hps1->find_unit_by_id(51).get());
    auto u12 = dynamic_cast<stm::unit *>(hps1->find_unit_by_id(52).get());
    auto pp1 = dynamic_cast<stm::power_plant *>(u11->pwr_station_().get());

    SUBCASE("single_client") {
      try {
        srv::dstm::client client{core::srv_connection{fmt::format("localhost:{}", port_no)}};
        std::vector<std::variant<time_series::dd::apoint_ts, evaluate_ts_error>> result;
        {
          CHECK_NOTHROW(result = client.evaluate_ts({{}, bind_period}).time_series);
          CHECK(result.size() == 0);
        }
        {
          CHECK_THROWS(client.evaluate_ts({{}, {}})); // NOTE: invalid period - jeh
        }
        {
          auto req0 = u01->production.result.clone_expr();
          CHECK_NOTHROW(result = client.evaluate_ts({{req0}, bind_period}).time_series);
          CHECK(result.size() == 1);
        }
        {
          auto req0 = u02->production.result.clone_expr();
          CHECK_NOTHROW(result = client.evaluate_ts({{req0}, bind_period}).time_series);
          CHECK(result.size() == 1);
        }
        {
          auto req0 = u02->production.result.clone_expr();
          auto req1 = u11->production.result.clone_expr();
          CHECK_NOTHROW(
            result = client
                       .evaluate_ts({
                         {req0, req1},
                         bind_period
          })
                       .time_series);
          CHECK(result.size() == 2);
          CHECK(std::ranges::all_of(result, [](auto const &v) {
            return std::holds_alternative<time_series::dd::apoint_ts>(v);
          }));
        }
        {
          auto req0 = u01->production.result.clone_expr();
          auto req1 = u02->production.result.clone_expr();
          auto req2 = pp1->production.result.clone_expr();
          CHECK_NOTHROW(
            result = client
                       .evaluate_ts({
                         {req0, req1, req2},
                         bind_period
          })
                       .time_series);
          CHECK(result.size() == 3);
          CHECK(std::ranges::all_of(result, [](auto const &v) {
            return std::holds_alternative<time_series::dd::apoint_ts>(v);
          }));
        }
        {
          auto req0 = pp0->production.result.clone_expr();
          CHECK_NOTHROW(result = client.evaluate_ts({{req0}, bind_period}).time_series);
          CHECK(result.size() == 1);
          CHECK(std::ranges::all_of(result, [](auto const &v) {
            return std::holds_alternative<time_series::dd::apoint_ts>(v);
          }));
        }
        {
          auto req0 = time_series::dd::apoint_ts(mk_ts_url(model_prefix0, u02->production, result))
                    + time_series::dd::apoint_ts(mk_ts_url(model_prefix1, u12->production, result));
          CHECK_NOTHROW(result = client.evaluate_ts({{req0}, bind_period}).time_series);
          CHECK(result.size() == 1);
          CHECK(std::ranges::all_of(result, [](auto const &v) {
            return std::holds_alternative<time_series::dd::apoint_ts>(v);
          }));
        }
        {
          auto req0 = time_series::dd::apoint_ts("shyft://test/unknown_url");
          CHECK_NOTHROW(result = client.evaluate_ts({{req0}, bind_period}).time_series);
          CHECK(result.size() == 1);
          auto err = std::get_if<evaluate_ts_error>(&result[0]);
          REQUIRE(err != nullptr);
          CHECK(err->what.contains("shyft://test/unknown_url did not resolve"));
        }
        {
          auto req0 = time_series::dd::apoint_ts("shyft://test/unknown_url");
          auto req1 = time_series::dd::apoint_ts(mk_ts_url(model_prefix0, u02->production, result));
          CHECK_NOTHROW(
            result = client
                       .evaluate_ts({
                         {req0, req1},
                         bind_period
          })
                       .time_series);
          CHECK(result.size() == 2);
          auto err = std::get_if<evaluate_ts_error>(&result[0]);
          REQUIRE(err != nullptr);
          CHECK(err->what.contains("shyft://test/unknown_url did not resolve"));
          auto ts = std::get_if<time_series::dd::apoint_ts>(&result[1]);
          REQUIRE(ts != nullptr);
        }
      } catch (std::runtime_error const &e) {
        FAIL(fmt::format("Unexpected exception: {}", e.what()));
      }
    }
  }

  TEST_CASE("stm/dstm_get_attrs") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server server;
    server.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_get_attrs.test."};
    add_container(*server.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = server.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    core::utcperiod bind_period{core::from_seconds(0), core::from_seconds(10)};
    time_axis::generic_dt time_axis{core::from_seconds(0), core::from_seconds(10), 3};
    std::string u2_prod_url{"shyft://test/u2.production"};
    do_store_ts(
      *server.dispatch.state->dtss,
      {
        time_series::dd::apoint_ts{
                                   u2_prod_url, time_series::dd::apoint_ts{time_axis, {1.0, 2.0, 3.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE}}
    },
      shyft::dtss::store_policy{.recreate = true, .strict = true, .cache = true});

    constexpr auto model_id = "m1";
    auto model = test::create_stm_system();
    auto hps = model->hps[0];

    auto g = dynamic_cast<stm::gate *>(hps->find_gate_by_id(1).get());
    g->discharge.schedule = time_series::dd::apoint_ts(time_axis, 3.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);

    auto u1 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(51).get());
    auto u2 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(52).get());

    time_axis::generic_dt ta(
      std::vector<utctime>{
        core::from_seconds(0),
        core::from_seconds(1),
        core::from_seconds(2),
        core::from_seconds(3),
        core::from_seconds(4),
        core::from_seconds(5)});
    model->run_params.run_time_axis = ta;

    add_model(server, model_id, model);


    srv::dstm::client client{core::srv_connection{fmt::format("localhost:{}", port_no)}};
    {
      auto [attrs] = client.get_attrs({});
      CHECK(attrs.empty());
    }
    {
      std::vector attr_urls{
        stm::url_format(model_id, {}, "run_params.run_time_axis"),
      };
      auto [attrs] = client.get_attrs({attr_urls});
      REQUIRE(attrs.size() == 1);
      REQUIRE(std::holds_alternative<any_attr>(attrs[0]));
      CHECK(std::holds_alternative<time_axis::generic_dt>(std::get<any_attr>(attrs[0])));
      CHECK(std::get<time_axis::generic_dt>(std::get<any_attr>(attrs[0])) == ta);
    }
    {
      std::vector attr_urls{
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u1->id)},
          "reserve.droop.cost"),
        stm::url_format(
          model_id,
          {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u2->id)},
          "discharge.constraint.max_from_downstream_level")};
      auto [attrs] = client.get_attrs({attr_urls});
      REQUIRE(attrs.size() == 2);
      REQUIRE(std::ranges::all_of(attrs, [](auto const &a) {
        return std::holds_alternative<any_attr>(a);
      }));
      CHECK(std::holds_alternative<time_series::dd::apoint_ts>(std::get<any_attr>(attrs[0])));
      CHECK(std::holds_alternative<stm::t_xy_>(std::get<any_attr>(attrs[1])));
    }
    {
      std::vector attr_urls{stm::url_format(
        model_id,
        {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u1->id)},
        "resexxxrve.droop.cost")};
      auto [attrs] = client.get_attrs({attr_urls});
      REQUIRE(attrs.size() == 1);
      auto as_err = std::get_if<stm::url_resolve_error>(&attrs[0]);
      CHECK(as_err != nullptr);
      CHECK(
        as_err->what
        == "attribute resexxxrve.droop.cost not found, requested url: dstm://Mm1/H1/U51.resexxxrve.droop.cost");
    }
    {
      std::vector attr_urls{stm::url_format(
        fmt::format("{}xxx", model_id),
        {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u1->id)},
        "resexxxrve.droop.cost")};
      auto [attrs] = client.get_attrs({attr_urls});
      REQUIRE(attrs.size() == 1);
      auto as_err = std::get_if<stm::url_resolve_error>(&attrs[0]);
      CHECK(as_err != nullptr);
      CHECK(as_err->what == "model with id m1xxx not found, requested url: dstm://Mm1xxx/H1/U51.resexxxrve.droop.cost");
    }
    {
      std::vector attr_urls{std::string("bluuurb")};
      auto [attrs] = client.get_attrs({attr_urls});
      REQUIRE(attrs.size() == 1);
      auto as_err = std::get_if<stm::url_resolve_error>(&attrs[0]);
      CHECK(as_err != nullptr);
      CHECK(as_err->what == "url bluuurb is not parseable: url bluuurb starts neither with dstm schema (dstm://) or dtss schema (shyft://)");
    }
  }

  TEST_CASE("stm/dstm_set_attrs") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server server;
    server.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_set_attrs.test."};
    add_container(*server.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = server.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    core::utcperiod bind_period{core::from_seconds(0), core::from_seconds(10)};
    time_axis::generic_dt time_axis{core::from_seconds(0), core::from_seconds(10), 3};
    std::string u2_prod_url{"shyft://test/u2.production"};
    do_store_ts(
      *server.dispatch.state->dtss,
      {
        time_series::dd::apoint_ts{
                                   u2_prod_url, time_series::dd::apoint_ts{time_axis, {1.0, 2.0, 3.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE}}
    },
      shyft::dtss::store_policy{.recreate = true, .strict = true, .cache = true});

    constexpr auto model_id = "m1";
    auto model = test::create_stm_system();
    auto hps = model->hps[0];

    auto g = dynamic_cast<stm::gate *>(hps->find_gate_by_id(1).get());
    g->discharge.schedule = time_series::dd::apoint_ts(time_axis, 3.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);

    auto u1 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(51).get());
    auto u2 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(52).get());

    add_model(server, model_id, model);

    srv::dstm::client client{core::srv_connection{fmt::format("localhost:{}", port_no)}};

    std::shared_ptr<hydro_power::turbine_description> turbine_description(
      new hydro_power::turbine_description{.operating_zones{
        {.efficiency_curves{
           {.xy_curve{.points{{.x = 1.0, .y = 0.9}}}, .z = 3.0}, {.xy_curve{.points{{.x = 2.0, .y = 0.8}}}, .z = 5.0}},
         .production_min{},
         .production_max{},
         .production_nominal{},
         .fcr_min = 0.0,
         .fcr_max = 0.0}}});
    stm::t_turbine_description_ turbine_description_attr(
      new std::map<core::utctime, std::shared_ptr<hydro_power::turbine_description>>({
        {core::utctime(), turbine_description}
    }));

    CHECK(client.set_attrs({}).attrs.empty());
    {
      auto [ok] = client.set_attrs({
        {{stm::url_format(
            model_id,
            {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u1->id)},
            "turbine_description"),
          stm::any_attr{turbine_description_attr}},
         {stm::url_format(
            model_id,
            {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u2->id)},
            "turbine_description"),
          stm::any_attr{turbine_description_attr}}}
      });
      auto all_ok = std::ranges::all_of(ok, [&](auto err) {
        return !err.has_value();
      });
      CHECK(all_ok);
      CHECK(stm::equal_attributes(turbine_description_attr, u1->turbine_description));
      CHECK(stm::equal_attributes(turbine_description_attr, u2->turbine_description));
    }
    {
      std::vector<std::pair<std::string, stm::any_attr>> attrs{
        {stm::url_format(
           model_id, {stm::url_make_step<&stm::stm_system::hps>(hps->id), stm::url_make_step<&stm::stm_hps::units>(u1->id)},
         "turbisxxxne_description"),
         stm::any_attr{turbine_description_attr}}
      };
      auto [result] = client.set_attrs({attrs});
      REQUIRE(result.size() == 1);
      CHECK(result[0].has_value());
      CHECK(
        result[0]->what
        == "attribute turbisxxxne_description not found, requested url: dstm://Mm1/H1/U51.turbisxxxne_description");
    }
    {
      std::vector<std::pair<std::string, stm::any_attr>> attrs{
        {"xbsdfasd", stm::any_attr{turbine_description_attr}}
      };
      auto [result] = client.set_attrs({attrs});
      REQUIRE(result.size() == 1);
      CHECK(result[0].has_value());
      CHECK(result[0]->what == "url xbsdfasd is not parseable: url xbsdfasd starts neither with dstm schema (dstm://) or dtss schema (shyft://)");
    }
    {
      std::vector<std::pair<std::string, stm::any_attr>> attrs{
        {stm::url_format(
           model_id, {stm::url_make_step<&stm::stm_system::hps>(hps->id + 2), stm::url_make_step<&stm::stm_hps::units>(u1->id)},
         "turbine_description"),
         stm::any_attr{turbine_description_attr}}
      };
      auto [result] = client.set_attrs({attrs});
      REQUIRE(result.size() == 1);
      CHECK(result[0].has_value());
      CHECK(result[0]->what == "component H3 not found, requested url: dstm://Mm1/H3/U51.turbine_description");
    }
  }

  TEST_CASE("stm/dstm_reset_model") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server server;
    server.set_listening_ip("127.0.0.1");

    temp_dir tmpdir{"dstm_reset_model.test."};
    add_container(*server.dispatch.state->dtss, "test", (tmpdir / "ts").string());
    auto port_no = server.start_server();
    REQUIRE_GT(port_no, 0); // require vs. test.abort this part of test if we fail here

    core::utcperiod bind_period{core::from_seconds(0), core::from_seconds(10)};
    time_axis::generic_dt time_axis{core::from_seconds(0), core::from_seconds(10), 3};
    std::string u2_prod_url{"shyft://test/u2.production"};
    do_store_ts(
      *server.dispatch.state->dtss,
      {
        time_series::dd::apoint_ts{
                                   u2_prod_url, time_series::dd::apoint_ts{time_axis, {1.0, 2.0, 3.0}, time_series::ts_point_fx::POINT_AVERAGE_VALUE}}
    },
      shyft::dtss::store_policy{.recreate = true, .strict = true, .cache = true});

    constexpr auto model_id = "m1";
    auto model = test::create_stm_system();
    auto hps = model->hps[0];

    auto g = dynamic_cast<stm::gate *>(hps->find_gate_by_id(1).get());
    g->discharge.schedule = time_series::dd::apoint_ts(time_axis, 3.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    auto u2 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(52).get());

    add_model(server, model_id, model);

    u2->production.schedule = time_series::dd::apoint_ts{u2_prod_url};

    REQUIRE(u2->production.schedule.needs_bind());
    srv::dstm::evaluate_model(server, model_id, bind_period, false, false);
    srv::dstm::client client{core::srv_connection{fmt::format("localhost:{}", port_no)}};
    model = server.dispatch.state->models.get(model_id).get();
    hps = model->hps[0];
    u2 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(52).get());
    REQUIRE(!u2->production.schedule.needs_bind());
    REQUIRE(client.reset_model({model_id}).status);
    model = server.dispatch.state->models.get(model_id).get();
    hps = model->hps[0];
    u2 = dynamic_cast<stm::unit *>(hps->find_unit_by_id(52).get());
    REQUIRE(u2->production.schedule.needs_bind());
  }

  TEST_CASE("stm/dstm_server_side_cycle") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0);
    try {
      auto host_port = fmt::format("localhost:{}", port_no);
      srv::dstm::client c{core::srv_connection{host_port}};
      SUBCASE("0") {
        auto stm_mdl = std::make_shared<stm::stm_system>(1, "m1", "");
        auto market = std::make_shared<stm::energy_market_area>(1, "1", "", stm_mdl);
        auto mdl_prefix = fmt::format("{}{}", "dstm://M", "m1");
        std::shared_ptr<time_series::dd::ipoint_ts const> buy_cycle{
          new time_series::dd::aref_ts{fmt::format("{}/m1.buy", mdl_prefix)}};
        std::shared_ptr<time_series::dd::ipoint_ts const> price_cycle{
          new time_series::dd::aref_ts{fmt::format("{}/m1.price", mdl_prefix)}};

        market->price = time_series::dd::apoint_ts(buy_cycle);
        market->buy = time_series::dd::apoint_ts(price_cycle);
        stm_mdl->market.push_back(market);
        CHECK_THROWS_AS(c.add_model({"m1", stm_mdl}), std::runtime_error);

        std::vector<std::pair<std::string, stm::any_attr>> attr_s;
        attr_s.emplace_back(
          std::make_pair(fmt::format("{}/m1.buy", mdl_prefix), time_series::dd::apoint_ts(price_cycle)));
        attr_s.emplace_back(
          std::make_pair(fmt::format("{}/m1.price", mdl_prefix), time_series::dd::apoint_ts(buy_cycle)));
        CHECK_THROWS_AS(c.set_attrs({attr_s}), std::runtime_error);
      }
      SUBCASE("1") {
        auto stm_mdl = std::make_shared<stm::stm_system>(1, "m1", "");
        auto market = std::make_shared<stm::energy_market_area>(1, "1", "", stm_mdl);
        auto mdl_prefix = fmt::format("{}{}", "dstm://M", "m1");
        auto mdl_prefix_2 = fmt::format("{}{}", "dstm://M", "m2");
        std::shared_ptr<time_series::dd::ipoint_ts const> price_cycle{
          new time_series::dd::aref_ts{fmt::format("{}/m2.price", mdl_prefix_2)}};
        market->price = time_series::dd::apoint_ts(price_cycle);
        stm_mdl->market.push_back(market);
        CHECK(c.add_model({"m1", stm_mdl}).status);

        auto stm_mdl_2 = std::make_shared<stm::stm_system>(1, "m2", "");
        auto em_2 = std::make_shared<stm::energy_market_area>(2, "2", "", stm_mdl_2);
        std::shared_ptr<time_series::dd::ipoint_ts const> price_cycle_2{
          new time_series::dd::aref_ts{fmt::format("{}/m1.price", mdl_prefix)}};
        em_2->price = time_series::dd::apoint_ts(price_cycle_2);
        stm_mdl_2->market.push_back(em_2);
        CHECK_THROWS_AS(c.add_model({"m2", stm_mdl_2}), std::runtime_error);

        std::vector<std::pair<std::string, stm::any_attr>> attr_s;
        attr_s.emplace_back(
          std::make_pair(fmt::format("{}/m2.price", mdl_prefix_2), time_series::dd::apoint_ts(price_cycle_2)));
        CHECK_THROWS_AS(c.set_attrs({attr_s}), std::runtime_error);

        time_series::dd::ats_vector vec;
        vec.emplace_back(price_cycle_2);
        CHECK_THROWS_AS(c.set_ts({"m2", vec}), std::runtime_error);
      }
      SUBCASE("2") {
        auto stm_mdl = std::make_shared<stm::stm_system>(1, "m1", "");
        CHECK(c.add_model({"m1", stm_mdl}).status);
        {
          auto stm_mdl_patch = std::make_shared<stm::stm_system>(1, "m1", "");
          auto market = std::make_shared<stm::energy_market_area>(1, "1", "", stm_mdl_patch);
          auto mdl_prefix = fmt::format("{}{}", "dstm://M", "m1");
          std::shared_ptr<time_series::dd::ipoint_ts const> price_cycle{
            new time_series::dd::aref_ts{fmt::format("{}/m1.price", mdl_prefix)}};
          market->price = time_series::dd::apoint_ts(price_cycle);
          stm_mdl_patch->market.push_back(market);
          CHECK_THROWS_AS(c.patch_model({"m1", stm_patch_op::add, stm_mdl_patch}), std::runtime_error);
        }
      }
    } catch (std::exception const &ex) {
      FAIL(ex.what());
      s.clear();
    }
  }

  TEST_CASE("stm/dstm/operation_timeout") {
    dlib::set_all_logging_levels(dlib::LNONE);
    srv::dstm::server s{};
    utctime reply_delay{core::from_seconds(0.00001)};
    s.dispatch.state->callback = [&](std::string mid, std::string args) {
      std::this_thread::sleep_for(reply_delay);
      return true;
    };

    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0);
    auto host_port = fmt::format("localhost:{}", port_no);
    srv::dstm::client c{
      core::srv_connection{host_port, 1000, 100}
    };
    // 1. test it just works
    auto r = c.fx({.model_id = "Hello", .fx_arg = "world"});
    REQUIRE_EQ(true, r.status);
    CHECK_EQ(0, c.connection.reconnect_count);
    // 2. configure to force operation timeout(no reply within time limit)
    reply_delay = core::from_seconds(0.150);
    c.close();
    CHECK_THROWS_AS(c.fx({.model_id = "Hello", .fx_arg = "world"}), std::runtime_error);
    CHECK_EQ(3, c.connection.reconnect_count);
    c.close();
    // 3. verify it works automatically after restoring reply timeout
    reply_delay = core::from_seconds(0.00001);
    std::ignore = c.fx({.model_id = "Hello", .fx_arg = "world"});
    CHECK_EQ(3, c.connection.reconnect_count);

    // 4. verify the connection timeout, and auto connects
    std::this_thread::sleep_for(core::from_seconds(0.150)); // should close now
    std::ignore = c.fx({.model_id = "Hello", .fx_arg = "world"});
    CHECK_EQ(4, c.connection.reconnect_count);

    s.set_graceful_close_timeout(500);
    s.clear();
  }

  TEST_SUITE_END;

#undef mk_ts_url

}
