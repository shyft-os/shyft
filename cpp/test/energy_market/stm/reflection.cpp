#if !defined(WIN32)

#include <boost/mp11/list.hpp>

#include <shyft/energy_market/stm/reflection.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

  static_assert(!is_attr_v<std::vector<std::shared_ptr<reservoir_aggregate>>>);
  static_assert(!is_attr_v<reservoir_aggregate>);
  static_assert(is_attr_v<time_series::dd::apoint_ts>);
  static_assert(std::is_same_v<stm::paths_of<stm_hps>, std::tuple<>>);

}

#endif
