#pragma once
#include <shyft/time/utctime_utilities.h>

namespace shyft {
  namespace energy_market::stm {
    struct stm_hps;
    struct stm_system;
    using stm_hps_ = std::shared_ptr<stm_hps>;
    using stm_system_ = std::shared_ptr<stm_system>;
  }

  namespace dtss {
    struct server_state;
  }
}

namespace shyft::energy_market::stm::test {
  using namespace core;
  using std::vector;
  using std::string;
  using std::shared_ptr;
  using std::make_shared;

  namespace {
    inline utctime _t(int64_t t1970s) {
      return utctime{seconds(t1970s)};
    }
  }

  stm_hps_ create_stm_hps(int id = 1, string name = "sørland");

  stm_hps_ create_simple_hps(int id = 1, string name = "simple");

  stm_system_ create_stm_system(int id = 1, string name = "stm_system", string json = "");

  stm_system_ create_simple_system(int id = 1, string name = "simple_system");

  stm_system_
    create_simple_system_with_dtss(shyft::dtss::server_state& dtss, int id = 1, string name = "simple_system");
}
