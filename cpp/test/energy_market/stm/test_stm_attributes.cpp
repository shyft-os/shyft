#include <memory>
#include <string>

#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/reflection.h>

#include <doctest/doctest.h>

namespace shyft::energy_market::stm {


  using shyft::core::utctime;
  using ta = shyft::time_axis::fixed_dt;
  using shyft::time_series::POINT_INSTANT_VALUE;

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/t_xy_") {
    // Test that checking equality does a deep by-value comparison

    auto a = std::make_shared<t_xy_::element_type>();
    auto b = std::make_shared<t_xy_::element_type>();
    auto t = utctime{11l};

    CHECK(equal_attributes(a, b)); // empty attributes are equal

    a->emplace(std::make_pair(t, std::make_shared<hydro_power::xy_point_curve>()));
    CHECK(!equal_attributes(a, b));

    b->emplace(std::make_pair(t, std::make_shared<hydro_power::xy_point_curve>()));
    CHECK(equal_attributes(a, b));

    a->at(t)->points.emplace_back(0.0, 110.0);
    CHECK(!equal_attributes(a, b));

    b->at(t)->points.emplace_back(0.0, 110.0);
    CHECK(equal_attributes(a, b));
  }

  TEST_CASE("stm/t_xyz_") {
    // Test that checking equality does a deep by-value comparison

    auto a = std::make_shared<t_xyz_::element_type>();
    auto b = std::make_shared<t_xyz_::element_type>();
    auto t = utctime{11l};

    CHECK(equal_attributes(a, b)); // empty attributes are equal

    a->emplace(std::make_pair(t, std::make_shared<hydro_power::xy_point_curve_with_z>()));
    CHECK(!equal_attributes(a, b));

    b->emplace(std::make_pair(t, std::make_shared<hydro_power::xy_point_curve_with_z>()));
    CHECK(equal_attributes(a, b));

    a->at(t)->xy_curve.points.emplace_back(10.0, 13.0);
    a->at(t)->z = 1.0;
    CHECK(!equal_attributes(a, b));

    b->at(t)->xy_curve.points.emplace_back(10.0, 13.0);
    b->at(t)->z = 1.0;
    CHECK(equal_attributes(a, b));
  }

  TEST_CASE("stm/t_xyz_list_") {
    // Test that checking equality does a deep by-value comparison

    auto a = std::make_shared<t_xyz_list_::element_type>();
    auto b = std::make_shared<t_xyz_list_::element_type>();
    auto t = utctime{11l};

    CHECK(equal_attributes(a, b)); // empty attributes are equal

    a->emplace(std::make_pair(t, std::make_shared<std::vector<hydro_power::xy_point_curve_with_z>>()));
    CHECK(!equal_attributes(a, b));

    b->emplace(std::make_pair(t, std::make_shared<std::vector<hydro_power::xy_point_curve_with_z>>()));
    CHECK(equal_attributes(a, b));

    a->at(t)->emplace_back();
    a->at(t)->back().xy_curve.points.emplace_back(10.0, 13.0);
    a->at(t)->back().z = 1.0;
    CHECK(!equal_attributes(a, b));

    b->at(t)->emplace_back();
    b->at(t)->back().xy_curve.points.emplace_back(10.0, 13.0);
    b->at(t)->back().z = 1.0;
    CHECK(equal_attributes(a, b));
  }

  TEST_CASE("stm/t_turbine_description_") {
    // Test that checking equality does a deep by-value comparison

    auto a = std::make_shared<t_turbine_description_::element_type>();
    auto b = std::make_shared<t_turbine_description_::element_type>();
    auto t = utctime{11l};

    CHECK(equal_attributes(a, b)); // empty attributes are equal

    a->emplace(std::make_pair(t, std::make_shared<hydro_power::turbine_description>()));
    CHECK(!equal_attributes(a, b));

    b->emplace(std::make_pair(t, std::make_shared<hydro_power::turbine_description>()));
    CHECK(equal_attributes(a, b));


    hydro_power::turbine_operating_zone z{
      .efficiency_curves{},
      .production_min{},
      .production_max{},
      .production_nominal{},
      .fcr_min = 1.0,
      .fcr_max = 2.0};

    a->at(t)->operating_zones.push_back(z);
    CHECK(!equal_attributes(a, b));

    b->at(t)->operating_zones.emplace_back(z);
    CHECK(equal_attributes(a, b));
  }

  TEST_CASE("stm/apoint_ts") {
    // Test that we can compare both bound and ubound time series
    auto a = apoint_ts("test");
    auto b = 2 * apoint_ts("test");
    auto c = 3 * apoint_ts("test");
    auto d = 2 * apoint_ts("other");
    auto e = apoint_ts(ta{1, 3600, 5}, 1.1, POINT_INSTANT_VALUE);
    auto f = apoint_ts("test", e);

    SUBCASE("both_bound") {
      CHECK(equal_attributes(e, f));
    }
  }

  TEST_CASE("stm/clone_attr") {
    SUBCASE("bool") {
      bool attr = true;
      auto cloned_attr = clone_attr(attr);
      auto as_bool = std::get_if<bool>(&cloned_attr);
      REQUIRE(as_bool);
      CHECK(equal_attributes(*as_bool, attr));
      // this is always copied, should be the same
    }
    SUBCASE("double") {
      double attr = -91.823;
      auto cloned_attr = clone_attr(attr);
      auto as_double = std::get_if<double>(&cloned_attr);
      REQUIRE(as_double);
      CHECK(equal_attributes(*as_double, attr));
      // this is always copied, should be the same
    }
    SUBCASE("std::int64_t") {
      std::int64_t attr = -8975;
      auto cloned_attr = clone_attr(attr);
      auto as_int64 = std::get_if<std::int64_t>(&cloned_attr);
      REQUIRE(as_int64);
      CHECK(equal_attributes(*as_int64, attr));
      // this is always copied, should be the same
    }
    SUBCASE("std::uint64_t") {
      std::uint64_t attr = 8975;
      auto cloned_attr = clone_attr(attr);
      auto as_uint64 = std::get_if<std::uint64_t>(&cloned_attr);
      REQUIRE(as_uint64);
      CHECK(equal_attributes(*as_uint64, attr));
      // this is always copied, should be the same
    }
    SUBCASE("apoint") {
      time_axis::generic_dt ta{core::from_seconds(0), core::from_seconds(10), 5};
      time_series::dd::apoint_ts attr{
        ta, {1.0, 2.0, 3.0, 4.0, 5.0},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      auto cloned_attr = clone_attr(attr);
      auto as_apoint = std::get_if<time_series::dd::apoint_ts>(&cloned_attr);
      REQUIRE(as_apoint);
      CHECK(*as_apoint == attr);
      // make sure underlying is not the same:
      CHECK(as_apoint->ts.get() != attr.ts.get());
    }
    SUBCASE("apoint_ref") {
      time_axis::generic_dt ta{core::from_seconds(0), core::from_seconds(10), 5};
      time_series::dd::apoint_ts rsv_volume_result{
        ta, {1.0, 2.0, 3.0, 4.0, 5.0},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      auto attr = time_series::dd::apoint_ts{"some_ref", rsv_volume_result};
      auto cloned_attr = clone_attr(attr);
      auto as_apoint = std::get_if<time_series::dd::apoint_ts>(&cloned_attr);
      REQUIRE(as_apoint);
      // same id:
      CHECK(as_apoint->id() == attr.id());
      // stays bound:
      CHECK(!as_apoint->needs_bind());
      // make sure underlying is not the same:
      CHECK(as_apoint->ts.get() != attr.ts.get());
    }
    SUBCASE("t_xy_") {
      auto attr = std::make_shared<t_xy_::element_type>();
      auto t = utctime{11l};
      attr->emplace(std::make_pair(t, std::make_shared<hydro_power::xy_point_curve>()));
      attr->at(t)->points.emplace_back(0.0, 110.0);
      auto cloned_attr = clone_attr(attr);
      auto as_txy = std::get_if<t_xy_>(&cloned_attr);
      REQUIRE(as_txy);
      CHECK(equal_attributes(*as_txy, attr));
      // make sure underlying is not the same:
      CHECK(as_txy->get() != attr.get());
    }
    SUBCASE("t_xyz_") {
      auto attr = std::make_shared<t_xyz_::element_type>();
      auto t = utctime{11l};
      attr->emplace(std::make_pair(t, std::make_shared<hydro_power::xy_point_curve_with_z>()));
      attr->at(t)->xy_curve.points.emplace_back(10.0, 13.0);
      attr->at(t)->z = 1.0;
      auto cloned_attr = clone_attr(attr);
      auto as_txyz = std::get_if<t_xyz_>(&cloned_attr);
      REQUIRE(as_txyz);
      CHECK(equal_attributes(*as_txyz, attr));
      // make sure underlying is not the same:
      CHECK(as_txyz->get() != attr.get());
    }
    SUBCASE("t_xyz_list_") {
      auto attr = std::make_shared<t_xyz_list_::element_type>();
      auto t = utctime{11l};
      attr->emplace(std::make_pair(t, std::make_shared<std::vector<hydro_power::xy_point_curve_with_z>>()));
      attr->at(t)->emplace_back();
      attr->at(t)->back().xy_curve.points.emplace_back(10.0, 13.0);
      attr->at(t)->back().z = 1.0;
      auto cloned_attr = clone_attr(attr);
      auto as_txyz_list = std::get_if<t_xyz_list_>(&cloned_attr);
      REQUIRE(as_txyz_list);
      CHECK(equal_attributes(*as_txyz_list, attr));
      // make sure underlying is not the same:
      CHECK(as_txyz_list->get() != attr.get());
    }
    SUBCASE("t_turbine_description_") {
      auto attr = std::make_shared<t_turbine_description_::element_type>();
      auto t = utctime{11l};
      attr->emplace(std::make_pair(t, std::make_shared<hydro_power::turbine_description>()));
      hydro_power::turbine_operating_zone z{
        .efficiency_curves{},
        .production_min{},
        .production_max{},
        .production_nominal{},
        .fcr_min = 1.0,
        .fcr_max = 2.0};

      attr->at(t)->operating_zones.push_back(z);
      auto cloned_attr = clone_attr(attr);
      auto as_trubine_description = std::get_if<t_turbine_description_>(&cloned_attr);
      REQUIRE(as_trubine_description);
      CHECK(equal_attributes(*as_trubine_description, attr));
      // make sure underlying is not the same:
      CHECK(as_trubine_description->get() != attr.get());
    }
    SUBCASE("string") {
      std::string attr = "test";
      auto cloned_attr = clone_attr(attr);
      auto as_str = std::get_if<std::string>(&cloned_attr);
      REQUIRE(as_str);
      CHECK(equal_attributes(*as_str, attr));
      // this is always copied, should be the same
    }
    SUBCASE("time_series::dd::ats_vector") {
      time_axis::generic_dt ta{core::from_seconds(0), core::from_seconds(10), 5};
      time_series::dd::apoint_ts some_ts{
        ta, {1.0, 2.0, 3.0, 4.0, 5.0},
         time_series::ts_point_fx::POINT_AVERAGE_VALUE
      };
      time_series::dd::ats_vector attr{some_ts + 4 * some_ts};
      auto cloned_attr = clone_attr(attr);
      auto as_apoint_list = std::get_if<time_series::dd::ats_vector>(&cloned_attr);
      REQUIRE(as_apoint_list);
      CHECK(*as_apoint_list == attr);
      // make sure underlying is not the same:
      CHECK(as_apoint_list->operator[](0).ts.get() != attr[0].ts.get());
    }
    SUBCASE("energy_market::stm::unit_group_type") {
      auto attr = energy_market::stm::unit_group_type::afrr_down;
      auto cloned_attr = clone_attr(attr);
      auto as_ugt = std::get_if<energy_market::stm::unit_group_type>(&cloned_attr);
      REQUIRE(as_ugt);
      CHECK(equal_attributes(*as_ugt, attr));
      // this is always copied, should be the same
    }
    SUBCASE("time_axis::generic_dt") {
      auto cal = std::make_shared<core::calendar>("Europe/Oslo");
      time_axis::generic_dt attr{cal, core::from_seconds(0), core::from_seconds(3600 * 24), 5};
      auto cloned_attr = clone_attr(attr);
      auto as_gt = std::get_if<time_axis::generic_dt>(&cloned_attr);
      REQUIRE(as_gt);
      CHECK(*as_gt == attr);
      // this is always copied, should be the same
    }
  }

  TEST_SUITE_END();

}
