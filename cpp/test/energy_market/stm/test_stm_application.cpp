#include <cstdint>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <fmt/chrono.h>
#include <fmt/core.h>

#include <shyft/energy_market/stm/srv/application/client.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/energy_market/stm/srv/application/server.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>

#include <doctest/doctest.h>

namespace shyft::energy_market::stm::srv {

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/application_protocol") {
    experimental::application::server s;

    constexpr auto server_ip = "127.0.0.1";
    s.set_listening_ip(server_ip);
    auto s_port = s.start_server();

    experimental::application::client c{{.connection{fmt::format("{}:{}", server_ip, s_port)}}};

    // 0. Arrange server and a client running on localhost, tempdir:
    CHECK_GE(s_port, 1024);
    std::vector<std::pair<std::int64_t, std::string>> cb_x; // arrange to capture callbacks
    s.manager.callback<experimental::application::model_tag::task>() = [&cb_x](std::int64_t mid, std::string arg) {
      cb_x.emplace_back(mid, arg);
      return cb_x.size() == 1; // true first time, then false, to verify we propagate back the result
    };

    std::vector<std::int64_t> mids;
    auto [mis] = c.get_task_infos({mids});
    CHECK_EQ(mis.size(), 0);

    // 1. Create one stm_task:
    auto task = std::make_shared<stm_task>(1, "testtask", core::from_seconds(3600));
    task->labels.push_back("test");

    auto r = std::make_shared<stm_case>(1, "testrun", core::from_seconds(3600));
    r->model_refs.push_back(std::make_shared<model_ref>("host", 123, 456, "key"));
    shyft::srv::model_info mi(task->id, task->name, task->created, task->json);
    task->add_case(r);

    // 2. store it to server:
    auto const [rid] = c.store_task({*task, mi});
    CHECK_EQ(rid, task->id);

    // 2.a make a callback
    CHECK(c.task_fx({1, "cb"}).result);
    CHECK_FALSE(c.task_fx({2, "2"}).result);
    CHECK_EQ(
      cb_x, std::vector<std::pair<std::int64_t, std::string>>{std::make_pair(1l, "cb"), std::make_pair(2l, "2")});

    // 3. verify model info and run:
    auto [task2] = c.read_task({task->id});
    CHECK_EQ(*task, task2);
    mids.emplace_back(mi.id);
    mis = c.get_task_infos({mids}).model_infos;
    CHECK_EQ(mis.size(), 1u);
    CHECK_EQ(mi, mis[0]);

    // 4. Filtered get_task_infos search:
    auto [mis2] = c.get_task_infos({mids, core::utcperiod(0, 1000)});
    CHECK_EQ(mis2.size(), 0u);
    mis2 = c.get_task_infos({mids, utcperiod(2000, 4000)}).model_infos;
    CHECK_EQ(mis2.size(), 1u);
    CHECK_EQ(mi, mis2[0]);

    c.close();

    // 5. Check that we can update model info of existing run
    mi.json = std::string("{\"updated\": true}");
    c.update_task_info({mi.id, mi});
    mis = c.get_task_infos({mids}).model_infos;
    CHECK_EQ(mis.size(), 1u);
    CHECK_EQ(mi, mis[0]);

    // 6. Add run to a task:
    auto r2 = std::make_shared<stm_case>(2, "testrun2", core::from_seconds(10));
    c.add_case({task->id, r2});
    task2 = c.read_task({task->id}).model;
    CHECK_EQ(task2.cases.size(), 2);
    CHECK_THROWS(c.add_case({1, r2}));

    // 7. Get run from a task:
    CHECK_EQ(*r, *c.get_case_by_id({task->id, r->id}).case_);
    CHECK_EQ(nullptr, c.get_case_by_id({task->id, 4}).case_);
    CHECK_EQ(c.get_case_by_id({-1, r->id}).case_, nullptr);

    CHECK_EQ(*r2, *c.get_case_by_name({task->id, r2->name}).case_);
    CHECK_EQ(nullptr, c.get_case_by_name({task->id, "norun"}).case_);

    // 8. Modify run inplace
    auto [r2_get] = c.get_case_by_name({task->id, r2->name});
    r2_get->name = "run_test";
    r2_get->json = "{'key': value}";
    r2_get->labels.insert(r2_get->labels.end(), {"test", "calibration"});
    c.update_case({task->id, r2_get});
    auto [r2_updated] = c.get_case_by_name({task->id, r2_get->name});
    CHECK_EQ(r2_updated->name, "run_test");
    CHECK_EQ(r2_updated->json, "{'key': value}");
    CHECK_EQ(r2_updated->labels, std::vector<std::string>{"test", "calibration"});

    // 9. Add a model_reference to a run:
    auto mr = std::make_shared<model_ref>("host", 12, 34, "testkey");
    c.add_task_ref({task->id, r->id, mr});
    CHECK_EQ(*mr, *c.get_task_ref({task->id, r->id, "testkey"}).ref);
    CHECK_EQ(nullptr, c.get_task_ref({task->id, r->id, "nonkey"}).ref);
    CHECK_EQ(nullptr, c.get_task_ref({task->id, -1, "testkey"}).ref);
    CHECK_EQ(c.get_task_ref({-1, r->id, "testkey"}).ref, nullptr);

    // Some invalid inputs:
    CHECK_EQ(c.get_case_by_id({task->id, r->id}).case_->model_refs.size(), 2);
    c.add_task_ref({task->id, -1, mr}); // Invalid run ID, so should not be added.
    CHECK_EQ(c.get_case_by_id({task->id, r->id}).case_->model_refs.size(), 2);

    // 10. Remove model reference from a run:
    CHECK(c.remove_task_ref({task->id, r->id, "testkey"}).result);
    CHECK_EQ(c.get_case_by_id({task->id, r->id}).case_->model_refs.size(), 1);
    CHECK_FALSE(c.remove_task_ref({-1, r->id, "testkey"}).result);
    CHECK_FALSE(c.remove_task_ref({task->id, -1, "testkey"}).result);
    CHECK_FALSE(c.remove_task_ref({task->id, r->id, "nonkey"}).result);

    // 11. Remove runs from a task:
    CHECK_FALSE(c.remove_case_by_id({task->id, 3}).result);
    CHECK(c.remove_case_by_id({task->id, 2}).result);
    task2 = c.read_task({task->id}).model;
    CHECK_EQ(task2.cases.size(), 1);
    CHECK_FALSE(c.remove_case_by_id({task->id, 2}).result);

    CHECK_FALSE(c.remove_case_by_name({task->id, "norun"}).result);
    CHECK(c.remove_case_by_name({task->id, "testrun"}).result);
    task2 = c.read_task({task->id}).model;
    CHECK_EQ(task2.cases.size(), 0);
    CHECK_FALSE(c.remove_case_by_name({task->id, "testrun"}).result);

    // Finally, remove the model, and verify we're are back to zero:
    CHECK(!c.remove_task({task->id}).result);
    mis = c.get_task_infos({mids}).model_infos;
    CHECK_EQ(mis.size(), 0u);
    c.close();
    s.clear();
  }

  TEST_SUITE_END();

}
