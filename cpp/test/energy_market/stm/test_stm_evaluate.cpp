#include <functional>
#include <memory>
#include <string>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include <doctest/doctest.h>
#include <test/energy_market/stm/build_test_system.h>
#include <test/energy_market/stm/utilities.h>

namespace shyft::energy_market::stm {

  using core::from_seconds;
  using core::deltahours;

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/with_reference_ts") {
    auto mdl = test::create_simple_system(1, "0");
    auto mkt = mdl->market[0];

    // create arefs in model to collect:
    auto price_url = fmt::format("dstm://M{}/m{}.price", mdl->name, mkt->id);
    auto load_url = fmt::format("dstm://M{}/m{}.load", mdl->name, mkt->id);
    auto other_model_url = fmt::format("dstm://M{}/m{}.load", "other", mkt->id);
    auto third_url = "shyft://third/url";

    std::vector<std::string> called_ids;

    SUBCASE("pre") {
      SUBCASE("oneself_gets_called") {
        mkt->tsm["custom"] = time_series::dd::apoint_ts(third_url);
        stm::with_reference_ts(
          mdl->name,
          *mdl,
          mkt->tsm["custom"].ts,
          [&](time_series::dd::aref_ts& ts) {
            called_ids.push_back(ts.id);
          },
          ignore);
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == third_url;
        }));
      }

      SUBCASE("traverse_down_same_mid") {
        mkt->load = time_series::dd::apoint_ts(price_url);
        mkt->price = time_series::dd::apoint_ts(third_url);
        mkt->tsm["custom"] = time_series::dd::apoint_ts(load_url);
        stm::with_reference_ts(
          mdl->name,
          *mdl,
          mkt->tsm["custom"].ts,
          [&](time_series::dd::aref_ts& ts) {
            called_ids.push_back(ts.id);
          },
          ignore);
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == third_url;
        }));
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == price_url;
        }));
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == load_url;
        }));
      }

      SUBCASE("collect_but_no_traverse_other_model") {
        mkt->load = time_series::dd::apoint_ts(price_url) - 30;
        mkt->price = time_series::dd::apoint_ts(other_model_url) + 1;
        mkt->tsm["custom"] = 22 * time_series::dd::apoint_ts(load_url);
        stm::with_reference_ts(
          mdl->name,
          *mdl,
          mkt->tsm["custom"].ts,
          [&](time_series::dd::aref_ts& ts) {
            called_ids.push_back(ts.id);
          },
          ignore);
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == other_model_url;
        }));
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == price_url;
        }));
        REQUIRE(std::ranges::any_of(called_ids, [&](auto&& id) {
          return id == load_url;
        }));
      }
    }
  }

  TEST_CASE("stm/rebind_expression") {
    auto internal_model_id = "a";
    auto model = std::make_shared<stm_system>(0, internal_model_id, "a");

    auto market = std::make_shared<energy_market_area>(1, "a", "a", model);
    model->market.push_back(market);

    CHECK(!rebind_expression(*model, "a"));

    auto dtss_url = dtss::shyft_url("stm", "abc");
    auto external_model_id = "b";
    auto external_url = url_format(
      external_model_id,
      {
        url_make_step<&stm_system::market>(market->id),
      },
      "sale");

    model->market[0]->buy = time_series::dd::apoint_ts(std::make_shared<time_series::dd::aref_ts>(external_url));
    model->market[0]->sale = time_series::dd::apoint_ts(
      std::make_shared<time_series::dd::aref_ts>(time_series::dd::aref_ts(dtss_url)));

    CHECK(!rebind_expression(*model, internal_model_id));

    CHECK(model->market[0]->buy.needs_bind());
    CHECK(model->market[0]->sale.needs_bind());

    model->market[0]->buy = time_series::dd::apoint_ts(std::make_shared<time_series::dd::aref_ts>(
      external_url, std::dynamic_pointer_cast<time_series::dd::gpoint_ts const>(test::create_t_double().ts)));
    model->market[0]->sale = time_series::dd::apoint_ts(std::make_shared<time_series::dd::aref_ts>(
      dtss_url, std::dynamic_pointer_cast<time_series::dd::gpoint_ts const>(test::create_t_double().ts)));

    CHECK(!model->market[0]->buy.needs_bind());
    CHECK(!model->market[0]->sale.needs_bind());

    CHECK(!rebind_expression(*model, internal_model_id));

    CHECK(!model->market[0]->buy.needs_bind());
    CHECK(!model->market[0]->sale.needs_bind());

    auto internal_url = url_format(
      internal_model_id,
      {
        url_make_step<&stm_system::market>(market->id),
      },
      "sale");

    model->market[0]->production = time_series::dd::apoint_ts(std::make_shared<time_series::dd::aref_ts>(internal_url));
    CHECK(model->market[0]->production.needs_bind());
    CHECK(rebind_expression(*model, "a", false));
    CHECK(model->market[0]->production.needs_bind());
    CHECK(rebind_expression(*model, "a", true));
    CHECK(!model->market[0]->production.needs_bind());

    model->market[0]->production = time_series::dd::apoint_ts(std::make_shared<time_series::dd::aref_ts>(
      internal_url, std::dynamic_pointer_cast<time_series::dd::gpoint_ts const>(test::create_t_double().ts)));
    CHECK(rebind_expression(*model, "a"));
    CHECK(!model->market[0]->production.needs_bind());
  }

  TEST_CASE("stm/evaluate_ts") {
    auto mdl = test::create_simple_system(1, "0");
    auto mkt = mdl->market[0];

    // In ids:
    auto price_url = fmt::format("dstm://M{}/m{}.price", mdl->name, mkt->id);
    auto max_buy_url = fmt::format("dstm://M{}/m{}.max_buy", mdl->name, mkt->id);
    auto custom_url = fmt::format("dstm://M{}/m{}.ts.custom", mdl->name, mkt->id);
    auto dtss_custom_url = "shyft://test/custom";
    auto dtss_other_url = "shyft://test/other";
    mkt->price = time_series::dd::apoint_ts(dtss_custom_url);
    mkt->tsm["custom"] = time_series::dd::apoint_ts(dtss_custom_url);
    mkt->max_buy = 2.0 + time_series::dd::apoint_ts(price_url);
    boost::asio::thread_pool execution_context;

    auto models = make_shared_models(
      execution_context.executor(),
      {
        {"0", mdl}
    });

    auto get_ts = []<typename T>(T const & d) -> std::optional<time_series::dd::apoint_ts> {
      if constexpr (time_series::dd::is_apoint_ts_v<T>) {
        return d;
      } else {
        static_assert(is_evaluate_ts_error_v<T>);
        return std::nullopt;
      }
    };

    auto get_err = []<typename T>(T const & d) -> std::optional<std::string> {
      if constexpr (time_series::dd::is_apoint_ts_v<T>) {
        return std::nullopt;
      } else {
        static_assert(is_evaluate_ts_error_v<T>);
        return d.what;
      }
    };

    std::string error_url = "shyft://test/error", error2_url = "shyft://test_error2";
    std::string non_returning_url = "shyft://does/not/return";
    auto dtss = [&](auto const & urls, utcperiod read_period, auto&&...) {
      std::tuple<
        std::vector<std::shared_ptr<time_series::dd::gpoint_ts const>>,
        std::vector<utcperiod>,
        std::vector<dtss::read_error>>
        result;
      auto& [result_ts, result_tp, errors] = result;
      for (auto&& [index, url] : std::views::enumerate(urls)) {
        if (url.starts_with(error_url)) {
          errors.push_back(
            dtss::read_error{.index = static_cast<std::size_t>(index), .code = dtss::lookup_error::container_throws});
          result_ts.push_back(std::make_shared<time_series::dd::gpoint_ts const>());
          result_tp.push_back(utcperiod{});
          continue;
        } else if (url.starts_with(non_returning_url)) {
          continue;
        }
        auto value = 1.0;
        if (url.starts_with(dtss_other_url)) {
          value = 2.0;
        }
        result_ts.push_back(std::make_shared<time_series::dd::gpoint_ts const>(
          time_axis::generic_dt{read_period.start, read_period.end, 2}, value));
        result_tp.emplace_back(read_period);
      }
      return result;
    };

    utcperiod bind_period{utctime{0l}, utctime{7l}};

    SUBCASE("simple_eval") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(price_url)
        + 4 * time_series::dd::apoint_ts(dtss_other_url) // 1.0 + 4.0*2.0 in values
      };
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      REQUIRE(tsv_eval.size() == 1);
      auto result = std::visit(get_ts, tsv_eval[0]);
      REQUIRE(result);
      CHECK(result->values()[0] == doctest::Approx(9.0));
    }
    SUBCASE("cycle_in_expression") {
      auto sale_url = fmt::format("dstm://M{}/m{}.sale", mdl->name, mkt->id);
      auto buy_url = fmt::format("dstm://M{}/m{}.buy", mdl->name, mkt->id);
      mkt->sale = time_series::dd::apoint_ts(buy_url);
      mkt->buy = time_series::dd::apoint_ts(sale_url);
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(buy_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value());
      CHECK(
        *err == "ts ((dstm://M0/m2.buy + dstm://M0/m2.max_buy) + shyft://test/custom) contains a cyclic expression");
    }
    SUBCASE("url_invalid_in_expression") {
      auto invalid_model_url = fmt::format("dstm://M{}i/m{}.sale", mdl->name, mkt->id);
      auto invalid_attr_url = fmt::format("dstm://M{}/m{}.salei", mdl->name, mkt->id);
      auto invalid_path_url = fmt::format("dstm://M{}/m{}i.sale", mdl->name, mkt->id);
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(invalid_model_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value());
      CHECK(*err == "dstm timeseries ((dstm://M0i/m2.sale + dstm://M0/m2.max_buy) + shyft://test/custom) did not resolve with error: model with id 0i not found, requested url: dstm://M0i/m2.sale");
      tsv.clear();
      tsv.push_back(
        time_series::dd::apoint_ts(invalid_attr_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url));
      tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value());
      CHECK(*err == "dstm timeseries ((dstm://M0/m2.salei + dstm://M0/m2.max_buy) + shyft://test/custom) did not resolve with error: attribute salei not found, requested url: dstm://M0/m2.salei");
      tsv.clear();
      tsv.push_back(
        time_series::dd::apoint_ts(invalid_path_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url));
      tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value());
      CHECK(*err == "dstm timeseries ((dstm://M0/m2i.sale + dstm://M0/m2.max_buy) + shyft://test/custom) did not resolve with error: url dstm://M0/m2i.sale is not parseable: i.sale does not start with an attribute or component delimiter");
    }
    SUBCASE("dtss_returns_empty") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(non_returning_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      REQUIRE_THROWS(evaluate_ts(dtss, models, tsv, bind_period, false, false, {}));
    }
    SUBCASE("dtss_returns_error") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(error_url) + time_series::dd::apoint_ts(max_buy_url)
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value());
      CHECK(*err == "dtss timeseries with url shyft://test/error did not resolve with error: container_throws");
    }

    SUBCASE("multiple_error_in_dtss") {
      time_series::dd::ats_vector tsv{
        time_series::dd::apoint_ts(error_url) + time_series::dd::apoint_ts(error_url + "2")
        + time_series::dd::apoint_ts(dtss_custom_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      REQUIRE(err.has_value());
      std::string_view err_v = *err;
      CHECK(err_v.contains("shyft://test/error did not resolve"));
      CHECK(err_v.contains("shyft://test/error2 did not resolve"));
    }
    SUBCASE("one_has_err_one_doesn_t") {
      time_series::dd::ats_vector tsv{
        (time_series::dd::apoint_ts(error_url) + time_series::dd::apoint_ts(max_buy_url)
         + time_series::dd::apoint_ts(dtss_custom_url)),
        (time_series::dd::apoint_ts(max_buy_url) + time_series::dd::apoint_ts(dtss_custom_url))};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 2);
      auto err = std::visit(get_err, tsv_eval[0]);
      CHECK(err.has_value());
      CHECK(err->contains("shyft://test/error did not resolve"));
      auto ts = std::get_if<time_series::dd::apoint_ts>(&tsv_eval[1]);
      CHECK(ts->ts != nullptr);
      // check that the evaluation is correct
      CHECK(
        std::ranges::all_of(ts->ts->values(), [](auto const & v) { // FIXME: use bind_back when supported by compiler
          return v == doctest::Approx{4.0};
        }));
    }
    SUBCASE("one_error_in_several_expressions_plus_valid") {
      time_series::dd::ats_vector tsv{
        (time_series::dd::apoint_ts(error_url) + time_series::dd::apoint_ts(max_buy_url)
         + time_series::dd::apoint_ts(dtss_custom_url)),
        (time_series::dd::apoint_ts(max_buy_url) + time_series::dd::apoint_ts(dtss_custom_url)),
        time_series::dd::apoint_ts(error_url) * time_series::dd::apoint_ts(max_buy_url),
        time_series::dd::apoint_ts(max_buy_url)};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 4);
      auto err = std::visit(get_err, tsv_eval[0]);
      CHECK(err.has_value());
      CHECK(err->contains("shyft://test/error did not resolve"));
      auto ts = std::get_if<time_series::dd::apoint_ts>(&tsv_eval[1]);
      REQUIRE(ts->ts != nullptr);
      // check that the evaluation is correct
      CHECK(
        std::ranges::all_of(ts->ts->values(), [](auto const & v) { // FIXME: use bind_back when supported by compiler
          return v == doctest::Approx{4.0};
        }));
      err = std::visit(get_err, tsv_eval[2]);
      CHECK(err.has_value());
      CHECK(err->contains("shyft://test/error did not resolve"));
      ts = std::get_if<time_series::dd::apoint_ts>(&tsv_eval[3]);
      REQUIRE(ts->ts != nullptr);
      // check that the evaluation is correct
      CHECK(
        std::ranges::all_of(ts->ts->values(), [](auto const & v) { // FIXME: use bind_back when supported by compiler
          return v == doctest::Approx{3.0};
        }));
    }
    SUBCASE("valid_and_two_errors") {
      time_series::dd::ats_vector tsv{
        (time_series::dd::apoint_ts(max_buy_url) + time_series::dd::apoint_ts(error_url)
         + time_series::dd::apoint_ts(error2_url))};
      auto tsv_eval = evaluate_ts(dtss, models, tsv, bind_period, false, false, {});
      CHECK(tsv_eval.size() == 1);
      auto err = std::visit(get_err, tsv_eval[0]);
      CHECK(err.has_value());
    }
  }

  TEST_SUITE_END;
}
