#include <string>
#include <vector>

#include <shyft/energy_market/id_base.h>

#include <doctest/doctest.h>

static std::vector<void *> obj_list;

static void test_destroy(void *obj) {
  obj_list.push_back(obj);
}

using namespace shyft::energy_market;

TEST_SUITE_BEGIN("em");

TEST_CASE("em/handle") {
  em_handle::destroy = test_destroy;
  { em_handle a; }
  CHECK_EQ(0, obj_list.size());
  int i;
  {
    em_handle a;
    a.obj = &i;
  }
  REQUIRE_EQ(1, obj_list.size());
  CHECK_EQ(&i, obj_list[0]);

  em_handle a{&i};
  em_handle b(std::move(a));
  CHECK_EQ(&i, b.obj);

  em_handle c;
  c = std::move(b);
  CHECK_EQ(&i, c.obj);
  CHECK_EQ(nullptr, b.obj);
  CHECK_EQ(nullptr, a.obj);
}

TEST_CASE("em/id_base/move") {
  id_base a;
  id_base b;
  a = std::move(b);
}

TEST_SUITE_END();
