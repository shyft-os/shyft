#include <chrono>
#include <cmath>
#include <random>
#include <vector>

#include <boost/math/constants/constants.hpp>

#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/hydro_functions.h>
#include <shyft/hydrology/methods/penman_monteith_vegetation.h>
#include <shyft/hydrology/methods/radiation.h>
#include <shyft/time/utctime_utilities.h>

#include "test_pch.h"

namespace shyft {
  using core::geo_cell_data;
  using core::vegetation_cell_data;

  namespace test {

    class trapezoidal_average {
      double area = 0.0;
      double f_a = 0.0;
      // Left hand side of next integration subinterval
      double t_start = 0.0; // Start of integration period
      double t_a = 0.0;     // Left hand side time of next integration subinterval
     public:
      trapezoidal_average() = default;

      /**
       * @brief initialize must be called to reset states before being used during ode integration.
       */
      void initialize(double f0, double t_start) {
        this->f_a = f0;
        this->t_start = t_start;
        t_a = t_start;
        area = 0.0;
      }

      /**
       * @brief Add contribution to average using a simple trapezoidal rule
       *
       * @see: http://en.wikipedia.org/wiki/Numerical_integration
       */
      void add(double f, double t) {
        area += 0.5 * (f_a + f) * (t - t_a);
        f_a = f;
        t_a = t;
      }

      [[nodiscard]] double result() const {
        return area / (t_a - t_start);
      }
    };

  }

  TEST_SUITE("penman_monteith_vegetation") {
    using shyft::core::penman_monteith_vegetation::parameter;
    using shyft::core::geo_cell_data;
    using namespace shyft::core;
    //    using shyft::core::radiation::surface_normal;
    using shyft::core::calendar;
    using shyft::core::utctime;
    using namespace shyft::test;
    // test basics: creation, etc

    TEST_CASE("test_penman_monteith_vegetation_equal_operator") {
      double height_ws = 2.0;
      double height_t = 2.0;

      parameter p(height_ws, height_t);
      parameter p2(height_ws + 1.0, height_t);
      parameter p3(height_ws, height_t + 1.0);

      TS_ASSERT(p != p2);
      TS_ASSERT(p != p3);

      p2.height_ws = height_ws;
      p3.height_t = height_t;

      TS_ASSERT(p == p2);
      TS_ASSERT(p == p3);
    }

    TEST_CASE("hydrology/pm/hourly_pmv") {
      radiation::parameter rad_p{};
      radiation::response rad_r{};
      rad_p.albedo = 0.2;
      rad_p.turbidity = 1.0;
      radiation::calculator rad(rad_p);
      calendar utc_cal;
      // Greeley, Colorado weather station
      double lat = 40.41;
      double elevation = 1462.4;
      utctime t;
      // checking for horizontal surface Eugene, OR, p.64, fig.1b
      arma::vec surface_normal({0.0, 0.0, 1.0});
      double slope = 0.0;
      double aspect = 0.0;
      utctime ta;

      ta = utc_cal.time(2000, 06, 2, 00, 00, 0, 0);
      // rad.psw_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);

      // evapotranspiraiton PM
      penman_monteith_vegetation::parameter pm_p(3.0, 1.68);
      penman_monteith_vegetation::response pm_r{};
      penman_monteith_vegetation::calculator pm_calculator(pm_p);
      trapezoidal_average av_et{};
      av_et.initialize(pm_r.et, 0.0);
      // ref.: ASCE=EWRI Appendix C: Example Calculation of ET
      double temperature[24] = {18.4, 16.5, 15.4, 15.5, 13.5, 13.2, 16.2, 20.0, 22.9, 26.4, 28.2, 29.8,
                                30.9, 31.8, 32.5, 32.9, 32.4, 30.2, 30.6, 28.3, 25.9, 23.9, 20.1, 19.9};
      double vap_pressure[24] = {1.32, 1.26, 1.34, 1.31, 1.26, 1.24, 1.31, 1.36, 1.39, 1.25, 1.17, 1.03,
                                 1.02, 0.98, 0.87, 0.86, 0.93, 1.14, 1.27, 1.27, 1.17, 1.20, 1.10, 1.05};
      double windspeed[24] = {0.3,  0.5,  1.0,   0.68, 0.69, 0.29, 1.24, 1.28, 0.88, 0.72, 1.52, 1.97,
                              2.07, 2.76, 2.990, 3.10, 2.77, 3.41, 2.78, 2.95, 3.27, 2.86, 2.7,  2.0};
      double radiation_m[24] = {0.0,  0.0,  0.0,  0.0,  0.0,  0.03, 0.46, 1.09, 1.74, 2.34, 2.84, 3.25,
                                3.21, 3.34, 2.96, 2.25, 1.35, 0.88, 0.79, 0.27, 0.03, 0.0,  0.0,  0.0};
      double et_m[24] = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.10, 0.19, 0.32, 0.46, 0.60, 0.72,
                         0.73, 0.79, 0.74, 0.62, 0.44, 0.35, 0.29, 0.17, 0.10, 0.07, 0.01, 0.01};
      // 24h-average
      double etm_avg{0.0};
      for (int i = 0; i < 24; i++) {
        etm_avg += et_m[i];
      }
      double svp[24] = {
        0.0,
      };
      double rhumidity[24] = {
        0.0,
      };
      for (int i = 0; i < 24; i++) {
        svp[i] = hydro_functions::svp(temperature[i]);
        rhumidity[i] = vap_pressure[i] * 100 / svp[i];
      }
      auto dt = deltahours(1);

      rad.net_radiation_inst(rad_r, lat, ta, slope, aspect, temperature[0], rhumidity[0], elevation);

      geo_cell_data gcd(geo_point(0, 500, 100), geo_point(2000, 500, 100), geo_point(1000, 2000, 100));
      vegetation_type arctic_grass = vegetation_type::grass_arctic;
      double hveg{0.12};
      auto t0 = utctime_now();
      vector<double> v{0.0, 1.0, 2.0, 3.0, 4.0, 5.0};
      auto n = v.size();
      timeaxis_t ta1{t0, dt, n};
      using dd::apoint_ts;
      apoint_ts lai(ta1, vector<double>{0.1, 0.7, 2.0, 2.6, 0.6, 0.1}, POINT_AVERAGE_VALUE);
      apoint_ts albedo(ta1, vector<double>{1.0, 0.5, 0.3, 0.1, 0.7, 0.99}, POINT_AVERAGE_VALUE);
      double lai_max{2.3};
      double cleaf_max{50.0};
      double hvegetation{0.6};
      double ks{0.5};
      double soil_moisture_deficit{0.5};
      double sai{2.0};
      auto vegetation = std::make_shared<vegetation_cell_data>(ta1);
      vegetation->set_vegetation_type(arctic_grass);
      vegetation->set_vegetation_properties(lai, albedo, hveg, lai_max, cleaf_max, sai,ks, soil_moisture_deficit);
      gcd.set_vegetation_parameter(vegetation);

      double lai_v = 2.0;
      double albedo_v = 0.5;

      for (int h = 0; h < 24; ++h) {
        t = utc_cal.time(2000, 06, 2, h, 00, 0, 0); // June
        rad.net_radiation_inst(rad_r, lat, t, slope, aspect, temperature[h], rhumidity[h], elevation);
        pm_calculator.evapotranspiration(
          pm_r,
          dt,
          radiation_m[h],
          0,
          temperature[h],
          temperature[h],
          rhumidity[h],
          lai_v,
          albedo_v,
          hvegetation,
          cleaf_max,
          ks,
          soil_moisture_deficit,
          elevation,
          windspeed[h]);
        av_et.add(pm_r.et, h);
      }
      CHECK_EQ(
        av_et.result(),
        doctest::Approx(0.4926).epsilon(0.005)); // this is a little bit more than for full ASCE PM, but acceptable
    }
  }

}
