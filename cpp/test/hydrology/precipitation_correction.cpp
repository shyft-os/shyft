#include <shyft/hydrology/methods/precipitation_correction.h>

#include "test_pch.h"

namespace shyft {
  using namespace std;
  using namespace shyft::core::precipitation_correction;
  using namespace shyft::core;

  TEST_SUITE_BEGIN("hydrology");

  TEST_CASE("hydrology/pc/equal_operator") {
    parameter pc1;
    parameter pc2{99};

    TS_ASSERT(pc1 != pc2);

    pc2.scale_factor = 1.0;

    TS_ASSERT(pc1 == pc2);
  }

  TEST_SUITE_END();

}
