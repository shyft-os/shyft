
#include <shyft/hydrology/methods/hbv_tank.h>

#include "test_pch.h"

namespace shyft {

  using namespace shyft::core;

  TEST_SUITE_BEGIN("hydrology");

  TEST_CASE("hydrology/hbv_tank/parmeter_equal_operator") {
    hbv_tank::parameter p1;
    hbv_tank::parameter p2(p1.uz1 + 1);
    hbv_tank::parameter p3(p1.uz1, p1.uz2 + 1);

    TS_ASSERT(p1 != p2);
    TS_ASSERT(p1 != p3);

    p2.uz1 = p1.uz1;
    p3.uz2 = p1.uz2;

    TS_ASSERT(p1 == p2);
    TS_ASSERT(p1 == p3);
  };

  /** @brief Extra arguments for routine
   *
   * @param from_soil
   * @param precip
   * @param t2m
   */
  struct extra_arguments {
    extra_arguments(double from_soil, double precip, double t2m)
      : from_soil(from_soil)
      , precip(precip)
      , t2m(t2m) {
    }

    double from_soil;
    double precip;
    double t2m;
  };

  /** @brief Do one step of the calculation and check water balance per zone.
   *
   */

  void do_one_step(hbv_tank::state& s, hbv_tank::response& r, hbv_tank::calculator calc, extra_arguments extra_args) {
    auto s_init = s;
    calc.step(s, r, extra_args.from_soil, extra_args.precip * calc.lake_fraction, extra_args.t2m);


    // water balance upper zone
    CHECK_EQ(doctest::Approx(s.uz), s_init.uz + extra_args.from_soil - r.perc_effective - r.quz());
    // water balance lower zone
    CHECK_EQ(
      doctest::Approx(s.lz), s_init.lz + r.perc_effective + extra_args.precip * calc.lake_fraction - r.elake - r.qlz);
  };

  hbv_tank::parameter p_def(15, 50, 1, 1, 1, 1, 1, 0.17 / 24., 1.1);
  hbv_tank::parameter p_def2(15, 50, 0.05, 0.1, 0.5, 0.5, 0.04, 0.17 / 24., 1.1);

  TEST_CASE("hydrology/hbv_tank/uz_lower_uz1_and_scaling_facators_are_one") {
    hbv_tank::parameter p = p_def;
    hbv_tank::state s(1.2, 2.5);
    hbv_tank::state s_init = s;

    hbv_tank::response r;
    double lake_fraction = 0;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(2, 0, -1);

    do_one_step(s, r, calc, extra_args);
    CHECK_EQ(r.q(), doctest::Approx(s_init.uz + s_init.lz + extra_args.from_soil));
  };

  TEST_CASE("hydrology/hbv_tank/no_from_soil_no_evap") {
    hbv_tank::parameter p = p_def;
    hbv_tank::state s(0, 1.123);
    hbv_tank::state s_init = s;

    hbv_tank::response r;
    double lake_fraction = 0;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(0, 0, -1);

    do_one_step(s, r, calc, extra_args);
    CHECK_EQ(r.q(), doctest::Approx(s_init.lz));
  };

  TEST_CASE("hydrology/hbv_tank/upper_tank_without_walls") {
    hbv_tank::state s(20, 1);
    hbv_tank::state s_init = s;

    hbv_tank::parameter p = p_def;
    p.uz1 = s.uz + 1;

    hbv_tank::response r;
    double lake_fraction = 0;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(0, 0, -1);

    do_one_step(s, r, calc, extra_args);
    CHECK_EQ(r.quz(), doctest::Approx(s_init.uz));
  };

  TEST_CASE("hydrology/hbv_tank/lower_tank_large_evaporation") {
    hbv_tank::state s(0, 10);
    hbv_tank::state s_init = s;

    hbv_tank::parameter p(0, 0, 0, 0, 0, 0, 1, 0.17 / 24., 1e6);

    hbv_tank::response r;
    double lake_fraction = 0.5;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(0, 0, -1);

    do_one_step(s, r, calc, extra_args);
    CHECK_EQ(r.quz(), doctest::Approx(0));
    CHECK_EQ(r.qlz + r.elake, doctest::Approx(s_init.lz));
  };

  TEST_CASE("hydrology/hbv_tank/charge_from_soil_no_evap") {
    // q should approach from_soil after many runs (for large from_soil, no evap)
    double uz, from_soil, expected_output;
    SUBCASE("case1") {
      uz = 100;
      from_soil = 50;
    }
    SUBCASE("case2") {
      uz = 15;
      from_soil = 10;
    }
    SUBCASE("case3") {
      uz = 1;
      from_soil = 5;
    }

    CAPTURE(uz);
    CAPTURE(from_soil);
    CAPTURE(expected_output);


    hbv_tank::state s(uz, 10);
    hbv_tank::parameter p = p_def2;

    hbv_tank::response r;
    double lake_fraction = 0.0;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(from_soil, 0, -1);

    int n_steps = 200;
    for (int i = 0; i < n_steps; ++i) {
      do_one_step(s, r, calc, extra_args);
    }
    CHECK_EQ(r.q(), doctest::Approx(from_soil));
  };

  TEST_CASE("hydrology/hbv_tank/evap_from_lake_no_precip") {
    // elake positive when lake fraction and temperature > 0 (default).
    double air_temp, lake_fraction;
    SUBCASE("case1") {
      air_temp = 10;
      lake_fraction = 1;
    }
    SUBCASE("case2") {
      air_temp = 0;
      lake_fraction = 1;
    }
    SUBCASE("case3") {
      air_temp = 10;
      lake_fraction = 0;
    }
    SUBCASE("case4") {
      air_temp = -5;
      lake_fraction = 0.3;
    }
    SUBCASE("case5") {
      air_temp = 5;
      lake_fraction = 0.5;
    }

    CAPTURE(air_temp);
    CAPTURE(lake_fraction);

    hbv_tank::state s(10, 10);

    hbv_tank::parameter p = p_def2;

    hbv_tank::response r;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(0, 0, air_temp);

    do_one_step(s, r, calc, extra_args);

    if (lake_fraction > 0 && air_temp > 0) {
      CHECK_GT(r.elake, 0);
    } else {
      CHECK_EQ(r.elake, doctest::Approx(0));
    }
  };

  TEST_CASE("hydrology/hbv_tank/evap_from_lake_with_precip") {
    // lz increasing per time step when precip on lake
    double precip, lake_fraction;
    SUBCASE("case1") {
      precip = 5;
      lake_fraction = 0.1;
    }
    SUBCASE("case2") {
      precip = 10;
      lake_fraction = 0.5;
    }
    SUBCASE("case3") {
      precip = 20;
      lake_fraction = 0.8;
    }

    CAPTURE(precip);
    CAPTURE(lake_fraction);

    hbv_tank::state s(10, 10);
    auto s_init = s;

    hbv_tank::parameter p = p_def2;

    hbv_tank::response r;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(10, precip, 10);

    do_one_step(s, r, calc, extra_args);

    CHECK_GT(s.lz, doctest::Approx(s_init.lz));
  };

  TEST_CASE("hydrology/hbv_tank/run_dry") {
    // 1) q should be decreasing per time step until zero when no water input.
    // 2) q should go to zero after many runs.
    double air_temp, lake_fraction;
    SUBCASE("case1") {
      air_temp = -5;
      lake_fraction = 0;
    }
    SUBCASE("case2") {
      air_temp = -5;
      lake_fraction = 0.3;
    }
    SUBCASE("case3") {
      air_temp = -5;
      lake_fraction = 1;
    }
    SUBCASE("case4") {
      air_temp = 10;
      lake_fraction = 0;
    }
    SUBCASE("case5") {
      air_temp = 10;
      lake_fraction = 0.3;
    }
    SUBCASE("case6") {
      air_temp = 10;
      lake_fraction = 1;
    }

    CAPTURE(air_temp);
    CAPTURE(lake_fraction);

    hbv_tank::state s(1, 1);
    // auto s_init = s;

    hbv_tank::parameter p = p_def2;

    hbv_tank::response r;
    hbv_tank::calculator calc(p, lake_fraction);
    extra_arguments extra_args(0, 0, air_temp);

    int n_steps = 200;
    double q_last;
    for (int i = 0; i < n_steps; ++i) {
      do_one_step(s, r, calc, extra_args);
      if (i > 0) {
        CHECK_GE(q_last, r.q());
      }
      q_last = r.q();
    }
    CHECK_LE(r.q(), 1e-4); // arbirary choise of "approaches to zero"
  };

  TEST_SUITE_END();

}
