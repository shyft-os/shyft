#include <chrono>
#include <cmath>
#include <random>
#include <tuple>
#include <vector>

#include <boost/math/constants/constants.hpp>

#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/hydro_functions.h>
#include <shyft/hydrology/methods/free_water_evaporation.h>
#include <shyft/hydrology/methods/radiation.h>
#include <shyft/time/utctime_utilities.h>

#include "test_pch.h"

#ifdef FREEWATER_WORK_IN_PROGRESS_DONE
namespace shyft::test {

  class trapezoidal_average {
   private:
    double area = 0.0;
    double f_a = 0.0;
    ;                     // Left hand side of next integration subinterval
    double t_start = 0.0; // Start of integration period
    double t_a = 0.0;     // Left hand side time of next integration subinterval
   public:
    explicit trapezoidal_average() {
    }

    /** \brief initialize must be called to reset states before being used during ode integration.
     */
    void initialize(double f0, double t_start) {
      this->f_a = f0;
      this->t_start = t_start;
      t_a = t_start;
      area = 0.0;
    }

    /** \brief Add contribution to average using a simple trapezoidal rule
     *
     * See: http://en.wikipedia.org/wiki/Numerical_integration
     */
    void add(double f, double t) {
      area += 0.5 * (f_a + f) * (t - t_a);
      f_a = f;
      t_a = t;
    }

    double result() const {
      return area / (t_a - t_start);
    }
  };

}

TEST_SUITE("free_water_evaporation") {
  using namespace shyft::core;
  //    using shyft::core::radiation::surface_normal;
  using shyft::core::calendar;
  using shyft::core::utctime;
  using namespace shyft::test;
  // test basics: creation, etc
}

TEST_CASE("hourly_free_water_evaporation") {

  //===========================//
  // getting radiation  //
  radiation::parameter rad_p;
  radiation::response rad_r;
  rad_p.albedo = 0.2;
  rad_p.turbidity = 1.0;
  radiation::calculator rad(rad_p);
  calendar utc_cal;
  // Greeley, Colorado weather station
  double lat = 40.41;
  double elevation = 1462.4;
  // double ht = 1.68;
  // double hws = 3.0;
  utctime t;
  // checking for horizontal surface Eugene, OR, p.64, fig.1b
  arma::vec surface_normal({0.0, 0.0, 1.0});
  double slope = 0.0;
  double aspect = 0.0;
  utctime ta;

  ta = utc_cal.time(2000, 06, 2, 00, 00, 0, 0);
  // rad.psw_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);

  // evapotranspiraiton PM
  shyft::core::free_water_evaporation::response fwe_r;
  shyft::core::free_water_evaporation::calculator<shyft::core::free_water_evaporation::response> fwe_calculator();
  trapezoidal_average av_et;
  av_et.initialize(fwe_r.et, 0.0);
  // ref.: ASCE=EWRI Appendix C: Example Calculation of ET
  double temperature[23] = {16.5, 15.4, 15.5, 13.5, 13.2, 16.2, 20.0, 22.9, 26.4, 28.2, 29.8, 30.9,
                            31.8, 32.5, 32.9, 32.4, 30.2, 30.6, 28.3, 25.9, 23.9, 20.1, 19.9};
  double vap_pressure[23] = {1.26, 1.34, 1.31, 1.26, 1.24, 1.31, 1.36, 1.39, 1.25, 1.17, 1.03, 1.02,
                             0.98, 0.87, 0.86, 0.93, 1.14, 1.27, 1.27, 1.17, 1.20, 1.10, 1.05};
  double windspeed[23] = {0.5,  1.0,   0.68, 0.69, 0.29, 1.24, 1.28, 0.88, 0.72, 1.52, 1.97, 2.07,
                          2.76, 2.990, 3.10, 2.77, 3.41, 2.78, 2.95, 3.27, 2.86, 2.7,  2.0};
  double radiation_m[23] = {0.0,  0.0,  0.0,  0.0,  0.03, 0.46, 1.09, 1.74, 2.34, 2.84, 3.25,
                            3.21, 3.34, 2.96, 2.25, 1.35, 0.88, 0.79, 0.27, 0.03, 0.0,  0.0};
  double svp[23];
  double rhumidity[23];
  for (int i = 0; i < 23; i++) {
    svp[i] = hydro_functions::svp(temperature[i]);
    // std::cout<<"svp: "<<svp[i]<<std::endl; // matched
    rhumidity[i] = svp[i] * 100 / vap_pressure[i];
  }
  auto dt = shyft::core::deltahours(1);

  rad.net_radiation_inst(rad_r, lat, ta, slope, aspect, temperature[0], rhumidity[0], elevation);

  double albedo_v = 0.5;
  double lake_area = 1; // in sqr km

  for (int h = 1; h < 24; ++h) {
    t = utc_cal.time(2000, 06, 2, h, 00, 0, 0); // June
    rad.net_radiation_inst(rad_r, lat, t, slope, aspect, temperature[h - 1], rhumidity[h - 1], elevation);
    fwe_calculator.evaporation(
      fwe_r,
      dt,
      radiation_m[h - 1],
      0,
      temperature[h - 1],
      temperature[h - 1],
      rhumidity[h - 1],
      lake_area,
      albedo_v,
      elevation,
      windspeed[h - 1]);
    av_et.add(fwe_r.et, h);
  }
  std::cout << "et: " << av_et.result() << std::endl;
  CHECK_EQ(av_et.result(), doctest::Approx(-0.067).epsilon(0.005));
}
}
#endif