#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/time_series/dd/traverse.h>
#include <shyft/time_series/time_series_dd.h>

#include "test_pch.h"

namespace shyft::time_series::dd {

  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/traverse") {

    constexpr auto url0 = "test://a/b/c0", url1 = "test://a/b/c1", url2 = "test://a/b/c2", url3 = "test://c3",
                   url_invalid = "test://none";

    std::shared_ptr<ipoint_ts const> ts0{
      new gpoint_ts{time_axis::generic_dt{time_axis::fixed_dt{0, 1, 5}}, 2.0}
    },
      ts1{new aref_ts{url0}}, ts2{(apoint_ts{ts0} + apoint_ts{ts1}).ts};

    using events = std::vector<std::tuple<traverse_event_tag, std::shared_ptr<ipoint_ts const>>>;
    using enum traverse_event_tag;
    auto trace = [&](auto &&root_ts) {
      events e;
      traverse_ts<std::monostate>(root_ts, [&](auto event) {
        if constexpr (event.tag == done) {
          e.push_back({event.tag, nullptr});
          return traverse_ret<std::monostate>{};
        } else {
          e.push_back({event.tag, *event.ts});
          return traverse_cont{};
        }
      });
      return e;
    };

    for (auto ts : std::vector{ts0, ts1}) {
      events expected{
        { pre,      ts},
        {post,      ts},
        {done, nullptr}
      };
      {
        auto actual = trace(ts);
        CHECK(actual == expected);
      }
      {
        auto actual = trace(&ts);
        CHECK(actual == expected);
      }
      {
        auto actual = trace(&std::as_const(ts));
        CHECK(actual == expected);
      }
    }

    {
      auto actual = trace(ts2);
      events expected{
        { pre,     ts2},
        { pre,     ts1},
        {post,     ts1},
        { pre,     ts0},
        {post,     ts0},
        {post,     ts2},
        {done, nullptr}
      };
      CHECK(actual == expected);
    }
  }

  TEST_SUITE_END();

}
