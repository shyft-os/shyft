#include <cstdint>
#include <ranges>
#include <vector>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include "test_pch.h"

namespace shyft::time_series::dd {

  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/inside/upper_and_lower_half") {

    core::utctime const t0(0);
    std::size_t const n = 3;
    std::vector<core::utctime> time_points{};
    for (auto t : std::views::iota(std::size_t(0), n))
      time_points.push_back(core::utctime(t));

    time_axis::fixed_dt ta(t0, core::utctime{1l}, n);

    apoint_ts ts(ta, std::vector{-2.0, 0.0, 2.0});

    auto uhm = upper_half_mask(ts), lhm = lower_half_mask(ts), uh = upper_half(ts), lh = lower_half(ts);

    apoint_ts uhm_expected(ta, std::vector{0.0, 1.0, 1.0}), lhm_expected(ta, std::vector{1.0, 1.0, 0.0}),
      uh_expected(ta, std::vector{0.0, 0.0, 2.0}), lh_expected(ta, std::vector{-2.0, 0.0, 0.0});

    CHECK_EQ(uhm, uhm_expected);
    CHECK_EQ(lhm, lhm_expected);
    CHECK_EQ(uh, uh_expected);
    CHECK_EQ(lh, lh_expected);
  }

  TEST_SUITE_END();

}
