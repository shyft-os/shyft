#include <shyft/time_series/dd/apoint_ts.h>

#include "test_pch.h"

using shyft::time_series::dd::apoint_ts;

using ta_ = shyft::time_series::dd::gta_t;
using shyft::core::utctime;
using shyft::core::calendar;
using shyft::core::deltahours;
using shyft::time_series::ts_point_fx;
using std::vector;
using v_ = vector<double>;
using t_ = vector<utctime>;
using shyft::core::no_utctime;

using shyft::time_series::dd::extend_ts_fill_policy;
using shyft::time_series::dd::extend_ts_split_policy;
using std::isfinite;

static inline utctime t(double x) {
  return shyft::core::from_seconds(x);
};

// clang format makes this file messy,
// clang-format off
namespace {
  void is_expected_result(apoint_ts const &ts, ta_ const &expect_ta, v_ const &expect_v) {
    CHECK_EQ(ts.time_axis(), expect_ta);
    auto v = ts.values();
    CHECK_EQ(v.size(), expect_v.size());
    for (size_t i = 0; i < v.size()&& i<expect_v.size(); ++i) {
      if (isfinite(expect_v[i])) {
        CHECK_EQ(v[i], doctest::Approx(expect_v[i])); // by values computation
        CHECK_EQ(ts.value(i), doctest::Approx(expect_v[i]));// by index access to value
        CHECK_EQ(ts(ts.time(i)), doctest::Approx(expect_v[i]));// by time-access to same value
      } else {
        CHECK_EQ(isfinite(v[i]), false);
        CHECK_EQ(isfinite(ts.value(i)), false);
        CHECK_EQ(isfinite(ts(ts.time(i))), false);
      }
    }
  }

  /** test to verify results, given two time-series a and b, formulated using different type of time-axis */
  void verify_case_with_gap(apoint_ts const &a, apoint_ts const &b) {
    // ts_extend_gap_lhs_last
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, shyft::nan, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 1, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 9.0, 3, 4});
    }
    // ts_extend_gap_rhs_first
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, shyft::nan, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 1, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 9.0, 3, 4});
    }
    // ts_extend_gap_value
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(3), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, shyft::nan, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(3), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 1, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(3), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 9.0, 3, 4});
    }
    // ts_extend_t_value_at_end_of_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(2), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, shyft::nan, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(2), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1, 1, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(2), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4), t(5)}},v_{0, 1.0, 9.0, 3, 4});
    }
    // ts_extend_t_value_inside_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(1.9), shyft::nan),
        ta_{t_{t(0), t(1), t(3), t(4), t(5)}},v_{0, 1, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(1.9), shyft::nan),
        ta_{t_{t(0), t(1), t(3), t(4), t(5)}},v_{0, 1, 3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(1.9), 9.0),
        ta_{t_{t(0), t(1), t(3), t(4), t(5)}},v_{0, 1.0, 3, 4});
    }
    // ts_extend_t_value_before_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(-1.0), shyft::nan),
        ta_{t_{t(3), t(4), t(5)}},v_{3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(-1.0), shyft::nan),
        ta_{t_{t(3), t(4), t(5)}},v_{3, 4});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(-1.0), 9.0),
        ta_{t_{ t(3), t(4), t(5)}},v_{3, 4});
    }
  }

  void verify_case_with_overlap(apoint_ts const &a, apoint_ts const &b) {
    // ts_extend_overlap_lhs_last
    {
      // apoint_ts a(ta_{t_{t(0),t(1),t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
      // apoint_ts b(ta_{t_{t(1),t(2),t(3)}},v_{1.1,2.1},ts_point_fx::POINT_AVERAGE_VALUE);
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}}, v_{0, 1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1, 2.1});
    }
    // ts_extend_overlap_rhs_first
    {
      // apoint_ts a(ta_{t_{t(0),t(1),t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
      // apoint_ts b(ta_{t_{t(1),t(2),t(3)}},v_{1.1,2.1},ts_point_fx::POINT_AVERAGE_VALUE);
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1.1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}}, v_{0, 1.1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1.1, 2.1});
    }
    // ts_extend_overlap_t_value_at_end_of_lhs
    {
      // apoint_ts a(ta_{t_{t(0),t(1),t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
      // apoint_ts b(ta_{t_{t(1),t(2),t(3)}},v_{1.1,2.1},ts_point_fx::POINT_AVERAGE_VALUE);
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(2), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(2), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}}, v_{0, 1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(2), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3)}}, v_{0, 1, 2.1});
    }
    // ts_extend_overlap_t_value_inside_lhs
    {
      // apoint_ts a(ta_{t_{t(0),t(1),t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
      // apoint_ts b(ta_{t_{t(1),t(2),t(3)}},v_{1.1,2.1},ts_point_fx::POINT_AVERAGE_VALUE);
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(1.9), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1.1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(1.9), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1.1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(1.9), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3)}},v_{0, 1.1, 2.1});
    }
    // ts_extend_t_value_before_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(-1.0), shyft::nan),
        ta_{t_{t(1), t(2), t(3)}},v_{1.1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(-1.0), shyft::nan),
        ta_{t_{t(1), t(2), t(3)}},v_{1.1, 2.1});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(-1.0), 9.0),
        ta_{t_{ t(1), t(2), t(3)}},v_{1.1, 2.1});
    }
  }

  void verify_case_exact_extend(apoint_ts const &a, apoint_ts const &b) {
    // ts_extend_lhs_last
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
    }
    // ts_extend_rhs_first
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
    }
    // ts_extend_value
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(3), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, shyft::nan, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(3), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 1, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(3), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 9, 3});
    }
    // ts_extend_no_gap_t_value_at_end_of_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(2), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(2), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(2), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
    }
    // ts_extend_no_gap_t_value_inside_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(1.9), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(1.9), shyft::nan),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(1.9), 9.0),
        ta_{t_{t(0), t(1), t(2), t(3), t(4)}},v_{0, 1, 2, 3});
    }
    // ts_extend_t_value_before_lhs
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(-1.0), shyft::nan),
        ta_{t_{t(2), t(3), t(4)}},v_{2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(-1.0), shyft::nan),
        ta_{t_{t(2), t(3), t(4)}},v_{2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(-1.0), 9.0),
        ta_{t_{t(2), t(3), t(4)}},v_{2, 3});
    }
  }

  void verify_case_interior_extend(apoint_ts const &a, apoint_ts const &b) {
    // ts_extend_lhs_last
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2.0), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(2.0), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(2.0), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
    }
    // ts_extend_rhs_first
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_LAST, no_utctime, shyft::nan),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_RHS_FIRST, extend_ts_fill_policy::EPF_FILL, no_utctime, 9.0),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
    }
    // ts_extend_value
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(1.4), shyft::nan),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(1.4), shyft::nan),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(1.4), 9.0),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
    }
    // ts_extend_value_plus_e
    {
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_NAN, t(1.6), shyft::nan),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_LAST, t(1.6), shyft::nan),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
      is_expected_result(
        a.extend(b, extend_ts_split_policy::EPS_VALUE, extend_ts_fill_policy::EPF_FILL, t(1.6), 9.0),
        ta_{t_{t(0), t(1), t(1.5), t(2.5), t(3.5)}},v_{0, 1, 2, 3});
    }
  }

}



TEST_SUITE_BEGIN("ts-extend");

TEST_CASE("ts/extend_overlap_p_p/issue_1241/a") {
  apoint_ts a(ta_{t_{t(0), t(3)}},v_{1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(1), t(4)}},v_{2},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_RHS_FIRST,
                    extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(4)}},v_{1.0,2.0},ts_point_fx::POINT_AVERAGE_VALUE);
  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/issue_1241/b") {
  apoint_ts a(ta_{t_{t(0), t(3)}},v_{1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{shyft::core::min_utctime, shyft::core::max_utctime}},v_{2},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_LHS_LAST,
                    extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan).evaluate();
  apoint_ts e(ta_{t_{t(0),t(3),shyft::core::max_utctime}},v_{1.0,2.0},ts_point_fx::POINT_AVERAGE_VALUE);
  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/issue_1241/c") {
  apoint_ts a(ta_{t_{t(0), t(3)}},v_{1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{shyft::core::min_utctime, shyft::core::max_utctime}},v_{2},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c1=b.extend(a,
                    extend_ts_split_policy::EPS_LHS_LAST,
                    extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan).evaluate();
  apoint_ts e1(b);
  CHECK_EQ(c1,e1);
  auto c2=b.extend(a,
                    extend_ts_split_policy::EPS_RHS_FIRST,
                    extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan).evaluate();
  apoint_ts e2(ta_{t_{shyft::core::min_utctime,t(0),t(3)}},v_{2.0,1.0},ts_point_fx::POINT_AVERAGE_VALUE);
  CHECK_EQ(c2,e2);
}

TEST_CASE("ts/extend_overlap_p_p/issue_1241/d") {
  apoint_ts a(ta_{t_{t(0), t(3)}},v_{1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(5), t(6)}},v_{2},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(4),0.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(3),t(5),t(6)}},v_{1.0,0.0,2.0},ts_point_fx::POINT_AVERAGE_VALUE);
  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/issue_1241/e") {
  apoint_ts a(ta_{t_{t(0),t(1), t(4)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(3), t(6)}},v_{3},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(2),2.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(3),t(6)}},v_{0.0,1.0,3.0},ts_point_fx::POINT_AVERAGE_VALUE);
  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/user_spec_unaligned/a") {
  apoint_ts a(ta_{t_{t(0),t(1), t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(0), t(1),t(3)}},v_{0.1,1.1},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(1.9),2.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(3)}},v_{0.0,1.1},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/user_spec_unaligned/b") {
  apoint_ts a(ta_{t_{t(0),t(1), t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(0), t(1.1),t(3)}},v_{0.1,1.1},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(1.9),2.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(1.1),t(3)}},v_{0.0,1.0,1.1},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/user_spec_unaligned/c") {
  apoint_ts a(ta_{t_{t(0),t(1.1), t(2)}},v_{0,1},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(0),t(1),      t(3)}},v_{0.1,1.1},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(1.9),2.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1.1),t(2),t(3)}},v_{0.0,1.0,1.1},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/user_spec_unaligned/d") {
  apoint_ts a(ta_{t_{t(0),  t(2)}},v_{0},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{   t(1),   t(4)}},v_{1.0},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(3),2.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(2),t(4)}},v_{0.0,1.0},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}

TEST_CASE("ts/extend_join_p_p/user_spec_aligned/a") {
  apoint_ts a(ta_{t_{t(0),  t(1)}},v_{0},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{   t(1), t(2),t(4)}},v_{1.0,2.0},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(2),3.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(2),t(4)}},v_{0.0,3.0,2.0},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/user_spec_unaligned/e") {
  apoint_ts a(ta_{t_{t(0),t(1),t(2),t(4)}},v_{0,1,2},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(0),t(0.5),t(3),t(5)}},v_{0.1,0.5,3.1},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(1.5),-1.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(2),t(3),t(5)}},v_{0.0,1.0,0.5,3.1},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}
TEST_CASE("ts/extend_overlap_p_p/user_spec_unaligned/f") {
  apoint_ts a(ta_{t_{t(0),t(1),t(2),t(4)}},v_{0,1,2},ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t_{t(0),t(0.5),t(1.6),t(5)}},v_{0.1,0.5,1.6},ts_point_fx::POINT_AVERAGE_VALUE);
  auto c=a.extend(b,
                    extend_ts_split_policy::EPS_VALUE,
                    extend_ts_fill_policy::EPF_FILL,t(1.5),-1.0).evaluate();
  apoint_ts e(ta_{t_{t(0),t(1),t(1.6),t(5)}},v_{0.0,1.0,1.6},ts_point_fx::POINT_AVERAGE_VALUE);

  CHECK_EQ(c,e);
}
//--- calendar
auto ta_c=[] (utctime t,utctime dt, size_t n) {
  ta_ r;
  r.impl=shyft::time_axis::calendar_dt{calendar::utc(),t,dt,n};
  return r;
};
TEST_CASE("ts/extend_gap/permutations") {
  apoint_ts a;
  apoint_ts b;
  SUBCASE("c_") {
    a=apoint_ts(ta_c(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(3), t(1), 2), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(3), t(1), 2), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(3), t(4), t(5)}), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("f_") {
    a=apoint_ts(ta_(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(3), t(1), 2), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(3), t(1), 2), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(3), t(4), t(5)}), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("p_") {
    a=apoint_ts(ta_( t_{t(0), t(1), t(2)}), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(3), t(1), 2), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(3), t(1), 2), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(3), t(4), t(5)}), v_{3.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  verify_case_with_gap(a, b);
}
TEST_CASE("ts/extend_exact/permutations") {
  apoint_ts a;
  apoint_ts b;
  SUBCASE("c_") {
    a=apoint_ts(ta_c(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(2), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(2), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(2), t(3), t(4)}), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("f_") {
    a=apoint_ts(ta_(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(2), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(2), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(2), t(3), t(4)}), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("p_") {
    a=apoint_ts(ta_(t_{t(0), t(1), t(2)}), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(2), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(2), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(2), t(3), t(4)}), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  verify_case_exact_extend(a, b);
}
TEST_CASE("ts/extend_overlap/permutations") {
  apoint_ts a;
  apoint_ts b;
  SUBCASE("c_") {
    a=apoint_ts(ta_c(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(1), t(1), 2), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
#if 1
    SUBCASE("f") {
      b=apoint_ts(ta_(t(1), t(1), 2), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(1), t(2), t(3)}), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
#endif
  }
#if 1
  SUBCASE("f_") {
    a=apoint_ts(ta_(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(1), t(1), 2), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(1), t(1), 2), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(1), t(2), t(3)}), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("p_") {
    a=apoint_ts(ta_(t_{t(0), t(1), t(2)}), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(1), t(1), 2), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(1), t(1), 2), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(1), t(2), t(3)}), v_{1.1, 2.1}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
#endif
  verify_case_with_overlap(a, b);
}

TEST_CASE("ts/extend_interior/permutations") {
  apoint_ts a;
  apoint_ts b;
  SUBCASE("c_") {
    a=apoint_ts(ta_c(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(1.5), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(1.5), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(1.5), t(2.5), t(3.5)}), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("f_") {
    a=apoint_ts(ta_(t(0), t(1), 2), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(1.5), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(1.5), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(1.5), t(2.5), t(3.5)}), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  SUBCASE("p_") {
    a=apoint_ts(ta_(t_{t(0), t(1), t(2)}), v_{0, 1}, ts_point_fx::POINT_AVERAGE_VALUE);
    SUBCASE("c") {
      b=apoint_ts(ta_c(t(1.5), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("f") {
      b=apoint_ts(ta_(t(1.5), t(1), 2), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
    SUBCASE("p") {
      b=apoint_ts(ta_(t_{t(1.5), t(2.5), t(3.5)}), v_{2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
    }
  }
  verify_case_interior_extend(a, b);
}
//---

TEST_CASE("ts/extend_null_series_case") {
  //yes could happen, ref issue #876
  apoint_ts a(ta_{no_utctime, t(0), 0}, v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts b(ta_{t(3), t(1), 3}, v_{4, 5, 6}, ts_point_fx::POINT_AVERAGE_VALUE);
  auto e1 = a.extend(b, extend_ts_split_policy::EPS_LHS_LAST, extend_ts_fill_policy::EPF_NAN, no_utctime, shyft::nan);
  auto e = e1.evaluate();
  FAST_REQUIRE_EQ(e1.size(), e.size());   // evaluated and instant should be same size
  for (size_t i = 0; i < e.size(); ++i) { // and values..
    CHECK_EQ(e.time(i), e1.time(i));
    FAST_CHECK_UNARY(shyft::core::nan_equal(e.value(i), e1.value(i)));
  }
}

TEST_CASE("ts/extend/f_f/empty") {
  apoint_ts a(ta_{t(0), t(1), 3}, v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_{no_utctime, t(0), 0}, v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  // a bit more difficult,
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());

  xe=a.slice(0,1);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());
}

TEST_CASE("ts/extend/f_c/empty") {
  auto utc=calendar::utc();
  apoint_ts a(ta_{t(0), t(1), 3}, v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_c(no_utctime, t(0), 0), v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());
}
TEST_CASE("ts/extend/c_c/empty") {
  apoint_ts a(ta_c(t(0), calendar::DAY, 3), v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_c(no_utctime, t(0), 0), v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,calendar::DAY,shyft::nan),xe.time_axis(),xe.values());
  xe=a.slice(0,1);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,calendar::DAY,shyft::nan),xe.time_axis(),xe.values());

}
TEST_CASE("ts/extend/c_f/empty") {
  apoint_ts a(ta_c(t(0), calendar::DAY, 3), v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_{no_utctime, t(0), 0}, v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,calendar::DAY,shyft::nan),xe.time_axis(),xe.values());

}
TEST_CASE("ts/extend/p_p/empty") {
  apoint_ts a(ta_{t_{t(0), t(1), t(3)},t(4)}, v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_{t_{}, no_utctime}, v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());
  xe=a.slice(0,1);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());
}
TEST_CASE("ts/extend/p_f/empty") {
  apoint_ts a(ta_{t_{t(0), t(1), t(3)},t(4)}, v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_{no_utctime, t(0), 0}, v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());
}
TEST_CASE("ts/extend/p_c/empty") {
  apoint_ts a(ta_{t_{t(0), t(1), t(3)},t(4)}, v_{1, 2, 3}, ts_point_fx::POINT_AVERAGE_VALUE);
  apoint_ts e(ta_c(no_utctime, t(0), 0), v_{}, ts_point_fx::POINT_AVERAGE_VALUE);
  is_expected_result(a.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),a.time_axis(),a.values());
  is_expected_result(e.extend(e,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_LAST,no_utctime,shyft::nan),e.time_axis(),e.values());
  auto xe=a.slice(1,2);
  is_expected_result(e.extend(a,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_LAST,t(1),shyft::nan),xe.time_axis(),xe.values());
}
TEST_SUITE_END();
