#pragma once
#include <algorithm>
#include <cmath>
#include <concepts>
#include <ranges>
#include <thread>

#include <shyft/core/fs_compat.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <doctest/doctest.h>

// Require you to include test_pch.h first.

namespace shyft::test::utils {

  class temp_dir {
    fs::path p;
   public:
    temp_dir(char const *tmp_pth = "shyft.") {
      p = fs::temp_directory_path()
        / (std::string(tmp_pth) + std::to_string(shyft::core::utctime_now().count())
           + std::to_string(std::hash<std::thread::id>()(std::this_thread::get_id())));
    }

    operator fs::path() const {
      return p;
    }

    fs::path operator/(std::string c) const {
      return p / c;
    }

    fs::path operator/(char const *c) const {
      return p / std::string(c);
    }

    fs::path operator/(fs::path const &c) const {
      return p / c;
    }

    std::string string() const {
      return p.string();
    }

    ~temp_dir() {
#ifdef _WIN32
      for (int i = 0, n = 10; i < n; ++i) {
        if (i > 0)
          MESSAGE("Unable to remove " << p.string() << ", retrying " << i << "/" << n - 1);
        std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(200));
        try {
          fs::remove_all(p);
          if (i > 0)
            MESSAGE("Successfully removed " << p.string());
          return;
        } catch (...) {
        }
      }
      // TODO: CHECK_MESSAGE(false, "Failed to remove " << p.string());
      WARN_MESSAGE(false, "Failed to remove " << p.string());
#else
      fs::remove_all(p);
#endif
    }
  };

  /**
   * @brief A lambda function that repeatedly invokes a user-provided functor until a specified timeout is reached or
   * the functor evaluates to true.
   *
   * This function allows executing a functor periodically while checking the current time against
   * a given timeout. The process continues until the functor returns `true` or the timeout is exceeded.
   * In case of an exception thrown by the functor, the function immediately returns `false`.
   *
   * @param fx The user-provided callable object (functor) that will be invoked periodically during
   *        the wait period. The functor must return a value convertible to `bool`.
   * @param max_wait The duration in UTC time to wait before timing out.
   * @param poll_delay The duration to wait before next invocation
   * @return Returns `true` if the callable object evaluates to `true` within the timeout period.
   *         Returns `false` if the timeout is exceeded or an exception is thrown by the functor.
   */
  constexpr auto wait_until =
    [](
      std::invocable auto &&fx,
      core::utctime const max_wait = core::from_seconds(10),
      core::utctime const poll_delay = std::chrono::milliseconds(1)) {
      auto const t_exit = core::utctime_now() + max_wait;
      while (core::utctime_now() < t_exit) {
        try {
          if (std::invoke(std::forward<decltype(fx)>(fx)))
            return true;
        } catch (std::exception &) {
          return false;
        }
        std::this_thread::sleep_for(poll_delay);
      }
      return false;
    };

  /*
   * Value compare two apoint_ts using doctest::Approx, with provided precision (defaults to 1/1000)
   */
  inline bool approx_equal(time_series::dd::apoint_ts &lhs, time_series::dd::apoint_ts &rhs, double precision = 0.001) {
    if (lhs.ts.get() == rhs.ts.get())
      return true;
    if (!lhs.ts || !rhs.ts)
      return false;
    if (lhs.ts->point_interpretation() != rhs.ts->point_interpretation())
      return false;
    if (lhs.ts->time_axis() != rhs.ts->time_axis())
      return false;
    return std::ranges::all_of(std::views::zip(lhs.values(), rhs.values()), [&](auto p) {
      auto [v, v_expected] = p;
      if (!std::isfinite(v) && !std::isfinite(v_expected))
        return true;
      return v == doctest::Approx(v_expected).epsilon(precision);
    });
  }
}
