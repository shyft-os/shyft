#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/time_axis_extend.h>

namespace shyft::time_axis {
  namespace {

    inline bool aligned_time_axis(fixed_dt const &a, fixed_dt const &b) {
      return a.dt == b.dt && a.t == b.t + ((a.t - b.t) / b.dt) * b.dt;
    }

    inline bool aligned_time_axis(calendar_dt const &a, calendar_dt const &b) {
      if (a.cal->get_tz_name() != b.cal->get_tz_name() || a.dt != b.dt)
        return false;
      utctimespan remainder;
      (void) a.cal->diff_units(a.total_period().start, b.total_period().end, a.dt, remainder);
      return remainder == utctimespan{0l};
    }

    inline bool consecutive_period(utcperiod const &sa, utcperiod const &sb) {
      return sa.empty() || sb.empty() || sa.end == sb.start;
    }

    generic_dt as_generic(fixed_dt const &base, size_t skip, size_t steps) {
      return generic_dt(fixed_dt(base.t + skip * base.dt, base.dt, steps));
    }

    generic_dt as_generic(calendar_dt const &base, size_t skip, size_t steps) {
      return generic_dt(calendar_dt{base.cal, base.cal->add(base.t, base.dt, skip), base.dt, steps});
    }

    generic_dt as_generic(point_dt const &base, size_t skip, size_t steps) {
      auto it_begin = base.t.cbegin();
      std::advance(it_begin, skip);
      auto it_end = it_begin;
      std::advance(it_end, steps);
      utctime end_time = it_end != base.t.cend() ? *it_end : base.t_end;
      return generic_dt(point_dt(std::vector<core::utctime>(it_begin, it_end), end_time));
    }

    /**
     * @brief create time-axis from a and b, and its computed extend info
     * @details
     * The returned time-axis is of simplest possible type
     * regarding what is passed in.
     * For  empty cases, the TA::null_range()  type is returned
     * In cases where only one time-axis contributes, a slice of the
     * same type is returned.
     * For cases with two contributions, a point_dt is create that
     * are according to the defined semantics for merge.
     *
     * @tparam TA time-axis type for `a`
     * @tparam TB time-axis type for `b`
     * @param a time-axis
     * @param b time-axis
     * @param ei extend info as computed by compute_extend_info(a,b,split_at)
     */
    template <class TA, class TB>
    auto create_time_axis_extend(const TA &a, const TB &b, extend_info const &ei) -> generic_dt {
      if (ei.empty())
        return generic_dt{TA::null_range()};

      if (!ei.b.contributes())
        return as_generic(a, 0, ei.a.n);

      if (!ei.a.contributes())
        return as_generic(b, ei.b.i, ei.b.n - ei.b.i);

      std::vector<utctime> points;
      points.reserve(ei.size() + 1u); // 1u for the end of interval

      for (auto i = 0u; i < ei.a.n; ++i) // a
        points.push_back(a._time(i));    // value[i] = a._value(i)

      if (ei.t_fill != no_utctime)
        points.push_back(ei.t_fill);

      auto b_start = ei.b.i;
      if (ei.b.t != no_utctime) {
        points.push_back(ei.b.t);
        ++b_start;
      }
      for (auto i = b_start; i < ei.b.n; ++i)
        points.push_back(b._time(i));
      points.push_back(b.total_period().end); // ok for constructing time-axis?
      return generic_dt{point_dt(std::move(points))};
    }

    template <class TA, class TB>
    auto extend(const TA &a, const TB &b, utctime const split_at) -> generic_dt {
      return create_time_axis_extend(a, b, compute_extend_info(a, b, split_at));
    }

    template <class TA>
    generic_dt extend_algo(TA const &a, TA const &b, utctime const split_at) {
      auto ei = compute_extend_info(a, b, split_at);
      if (!ei.a.contributes() || !ei.b.contributes())
        return create_time_axis_extend(a, b, ei);

      //
      // This is the only section of the algo that is specific to calendar//fixed
      // the rationale for it is to ensure that if we can keep an efficient
      // calendar//fixed repr, we do that, rather than the easy
      // convert to point.
      // We can keep the time-axis type, given that they are
      // 1. aligned (time-stamps follow same frequency, alignment etc.)
      // 2. consecutive (the first end perfectly where the other starts)
      //
      //

      utcperiod const span_a(a.total_period().start, ei.a.n < a.size() ? a.time(ei.a.n) : a.total_period().end);
      utcperiod const span_b(b.time(ei.b.i), b.total_period().end);
      // equivalent calendars, aligned dt, and consecutive
      if (aligned_time_axis(a, b) && consecutive_period(span_a, span_b))
        return generic_dt(a.slice(0, ei.size())); // notice we use at least the ei.size() here
                                                  //  so time-axis and values are same size
                                                  //  but still possible different partitions

      // fallback to the generic algo that results in a point_dt
      return create_time_axis_extend(a, b, compute_extend_info(a, b, split_at));
    }

    inline generic_dt extend(calendar_dt const &a, calendar_dt const &b, utctime const split_at) {
      return extend_algo(a, b, split_at);
    }

    inline generic_dt extend(fixed_dt const &a, fixed_dt const &b, utctime const split_at) {
      return extend_algo(a, b, split_at);
    }
  }

  /**
   * @brief Extend time-axis `a` with time-axis `b` given `split_at` time
   *
   * @details
   *
   * This algorithm is used in the 'bind phase' of ts.extend,
   *
   * @note
   *
   * Still some improvements to be done, ensuring
   * the partitioning of time-axis for fixed/calendar are
   * exactly as for compute_extend_info.
   * Currently the sizes are matched up,
   * and the criteria for when it should
   * be applied are really narrowed to a minimum,
   * but its not bullet proof, - yet.
   *
   * @param a  Time-axis to extend.
   * @param b  Time-axis to extend.
   * @param split_at  Time-point to split between `a` and `b`.
   */
  generic_dt extend(generic_dt const &a, generic_dt const &b, utctime const split_at) {
    return std::visit(
      [&](auto const &a, auto const &b) {
        return extend(a, b, split_at);
      },
      a.impl,
      b.impl);
  }
}
