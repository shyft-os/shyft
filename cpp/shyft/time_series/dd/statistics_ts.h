/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/fx_statistics.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /**
   * @brief The statistics_ts is provides statistics over a time-axis for a  ts
   *
   * @details
   * Given a source ts, and a time-axis, compute the statistical statistics_property
   * for each of the periods in time-axis.
   *
   */
  struct statistics_ts : ipoint_ts {
    ipoint_ts_ref ts; ///< the source ts
    gta_t ta;         ///< the time-axis that we use to collect/partition statistical information into
    int64_t p{50};    ///< the statistical parameter [0..100] MIN_EXTREME,MAX_EXTREME,AVERAGE

    // std copy ct and assign
    statistics_ts() = default;

    statistics_ts(apoint_ts const & ats, gta_t const & ta, int64_t p);
    statistics_ts(apoint_ts&& ats, gta_t const & ta, int64_t p);

    statistics_ts(ipoint_ts_ref ts, gta_t ta, int64_t p)
      : ts{std::move(ts)}
      , ta{std::move(ta)}
      , p{p} {
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx pfx) override {
      if (ts)
        dref(ts).set_point_interpretation(pfx);
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      assert_bound();
      return ta;
    }

    [[nodiscard]] utcperiod total_period() const override {
      assert_bound();
      return ta.total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      assert_bound();
      return ta.index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      assert_bound();
      return ta.size();
    }

    [[nodiscard]] utctime time(size_t i) const override {
      assert_bound();
      return ta.time(i);
    };

    // methods that needs special implementation according to qac rules
    [[nodiscard]] double value(size_t i) const override;
    [[nodiscard]] double value_at(utctime t) const override;
    [[nodiscard]] vector<double> values() const override;

    // methods for binding and symbolic ts
    [[nodiscard]] bool needs_bind() const override;
    void do_bind() override;
    void do_unbind() override;
    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
   protected:
    void assert_bound() const {
      if (ts && ts->needs_bind())
        throw runtime_error("statistical_ts:attempt to use method on unbound ts");
    }

    void check_percentile_range_or_throw();
  };

}

x_poly_serialize_export_key(shyft::time_series::dd::statistics_ts)
