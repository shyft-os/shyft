/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /**
   * @brief repeat ts  repeats the first period of ts over a larger time-axis
   * @details
   * Given a source time-series ts, and a time-axis ta,
   * The resulting ts have time_axis total period equal to ta,
   * where the time-points in each period of the time-axis ta,
   * is equal to replica of the time-points of the ts.time_axis()
   * The values of the new time-series is equal to the corresponding
   * points in the source ts.
   *
   * Typical use: Repeat a metered temperature  season -max mask over several years.
   *
   */
  struct repeat_ts : ipoint_ts {
    ipoint_ts_ref ts;  ///< the time-series to repeat
    gta_t rta;         ///< the repeat time-axis, with periods like year etc.
    gta_t ta;          ///< the resulting time-axis from using ts.time_axis repeated over rta-periods
    bool bound{false}; ///< flag to keep track of

    repeat_ts() = default;
    //-- useful ct goes here
    repeat_ts(apoint_ts const & ats, gta_t const & rta);
    repeat_ts(apoint_ts&& ats, gta_t&& rta);

    repeat_ts(ipoint_ts_ref  ts, gta_t rta)
      : ts{std::move(ts)}
      , rta{std::move(rta)} {
      do_early_bind();
    }

    repeat_ts(ipoint_ts_ref ts, gta_t rta, gta_t ta, bool bound) // support expr. serialization
      : ts{std::move(ts)}
      , rta{std::move(rta)}
      , ta{std::move(ta)}
      , bound{bound} {
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx pfx) override {
      if (ts)
        dref(ts).set_point_interpretation(pfx);
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      assert_bound();
      return ta;
    }

    [[nodiscard]] utcperiod total_period() const override {
      assert_bound();
      return ta.total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      assert_bound();
      return ta.index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      assert_bound();
      return ta.size();
    }

    [[nodiscard]] utctime time(size_t i) const override {
      assert_bound();
      return ta.time(i);
    };

    // methods that needs special implementation according to qac rules
    [[nodiscard]]  double value(size_t i) const override;
    [[nodiscard]]  double value_at(utctime t) const override;
    [[nodiscard]] vector<double> values() const override;

    // methods for binding and symbolic ts
    [[nodiscard]] bool needs_bind() const override;
    void do_bind() override;
    void do_unbind() override;
    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
   protected:
    void assert_bound() const {
      if (!bound)
        throw runtime_error("repeat_ts:attemt to use method on unbound ts");
    }

    void do_early_bind() {
      if (ts && !ts->needs_bind())
        local_do_bind();
    }

    void local_do_bind();
    void local_do_unbind();
  };

}

x_poly_serialize_export_key(shyft::time_series::dd::repeat_ts)
