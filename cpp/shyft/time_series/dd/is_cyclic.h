#pragma once
#include <memory>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/container/flat_set.hpp>

#include <shyft/core/reflection.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/functional.h>
#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/time_series/dd/traverse.h>

namespace shyft::time_series::dd {

  /**
   * @brief check if a timeseries expression contains a cycle
   *
   * @param resolver function to resolve symbolic references.
   * @param root_ts root expression
   */
  bool is_cyclic(resolver auto &&resolver, apoint_ts const &root_ts) {
    boost::container::flat_set<void *> seen;
    return traverse_ts<bool>(root_ts.ts, [&](auto &&event) {
      using node = traverse_node_t<const std::shared_ptr<const ipoint_ts> &>;
      using enum traverse_event_tag;
      using event_type = std::decay_t<decltype(event)>;
      if constexpr (event_type::tag == pre) {
        {
          auto addr = (void *) event.ts->get();
          auto [itr, emplaced] = seen.emplace(addr);
          if (!emplaced)
            return any_traverse_ret<bool, node>(true);
        }
        if (auto id = resolve_id(*event.ts)) {
          auto resolved_ts = resolver(*event.ts, *id);
          if (!resolved_ts)
            return any_traverse_skip<bool, node>;
          return any_traverse_jump<bool, node>(resolved_ts);
        }
        return any_traverse_cont<bool, node>;
      } else if constexpr (event_type::tag == post) {
        auto addr = (void *) event.ts->get();
        seen.erase(addr);
        return traverse_cont{};
      } else
        return traverse_ret<bool>{false};
    });
  }

}
