/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/gpoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api

  /**
   * @brief a symbolic reference ts
   * @details
   * Represents a terminal, can be a symbolic reference to a concrete terminal (type gpoint_ts),
   * that can be bound to the ref ts later on.
   * Typically expressions are created using aref_ts (symbolic references, in form of an url),
   * that is later resolved by a service (e.g. the DtsService) as part of the bind/execute
   * expression operation.
   * Once bound, it behaves like it's representation (gpoint_ts).
   */
  struct aref_ts : ipoint_ts {
    using ref_ts_t = shared_ptr<gpoint_ts const>; // shyft::time_series::ref_ts<gts_t> ref_ts_t;
    ref_ts_t rep;
    string id;

    explicit aref_ts(string sym_ref)
      : id{std::move(sym_ref)} {
    }

    aref_ts(string sym_ref, ref_ts_t r)
      : rep{std::move(r)}
      , id{std::move(sym_ref)} {
    }

    aref_ts() = default; // default for serialization conv

    // implement ipoint_ts contract:
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      return rep->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx point_interpretation) override {
      if (rep)
        dref(rep).set_point_interpretation(point_interpretation);
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      return rep->time_axis();
    }

    [[nodiscard]] utcperiod total_period() const override {
      return rep->total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      return rep->index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      return rep->size();
    }

    [[nodiscard]] utctime time(size_t i) const override {
      return rep->time(i);
    };

    [[nodiscard]] double value(size_t i) const override {
      return rep->value(i);
    }

    [[nodiscard]] double value_at(utctime t) const override {
      return rep->value_at(t);
    }

    [[nodiscard]] vector<double> values() const override {
      return rep->values();
    }

    // implement some extra functions to manipulate the points
    void set(size_t i, double x) {
      dref(rep).set(i, x);
    }

    void fill(double x) {
      dref(rep).fill(x);
    }

    void scale_by(double x) {
      dref(rep).scale_by(x);
    }

    [[nodiscard]] bool needs_bind() const override {
      return rep == nullptr;
    }

    void do_bind() override;
    void do_unbind() override;

    [[nodiscard]] gts_t const & core_ts() const {
      if (rep)
        return rep->core_ts();
      throw runtime_error("Attempt to use unbound ref_ts");
    }

    [[nodiscard]] gts_t & core_ts() {
      if (rep)
        return std::const_pointer_cast<gpoint_ts>(rep)->core_ts();
      throw runtime_error("Attempt to use unbound ref_ts");
    }

    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
  };

  template <class Archive>
  void serialize_save_no_tracking(Archive &oa, std::vector<aref_ts> const &r) {
    oa << static_cast<std::uint64_t>(r.size());
    for (auto const &r_ts : r) {
      oa << r_ts.id << (r_ts.rep ? true : false);
      if (r_ts.rep)
        oa << r_ts.rep->rep.fx_policy << r_ts.rep->rep.ta << r_ts.rep->rep.v; // non-track version
    }
  }

  template <class Archive>
  void serialize_load_no_tracking(Archive &ia, std::vector<aref_ts> &r) {
    std::uint64_t n{0};
    ia >> n;
    r.clear();
    r.reserve(n);
    for (std::uint64_t i = 0; i < n; ++i) {
      bool ts_exist{false};
      std::string id;
      ia >> id >> ts_exist;
      if (ts_exist) {
        gpoint_ts gts;
        ia >> gts.rep.fx_policy >> gts.rep.ta >> gts.rep.v;
        r.emplace_back(id, std::make_shared<gpoint_ts const>(std::move(gts)));
      } else {
        r.emplace_back(id);
      }
    }
  }
}

x_poly_serialize_export_key(shyft::time_series::dd::aref_ts)
