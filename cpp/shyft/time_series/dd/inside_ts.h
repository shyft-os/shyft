/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /**
   * @brief inside ts function parameters
   * @details
   * Ref to parameter definitions
   *  [ min_x .. max_x >
   *
   */
  struct inside_parameter {
    double min_x{shyft::nan}; ///< x < min_x  -> nan
    double max_x{shyft::nan}; ///< x > max_x  -> nan
    double nan_x{shyft::nan}; ///< if x is nan the inside is this value
    double x_inside{1.0};     ///< result if value x is inside
    double x_outside{0.0};    ///< result if value x is outside

    // inside_parameter()=default;
    /** computes the inside value based on x is inside range [min_x .. max_x> agains min-max is set */
    [[nodiscard]] double inside_value(double const & x) const noexcept {
      if (!isfinite(x))
        return nan_x;
      if (isfinite(min_x) && x < min_x)
        return x_outside;
      if (isfinite(max_x) && x >= max_x)
        return x_outside;
      return x_inside;
    }

    [[nodiscard]] bool equal(inside_parameter const & o, double abs_e = 1e-9) const {
      return nan_equal(min_x, o.min_x, abs_e) && nan_equal(max_x, o.max_x, abs_e) && nan_equal(nan_x, o.nan_x, abs_e)
          && nan_equal(x_inside, o.x_inside, abs_e) && nan_equal(x_outside, o.x_outside, abs_e);
    }

    // binary serialization, so no x_serialize_decl();
  };

  /**
   * @brief The inside_ts maps a value range into 1.0 and 0.0
   *
   * @details
   * The inside_ts provide needed function to transform the source time-series
   * into a sequence of is_inside_value and is_outside_value
   * based on a range-criteria [min .. max >
   */
  struct inside_ts : ipoint_ts {
    ipoint_ts_ref ts;   ///< the source ts
    inside_parameter p; ///< the parameters that control how the inside is done

    // useful constructors

    explicit inside_ts(apoint_ts const & ats)
      : ts{ats.ts} {
    }

    explicit inside_ts(apoint_ts&& ats)
      : ts{std::move(ats.ts)} {
    }

    inside_ts(apoint_ts const & ats, inside_parameter const & qp)
      : ts{ats.ts}
      , p{qp} {
    }

    // std copy ct and assign
    inside_ts() = default;

    void assert_ts() const {
      if (!ts)
        throw runtime_error("inside_ts:source ts is null");
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      assert_ts();
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx pfx) override {
      assert_ts();
      dref(ts).set_point_interpretation(pfx);
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      assert_ts();
      return ts->time_axis();
    }

    [[nodiscard]] utcperiod total_period() const override {
      assert_ts();
      return ts->time_axis().total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      assert_ts();
      return ts->index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      return ts ? ts->size() : 0;
    }

    [[nodiscard]] utctime time(size_t i) const override {
      assert_ts();
      return ts->time(i);
    };

    // methods that needs special implementation according to inside rules
    [[nodiscard]]  double value(size_t i) const override;
    [[nodiscard]]  double value_at(utctime t) const override;
    [[nodiscard]] vector<double> values() const override;

    // methods for binding and symbolic ts
    [[nodiscard]] bool needs_bind() const override {
      return ts && ts->needs_bind();
    }

    void do_bind() override {
      if (ts)
        dref(ts).do_bind();
    }

    void do_unbind() override {
      if (ts)
        dref(ts).do_unbind();
    }

    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_binary(shyft::time_series::dd::inside_parameter)
x_poly_serialize_export_key(shyft::time_series::dd::inside_ts)
