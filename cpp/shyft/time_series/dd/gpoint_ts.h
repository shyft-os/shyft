/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api

  /**
   * @brief gpoint_ts a generic concrete point_ts, a terminal, not an expression
   *
   * @details
   * The gpoint_ts is typically provided by repositories, that reads time-series
   * from some provider, like database, file(netcdf etc), providing a set of values
   * that are aligned to the specified time-axis.
   *
   */
  struct gpoint_ts : ipoint_ts {
    gts_t rep{};

    // To create gpoint_ts, we use const ref, move ct wherever possible:
    explicit gpoint_ts(gts_t&& x)
      : rep{std::move(x)} {
    }

    // note (we would normally use ct template here, but we are aiming at exposing to python)
    gpoint_ts(gta_t const & ta, double fill_value, ts_point_fx point_fx = POINT_INSTANT_VALUE)
      : rep{ta, fill_value, point_fx} {
    }

    gpoint_ts(gta_t const & ta, vector<double> const & v, ts_point_fx point_fx = POINT_INSTANT_VALUE)
      : rep(ta, v, point_fx) {
    }

    gpoint_ts(gta_t&& ta, double fill_value, ts_point_fx point_fx = POINT_INSTANT_VALUE)
      : rep{std::move(ta), fill_value, point_fx} {
    }

    gpoint_ts(gta_t&& ta, vector<double>&& v, ts_point_fx point_fx = POINT_INSTANT_VALUE)
      : rep{std::move(ta), std::move(v), point_fx} {
    }

    gpoint_ts(gta_t const & ta, vector<double>&& v, ts_point_fx point_fx = POINT_INSTANT_VALUE)
      : rep{ta, std::move(v), point_fx} {
    }

    // now for the gpoint_ts itself, constructors incl. move
    gpoint_ts() = default; // default for serialization conv

    // implement ipoint_ts contract:
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      return rep.point_interpretation();
    }

    void set_point_interpretation(ts_point_fx point_interpretation) override {
      rep.set_point_interpretation(point_interpretation);
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      return rep.time_axis();
    }

    [[nodiscard]] utcperiod total_period() const override {
      return rep.total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      return rep.index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      return rep.size();
    }

    [[nodiscard]] utctime time(size_t i) const override {
      return rep.time(i);
    };

    [[nodiscard]] double value(size_t i) const override {
      return rep.v[i];
    }

    [[nodiscard]] double value_at(utctime t) const override {
      return rep(t);
    }

    [[nodiscard]] vector<double> values() const override {
      return rep.v;
    }

    // implement some extra functions to manipulate the points
    void set(size_t i, double x) {
      rep.set(i, x);
    }

    void fill(double x) {
      rep.fill(x);
    }

    void scale_by(double x) {
      rep.scale_by(x);
    }

    [[nodiscard]] gpoint_ts slice(int i0, int n) const {
      return gpoint_ts{rep.slice(i0, n)};
    }

    [[nodiscard]] bool needs_bind() const override {
      return false;
    }

    void do_bind() override {
    }

    void do_unbind() override {
    }

    gts_t& core_ts() {
      return rep;
    }

    [[nodiscard]] gts_t const & core_ts() const {
      return rep;
    }

    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
  };

  /** const cast shared_ptr to a mutable reference */
  inline gpoint_ts& dref(shared_ptr<gpoint_ts const> const & ts) {
    return *const_cast<gpoint_ts*>(ts.get());
  }

  inline auto ensure_gpoint(std::shared_ptr<ipoint_ts const> const & its) -> std::shared_ptr<gpoint_ts const> {
    if (!its || its->needs_bind())
      return nullptr;
    if (auto gts = std::dynamic_pointer_cast<gpoint_ts const>(its))
      return gts;
    eval_ctx ctx;
    its->prepare(ctx);
    return std::dynamic_pointer_cast<gpoint_ts const>(its->evaluate(ctx, its));
  }

  template <class Archive>
  void serialize_save_no_tracking(Archive& oa, std::vector<std::shared_ptr<gpoint_ts const>> const & r) {
    oa << static_cast<std::uint64_t>(r.size());
    for (auto const & r_ts : r) {
      oa << (r_ts ? true : false);
      if (r_ts)
        oa << r_ts->rep.fx_policy << r_ts->rep.ta << r_ts->rep.v; // non-track version
    }
  }

  template <class Archive>
  void serialize_load_no_tracking(Archive& ia, std::vector<std::shared_ptr<gpoint_ts const>>& r) {
    std::uint64_t n{0};
    ia >> n;
    r.clear();
    r.reserve(n);
    for (std::uint64_t i = 0; i < n; ++i) {
      bool ts_exist{false};
      ia >> ts_exist;
      if (ts_exist) {
        gpoint_ts gts;
        ia >> gts.rep.fx_policy >> gts.rep.ta >> gts.rep.v;
        r.emplace_back(std::make_shared<gpoint_ts const>(std::move(gts)));
      } else
        r.emplace_back(nullptr);
    }
  }

}

x_poly_serialize_export_key(shyft::time_series::dd::gpoint_ts)
