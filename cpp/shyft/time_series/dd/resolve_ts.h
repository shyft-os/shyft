#pragma once
#include <memory>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/optional.h>
#include <shyft/core/reflection.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/functional.h>
#include <shyft/time_series/dd/traverse.h>

namespace shyft::time_series::dd {

  /**
   * @brief resolver concept
   * @details
   * The resolver takes a  ref to a shared ptr const ipoint_ts, and a symbol,
   * and should return the resolved ts for the symbol, that can be a nullptr, terminal or expression that needs more
   * resolve.
   * @tparam R the invocable type
   */
  template <typename R>
  concept resolver =
    std::is_invocable_v<R, std::shared_ptr<ipoint_ts const> const &, std::string_view>
    && std::is_same_v<
      std::remove_cvref_t<std::invoke_result_t<R, std::shared_ptr<ipoint_ts const> const &, std::string_view>>,
      std::shared_ptr<ipoint_ts const>>;

  template <typename T>
  constexpr bool needs_resolve_v = std::is_same_v<std::remove_cvref_t<T>, aref_ts>;

  inline auto resolve_id(std::shared_ptr<ipoint_ts const> const &ts) {
    return with_operation(
      ts,
      []<typename T>(T const &t) {
        if constexpr (needs_resolve_v<T>)
          return just(std::string_view{t.id});
        return none<std::string_view>;
      },
      [] {
        return none<std::string_view>;
      });
  }

  /**
   *  @brief Substitute symbolic time-series with concrete time-series
   *  @details
   *  Iteratively tries to substitute symbolic time-series with concrete time-series.
   *  Is parametrized on the specific resolution method. The @p resolver is passed a
   *  time-series id and the pointer to the symbolic time-series, and returns either
   *  - A different, resolved, time-series. The returned series will also be resolved.
   *  - The same time-series. The time-series will then be skipped.
   *  - A nullptr, in which case the resolution algorithm will fail.
   *  @param resolver ref concept `resolver`, invocable `shared_ptr<const ipoint_ts>( shared_ptr<const ipoint_ts>
   * &ts,string_view symbol)`
   *  @param root_ts
   *  @returns whether or not resolution was successful
   */
  bool resolve_ts(resolver auto &&resolver, apoint_ts &root_ts) {
    return traverse_ts<bool>(&root_ts.ts, [&](auto &&event) {
      using node = traverse_node_t<std::shared_ptr<const ipoint_ts> *>;
      using enum traverse_event_tag;
      using event_type=std::decay_t<decltype(event)>;
      if constexpr (event_type::tag == pre) {
        if (auto id = resolve_id(*event.ts)) {
          auto &&resolved_ts = resolver(*event.ts, *id);
          if (!resolved_ts)
            return any_traverse_ret<bool, node>(false);
          else if (resolved_ts != *event.ts) {
            *event.ts = SHYFT_FWD(resolved_ts);
            return any_traverse_spin<bool, node>;
          }
        }
        return any_traverse_cont<bool, node>;
      } else if constexpr (event_type::tag == post)
        return traverse_cont{};
      else
        return traverse_ret<bool>{true};
    });
  }

}
