/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api

  /**
   * @brief Extend for ts.extend(ts).
   *
   */
  struct extend_ts : ipoint_ts {

    apoint_ts lhs;
    apoint_ts rhs;
    extend_ts_split_policy ets_split_p{EPS_LHS_LAST};
    utctime split_at{no_utctime};
    extend_ts_fill_policy ets_fill_p{EPF_NAN};
    double fill_value{shyft::nan};

    gta_t ta;
    ts_point_fx fx_policy{POINT_AVERAGE_VALUE}; // how f(t) are mapped to t

    bool bound{false};

    [[nodiscard]] ts_point_fx point_interpretation() const override {
      return fx_policy;
    }

    void set_point_interpretation(ts_point_fx x) override {
      fx_policy = x;
    }

    void local_do_bind() {
      if (!bound) {
        fx_policy = result_policy(lhs.point_interpretation(), rhs.point_interpretation());
        ta = time_axis::extend(lhs.time_axis(), rhs.time_axis(), get_split_at());
        bound = true;
      }
    }

    void local_do_unbind() {
      if (bound)
        bound = false;
    }

    [[nodiscard]] bool args_needs_bind() const {
      return lhs.needs_bind() || rhs.needs_bind();
    }

    extend_ts() = default;

    extend_ts(
      apoint_ts lhs,
      apoint_ts rhs,
      extend_ts_split_policy split_policy,
      extend_ts_fill_policy fill_policy,
      utctime split_at,
      double fill_value)
      : lhs{std::move(lhs)}
      , rhs{std::move(rhs)}
      , ets_split_p{split_policy}
      , split_at{split_at}
      , ets_fill_p{fill_policy}
      , fill_value{fill_value} {
      if (!args_needs_bind())
        local_do_bind();
    }

    [[nodiscard]] utctime get_split_at() const {
      switch (this->ets_split_p) {
      default:
      case EPS_LHS_LAST:
        return this->lhs.total_period().end;
      case EPS_RHS_FIRST:
        return this->rhs.total_period().start;
      case EPS_VALUE:
        return this->split_at;
      }
    }

    void bind_check() const {
      if (!bound)
        throw runtime_error("attempting to use unbound timeseries, context abin_op_ts");
    }

    [[nodiscard]] utcperiod total_period() const override {
      return time_axis().total_period();
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      bind_check();
      return ta;
    }; // combine lhs,rhs

    [[nodiscard]] size_t index_of(utctime t) const override {
      return time_axis().index_of(t);
    };

    [[nodiscard]] size_t size() const override {
      return time_axis().size();
    }; // use the combined ta.size();

    [[nodiscard]] utctime time(size_t i) const override {
      return time_axis().time(i);
    }; // return combined ta.time(i)

    /** Get ta value at time. */
    [[nodiscard]] double value_at(utctime t) const override;

    /** Get ts value at point no. */
    [[nodiscard]] double value(size_t i) const override;

    /** Collect all values for the extended ts. */
    [[nodiscard]] vector<double> values() const override;

    [[nodiscard]] bool needs_bind() const override {
      return args_needs_bind();
    }

    void do_bind() override {
      lhs.do_bind();
      rhs.do_bind();
      local_do_bind();
    }

    void do_unbind() override {
      lhs.do_unbind();
      rhs.do_unbind();
      local_do_unbind();
    }

    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
  };

}

x_poly_serialize_export_key(shyft::time_series::dd::extend_ts)