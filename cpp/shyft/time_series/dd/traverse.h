#pragma once
#include <cstdint>
#include <memory>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

#include <boost/mp11/algorithm.hpp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/reflection.h>
#include <shyft/core/utility.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/functional.h>

namespace shyft::time_series::dd {

  template <typename R>
  using traverse_node_t = std::conditional_t<std::is_pointer_v<std::remove_reference_t<R>>,
                                             std::remove_reference_t<R>, std::remove_cvref_t<R>>;

#define SHYFT_TIME_SERIES_TRAVERSE_EVENTS (pre, post, done)
  SHYFT_DEFINE_ENUM(traverse_event_tag, std::uint8_t, SHYFT_TIME_SERIES_TRAVERSE_EVENTS);

  template <traverse_event_tag E, typename... T>
  struct traverse_event {
    static constexpr traverse_event_tag tag = E;

    auto operator<=>(traverse_event const &) const = default;
  };

  template <traverse_event_tag E, typename N>
  requires(E == traverse_event_tag::pre || E == traverse_event_tag::post)
  struct traverse_event<E, N> {
    static constexpr traverse_event_tag tag = E;

    using P = std::conditional_t<std::is_pointer_v<N>, N, N *>;
    P ts;

    auto operator<=>(traverse_event const &) const = default;
  };

#define SHYFT_LAMBDA(r, data, elem) using traverse_##elem = traverse_event<traverse_event_tag::elem>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST((done)))
#undef SHYFT_LAMBDA
#define SHYFT_LAMBDA(r, data, elem) \
  template <typename T> \
  using traverse_##elem = traverse_event<traverse_event_tag::elem, T>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST((pre, post)))
#undef SHYFT_LAMBDA

  namespace detail {
    template <traverse_event_tag E, typename... T>
    struct traverse_event_t_impl {
      using type = traverse_event<E>;
    };

    template <traverse_event_tag E, typename N>
    requires(E == traverse_event_tag::pre || E == traverse_event_tag::post)
    struct traverse_event_t_impl<E, N> {
      using type = traverse_event<E, N>;
    };
  }

  template <traverse_event_tag E, typename N>
  using traverse_event_t = typename detail::traverse_event_t_impl<E, N>::type;

  namespace detail {
    template <typename N>
    struct any_traverse_event_impl {
      template <traverse_event_tag E>
      using fn = traverse_event_t<E, N>;
      using type = boost::mp11::mp_apply<std::variant, tagged_types_t<traverse_event_tag, fn>>;
    };
  }

  template <typename N>
  using any_traverse_event = typename detail::any_traverse_event_impl<N>::type;
  template <typename R>
  using any_traverse_event_t = any_traverse_event<traverse_node_t<R>>;

#define SHYFT_TIME_SERIES_TRAVERSE_CMDS (cont, ret, jump, skip, spin)
  SHYFT_DEFINE_ENUM(traverse_cmd_tag, std::uint8_t, SHYFT_TIME_SERIES_TRAVERSE_CMDS);

  template <traverse_cmd_tag C, typename...>
  struct traverse_cmd {
    static const traverse_cmd_tag tag = C;
  };

  template <typename T>
  struct traverse_cmd<traverse_cmd_tag::ret, T> {
    static const traverse_cmd_tag tag = traverse_cmd_tag::ret;
    T result;
  };

  template <typename N>
  struct traverse_cmd<traverse_cmd_tag::jump, N> {
    static const traverse_cmd_tag tag = traverse_cmd_tag::ret;

    N ts;
  };

#define SHYFT_LAMBDA(r, data, elem) using traverse_##elem = traverse_cmd<traverse_cmd_tag::elem>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST((cont, skip, spin)))
#undef SHYFT_LAMBDA
#define SHYFT_LAMBDA(r, data, elem) \
  template <typename X> \
  using traverse_##elem = traverse_cmd<traverse_cmd_tag::elem, X>;
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST((ret, jump)))
#undef SHYFT_LAMBDA

  template <typename T>
  inline constexpr bool is_traverse_cmd_v = false;
  template <traverse_cmd_tag C, typename... U>
  inline constexpr bool is_traverse_cmd_v<traverse_cmd<C, U...>> = true;

  namespace detail {
    template <traverse_cmd_tag C, typename T, typename N>
    struct traverse_cmd_t_impl {
      using type = traverse_cmd<C>;
    };

    template <typename T, typename N>
    struct traverse_cmd_t_impl<traverse_cmd_tag::ret, T, N> {
      using type = traverse_cmd<traverse_cmd_tag::ret, T>;
    };

    template <typename T, typename N>
    struct traverse_cmd_t_impl<traverse_cmd_tag::jump, T, N> {
      using type = traverse_cmd<traverse_cmd_tag::jump, N>;
    };
  }

  template <traverse_cmd_tag C, typename T, typename N>
  using traverse_cmd_t = typename detail::traverse_cmd_t_impl<C, T, N>::type;

  namespace detail {
    template <typename T, typename N>
    struct any_traverse_cmd_impl {
      template <traverse_cmd_tag C>
      using fn = traverse_cmd_t<C, T, N>;
      using type = boost::mp11::mp_apply<std::variant, tagged_types_t<traverse_cmd_tag, fn>>;
    };
  }

  template <typename T, typename N>
  using any_traverse_cmd = typename detail::any_traverse_cmd_impl<T, N>::type;

#define SHYFT_LAMBDA(r, data, elem) \
  template <typename T, typename N> \
  inline constexpr any_traverse_cmd<T, N> any_traverse_##elem{traverse_cmd<traverse_cmd_tag::elem>{}};
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST((cont, skip, spin)))
#undef SHYFT_LAMBDA
#define SHYFT_LAMBDA(r, data, elem) \
  template <typename T, typename N> \
  constexpr any_traverse_cmd<T, N> any_traverse_##elem(auto &&...args) { \
    return {traverse_cmd_t<traverse_cmd_tag::elem, T, N>{SHYFT_FWD(args)...}}; \
  }
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST((ret, jump)))
#undef SHYFT_LAMBDA

  template <typename T, typename N, traverse_cmd_tag C, traverse_cmd_tag V, typename... W>
  constexpr bool is_traverse_cmd(traverse_cmd<V, W...> const &) {
    return C == V;
  }

#define SHYFT_LAMBDA(r, data, elem) \
  template <typename T, typename N, traverse_cmd_tag U, typename... V> \
  constexpr bool is_traverse_##elem(const traverse_cmd<U, V...> &c) { \
    return is_traverse_cmd<T, N, traverse_cmd_tag::elem>(c); \
  }
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_TIME_SERIES_TRAVERSE_CMDS))
#undef SHYFT_LAMBDA

  template <typename T, typename N, traverse_cmd_tag C>
  constexpr bool is_traverse_cmd(any_traverse_cmd<T, N> const &c) {
    return std::visit(
      [](auto const &c) {
        return is_traverse_cmd<T, N, C>(c);
      },
      c);
  }

#define SHYFT_LAMBDA(r, data, elem) \
  template <typename T, typename N> \
  constexpr bool is_traverse_##elem(const any_traverse_cmd<T, N> &c) { \
    return is_traverse_cmd<T, N, traverse_cmd_tag::elem>(c); \
  }
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_TIME_SERIES_TRAVERSE_CMDS))
#undef SHYFT_LAMBDA

  namespace concepts {
    template <typename X, typename T, typename N>
    concept traverse_cmd = is_traverse_cmd_v<X> || std::is_same_v<any_traverse_cmd<T, N>, X>;
  }

  template <typename X, typename T, typename R, typename N = traverse_node_t<R>>
  concept traverse_fn = requires(X &&x) {
#define SHYFT_LAMBDA(r, data, elem) \
    { std::invoke(x, std::declval<traverse_event_t<traverse_event_tag::elem, N>>()) } -> concepts::traverse_cmd<T, N>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_TIME_SERIES_TRAVERSE_EVENTS))
#undef SHYFT_LAMBDA
  };

  template <typename T, typename N>
  constexpr auto &&traverse_jump_ts(traverse_jump<N> &&cmd) {
    return SHYFT_FWD(cmd).ts;
  }

  template <typename T, typename N>
  constexpr auto &&traverse_jump_ts(any_traverse_cmd<T, N> &&cmd) {
    return traverse_jump_ts<T, N>(std::get<traverse_jump<N>>(std::move(cmd)));
  }

  template <typename T, typename N>
  constexpr auto traverse_result(traverse_ret<T> &&cmd) {
    return SHYFT_FWD(cmd).result;
  }

  template <typename T, typename N>
  constexpr auto traverse_result(any_traverse_cmd<T, N> &&cmd) {
    return traverse_result<T, N>(std::get<traverse_ret<T>>(std::move(cmd)));
  }

  template <typename N>
  struct traverse_ctx {
    struct frame {
      N ts;
      bool done = false;
    };

    std::vector<frame> queue;
  };

  template <typename R>
  using traverse_ctx_t = traverse_ctx<traverse_node_t<R>>;

  template <typename T, typename R, traverse_fn<T, R> F, decays_to<traverse_ctx_t<R>> X>
  auto traverse_ts(R &&root_ts, F &&fn, X &&ctx) {

    using N = traverse_node_t<R>;

    auto unwrap = [&](N &ts) -> auto &&{
      if constexpr(std::is_pointer_v<N>)
        return *ts;
      else
        return ts;
    };
    auto wrap = [&](auto &&ts) -> decltype(auto){
      if constexpr(std::is_pointer_v<N>)
        return &ts;
      else
        return SHYFT_FWD(ts);
    };
    auto ptr = [&](auto &&ts){
      if constexpr(std::is_pointer_v<N>)
        return ts;
      else
        return &ts;
    };

    auto try_push = [&](N ts) {
      if constexpr(std::is_pointer_v<N>){
        if(!ts || !*ts)
          return;
      }
      else if (!ts)
        return;
      ctx.queue.push_back({.ts = std::move(ts), .done = false});
    };
    try_push(SHYFT_FWD(root_ts));
    while (!ctx.queue.empty()) {
      auto &&[ts, done] = ctx.queue.back();
      if (std::exchange(done, true)) {
        auto cmd = std::invoke(fn, traverse_post<N>{ptr(ts)});
        if (is_traverse_ret<T, N>(cmd))
          return traverse_result<T, N>(std::move(cmd));
        ctx.queue.pop_back();
      } else {
        auto cmd = std::invoke(fn, traverse_pre<N>{ptr(ts)});
        if (is_traverse_ret<T, N>(cmd))
          return traverse_result<T, N>(std::move(cmd));
        else if (is_traverse_skip<T, N>(cmd))
          continue;
        else if (is_traverse_spin<T, N>(cmd)) {
          done = false;
          continue;
        } else if (is_traverse_jump<T, N>(cmd)) {
          try_push(traverse_jump_ts<T, N>(std::move(cmd)));
          continue;
        }
        with_operation(
          unwrap(ts),
          [&](auto &&ts) {
            for_each_operands(SHYFT_FWD(ts),
                              [&](auto &&ts){
                                try_push(wrap(SHYFT_FWD(ts)));
                              });
          },
          []() {
            throw std::runtime_error("unexpected time_series type");
          });
      }
    }
    return traverse_result<T, N>(std::invoke(fn, traverse_done{}));
  }

  template <typename T, typename R, traverse_fn<T, R> F>
  auto traverse_ts(R &&root_ts, F &&fn) {
    return traverse_ts<T>(SHYFT_FWD(root_ts), SHYFT_FWD(fn), traverse_ctx_t<R>{});
  }

}
