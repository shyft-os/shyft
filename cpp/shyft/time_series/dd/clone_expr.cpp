#include <shyft/time_series/time_series_dd.h>

namespace shyft::time_series::dd {
  using std::shared_ptr;
  using std::runtime_error;
  using std::make_shared;

  shared_ptr<ipoint_ts> gpoint_ts::clone_expr() const {
    return make_shared<gpoint_ts>(*this);
  }

  shared_ptr<ipoint_ts> periodic_ts::clone_expr() const {
    return make_shared<periodic_ts>(*this);
  }

  shared_ptr<ipoint_ts> aref_ts::clone_expr() const {
    return make_shared<aref_ts>(id, rep ? std::dynamic_pointer_cast<gpoint_ts const>(rep->clone_expr()) : nullptr);
  }

  shared_ptr<ipoint_ts> abin_op_ts::clone_expr() const {
    return make_shared<abin_op_ts>(lhs.clone_expr(), op, rhs.clone_expr());
  }

  shared_ptr<ipoint_ts> use_time_axis_from_ts::clone_expr() const {
    return make_shared<use_time_axis_from_ts>(lhs.clone_expr(), rhs.clone_expr());
  }

  shared_ptr<ipoint_ts> extend_ts::clone_expr() const {
    return make_shared<extend_ts>(lhs.clone_expr(), rhs.clone_expr(), ets_split_p, ets_fill_p, split_at, fill_value);
  }

  shared_ptr<ipoint_ts> abin_op_scalar_ts::clone_expr() const {
    return make_shared<abin_op_scalar_ts>(lhs, op, rhs.clone_expr());
  }

  shared_ptr<ipoint_ts> abin_op_ts_scalar::clone_expr() const {
    return make_shared<abin_op_ts_scalar>(lhs.clone_expr(), op, rhs);
  }

  shared_ptr<ipoint_ts> anary_op_ts::clone_expr() const {
    vector<apoint_ts> clone_args;
    clone_args.reserve(args.size());
    for (auto const & a : args)
      clone_args.emplace_back(a.clone_expr());
    return make_shared<anary_op_ts>(clone_args, op, lead_time, fc_interval);
  }

  namespace {
    template <class T>
    shared_ptr<ipoint_ts> safe_clone_expr(T const & ts) {
      if (!ts) {
        return nullptr;
        // throw std::runtime_error("clone_expr: Attempt to clone nullptr");
      }
      return ts->clone_expr();
    }
  }

  shared_ptr<ipoint_ts> average_ts::clone_expr() const {
    return make_shared<average_ts>(ta, safe_clone_expr(ts));
  }

  shared_ptr<ipoint_ts> integral_ts::clone_expr() const {
    return make_shared<integral_ts>(ta, safe_clone_expr(ts));
  }

  shared_ptr<ipoint_ts> derivative_ts::clone_expr() const {
    return make_shared<derivative_ts>(safe_clone_expr(ts), dm);
  }

  shared_ptr<ipoint_ts> accumulate_ts::clone_expr() const {
    return make_shared<accumulate_ts>(ta, safe_clone_expr(ts));
  }

  shared_ptr<ipoint_ts> repeat_ts::clone_expr() const {
    return make_shared<repeat_ts>(safe_clone_expr(ts), rta);
  }

  shared_ptr<ipoint_ts> abs_ts::clone_expr() const {
    return make_shared<abs_ts>(safe_clone_expr(ts));
  }

  shared_ptr<ipoint_ts> time_shift_ts::clone_expr() const {
    return make_shared<time_shift_ts>(safe_clone_expr(ts), dt);
  }

  shared_ptr<ipoint_ts> inside_ts::clone_expr() const {
    return make_shared<inside_ts>(apoint_ts{safe_clone_expr(ts)}, p);
  }

  shared_ptr<ipoint_ts> decode_ts::clone_expr() const {
    return make_shared<decode_ts>(apoint_ts{safe_clone_expr(ts)}, p);
  }

  shared_ptr<ipoint_ts> bucket_ts::clone_expr() const {
    return make_shared<bucket_ts>(apoint_ts{safe_clone_expr(ts)}, p);
  }

  shared_ptr<ipoint_ts> transform_spline_ts::clone_expr() const {
    return make_shared<transform_spline_ts>(safe_clone_expr(ts), p);
  }

  shared_ptr<ipoint_ts> aglacier_melt_ts::clone_expr() const {
    auto temp = gm.temperature ? gm.temperature->clone_expr() : nullptr;
    auto sca_m2 = gm.sca_m2 ? gm.sca_m2->clone_expr() : nullptr;
    return make_shared<aglacier_melt_ts>(apoint_ts{temp}, apoint_ts{sca_m2}, gm.glacier_area_m2, gm.dtf);
  }

  shared_ptr<ipoint_ts> qac_ts::clone_expr() const {
    auto nts = ts ? ts->clone_expr() : nullptr;
    auto ncts = cts ? cts->clone_expr() : nullptr;
    return make_shared<qac_ts>(apoint_ts{nts}, p, apoint_ts{ncts});
  }

  shared_ptr<ipoint_ts> convolve_w_ts::clone_expr() const {
    return make_shared<convolve_w_ts>(ts_impl.ts.clone_expr(), ts_impl.w, ts_impl.policy);
  }

  shared_ptr<ipoint_ts> ice_packing_recession_ts::clone_expr() const {
    return make_shared<ice_packing_recession_ts>(
      flow_ts.clone_expr(), ice_packing_ts.clone_expr(), ipr_param, fx_policy);
  }

  shared_ptr<ipoint_ts> ice_packing_ts::clone_expr() const {
    return make_shared<ice_packing_ts>(ts.temp_ts.clone_expr(), ts.ip_param, ts.ipt_policy);
  }

  shared_ptr<ipoint_ts> rating_curve_ts::clone_expr() const {
    return make_shared<rating_curve_ts>(ts.level_ts.clone_expr(), ts.rc_param);
  }

  shared_ptr<ipoint_ts> krls_interpolation_ts::clone_expr() const {
    return make_shared<krls_interpolation_ts>(ts.clone_expr(), predictor);
  }

  shared_ptr<ipoint_ts> statistics_ts::clone_expr() const {
    return make_shared<statistics_ts>(safe_clone_expr(ts), ta, p);
  }

}
