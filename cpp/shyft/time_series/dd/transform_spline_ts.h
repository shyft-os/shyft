/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/core/math_utilities.h>
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  enum interpolation_scheme : uint8_t { // ensure definite size of the enum to facilitate cross win/linux serialization
    SCHEME_LINEAR,
    SCHEME_POLYNOMIAL,
    SCHEME_CATMULL_ROM,
  };

  struct xy_point_curve {
    using point_t = std::array<double, 2>;
    vector<point_t> points;

    xy_point_curve() = default;
    explicit xy_point_curve(vector<point_t> const & points)
      : points(points){};

    // Contruction from boost::multi_array_ref types, including numpy_boost
    template <typename point_array_t, typename r = typename point_array_t::reference>
    explicit xy_point_curve(const point_array_t point_array) {
      if (point_array.num_dimensions() != 2)
        throw std::runtime_error("Array must be two-dimensional.");
      if (point_array.shape()[1] != 2)
        throw std::runtime_error("Array must have length equal to 2 in second dimension.");

      for (size_t i = 0; i < point_array.shape()[0]; ++i)
        points.push_back(std::array<double, 2>{point_array[i][0], point_array[i][1]});
    };
  };

  /**
   * @brief spline_parameter
   *
   * @details
   * This represents a spline function
   *       knots: Non-decreasing sequence of double
   *       coeff: Coefficient vector for the B-spline basis
   *       degree: The polynomial degree for the B-splines
   *
   *   requirements:
   *       knots is non-decreasing and contains at least 2 distinct values
   *       knots.size() == coeff.size() + degree + 1
   *
   */
  struct spline_parameter {
    vector<double> knots;
    vector<double> coeff;
    size_t degree{0};


    // Offsets of first and last non-empty knot spans
    vector<double>::difference_type first{0};
    vector<double>::difference_type last{0};

    spline_parameter() = default;
    spline_parameter(vector<double> const & knots, vector<double> const & coeff, std::size_t degree);

    bool operator==(spline_parameter const & o) const {
      using shyft::core::nan_equal;
      return first== o.first && last==o.last && nan_equal(knots, o.knots)
          && nan_equal(coeff, o.coeff) && degree == o.degree;
    }

    bool operator!=(spline_parameter const & o) const {
      return !operator==(o);
    }

    // Get the index of the interval containing x
    [[nodiscard]] vector<double>::difference_type get_interval(double x) const;
    x_serialize_decl();
  };

  struct spline_interpolator {
    struct method {
      struct POLYNOMIAL { };

      struct LINEAR { };

      struct CATMULL_ROM { };
    };

    // Select implementation from enum
    static spline_parameter interpolate(xy_point_curve const & xy, interpolation_scheme scheme);

    template <typename method_tag>
    static spline_parameter interpolate(xy_point_curve const & xy, method_tag);
  };

  /**
   * @brief transform_spline_ts maps a time-series values through a spline function.
   *
   *
   */
  struct transform_spline_ts : ipoint_ts {
    ipoint_ts_ref ts;   ///< the source ts
    spline_parameter p; ///< the parameters that control how the evaluation is done

    // useful constructors
    transform_spline_ts(ipoint_ts_ref its, spline_parameter qp)
      : ts{std::move(its)}
      , p{std::move(qp)} {
    }

    transform_spline_ts(apoint_ts ats, spline_parameter qp)
      : ts{std::move(ats.ts)}
      , p{std::move(qp)} {
    }

    // std copy ct and assign
    transform_spline_ts() = default;

    void assert_ts() const {
      if (!ts)
        throw runtime_error("transform_spline_ts:source ts is null");
    }

    // implement ipoint_ts contract, these methods just forward to source ts
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      assert_ts();
      return ts->point_interpretation();
    }

    void set_point_interpretation(ts_point_fx pfx) override {
      assert_ts();
      dref(ts).set_point_interpretation(pfx);
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      assert_ts();
      return ts->time_axis();
    }

    [[nodiscard]] utcperiod total_period() const override {
      assert_ts();
      return ts->time_axis().total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      assert_ts();
      return ts->index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      return ts ? ts->size() : 0;
    }

    [[nodiscard]] utctime time(size_t i) const override {
      assert_ts();
      return ts->time(i);
    }

    // methods that needs special implementation
    [[nodiscard]] double value(size_t i) const override;
    [[nodiscard]] double value_at(utctime t) const override;
    [[nodiscard]] vector<double> values() const override;

    // methods for binding and symbolic ts
    [[nodiscard]] bool needs_bind() const override {
      return ts &&ts->needs_bind();
    }

    void do_bind() override {
      if (ts)
        dref(ts).do_bind();
    }

    void do_unbind() override {
      if (ts)
        dref(ts).do_unbind();
    }

    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::time_series::dd::spline_parameter)
x_poly_serialize_export_key(shyft::time_series::dd::transform_spline_ts)
