/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/convolve_w.h>
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd { // dd= dynamic_dispatch version of the time_series library, aiming at python api
  struct apoint_ts;

  /** @brief convolve_w is used for providing a convolution by weights ts
   *
   * The convolve_w_ts is particularly useful for implementing routing and model
   * time-delays and shape-of hydro-response.
   *
   */
  struct convolve_w_ts : ipoint_ts {
    typedef vector<double> weights_t;
    typedef shyft::time_series::convolve_w_ts<apoint_ts> cnv_ts_t;
    cnv_ts_t ts_impl;

    explicit convolve_w_ts(cnv_ts_t  cnv_ts)
      : ts_impl{std::move(cnv_ts)} {
    }

    convolve_w_ts(apoint_ts const & ats, weights_t const & w, convolve_policy conv_policy)
      : ts_impl{ats, w, conv_policy} {
    }

    convolve_w_ts(apoint_ts&& ats, weights_t const & w, convolve_policy conv_policy)
      : ts_impl{std::move(ats), w, conv_policy} {
    }

    // std.ct
    convolve_w_ts() = default;

    // implement ipoint_ts contract
    [[nodiscard]] ts_point_fx point_interpretation() const override {
      return ts_impl.point_interpretation();
    }

    void set_point_interpretation(ts_point_fx) override {
      throw runtime_error("not implemented");
    }

    [[nodiscard]] gta_t const & time_axis() const override {
      return ts_impl.time_axis();
    }

    [[nodiscard]] utcperiod total_period() const override {
      return ts_impl.total_period();
    }

    [[nodiscard]] size_t index_of(utctime t) const override {
      return ts_impl.index_of(t);
    }

    [[nodiscard]] size_t size() const override {
      return ts_impl.size();
    }

    [[nodiscard]] utctime time(size_t i) const override {
      return ts_impl.time(i);
    }

    [[nodiscard]] double value(size_t i) const override {
      return ts_impl.value(i);
    }

    [[nodiscard]] double value_at(utctime t) const override {
      return value(index_of(t));
    }

    [[nodiscard]] vector<double> values() const override {
      vector<double> r;
      r.reserve(size());
      for (size_t i = 0; i < size(); ++i)
        r.push_back(ts_impl.value(i));
      return r;
    }

    [[nodiscard]] bool needs_bind() const override {
      return ts_impl.needs_bind();
    }

    void do_bind() override {
      ts_impl.do_bind();
    }

    void do_unbind() override {
      ts_impl.do_unbind();
    }

    [[nodiscard]] ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const & shared_this) const override;
    [[nodiscard]] shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx& ctx) const override;
    [[nodiscard]] string stringify() const override;
    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::time_series::convolve_w_ts<shyft::time_series::dd::apoint_ts>)
x_poly_serialize_export_key(shyft::time_series::dd::convolve_w_ts)
