/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <tuple>

#include <shyft/time/utctime_utilities.h>

namespace shyft::time_axis {

  namespace detail {

    /** @brief compute if time-axis a and b contributes in an extend split-at operation */
    template <class TA, class TB>
    inline std::tuple<bool, bool> contributions(TA const &a, TB const &b, utctime const &split_at) {
      return {(!a.empty() && split_at > a.total_period().start), (!b.empty() && split_at < b.total_period().end)};
    }
  }

  /**
   * @brief extend_info
   * @details
   * This structure contains the result of
   * the `compute_extend_info` algorithm
   * that figures out how to extend one time_series `lhs` with another time_series `rhs`,
   * given a utctime `split_at` point.
   * The result is contributions of partitions of time-axis and points, from
   * `lhs` and `rhs` time-series.
   * Special rules, like what to do if there is a gap,
   * or if there is common time-points, matching the intervals where `split_at` hits,
   * are taken care of in the algorithm.
   */
  struct extend_info {
    struct lhs_ {
      std::size_t n{0u}; // copy [0__n) of v and t to result t,v

      inline bool contributes() const noexcept {
        return n > 0u;
      }
    };

    struct rhs_ {
      std::size_t i{0u};     ///< index of first value b[i]
      std::size_t n{0u};     ///< should be b.size(): copy [i__n) to result v
      utctime t{no_utctime}; ///< if set insert t to result t,
                             // then [i+1__n) to result t
                             // if not set, just copy [i__n) to result t

      inline bool contributes() const noexcept {
        return i < n;
      }
    };

    lhs_ a{};                   ///< lhs a contributions, if any
    utctime t_fill{no_utctime}; ///< inject (t_fill,v_fill) to result tv,
    rhs_ b{};                   ///< rhs b contributions, if any

    /** @brief size of values, or periods of the extend operation
     * */
    inline std::size_t size() const noexcept {
      return a.n + (b.n - b.i) + (t_fill != no_utctime ? 1u : 0u);
    }

    inline bool empty() const noexcept {
      return !a.contributes() && !b.contributes();
    }
  };

  /**
   * @brief compute_extend_info
   *
   * @details
   * This algorithm computes the partitions/contributions
   * from lhs.time_axis `a` and rhs.time_axis `b`,
   * so that time-axis and corresponding values
   * can be computed and perfectly
   * constructed using this information.
   *
   * The rules for the computations are, unfortunately, a bit more involved than we expect(and would like).
   *
   * The algo have 4 branches, given by:
   *
   * a_contributes: if and only if `a.size()>0` and `split_at` > a.total_period().start
   * b_contributes: if and only if `b.size()>0` and `split_at` < b.total_period().end
   *
   * if none contributes, then r.empty() is true
   *
   * if only a_contributes, then r.b.empty() is true, and the result should be using range lhs[0__r.a.n)
   * ___ where r.a.n equal to the end of the index range, computed as lhs.index_of(split_at-epsilon)+1
   * ___ and epsilon smallest reprs. time (1us)
   * ___ or lhs.n if split_at >= lhs.total_period()
   *
   * if only b_contributes then r.b.empty() is true, and the result should be rhs[r.b.i__r.b.n)
   * ___ where r.b.i computed as rhs.index_of(split_at) for split_at>rhs.total_period().start, otherwise 0
   * ___ and r.b.n == rhs.size() (e.g. copy all until the end.
   *
   *
   * if both contributes, (or may contribute according to definition above)
   * then there are 3 components computed
   * - the lhs `a` partition, range (could be empty)
   * - the fill-gap time-point (optional, if lhs ends before rhs begins contrib, there is a gap)
   * - the rhs `b` partition
   *
   * Due to the nature of the problem, and partly also the definition of the original semantics,
   * this is a bit more complex.
   *
   * The semantics definition forbids to insert extra time-point to the time-axis that are not already
   * there, the only exception is when `split_at` are equal to or greater a.total_period().end,
   * then, and only then, the a.total_period().end is allowed to be injected as a fill-point.
   *
   * The rationale for this is that when extending the gap between lhs and rhs, then the time-point for
   * that extension would start at that end-point, and its value would be determined by other parameters
   * passed to the ts `a.extend(b...)` algorithm, like fill with last value or `a`, or a provided constant.
   *
   * So the algorithm must take care of the cases where rhs `b` starts before `a` with its only values,
   * that might last pass the `split_at` and/or the lhs `b` end. Including all cases of intermediate.
   *
   * Notice that the extend algorithm, always begins with lhs `a` contributions (it clips away
   * any b that for example is before `a` starts).
   *
   * In the following `r` denotes the resulting computed values.
   *
   * The lhs `a` partition:
   * r.a.n = a.index_of(split_at - epsilon) + 1 or a.size() if split_at >= a.total_period().end
   * ___ except if lhs a.time(r.a.n-1) == rhs b.time(b-first-contribution), then --r.a.n
   * ___ because `split_at` are part of half open interval of b, not a (b has priority)
   * ____ (This particular point of semantics would be nice to change)
   * r.t_fill = a.total_period().end if a gap until rhs b starts its contribution
   * r.b.i = b.index_of(split_at) if split_at> rhs b.total_period().start else 0
   * ___ except a weird case where b.time(r.b.i) starts before a.time(xx)
   * ___ and when a.period(xx) is come to an end, then another b.time(r.b.i+1)
   * ___ have the defined time and value that should appear as the continuation
   * ___ of a. (This particular point of semantics would be nice to change)
   * r.b.n = b.size()
   * r.b.t = if set, it influences how we construct the time-axis:
   * ___ The first time-point from the b value contribution is
   * ___ moved to this time-point, because the `split_at` is in the interior of
   * ___ rhs `b` period. (does not start at the beginning of a `b` period).
   * ___ so when constructing the time-axis points, the first time-point is r.b.t(if set)
   * ___ then all the time-points from r.b.i+1 until end of the time-axis

   * @tparam TA
   * @tparam TB
   * @param a the lhs.time_axis
   * @param b the rhs.time_axis
   * @param split_at
   * @return r the, extend_info
   */
  template <class TA, class TB>
  extend_info compute_extend_info(const TA &a, const TB &b, utctime const split_at) {
    constexpr utctime const t_epsilon{1l};
    auto const [a_contributes, b_contributes] = detail::contributions(a, b, split_at);
    extend_info ei{};
    if (!a_contributes && !b_contributes) {
      // null range;
    } else if (!b_contributes) {
      ei.a.n = split_at < a.total_period().end ? a.index_of(split_at - t_epsilon) + 1 : a.size();
    } else if (!a_contributes) {
      auto i = b.index_of(split_at);
      ei.b.i = i == std::string::npos ? 0 : i;
      ei.b.n = b.size();
    } else {
      // both contributes
      auto const pa = a.total_period();
      auto const pb = b.total_period();
      ei.a.n = split_at < pa.end ? a.index_of(split_at - t_epsilon) + 1 : a.size(); // always >0 pr def. here
      ei.b.n = b.size();
      ei.b.i = split_at < pb.start ? 0u : b.index_of(split_at);
      if (b._time(ei.b.i) == a._time(ei.a.n - 1))
        --ei.a.n; // b wins the t,v  if back-to-back (would be nice to change semantics)

      if (split_at >= pa.end) {         // entire a covered: but we clamp to pa.end, so really its split_at >= pa.end
        if (pa.end < b._time(ei.b.i)) { // only case where there is a gap
          ei.t_fill = pa.end;           // as in ADD a point at end of a
        } else if (split_at > b._time(ei.b.i)) {
          ei.b.t = pa.end; // MOVE first b.start until split_at, aka pa.end in this case
        } else {
          ; // do nothing: it happens when we have a perfect join a|b
        }
      } else if ((ei.a.n > 0 ? a._time(ei.a.n - 1) : pa.end) > b._time(ei.b.i)) { // if `a` is after b.time(ib),
        auto a_end_p = (ei.a.n < a.size() ? a._time(ei.a.n) : pa.end); // we have to figure out when the 'b'  value
        auto b_end_p = (ei.b.i + 1 < b.size() ? b._time(ei.b.i + 1) : pb.end); // and what b.value to show when
        if (a_end_p < b_end_p) {                                               // the 'a' interval ends
          ei.b.t = a_end_p; // MOVE first b.start time-point, 'b' shows up after 'a' interval is done
        } else {
          ++ei.b.i; // SKIP! The 'a' covered value disappears, and it is the b. next time/value
                    // that starts when 'a' is done.(and again, would be nice to change semantics here)
        }
      }
    }
    return ei;
  }
}