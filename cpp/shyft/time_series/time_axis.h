/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <algorithm>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::time_axis {
  using std::string;
  using std::vector;
  using std::map;
  using std::shared_ptr;
  using std::lower_bound;
  using std::runtime_error;
  using std::out_of_range;

  /**
   * @brief The time-axis concept is an important component of time-series.
   *
   * @details
   * time_axis contains all the definitions of time_axis including the
   * combine algorithms:
   *
   *   combine(ta a,ta b)-> ta : the intersection of a.total_period and b.total_period
   *
   *   extend(ta a, ta b, extend-policy) -> ta :  a first, then at some point b
   *
   *   merge(ta a, ta b) -> ta: the union of the two, but a periods have precedence
   *
   * definition: time-axis is an ordered sequence of non-overlapping periods
   *
   *  @note:
   *
   *      a) the most usual time-axis is the fixed_dt time-axis
   *         in the Shyft core, this is the one we use(could even need a high-speed, no compromise version)
   *
   *      b) continuous/dense time-axis: there are no holes in total_period
   *                               types: fixed_dt, calendar_dt, point_dt
   *      c) sparse time-axis: not supported, we found using nans as better approach
   */


  using namespace shyft::core;

  /** generic test if two different time-axis types resembles the same conceptual time-axis
   *
   * Given time-axis are of different types (point, versus period versus fixed_dt etc.)
   * just compare and see if they produces the same number of periods, and that each
   * period is equal.
   */
  template <class A, class B>
  bool equivalent_time_axis(A const & a, B const & b) {
    if (a.size() != b.size())
      return false;
    for (size_t i = 0; i < a.size(); ++i) {
      if (a.period(i) != b.period(i))
        return false;
    }
    return true;
  }

  /** Specialization of the equivalent_time_axis given that they are of the same type.
   *  In this case we forward the comparison to the type it self relying on the
   *  fact that the time_axis it self knows how to fastest figure out if it's equal.
   */
  template <class A>
  bool equivalent_time_axis(A const & a, A const & b) {
    return a == b;
  }

  /**
   * @brief a simple regular time-axis, starting at t, with n consecutive periods of fixed length dt
   *
   * @details
   *  In the shyft::core this is the most useful&fast variant
   */
  struct fixed_dt {
    utctime t{no_utctime};
    utctimespan dt{0l};
    size_t n{0};

    SHYFT_DEFINE_STRUCT(fixed_dt, (), (t, dt, n));

    fixed_dt() = default;

    fixed_dt(utctime start, utctimespan deltat, size_t n_periods)
      : t{start}
      , dt{deltat}
      , n{n_periods} {
    }

    fixed_dt(int64_t start, int64_t dt, size_t n_periods)
      : t{seconds{start}}
      , dt{seconds{dt}}
      , n{n_periods} {
    }

    fixed_dt(utctime start, int64_t dt, size_t n_periods)
      : t{start}
      , dt{seconds{dt}}
      , n{n_periods} {
    }

    [[nodiscard]] utctimespan delta() const {
      return dt;
    } // BW compat

    [[nodiscard]] utctime start() const noexcept {
      return t;
    } // BW compat

    [[nodiscard]] size_t size() const noexcept {
      return n;
    }

    [[nodiscard]] bool empty() const noexcept {
      return n == 0;
    }

    bool operator==(fixed_dt const & other) const noexcept {
      return t == other.t && dt == other.dt && n == other.n;
    }

    bool operator!=(fixed_dt const & other) const noexcept {
      return !this->operator==(other);
    }

    auto operator<=>(fixed_dt const &) const = default;

    [[nodiscard]] utcperiod total_period() const noexcept {
      return utcperiod(t, t + n * dt);
    }

    [[nodiscard]] utctime time(size_t i) const {
      if (i < n)
        return t + i * dt;
      throw std::out_of_range("fixed_dt.time(i)");
    }

    [[nodiscard]] utctime _time(size_t i) const noexcept {
      return t + i * dt;
    }

    [[nodiscard]] utcperiod period(size_t i) const {
      if (i < n)
        return {t + i * dt, t + (i + 1) * dt};
      throw std::out_of_range("fixed_dt.period(i)");
    }

    [[nodiscard]] size_t _index_of(utctime tx) const noexcept {
      return (tx - t) / dt;
    };

    [[nodiscard]] size_t index_of(utctime tx) const {
      if (tx < t || dt == utctimespan{0l})
        return std::string::npos;
      size_t r = (tx - t) / dt;
      if (r < n)
        return r;
      return std::string::npos;
    }

    [[nodiscard]] fixed_dt slice(size_t i0, size_t n_slice) const {
      return {time(i0), dt, n_slice};
    }

    [[nodiscard]] size_t open_range_index_of(utctime tx) const {
      return n > 0 && (tx >= t + utctimespan(n * dt)) ? n - 1 : index_of(tx);
    }

    [[nodiscard]] size_t open_range_index_of(utctime tx, size_t) const {
      return open_range_index_of(tx);
    }

    static fixed_dt full_range() {
      return {min_utctime, max_utctime, 2};
    } // Hmm.

    static fixed_dt null_range() {
      return {no_utctime, utctimespan{0l}, 0};
    }

    x_serialize_decl();
  };

  /**
   * @brief fixed calendar interval time-axis
   *
   * @details
   * A variant of time_axis that adheres to calendar periods, possibly including DST handling
   *  e.g.: a calendar day might be 23,24 or 25 hour long in a DST calendar.
   *  If delta-t is less or equal to one hour, it's close to as efficient as time_axis
   * @note that time-zone calendar semantics only applies to day or larger deltas. controlled by
   *        by compile time constant calendar_dt::dt_tz_semantics
   *        the rationale behind this is pure empirical practice at statkraft
   *        - we could easily provide this as a parameter -
   */
  struct calendar_dt {

    static constexpr utctimespan dt_tz_semantics = seconds(3600 * 24l); // tz-semantics only applies to >= day

    shared_ptr<calendar> cal; ///< notice that we provide a default singleton for this
    utctime t;                ///< t0, usually aligned/trimmed to cal entity
    utctimespan dt;           ///< the  calendar::DAY WEEK etc.
    size_t n;

    SHYFT_DEFINE_STRUCT(calendar_dt, (), (cal, t, dt, n));

    [[nodiscard]] shared_ptr<calendar> get_calendar() const {
      return cal;
    }

    calendar_dt()
      : cal{calendar::utc()}
      , t{no_utctime}
      , dt{0l}
      , n{0} {
    }

    calendar_dt(shared_ptr< calendar> const & cal, utctime t, utctimespan dt, size_t n)
      : cal{cal}
      , t{t}
      , dt{dt}
      , n{n} {
    }

    calendar_dt(shared_ptr< calendar> const & cal, int64_t t, int64_t dt, size_t n)
      : cal{cal}
      , t{seconds{t}}
      , dt{seconds{dt}}
      , n{n} {
    }

    calendar_dt(calendar_dt const &) = default;

    calendar_dt(calendar_dt&& c) noexcept
      : cal{(std::move(c.cal))}
      , t{c.t}
      , dt{c.dt}
      , n{c.n} {
    }

    calendar_dt& operator=(calendar_dt&& c) noexcept {
      cal = std::move(c.cal);
      t = c.t;
      dt = c.dt;
      n = c.n;
      return *this;
    }

    calendar_dt& operator=(calendar_dt const & x) {
      if (this != &x) {
        cal = x.cal;
        t = x.t;
        dt = x.dt;
        n = x.n;
      }
      return *this;
    }

    /** equality, notice that calendar is equal if they refer to exactly same calendar pointer */
    bool operator==(calendar_dt const & other) const {
      return dt == other.dt && n == other.n && t == other.t
          && (cal.get() == other.cal.get() || ((cal && other.cal) && cal->get_tz_name() == other.cal->get_tz_name()));
    }

    bool operator!=(calendar_dt const & other) const {
      return !this->operator==(other);
    }

    auto operator<=>(calendar_dt const &) const = default;

    [[nodiscard]] size_t size() const noexcept {
      return n;
    }

    [[nodiscard]] bool empty() const noexcept {
      return n == 0;
    }

    [[nodiscard]] utcperiod total_period() const noexcept {
      return utcperiod(t, dt < dt_tz_semantics || cal == nullptr ? utctime{t + n * dt} : cal->add(t, dt, long(n)));
    }

    [[nodiscard]] utctime _time(size_t i) const noexcept {
      return dt < dt_tz_semantics || cal == nullptr ? t + int64_t(i) * dt : cal->add(t, dt, long(i));
    }

    [[nodiscard]] utctime time(size_t i) const {
      if (i < n)
        return _time(i);
      throw out_of_range("calendar_dt.time(i)");
    }

    [[nodiscard]] utcperiod period(size_t i) const {
      if (i < n) {
        return dt < dt_tz_semantics || cal == nullptr
               ? utcperiod(t + i * dt, t + (i + 1) * dt)
               : utcperiod(cal->add(t, dt, static_cast<long>(i)), cal->add(t, dt, static_cast<long>(i + 1)));
      }
      throw out_of_range("calendar_dt.period(i)");
    }

    [[nodiscard]] size_t _index_of(utctime tx) const noexcept {
      return dt < dt_tz_semantics || cal == nullptr
             ? static_cast<size_t>((tx - t) / dt)
             : static_cast<size_t>(cal->diff_units(t, tx, dt));
    }

    [[nodiscard]] size_t index_of(utctime tx) const {
      auto p = total_period();
      if (!p.contains(tx))
        return std::string::npos;
      return _index_of(tx);
    }

    [[nodiscard]] calendar_dt slice(size_t i0, size_t n_slice) const {
      return {cal, time(i0), dt, n_slice};
    }

    [[nodiscard]] size_t open_range_index_of(utctime tx, size_t) const {
      return tx >= total_period().end && n > 0 ? n - 1 : index_of(tx);
    }

    [[nodiscard]] size_t open_range_index_of(utctime tx) const {
      return tx >= total_period().end && n > 0 ? n - 1 : index_of(tx);
    }

    [[nodiscard]] static calendar_dt null_range() {
      return {};
    }

    [[nodiscard]] string get_tz_name() const {
      if (cal) {
        if (auto const & tzi = cal->tz_info)
          return tzi->name();
      }
      return "UTC";
    }

    [[nodiscard]] bool is_simple() const noexcept {
      return dt < dt_tz_semantics;
    }

    [[nodiscard]] fixed_dt simplify() const noexcept {
      return {t, dt, n};
    }

    x_serialize_decl();
  };

  /**
   * @brief point_dt is the most generic dense time-axis.
   *
   * @details
   * The representation of time-axis, are n time points + end-point,
   * where interval(i) utcperiod(t[i],t[i+1])
   *          except for the last
   *                   utcperiod(t[i],te)
   *
   * very flexible, but inefficient in space and time
   * to minimize the problem the, .index_of() provides
   *  'ix_hint' to hint about the last location used
   *    then if specified, search +- 10 positions to see if we get a hit
   *   otherwise just a binary-search for the correct index.
   *  TODO: maybe even do some smarter partitioning, guessing equidistance on average between points.
   *        then guess the area (+-10), if within range, search there ?
   */
  struct point_dt {
    vector<utctime> t;
    utctime t_end; // need one extra, after t.back(), to give the last period!

    SHYFT_DEFINE_STRUCT(point_dt, (), (t, t_end));

    inline void throw_if_invalid_points() const {
      if (t.empty()) {
        if (t_end != no_utctime)
          throw runtime_error("time_axis::point_dt: need at least two time-points to define one period");
      } else if (t.back() >= t_end)
        throw runtime_error("time_axis::point_dt: t_end should be after last time-point");
    }

    inline void checked_ct_from_points() {
      if (!t.empty()) {
        if (t.size() < 2)
          throw runtime_error("time_axis::point_dt() needs at least two time-points");
        t_end = t.back();
        t.pop_back();
        throw_if_invalid_points();
      }
    }

    point_dt()
      : t(vector<utctime>{})
      , t_end(no_utctime) {
    }

    point_dt(vector<utctime> const & t, utctime t_end)
      : t(t)
      , t_end(t_end) {
      throw_if_invalid_points();
    }

    point_dt(vector<utctime>&& tx, utctime t_end)
      : t(std::move(tx))
      , t_end(t_end) {
      throw_if_invalid_points();
    }

    explicit point_dt(vector<utctime> const & all_points)
      : t(all_points)
      , t_end(no_utctime) {
      checked_ct_from_points();
    }

    explicit point_dt(vector<utctime>&& all_points)
      : t(std::move(all_points))
      , t_end(no_utctime) {
      checked_ct_from_points();
    }

    // ms seems to need explicit move etc.
    point_dt(point_dt const &) = default;

    point_dt(point_dt&& c) noexcept
      : t(std::move(c.t))
      , t_end(c.t_end) {
    }

    point_dt& operator=(point_dt&& c) noexcept {
      t = std::move(c.t);
      t_end = c.t_end;
      return *this;
    }

    point_dt& operator=(point_dt const & x) {
      if (this != &x) {
        t = x.t;
        t_end = x.t_end;
      }
      return *this;
    }

    bool operator==(point_dt const & other) const {
      return t == other.t && t_end == other.t_end;
    }

    bool operator!=(point_dt const & other) const {
      return !this->operator==(other);
    }

    auto operator<=>(point_dt const &) const = default;

    [[nodiscard]] size_t size() const {
      return t.size();
    }

    [[nodiscard]] bool empty() const noexcept {
      return t.empty();
    }

    [[nodiscard]] utcperiod total_period() const {
      return t.empty() ? utcperiod(min_utctime, min_utctime) : // maybe just a non-valid period?
               utcperiod(t[0], t_end);
    }

    [[nodiscard]] utctime time(size_t i) const {
      if (i < t.size())
        return t[i];
      throw std::out_of_range("point_dt.time(i)");
    }

    [[nodiscard]] utctime _time(size_t i) const noexcept {
      return t[i];
    }

    [[nodiscard]] utcperiod period(size_t i) const {
      if (i < t.size())
        return {t[i], i + 1 < t.size() ? t[i + 1] : t_end};
      throw std::out_of_range("point_dt.period(i)");
    }

    [[nodiscard]] size_t index_of(utctime tx, size_t ix_hint) const noexcept {
      if (t.empty() || tx < t[0] || tx >= t_end)
        return std::string::npos;
      if (tx >= t.back())
        return t.size() - 1;

      if (ix_hint != std::string::npos && ix_hint < t.size()) {
        if (t[ix_hint] == tx)
          return ix_hint;
        size_t const max_directional_search = 10; // just  a wild guess
        if (t[ix_hint] < tx) {
          size_t j = 0;
          while (t[ix_hint] < tx && ++j < max_directional_search && ix_hint < t.size()) {
            ix_hint++;
          }
          if (ix_hint == t.size()) // we started below p.start, so we got one to far(or at end), so revert back one step
            return ix_hint - 1;
          if (t[ix_hint] > tx)
            return ix_hint - 1;
          if (t[ix_hint] == tx)
            return ix_hint;
          // give up and fall through to binary-search
        } else {
          size_t j = 0;
          while (t[ix_hint] > tx && ++j < max_directional_search && ix_hint > 0) {
            --ix_hint;
          }
          if (t[ix_hint] > tx && ix_hint > 0) // if we are still not before p.start, and `i` is >0, there is a hope to
                                              // find better index, otherwise we are at/before start
            ;                                 // bad luck searching downward, need to use binary search.
          else
            return ix_hint;
        }
      }

      auto r = lower_bound(t.cbegin(), t.cend(), tx, [](utctime pt, utctime val) {
        return pt <= val;
      });
      return static_cast<size_t>(r - t.cbegin()) - 1;
    }

    [[nodiscard]] size_t index_of(utctime tx) const noexcept {
      return index_of(tx, std::string::npos);
    }

    [[nodiscard]] point_dt slice(size_t i0, size_t n) const {
      //            0 1 2 :3
      //  0,1:      0 ]
      //  0,2:      0 1 ]
      //  0,3       0 1 2 :3
      //
      if (i0 + n < t.size()) {
        return point_dt{vector<utctime>(begin(t) + i0, begin(t) + i0 + n + 1)};
      } else {
        return {vector<utctime>(begin(t) + i0, end(t)), t_end};
      }
    }

    [[nodiscard]] size_t open_range_index_of(utctime tx, size_t ix_hint = std::string::npos) const {
      return size() > 0 && tx >= t_end ? size() - 1 : index_of(tx, ix_hint);
    }

    [[nodiscard]] static point_dt null_range() {
      return {};
    }

    x_serialize_decl();
  };

  /**
   * @brief a generic (not sparse) time interval time-axis.
   *
   * @details
   * This is a static dispatch generic time-axis for all dense time-axis.
   * It's merely utilizing the three other types to do the implementation.
   * It's useful when combining time-axis, and we would like to keep
   * the internal rep. to the most efficient as determined at runtime.
   */
  struct generic_dt {
    /**
     * @brief Possible time-axis types.
     */
    enum generic_type : int8_t {
      FIXED = 0,    /**< Represents storage of fixed_dt. */
      CALENDAR = 1, /**< Represents storage of calendar_dt. */
      POINT = 2     /**< Represents storage of point_dt. */
    };

    std::variant<fixed_dt, calendar_dt, point_dt> impl;

    SHYFT_DEFINE_STRUCT(generic_dt, (), (impl));

    ///< explicit construct from any time-axis w.forward

    explicit generic_dt(fixed_dt const & ta)
      : impl{ta} {
    }

    explicit generic_dt(fixed_dt&& ta) noexcept
      : impl{ta} {
    }

    explicit generic_dt(calendar_dt const & ta) {
      if (ta.is_simple())
        impl = ta.simplify();
      else
        impl = ta;
    }

    explicit generic_dt(calendar_dt&& ta) {
      if (ta.is_simple())
        impl = ta.simplify();
      else
        impl = std::move(ta);
    }

    explicit generic_dt(point_dt const & ta)
      : impl{ta} {
    }

    explicit generic_dt(point_dt&& ta)
      : impl{std::move(ta)} {
    }

    generic_dt() = default;

    generic_dt(utctime t0, utctime dt, size_t n)
      : impl{
          fixed_dt{t0, dt, n}
    } {
    }

    generic_dt(shared_ptr<calendar> const & cal, utctime t0, utctime dt, size_t n) {
      if (dt < calendar_dt::dt_tz_semantics)
        impl = fixed_dt{t0, dt, n};
      else
        impl = calendar_dt{cal, t0, dt, n};
    }

    ///< ensure to efficiently construct time-point vectors
    generic_dt(vector<utctime>&& tp, utctime t_end)
      : impl{
          point_dt{std::move(tp), t_end}
    } {
    }

    generic_dt(vector<utctime> const & tp, utctime a_t_end)
      : impl{
          point_dt{tp, a_t_end}
    } {
    }

    explicit generic_dt(vector<utctime>&& allpoints)
      : impl{point_dt{std::move(allpoints)}} {
    }

    explicit generic_dt(vector<utctime> const & allpoints)
      : impl{point_dt{allpoints}} {
    }

    [[nodiscard]] generic_type gt() const noexcept {
      return static_cast<generic_type>(impl.index());
    }

    void set_gt(generic_type v) { // only used in ts db layer
      if (v != gt()) {
        switch (v) {
        case generic_type::FIXED:
          impl = fixed_dt{};
          break;
        case generic_type::CALENDAR:
          impl = calendar_dt{};
          break;
        case generic_type::POINT:
          impl = point_dt{};
          break;
        }
      }
    }

    [[nodiscard]] bool is_fixed_dt() const noexcept {
      return gt() != generic_type::POINT;
    }

    bool operator==(generic_dt const & other) const {
      if (impl.index() == other.impl.index()) {
        return impl == other.impl; // same type, rely on variant ==.
      } else {                     // different type, we still can do equality by equivalent_time_axis
        return std::visit(
          [&](auto&& a, auto&& b) {
            return equivalent_time_axis(a, b);
          },
          impl,
          other.impl);
      }
    }

    bool operator!=(generic_dt const & other) const {
      return !operator==(other);
    }

    auto operator<=>(generic_dt const &) const = default;

    [[nodiscard]] size_t size() const noexcept {
      return std::visit(
        [&](auto&& ta) {
          return ta.size();
        },
        impl);
    }

    [[nodiscard]] bool empty() const noexcept {
      return std::visit(
        [&](auto&& ta) {
          return ta.empty();
        },
        impl);
    }

    [[nodiscard]] utcperiod total_period() const noexcept {
      return std::visit(
        [](auto&& ta) {
          return ta.total_period();
        },
        impl);
    }

    [[nodiscard]] utcperiod period(size_t i) const {
      return std::visit(
        [&](auto&& ta) {
          return ta.period(i);
        },
        impl);
    }

    [[nodiscard]] utctime time(size_t i) const {
      return std::visit(
        [&](auto&& ta) {
          return ta.time(i);
        },
        impl);
    }

    [[nodiscard]] utctime dt() const noexcept {
      return std::visit(
        [](auto&& ta) {
          if constexpr (std::is_same_v<std::remove_cvref_t<decltype(ta)>, point_dt>) {
            return utctime(0l);
          } else {
            return ta.dt;
          }
        },
        impl);
    }

    [[nodiscard]] size_t index_of(utctime t, size_t ix_hint = std::string::npos) const {
      return std::visit(
        [&](auto&& ta) {
          if constexpr (std::is_same_v<std::remove_cvref_t<decltype(ta)>, point_dt>) {
            return ta.index_of(t, ix_hint);
          } else {
            return ta.index_of(t);
          }
        },
        impl);
    }

    [[nodiscard]] size_t open_range_index_of(utctime t, size_t ix_hint = std::string::npos) const {
      return std::visit(
        [&](auto&& ta) {
          if constexpr (std::is_same_v<std::remove_cvref_t<decltype(ta)>, point_dt>) {
            return ta.open_range_index_of(t, ix_hint);
          } else {
            return ta.open_range_index_of(t);
          }
        },
        impl);
    }

    [[nodiscard]] generic_dt slice(size_t i0, size_t n) const {
      return std::visit(
        [&](auto&& ta) {
          return generic_dt{ta.slice(i0, n)};
        },
        impl);
    }

    fixed_dt& f() {
      return get<fixed_dt>(impl);
    }

    [[nodiscard]] fixed_dt const & f() const {
      return get<fixed_dt>(impl);
    }

    calendar_dt& c() {
      return get<calendar_dt>(impl);
    }

    [[nodiscard]] calendar_dt const & c() const {
      return get<calendar_dt>(impl);
    }

    point_dt& p() {
      return get<point_dt>(impl);
    }

    [[nodiscard]] point_dt const & p() const {
      return get<point_dt>(impl);
    }

    x_serialize_decl();
  };

  /** create a new time-shifted dt time-axis */
  inline fixed_dt time_shift(fixed_dt const & src, utctimespan dt) {
    return {src.t + dt, src.dt, src.n};
  }

  /** create a new time-shifted dt time-axis */
  inline calendar_dt time_shift(calendar_dt const & src, utctimespan dt_shift) {
    calendar_dt r(src);
    r.t += dt_shift;
    return r;
  }

  /** create a new time-shifted dt time-axis */
  inline point_dt time_shift(point_dt const & src, utctimespan dt) {
    point_dt r(src);
    for (auto& t : r.t)
      t += dt; // potential cost, we could consider other approaches with refs.
    r.t_end += dt;
    return r;
  }

  /** create a new time-shifted dt time-axis */
  inline generic_dt time_shift(generic_dt const & src, utctimespan dt) {
    if (src.gt() == generic_dt::FIXED)
      return generic_dt(time_shift(src.f(), dt));
    if (src.gt() == generic_dt::CALENDAR)
      return generic_dt(time_shift(src.c(), dt));
    return generic_dt(time_shift(src.p(), dt));
  }

  /**
   * @brief
   * @details
   * Contributions from a strictly from the left of split_at, then b.
   *
   * @param a
   * @param b
   * @param split_at
   * @return
   */
  generic_dt extend(generic_dt const & a, generic_dt const & b, utctime const split_at);

  /** @brief fast&efficient combine for two fixed_dt time-axis */
  inline fixed_dt combine(fixed_dt const & a, fixed_dt const & b) {
    // 0. check if they overlap (todo: this could hide dt-errors checked for later)
    auto require_t0_alignment = [&](utctime const & dt) {
      if ((a.t.count() - b.t.count()) % dt.count() != 0)
        throw std::runtime_error("combine(fixed_dt a,b) needs t to align");
    };
    auto require_dt_alignment = [&](utctime const & a_dt, utctime const & b_dt) {
      if ((a_dt.count() % b_dt.count()) != 0)
        throw std::runtime_error("combine(fixed_dt a,b) needs dt to align");
    };
    utcperiod pa = a.total_period();
    utcperiod pb = b.total_period();
    if (!pa.overlaps(pb) || a.empty() || b.empty())
      return fixed_dt::null_range();
    if (a.dt == b.dt) {
      if (a.t == b.t && a.n == b.n)
        return a;
      require_t0_alignment(a.dt);
      utctime t0 = max(pa.start, pb.start);
      return {t0, a.dt, static_cast<size_t>((min(pa.end, pb.end) - t0) / a.dt)};
    }
    if (a.dt > b.dt) {
      if ((a.dt.count() % b.dt.count()) != 0)
        throw std::runtime_error("combine(fixed_dt a,b) needs dt to align");
      require_dt_alignment(a.dt, b.dt);
      require_t0_alignment(b.dt);
      utctime t0 = max(pa.start, pb.start);
      return {t0, b.dt, static_cast<size_t>((min(pa.end, pb.end) - t0) / b.dt)};
    } else {
      require_dt_alignment(b.dt, a.dt);
      require_t0_alignment(a.dt);
      utctime t0 = max(pa.start, pb.start);
      return {t0, a.dt, static_cast<size_t>((min(pa.end, pb.end) - t0) / a.dt)};
    }
  }

  /**
   * @brief combine continuous (time-axis,time-axis) template
   * for combining any continuous time-axis with another continuous time-axis
   * @note this could have potentially linear cost of n-points
   */
  template <class TA, class TB>
  inline generic_dt combine(const TA& a, const TB& b, void* = nullptr) {
    utcperiod pa = a.total_period();
    utcperiod pb = b.total_period();
    if (!pa.overlaps(pb) || a.size() == 0 || b.size() == 0)
      return generic_dt(point_dt::null_range());
    if (pa == pb && a.size() == b.size()) { // possibly exact equal ?
      bool all_equal = true;
      for (size_t i = 0; i < a.size(); ++i) {
        if (a.period(i) != b.period(i)) {
          all_equal = false;
          break;
        }
      }
      if (all_equal)
        return generic_dt(a);
    }
    // the hard way merge points in the intersection of periods
    utctime t0 = std::max(pa.start, pb.start);
    utctime te = std::min(pa.end, pb.end);
    size_t ia = a.open_range_index_of(t0);     // first possible candidate from a
    size_t ib = b.open_range_index_of(t0);     // first possible candidate from b
    size_t ea = 1 + a.open_range_index_of(te); // one past last possible candidate from a
    size_t eb = 1 + b.open_range_index_of(te); // one past last possible candidate from b
    point_dt r;                                // result generic type for dense time-axis
    r.t.reserve((ea - ia) + (eb - ib));        // assume worst case here, avoid realloc
    r.t_end = te;                              // last point set
    if (pa.start < pb.start)                   // skip first a-point, since b have the first value
      ++ia;
    else if (pb.start < pa.start) // skip first b-point since a have the first value
      ++ib;
    while (ia < ea && ib < eb) {
      utctime ta = a.time(ia);
      utctime tb = b.time(ib);

      if (ta == tb) {
        r.t.push_back(ta);
        ++ia;
        ++ib; // common point,push&incr. both
      } else if (ta < tb) {
        r.t.push_back(ta);
        ++ia; // a contribution only, incr. a
      } else {
        r.t.push_back(tb);
        ++ib; // b contribution only, incr. b
      }
    }
    // a or b (or both) are empty for time-points, we need to fill up remaining < te
    if (ia < ea) { // more to fill in from a ?
      while (ia < ea) {
        auto t_i = a.time(ia++);
        if (t_i < te)
          r.t.push_back(t_i);
      }
    } else { // more to fill in from b ?
      while (ib < eb) {
        auto t_i = b.time(ib++);
        if (t_i < te)
          r.t.push_back(t_i);
      }
    }

    if (r.t.back() == r.t_end) // make sure we leave t_end as the last point.
      r.t.pop_back();
    return generic_dt(r);
  }

  /**
   * @brief combine
   * @details
   * ensure generic_dt optimizes fixed-interval cases
   */
  inline generic_dt combine(generic_dt const & a, generic_dt const & b) {
    switch (a.gt()) {
    case generic_dt::FIXED:
      switch (b.gt()) {
      case generic_dt::FIXED:
        try {
          return generic_dt{combine(a.f(), b.f())}; // fast
        } catch (std::runtime_error const &) {      // failed,misaligned
          return combine(a.f(), b.f(), nullptr);    // hit the generic case above
        }
      case generic_dt::CALENDAR:
        return combine(a.f(), b.c());
      case generic_dt::POINT:
        return combine(a.f(), b.p());
      }
      break;
    case generic_dt::CALENDAR:
      switch (b.gt()) {
      case generic_dt::FIXED:
        return combine(a.c(), b.f());
      case generic_dt::CALENDAR:
        return combine(a.c(), b.c());
      case generic_dt::POINT:
        return combine(a.c(), b.p());
      }
      break;
    case generic_dt::POINT:
      switch (b.gt()) {
      case generic_dt::FIXED:
        return combine(a.p(), b.f());
      case generic_dt::CALENDAR:
        return combine(a.p(), b.c());
      case generic_dt::POINT:
        return combine(a.p(), b.p());
      }
      break;
    }
    return generic_dt{}; // never reached
  }

  /** @brief time-axis combine type deduction system for combine algorithm
   *
   * The goal here is to deduce the fastest possible representation type of
   * two time-axis to combine.
   */
  template <typename T_A, typename T_B, typename C = void >
  struct combine_type { // generic fallback to period_list type, very general, but expensive
                        // typedef point_dt type;
  };

  /** specialization for fixed_dt at max speed */
  template <>
  struct combine_type<fixed_dt, fixed_dt, void> {
    typedef fixed_dt type;
  };

  /** specialization for all continuous time_axis types */
  template <typename T_A, typename T_B> // then take care of all the continuous type of time-axis, they all goes into
                                        // generic_dt type
  struct combine_type< T_A, T_B > {
    typedef generic_dt type;
  };

  //-- fixup missing index-hint for some time-axis-types
  template <class TA >
  inline size_t ta_index_of(TA const & ta, utctime t, size_t) {
    return ta.index_of(t);
  }

  template <>
  inline size_t ta_index_of(time_axis::point_dt const & ta, utctime t, size_t ix_hint) {
    return ta.index_of(t, ix_hint);
  }

  template <>
  inline size_t ta_index_of(time_axis::generic_dt const & ta, utctime t, size_t ix_hint) {
    return ta.index_of(t, ix_hint);
  }

  /* The section below contains merge functionality for
   * time-axis, but to be used in the context of time-series
   * the 'merge(ta a,ta b)' operation
   * require the time-axis to be compatible
   * and the .total_period() should overlap, or extend.
   * It's important that time-axis and values are merged using
   * same info/algorithm.
   */

  /** helper class to keep time-series/axis merge info
   *  for time-axis a (priority) and b (fill-in/extend)
   */
  struct merge_info {
    size_t b_n{0};            ///< copy n- first from b before a
    size_t a_i{string::npos}; ///< extend a with  b[a_i]..a_n after a
    size_t a_n{0};            ///< number of elements to extend after a is at the end.

    [[nodiscard]] size_t size() const {
      return b_n + a_n;
    }

    utctime t_end{no_utctime}; ///< the t_end, relevant for point_dt time-axis
  };

  /**returns true if the period a and be union can be one continuous period*/
  inline bool continuous_merge(utcperiod const & a, utcperiod const & b) {
    return !(a.end < b.start || b.end < a.start);
  }

  /**return true if a calendars are reference equal or have same name */
  inline bool equal_calendars(shared_ptr<calendar> const & a, shared_ptr<calendar> const & b) {
    return a.get() == b.get() || (a->get_tz_name() == b->get_tz_name());
  }

  /** return true if fixed time-axis a and b can be merged into one time-axis */
  inline bool can_merge(fixed_dt const & a, fixed_dt const & b) {
    return a.dt == b.dt && a.dt != utctimespan{0l} && a.n > 0 && b.n > 0
        && ((a.t.count() - b.t.count()) % a.dt.count() == 0) && continuous_merge(a.total_period(), b.total_period());
  }

  /** return true if calendar time-axis a and b can be merged into one time-axis */
  inline bool can_merge(calendar_dt const & a, calendar_dt const & b) {
    auto compatible_t0 = [&]() {
      if (a.t == b.t)
        return true;
      utctime reminder{0u};
      (void) a.cal->diff_units(a.t, b.t, a.dt, reminder);
      return reminder.count() == 0;
    };
    return a.dt == b.dt && a.dt != utctimespan{0l} && a.n > 0 && b.n > 0 && equal_calendars(a.cal, b.cal)
        && continuous_merge(a.total_period(), b.total_period()) && compatible_t0();
  }

  /** return true if point time-axis a and b can be merged into one time-axis */
  inline bool can_merge(point_dt const & a, point_dt const & b) {
    return continuous_merge(a.total_period(), b.total_period());
  }

  /** return true if generic time-axis a and b can be merged into one time-axis */
  inline bool can_merge(generic_dt const & a, generic_dt const & b) {
    if (a.gt() == b.gt()) {
      switch (a.gt()) {
      case generic_dt::FIXED:
        return can_merge(a.f(), b.f());
      case generic_dt::CALENDAR:
        return can_merge(a.c(), b.c());
      case generic_dt::POINT:
        return can_merge(a.p(), b.p());
      }
      throw runtime_error("unsupported time-axis in can_merge");
    } else {
      return continuous_merge(a.total_period(), b.total_period());
    }
  }

  /**computes the merge-info for two time-axis
   *
   * to enable easy and consistent time-series merge
   * operations.
   */
  template <class TA > // enable if time-axis
  inline merge_info compute_merge_info(const TA& a, const TA& b) {
    auto const a_p = a.total_period();
    auto const b_p = b.total_period();
    if (!continuous_merge(a_p, b_p))
      throw runtime_error(string("attempt to merge disjoint non-overlapping time-axis"));
    merge_info r;
    if (a_p.start > b_p.start) { // a starts after b, so b contribute before a starts
      r.b_n = b.index_of(a_p.start - utctimespan{1l}) + 1;
    }
    if (a_p.end < b_p.end) {       // a ends before b ends, so b extends the result
      r.a_i = b.index_of(a_p.end); // check if b.time(i) is >= a_p.end, if not increment i.
      if (b.time(r.a_i) < a_p.end)
        ++r.a_i;
      r.a_n = b.size() - r.a_i;
      r.t_end = b_p.end;
    } else { // a ends after b
      r.t_end = a_p.end;
    }
    return r;
  }

  /** merge time-axis a and b into one.
   *  require a.dt equal to b.dt
   *         and that the two axis covers a contiguous period
   */
  inline fixed_dt merge(fixed_dt const & a, fixed_dt const & b, merge_info const & m) {
    auto const a_p = a.total_period();
    auto const b_p = b.total_period();
    utcperiod p{min(a_p.start, b_p.start), max(a_p.end, b_p.end)};
    return fixed_dt{p.start, a.dt, a.size() + m.size()};
  }

  /** merge time-axis a and b into one.
   *  require a.dt equal to b.dt,
   *         and same calendar (tz-id)
   *         and that the two axis covers a contiguous period
   */
  inline calendar_dt merge(calendar_dt const & a, calendar_dt const & b, merge_info const & m) {
    auto const a_p = a.total_period();
    auto const b_p = b.total_period();
    utcperiod p{min(a_p.start, b_p.start), max(a_p.end, b_p.end)};
    return calendar_dt{a.cal, p.start, a.dt, a.size() + m.size()};
  }

  /** merge value-vector from two time-series a and b using merge_info */
  template <class T>
  vector<T> merge(vector<T> const & a, vector<T> const & b, merge_info const & m) {
    auto n = m.size() + a.size();
    vector<T> r;
    r.reserve(n + 1); //+1, because in case of break point ts, we might add one continuation point at the end
    if (m.b_n)
      copy(begin(b), begin(b) + m.b_n, back_inserter(r));
    copy(begin(a), end(a), back_inserter(r));
    if (m.a_n)
      copy(begin(b) + m.a_i, begin(b) + m.a_i + m.a_n, back_inserter(r));
    return r;
  }

  /** merge time-axis a and b into one using merge_info.
   *  require a validated merg_info for a and b
   *  @return a new point_dt where the points are
   *           all points of a, plus points of b not covered by a
   */
  inline point_dt merge(point_dt const & a, point_dt const & b, merge_info const & m) {
    return point_dt{merge(a.t, b.t, m), m.t_end};
  }

  /** convert any time-axis to it's point_dt equivalent */
  template <class TA>
  inline point_dt convert_to_point_dt(const TA& a) {
    if (a.size() == 0)
      return point_dt{};
    vector<utctime> t;
    t.reserve(a.size());
    for (size_t i = 0; i < a.size(); ++i)
      t.push_back(a.time(i));
    return point_dt{std::move(t), a.total_period().end};
  }

  /** merge generic_dt algorithm */
  inline generic_dt merge(generic_dt const & a, generic_dt const & b, merge_info const & m) {
    if (a.gt() == b.gt()) { // if same representation, pass to the specific & fast routines
      switch (a.gt()) {
      case generic_dt::FIXED:
        if (can_merge(a.f(), b.f()))
          return generic_dt(merge(a.f(), b.f(), m));
        break;
      case generic_dt::CALENDAR:
        if (can_merge(a.c(), b.c()))
          return generic_dt(merge(a.c(), b.c(), m));
        break;
      case generic_dt::POINT:
        return generic_dt(merge(a.p(), b.p(), m));
      }
      // fall through, and promote to point-dt that always can merge
    }
    // promote to point_dt and merge those
    point_dt const * pa = nullptr;
    point_dt const * pb = nullptr;
    point_dt xa;
    point_dt xb;
    if (a.gt() != generic_dt::POINT) {
      xa = convert_to_point_dt(a);
      pa = &xa;
    } else
      pa = &a.p();
    if (b.gt() != generic_dt::POINT) {
      xb = convert_to_point_dt(b);
      pb = &xb;
    } else
      pb = &b.p();
    return generic_dt(merge(*pa, *pb, m));
  }

  /** simple template that merges two equally typed time-series
   *
   *  @returns the merged time-series, or throws if not compatible
   */
  template <class TA>
  TA merge(const TA& a, const TA& b) {
    if (!continuous_merge(a.total_period(), b.total_period()))
      throw runtime_error("can not merge time-axis, disjoint total_period");
    return merge(a, b, compute_merge_info(a, b));
  }

  namespace repeat {
    inline auto compute_t0_n0(generic_dt const & src, generic_dt const & r) {
      // only requirement to src is .total_period(), .time() and .size(), it can be of any kind/type.
      // figure out the offset to first t0 in the r
      auto dt0 = r.gt() == generic_dt::CALENDAR
                 ? r.time(0) - r.c().cal->trim(r.time(0), r.c().dt)
                 : r.time(0) - utctime_floor(r.time(0), r.f().dt);
      // figure out t0 relative src.time(0), trim to p.period boundary
      auto t0 = dt0
              + (r.gt() == generic_dt::CALENDAR ? r.c().cal->trim(src.time(0), r.c().dt)
                                                : utctime_floor(src.time(0), r.f().dt));
      auto n0 = src.index_of(
        r.gt() == generic_dt::CALENDAR
          ? r.c().cal->add(t0, r.c().dt, 1)
          : t0 + r.f().dt);        // an estimate for number of points
      if (n0 == std::string::npos) // yes this is possible,
        n0 = src.size();           // e.g. a short src ts (1 month) relative repeat period ( a year)
      return std::make_tuple(t0, n0);
    }

    /**
     * @brief construct a repeated a src time-axis over a fixed or calendar time-axis.
     * @details
     * The purpose is to create a repeated time-series, and constructing a
     * repeated time_axis is part of that work.
     *
     * It pads first point, and if needed last-point (src.total_period().end) of each period
     * if needed.
     *
     * @note If more speed is needed, implement specialized trivial cases for e.g. hourly src repeated over years->a
     * fixed dt time-axis instead of points.
     *
     * @see shyft::time_series::dd::repeat_ts
     * @param src a source time-axis, can be any type
     * @param r a calendar of fixed-interval repeat time-axis that provides the larger period into which the matching
     * portion of src will be mapped.
     * @throws runtime_error in case argument r is not of proper type.
     * @return a new time-axis with the matching timepoints repeated, possibly with patching start/end of each
     * repeat-period
     */
    inline generic_dt construct(generic_dt const & src, generic_dt const & r) {
      if (r.gt() == generic_dt::POINT)
        throw runtime_error("repeat time-axis must be of some fixed delta-t type");

      if (src.empty() || r.empty()) // trivial case where one of ta is empy->empty result
        return generic_dt{};
      // TODO: we can increase performance if the src and r time-axis are 'compatible',
      //       f.ex: src is fixed dt, complete cover r.dt, and aligned with r
      auto [t0, n0] = repeat::compute_t0_n0(src, r);
      vector<utctime> t;
      t.reserve(n0 * r.size() + n0); // allocate some reserve to allow for cal. semantics
                                     // if not exact match at the beginning, then insert a time-point(with a nan)
      bool inject_p_start = t0 != src.time(0);
      auto src_end = src.total_period().end;
      for (size_t p = 0; p < r.size(); p++) { // for each period fill in offsets
        auto p_i = r.period(p);
        if (inject_p_start)
          t.emplace_back(p_i.start);
        for (size_t i = 0; i < src.size() && p_i.contains(p_i.start + (src.time(i) - t0)); ++i) {
          t.emplace_back(p_i.start + (src.time(i) - t0)); // consider cal.add(p_i.start, (),1) if calendar... but it
                                                          // seems most correct just to repeat the pattern-offsets
        }
        if (p_i.start + (src_end - t0) < p_i.end) // do we need to finish-off the ts even before reaching next period ?
          t.emplace_back(p_i.start + (src_end - t0)); // insert a point her to place the nan value
      }
      return {std::move(t), r.total_period().end};
    }
  }


}

//--serialization support
x_serialize_binary(shyft::time_axis::fixed_dt)
x_serialize_export_key_nt(shyft::time_axis::calendar_dt)
x_serialize_export_key_nt(shyft::time_axis::point_dt)
x_serialize_export_key_nt(shyft::time_axis::generic_dt)

BOOST_CLASS_VERSION(shyft::time_axis::point_dt, 1) //-> blob t vector

template <typename Char>
struct fmt::formatter<shyft::time_axis::fixed_dt, Char>
  : shyft::reflection::struct_formatter<shyft::time_axis::fixed_dt, Char> { };

template <typename Char>
struct fmt::formatter<shyft::time_axis::calendar_dt, Char>
  : shyft::reflection::struct_formatter<shyft::time_axis::calendar_dt, Char> { };

template <typename Char>
struct fmt::formatter<shyft::time_axis::point_dt, Char>
  : shyft::reflection::struct_formatter<shyft::time_axis::point_dt, Char> { };

template <typename Char>
struct fmt::formatter<shyft::time_axis::generic_dt, Char>
  : shyft::reflection::struct_formatter<shyft::time_axis::generic_dt, Char> { };
