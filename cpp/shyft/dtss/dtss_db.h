/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <shyft/dtss/db_deleter.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/ts_db_interface.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::dtss {

  using std::vector;
  using std::map;
  using std::unique_ptr;
  using std::string;
  using std::size_t;

  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using gta_t = shyft::time_axis::generic_dt;
  using gts_t = shyft::time_series::point_ts<gta_t>;


/** @brief Storage layer header-record.
 *
 * The storage layer header-record contains enough
 * information to
 *  a) identify the format version (and thus the rest of the layout)
 *  b) provide  minimal quick summary information that could ease search
 *  c) vital features of the ts that is invariant
 *
 * @note This is a packed record, and should portable through small-endian cpu-arch.
 *
 * Format specification:
 * The binary format of the file is then defined as :
 *   <ts.db.file>   -> <header><time-axis><values>
 *       <header>   -> ()'TS1'|'TS2') <point_fx> <ta_type> <n> <data_period>
 *                            note:
 *                               if 'TS1', then all time-values (int64_t) are in seconds,
 *                               if 'TS2,' all time-values(int64_t) are in micro seconds
 *     <point_fx>   -> ts_point_fx:uint8_t
 *      <ta_type>   -> time_axis::generic_dt::generic_type:uint8_t
 *            <n>   -> uint32_t // number points
 *  <data_period>   -> int64_t int64_t // from .. until
 *
 * <time-axis>      ->
 *   if ta_type == fixed_dt:
 *        <start>   -> int64_t
 *        <delta_t> -> int64_t
 *
 *   if ta_type == calendar_dt:
 *        <start>   -> int64_t
 *        <delta_t> -> int64_t
 *        <tz_sz>   -> uint32_t // the size of tz-info string bytes following
 *        <tz_name> -> uint8_t[<tz_sz>] // length given by  tz_sz above
 *
 *   if ta_type == point_dt:
 *        <t_end>   -> int64_t // the end of the last interval, aka t_end
 *        <t>       -> int64_t[<n>] // <n> from the header
 *
 * <values>         -> double[<n>] // <n> from the header
 *
 */
#pragma pack(push, 1)

  struct ts_db_header {
    char signature[4] = {'T', 'S', '1', '\0'};                                  ///< signature header with version #
    time_series::ts_point_fx point_fx = time_series::POINT_AVERAGE_VALUE;       ///< point_fx feature
    time_axis::generic_dt::generic_type ta_type = time_axis::generic_dt::FIXED; ///< time-axis type
    uint32_t n = 0;        ///< number of points in the time-series (time-axis and values)
    utcperiod data_period; ///< [from..until> period range

    ts_db_header() = default;

    ts_db_header(
      time_series::ts_point_fx point_fx,
      time_axis::generic_dt::generic_type ta_type,
      uint32_t n,
      utcperiod data_period,
      char v = '1')
      : point_fx(point_fx)
      , ta_type(ta_type)
      , n(n)
      , data_period(data_period) {
      signature[2] = v;
    }

    [[nodiscard]] bool is_seconds() const noexcept {
      return signature[2] == '1';
    }

    [[nodiscard]] bool is_valid() const noexcept {
      return signature[0] == 'T' && signature[1] == 'S' && (signature[2] == '1' || signature[2] == '2')
          && signature[3] == 0
          && (ta_type == time_axis::generic_dt::FIXED || ta_type == time_axis::generic_dt::CALENDAR
              || ta_type == time_axis::generic_dt::POINT)
          && (point_fx == time_series::POINT_AVERAGE_VALUE || point_fx == time_series::POINT_INSTANT_VALUE);
    }
  };

#pragma pack(pop)


  struct ts_db_impl; // fwd. detailed impl.

  /**
   * @brief A simple file-io based internal time-series storage for the dtss.
   *
   * @details
   *
   * Utilizing standard c++ libraries to store time-series
   * to regular files, that resides in directory containers.
   * Features are limited to simple write/replace, read, search and delete.
   *
   * The simple idea is just to store one time-series(fragment) pr. file.
   *
   *
   * Using a simple client side url naming:
   *
   *  shyft://<container>/<container-relative-path>
   *
   * inside the ts_db at the server-side there is a map
   *   <container> -> root_dir
   * and thus the fullname of the ts is
   *   <container>.root_dir/<container-relative-path>
   *
   *  e.g.:
   *              (proto)  (container ) (   path within container     )
   *  client url: 'shyft://measurements/hydmet_station/1/temperature_1'
   *
   *  server side:
   *    ts_db_container['measurements']=ts_db('/srv/shyft/ts_db/measurements')
   *
   *   which then would resolve into the full-path for the stored ts-file:
   *
   *     '/srv/shyft/ts_db/measurements/hydmet_station/1/temperature_1'
   *
   * @note that ts-urls that do not match internal 'shyft' protocol are dispatched
   *      to the external setup callback (if any). This way we support both
   *      internally managed as well as externally mapped ts-db
   *
   */
  struct ts_db : its_db {

    db_deleter delete_db_action; // defined before impl so it gets destroyed last
    unique_ptr<ts_db_impl> impl; ///< just to get details out of header

    /** constructs a ts_db with specified container root */
    explicit ts_db(string const &root_dir);
    ~ts_db() override;

    void time_format_micro_seconds(bool use_micro_seconds);
    /** provide the root directory for this ts-db container */
    [[nodiscard]] string root_dir() const override;
    // not supported:
    ts_db() = delete;
    ts_db(ts_db const &) = delete;
    ts_db(ts_db &&) = delete;
    ts_db &operator=(ts_db const &) = delete;
    ts_db &operator=(ts_db &&) = delete;

    [[nodiscard]] diags_t save(
      std::string_view fn,
      gts_t const &ts,
      fx_cache_t fx_cache,
      store_policy const &policy) override;
    [[nodiscard]] diags_t
      save(size_t n, fx_ts_item_t const &fx_item, fx_cache_t fx_cache, store_policy const &policy) override;
    [[nodiscard]] tuple<gts_t, utcperiod> read(std::string_view fn, utcperiod p) override;
    void remove(std::string_view fn) override;
    [[nodiscard]] ts_info get_ts_info(std::string_view fn) override;
    [[nodiscard]] std::vector<ts_info> find(std::string_view match) override;

    void mark_for_deletion() override;
  };

}
