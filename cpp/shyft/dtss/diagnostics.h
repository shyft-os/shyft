#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <optional>
#include <vector>
#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection.h>

namespace shyft::dtss {

  /**
   * @brief ts_diagnostics
   * @details
   * Rather than using text/exceptions, during best-effort store operations,
   * we currently do have a limited set of 'logical' errors that
   * we enforce (by selected policy), and that we can return back to caller.
   * @note
   * for external storage, callback store through python, we do not support individual
   * failure report, as we do not know what failed, and did not fail.
   * So if we get exception, we assume all failed, if no exception, assume all success.
   * This might not reflect the reality, so we encourage to move to shyft storage.
   */
  SHYFT_DEFINE_ENUM(ts_diagnostics, std::int16_t, (undefined,miss_matched_resolution,external_store_failed))

  /**
   * @brief dtss diagnostics (when storing)
   * @details
   * using best-effort policy, the `dtss.store` returns
   * and optional list item,diag pairs that failed
   * to store, to help the client/storing side to make
   * efficient decisions for what to do with the failed
   * items. The return is optional, so we do not traffic
   * other than minimal amount of data.
   */
  struct diagnostics {
    size_t ix{0ul}; ///< index referring to the `ix` element in the time-series set
    ts_diagnostics diag{ts_diagnostics::undefined}; ///< enumeration of known possible causes.
    auto operator<=>(diagnostics const &) const = default;
    SHYFT_DEFINE_STRUCT(diagnostics,(),(ix,diag));
    x_serialize_decl();
  };
  using diags_t =
    std::optional<std::vector<diagnostics>>; ///< dtss.store_ts: returns optional diagnostics if something goes wrong

}

x_serialize_export_key(shyft::dtss::diagnostics)
SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::ts_diagnostics)
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::diagnostics)
