#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <shyft/dtss/db_cfg.h>
#include <shyft/dtss/db_deleter.h>
#include <shyft/dtss/ts_db_interface.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss {

  namespace detail {
    struct ts_db_rocks_impl; // fwd. detailed impl.
  }

  /**
   * @brief The shyft backend ts store using rocks db,  of google level db
   */
  struct ts_db_rocks : its_db {
    db_deleter delete_db_action; // defined before impl so it gets destroyed last
    std::unique_ptr<detail::ts_db_rocks_impl> impl;
    ts_db_rocks(std::string const &root_dir, db_cfg const &cfg);
    ~ts_db_rocks() override;
    //-- for debug/testing and behaviour verification (important)
    [[nodiscard]] std::uint64_t current_uid() const;
    [[nodiscard]] std::uint64_t clean_shutdown() const;
    [[nodiscard]] std::uint64_t sync_count() const;
    //-- the real business
    [[nodiscard]] std::string root_dir() const override;

    [[nodiscard]] diags_t save(
      std::string_view fn,
      gts_t const &ts,
      fx_cache_t fx_cache, // nullptr
      store_policy const &policy) override;
    [[nodiscard]] diags_t
      save(size_t n, fx_ts_item_t const &fx_item, fx_cache_t fx_cache, store_policy const &policy) override;
    [[nodiscard]] tuple<gts_t, utcperiod> read(std::string_view fn, utcperiod p) override;
    void remove(std::string_view fn) override;
    [[nodiscard]] ts_info get_ts_info(std::string_view fn) override;
    [[nodiscard]] std::vector<ts_info> find(std::string_view match) override;
    void maintain(bool const info_db, bool const data_db) const;
    [[nodiscard]] static bool exists_at(std::string const &root_dir);
    [[nodiscard]] static bool rollback_migration_at(std::string const &root_dir);

    void mark_for_deletion() override;
  };

}
