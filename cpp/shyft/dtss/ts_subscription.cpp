#include <shyft/dtss/ts_subscription.h>

namespace shyft::dtss::subscription {

  ts_observer::ts_observer(manager_ const & sm, string const & request_id)
    : observer_base(sm, request_id),global_change_version{sm->total_change_count.load()} {
  }

  ts_observer::~ts_observer() {
    // ok,the base class takes care of the manager subs part
    // protected by recursive mutex.
    // We only need to care about our internals
    published_versions.clear(); // not really needed, it goes by auto.
  }

  void ts_observer::subscribe_to(id_vector_t const & ts_ids, utcperiod const & read_period) {
    auto t = sm->add_subscriptions(ts_ids);
    for (auto const & i : t) {
      if (auto f = published_versions.find(i->id); f == published_versions.end()) {
        // New entry, set version and period
        published_versions[i->id] = ts_o{i->v, read_period, i};
        terminals.push_back(i);
      } else {
        // Extend current period on terminal
        utcperiod& p = f->second.p;
        p = utcperiod(std::min(p.start, read_period.start), std::max(p.end, read_period.end));
      }
    }
  }

  void ts_observer::unsubscribe(id_vector_t const & ts_ids) {
    vector<core::subscription::observable_> e;
    for (auto const & id : ts_ids) {
      if (auto f = published_versions.find(id); f != published_versions.end()) {
        auto ri = std::find_if(terminals.begin(), terminals.end(), [&id](auto const & x) {
          return id == x->id;
        });

        if (ri != terminals.end()) {
          e.emplace_back(*ri);
          terminals.erase(ri);
        }
        published_versions.erase(f);
      }
    }
    sm->remove_subscriptions(e);
  }

  bool ts_observer::recalculate() {
    return false;
  }

  unordered_map<utcperiod, std::vector<ts_o*>, utcperiod_hasher> ts_observer::find_changed_ts_o() {
    auto realized = sm->total_change_count.load();
    if (realized == global_change_version) { //no changes since last time, just return early
      return {};
    }
    global_change_version=realized;
    unordered_map<utcperiod, std::vector<ts_o*>, utcperiod_hasher> r;
    for (auto& p : published_versions) {
      if (p.second.v != p.second.o->v) {
        r[p.second.p].emplace_back(&p.second); // TODO: First time emplace, be sure to resize id_vector_t .. to
                                                    // lets say size(published_version)? worst case?
        p.second.v = p.second.o->v;
      }
    }
    return r;
  }

  unordered_map<utcperiod, id_vector_t, utcperiod_hasher> ts_observer::find_changed_ts() {
    auto r_tso=find_changed_ts_o();
    if(r_tso.empty())
      return {};
    unordered_map<utcperiod, id_vector_t, utcperiod_hasher> r;
    r.reserve(r_tso.size());
    for (auto& [p, v] : r_tso) {
      id_vector_t ts_ids;ts_ids.reserve(v.size());
      std::transform(v.begin(), v.end(), std::back_inserter(ts_ids), [](ts_o* ts) {
        return ts->o->id;
      });
      r[p]=ts_ids;
    }
    return r;
  }

}
