/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstddef>
#include <cstdint>

#include <boost/serialization/tracking.hpp>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>

namespace shyft::dtss {
  SHYFT_DEFINE_ENUM(lookup_error, std::uint8_t, (container_not_found, container_throws, unknown_schema))

  struct read_error {
    std::size_t index;
    lookup_error code;
    SHYFT_DEFINE_STRUCT(read_error, (), (index, code));
    auto operator<=>(read_error const &) const = default;
    x_serialize_decl();
  };
}

x_serialize_export_key_nt(shyft::dtss::read_error);

SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::lookup_error)
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::read_error)