/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once


#include <algorithm>
#include <atomic>
#include <cstdint>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <ranges>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <tuple>
#include <optional>
#include <string_view>

#include <shyft/core/core_serialization.h>
#include <shyft/core/lru_cache.h>
#include <shyft/core/optional.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/fx_merge.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::dtss {
  using std::size_t;
  using std::vector;
  using std::map;
  using std::unordered_map;
  using std::pair;
  using std::list;
  using std::string_view;
  using std::make_shared;
  using std::shared_ptr;
  using std::lower_bound;
  using std::upper_bound;
  using std::string;
  using std::mutex;
  using std::optional;
  using std::tuple;
  // using std::scoped_lock;
  using std::lock_guard;
  using std::min;
  using std::max;
  using std::runtime_error;
  using std::dynamic_pointer_cast;

  using shyft::core::utcperiod;
  using shyft::core::utctime;
  using shyft::time_series::merge;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::gta_t;
  using shyft::time_series::dd::gpoint_ts;
  using shyft::time_series::dd::ts_as;
  using shyft::core::lru_cache;

  template <typename T>
  concept ts_id_range = std::ranges::input_range<T> && std::is_same_v<std::ranges::range_value_t<T>, std::string>;

  using ts_frag=shared_ptr<gpoint_ts const>; ///< ts frags are immutables

  template <typename T>
  concept cache_update_range = std::ranges::input_range<T> && requires(std::ranges::range_reference_t<T> a) {
    { std::get<0>(a) } -> std::convertible_to<std::string>;
    { std::get<1>(a) } -> std::convertible_to<ts_frag>;
    { std::get<2>(a) } -> std::convertible_to<utcperiod>;
  };

  using ts_frag_tp=tuple<ts_frag,utcperiod>;///< ts_frag,total_period tuple that we return from cache
  /**
   * @brief make_merge returns a NEW apoint_ts_frag
   * @details
   * Performs a ts_merge of `a` fragment (high priority) with the `b`
   * such that the new fragment as minimum keeps this, plus extensions from
   * the other at none, one or both sides of this
   * @sa shyft::time_series::merge
   *
   * @param a the the high pri fragment
   * @param b the other low pri frag (only visible where a is not covering)
   * @return a new ts-fragment
   */
  [[nodiscard]] inline  auto make_merged (ts_frag const&a, ts_frag const&b ) {
    return make_shared<gpoint_ts const>(time_series::merge(a->rep, b->rep));
  }

  /**
   * @brief size_in_memory for a ts_frag
   */
  [[nodiscard]] inline auto size_in_memory(ts_frag const&x) {
    return x?x->size()*(x->time_axis().is_fixed_dt()?8u:16u):0u;//2x8 for breakpoint repr
  }
  /**
   * @brief mini_frag provides a container that minimizes the set of time-series fragments
   *
   * @details
   *  The purpose of this class is to provide a container that keeps
   *  a minimum set of ts-fragments, each with non-overlapping.total_period().
   *
   *  When adding a new fragment, the existing fragments are checked, and if it's
   *  possible to merge with already existing ts-fragments, this is done, and
   *  existing fragments is merged into the new one.
   *  The merge is assumed to put priority to the newly inserted fragment, so
   *  overlapping parts are replaced with the new value-fragment.
   *
   */
  struct mini_frag {
    utcperiod tp;      ///< total period of underlying ts, so that we can avoid futile read requests
    vector<ts_frag> f; ///< fragments, ordered by .total_period().start, non-overlapping, disjoint periods

    /**
     * @brief get frag that matches p
     *
     * @details
     * Clips the supplied period to the total period of the underlying time-series.
     * Then find the frag that covers the resulting period.
     * Later we can consider also supporting partial/completing reads,
     * but it is a bit more involved algorithm.
     * IF found match a shared_ptr is returned(incref).
     *
     * @param p
     *  The requested period to cover.
     *
     * @returns empty or tuple<ts_frag,tp>
     *
     */
    optional<tuple<ts_frag,utcperiod>> get_frag(utcperiod p) const {
      if(tp.valid()) {//important: for external backend, we do not yet support total_period concept
        p=intersection(p,tp); // clip within possible limits, but only if we know them!
      }
      if(!p.valid()) // or empty() as well?
        return {};// leave early if no hope
      auto r = lower_bound(begin(f), end(f), p.start, [](ts_frag const & x, utctime t) {
        return x->total_period().start <= t;
      });
      size_t i = static_cast<size_t>(r - begin(f)) - 1;
      if (i == string::npos || !f[i]->total_period().contains(p))
        return {};
      //FIXME: consider make a slice of f[i] if p is small compared to tp(give tp is valid)
      return make_tuple(f[i],tp); // incref here, maybe a notch early, move needed on next handling
    }


    /** return number of fragments */
    size_t count_fragments() const {
      return f.size();
    }

    /** @return the accumulated .size() for all fragments, x8  ~ approx. bytes */
    size_t estimate_size() const {
      size_t s = 0;
      for (auto const & x : f)
        s += size_in_memory(x);
      return s/8;// bw compat, return size in units of number of double/8bytes units
    }

    /**
     * @brief add a new fragment to the container
     * @details
     * ensures that the internal container remains ordered by .total_period().start
     * and that all periods are disjoint (not overlapping, not touching ends)
     *
     * @param tsf
     *   the new time-series fragment to be added into the container.
     * @param tp_update
     *   the update of the total period covered by this time-series
     */
    void add(ts_frag const & tsf, utcperiod tp_update) {
      tp=tp_update;// update the total period now covered. assumes it is correct here.
      auto p = tsf->total_period();
      auto p1 = lower_bound(begin(f), end(f), p.start, [](ts_frag const & x, utctime const & t) -> bool {
        return x->total_period().end < t;
      });
      if (p1 == end(f)) { // entirely after last (if any) elements
        f.emplace_back(tsf); //incref. takes ownership here
        return;
      }
      if (p.end < (*p1)->total_period().start) { // entirely before first.
        f.insert(p1, tsf);
        return;
      }
      auto p2 = upper_bound(p1, end(f), p.end, [](utctime const & t, ts_frag const & x) {
        return t < x->total_period().end;
      });                                        // figure out upper bound element
      if (p.start <= (*p1)->total_period().start) { // p1 completely covered
        if (p2 == end(f)) {                      //  p2  also completely covered
          *p1 = tsf;
          f.erase(p1 + 1, p2);
          return;
        }
        // parts of p2 must be merged
        if ((*p2)->total_period().start <= p.end) { // overlap, - we merge with p2
          *p1 = make_merged(tsf,*p2);
          f.erase(p1 + 1, p2 + 1); // and consumes p2
        } else {
          *p1 = tsf;
          f.erase(p1 + 1, p2); // p2 is above, so we merge until p2
        }
      } else {              // parts of p1 must be merged
        if (p2 == end(f)) { // now look at p2
          *p1 = make_merged(tsf,*p1);
          f.erase(p1 + 1, p2); // parts of p1 merged, p2 vanishes
          return;
        }
        if ((*p2)->total_period().start <= p.end) {
          *p1 = make_merged(make_merged(tsf,*p1),*p2);
          f.erase(p1 + 1, p2 + 1); // both p1 and p2 merged.
        } else {
          *p1 = make_merged(tsf,*p1);
          f.erase(p1 + 1, p2); // p2 is above p.end, p2 must remain
        }
      }
    }
  };



  /** cache stats for performance measures */
  struct cache_stats {
    cache_stats() = default;
    // TODO: Add size_t memory_target{ 0 };
    size_t hits{0};            ///< accumulated hits by id
    size_t misses{0};          ///< accumulated misses by id
    size_t coverage_misses{0}; ///< accumulated misses for period-coverage (the id exists, but not requested period)
    size_t id_count{0};        ///< current count of distinct ts-ids in the cache
    size_t point_count{0};     ///< current estimate of ts-points in the cache, one point ~8 bytes
    size_t fragment_count{0};  ///< current count of ts-fragments, equal or larger than id_count

    /** zero out counted items in the cache_stats */
    void flush() {
      id_count = point_count = fragment_count = 0;
    }

    /** zero out the hits/misses */
    void reset_stats() {
      hits = misses = coverage_misses = 0;
    }

    [[nodiscard]] size_t size_in_bytes() const noexcept {
      return point_count * 8;
    }

    auto operator<=>(cache_stats const &) const = default;

    /** nice to have summary function */
    friend inline cache_stats operator+(cache_stats l, cache_stats const & r) {
      l.hits += r.hits;
      l.misses += r.misses;
      l.coverage_misses += r.coverage_misses;
      l.id_count += r.id_count;
      l.point_count += r.point_count;
      l.fragment_count += r.fragment_count;
      return l;
    }

    x_serialize_decl();
  };

  /**
   * @brief a dtss cache for id-based ts-fragments
   * @details
   * Provides thread-safe:
   *	 .cache( (id|ts) | (ids| tsv) | tsv<ref-ts>)
   *  .try_get( id | ids ) -> vector<id> (null or real)
   *  .remove( (id|ids))
   *  .statistics (..)->stat
   *  .size(#n ts, # max elems)
   *
   * key-value based lru cache
   *  key-type: string, url type as passed
   * 	value-type: mini_frag<ts_frag>
   *
   * maintain by lru, - where the value-type (ts_frag) is maintained as a
   *                    minimal set of non-overlapping disjoint ts-fragments.
   *
   * @sa lru_cache
   * @sa cache_stats
   * @sa apoints_ts_frag
   *
   */
  struct cache {
    using internal_cache = lru_cache<string, mini_frag, unordered_map>;
   private:
    mutable mutex mx; ///< mutex to protect access to c and cs
    internal_cache c; ///< internal cache implementation
    cache_stats cs; ///< internal cache stats to collect misses/hits and keeps the size of cache, incrementally updated
                    ///< on put/get/remove
    size_t mem_max{0};                          ///< we try to keep the effective cache size less than this
    size_t avg_ts_sz{3 * 10000 * 8};            ///< average ts tsize in cache.
    static constexpr const size_t min_cap = 10; ///< minimum cache-cap.
    static constexpr const size_t min_mem_target = 10 * 1024;
   public:
    /**
     * @brief set the average ts size estimate
     *
     * @details
     *
     * The initial ts average estimate size determines the
     * target memory max target in the internal_adjust_cache
     * if there is not enough data to compute it automagically.
     *
     */
    void set_ts_size(size_t avg_sz) {
      lock_guard guard(mx);
      if (avg_sz < 1)
        throw runtime_error("ts_cache: average ts-size estimate must be >0");
      avg_ts_sz = avg_sz;
      internal_adjust_cache_size();
    }

    /** returns the initial ts estimate in bytes */
    size_t get_ts_size() const {
      return avg_ts_sz;
    }

    /**
     * @brief Set the memory max-target for the cache.
     *
     * @details
     * This directly set the memory-target for the cache,
     * and adjusts the number of time-series in the cache
     * accordingly, to meet this upper target.
     *
     * After setting the memory-target, adjustment of the
     * available ts-items in the cache is done.
     * If there is enough data to do some statistics (>min_cap items),
     * then an 'accurate' number of items in cache is computed.
     * Otherwise, we use the avg_ts_sz estimate given by the user
     * to set the most likely number of items.
     * As data is added to the cache, this number is
     * adjusted according to the average size/actual size of the
     * cache.
     */
    void set_mem_max(size_t mem_max_bytes) {
      lock_guard guard(mx);
      if (mem_max_bytes < min_mem_target)
        throw runtime_error("ts_cache: memory max target should be > bytes" + std::to_string(min_mem_target));
      mem_max = mem_max_bytes;
      internal_adjust_cache_size();
    }

    size_t get_mem_max() const {
      lock_guard guard(mx);
      return mem_max;
    }

    /** compute and return new capacity based on statistics or estimates
     *
     * public for testing only(lacks a lock on purpose)
     */
    size_t compute_capacity() const {
      size_t cap;
      if (cs.id_count > std::max(min_cap, c.get_capacity() / 10))
        cap = mem_max / (cs.size_in_bytes() / cs.id_count);
      else
        cap = avg_ts_sz > 1 ? mem_max / avg_ts_sz : min_cap;
      return cap < min_cap ? min_cap : cap;
    }
   private:
    /** auto adjust capacity
     *
     * if content of cache large enough, that is:
     *   number of ts > max(1000, c.capacity/4), we require 1000 ts as min.estimate
     *
     * then compute real average ts-size
     *   and let cap of cache be set to
     *   new_cap = min_cap + mem_max/cs.size_in_bytes()
     *   c.set_capacity(new_cap)
     * else
     *   new_cap = min_cap + mem_max/
     *
     */
    void internal_adjust_cache_size() {
      auto new_cap = compute_capacity();
      c.set_capacity(new_cap); // will call evict if needed
    }

    /** get one single item from cache, if exists, and matches period, record hits/misses */
    optional<tuple<ts_frag,utcperiod>> internal_try_get(string const& id, utcperiod const & p) {
      if (!c.item_exists(id)) {
        ++cs.misses;
        return {};
      }
      ++cs.hits;
      auto const & mf = c.get_item(id);
      auto f=mf.get_frag(p);
      if (!f) {
        ++cs.coverage_misses;
        return {};
      }
      return f;
    }

    /** add one single item to cache, defrag if already there */
    void internal_add(string const & id, ts_frag const & ts, utcperiod const&tp) {
      if (!ts || ts->size() == 0)
        return;
      if (!c.item_exists(id)) {
        mini_frag mf{tp,{ts}}; //incref goes here
        cs.point_count += mf.estimate_size();
        cs.id_count++;
        cs.fragment_count++;
        c.add_item(id, mf); // if this causes eviction, the to_be_removed below is called
        // FIXME: the c.add_item takes a const ref, that will give one unneeded incref
      } else {
        auto& mf = c.get_item(id);
        auto tmp_size = mf.estimate_size();
        auto tmp_frags = mf.count_fragments();
        mf.add(ts,tp);
        cs.point_count += mf.estimate_size() - tmp_size;
        cs.fragment_count += mf.count_fragments() - tmp_frags;
      }
      internal_adjust_cache_size(); //
    }

    std::atomic_int_fast64_t count_evicted{0}; ///< indication of ts ids going out from cache for master-slave sync

    /** Callback used in lru_cache, to report item to be removed when evicted from cache */
    void to_be_removed(mini_frag const & mf) {
      cs.point_count -= mf.estimate_size();
      cs.id_count--;
      cs.fragment_count -= mf.count_fragments();
      ++count_evicted;
    }
   public:
    /** @brief get the total evicted count so that master-slave sync worker get a hint */
    size_t get_total_evicted_count() const {
      return (size_t) count_evicted.load();
    }


   public:
    /** construct a cache with max ts-id count */
    explicit cache(size_t id_max_count, size_t avg_ts_size = 8 * 3 * 10000)
      : c(id_max_count, [this](mini_frag const & x) {
        to_be_removed(x);
      }) {
      avg_ts_sz = avg_ts_size;
      mem_max = id_max_count * avg_ts_sz;
    }

    /**
     * @brief adjust the cache capacity
     *
     * Set the capacity related to unique ts-ids to specified count.
     * If adjusting down, elements are evicted from cache in lru-order.
     *
     * @param id_max_count the new maximium number of unique ts-ids to keep
     *
     */
    void set_capacity(size_t id_max_count) {
      lock_guard guard(mx);
      c.set_capacity(id_max_count);
    }

    size_t get_capacity() const {
      lock_guard guard(mx);
      return c.get_capacity();
    }

    /**
     * @brief try get a ts that matches id and period.
     *
     * @sa get
     *
     * @param id any valid time-series id
     * @param p  period specification
     * @return optional<tuple<tsfrag,totperiod> if  found and matches period
     */
    auto try_get(string const & id, utcperiod const & p) {
      lock_guard guard(mx);
      return internal_try_get(id, p);
    }

    /**
     * @brief get out a list of matching ts from cache
     *
     * @details
     *
     * Thread-safe get out cache content by callable ts-id and period specification
     * We provide this with callables to allow the user to
     * avoid generating long vectors of names, and to support
     * different period for each item i.
     * This is typically needed for geo-enabled services with
     * multimillion number of time-series entries,
     * where we also need to know which items is *not* matching
     * the cache and take action accordingly.
     *
     * @sa try_get
     * @tparam TSID a callable type with signature string(size_t i)
     * @tparam TSP  a callable type with signature utcperiod(size_t i)
     * @tparam FCB  a callable type with signature void(size_t i, bool, ts_frag, utcperiod)
     *
     * @param n number of items, will call ids,p,found n-times
     * @param ids a callable of type TSID
     * @param p a callable of type TSP
     * @param found a callable of type FCB, called for each item i with optional<utcperiod,ts_frag>
     */
    template <class TSID, class TSP, class FCB>
    void get(size_t n, TSID&& tsid, TSP&& p, FCB&& found) {
      lock_guard guard(mx);
      for (size_t i = 0; i < n; ++i) {
        auto found_it = internal_try_get(tsid(i), p(i));
        if(found_it.has_value()) //moving the shared pointer to the arg list
          found(i, true, std::move(std::get<0>(*found_it)),std::get<1>(*found_it));
        else
          found(i, false, nullptr, {});
      }
    }



    /**
     * @brief call function on all ts in cache, nullopt if not in cache
     *
     * thread-safe get out cache content by ts-id and period specification
     * Can be assumed to call with index in ascending order.
     *
     * @sa try_get
     *
     * @param ids an input range of ts-ids to be fetched
     * @param p specifies the period requirement
     * @param function invocable object to get called with (ts_id, index, std::optional<tsfrag_tp>) for each id
     */

    template <typename Function>
    requires std::invocable<Function, std::string const &, std::size_t, std::optional< tuple<ts_frag,utcperiod>> >
    void get_with(ts_id_range auto&& ts_ids, utcperiod const & p, Function&& function) {
      std::lock_guard guard(mx);
      std::ranges::for_each(std::views::enumerate(ts_ids), [&](auto const & el) {
        auto const& [index, id] = el;
        auto ts_tp= internal_try_get(id, p);
        std::invoke(SHYFT_FWD(function), id, index, std::move(ts_tp));
      });
    }

    /**
     * @brief get out a list of ts by specified id and period from cache
     *
     * thread-safe get out cache content by ts-id and period specification
     *
     * @sa try_get
     *
     * @param ids an input range of ts-ids to be fetched
     * @param p specifies the period requirement
     * @return a map<string,ts_t> with the time-series from cache that matches the criteria
     */
    std::unordered_map<std::string, tuple<ts_frag,utcperiod>>
      get(ts_id_range auto&& ts_ids, utcperiod const & p) {
      std::unordered_map<std::string, tuple<ts_frag,utcperiod>> r;
      std::lock_guard guard(mx);
      std::ranges::for_each(ts_ids, [&](auto const & id) {
        auto ts_tp= internal_try_get(id,p);
        if (ts_tp.has_value())
          r[id] = std::move(*ts_tp); // keep only those found
      });
      return r;
    }

    /**
     * @brief add (id,ts) to cache
     *
     * Adds or replaces id,ts pair into the cache,
     * possibly merge fragments if that id have fragments already present in the
     * cache
     *
     * @sa add vector
     *
     * @param id any valid time-series id
     * @param ts a const ref to ts (fragment) to be added/replaced in the cache
     *
     */
    void add(string const & id, ts_frag const & ts, utcperiod const&p) {
      lock_guard guard(mx);
      internal_add(id, ts,p);
    }
    /**
     * @brief update the cache with a range
     *
     * @details
     *
     * Thread-safe update cache content by range[tuple[ts_id, ts,utcperiod]]
     *
     * Each element is accessed once.
     *
     * The update can any combination of
     *  cache|remove
     *   t      f     - merge-add to cache,
     *   t      t     - remove then add to cache (replace)
     *   f      t     - remove item
     *   f      f     - noop (no callbacks invoked)
     *
     *
     * @sa get
     * @param ts_update a range of type tuple[string_like, ts_like]
     * @param cache_ if true, add items to cache
     * @param remove if true, remove item before eventual add cache
     */
    void update(cache_update_range auto&& ts_update, bool cache_, bool remove) {
      if (!cache_ && !remove)
        return; // noop supported
      lock_guard guard(mx);
      std::ranges::for_each(ts_update, [&](auto const & elements) {
        const auto& [name, ts, tp] = elements;
        if (remove)
          c.remove_item(name);
        if (cache_)
          internal_add(name, ts, tp);
      });
    }

    /** @brief add a range of tuple[tsid,time-series] to cache
     *
     * Iterate over the pairs
     * and add/replace those items to cache in the ascending index order.
     *
     * @param ts_update a range of valid time-series identifiers and time-series
     * @param replace if true, then flush cache, and replace
     */
    void add(cache_update_range auto&& ts_update, bool replace = false) {
      update(ts_update, true, replace);
    }

    /**
     * @brief remove a specified ts-id from cache
     *
     * If the id does not exits, this call has null-effect
     *
     * @param id any valid time-series id
     */
    void remove(string const & id) {
      lock_guard guard(mx);
      c.remove_item(id);
    }

    /** @brief remove specified ts-ids from cache
     *
     * If one or more of the  id does not exits, this is ignored(not an error)
     *
     * @param ids a list of valid time-series id
     */
    void remove(vector<string> const & ids) {
      lock_guard guard(mx);
      for (auto const & id : ids)
        c.remove_item(id);
    }

    /** @brief flushes the cache
     *
     * All elements are removed from cache, resources released
     *
     *  the  cache-statistics (cs) is cleared
     */
    void flush() {
      lock_guard guard(mx);
      auto sz = c.size();
      c.flush();
      cs.flush();
      count_evicted += sz;
    }

    /**
     * @brief provides a list of current ts-ids in cache
     *
     */
    vector<std::string> get_keys() const {
      lock_guard guard(mx);
      vector<std::string> r;
      r.reserve(c.size());
      c.get_mru_keys(std::back_inserter(r));
      return r;
    }

    /**
     * @brief Provide cache-statistics
     *
     * @return cache_stats with accumulated hits/misses as well as current id-count and point-count
     */
    cache_stats get_cache_stats() {
      lock_guard guard(mx);
      cache_stats r{cs};
      return r;
    }

    /** clear accumulated cache-stats */
    void clear_cache_stats() {
      lock_guard guard(mx);
      cs.reset_stats(); // stats only, keep id,frag and point-count
    }
  };
}

x_serialize_export_key(shyft::dtss::cache_stats)
