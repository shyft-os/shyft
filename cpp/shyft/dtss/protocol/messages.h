#pragma once
#include <cstdint>
#include <iostream>
#include <optional>
#include <string>
#include <vector>

#include <boost/archive/basic_archive.hpp>
#include <boost/mp11/algorithm.hpp>

#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/protocol.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/core/serialize_blob.h>
#include <shyft/core/utility.h>
#include <shyft/dtss/db_cfg.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_error.h>
#include <shyft/dtss/exchange/protocol.h>
#include <shyft/dtss/geo.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/q_bridge/config.h>
#include <shyft/dtss/q_bridge/status.h>
#include <shyft/dtss/queue_msg.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/transfer/config.h>
#include <shyft/dtss/transfer/status.h>
#include <shyft/dtss/url_ts_frag.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/expression_serialization.h>
#include <shyft/time_series/time_axis.h>

namespace shyft {
  namespace dtss {

    struct ostream_archive {

      std::ostream &stream;
      boost::archive::archive_flags flags = core::core_arch_flags;

      ostream_archive(std::ostream &stream)
        : stream(stream) {
      }

      template <reflected_struct T>
      void operator<<(T &message) {
        if constexpr (!reflection::empty_struct<T>) {
          core::core_oarchive oa(stream, flags);
          using members =
            boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>;
          boost::mp11::mp_for_each<members>([&](auto mem) {
            oa << std::invoke(mem.pointer, message);
          });
        }
      }

      template <auto m>
      requires(
        is_protocol(m.protocol) && protocols::is_internal_message<m> && m.tag == message_tags<m.version>::FIND_TS)
      void operator<<(protocols::request<m> &req) {
        reflection::write_string<protocols::compatibility<protocol>>(stream, req.search_expression);
      }

      template <auto m>
      requires(
        is_protocol(m.protocol) && protocols::is_internal_message<m>
        && m.tag == any_of(message_tags<m.version>::GET_TS_INFO, message_tags<m.version>::REMOVE_TS))
      void operator<<(protocols::request<m> &req) {
        reflection::write_string<protocols::compatibility<protocol>>(stream, req.url);
      }

      template <auto m>
      requires(
        is_protocol(m.protocol) && protocols::is_internal_message<m> && m.tag == message_tags<m.version>::STORE_TS_2)
      void operator<<(protocols::request<m> &req) {
        core::core_oarchive oa(stream, flags);
        oa << static_cast<std::uint64_t>(req.fragments.size());
        for (auto &f : req.fragments)
          oa << f.url << f.ts.fx_policy << f.ts.ta << f.ts.v;
        oa << req.policy;
      }

      template <auto m>
      requires(is_protocol(m.protocol) && m.tag == message_tags<m.version>::TRY_SLAVE_READ)
      void operator<<(protocols::reply<m> &rep) {
        core::core_oarchive oa(stream, flags);
        time_series::dd::serialize_save_no_tracking(oa, rep.fragments);
        core::store_blob(oa, rep.periods);
        core::store_blob(oa, rep.errors);
        time_series::dd::serialize_save_no_tracking(oa, rep.fragment_updates);
        core::store_blob(oa, rep.period_updates);
      }

      template <auto m>
      requires(is_protocol(m.protocol) && m.tag == message_tags<m.version>::SLAVE_READ_SUBSCRIPTION)
      void operator<<(protocols::reply<m> &rep) {
        core::core_oarchive oa(stream, flags);
        time_series::dd::serialize_save_no_tracking(oa, rep.series);
        core::store_blob(oa, rep.periods);
      }
    };

    struct istream_archive {

      std::istream &stream;
      boost::archive::archive_flags flags = core::core_arch_flags;

      istream_archive(std::istream &stream)
        : stream(stream) {
      }

      template <reflected_struct T>
      void operator>>(T &message) {
        if constexpr (!reflection::empty_struct<T>) {
          core::core_iarchive ia(stream, flags);
          using members =
            boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>;
          boost::mp11::mp_for_each<members>([&](auto mem) {
            ia >> std::invoke(mem.pointer, message);
          });
        }
      }

      template <auto m>
      requires(
        is_protocol(m.protocol) && protocols::is_internal_message<m> && m.tag == message_tags<m.version>::FIND_TS)
      void operator>>(protocols::request<m> &req) {
        auto s = reflection::try_read_string<protocols::compatibility<protocol>>(stream);
        if (!s)
          throw dlib::socket_error("failed reading error message");
        req.search_expression = std::move(*s);
      }

      template <auto m>
      requires(
        is_protocol(m.protocol) && protocols::is_internal_message<m>
        && m.tag == any_of(messages<m.version>::tags::GET_TS_INFO, messages<m.version>::tags::REMOVE_TS))
      void operator>>(protocols::request<m> &req) {
        auto s = reflection::try_read_string<protocols::compatibility<protocol>>(stream);
        if (!s)
          throw dlib::socket_error("failed reading error message");
        req.url = std::move(*s);
      }

      template <auto m>
      requires(
        is_protocol(m.protocol) && protocols::is_internal_message<m> && m.tag == message_tags<m.version>::STORE_TS_2)
      void operator>>(protocols::request<m> &req) {
        core::core_iarchive ia(stream, flags);
        std::uint64_t n;
        ia >> n;
        req.fragments.resize(n);
        for (auto &f : req.fragments)
          ia >> f.url >> f.ts.fx_policy >> f.ts.ta >> f.ts.v;
        ia >> req.policy;
      }

      template <auto m>
      requires(is_protocol(m.protocol) && m.tag == message_tags<m.version>::TRY_SLAVE_READ)
      void operator>>(protocols::reply<m> &rep) {
        core::core_iarchive ia(stream, flags);
        serialize_load_no_tracking(ia, rep.fragments);
        core::load_blob(ia, rep.periods);
        core::load_blob(ia, rep.errors);
        serialize_load_no_tracking(ia, rep.fragment_updates);
        core::load_blob(ia, rep.period_updates);
      }

      template <auto m>
      requires(is_protocol(m.protocol) && m.tag == message_tags<m.version>::SLAVE_READ_SUBSCRIPTION)
      void operator>>(protocols::reply<m> &rep) {
        core::core_iarchive ia(stream, flags);
        serialize_load_no_tracking(ia, rep.series);
        core::load_blob(ia, rep.periods);
      }
    };
  }

  namespace protocols {
    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_TS_VECTOR)
    struct request<m> {
      core::utcperiod period;
      time_series::dd::ats_vector tsv;
      bool use_ts_cached_read;
      bool update_ts_cache;
      SHYFT_DEFINE_STRUCT(request, (), (period, tsv, use_ts_cached_read, update_ts_cache));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_CLIP)
    struct request<m> {
      core::utcperiod period;
      time_series::dd::ats_vector tsv;
      bool use_ts_cached_read;
      bool update_ts_cache;
      core::utcperiod clip_period;
      SHYFT_DEFINE_STRUCT(request, (), (period, tsv, use_ts_cached_read, update_ts_cache, clip_period));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_EXPRESSION)
    struct request<m> {
      core::utcperiod period;
      time_series::dd::compressed_ts_expression compressed_expression;
      bool use_ts_cached_read;
      bool update_ts_cache;
      SHYFT_DEFINE_STRUCT(request, (), (period, compressed_expression, use_ts_cached_read, update_ts_cache));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_EXPRESSION_CLIP)
    struct request<m> {
      core::utcperiod period;
      time_series::dd::compressed_ts_expression compressed_expression;
      bool use_ts_cached_read;
      bool update_ts_cache;
      core::utcperiod clip_period;
      SHYFT_DEFINE_STRUCT(
        request,
        (),
        (period, compressed_expression, use_ts_cached_read, update_ts_cache, clip_period));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && is_internal_message<m>
             && m.tag
                  == any_of(
                    dtss::message_tags<m.version>::EVALUATE_TS_VECTOR,
                    dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_CLIP,
                    dtss::message_tags<m.version>::EVALUATE_EXPRESSION,
                    dtss::message_tags<m.version>::EVALUATE_EXPRESSION_CLIP))
    inline constexpr auto message_reply_tag<m> = etoi(dtss::message_tags<m.version>::EVALUATE_TS_VECTOR);

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m>
      && m.tag
           == any_of(
             dtss::message_tags<m.version>::EVALUATE_TS_VECTOR,
             dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_CLIP,
             dtss::message_tags<m.version>::EVALUATE_EXPRESSION,
             dtss::message_tags<m.version>::EVALUATE_EXPRESSION_CLIP))
    struct reply<m> {
      time_series::dd::ats_vector result;
      SHYFT_DEFINE_STRUCT(reply, (), (result));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_PERCENTILES)
    struct request<m> {
      core::utcperiod period;
      time_series::dd::ats_vector tsv;
      time_axis::generic_dt ta;
      std::vector<std::int64_t> percentile_spec;
      bool use_ts_cached_read;
      bool update_ts_cache;
      SHYFT_DEFINE_STRUCT(request, (), (period, tsv, ta, percentile_spec, use_ts_cached_read, update_ts_cache));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_EXPRESSION_PERCENTILES)
    struct request<m> {
      core::utcperiod period;
      time_series::dd::compressed_ts_expression compressed_expression;
      time_axis::generic_dt ta;
      std::vector<std::int64_t> percentile_spec;
      bool use_ts_cached_read;
      bool update_ts_cache;
      SHYFT_DEFINE_STRUCT(
        request,
        (),
        (period, compressed_expression, ta, percentile_spec, use_ts_cached_read, update_ts_cache));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && is_internal_message<m>
             && m.tag
                  == any_of(
                    dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_PERCENTILES,
                    dtss::message_tags<m.version>::EVALUATE_EXPRESSION_PERCENTILES))
    inline constexpr auto message_reply_tag<m> = etoi(dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_PERCENTILES);

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m>
      && m.tag
           == any_of(
             dtss::message_tags<m.version>::EVALUATE_TS_VECTOR_PERCENTILES,
             dtss::message_tags<m.version>::EVALUATE_EXPRESSION_PERCENTILES))
    struct reply<m> {
      time_series::dd::ats_vector percentiles;
      SHYFT_DEFINE_STRUCT(reply, (), (percentiles));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::FIND_TS)
    struct request<m> {
      std::string search_expression;
      SHYFT_DEFINE_STRUCT(request, (), (search_expression));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::FIND_TS)
    struct reply<m> {
      std::vector<dtss::ts_info> infos;
      SHYFT_DEFINE_STRUCT(reply, (), (infos));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && m.tag == any_of(dtss::message_tags<m.version>::GET_TS_INFO, dtss::message_tags<m.version>::REMOVE_TS))
    struct request<m> {
      std::string url;
      SHYFT_DEFINE_STRUCT(request, (), (url));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::GET_TS_INFO)
    struct reply<m> {
      dtss::ts_info info;
      SHYFT_DEFINE_STRUCT(reply, (), (info));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::CACHE_STATS)
    struct reply<m> {
      dtss::cache_stats stats;
      SHYFT_DEFINE_STRUCT(reply, (), (stats));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::MERGE_STORE_TS)
    struct request<m> {
      time_series::dd::ats_vector tsv;
      bool cache;
      SHYFT_DEFINE_STRUCT(request, (), (tsv, cache));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.version >= 1 && m.tag == dtss::message_tags<m.version>::MERGE_STORE_TS)
    struct request<m> {
      std::vector<dtss::url_ts_frag> fragments;
      bool cache;
      SHYFT_DEFINE_STRUCT(request, (), (fragments, cache));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_GEO)
    struct request<m> {
      dtss::geo::eval_args arguments;
      bool cache_read;
      bool cache_write;
      SHYFT_DEFINE_STRUCT(request, (), (arguments, cache_read, cache_write));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::EVALUATE_GEO)
    struct reply<m> {
      dtss::geo::geo_ts_matrix matrix;
      SHYFT_DEFINE_STRUCT(reply, (), (matrix));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_GEO_INFO)
    struct reply<m> {
      std::vector<dtss::geo::ts_db_config_> configs;
      SHYFT_DEFINE_STRUCT(reply, (), (configs));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::STORE_GEO)
    struct request<m> {
      std::string db_name;
      dtss::geo::ts_matrix matrix;
      bool replace;
      bool cache_write;
      SHYFT_DEFINE_STRUCT(request, (), (db_name, matrix, replace, cache_write));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::ADD_GEO_DB)
    struct request<m> {
      std::shared_ptr<dtss::geo::ts_db_config> config;
      SHYFT_DEFINE_STRUCT(request, (), (config));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && ((m.tag == any_of(dtss::message_tags<m.version>::Q_SIZE, dtss::message_tags<m.version>::Q_INFOS)) || (is_internal_message<m> && m.tag == any_of(dtss::message_tags<m.version>::Q_ADD, dtss::message_tags<m.version>::REMOVE_GEO_DB, dtss::message_tags<m.version>::Q_REMOVE))))
    struct request<m> {
      std::string name;
      SHYFT_DEFINE_STRUCT(request, (), (name));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::GET_CONTAINERS)
    struct reply<m> {
      std::vector<std::string> names;
      SHYFT_DEFINE_STRUCT(reply, (), (names));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::SLAVE_UNSUBSCRIBE)
    struct request<m> {
      std::vector<std::string> ids;
      SHYFT_DEFINE_STRUCT(request, (), (ids));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::SLAVE_READ_SUBSCRIPTION)
    struct reply<m> {
      std::vector<time_series::dd::aref_ts> series;
      std::vector<core::utcperiod> periods;
      SHYFT_DEFINE_STRUCT(reply, (), (series, periods));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::GET_VERSION)
    struct reply<m> {
      std::string shyft_version;
      SHYFT_DEFINE_STRUCT(reply, (), (shyft_version));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_LIST)
    struct reply<m> {
      std::vector<std::string> queues;
      SHYFT_DEFINE_STRUCT(reply, (), (queues));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_INFOS)
    struct reply<m> {
      std::vector<dtss::queue::msg_info> infos;
      SHYFT_DEFINE_STRUCT(reply, (), (infos));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_INFO)
    struct request<m> {
      std::string name;
      std::string message_id;
      SHYFT_DEFINE_STRUCT(request, (), (name, message_id));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_INFO)
    struct reply<m> {
      dtss::queue::msg_info info;
      SHYFT_DEFINE_STRUCT(reply, (), (info));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::Q_PUT)
    struct request<m> {
      std::string name;
      std::string message_id;
      std::string description;
      core::utctime ttl;
      time_series::dd::ats_vector tsv;
      SHYFT_DEFINE_STRUCT(request, (), (name, message_id, description, ttl, tsv));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.version >= 1 && m.tag == dtss::message_tags<m.version>::Q_PUT)
    struct request<m> {
      std::string name;
      std::string message_id;
      std::string description;
      core::utctime ttl;
      std::vector<dtss::url_ts_frag> fragments;
      SHYFT_DEFINE_STRUCT(request, (), (name, message_id, description, ttl, fragments));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_GET)
    struct request<m> {
      std::string name;
      core::utctime max_wait;
      SHYFT_DEFINE_STRUCT(request, (), (name, max_wait));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::Q_GET)
    struct reply<m> {
      std::shared_ptr<dtss::queue::tsv_msg> message;
      SHYFT_DEFINE_STRUCT(reply, (), (message));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && (is_internal_message<m> || (m.version > 1))
      && m.tag == dtss::message_tags<m.version>::Q_FIND)
    struct reply<m> {
      std::optional<dtss::queue::msg_info> info;
      std::vector<dtss::url_ts_frag> fragments;
      SHYFT_DEFINE_STRUCT(reply, (), (info, fragments));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.version >= 1 && m.tag == dtss::message_tags<m.version>::Q_GET)
    struct reply<m> {
      dtss::queue::msg_info info;
      std::vector<dtss::url_ts_frag> fragments;
      SHYFT_DEFINE_STRUCT(reply, (), (info, fragments));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && (is_internal_message<m> || m.version > 1)
      && (m.tag == dtss::message_tags<m.version>::Q_FIND))
    struct request<m> {
      std::string name, message_id;
      SHYFT_DEFINE_STRUCT(request, (), (name, message_id));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_ACK)
    struct request<m> {
      std::string name;
      std::string message_id;
      std::string diagnosis;
      SHYFT_DEFINE_STRUCT(request, (), (name, message_id, diagnosis));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_SIZE)
    struct reply<m> {
      std::size_t size;
      SHYFT_DEFINE_STRUCT(reply, (), (size));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::Q_MAINTAIN)
    struct request<m> {
      std::string name;
      bool keep_ttl_items;
      bool flush;
      SHYFT_DEFINE_STRUCT(request, (), (name, keep_ttl_items, flush));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::SET_CONTAINER)
    struct request<m> {
      std::string name;
      std::string path;
      std::string type;
      dtss::db_cfg config;
      SHYFT_DEFINE_STRUCT(request, (), (name, path, type, config));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::REMOVE_CONTAINER)
    struct request<m> {
      std::string url;
      bool remove_from_disk;
      SHYFT_DEFINE_STRUCT(request, (), (url, remove_from_disk));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::SWAP_CONTAINER)
    struct request<m> {
      std::string name_a;
      std::string name_b;
      SHYFT_DEFINE_STRUCT(request, (), (name_a, name_b));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::TRY_SLAVE_READ)
    struct request<m> {
      std::vector<std::string> ids;
      core::utcperiod period;
      bool cache;
      bool subscribe;
      SHYFT_DEFINE_STRUCT(request, (), (ids, period, cache, subscribe));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::TRY_SLAVE_READ)
    struct reply<m> {
      std::vector<dtss::ts_frag> fragments;
      std::vector<core::utcperiod> periods;
      std::vector<time_series::dd::aref_ts> fragment_updates;
      std::vector<core::utcperiod> period_updates;
      std::vector<dtss::read_error> errors;
      SHYFT_DEFINE_STRUCT(reply, (), (fragments, periods, fragment_updates, period_updates, errors));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::UPDATE_GEO_DB)
    struct request<m> {
      std::string name;
      std::string description;
      std::string json;
      std::string origin;
      SHYFT_DEFINE_STRUCT(request, (), (name, description, json, origin));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::STORE_TS_2)
    struct request<m> {
      dtss::store_policy policy;
      std::vector<dtss::url_ts_frag> fragments;
      SHYFT_DEFINE_STRUCT(request, (), (policy, fragments));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::STORE_TS_2)
    struct reply<m> {
      std::optional<std::vector<dtss::diagnostics>> diagnostics;
      SHYFT_DEFINE_STRUCT(reply, (), (diagnostics));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::START_TRANSFER)
    struct request<m> {
      dtss::transfer::configuration config;
      SHYFT_DEFINE_STRUCT(request, (), (config));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_TRANSFERS)
    struct reply<m> {
      std::vector<dtss::transfer::configuration> configurations;
      SHYFT_DEFINE_STRUCT(reply, (), (configurations));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_TRANSFER_STATUS)
    struct request<m> {
      std::string name;
      bool clear_status;
      SHYFT_DEFINE_STRUCT(request, (), (name, clear_status));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_TRANSFER_STATUS)
    struct reply<m> {
      dtss::transfer::status status;
      SHYFT_DEFINE_STRUCT(reply, (), (status));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::STOP_TRANSFER)
    struct request<m> {
      std::string name;
      core::utctime max_wait_time;
      SHYFT_DEFINE_STRUCT(request, (), (name, max_wait_time));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::START_Q_BRIDGE)
    struct request<m> {
      dtss::q_bridge::configuration config;
      SHYFT_DEFINE_STRUCT(request, (), (config));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_Q_BRIDGES)
    struct reply<m> {
      std::vector<dtss::q_bridge::configuration> configurations;
      SHYFT_DEFINE_STRUCT(reply, (), (configurations));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_Q_BRIDGE_STATUS)
    struct request<m> {
      std::string name;
      bool clear_status;
      SHYFT_DEFINE_STRUCT(request, (), (name, clear_status));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol)
      && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::GET_Q_BRIDGE_STATUS)
    struct reply<m> {
      dtss::q_bridge::status status;
      SHYFT_DEFINE_STRUCT(reply, (), (status));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(
      dtss::is_protocol(m.protocol) && is_internal_message<m> && m.tag == dtss::message_tags<m.version>::STOP_Q_BRIDGE)
    struct request<m> {
      std::string name;
      core::utctime max_wait_time;
      SHYFT_DEFINE_STRUCT(request, (), (name, max_wait_time));
      auto operator<=>(request const &) const = default;
    };

    template <auto m>
    requires(dtss::is_protocol(m.protocol) && m.tag == dtss::message_tags<m.version>::PROTOCOL_VERSIONS)
    struct reply<m> {
      dtss::version_type minimum;
      dtss::version_type maximum;
      SHYFT_DEFINE_STRUCT(reply, (), (minimum, maximum));
      auto operator<=>(reply const &) const = default;
    };

    template <auto m>
    requires(m.protocol == dtss::protocol)
    struct protocol_archives<m> {
      static auto make_oarchive(std::ostream &stream) {
        return dtss::ostream_archive{stream};
      }

      static auto make_iarchive(std::istream &stream) {
        return dtss::istream_archive{stream};
      }
    };
  }
}
