/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>

#include <shyft/core/protocol.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/utility.h>

namespace shyft::dtss {

  using version_type = std::uint8_t;
  using message_tag_type = std::uint8_t;

  constexpr version_type internal_version = 0;
  constexpr version_type min_version = 1;
  constexpr version_type num_versions = 2;
  inline auto constexpr latest_version = min_version + num_versions - 1;
  constexpr message_tag_type error_tag = 0x0;

  template <version_type V>
  struct messages;

  template <version_type V>
  using message_tags = typename messages<V>::tags;

  template <>
  struct messages<internal_version> {
    SHYFT_DEFINE_NESTED_ENUM(
      tags,
      message_tag_type,
      (NONE,
       EVALUATE_TS_VECTOR,
       EVALUATE_TS_VECTOR_PERCENTILES,
       FIND_TS,
       GET_TS_INFO,
       STORE_TS,
       CACHE_FLUSH,
       CACHE_STATS,
       EVALUATE_EXPRESSION,
       EVALUATE_EXPRESSION_PERCENTILES,
       MERGE_STORE_TS,
       REMOVE_TS,
       EVALUATE_TS_VECTOR_CLIP,
       EVALUATE_EXPRESSION_CLIP,
       EVALUATE_GEO,
       GET_GEO_INFO,
       STORE_GEO,
       ADD_GEO_DB,
       REMOVE_GEO_DB,
       GET_CONTAINERS,
       SLAVE_READ,
       SLAVE_UNSUBSCRIBE,
       SLAVE_READ_SUBSCRIPTION,
       GET_VERSION,
       Q_LIST,
       Q_INFOS,
       Q_INFO,
       Q_PUT,
       Q_GET,
       Q_ACK,
       Q_SIZE,
       Q_ADD,
       Q_REMOVE,
       Q_MAINTAIN,
       SET_CONTAINER,
       REMOVE_CONTAINER,
       SWAP_CONTAINER,
       TRY_SLAVE_READ,
       STORE_TS_W_POLICY,
       UPDATE_GEO_DB,
       STORE_TS_2,
       START_TRANSFER,
       GET_TRANSFERS,
       GET_TRANSFER_STATUS,
       STOP_TRANSFER,
       PROTOCOL_VERSIONS,
       Q_FIND,
       START_Q_BRIDGE,
       GET_Q_BRIDGES,
       GET_Q_BRIDGE_STATUS,
       STOP_Q_BRIDGE));
  };

  template <>
  struct messages<1> {
    SHYFT_DEFINE_NESTED_ENUM(
      tags,
      message_tag_type,
      (NONE,
       FIND_TS,
       GET_TS_INFO,
       STORE_TS_2,
       MERGE_STORE_TS,
       REMOVE_TS,
       TRY_SLAVE_READ,
       SLAVE_READ_SUBSCRIPTION,
       SLAVE_UNSUBSCRIBE,
       GET_VERSION,
       Q_LIST,
       Q_INFOS,
       Q_INFO,
       Q_PUT,
       Q_GET,
       Q_ACK,
       Q_SIZE,
       Q_MAINTAIN,
       GET_CONTAINERS,
       SET_CONTAINER,
       REMOVE_CONTAINER,
       SWAP_CONTAINER,
       PROTOCOL_VERSIONS));
  };

  template <>
  struct messages<2> {
    SHYFT_DEFINE_NESTED_ENUM(
      tags,
      message_tag_type,
      (NONE,
       FIND_TS,
       GET_TS_INFO,
       STORE_TS_2,
       MERGE_STORE_TS,
       REMOVE_TS,
       TRY_SLAVE_READ,
       SLAVE_READ_SUBSCRIPTION,
       SLAVE_UNSUBSCRIBE,
       GET_VERSION,
       Q_LIST,
       Q_INFOS,
       Q_INFO,
       Q_PUT,
       Q_GET,
       Q_ACK,
       Q_SIZE,
       Q_MAINTAIN,
       GET_CONTAINERS,
       SET_CONTAINER,
       REMOVE_CONTAINER,
       SWAP_CONTAINER,
       PROTOCOL_VERSIONS,
       Q_FIND));
  };

  inline constexpr protocols::versioned_protocol<version_type, message_tag_type, messages> protocol{
    .internal_version = internal_version,
    .min_version = min_version,
    .num_versions = num_versions,
    .error_tag = error_tag,
    .compatability_mode = true};

  inline constexpr auto is_protocol = any_of_weak(protocol);

  static_assert(protocols::compatibility<protocol>);
  static_assert(protocols::error_tag<protocol> == 0x0);

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::messages<0>::tags);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::messages<1>::tags);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::messages<2>::tags);
