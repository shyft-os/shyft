#pragma once
#include <iostream>
#include <string>

#include <dlib/logger.h>
#include <fmt/core.h>
#include <fmt/ostream.h>

#include <shyft/core/dlib_utils.h>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/protocol/messages.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/dtss/ts_subscription_read.h>
#include <shyft/version.h>

namespace shyft::dtss {

  struct handler {
    server_state& s;
    std::string const & ip;
    unsigned short port;
    core::scoped_count count{s.alive_connections};
    subscription::ts_observer subs{s.sm, fmt::format("{}@{}", ip, port)};
    static dlib::logger dlog;

    void log(dlib::log_level level, std::string const & message) const {
      dlog << level << message;
    }

    static auto with_connection_error(auto&& f) {
      try {
        return std::invoke(SHYFT_FWD(f));
      } catch (dlib::socket_error const & e) {
        throw std::runtime_error(fmt::format("msync connection lost:{}", e.what()));
      }
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::NONE)
    protocols::reply<msg> operator()(protocols::request<msg>&&) {
      throw std::runtime_error("Exception tag not handled server side.");
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag
           == any_of(
             messages<msg.version>::tags::STORE_TS,
             messages<msg.version>::tags::SLAVE_READ,
             messages<msg.version>::tags::STORE_TS_W_POLICY))
    protocols::reply<msg> operator()(protocols::request<msg>&&) {
      throw std::runtime_error(fmt::format("{} is deprecated, update client side!", msg.tag));
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_TS_VECTOR)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_evaluate_ts_vector(
          s, std::move(req.period), req.tsv, std::move(req.use_ts_cached_read), std::move(req.update_ts_cache), {})};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_TS_VECTOR_CLIP)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_evaluate_ts_vector(
          s,
          std::move(req.period),
          req.tsv,
          std::move(req.use_ts_cached_read),
          std::move(req.update_ts_cache),
          std::move(req.clip_period))};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_EXPRESSION)
    auto operator()(protocols::request<msg>&& req) {
      time_series::dd::ats_vector atsv = time_series::dd::expression_decompressor::decompress(
        req.compressed_expression);
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_evaluate_ts_vector(
          s, std::move(req.period), atsv, std::move(req.use_ts_cached_read), std::move(req.update_ts_cache), {})};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_EXPRESSION_CLIP)
    auto operator()(protocols::request<msg>&& req) {
      time_series::dd::ats_vector atsv = time_series::dd::expression_decompressor::decompress(
        req.compressed_expression);
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_evaluate_ts_vector(
          s,
          std::move(req.period),
          atsv,
          std::move(req.use_ts_cached_read),
          std::move(req.update_ts_cache),
          std::move(req.clip_period))};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_TS_VECTOR_PERCENTILES)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_evaluate_percentiles(
          s,
          std::move(req.period),
          req.tsv,
          req.ta,
          std::move(req.percentile_spec),
          std::move(req.use_ts_cached_read),
          std::move(req.update_ts_cache))};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_EXPRESSION_PERCENTILES)
    auto operator()(protocols::request<msg>&& req) {
      time_series::dd::ats_vector atsv = time_series::dd::expression_decompressor::decompress(
        req.compressed_expression);
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_evaluate_percentiles(
          s,
          std::move(req.period),
          atsv,
          req.ta,
          std::move(req.percentile_spec),
          std::move(req.use_ts_cached_read),
          std::move(req.update_ts_cache))};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::FIND_TS)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_find_ts(s, std::move(req.search_expression))};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::GET_TS_INFO)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_get_ts_info(s, std::move(req.url))};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::CACHE_FLUSH)
    auto operator()(protocols::request<msg>&&) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.ts_cache.flush();
        s.ts_cache.clear_cache_stats();
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::CACHE_STATS)
    auto operator()(protocols::request<msg>&&) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {s.ts_cache.get_cache_stats()};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::MERGE_STORE_TS)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        if constexpr (msg.version >= 1) {
          do_merge_store_ts(s, from_fragment(std::move(req.fragments)), req.cache);
        } else {
          do_merge_store_ts(s, std::move(req.tsv), req.cache);
        }
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::REMOVE_TS)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        do_remove_ts(s, std::move(req.url));
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::EVALUATE_GEO)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_geo_evaluate(s, std::move(req.arguments), req.cache_read, req.cache_write)};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::GET_GEO_INFO)
    auto operator()(protocols::request<msg>&&) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_get_geo_info(s)};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::STORE_GEO)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        do_geo_store(s, std::move(req.db_name), std::move(req.matrix), req.replace, req.cache_write);
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::ADD_GEO_DB)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        add_geo_ts_db(s, std::move(req.config));
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::REMOVE_GEO_DB)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        remove_geo_ts_db(s, std::move(req.name));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::GET_CONTAINERS)
    auto operator()(protocols::request<msg>&&) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {do_get_container_names(s)};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::SLAVE_UNSUBSCRIBE)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        subs.unsubscribe(std::move(req.ids));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::SLAVE_READ_SUBSCRIPTION)
    auto operator()([[maybe_unused]] protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        auto [fragments, periods] = subscription::read_changed_subs(subs, [&](auto const & ts_ids, auto const & p) {
          auto [r_ts, r_tp, r_err] = try_slave_read(s, ts_ids, p, true);
          return std::make_pair(std::move(r_ts), std::move(r_tp));
        });
        return {std::move(fragments), std::move(periods)};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::GET_VERSION)
    protocols::reply<msg> operator()(protocols::request<msg>&&) {
      return {_version_string()};
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_LIST)
    auto operator()(protocols::request<msg>&&) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {s.queue_manager.queue_names()};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_INFOS)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {s.queue_manager(std::move(req.name))->get_msg_infos()};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_INFO)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {s.queue_manager(std::move(req.name))->get_msg_info(std::move(req.message_id))};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_PUT)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        if constexpr (msg.version >= 1) {
          s.queue_manager(std::move(req.name))
            ->put(
              std::move(req.message_id),
              std::move(req.description),
              std::move(req.ttl),
              from_fragment(std::move(req.fragments)));
        } else {
          s.queue_manager(std::move(req.name))
            ->put(std::move(req.message_id), std::move(req.description), std::move(req.ttl), std::move(req.tsv));
        }
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_GET)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        auto q = s.queue_manager(req.name);
        auto r = q->try_get();
        if (r == nullptr && req.max_wait > utctime(0l)) {
          auto const dt = std::chrono::milliseconds(5l);
          auto const t_exit = core::utctime_now() + req.max_wait;
          do {
            std::this_thread::sleep_for(dt);
            r = q->try_get();
          } while (r == nullptr && core::utctime_now() < t_exit);
        }
        if constexpr (msg.version >= 1) {
          if (!r)
            return {};
          return {.info = r->info, .fragments = to_fragments(r->tsv)};
        } else { // note: we promise that we only observe the elements here, so cast is safe
          return {std::const_pointer_cast<std::remove_cvref_t<typename decltype(r)::element_type>>(r)};
        }
      });
    }

    template <auto msg>
    requires((protocols::is_internal_message<msg> || msg.version > 1) && msg.tag == messages<msg.version>::tags::Q_FIND)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        if (auto const m = s.queue_manager(req.name)->find(req.message_id); m)
          return {.info = m->info, .fragments = to_fragments(m->tsv)};
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_ACK)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.queue_manager(std::move(req.name))->done(std::move(req.message_id), std::move(req.diagnosis));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_SIZE)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {s.queue_manager(std::move(req.name))->size()};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::Q_ADD)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.queue_manager.add(std::move(req.name));
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::Q_REMOVE)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.queue_manager.remove(std::move(req.name));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::Q_MAINTAIN)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        if (req.flush)
          s.queue_manager(std::move(req.name))->reset();
        else
          s.queue_manager(std::move(req.name))->flush_done_items(req.keep_ttl_items);
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::SET_CONTAINER)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        do_set_container(s, std::move(req.name), std::move(req.path), std::move(req.type), std::move(req.config));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::REMOVE_CONTAINER)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        do_remove_container(s, std::move(req.url), req.remove_from_disk);
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::SWAP_CONTAINER)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        do_swap_container(s, std::move(req.name_a), std::move(req.name_b));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::TRY_SLAVE_READ)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        if (req.subscribe)
          subs.subscribe_to(req.ids, req.period);
        auto [fragments, periods, errors] = try_slave_read(s, req.ids, req.period, req.cache);
        auto [fragment_updates, period_updates] = subscription::read_changed_subs(
          subs, [&](std::vector<std::string> const & ts_ids, utcperiod const & p) {
            auto [r_ts, r_tp, r_err] = try_slave_read(s, ts_ids, p, req.cache);
            return std::make_pair(std::move(r_ts), std::move(r_tp));
          });
        for (auto i = 0u; i < fragments.size(); ++i)
          if (fragments[i] && !req.period.contains(fragments[i]->total_period()))
            subs.subscribe_to(id_vector_t{req.ids[i]}, fragments[i]->total_period());
        return {
          std::move(fragments),
          std::move(periods),
          std::move(fragment_updates),
          std::move(period_updates),
          std::move(errors)};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::UPDATE_GEO_DB)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        do_update_geo_db(
          s, std::move(req.name), std::move(req.description), std::move(req.json), std::move(req.origin));
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::STORE_TS_2)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        diags_t diagnostics;
        if (s.msync) {
          return {s.msync->store(std::move(req.fragments), req.policy)};
        }
        return {do_local_store(
          s,
          req.fragments.size(),
          [&](std::size_t i) {
            return movable_ts_item_t{req.fragments[i].url, req.fragments[i].ts}; // obs! we accept move on .ts!
          },
          req.policy)};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && (msg.tag == messages<msg.version>::tags::START_TRANSFER))
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.exchange_manager.start_transfer(req.config);
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::GET_TRANSFERS)
    auto operator()([[maybe_unused]] protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {.configurations = s.exchange_manager.get_transfers()};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::GET_TRANSFER_STATUS)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {.status = {s.exchange_manager.get_transfer_status(req.name, req.clear_status)}};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::STOP_TRANSFER)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.exchange_manager.stop_transfer(req.name, req.max_wait_time);
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && (msg.tag == messages<msg.version>::tags::START_Q_BRIDGE))
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.exchange_manager.start_q_bridge(req.config);
        return {};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::GET_Q_BRIDGES)
    auto operator()([[maybe_unused]] protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {.configurations = s.exchange_manager.get_q_bridges()};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::GET_Q_BRIDGE_STATUS)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        return {.status = {s.exchange_manager.get_q_bridge_status(req.name, req.clear_status)}};
      });
    }

    template <auto msg>
    requires(
      protocols::is_versioned<msg.protocol> && protocols::is_internal_version<msg.protocol>(msg.version)
      && msg.tag == messages<msg.version>::tags::STOP_Q_BRIDGE)
    auto operator()(protocols::request<msg>&& req) {
      return with_connection_error([&]() -> protocols::reply<msg> {
        s.exchange_manager.stop_q_bridge(req.name, req.max_wait_time);
        return {};
      });
    }

    template <auto msg>
    requires(protocols::is_versioned<msg.protocol> && msg.tag == messages<msg.version>::tags::PROTOCOL_VERSIONS)
    protocols::reply<msg> operator()(protocols::request<msg>&&) {
      return {min_version, latest_version};
    }
  };

  struct dispatcher {
    server_state& s;

    handler operator()(std::string const & ip, unsigned short const port) const {
      return {.s = s, .ip = ip, .port = port};
    }

    void failed_connection() const {
      ++s.failed_connections;
    }
  };
}
