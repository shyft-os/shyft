#include <cstdint>
#include <memory>
#include <mutex>
#include <queue>
#include <ranges>
#include <stdexcept>
#include <vector>

#include <fmt/core.h>

#include <shyft/dtss/queue.h>

namespace shyft::dtss::queue {

  namespace {
    auto q_error(std::string_view queue, std::string_view what) {
      return std::runtime_error(fmt::format("queue '{}': {}", queue, what));
    }

    auto msg_error(std::string_view queue, std::string_view message, std::string_view what) {
      return std::runtime_error(fmt::format("queue '{}', message '{}': {}", queue, message, what));
    }
  }

  std::vector<msg_info> tsv_queue::get_msg_infos() const {
    std::scoped_lock _{mx};
    std::vector<msg_info> r;
    r.reserve(all.size());
    for (auto const &m : std::views::values(all))
      r.push_back(m->info);
    return r;
  }

  msg_info tsv_queue::get_msg_info(std::string const &msg_id) const {
    std::scoped_lock _{mx};
    if (auto const f = all.find(msg_id); f != all.end())
      return f->second->info;
    throw msg_error(name, msg_id, "message not found");
  }

  void tsv_queue::put(std::string const &msg_id, std::string const &descript, utctime ttl, ats_vector const &tsv) {
    std::scoped_lock _{mx};
    auto const f = all.find(msg_id);
    if (f != all.end())
      throw msg_error(name, msg_id, "already exists");
    auto const t_now = utctime_now();
    auto const m = std::make_shared<tsv_msg>(msg_info{msg_id, descript, ttl, t_now}, tsv);
    all[msg_id] = m;
    mq.push(m);
  }

  std::shared_ptr<tsv_msg const> tsv_queue::try_get() {
    std::scoped_lock _{mx};
    if (mq.empty())
      return nullptr;
    auto m = mq.front();
    mq.pop();
    m->info.fetched = utctime_now();
    return m;
  }

  std::shared_ptr<tsv_msg const> tsv_queue::find(std::string const &msg_id) const {
    std::scoped_lock _{mx};
    auto const it = all.find(msg_id);
    if (it == all.end())
      return nullptr;
    return it->second;
  }

  std::size_t tsv_queue::size() const {
    std::scoped_lock _{mx};
    return mq.size();
  }

  std::size_t tsv_queue::get_full_size() const {
    std::scoped_lock lock{mx};
    return all.size();
  }

  void tsv_queue::done(std::string const &msg_id, std::string const &diag) {
    std::scoped_lock _{mx};
    auto const f = all.find(msg_id);

    if (f == all.end())
      throw msg_error(name, msg_id, "message not found");

    if (f->second->info.fetched == no_utctime)
      throw msg_error(name, msg_id, "can't done un-fetched message");

    f->second->info.done = utctime_now();
    f->second->info.diagnostics = diag;
  }

  void tsv_queue::reset() {
    std::scoped_lock _{mx};
    all.clear();
    mq = {};
  }

  std::size_t tsv_queue::flush_done_items(bool keep_ttl_items, utctime x_now) {
    std::scoped_lock _{mx};
    std::size_t n_removed{0};
    auto const t_now = x_now != no_utctime ? x_now : utctime_now(); // to compute eol
    auto m = all.begin();
    while (m != all.end()) {
      if (m->second->info.done != no_utctime) {
        if (!keep_ttl_items || t_now > m->second->info.eol()) {
          m = all.erase(m);
          ++n_removed;
          continue;
        }
      }
      ++m;
    }
    return n_removed;
  }

  void q_mgr::add(std::string const &q_name) {
    std::scoped_lock _{mx};
    if (auto const f = q.find(q_name); f != q.end())
      throw q_error(q_name, "already exists");
    q[q_name] = std::make_shared<tsv_queue>(q_name);
  }

  void q_mgr::remove(std::string const &q_name) {
    std::scoped_lock _{mx};
    auto const f = q.find(q_name);
    if (f == q.end())
      throw q_error(q_name, "not found");
    q.erase(f); // consider test if q.empty()?
  }

  bool q_mgr::exists(std::string const &q_name) {
    std::scoped_lock _{mx};
    return q.contains(q_name);
  }

  std::size_t q_mgr::size() const {
    std::scoped_lock _{mx};
    return q.size();
  }

  std::vector<std::string> q_mgr::queue_names() const {
    std::scoped_lock _{mx};
    std::vector<std::string> r;
    for (auto const &k : std::views::keys(q))
      r.push_back(k);
    return r;
  }

  std::shared_ptr<tsv_queue> q_mgr::operator()(std::string const &q_name) const {
    std::scoped_lock _{mx};
    auto const f = q.find(q_name);
    if (f == q.end())
      throw q_error(q_name, "not found");
    return f->second;
  }

}
