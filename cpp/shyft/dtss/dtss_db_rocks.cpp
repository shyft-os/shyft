#include <algorithm>
#include <atomic>
#include <cstdint>
#include <map>
#include <ranges>
#include <regex>
#include <stdexcept>
#include <string>

#include <dlib/logger.h>
#include <fmt/chrono.h>
#include <fmt/core.h>
#include <rocksdb/advanced_cache.h>
#include <rocksdb/cache.h>
#include <rocksdb/db.h>
#include <rocksdb/env.h>
#include <rocksdb/filter_policy.h>
#include <rocksdb/slice_transform.h>
#include <rocksdb/table.h>
#include <rocksdb/write_batch.h>

#include <shyft/core/fs_compat.h>
#include <shyft/dtss/detail/make_aligned_ts.h>
#include <shyft/dtss/detail/ts_db_rocks_impl.h>
#include <shyft/dtss/dtss_db_rocks.h>

namespace shyft::dtss::detail {

  using core::calendar;
  using core::deltahours;
  using core::max_utctime;
  using core::min_utctime;
  using core::utctime_now;
  using time_series::ts_point_fx;
  using ta_generic_type = time_axis::generic_dt::generic_type;

  //------------------ ts_db_rocks_impl --------
  /**
   * @brief configuration for each instance
   *
   * @details
   * for header use standard settings.
   * -- for data consider:
   * options.max_file_size=_cfg.max_file_size;//100*1024*1024;//100 MB
   * options.write_buffer_size=_cfg.write_buffer_size;//50*1024*1024;// 50 MB
   * interesting options:
   * .max_file_size = 2 * 1024 * 1024;     __ 2 M-bytes. is small in our context
   *            we should consider 100x ?
   * .block_size = 4 * 1024;               __ unzipped data block
   *            we should consider 10x?
   * .write_buffer_size = 4 * 1024 * 1024; __ greater means more performance, at cost of startup.
   *            we could consider 2x100    __ 100 Mega points (is not much)
   *
   * .max_open_files = 1000;               __
   *            most likely ok for us
   *
   * .block_cache= rocksdb::NewLRUCache(100*1024*1024)
   *           remember to remove cache at exit...
   *           also read options.fill_cache can be set false for this one.
   * keep in mind we are also caching ts at the ts-cache(so this would be for the internals of rocksdb)
   *
   * We store, and later retrieve the level db configuration when opening a database.
   *
   */
  struct rocks_db_cfg {
    static constexpr std::uint64_t magic1{0xdeadbeefbaadf00dul};
    static constexpr std::uint64_t ppf_default{1024 * 8}; ///< 8 k frags
    std::uint64_t magic{magic1};                          ///< Ensure some kind of integrity
    std::uint64_t ppf{ppf_default};                       ///< points per fragment for the database
    std::uint64_t uid{0};                                 ///< current uid counter of the database
    std::uint64_t clean_shutdown{0};                      ///<  0, shutdown uncontrolled, full scan to recover uid
                                                          ///   1, clean shutdown (uid can be trusted)
                                                          ///  >1, (uid +n) can be trusted

    [[nodiscard]] bool valid() const {
      return magic == magic1 && ppf > 1;
    }
  };

  /** @brief logger */
  struct rocks_db_log final : rocksdb::Logger {
    static dlib::logger dlog;

    void Logv(char const * format, std::va_list ap) override {
      if (dlog.level() >= dlib::LDEBUG) {
        char buf[1024];
        std::vsnprintf(buf, sizeof(buf), format, ap);
        dlog << dlib::LINFO << buf;
      }
    }
  };

  dlib::logger rocks_db_log::dlog{"rocksdb"};

  struct ts_db_rocks_impl {
    static constexpr auto const * data_dir = "/rdb_d";   ///< root_dir/data
    static constexpr auto const * header_dir = "/rdb_h"; ///< root_dir/headers
    static constexpr auto const * info_column = "info";
    static constexpr auto const * cfg_key_ = "cfg";

    std::string const cfg_key{cfg_key_};
    std::string root_dir;                                   ///< root_dir points to the top of the container
    mutable std::atomic_uint64_t uid{0};                    ///< uid used while creating new ts-ids, synced to cfg.
    mutable std::atomic_uint64_t n_uid{0};                  ///< n of u-ids allocated since last sync
    constexpr static std::int64_t max_n_before_sync{10000}; ///< force sync uid when n_uid exceeds this
    std::mutex uid_mx;                                      ///< uid mx sync to ensure consistent sync

    std::unique_ptr<rocksdb::DB, std::function<void(rocksdb::DB*)>> db; ///< db. w custom deleter
    rocksdb::ColumnFamilyHandle* db_d{nullptr};                         ///< data column handle
    rocksdb::ColumnFamilyHandle* db_i{nullptr};                         ///< info column handle

    rocks_db_cfg cfg{};                                         ///< the in memory configuration, stored at dbh.cfg key
    std::uint64_t sync_count{0};                                ///< for test/verification number of syncs called
    std::map<std::string, std::shared_ptr<calendar>> calendars; ///< fast lookup of all calendars

    std::uint64_t test_mode{0}; ///< as pr. db_cfg for testing only
    std::shared_ptr<rocks_db_log> log = std::make_shared<rocks_db_log>();

    static bool exists_at(std::string const & root_dir) {
      if (!fs::exists(root_dir))
        return false;
      return fs::exists(root_dir + data_dir);
    }

    /**
     *  @brief set options that applies in general
     *  @details
     *  as pr https://github.com/facebook/rocksdb/wiki/Setup-Options-and-Basic-Tuning
     */
    static void set_standard_options(rocksdb::Options& o, std::shared_ptr<rocksdb::Cache> cache, bool bloom_filter) {
      o.bytes_per_sync = 1048576; // 1M, suitable for rather large commit/sync intervals(could be shorter for SSD/nVME)
      o.compaction_pri = rocksdb::kMinOverlappingRatio;
      o.max_background_jobs = 6;
      o.level_compaction_dynamic_level_bytes = true;
      o.IncreaseParallelism();
      o.OptimizeLevelStyleCompaction();
      o.max_open_files = 1000; // TODO: max open files set less than 1024 due to experience it exceeded 1024 if not.
      if (cache || bloom_filter) {
        rocksdb::BlockBasedTableOptions table_options;
        table_options.block_cache = cache;
        if (bloom_filter) {
          table_options.optimize_filters_for_memory = true;
          table_options.filter_policy.reset(rocksdb::NewBloomFilterPolicy(10, false));
        }
        auto const table_factory = NewBlockBasedTableFactory(table_options);
        o.table_factory.reset(table_factory);
      }
    }

    static void migrate_old_header_db_to_column_db(
      std::string const & root_dir,
      rocksdb::DB* dbo,
      rocksdb::ColumnFamilyHandle* db_i) {
      if (auto const old_header_dir = (fs::path(root_dir + header_dir)).make_preferred().string();
          fs::exists(old_header_dir) && fs::is_directory(old_header_dir)) {
        rocksdb::DB* hdr_db{nullptr};
        auto constexpr ix_cache_size = 1024 * 1024 * 1024ul;
        auto cache = rocksdb::NewLRUCache(ix_cache_size);
        rocksdb::Options old_hdr_options;
        set_standard_options(old_hdr_options, cache, true);
        old_hdr_options.create_if_missing = false;
        if (auto s_hdr = rocksdb::DB::Open(old_hdr_options, old_header_dir, &hdr_db); s_hdr.ok()) {
          rocksdb::ReadOptions read_fast_options;
          read_fast_options.fill_cache = false;                   /// read fast, no need to fill cache
          read_fast_options.readahead_size = 1024 * 1024 * 200ul; // 200mb
          read_fast_options.auto_readahead_size = false;
          auto src_it = hdr_db->NewIterator(read_fast_options);
          auto const t0 = utctime_now();
          rocks_db_log::dlog << dlib::LINFO
                             << fmt::format("Auto-migrate old header db to column db {}", old_header_dir);
          auto constexpr reserved_batch_size = 200 * 1'000'000ul; // 1 mill elements, 100b key, 64b. value
          rocksdb::WriteBatch wb{reserved_batch_size};
          auto n_items = 0u;
          for (src_it->SeekToFirst(); src_it->Valid(); src_it->Next()) {
            wb.Put(db_i, src_it->key(), src_it->value());
            ++n_items;
          }
          rocksdb::WriteOptions wsync;
          wsync.sync = true; // enforce write in this case, do not defer.
          if (auto s_write = dbo->Write(wsync, &wb); !s_write.ok()) {
            rocks_db_log::dlog
              << dlib::LWARN
              << fmt::format("Auto-migrate old header db to column db failed, diag={}", s_write.ToString());
            throw std::runtime_error(
              fmt::format("ts_db_rocks: failed to auto-migrate old header db to column db {}", old_header_dir));
          }
          auto t_used = to_seconds(utctime_now() - t0);
          delete hdr_db;
          auto const to_be_removed_dir = old_header_dir + ".migrated";
          fs::rename(old_header_dir, to_be_removed_dir); // (let user remove it)
          rocks_db_log::dlog
            << dlib::LINFO
            << fmt::format(
                 "Auto-migrate old header db {} done for {} items in {} seconds", old_header_dir, n_items, t_used);
          rocks_db_log::dlog
            << dlib::LINFO
            << fmt::format("Old db header is now located at {} and can be removed to recover space", to_be_removed_dir);
        }
      }
    }

    /**
     * @brief Rollback migration to old separate header/data rocksdb format
     *
     * @details
     * This function checks for the existence of rocks database in the specified directory.
     * If found, and it has an info column family,
     * it continues opening a new fresh old style header-db,
     * and fills it with the main.db info column.
     * On success drops the main.db info column family and closes
     * everything.
     *
     * @param root_dir The root directory containing the old header database.
     */
    static bool rollback_migration_at(std::string const & root_dir) {
      auto const old_header_dir = (fs::path(root_dir + header_dir)).make_preferred().string();

      if (fs::exists(old_header_dir) || fs::is_directory(old_header_dir))
        throw std::runtime_error(
          fmt::format("ts_db_rocks: can not rollback migration, old header db found at {}", old_header_dir));

      auto const db_dir = (fs::path(root_dir + data_dir)).make_preferred().string();
      if (!fs::exists(db_dir) || !fs::is_directory(db_dir))
        throw std::runtime_error(fmt::format("ts_db_rocks: can not rollback, db dir not found at {}", db_dir));

      // open the migrated db
      rocksdb::Options db_options;
      auto const cache = rocksdb::NewLRUCache(1024 * 1024 * 1024ul);
      set_standard_options(db_options, cache, true);    // give it some cache, and set bloom filter
      db_options.compression = rocksdb::kNoCompression; // we ignore the compression of cf, assume no here
      db_options.create_if_missing = false;
      std::vector<std::string> existing_cfs;

      if (auto const cf_ok = rocksdb::DB::ListColumnFamilies(db_options, db_dir, &existing_cfs); !cf_ok.ok())
        throw std::runtime_error(
          fmt::format("ts_db_rocks: can not rollback, no info column family in db dir not found at {}", db_dir));

      std::vector<rocksdb::ColumnFamilyDescriptor> cols;
      rocksdb::ColumnFamilyOptions cf_options(db_options);
      cols.emplace_back(rocksdb::kDefaultColumnFamilyName, db_options);
      cols.emplace_back(info_column, db_options);
      std::vector<rocksdb::ColumnFamilyHandle*> handles;
      rocksdb::DB* dbo{nullptr};
      if (auto hs = rocksdb::DB::Open(db_options, db_dir, cols, &handles, &dbo); !hs.ok())
        throw std::runtime_error(fmt::format("ts_db_rocks: can not rollback, failed to open db dir at {}", db_dir));
      auto db = std::unique_ptr<rocksdb::DB, std::function<void(rocksdb::DB*)>>(dbo, [&](rocksdb::DB* dbx) {
        for (auto const & cf_h : handles)
          if (cf_h)
            dbx->DestroyColumnFamilyHandle(cf_h);
        delete dbx;
      });

      auto const db_i = handles[1]; // after 1st time creation, it is passed on in the open call above.

      if (!fs::create_directory(old_header_dir))
        throw std::runtime_error(
          fmt::format("ts_db_rocks: can not rollback, failed to create old header dir at {}", old_header_dir));

      rocksdb::DB* hdr_db{nullptr};
      rocksdb::Options old_hdr_options;
      set_standard_options(old_hdr_options, cache, true);
      old_hdr_options.create_if_missing = true; // really create
      if (auto s_hdr = rocksdb::DB::Open(old_hdr_options, old_header_dir, &hdr_db); !s_hdr.ok())
        throw std::runtime_error(
          fmt::format("ts_db_rocks: can not rollback, failed to create old header db at {}", old_header_dir));
      std::unique_ptr<rocksdb::DB> dbh(hdr_db);
      rocksdb::ReadOptions read_fast_options;
      read_fast_options.fill_cache = false;                   /// read fast, no need to fill cache
      read_fast_options.readahead_size = 1024 * 1024 * 200ul; // 200mb
      read_fast_options.auto_readahead_size = false;
      auto src_it = dbo->NewIterator(read_fast_options, db_i);
      auto const t0 = utctime_now();
      rocks_db_log::dlog << dlib::LINFO
                         << fmt::format("rollback-migrate old header db to column db {}", old_header_dir);
      auto constexpr reserved_batch_size = 200 * 1'000'000ul; // 1 mill elements, 100b key, 64b. value
      rocksdb::WriteBatch wb{reserved_batch_size};
      auto n_items = 0u;
      for (src_it->SeekToFirst(); src_it->Valid(); src_it->Next()) {
        wb.Put(src_it->key(), src_it->value());
        ++n_items;
      }
      rocksdb::WriteOptions wsync;
      wsync.sync = true; // enforce write in this case, do not defer.
      if (auto s_write = hdr_db->Write(wsync, &wb); !s_write.ok()) {
        rocks_db_log::dlog
          << dlib::LWARN
          << fmt::format("rollback-migrate old header db to column db failed, diag={}", s_write.ToString());
        throw std::runtime_error(
          fmt::format("ts_db_rocks: failed to rollback-migrate old header db to column db {}", old_header_dir));
      }
      auto t_used = to_seconds(utctime_now() - t0);
      rocks_db_log::dlog
        << dlib::LINFO
        << fmt::format(
             "Rollback-migrate old header db {} done for {} items in {} seconds", old_header_dir, n_items, t_used);

      if (auto const s_drop = dbo->DropColumnFamily(db_i); !s_drop.ok()) {
        rocks_db_log::dlog
          << dlib::LWARN
          << fmt::format(
               "rollback-migrate old header db to column db failed, DropColumnFamily failed diag={}",
               s_drop.ToString());
        return false;
      }
      return true;
    }

    ts_db_rocks_impl(std::string const & root_dir, db_cfg const & _cfg)
      : root_dir{root_dir} {
      if (_cfg.ppf < 1)
        throw std::runtime_error(fmt::format("ts_db_rocks: ppf must be >0, attempt at {}", root_dir));
      bool needs_create{false};
      test_mode = _cfg.test_mode;
      if (!fs::is_directory(root_dir)) {
        if (!fs::exists(root_dir)) {
          if (!fs::create_directories(root_dir)) {
            throw std::runtime_error(fmt::format("ts_db_rocks: failed to create root directory '{}'", root_dir));
          }
        } else {
          throw std::runtime_error(
            fmt::format("ts_db_rocks: designated root directory is not a directory '{}'", root_dir));
        }
        cfg.ppf = _cfg.ppf;
        needs_create = true;
      }
      rocks_db_log::dlog.set_level(dlib::log_level{static_cast<int>(_cfg.log_level), "rdb_cfg"});
      rocks_db_log::dlog << dlib::LINFO << "Create rocks db @" << root_dir;
      if (_cfg.test_mode)
        rocks_db_log::dlog << dlib::LWARN << "Rocks db driver in test-mode";

      rocksdb::Options info_options;
      std::shared_ptr<rocksdb::Cache> common_cache = _cfg.ix_cache > 0 ? rocksdb::NewLRUCache(_cfg.ix_cache) : nullptr;
      info_options.info_log = log; // route logs to our logger.
      info_options.compression = _cfg.compression ? rocksdb::kSnappyCompression : rocksdb::kNoCompression;
      set_standard_options(info_options, common_cache, true);
      info_options.create_if_missing = true;
      rocksdb::Options data_options;
      set_standard_options(info_options, common_cache, false); // ensure to pass it, usually zero
      data_options.write_buffer_size = _cfg.write_buffer_size;
      data_options.create_if_missing = true;
      data_options.compression = info_options.compression;
      data_options.info_log = info_options.info_log;
      data_options.prefix_extractor.reset(rocksdb::NewFixedPrefixTransform(8)); // tsid is 8 bytes
      rocksdb::Options db_options;
      set_standard_options(db_options, nullptr, false);
      db_options.info_log = log;
      db_options.compression = _cfg.compression ? rocksdb::kSnappyCompression : rocksdb::kNoCompression;
      db_options.create_if_missing = needs_create;
      bool needs_cf_create = needs_create;
      if (!needs_create) { // incredible, we must check if we have column family here!
        std::vector<std::string> existing_cfs;
        auto const cf_ok = rocksdb::DB::ListColumnFamilies(db_options, root_dir + data_dir, &existing_cfs);
        needs_cf_create = !cf_ok.ok() || (cf_ok.ok() && existing_cfs.size() < 2);
      }
      std::vector<rocksdb::ColumnFamilyDescriptor> cols;
      rocksdb::ColumnFamilyOptions cf_options(data_options);
      cols.emplace_back(rocksdb::kDefaultColumnFamilyName, data_options);
      if (!needs_cf_create) // and only dare to use cf options if we are sure it is there
        cols.emplace_back(info_column, info_options);

      std::vector<rocksdb::ColumnFamilyHandle*> handles;
      rocksdb::DB* dbo{nullptr};
      if (auto hs = rocksdb::DB::Open(db_options, root_dir + data_dir, cols, &handles, &dbo); !hs.ok()) {
        if (needs_create)
          throw std::runtime_error(
            fmt::format("ts_db_rocks: failed to create header db at {}, diag={}", root_dir, hs.ToString()));
        db_options.create_if_missing = true; // ok, the directory was there, but no db, automagically create.
        cfg.ppf = _cfg.ppf;                  // ensure we put this inplace for new db
        rocks_db_log::dlog << dlib::LINFO << "Creating fresh db @" << root_dir;
        hs = rocksdb::DB::Open(db_options, root_dir + data_dir, cols, &handles, &dbo);
        if (!hs.ok())
          throw std::runtime_error(
            fmt::format("ts_db_rocks: failed to create fresh db at {}, diag={}", root_dir, hs.ToString()));
      }
      db_d = handles[0];
      if (needs_cf_create) { // we need to manually add the column family first time!!
        dbo->CreateColumnFamily(info_options, info_column, &db_i);
        if (!needs_create) { // migration might be needed
          migrate_old_header_db_to_column_db(root_dir, dbo, db_i);
        }
      } else {
        db_i = handles[1]; // after 1st time creation, it is passed on in the open call above.
      }
      db = std::unique_ptr<rocksdb::DB, std::function<void(rocksdb::DB*)>>(dbo, [this](rocksdb::DB* dbx) {
        if (db_d)
          dbx->DestroyColumnFamilyHandle(db_d);
        if (db_i)
          dbx->DestroyColumnFamilyHandle(db_i);
        delete dbx;
      });

      if (db_options.create_if_missing) { // then, give the above, for sure, a fresh db is in place, and we put the cfg
                                          // inside it to mark it.
        rocksdb::WriteOptions wsync;
        wsync.sync = true;
        cfg.clean_shutdown = 1; // ensure we start clean
        if (!db->Put(wsync, db_i, cfg_key, rocksdb::Slice{reinterpret_cast<char const *>(&cfg), sizeof(rocks_db_cfg)})
               .ok())
          throw std::runtime_error(
            fmt::format("ts_db_rocks: failed to store initial cfg key to header db at {}", root_dir));
      } else {
        std::string cfgs;
        if (!db->Get(rocksdb::ReadOptions(), db_i, cfg_key, &cfgs).ok()) {
          throw std::runtime_error(
            fmt::format("ts_db_rocks: failed to read cfg key from existing header db at {}", root_dir));
        }
        if (cfgs.size() != sizeof(rocks_db_cfg)) {
          throw std::runtime_error(
            fmt::format("ts_db_rocks: failed to read cfg key, wrong size, from existing header db at {}", root_dir));
        }
        cfg = *reinterpret_cast<rocks_db_cfg const *>(cfgs.data());
        if (!cfg.valid()) {
          throw std::runtime_error(
            fmt::format("ts_db_rocks: cfg from header db failed to validate(probably corrupt) at {}", root_dir));
          // here we could try to recover the db.
        }
        if (cfg.clean_shutdown == 0) { // was not clean shutdown
          rocks_db_log::dlog << dlib::LWARN << "Shutdown was not clean, last uid=" << cfg.uid
                             << ", Recovery might take long time depending on disk speed and db size";
          auto const recovered_uid = recover_uid(); // recover the uid, and let it remain 'open'/unclean
          uid = recovered_uid;
          rocks_db_log::dlog << dlib::LWARN << "Recovered updated uid=" << recovered_uid;
          sync_uid(1); // write back clean shutdown
        } else if (cfg.clean_shutdown > 1) {
          rocks_db_log::dlog << dlib::LWARN << "Shutdown was not pure clean, last uid=" << cfg.uid
                             << ", adding configured slack " << cfg.clean_shutdown;
          uid = cfg.uid + cfg.clean_shutdown;
          sync_uid(1);   // make it clean
        } else {         // == 1
          uid = cfg.uid; // update the running uid we are using.
          // do not touch. let it remain clean until first write.
        }
      }

      // ensure to use file options for data part.

      make_calendar_lookups();
    }

    void maintain(bool const info_db, bool const data_db) const {
      auto compact_db = [&](rocksdb::ColumnFamilyHandle* const db_h) {
        auto const t0 = utctime_now();
        rocks_db_log::dlog << dlib::LWARN << fmt::format("Maintaining {}/{} ", root_dir, db_h->GetName());
        rocksdb::CompactRangeOptions compact_range_options;
        db->CompactRange(compact_range_options, db_h, nullptr, nullptr);
        rocks_db_log::dlog
          << dlib::LWARN
          << fmt::format(
               "Maintaining done {}/{} done in {} seconds", root_dir, db_h->GetName(), to_seconds(utctime_now() - t0));
      };
      if (info_db) {
        compact_db(db_i);
      }
      if (data_db) {
        compact_db(db_d);
      }
    }

    ~ts_db_rocks_impl() {

      if (test_mode == 0)
        sync_uid(1); // ensure to sync uid before closing.
    }

    void sync_uid(std::uint64_t const clean_shutdown = 0, bool const reset_n_uid = true) {
      std::scoped_lock _(uid_mx); // ensure we protect cfg.uid and db.
      if (cfg.uid != uid || clean_shutdown != cfg.clean_shutdown) {
        ++sync_count; // keep track to verify behavior in tests
        cfg.uid = uid;
        cfg.clean_shutdown = clean_shutdown;
        db->Put(
          rocksdb::WriteOptions(),
          db_i,
          cfg_key,
          rocksdb::Slice{reinterpret_cast<char const *>(&cfg), sizeof(rocks_db_cfg)}); // best effort write.
      }
      if (reset_n_uid) // reset counter, only if asked for it, ref. mk_unique_ts_id
        n_uid = 0u;    // ref. mk_unique_ts_id, when n=1, we make lazy dirty mode
    }

    std::uint64_t recover_uid() const { // only during startup, so safe with no lock
      std::unique_ptr<rocksdb::Iterator> const it(db->NewIterator(rocksdb::ReadOptions(), db_i));
      std::uint64_t max_uid = 0;
      for (it->SeekToFirst(); it->Valid(); it->Next()) {
        if (it->key().ToString() == cfg_key)
          continue;
        auto const h = reinterpret_cast< ts_db_rocks_header const *>(it->value().data());
        max_uid = std::max(max_uid, h->ts_id);
      }
      return max_uid; /// recovered value.
    }

    std::uint64_t mk_unique_ts_id() {
      std::uint64_t const r = ++uid;
      if (std::uint64_t const n = ++n_uid;
          n >= max_n_before_sync || n == 1) { // ensure to mark speculative/dirty on first write
        sync_uid(max_n_before_sync, n != 1);  // and to sync every max_n_before_sync
      }
      return r;
    }

    auto lookup_calendar(std::string const & tz) const {
      auto const it = calendars.find(tz);
      if (it == calendars.end())
        return std::make_shared<calendar>(tz);
      return it->second;
    }

    ts_info get_ts_info(std::string_view const fn) const {
      auto h = read_header(fn);
      ts_info i;
      i.name = fn;
      i.point_fx = h.point_fx;
      i.modified = h.modified;
      // i.data_period = h.data_period;
      switch (h.ta_type) {
      case gta_t::CALENDAR:
      case gta_t::FIXED: {
        i.delta_t = h.dt; //(h.data_period.end-h.data_period.start)/h.n;;
        if (h.ta_type == gta_t::CALENDAR) {
          // read tz_info
          i.olson_tz_id = std::string(h.tz);
          i.data_period = utcperiod{h.t0, lookup_calendar(h.tz)->add(h.t0, h.dt, static_cast<std::int64_t>(h.n))};
        } else {
          i.data_period = utcperiod{h.t0, h.t0 + h.n * h.dt};
        }
      } break;
      case gta_t::POINT: {
        i.delta_t = utctime{0l};
        i.data_period = utcperiod{h.t0, h.t0 + h.dt};
      } break;
      }
      return i;
    }

    std::vector<ts_info> find(std::string_view const match_) const {
      std::vector<ts_info> r;
      std::string const match(match_);
      std::regex const r_match(match, std::regex_constants::ECMAScript);
      std::unique_ptr<rocksdb::Iterator> const it(db->NewIterator(rocksdb::ReadOptions(), db_i));
      // find prefix
      static char constexpr re_char[] = {'\\', '.', '(', '[', '^', '$', '*', '?', '+', '{', '|'};
      char const * e = match.data();
      while (*e && std::ranges::find(re_char, *e) == std::ranges::end(re_char))
        e++;
      auto const prefix_size = e - match.data();
      rocksdb::Slice const prefix(match.data(), prefix_size);

      for (it->Seek(prefix); it->Valid() && it->key().starts_with(prefix); it->Next()) {
        std::string key = it->key().ToString();
        if (key == cfg_key)
          continue;
        // match require full match to the expression, search allows trailing characters.
        if (std::regex_match(key, r_match)) {
          r.push_back(get_ts_info(key)); // TODO: maybe multi-core this into a job-queue
        }
      }
      return r;
    }

    void make_calendar_lookups() {
      for (int hour = -11; hour < 12; hour++) { // common fixed interval or no-dst time-zones
        auto const c = std::make_shared<calendar>(deltahours(hour));
        calendars[c->tz_info->name()] = c;
      }
      for (auto const & tz_id : calendar::region_id_list()) { // ensure we have the std list available
        calendars[tz_id] = std::make_shared<calendar>(tz_id);
      }
    }

    void remove_data_frags(std::uint64_t const ts_id, rocksdb::WriteBatch& b) const {
      frag_key const ts_begin{ts_id, 'v', utctime{0l}};
      std::unique_ptr<rocksdb::Iterator> const it(db->NewIterator(rocksdb::ReadOptions(), db_d));
      auto const key_begin = ts_begin.slice_ts_id();
      for (it->Seek(key_begin); it->Valid() && it->key().starts_with(key_begin); it->Next()) {
        b.Delete(it->key()); //
      }
    }

    void remove(std::string_view const fn) const {
      std::string old_header_s;
      if (auto const s = db->Get(rocksdb::ReadOptions(), db_i, fn, &old_header_s); !s.ok()) {
        throw std::runtime_error(fmt::format("rocksdb::failed: '{}', '{}'", fn, s.ToString()));
      }
      rocksdb::WriteBatch b;
      remove(fn, b);
      db->Write(rocksdb::WriteOptions(), &b);
    }

    void remove(std::string_view const fn, rocksdb::WriteBatch& b) const {
      std::string old_header_s;
      if (auto const s = db->Get(rocksdb::ReadOptions(), db_i, fn, &old_header_s); !s.ok()) {
        throw std::runtime_error(fmt::format("rocksdb::failed: '{}', '{}'", fn, s.ToString()));
      }
      auto const old_header = reinterpret_cast<ts_db_rocks_header*>(old_header_s.data());
      remove_data_frags(old_header->ts_id, b);
      b.Delete(db_i, fn);
    }

    utcperiod header_total_period(ts_db_rocks_header const & h) const {
      return h.total_period([this](char const * tz) {
        return *lookup_calendar(tz);
      });
    }

    template <class CFX> // CFX callable (optional<gts_t>..)
    void save(
      std::string_view fn,
      gts_t const & ts,
      bool const overwrite,
      rocksdb::WriteBatch& b,
      bool const strict_alignment,
      CFX&& fx_cache) {
      std::string old_header_s;
      std::optional<gts_t> cts; // in case we need to transform before (over)write.
      auto const s = db->Get(rocksdb::ReadOptions(), db_i, fn, &old_header_s);
      auto tp = ts.total_period();
      if (s.IsNotFound()) {
        auto const ts_id = mk_unique_ts_id();
        write_new_ts(fn, ts, ts_id, b);
      } else if (!s.ok()) {
        throw std::runtime_error(fmt::format("rocksdb::failed: '{}', '{}'", fn, s.ToString()));
      } else {
        if (overwrite) {
          remove(fn, b);
          auto const ts_id = mk_unique_ts_id(); // create new ts_id
          write_new_ts(fn, ts, ts_id, b);       // overwrite(with a converted ts if needed)
          tp = ts.total_period();
        } else {
          auto const old_header = reinterpret_cast<ts_db_rocks_header*>(old_header_s.data());
          cts = check_ta_alignment(strict_alignment, *old_header, ts);
          if (header_total_period(*old_header).empty()) { // then start out new ts from a new relocated fragment
            auto const t0 = (cts ? *cts : ts).total_period().start;
            old_header->tff = t0;
            old_header->t0 = t0;
          }
          tp = merge_ts(fn, cts ? *cts : ts, *old_header, b);
        }
      }
      fx_cache(tp, std::move(cts));
    }

    static ts_db_rocks_header mk_header(
      ts_point_fx const pfx,
      ta_generic_type const gt,
      utctime const t0,
      utctime const dt,
      std::uint64_t const n,
      utctime const tff,
      std::uint64_t const ts_id,
      std::string const & tz = std::string("")) {
      auto const modified = utctime_now();
      return ts_db_rocks_header{pfx, gt, t0, dt, n, tff, ts_id, modified, tz};
    }

    static ts_db_rocks_header mk_header(
      ts_point_fx const pfx,
      ta_generic_type const gt,
      utctime const t0,
      utctime const dt,
      std::uint64_t const n,
      std::uint64_t const ts_id,
      std::string const & tz = std::string("")) {
      auto const tff = t0;
      return mk_header(pfx, gt, t0, dt, n, tff, ts_id, tz);
    }

    static ts_db_rocks_header mk_header(gts_t const & ts, std::uint64_t const ts_id) {
      std::string tz;
      switch (ts.ta.gt()) {
      case time_axis::generic_dt::CALENDAR:
        tz = ts.ta.c().cal->tz_info->name();
        [[fallthrough]];
      case time_axis::generic_dt::FIXED:
        return mk_header(
          ts.point_interpretation(), ts.ta.gt(), ts.ta.total_period().start, ts.time_axis().dt(), ts.size(), ts_id, tz);
      case time_axis::generic_dt::POINT:
        break;
      }
      return mk_header(
        ts.point_interpretation(),
        ts.ta.gt(),
        ts.ta.total_period().start,
        ts.ta.total_period().timespan(),
        ts.size(),
        ts_id,
        tz);
    }

    void write_header(std::string_view const fn, ts_db_rocks_header const & h, rocksdb::WriteBatch& b) const {
      b.Put(db_i, fn, rocksdb::Slice(reinterpret_cast<char const *>(&h), sizeof(ts_db_rocks_header)));
    }

    /**
     * @brief write to batch key, value
     * @tparam TV std::vector type, like std::vector<utctime> or std::vector<double>, required .data() and .size()
     * @param df data fragment
     * @param key the key, as slice, by value is ok since it's a POD, pointer,size
     * @param b the rocksdb batch structure
     */
    template <std::ranges::contiguous_range TV>
    void db_write(TV&& df, rocksdb::Slice const key, rocksdb::WriteBatch& b) const {
      using T = std::ranges::range_value_t<TV>;
      b.Put(db_d, key, rocksdb::Slice(reinterpret_cast<char const *>(df.data()), sizeof(T) * df.size()));
    }

    /**
     * @brief Write time-axis/values to db
     * @precondtions:
     * there are no existing entries/keys for this ts
     */
    void write_new_data(gts_t const & ts, ts_db_rocks_header const & h, rocksdb::WriteBatch& b) {
      using std::views::counted;

      auto const write_values = [&](auto&& frag_key) {
        auto const nff = static_cast<std::int64_t>(h.n / cfg.ppf); // Number of full fragments
        for (std::int64_t i = 0; i < nff; ++i) {
          db_write(
            counted(ts.v.begin() + i * static_cast<std::int64_t>(cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
            frag_key(i),
            b);
        }
        if (h.n % cfg.ppf) {
          auto const n = h.n % cfg.ppf;
          db_write(
            counted(ts.v.cbegin() + static_cast<std::int64_t>(nff * cfg.ppf), static_cast<std::int64_t>(n)),
            frag_key(nff),
            b);
        }
      };

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        write_values([&](auto i) {
          return frag_key{h.ts_id, 'v', h.t0 + i * cfg.ppf * h.dt};
        });
      } break;
      case time_axis::generic_dt::CALENDAR: {
        auto const cal = lookup_calendar(h.tz);
        write_values([&](auto i) {
          return frag_key{h.ts_id, 'v', cal->add(h.t0, h.dt, i * cfg.ppf)};
        });
      } break;
      case time_axis::generic_dt::POINT: {
        auto const nlf = static_cast<std::int64_t>(h.n / cfg.ppf);

        auto const frag_key_fx = [&](auto i, char tv) {
          auto const ts_tp = i == ts.size() ? ts.ta.p().t_end : ts.ta.p().t[i];
          return frag_key{h.ts_id, tv, ts_tp}; // the time-point where fragment ends is used to timestamp the key
        };

        for (std::int64_t i = 0; i < nlf; ++i) {
          db_write(
            counted(ts.ta.p().t.begin() + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
            frag_key_fx((i + 1) * cfg.ppf, 't'),
            b);
          db_write(
            counted(ts.v.begin() + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
            frag_key_fx((i + 1) * cfg.ppf, 'v'),
            b);
        }

        if (auto const n_pts = ts.size() % cfg.ppf) { // last fragment could be fewer points.
          db_write(
            counted(ts.ta.p().t.begin() + static_cast<std::int64_t>(nlf * cfg.ppf), static_cast<std::int64_t>(n_pts)),
            frag_key_fx(nlf * cfg.ppf + n_pts, 't'),
            b);
          db_write(
            counted(ts.v.begin() + static_cast<std::int64_t>(nlf * cfg.ppf), static_cast<std::int64_t>(n_pts)),
            frag_key_fx(nlf * cfg.ppf + n_pts, 'v'),
            b);
        }

      } break;
      }
    }

    void write_new_ts(std::string_view const fn, gts_t const & ts, std::uint64_t const ts_id, rocksdb::WriteBatch& b) {
      auto const h = mk_header(ts, ts_id);
      write_header(fn, h, b);
      write_new_data(ts, h, b);
    }

    tuple<gts_t, utcperiod> read(std::string_view const fn, utcperiod const p) {
      auto const h = read_header(fn);
      std::uint64_t skip_n = 0u;
      std::uint64_t skip_nb = 0u; // Number of blocks to skip (only used by time_axis::generic_dt::POINT)
      auto ta = read_time_axis(h, p, skip_n, skip_nb);
      auto v = read_values(h, ta, skip_n, skip_nb);
      return std::make_tuple(gts_t{std::move(ta), std::move(v), h.point_fx}, header_total_period(h));
    }

    ts_db_rocks_header read_header(std::string_view const fn) const {
      std::string hs;
      rocksdb::ReadOptions const ro;
      if (auto const s = db->Get(ro, db_i, fn, &hs); !s.ok())
        throw std::runtime_error(fmt::format("rocksdb::failed: '{}', '{}' ", fn, s.ToString()));

      return *reinterpret_cast<ts_db_rocks_header*>(hs.data());
    }

    /**
     * @brief compute read-time-axis
     * @details
     * Given header(with its implication on period, dt, calendar),
     * and the read period, then
     * compute the resulting time-axis, and the skip sizes relative fragments.
     */
    gta_t
      read_time_axis(ts_db_rocks_header const & h, utcperiod const p, std::uint64_t& skip_n, std::uint64_t& skip_nb) {
      gta_t ta;
      ta.set_gt(h.ta_type);
      if (h.ta_type == time_axis::generic_dt::CALENDAR) {
        ta.c().cal = lookup_calendar(h.tz); // ensure to set correct calendar
        ta.c().dt = h.dt;                   // also ensure to set dt, n is 0/empty
      } else if (h.ta_type == time_axis::generic_dt::FIXED) {
        ta.f().dt = h.dt; // also ensure to set dt, n is 0/empty
      }


      utctime t_start = p.start;
      utctime t_end = p.end;
      if (t_start == no_utctime)
        t_start = min_utctime;
      if (t_end == no_utctime)
        t_end = max_utctime;

      // no overlap?
      auto h_data_period = header_total_period(h);
      if (h_data_period.end <= t_start || h_data_period.start >= t_end) {
        return ta;
      }

      skip_n = 0;

      auto const compute_ta = [&](auto& rta, auto&& diff_units, auto&& add_units) {
        rta.dt = h.dt;
        // handle various overlapping periods
        if (t_start <= h_data_period.start && t_end >= h_data_period.end) {
          // fully around or exact
          rta.t = h.t0;
          rta.n = h.n;
        } else {
          std::uint64_t drop_n = 0;
          if (t_start > h_data_period.start) // start inside
            skip_n = diff_units(h_data_period.start, t_start, rta.dt);
          rta.t = add_units(h.t0, rta.dt, skip_n);
          if (t_end < h_data_period.end) { // end inside, take care to add terminating point (linear case needed)
            if (
              h_data_period.end
              > add_units(t_end, rta.dt, 1)) // more than one unit to the end(e.g. not the last interval)
              drop_n = diff_units(t_end, h_data_period.end, rta.dt) - 1; // then include one ore(drop one less)
          }
          // -----
          rta.n = h.n - skip_n - drop_n;
        }
      };

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        compute_ta(
          ta.f(),
          [](auto t0, auto t1, auto dt) {
            return (t1 - t0) / dt;
          },
          [](auto t0, auto dt, auto n) {
            return t0 + dt * n;
          });
      } break;
      case time_axis::generic_dt::CALENDAR: {
        compute_ta(
          ta.c(),
          [&cal = ta.c().cal](auto t0, auto t1, auto dt) {
            return cal->diff_units(t0, t1, dt);
          },
          [&cal = ta.c().cal](auto t0, auto dt, auto n) {
            return cal->add(t0, dt, n);
          });

      } break;
      case time_axis::generic_dt::POINT: {
        frag_key t_key{h.ts_id, 't', utctime{0l}};
        auto st_key = t_key.slice_ts_id_type();
        utctime ts_tp{max_utctime};
        rocksdb::ReadOptions ro;
        ro.fill_cache = false; // do not try to use cache, we cache ts.internally
        std::unique_ptr<rocksdb::Iterator> it(db->NewIterator(ro, db_d));
        for (it->Seek(st_key); it->Valid() && it->key().starts_with(st_key); it->Next()) {
          ts_tp = frag_key_view(it->key()).time();
          if (ts_tp <= t_start) {
            skip_nb++;
          } else
            break;
        }

        if (it->Valid() && it->key().starts_with(st_key)) {
          frag_v<utctime> ts(it); // get a view to it, no copy
          std::vector<utctime> tmp;
          tmp.reserve(1 + ts.size()); // at least the size of what we will copy
          ts.copy(tmp);
          if (ts.back() < t_end) { // read more frag? we need surrounding reads (for linear btw. points ts)
            it->Next();
            for (; it->Valid() && it->key().starts_with(st_key); it->Next()) {
              ts_tp = frag_key_view(it->key()).time();
              frag_v<utctime>(it).copy(tmp);
              if (tmp.back() >= t_end) // did we get timepoints that covers t_end?(either exactly of or after)
                break;                 // we are done reading frags.
            }
          } // else we are done with the one frag that we read,

          //-- now we figure out what part of tmp that should be our result
          auto it_b = tmp.begin();     // first the start of the sequence
          if (t_start > tmp.front()) { // get rid of all excessive points at the beginning up to last <=t_start
            it_b = std::ranges::upper_bound(tmp.begin(), tmp.end(), t_start, std::less());
            if (it_b != tmp.begin()) { // back off one to have surrounding beginning point
              std::advance(it_b, -1);
            }
          }
          // then the end condition, recall: surrounding end point(linear between points)
          utctime f_time = ts_tp; // end of ts or end of last frag read.
          auto it_e = tmp.end();
          if (t_end <= tmp.back()) {
            it_e = std::lower_bound(it_b, tmp.end(), t_end, std::less()); // allow us to find t_end, if it exists.
            if (it_e != tmp.end()) {
              std::advance(it_e, 1); // ensure to include point at or first after t_end
              if (it_e != tmp.end()) // are we still interior of a frag?
                f_time = *it_e;      // mark our ts-fragment end where the next(unread) point starts.
            }
          }
          // -----
          skip_n = std::distance(tmp.begin(), it_b);
          ta.p().t.reserve(std::distance(it_b, it_e));
          ta.p().t.assign(it_b, it_e); // excluding  the t_end
          ta.p().t_end = f_time;
        } // else: there was no frags to read, so time-axis is zero
      } break;
      }
      return ta;
    }

    std::vector<double> read_values(
      ts_db_rocks_header const & h,
      gta_t const & ta,
      std::uint64_t const skip_n,
      std::uint64_t const skip_nb) {
      auto const h_data_period = header_total_period(h);
      auto const read_fixed_values = [&](auto& rta) {
        std::vector<double> val;
        if (rta.n == 0u) // early exit for zero reads
          return val;
        rocksdb::ReadOptions ro;
        ro.fill_cache = false; // we do not want to copy content to cache, since we are caching ts in dtss(increases
                               // performance, drops a copy)
        ro.prefix_same_as_start = true; // ensure we hint it to the 8byte ts-id prefix
        std::unique_ptr<rocksdb::Iterator> const it(db->NewIterator(ro, db_d));
        // Initial fragment might have values to skip_n
        auto const n_nan_ff = (h_data_period.start - h.tff) / h.dt; // Number of leading NaNs in the first fragment
        auto skip_n_sf = (skip_n + n_nan_ff)
                       % cfg.ppf; // Number of values to be skipped in the starting fragment to be read
        frag_key const s_frag{
          h.ts_id, 'v', h.tff + cfg.ppf * ((skip_n + n_nan_ff) / cfg.ppf) * h.dt}; // complete start frag key
        auto const ts_key = s_frag.slice_ts_id_type();
        it->Seek(s_frag);
        if (skip_n_sf) { // the first frag, we start within it, so skip the first values.
          if (!it->key().starts_with(ts_key)) {
            throw std::runtime_error("rocksdb::failed: reading from illegal fragment");
          }
          frag_v<double> const f(it);
          auto const n = f.size(); // notice: the frag does not have to be 'full'
          if (skip_n_sf < n) {     // in this case: we have a slice we should read,(we do not skip all of them)
            f.copy_slice(skip_n_sf, std::min<std::size_t>(rta.n, n - skip_n_sf), val);
          }
          // important: this is how many elements we *should* have out of this fragment, regardless
          auto const m = std::min<std::size_t>(cfg.ppf - skip_n_sf, rta.n);// min of rest and rta.n
          if (val.size() < m) {
            val.resize(m, nan); // ensure we fill out with nan (as if we read the nans), ref issue 130
          }
          it->Next();
        }

        // Read full fragments
        auto const nff = static_cast<std::int64_t>(
          skip_n_sf ? static_cast<std::int64_t>(rta.n - cfg.ppf + skip_n_sf) / static_cast<std::int64_t>(cfg.ppf)
                    : rta.n / cfg.ppf); // Number of full fragments
        for (std::int64_t i = 0ul; i < nff; ++i) {
          if (!it->key().starts_with(ts_key)) {
            throw std::runtime_error("rocksdb::failed: reading from illegal fragment");
          }
          frag_v<double> f(it);
          f.copy(val); // the frag might be less than cfg.ppf
          val.resize(val.size() + (cfg.ppf - f.size()), nan);
          it->Next();
        }
        // Last fragment might have values remaining to read
        if (auto nvr = rta.n - val.size()) { // Number of values left to read
          if (!it->key().starts_with(ts_key)) {
            throw std::runtime_error("rocksdb::failed: reading from illegal fragment");
          }
          frag_v<double> f(it);
          std::size_t to_read = std::min<std::size_t>(f.size(), nvr);
          f.copy_slice(0, to_read, val);
          val.resize(val.size() + (nvr - to_read), shyft::nan);
        }

        return val;
      };

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        return read_fixed_values(ta.f());
      }
      case time_axis::generic_dt::CALENDAR: {
        return read_fixed_values(ta.c());
      }

      case time_axis::generic_dt::POINT: {
        frag_key const f_key{h.ts_id, 'v', utctime{0l}};
        auto const vt_key = f_key.slice_ts_id_type(); //(h.ts_id, 'v');
        std::unique_ptr<rocksdb::Iterator> const it(db->NewIterator(rocksdb::ReadOptions(), db_d));
        it->Seek(vt_key);
        for (std::uint64_t i = 0; i < skip_nb; ++i)
          it->Next();
        auto const needed_size = ta.p().t.size();
        std::vector<double> tmp;
        tmp.reserve(needed_size);
        auto skip_first = skip_n; // we might have to skip some at the first frag size,
        for (; it->Valid() && it->key().starts_with(vt_key); it->Next()) {
          frag_v<double>(it).copy_slice(skip_first, tmp);
          skip_first = 0; // reset the skip_first, remaining blocks are ready as they are
          if (tmp.size() >= needed_size)
            break;
        }
        if (tmp.size() < needed_size) {
          throw std::runtime_error(
            fmt::format(
              "rocksdb::failed: read values, skip_n={}, skip_nb={}, size={} < needed_size={}",
              skip_n,
              skip_nb,
              tmp.size(),
              needed_size));
        }
        if (tmp.size() > needed_size)
          tmp.resize(needed_size); // we might get more than needed, trailing values, due to reading full frags, so we
                                   // reduce the size
        return tmp;
      }
      }
      throw std::runtime_error("rocksdb::failed: unknown time-axis type");
    }

    std::optional<gts_t>
      check_ta_alignment(bool const strict_alignment, ts_db_rocks_header const & old_header, gts_t const & ats) const {
      return make_aligned_ts(
        strict_alignment,
        old_header,
        [&](auto const & tz) {
          return lookup_calendar(tz);
        },
        ats);
    }

    template <class AU, class DU, class FT>
    utcperiod do_fixed_merge(
      std::string_view const fn,
      ts_db_rocks_header const & old_header,
      gts_t const & new_ts,
      rocksdb::WriteBatch& b,
      AU&& add_units,
      DU&& diff_units,
      FT&& frag_time) {
      auto const frag_key_fx = [&](utctime const t) {
        return frag_key{old_header.ts_id, 'v', t};
      };
      // assume the two time-axes have the same type and are aligned
      using std::views::counted;

      auto const old_p = header_total_period(old_header);
      auto new_p = new_ts.ta.total_period();


      utcperiod const ofp{old_header.tff, frag_time(add_units(old_p.end, cfg.ppf - 1))};           // [ tff, tef)
      utcperiod const ts_fp{frag_time(new_p.start), frag_time(add_units(new_p.end, cfg.ppf - 1))}; // [ tff, tef)
      utcperiod nfp{std::min(ofp.start, ts_fp.start), std::max(ofp.end, ts_fp.end)}; // the complete new fragment range.

      std::vector<double> nan_frag;       //(cfg.ppf,nan);
      auto const ts_v = new_ts.v.begin(); // points to current pos we want to write.
      std::uint64_t i = 0;                // current position into ts_v, updated as we consume values from it.

      for (utctime t = nfp.start; t < nfp.end;
           t = add_units(t, cfg.ppf)) {     // iterate over ALL fragments of the new ts, then consider each of them.
        utctime te = add_units(t, cfg.ppf); // end of current, start of next
        if (!ofp.contains(t) && !ts_fp.contains(t)) { // empty nan areas to fill,with complete nan frags.
          if (nan_frag.empty())
            nan_frag = std::vector(cfg.ppf, nan);
          db_write(nan_frag, frag_key_fx(t), b);
        } else if (ofp.contains(t) && ts_fp.contains(t)) { // tangling with old frags
          // do we need to read the old ?
          if (auto const ti = i < new_ts.ta.size() ? new_ts.ta.time(i) : new_ts.ta.total_period().end;
              ti != t || (ti == t && new_ts.size() - i < cfg.ppf)) { // we need if we start into the frag, or if we
                                                                     // start at frag, but end before
            std::vector<double> val;
            val.reserve(cfg.ppf);
            std::uint64_t ts_start_ix = diff_units(t, ti);
            utcperiod nts_frag_period{ti, std::min(te, new_p.end)};                       // new ts...
            utcperiod ots_frag_period{std::max(t, old_p.start), std::min(old_p.end, te)}; //
            double const * fvs_begin{nullptr};
            std::uint64_t fvs_size{0};
            std::uint64_t n_ts_first{0};
            std::string fvs; // needs to stay here because, refs fvs_begin etc. refs to it
            if (!nts_frag_period.contains(ots_frag_period)) { // if not new ts cover effective area. existing part:
              auto s = db->Get(rocksdb::ReadOptions(), frag_key_fx(t), &fvs);
              fvs_size = fvs.size() / sizeof(double);
              fvs_begin = reinterpret_cast<double const *>(fvs.data());
              n_ts_first = std::min(fvs_size, ts_start_ix);
              std::copy_n(fvs_begin, n_ts_first, std::back_inserter(val)); // opt:old contrib goes here.
            }
            val.insert(val.end(), ts_start_ix - n_ts_first, nan); // opt:nan up to new contrib
            auto const n_ts_fill = std::min(cfg.ppf - val.size(), new_ts.size() - i);
            std::copy_n(ts_v + static_cast<std::int64_t>(i), n_ts_fill, std::back_inserter(val)); // new contrib,
            i += n_ts_fill;

            if (fvs_begin && fvs_size > val.size()) {
              std::copy(
                fvs_begin + val.size(), fvs_begin + fvs_size, std::back_inserter(val)); // opt: old contrib trailer part
            }
            db_write(counted(val.begin(), static_cast<std::int64_t>(val.size())), frag_key_fx(t), b);
          } else { // we are covering the cfg.ppf
            db_write(
              counted(ts_v + static_cast<std::int64_t>(i), static_cast<std::int64_t>(cfg.ppf)),
              frag_key_fx(t),
              b); // ts does contain enough values.
            i += cfg.ppf;
          }
        } else if (!ofp.contains(t) && ts_fp.contains(t)) { // only new frags
          // just write new frags. trivial, almost nans, in the beginning, and the last ones
          if (auto const ti = i < new_ts.ta.size() ? new_ts.ta.time(i) : new_ts.ta.total_period().end; ti != t) {
            std::vector<double> val;
            val.reserve(cfg.ppf);
            auto const n_nan = diff_units(t, ti); // diff units(..)
            val.insert(val.end(), n_nan, nan);    // prefill nans
            auto const n_vals = std::min(
              static_cast<std::uint64_t>(cfg.ppf - n_nan), static_cast<std::uint64_t>(new_ts.v.size() - i));
            std::copy_n(ts_v + static_cast<std::int64_t>(i), n_vals, std::back_inserter(val)); // fill with avail values
            i += n_vals;
            // if out of values, this is the last frag.
            if (t < ofp.start) { // need to fill because there are nan blocks to fill after
              val.insert(val.end(), cfg.ppf - val.size(), nan); // post fill nans
            }
            db_write(counted(val.begin(), static_cast<std::int64_t>(val.size())), frag_key_fx(t), b);
          } else { // starts at beginning of frag, flush out to cfg.ppf, or end of ts
            auto const n = std::min(cfg.ppf, new_ts.size() - i);
            db_write(counted(ts_v + static_cast<std::int64_t>(i), static_cast<std::int64_t>(n)), frag_key_fx(t), b);
            i += n;
          }
        } // else  case only old values, do nothing
      }
      // update header
      // nfp fragment period, tff,
      utcperiod total_p{std::min(old_p.start, new_p.start), std::max(old_p.end, new_p.end)};
      auto points_n = diff_units(total_p.start, total_p.end);

      auto new_header = mk_header(
        new_ts.fx_policy,
        old_header.ta_type,
        total_p.start,
        old_header.dt,
        points_n,
        nfp.start,
        old_header.ts_id,
        old_header.tz);
      write_header(fn, new_header, b);
      return total_p;
    }

    utcperiod do_point_merge(
      std::string_view fn,
      ts_db_rocks_header const & old_header,
      gts_t const & new_ts,
      rocksdb::WriteBatch& b) const {
      using std::views::counted;

      auto const old_p = header_total_period(old_header);
      auto const new_p = new_ts.ta.total_period();
      // determine if we are entirely before or after old ts, because then we do not touch existing frags.

      auto const db_write_tv = [&](auto const t_range, auto const v_range, utctime const lts) {
        db_write(t_range, frag_key{old_header.ts_id, 't', lts}, b);
        db_write(v_range, frag_key{old_header.ts_id, 'v', lts}, b);
      };
      std::vector<utctime> tx_; // in case merge with any other ta, fixed/cal, make timepoints here.
      if (new_ts.ta.gt() != time_axis::generic_dt::POINT) {
        tx_.reserve(new_ts.ta.size());
        for (std::uint64_t i = 0; i < new_ts.ta.size(); ++i)
          tx_.push_back(new_ts.ta.time(i));
      }

      auto t_ = tx_.empty() ? new_ts.ta.p().t.cbegin() : tx_.cbegin(); // shorthands for t_,v_ begin/iterators
      auto v_ = new_ts.v.cbegin();
      std::uint64_t ndp{0}; // number of deleted points (we need to track what we delete, hmm)
      std::uint64_t nip{0}; // number of inserted points(could be one nan-insert to keep f(t) semantics

      if (new_p.end <= old_p.start) { // entirely before, in a way that we do not have to touch existing frags

        // we fill up the last frag with more date than n-ppf.
        std::uint64_t n_frags = (new_ts.size() + cfg.ppf - 1) / cfg.ppf;
        for (std::uint64_t i = 0; i < n_frags; ++i) {
          if (i + 1 == n_frags) { // last frag? (and possibly first!)
            auto n_left = new_ts.size() - i * cfg.ppf;
            if (
              new_p.end < old_p.start) { // last frag might need extra nan at the end of it, if new_p.end < old_p.start,
              std::vector<double> v;
              v.reserve(1 + n_left);
              std::vector<utctime> t;
              t.reserve(1 + n_left);
              std::copy_n(
                t_ + static_cast<std::int64_t>(i * cfg.ppf), n_left, std::back_inserter(t)); // then fill in with rest.
              std::copy_n(v_ + static_cast<std::int64_t>(i * cfg.ppf), n_left, std::back_inserter(v));
              t.push_back(new_p.end);
              v.push_back(nan); // at the end, add t, nan to ensure f(t) is consistent
              db_write_tv(
                counted(t.cbegin(), static_cast<std::int64_t>(t.size())),
                counted(v.cbegin(), static_cast<std::int64_t>(v.size())),
                new_p.end);
              ++nip;
            } else {
              db_write_tv(
                counted(t_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(n_left)),
                counted(v_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(n_left)),
                new_p.end);
            }
          } else { // more than 1 fragment, so we can write cfg.ppf elements
            db_write_tv(
              counted(t_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
              counted(v_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
              new_ts.ta.time((i + 1) * cfg.ppf)); // write cfg.ppf
          }
        }
      } else if (new_p.start >= old_p.end) { // entirely after,  in a way that we can leave existing frags as they are.
        std::uint64_t n_frags = (new_ts.size() + cfg.ppf - 1)
                              / cfg.ppf; // we fill up the last frag with more date than n-ppf.
        for (std::uint64_t i = 0; i < n_frags; ++i) {
          // first frag (potentially also the last)
          if (i == 0) {
            // might be cfg.ppf, or remainder of ts.
            auto const n_left = std::min(static_cast<std::uint64_t>(new_ts.size()), cfg.ppf);
            // first frag might need extra nan in front, to ensure f(t) consistency is prevailed
            if (new_p.start > old_p.end) {
              std::vector<double> v;
              v.reserve(1 + n_left);
              std::vector<utctime> t;
              t.reserve(1 + n_left);
              t.push_back(old_p.end);
              v.push_back(nan); // at beginning, add t, nan to ensure f(t) is consistent
              std::copy_n(
                t_ + static_cast<std::int64_t>(i * cfg.ppf), n_left, std::back_inserter(t)); // then fill in with rest.
              std::copy_n(v_ + static_cast<std::int64_t>(i * cfg.ppf), n_left, std::back_inserter(v));
              db_write_tv(
                counted(t.cbegin(), static_cast<std::int64_t>(t.size())),
                counted(v.cbegin(), static_cast<std::int64_t>(v.size())),
                n_left == new_ts.size() ? new_p.end : new_ts.ta.time(cfg.ppf));
              ++nip;
            } else {
              // write cfg.ppf
              db_write_tv(
                counted(t_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(n_left)),
                counted(v_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(n_left)),
                n_left == new_ts.size() ? new_p.end : new_ts.ta.time(cfg.ppf));
            }
          } else if (i + 1 == n_frags) { // last frag.
            auto n_left = new_ts.size() - i * cfg.ppf;
            db_write_tv(
              counted(t_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(n_left)),
              counted(v_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(n_left)),
              new_p.end); // write cfg.ppf
          } else {
            db_write_tv(
              counted(t_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
              counted(v_ + static_cast<std::int64_t>(i * cfg.ppf), static_cast<std::int64_t>(cfg.ppf)),
              new_ts.ta.time((i + 1) * cfg.ppf)); // write cfg.ppf
          }
        }
      } else { // some kind of overlap (beginning, interior-only, or end), so we  must iterate over existing frags.
        frag_key f_key{old_header.ts_id, 't', utctime{0l}};
        auto st_key = f_key.slice_ts_id_type();
        std::unique_ptr<rocksdb::Iterator> it(db->NewIterator(rocksdb::ReadOptions(), db_d));
        std::unique_ptr<rocksdb::Iterator> iv(db->NewIterator(rocksdb::ReadOptions(), db_d));
        utctime t_frag_start{old_header.t0}; // the first frag from the old ts will have this start-time
        // question: should the first loop just read/prep
        utctime t_frag_end;
        std::vector<utctime> t_prepend, t_append; // next loop will add values to prepend//append
        std::vector<double> v_prepend, v_append;  // for existing frags that we partly overlap before and/or after

        for (it->Seek(st_key); it->Valid() && it->key().starts_with(st_key);
             it->Next()) { // ts_id t, we are searching time index, ordered
          auto t_key = it->key();
          t_frag_end = frag_key_view(t_key).time();
          if (t_frag_end < new_p.start) { // the frag is entirely before our period range, so untouched.
            t_frag_start = t_frag_end;    // use the end of this frag as estimate for the start of next frag.
            continue;
          }
          frag_key v_key{old_header.ts_id, 'v', t_frag_end};
          // our range cover everything, nothing to keep from the old frag,
          if (new_p.start <= t_frag_start && new_p.end >= t_frag_end) {
            // the frag is entirely covered by new,
            // so delete t&v key -- done conditionally at the end of the loop
            // conceptually,we need to count how many points we delete, thus we need to read (ouch) the time-index.
            ndp += frag_v<utctime>(it).size(); // a read, just to count number of deleted points.
          } else {                             // partial overlap
            iv->Seek(v_key);                   // we need to get the values as well, for this specific frag
            if (!iv->Valid()) {
              std::string tstamp{calendar().to_string(t_frag_end)};
              throw std::runtime_error(
                fmt::format("rocksdb::failed: no such v-key: {} @ {}", old_header.ts_id, tstamp));
            }
            frag_v<double> fv(iv); // a slice, zero copy
            frag_v<utctime> ft(it);

            std::uint64_t n_kept{0};        // we need to keep track of points kept/ n-pd count
            if (ft.front() < new_p.start) { // keep first value case
              t_prepend.reserve(cfg.ppf);
              v_prepend.reserve(cfg.ppf);
              auto ti = ft.data();
              auto te = ti + ft.size();
              auto vi = fv.data();
              while (ti < te && *ti < new_p.start) {
                t_prepend.push_back(*ti++);
                v_prepend.push_back(*vi++);
              }
              n_kept += std::distance(ft.data(), ti);
            }

            if (new_p.end < t_frag_end) { // keep last value case
              auto ti = ft.data();
              auto te = ti + ft.size();
              ti = std::lower_bound(ti, te, new_p.end);
              auto n = std::distance(ft.data(), ti);
              t_append.reserve(cfg.ppf);
              v_append.reserve(cfg.ppf);
              auto vi = fv.data() + n;
              if (new_p.end != *ti) { // new end INSIDE a period of old  =>  insert value from old where new end
                t_append.push_back(new_p.end);
                vi--;
                v_append.push_back(*vi++);
                n_kept++;
              }
              n_kept += std::distance(ti, te);
              while (ti < te) {
                t_append.push_back(*ti++);
                v_append.push_back(*vi++);
              }
            }
            ndp += ft.size() - n_kept; // update effective number of deleted points
          }
          // delete
          b.Delete(t_key);
          b.Delete(v_key);

          if (new_p.end < t_frag_end) { // next frags will be entirely new ts period, we are done iterating old frags
            break;
          }
          t_frag_start = t_frag_end; // use the end of this frag as estimate for the start of next frag.
        }

        // we are done deleting overlapped t/v keys, and collected prepend/append values for frag overlaps.
        // Straight forward: write prepend, then ts, then append fragment
        //  TODO: possible to do some optimisation to prune out small frags:
        //        if prepend.size()+ts.size()+append.size() < 2*cfg.ppf: Then merge and write one frag.
        if (!t_prepend.empty()) { // write the overlap frag at the beginning
          db_write_tv(
            counted(t_prepend.cbegin(), static_cast<std::int64_t>(t_prepend.size())),
            counted(v_prepend.cbegin(), static_cast<std::int64_t>(v_prepend.size())),
            new_p.start);
        }

        std::uint64_t n_frags = (new_ts.size() + cfg.ppf - 1) / cfg.ppf; // the frags from ts
        for (std::uint64_t i = 0; i < n_frags; ++i) {
          auto n = static_cast<std::int64_t>(std::min(cfg.ppf, new_ts.size() - i * cfg.ppf));
          db_write_tv(
            counted(t_ + static_cast<std::int64_t>(i * cfg.ppf), n),
            counted(v_ + static_cast<std::int64_t>(i * cfg.ppf), n),
            i == n_frags - 1 ? new_p.end : new_ts.ta.time((i + 1) * cfg.ppf));
        }

        if (!t_append.empty()) { // and eventually a part of the trailing values.
          db_write_tv(
            counted(t_append.cbegin(), static_cast<std::int64_t>(t_append.size())),
            counted(v_append.cbegin(), static_cast<std::int64_t>(v_append.size())),
            t_frag_end);
        }
      }
      // finally write the header:
      utcperiod tot_p = old_p.empty() ? new_p
                                      : utcperiod{std::min(old_p.start, new_p.start), std::max(old_p.end, new_p.end)};
      auto new_header = mk_header(
        new_ts.fx_policy,
        old_header.ta_type,
        tot_p.start,
        tot_p.timespan(), // dt is the complete span of the time-series in this case
        old_header.n + nip + new_ts.size() - ndp,
        old_header.ts_id);
      write_header(fn, new_header, b);
      return tot_p;
    }

    utcperiod merge_ts(
      std::string_view const fn,
      gts_t const & new_ts,
      ts_db_rocks_header const & old_header,
      rocksdb::WriteBatch& b) {
      // assume the two time-axes have the same type and are aligned
      // already done: check_ta_alignment(old_header, new_ts);

      switch (old_header.ta_type) {
      case time_axis::generic_dt::FIXED: {
        return do_fixed_merge(
          fn,
          old_header,
          new_ts,
          b,
          [&](utctime const t0, int const n) { // add
            return t0 + n * old_header.dt;
          },

          [&](utctime const t0, utctime const t1) { // diff
            return (t1 - t0) / old_header.dt;       // fixed interval def.
          },

          [&](utctime const t) { // frag time
            if (t >= old_header.tff) {
              return old_header.tff
                   + old_header.dt * cfg.ppf
                       * static_cast<std::int64_t>((t - old_header.tff) / (cfg.ppf * old_header.dt));
            }
            return old_header.tff
                 - old_header.dt * cfg.ppf
                     * (1 + static_cast<std::int64_t>((old_header.tff - t) / (cfg.ppf * old_header.dt)));

          });
      }
      case time_axis::generic_dt::CALENDAR: {
        auto const cal = lookup_calendar(old_header.tz); // maybe better/faster to use new_ts.ta.c().cal, avoid  lookup.
        return do_fixed_merge(
          fn,
          old_header,
          new_ts,
          b,
          [&](utctime const t0, int const n) { // add
            return cal->add(t0, old_header.dt, n);
          },
          [&](utctime const t0, utctime const t1) { // diff
            return cal->diff_units(t0, t1, old_header.dt);
          },
          [&](utctime const t) { // frag-time
            if (t >= old_header.tff) {
              auto const n_ppf_units = cal->diff_units(old_header.tff, t, old_header.dt) / cfg.ppf;
              return cal->add(old_header.tff, old_header.dt, static_cast<std::int64_t>(cfg.ppf * n_ppf_units));
            }
            auto const n_ppf_units = 1 + cal->diff_units(t, old_header.tff, old_header.dt) / cfg.ppf;
            return cal->add(old_header.tff, old_header.dt, -static_cast<std::int64_t>(cfg.ppf * n_ppf_units));
          });
      }
      case time_axis::generic_dt::POINT: {
        return do_point_merge(fn, old_header, new_ts, b);
      }
      }
      return {};
    }
  };
}

//------------------ ts_db_rocks -------------
namespace shyft::dtss {
  using namespace detail;

  bool ts_db_rocks::exists_at(std::string const & root_dir) {
    return ts_db_rocks_impl::exists_at(root_dir);
  }

  ts_db_rocks::ts_db_rocks(std::string const & root_dir, db_cfg const & cfg)
    : impl{std::make_unique<ts_db_rocks_impl>(root_dir, cfg)} {
  }

  ts_db_rocks::~ts_db_rocks() = default;

  diags_t ts_db_rocks::save(std::string_view fn, gts_t const & ts, fx_cache_t fx_cache, store_policy const & policy) {
    return save(
      1u,
      [&](std::uint64_t) {
        return ts_item_t{fn, ts};
      },
      fx_cache,
      policy);
  }

  diags_t
    ts_db_rocks::save(std::size_t n, fx_ts_item_t const & fx_item, fx_cache_t fx_cache, store_policy const & policy) {
    if (n == 0u)
      return {}; // noop as pr now
    rocksdb::WriteBatch batch;
    diags_t diags;
    for (std::size_t i = 0; i < n; ++i) {
      auto [fn, ts] = fx_item(i);
      try {
        impl->save(fn, ts, policy.recreate, batch, policy.strict, [&](utcperiod const & tp, std::optional<gts_t> ats) {
          if (fx_cache) {
            fx_cache(i, std::move(ats), tp);
          }
        });
      } catch (std::runtime_error const & re) {
        if (policy.best_effort) {
          if (!diags) // assumption: currently any runtime error is miss-matched resolution
            diags.emplace();
          diags->emplace_back(i, ts_diagnostics::miss_matched_resolution);
        } else
          throw std::runtime_error(fmt::format("rocksdb save: {}, ts-name={}", re.what(), fn)); // annotate problem ts.
      }
    }
    auto r1 = impl->db->Write(rocksdb::WriteOptions(), &batch);
    if (!r1.ok())
      throw std::runtime_error(fmt::format("rocksdb: batch ts data commit failed {}", r1.ToString()));

    return diags;
  }

  tuple<gts_t, utcperiod> ts_db_rocks::read(std::string_view fn, utcperiod p) {
    return impl->read(fn, p);
  }

  void ts_db_rocks::remove(std::string_view fn) {
    impl->remove(fn);
  }

  ts_info ts_db_rocks::get_ts_info(std::string_view fn) {
    return impl->get_ts_info(fn);
  }

  std::vector<ts_info> ts_db_rocks::find(std::string_view match) {
    return impl->find(match);
  }

  std::string ts_db_rocks::root_dir() const {
    return impl->root_dir;
  }

  void ts_db_rocks::mark_for_deletion() {
    delete_db_action.arm(impl->root_dir);
  }

  std::uint64_t ts_db_rocks::current_uid() const {
    return impl->uid;
  }

  std::uint64_t ts_db_rocks::clean_shutdown() const {
    return impl->cfg.clean_shutdown;
  }

  std::uint64_t ts_db_rocks::sync_count() const {
    return impl->sync_count;
  }

  bool ts_db_rocks::rollback_migration_at(std::string const & root_dir) {
    return ts_db_rocks_impl::rollback_migration_at(root_dir);
  }

  void ts_db_rocks::maintain(bool const info_db, bool const data_db) const {
    impl->maintain(info_db, data_db);
  }

}
