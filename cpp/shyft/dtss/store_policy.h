#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/core/reflection.h>

namespace shyft::dtss {

  /**
   * @brief store_policy
   * @details
   * When storing ts fragments, the policy keep
   * information how to store it.
   */
  struct store_policy {
    bool recreate{false};   ///< if existing ts, wipe it/clear it from cache prior to store
    bool strict{true};      ///< strict require fragment to equal the target time-series time-axis
    bool cache{true};       ///< cache fragments as they are stored
    bool best_effort{false}; ///< store as many as possible, returns diags instead of throw on error for items

    SHYFT_DEFINE_STRUCT(store_policy, (), (recreate, strict, cache, best_effort));
    auto operator<=>(store_policy const &) const = default;

    x_serialize_decl();
  };

}

x_serialize_export_key_nt(shyft::dtss::store_policy);
BOOST_CLASS_VERSION(shyft::dtss::store_policy, 1);

template <typename Char>
struct fmt::formatter<shyft::dtss::store_policy, Char> {
  FMT_CONSTEXPR auto parse(auto& ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  auto format(shyft::dtss::store_policy const & o, auto& ctx) const {
    auto out = ctx.out();
    return fmt::format_to(
      out,
      "{{.recreate = {},.strict = {},.cache = {},.best_effort = {} }}",
      o.recreate,
      o.strict,
      o.cache,
      o.best_effort);
  }
};
