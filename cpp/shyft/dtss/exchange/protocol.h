#pragma once

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>

namespace shyft::dtss::exchange {

#define EXCHANGE_TYPES (queue, time_series)

  SHYFT_DEFINE_ENUM(exchange_type, std::uint8_t, EXCHANGE_TYPES)


}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::dtss::exchange::exchange_type);
