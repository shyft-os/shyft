#pragma once

#include <mutex>
#include <ranges>
#include <unordered_map>
#include <utility>
#include <vector>

#include <shyft/dtss/q_bridge/engine.h>
#include <shyft/dtss/transfer/engine.h>

namespace shyft::dtss::exchange {

  /// @brief Namespace for tag-mapping the types the manager will deal with
  namespace detail {

    /// @brief the engine_type_impl map exchange_type to its engine, using unique ptr as holders
    template <auto>
    struct engine_type_impl { };

    template <>
    struct engine_type_impl<exchange_type::time_series> {
      using e_type = transfer::engine;
      using type = std::unique_ptr<e_type>;
    };

    template <>
    struct engine_type_impl<exchange_type::queue> {
      using e_type = q_bridge::engine;
      using type = std::unique_ptr<e_type>;
    };

    /// @brief the config_type_impl map exchange_type to the engine config type
    template <auto>
    struct config_type_impl { };

    template <>
    struct config_type_impl<exchange_type::time_series> {
      using type = transfer::configuration;
    };

    template <>
    struct config_type_impl<exchange_type::queue> {
      using type = q_bridge::configuration;
    };

    /// @brief the status_type_impl map exchange_type to the engine status type
    template <auto>
    struct status_type_impl { };

    template <>
    struct status_type_impl<exchange_type::time_series> {
      using type = transfer::status;
    };

    template <>
    struct status_type_impl<exchange_type::queue> {
      using type = q_bridge::status;
    };

  }

  template <auto tag>
  using engine_type = typename detail::engine_type_impl<tag>::type;

  /**
   * @brief engine_manager
   * @details
   * Keeps the list of active engines, one for each type,
   * and ensure access to each engine, adding/removing
   * engines are done in an optimal and threadsafe way.
   *
   */
  struct engine_manager {

    /// @brief the engine_container is map where key is name of engine, and the engine.
    /// @details
    /// Protected access for the map is provided through the mutex,
    /// and mutate and observe functions encapsulates this allowing to use
    /// lambda/verbs to perform operations.
    template <exchange_type M>
    struct engine_container {
      mutable std::mutex mutex;
      std::unordered_map<std::string, engine_type<M>> engines;
    };

    /// @brief container_types
    /// @details mp_rename() creates a std::tuple< engine_container<type>...>
    using container_types = boost::mp11::mp_rename<tagged_types_t<exchange_type, engine_container>, std::tuple>;

    template <exchange_type T>
    decltype(auto) mutate(auto &&f) {
      auto &[mutex, models] = container<T>();
      std::scoped_lock lock{mutex};
      return std::invoke(SHYFT_FWD(f), models);
    }

    template <exchange_type M>
    decltype(auto) observe(auto &&f) const {
      auto &[mutex, models] = container<M>();
      std::scoped_lock lock{mutex};
      return std::invoke(SHYFT_FWD(f), models);
    }

    template <exchange_type T>
    auto const &container() const noexcept {
      return std::get<etoi(T)>(containers);
    }

    template <exchange_type T>
    auto &container() noexcept {
      return std::get<etoi(T)>(containers);
    }

    container_types containers; ///< the container that keep the engines<type>
  };

  /**
   * @brief The exchange manager
   * @details
   * Responsible for managing the exchange engines, currently dtss::q_bridge and dtss::transfer,
   * which most likely is not going to be extended with new types of exchanges due to the
   * role of the dtss (it deals with time-series, in queues, or as stored entities).
   * The class somewhat streamlines the use of common verbs for engines,
   * like start,stop, status and get_config.
   *
   */
  struct manager {

    server_state &server;
    engine_manager engines;

    //-- common stuff that require type-similarity, or tagged types

    /**
     * @brief start_activity
     * For engine_type, create named engine with the supplied config,
     * and start it.
     * @note
     * The engines should be created so that constructor and start() are short time,
     * otherwise, this operation will influence other calls that tries to
     * reach the same engine-type.
     */
    template <exchange_type type>
    void start_activity(typename detail::config_type_impl<type>::type const &conf) {
      engines.mutate<type>([&](auto &&engines_) {
        if (auto it = engines_.find(conf.name); it != engines_.end()) {
          throw std::runtime_error(fmt::format("Attempt to start already existing exchange activity {}", conf.name));
        }
        auto e = std::make_unique<typename detail::engine_type_impl<type>::e_type>(server, conf);
        e->start(); // might take time, and might throw, which is ok, because it is then taken care of
        // as a consequence, only start-able engines will enter the engines map.
        engines_[conf.name] = std::move(e);
      });
    }

    /**
     * @brief get activities
     * @details
     * For a specific exchange type, provide the list of active engines.
     */
    template <exchange_type type>
    auto get_activities() {
      using return_type = std::vector<typename detail::config_type_impl<type>::type>;
      return_type ret;
      engines.observe<type>([&](auto &&engines_) {
        ret.reserve(engines_.size());
        std::ranges::copy(
          engines_ | std::views::values | std::views::transform([&](auto &&value) {
            return value->get_configuration();
          }),
          std::back_inserter(ret));
      });
      return ret;
    }

    /**
     * @brief get activity status
     * @details
     * For a specific exchange type, provide the activity status,
     * for the engine, typically provides useful monitoring,
     * performance, and diagnostics information.
     * @param name The name of the activity
     * @param clear Clear out errors/statistics before returning
     */
    template <exchange_type type>
    auto get_activity_status(std::string const &name, bool const clear) {
      using return_type = typename detail::status_type_impl<type>::type;
      return_type ret;
      engines.observe<type>([&](auto &&engines_) {
        auto it = engines_.find(name);
        if (it == engines_.end())
          throw std::runtime_error(fmt::format("Exchange activity {} not found", name));
        ret = it->second->get_status(clear);
      });
      return ret;
    }

    /**
     * @brief stop activity
     * @details
     * For a specific exchange type, stop and remove the activity
     * @note
     * The stop af ongoing activity might take time due to io and related
     * timing. The engines always try to stop in a fast but controlled
     * way.
     * @note
     * As from the original code of dtss::transfer,
     * it is important to not (b)lock the
     * engines while performing the potential long time operation of stopping the service
     * @param name The name of the activity
     * @param max_wait_time To use in the attempt of stopping the activity
     */
    template <exchange_type type>
    void stop_activity(std::string const &name, utctime const &max_wait_time) {
      auto engine = engines.mutate<type>([&](auto &&engines_) {
        auto find = engines_.find(name);
        if (find == std::end(engines_))
          throw std::runtime_error(
            fmt::format(
              "Attempt to stop not found exchange activity {} of ({} {} activities)", name, type, engines_.size()));
        auto found_engine = std::move(find->second); // move it out
        engines_.erase(find);                        // remove from list
        return found_engine;                         // return it
      });
      if (engine)
        engine->stop(max_wait_time); // ensure to allow stop request to be executed in parallel
    }

    /**
     * @defgroup transfer exchange functions
     * @brief Dedicated functions related to dtss::transfer
     * @{
     */
    void start_transfer(transfer::configuration const &cfg) {
      start_activity<exchange_type::time_series>(cfg);
    }

    auto get_transfers() {
      return get_activities<exchange_type::time_series>();
    }

    auto get_transfer_status(std::string const &name, bool const clear) {
      return get_activity_status<exchange_type::time_series>(name, clear);
    }

    void stop_transfer(std::string const &name, utctime const &max_wait_time) {
      stop_activity<exchange_type::time_series>(name, max_wait_time);
    }

    /// @}

    /**
     * @defgroup q_bridge exchange functions
     * @brief Dedicated functions related to dtss::q_bridge
     * @{
     */

    void start_q_bridge(q_bridge::configuration const &cfg) {
      start_activity<exchange_type::queue>(cfg);
    }

    auto get_q_bridges() {
      return get_activities<exchange_type::queue>();
    }

    auto get_q_bridge_status(std::string const &name, bool const clear) {
      return get_activity_status<exchange_type::queue>(name, clear);
    }

    void stop_q_bridge(std::string const &name, utctime const &max_wait_time) {
      stop_activity<exchange_type::queue>(name, max_wait_time);
    }

    /// @}
  };

}
