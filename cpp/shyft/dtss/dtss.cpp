#include <ranges>
#include <unordered_map>
#include <utility>
#include <vector>

#include <fmt/core.h>

#include <shyft/core/dlib_utils.h>
#include <shyft/core/fs_compat.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/master_slave_sync.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/resolve_tsv.h>

namespace shyft::dtss {
  /**
   * @brief reconstruct and add correct idb at specified location
   *
   */
  void reconstruct_db_at(
    server_state &state,
    std::string const &c_name,
    std::string const &c_pth,
    db_cfg const &default_cfg) {
    if (ts_db_rocks::exists_at(c_pth))
      state.container[c_name] = std::make_unique<ts_db_rocks>(c_pth, default_cfg);
    else
      state.container[c_name] = std::make_unique<ts_db>(c_pth);
    if (auto found_geo = geo_ts_db_scan(state, c_pth); found_geo)
      state.geo[c_name] = *found_geo;
  }

  void set_container_cfg(server_state &state, std::string const &cfg_path) {
    if (cfg_path.empty())
      return;
    std::unique_lock sl(state.c_mx);
    if (!fs::exists(cfg_path)) {
      fs::path p{cfg_path};
      if (auto p_dir = p.parent_path(); !fs::exists(p_dir))
        fs::create_directories(p_dir);
      container_config cc;
      for (auto const &[c_name, db] : state.container) {
        cc.container_cfg[c_name] = db->root_dir();
      }
      container_config::store(cfg_path, cc);
    } else {
      // there is one, read it
      //  add//merge existing containers to the map and store
      //  construct those in the map, that are missing in current.
      if (!fs::is_regular_file(cfg_path))
        throw std::runtime_error(fmt::format("dtss config must be a file: {}", cfg_path));
      auto cc = container_config::read(cfg_path);
      bool modified{false};
      for (auto const &[c_name, db] : state.container) {
        if (cc.container_cfg.find(c_name) == cc.container_cfg.end()) {
          cc.container_cfg[c_name] = db->root_dir(); // add container we are missing to cc
          modified = true;
        } else {
          // its already there, we could check for db->root_dir() vs. cc[c_name]
          // and if different:
          //  what could we do?
          // .. [X] just update the cc!
          // .. [ ] we could update location
          // .. [ ] we could throw.
          if (cc.container_cfg[c_name] != db->root_dir()) {
            cc.container_cfg[c_name] = db->root_dir();
            modified = true;
          }
        }
      }
      if (modified)
        container_config::store(cfg_path, cc); // store it
      // now iterate over the cc, and reconstruct missing
      for (auto const &[c_name, db_root] : cc.container_cfg) {
        if (state.container.find(c_name) != state.container.end())
          continue; // skip those that already exist
        // create and add those not yet in container map:
        reconstruct_db_at(state, c_name, db_root, state.default_geo_db_cfg);
      }
    }
    state.cfg_file = cfg_path;
  }

  server_state::server_state()
    : exchange_manager{*this} {
  }

  server_state::server_state(server_callbacks const &callbacks)
    : callbacks{callbacks}
    , exchange_manager{*this} {
  }

  server_state::~server_state() {
    clear_master(*this);
  }

  void set_master(
    server_state &state,
    string ip,
    int port,
    double master_poll_time,
    size_t unsubscribe_min_threshold,
    double unsubscribe_max_delay,
    version_type protocol_version) {
    if (state.msync)
      throw std::runtime_error("Trying to set master on a dtss with master set");
    state.msync = std::make_unique<master_slave_sync>(
      state, ip, port, master_poll_time, unsubscribe_min_threshold, unsubscribe_max_delay, protocol_version);
  }

  void clear_master(server_state &state) {
    if (state.msync) {
      state.terminate = true;
      state.msync.reset();
    }
  }

  void add_container(
    server_state &state,
    std::string const &container_name,
    std::string const &root_path,
    std::string container_type,
    db_cfg cfg) {
    std::unique_lock sl(state.c_mx);
    // factory.. CD::create_container(container_name, container_type, root_dir, *this);
    std::string server_container_name;
    if (!container_type.empty() && container_type != "ts_db" && container_type != "ts_rdb")
      throw std::runtime_error{fmt::format("Cannot construct unknown container type: {}", container_type)};
    if (state.container.contains(container_name))
      return; // if container is already there, then ignore the call, the caller can ask for containers, remove and
              // add.
    // above is also useful in hybrid/server/client situations, where some containers are added server-side,
    // and others are added by client later (on restart server, we might end up adding twice, so we rather accept
    // that)
    server_container_name = container_name;
    if (container_type == "ts_rdb") {
      state.container[server_container_name] = std::make_unique<ts_db_rocks>(root_path, cfg);
    } else {
      state.container[server_container_name] = std::make_unique<ts_db>(root_path);
    }

    if (auto found_geo = geo_ts_db_scan(state, root_path); found_geo)
      state.geo[container_name] = *found_geo;

    // NOTE: The "" empty root do have specific semantics: scan and load auto geo
    // db on 1st level directories
    if (server_container_name.empty()) {
      fs::directory_iterator dir(root_path), end;
      for (; dir != end; ++dir) {
        if (!fs::is_directory(dir->path()))
          continue;
        auto found_geo = geo_ts_db_scan(state, dir->path());
        if (!found_geo)
          continue;
        auto geo_container = (*found_geo)->name; // the shyft://geo_container/...
        if (state.container.find(geo_container) != state.container.end())
          continue;
        // now, register found geo/container
        // could be a ldb, or a plain file db
        reconstruct_db_at(state, geo_container, dir->path().generic_string(), state.default_geo_db_cfg);
      }
    }
    if (!state.cfg_file.empty()) { // update the persisted storage here.
      auto cc = container_config::read(state.cfg_file);
      cc.container_cfg[server_container_name] = root_path;
      container_config::store(state.cfg_file, cc);
    }
  }

  void maintain_backend_container(
    server_state &state,
    std::string const &container_name,
    bool const info_db,
    bool const data_db) {
    auto const db = dynamic_cast<ts_db_rocks *>(internal_ptr(state, container_name));
    if (!db)
      throw std::runtime_error(fmt::format("Cannot find rocksdb type of container named: {}", container_name));
    db->maintain(info_db, data_db);
  }

  void remove_container(server_state &state, std::string const &container_url, bool remove_from_disk) {
    if (container_url.empty()) {
      return;
    }
    auto container_name = extract_shyft_url_container(container_url);
    if (!container_name.empty()) { // is non root shyft container
      auto itr = state.container.find(container_name);
      if (itr != state.container.end()) {
        if (remove_from_disk) {
          itr->second->mark_for_deletion();
        }
        state.container.erase(itr);
        if (!state.cfg_file.empty()) { // if persistent cfg configure, ensure to remove
          auto cc = container_config::read(state.cfg_file);
          cc.container_cfg.erase(container_name);
          container_config::store(state.cfg_file, cc);
        }
      }
      return;
    }
    if (state.callbacks.remove_external) {
      state.callbacks.remove_external(container_url, remove_from_disk);
    }
  }

  void swap_container(server_state &state, std::string const &container_name_a, std::string const &container_name_b) {
    std::unique_lock sl(state.c_mx);
    auto a = state.container.find(container_name_a);
    if (a == state.container.end())
      throw std::runtime_error(fmt::format("container a: {} not found", container_name_a));
    auto b = state.container.find(container_name_b);
    if (b == state.container.end())
      throw std::runtime_error(fmt::format("container b: {} not found", container_name_b));
    // also ensure we verify and swap config
    if (!state.cfg_file.empty()) {
      auto cc = container_config::read(state.cfg_file);
      auto a = cc.container_cfg.find(container_name_a);
      // if anything goes wrong here.. it is consistent
      // currently we just throw here, but we could
      // attempt to repair later.
      if (a == cc.container_cfg.end())
        throw std::runtime_error(fmt::format("container.cfg a: {} not found", container_name_a));
      auto b = cc.container_cfg.find(container_name_b);
      if (b == cc.container_cfg.end())
        throw std::runtime_error(fmt::format("container.cfg b: {} not found", container_name_b));
      std::swap(a->second, b->second);
      container_config::store(state.cfg_file, cc);
    }
    std::swap(a->second, b->second);
  }

  server_state::container_t::iterator container_find(server_state &state, std::string_view container_name) {
    auto f = state.container.find(container_name);
    if (f == std::end(state.container)) {
      f = state.container.find(""); // try to find default container
    }
    return f;
  }

  its_db &internal(server_state &state, std::string_view container_name) {
    auto f = container_find(state, container_name);
    if (f == std::end(state.container))
      throw std::runtime_error(fmt::format("Failed to find shyft container:{}", container_name));
    return *f->second;
  }

  its_db *internal_ptr(server_state &state, std::string_view container_name) {
    auto f = state.container.find(container_name);
    if (f == std::end(state.container)) {
      f = state.container.find(""); // need default logic here as well
      if (f == std::end(state.container))
        return nullptr;
    }

    return f->second.get();
  }

  ts_info_vector_t do_find_ts(server_state &state, std::string const &search_expression) {
    if (state.msync) {
      return state.msync->find(search_expression);
    } else {
      // 1. filter shyft://<container>/
      auto container_name = extract_shyft_url_container(search_expression);
      if (!container_name.empty()) {
        string eff_rexp = search_expression.substr(std::size(shyft_prefix) + container_name.size() + 1);
        return internal(state, container_name).find(eff_rexp);
      } else if (state.callbacks.find_ts) {
        return state.callbacks.find_ts(search_expression);
      } else {
        return {};
      }
    }
  }

  ts_info do_get_ts_info(server_state &state, std::string const &ts_name) {
    if (state.msync) {
      return state.msync->get_ts_info(ts_name);
    }
    // 1. filter shyft://<container>/
    auto pattern = extract_shyft_url_container(ts_name);
    if (!pattern.empty()) {
      return internal(state, pattern).get_ts_info(extract_shyft_url_path(ts_name, pattern));
    } else {
      return ts_info{};
    }
  }

  /** if overwrite on write, then flush the cache prior to writing */
  void do_cache_update_on_write(
    server_state &state,
    ts_vector_t tsv,
    vector<utcperiod> const &tsv_tp,
    bool overwrite_on_write) {
    auto add_range = std::views::transform(std::views::zip(std::move(tsv), tsv_tp), [](auto &&ts_tp) {
      auto rts = ts_as<aref_ts>(std::get<0>(ts_tp).ts);
      if (!rts) {
        throw std::runtime_error("do_cache_update_on_write called with non aref ts");
      }
      return std::make_tuple(rts->id, rts->rep, std::get<1>(ts_tp));
    });
    state.ts_cache.add(add_range, overwrite_on_write);
  }

  void do_store_ts(server_state &state, ts_vector_t tsv, store_policy p) {
    if (tsv.empty())
      return;

    if (state.msync) {
      state.msync->store_ts(std::move(tsv), p);
      return;
    }
    // TODO: a must.. we need to ensure are aref_ts inside tsv is unique
    //       because we are indeed moving it when adding to cache
    //       (maybe we should avoid that? the cache makes a copy anyway?)
    //       in normal use, they are, but in tests, we pass multiple
    //       refs to same underlying concrete gpoint-ts.
    //       so : loop over and make copies.
    for (auto &e : tsv) {
      auto rts = std::dynamic_pointer_cast<aref_ts>(e.sts());
      if (rts == nullptr)
        throw std::runtime_error("fatal: do_local_store invoked with not supported types");
      if (rts->rep.use_count() > 1) {                        // deduplicate refs to gpoint_ts
        rts->rep = std::make_shared<gpoint_ts>(*(rts->rep)); // copy
      }
    }
    (void) do_local_store(
      state,
      tsv.size(),
      [&](size_t i) {
        auto rts = std::dynamic_pointer_cast<aref_ts>(tsv[i].sts());
        if (rts == nullptr)
          throw std::runtime_error("fatal: do_local_store invoked with not supported types");
        auto &gts = rts->core_ts();
        return movable_ts_item_t{rts->id, gts};
      },
      p);
  }

  namespace {
    using store_ts_item_t = std::tuple<size_t, string_view, movable_ts_item_t>;
  }

  diags_t do_local_store(server_state &state, size_t n, fx_movable_ts_item_t const &fx, store_policy p) {
    if (n == 0)
      return {};
    if (state.msync) {
      throw std::runtime_error("do_local_store: does not yet support msync protocol yet");
    }
    // 1. filter out all shyft://<container>/<ts-path> elements
    //    and route these to the internal storage controller (threaded)
    //    std::map<std::string, ts_db> shyft_internal;
    //  perf goals: avoid any increfs and copy of string/ids if possible.
    std::vector<std::size_t> other;
    other.reserve(n);

    map<std::string_view, vector<store_ts_item_t>> internals; // map[container]->vector<store_ts_item_t>
    for (std::size_t i = 0; i < n; ++i) {
      auto [ts_id, ts] = fx(i); // string_view, gts_t const& so light
      auto [c, ts_name] = parse_url(ts_id);
      if (!c.empty()) {
        auto fc = internals.find(c);
        if (fc == internals.end()) {
          vector<store_ts_item_t> cx;
          cx.reserve(n);
          cx.emplace_back(i, ts_id, movable_ts_item_t{ts_name, ts});
          internals[c] = std::move(cx);
        } else {
          fc->second.emplace_back(i, ts_id, movable_ts_item_t{ts_name, ts});
        }
        if (p.recreate || !p.cache)
          state.ts_cache.remove(std::string(ts_id)); // invalidate previous defs. if any
      } else {
        other.push_back(i); // keep idx of those we have not saved, or better use store_ts_item_t??
      }
    }
    // 1.b:  multi-save internals, one invocation pr. container
    diags_t diags;
    for (auto const &e : internals) {
      fx_cache_t fx_cache;
      if (p.cache)
        fx_cache = [&](size_t i, std::optional<gts_t> ts, utcperiod tp) {
          if (ts) { // cache transformed item ts
            state.ts_cache.add(
              std::string(std::get<std::string_view>(e.second[i])), std::make_shared<gpoint_ts>(std::move(*ts)), tp);
          } else { // cache original item
            state.ts_cache.add(
              std::string(std::get<std::string_view>(e.second[i])),
              std::make_shared<gpoint_ts>(std::move(std::get<gts_t &>(std::get<movable_ts_item_t>(e.second[i])))),
              tp);
          }
        };
      auto partial_diags =
        internal(state, e.first)
          .save(
            e.second.size(),
            [&](size_t i) {
              return ts_item_t{
                std::get<std::string_view>(std::get<movable_ts_item_t>(e.second[i])),
                std::get<gts_t &>(std::get<movable_ts_item_t>(e.second[i]))}; // construct a non movable item
            },
            fx_cache,
            p);
      if (partial_diags) {
        // have to map from local ix to global ix:
        for (auto &d : *partial_diags) {           // so the d.ix now refer to the e.second[i] vector
          d.ix = std::get<size_t>(e.second[d.ix]); // replace with its global user provided index, so it makes sense
        }
        if (!diags) {
          diags = partial_diags;
        } else { // append
          (*diags).reserve((*diags).size() + (*partial_diags).size());
          std::copy(std::begin(*partial_diags), std::end(*partial_diags), std::back_inserter(*diags));
        }
      }
      if (state.sm) { // notify all subs for this container(including those that eventually failed, no direct harm)
        auto subs_view = e.second | std::views::transform([](auto const &se) {
                           return std::get<std::string_view>(se);
                         });
        state.sm->notify_change(subs_view.begin(), subs_view.end());
      }
    }

    // 2. for all non shyft:// forward those to the
    //    callbacks.store_ts, if policy is best.effort.. we do not support that pr now, first to fail here.
    if (state.callbacks.store_ts && !other.empty()) {
      ts_vector_t r; // The callback interface require classical ts_vector!
      for (auto i : other) {
        auto [ts_id, ts] = fx(i); // another pull, maybe we should ensure just one time, so first loop?
        r.push_back(apoint_ts(string(ts_id), apoint_ts(std::make_shared<gpoint_ts>(std::move(ts)))));
      }
      try {
        state.callbacks.store_ts(r);
      } catch (std::runtime_error const &re) {
        if (!p.best_effort)
          throw;
        // note: report all other failed//undefined since interface currently does not give us details
        if (!diags)
          diags.emplace();
        for (auto ix : other)
          (*diags).emplace_back(ix, ts_diagnostics::external_store_failed);
      }
      if (p.cache)
        do_cache_update_on_write(state, r, vector<utcperiod>{r.size()}, p.recreate); // externals have no total period
      else {
        for (auto &ts : r)
          state.ts_cache.remove(ts_as<aref_ts>(ts.ts)->id); // invalidate previous defs. if any
      }
      if (state.sm) {
        auto subs_view = std::views::transform(r, [](auto const &ats) {
          return ats.id();
        });
        state.sm->notify_change(std::begin(subs_view), std::end(subs_view));
      }
    }
    return diags;
  }

  void do_merge_store_ts(server_state &state, ts_vector_t tsv, bool cache_on_write) {
    if (tsv.empty())
      return;

    if (state.msync) {
      state.msync->merge_store_ts(std::move(tsv), cache_on_write);
      return;
    }

    //
    // 0. check & prepare the read time-series in tsv for the specified period of each ts
    //    (we optimize a little bit grouping on common period, and reading in batches with equal periods)
    //
    id_vector_t ts_ids;
    ts_ids.reserve(tsv.size());
    std::unordered_map<utcperiod, id_vector_t, core::utcperiod_hasher> read_map;
    std::unordered_map<std::string_view, apoint_ts, string_hash, std::equal_to<>> id_map;

    for (auto const &i : tsv) {
      auto rts = ts_as<aref_ts>(i.ts);
      if (!rts)
        throw std::runtime_error("dtss store merge: require ts with url-references");
      // sanity check
      if (id_map.find(rts->id) != end(id_map))
        throw std::runtime_error("dtss store merge requires distinct set of ids, first duplicate found:" + rts->id);
      id_map[rts->id] = apoint_ts(rts->rep);
      // then just build up map[period] = list of time-series to read
      auto rp = rts->rep->total_period();
      if (read_map.find(rp) != end(read_map)) {
        read_map[rp].push_back(rts->id);
      } else {
        read_map[rp] = id_vector_t{rts->id};
      }
    }

    //
    // 1. do the read-merge for each common period, append to final minimal write list
    //
    ts_vector_t tsv_store;
    tsv_store.reserve(tsv.size());
    for (auto &rr : read_map) {
      auto [read_ts, read_tp, read_err] = try_read(state, rr.second, rr.first, false, cache_on_write);
      if (!read_err.empty())
        throw std::runtime_error(
          fmt::format(
            "shyft-read failed to read {} time-series, first is '{}'",
            read_err.size(),
            rr.second[read_err.front().index]));
      // read_ts is in the order of the ts-id-list rr->second
      for (std::size_t i = 0; i < read_ts.size(); ++i) {
        auto ts_id = rr.second[i];
        apoint_ts r_i{std::move(read_ts[i])};
        r_i.merge_points(id_map[ts_id]);
        tsv_store.emplace_back(ts_id, r_i);
      }
    }

    //
    // 2. finally write the merged result back to whatever store is there
    //
    do_store_ts(state, std::move(tsv_store), store_policy{.recreate = false, .strict = true, .cache = cache_on_write});
  }

  namespace {
    ts_frag clip_ts_frag(ts_frag const &f, utcperiod const &p) {
      auto f_tp = f->total_period();
      if (!f_tp.overlaps(p))
        return std::make_shared<gpoint_ts const>(gta_t{}, shyft::nan, f->point_interpretation());
      auto const &ta = f->time_axis();
      size_t ix_hint = 0;
      auto ix_left = ta.open_range_index_of(p.start, ix_hint); // get left ix, less than p.start
      if (ix_left == string::npos)
        ix_left = 0; // possible scenario, then we start at i0
      auto ix_right = ta.open_range_index_of(p.end, ix_hint);
      assert(ix_right != string::npos); // pr.def. we should **always** have ix_right if it's overlap.
      auto t_right = ta.time(ix_right);
      if (f->point_interpretation() == time_series::ts_point_fx::POINT_INSTANT_VALUE) { // linear between point
        if (t_right < p.end && ix_right + 1 < ta.size()) // tricky end-definition, we need point to the right
          ++ix_right; // add end point, so we can do f(t) when t in range [p.start..p().end>
      } else if (t_right == p.end && ix_right > ix_left) {
        --ix_right; // drop off this point, since we do not need it.
      }
      return std::make_shared<gpoint_ts const>(f->slice(ix_left, 1u + ix_right - ix_left));
    }

    vector<ts_frag> clip_ts_frag_to_period(vector<ts_frag> &s, utcperiod const &p) {
      // note: mutates the vector, but not the ts_frag's, and only if needed/useful
      for (auto &f : s) {
        if (!f || f->size() == 0)
          continue;
        auto f_tp = f->total_period();
        if (p.contains(f_tp))
          continue;
        f = clip_ts_frag(f, p);
      }
      return s;
    }
  }

  std::tuple<vector<ts_frag>, vector<utcperiod>, vector<read_error>>
    try_slave_read(server_state &state, id_vector_t const &ts_ids, utcperiod p, bool use_ts_cached_read) {
    auto [res, res_tp, res_err] = try_read(state, ts_ids, p, use_ts_cached_read, false);
    return std::make_tuple(clip_ts_frag_to_period(res, p), std::move(res_tp), std::move(res_err));
  }

  tuple<vector<ts_frag>, vector<utcperiod>, vector<read_error>> try_read(
    server_state &state,
    id_vector_t const &ts_ids,
    utcperiod p,
    bool use_ts_cached_read,
    bool update_ts_cache) {
    auto result = std::make_tuple(
      vector<ts_frag>{ts_ids.size()}, vector<utcperiod>{ts_ids.size()}, vector<read_error>{});
    auto &[result_ts, result_tp, result_errors] = result;

    if (ts_ids.empty())
      return result;
    std::size_t read_from_cache{0};
    if (use_ts_cached_read) {
      state.ts_cache.get_with(ts_ids, p, [&](auto const &, std::size_t index, auto &&ts_opt) {
        if (!ts_opt) {
          return;
        }
        auto &[ts, tp] = *ts_opt;
        result_ts[index] = std::move(ts);
        result_tp[index] = tp;
        ++read_from_cache;
      });
      if (read_from_cache == ts_ids.size())
        return result;
    }

    bool const cache_read_results = update_ts_cache || state.cache_all_reads;

    if (!state.msync) { // then it is local read, that splits into shyft://something/ or ext://otherthings
      std::vector<size_t> external_idxs;
      std::ranges::for_each(std::views::enumerate(std::views::zip(ts_ids, result_ts, result_tp)), [&](auto &&pack) {
        auto &[index, id_ts_tp] = pack;
        auto &[ts_id, ts, tp] = id_ts_tp;
        if (ts) {
          return; // got it from cache
        }

        auto container_name = extract_shyft_url_container(ts_id);
        if (container_name.empty()) {
          if (!state.callbacks.bind_ts) // if no callbacks.bind_ts, its not possible to solve
            result_errors.push_back({.index = static_cast<std::size_t>(index), .code = lookup_error::unknown_schema});
          else
            external_idxs.push_back(static_cast<std::size_t>(index)); // collect them for ext read later
          return;
        }
        auto container = internal_ptr(state, container_name);
        if (container == nullptr) {
          result_errors.push_back(
            {.index = static_cast<std::size_t>(index), .code = lookup_error::container_not_found});
          return;
        }
        try {
          auto [rts, tp_read] = container->read(extract_shyft_url_path(ts_id, container_name), p);
          auto gts = std::make_shared<gpoint_ts const>(std::move(rts));
          if (cache_read_results)
            state.ts_cache.add(ts_id, gts, tp_read);
          ts = std::move(gts);
          tp = std::move(tp_read);
        } catch (...) {
          result_errors.push_back({.index = static_cast<std::size_t>(index), .code = lookup_error::container_throws});
        }
      });
      // deal with external ids, like ext://something?abc

      if (!external_idxs.empty()) { // external resolve, that is still ts_vector_t,
        // some helpers for transforming to cache-able range
        constexpr auto as_cache_update_range = std::views::transform([](auto &&el) {
          auto const &[id, ts] = el;
          auto gts = ts.ts ? std::dynamic_pointer_cast<gpoint_ts const>(ts.ts) : nullptr;
          return std::make_tuple(id, gts, utcperiod{});
        });
        constexpr auto only_gpoint_and_valid = [](auto &&its) {
          auto const &[id, ts] = its;
          return std::dynamic_pointer_cast<gpoint_ts const>(ts.ts) != nullptr;
        };
        constexpr auto ensure_evaluated = [](auto &&atsv) {
          for (auto &ats : atsv) {
            if (!dynamic_cast<gpoint_ts const *>(ats.ts.get()))
              ats = ats.evaluate(); // expression passed, so we replace with it evaluated.
          }
          return atsv;
        };
        // collect & handle external references
        vector<string> external_ts_ids;
        external_ts_ids.reserve(external_idxs.size());
        for (auto i : external_idxs)
          external_ts_ids.push_back(ts_ids[i]);
        time_series::dd::ats_vector ext_resolved;
        try {
          ext_resolved = ensure_evaluated(state.callbacks.bind_ts(external_ts_ids, p));
          // caching?
          if (cache_read_results) {
            state.ts_cache.add(
              std::views::filter(std::views::zip(external_ts_ids, ext_resolved), only_gpoint_and_valid)
              | as_cache_update_range);
          }
          // merge external results into output results
          for (std::size_t i = 0; i < ext_resolved.size(); ++i) {
            auto gts = std::dynamic_pointer_cast<gpoint_ts const>(std::move(ext_resolved[i].ts));
            result_ts[external_idxs[i]] = std::move(gts);
            result_tp[external_idxs[i]] = {}; // empty total period for ext cb, not supported yet
          }
        } catch (std::runtime_error const &) {
          for (auto eix : external_idxs) {
            result_errors.push_back({.index = static_cast<std::size_t>(eix), .code = lookup_error::container_throws});
          }
        }
      }
      return result;
    }
    // else its msync, so fetch data from main dtss
    std::vector<std::size_t> master_indices(ts_ids.size() - read_from_cache);
    std::ranges::copy(
      std::views::elements<0>(std::views::filter(
        std::views::enumerate(result_ts),
        [](auto &&pack) {
          return !std::get<1>(pack);
        })),
      master_indices.data());

    std::vector<std::string> master_ts_ids{ts_ids.size() - read_from_cache};
    std::ranges::copy(
      std::views::transform(
        master_indices,
        [&](auto index) {
          return ts_ids[index];
        }),
      master_ts_ids.data());
    vector<ts_frag> master_ts;
    vector<utcperiod> master_tp;
    std::tie(master_ts, master_tp, result_errors) = state.msync->try_read(master_ts_ids, p, true);
    std::ranges::for_each(result_errors, [&](auto &error) {
      error.index = master_indices[error.index];
    });

    if (result_errors.empty()) {
      if (cache_read_results)
        state.ts_cache.add(std::views::zip(master_ts_ids, master_ts, master_tp));
      for (auto &&[ts, index] : std::views::zip(master_ts, master_indices))
        result_ts[index] = std::move(ts);
      return result;
    }

    if (cache_read_results) // the ts reads from master might be cached(probably smart)
      state.ts_cache.add(std::views::filter(std::views::zip(master_ts_ids, master_ts, master_tp), [](auto &&pack) {
        return static_cast<bool>(std::get<1>(pack));
      }));
    {
      auto resolved_master_result = std::views::transform(master_indices, [&](auto master_index) -> auto & {
        return result_ts[master_index];
      });
      std::ranges::move(master_ts, std::ranges::begin(resolved_master_result));
    }

    return result;
  }

  void do_bind_ts(
    server_state &state,
    utcperiod bind_period,
    ts_vector_t &atsv,
    bool use_ts_cached_read,
    bool update_ts_cache) {
    using shyft::time_series::dd::resolve_symbols;
    // explanation compressed: this will resolve pure symbolic refs (e.g. unbound terminal of type aref_ts)
    // by calling our resolver (the lambda below), that will eventually call to do_read,
    // that first will attempt doing cache lookup,
    //  and furthermore, for those not resolved by cache lookup above,
    //  then do msync read(if master/slave config),
    //  or do internal read of shyft://, or callback for non shyft:// refs
    resolve_symbols(atsv, [&](vector<string> const &tsids) {
      auto [gtsv, r_tp, r_err] = try_read(state, tsids, bind_period, use_ts_cached_read, update_ts_cache);
      if (!r_err.empty())
        throw std::runtime_error(
          fmt::format(
            "shyft-read failed to read {} time-series, first is '{}'", r_err.size(), tsids[r_err.front().index]));
      ts_vector_t r;
      r.reserve(gtsv.size());
      for (auto &&gts : gtsv)
        r.emplace_back(std::move(gts));
      return r;
    });
  }

  ts_vector_t do_evaluate_ts_vector(
    server_state &state,
    utcperiod bind_period,
    ts_vector_t &atsv,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period) {
    do_bind_ts(state, bind_period, atsv, use_ts_cached_read, update_ts_cache);
    if (clip_period.valid())
      return clip_to_period(ts_vector_t{shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv)}, clip_period);
    else
      return ts_vector_t{shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv)};
  }

  ts_vector_t do_evaluate_percentiles(
    server_state &state,
    utcperiod bind_period,
    ts_vector_t &atsv,
    gta_t const &ta,
    std::vector<int64_t> const &percentile_spec,
    bool use_ts_cached_read,
    bool update_ts_cache) {
    do_bind_ts(state, bind_period, atsv, use_ts_cached_read, update_ts_cache);
    std::vector<int64_t> p_spec;
    for (auto const p : percentile_spec)
      p_spec.push_back(int(p));           // convert
    return percentiles(atsv, ta, p_spec); // we can assume the result is trivial to serialize
  }

  void do_remove_ts(server_state &server, std::string const &ts_url) {
    if (!server.can_remove) {
      throw std::runtime_error("dtss::server: server does not support removing");
    }

    if (server.msync) {
      server.msync->remove(ts_url);
      return;
    }

    // 1. filter shyft://<container>/
    auto pattern = extract_shyft_url_container(ts_url);
    if (!pattern.empty()) {
      auto shyft_ts_url = extract_shyft_url_path(ts_url, pattern);
      internal(server, pattern).remove(shyft_ts_url);
      server.ts_cache.remove(ts_url); // remove it from cache as well!
    } else {
      throw std::runtime_error("dtss::server: server does not allow removing for non shyft-url type data");
    }
  }

  id_vector_t do_get_container_names(server_state &state) {
    id_vector_t container_names;
    container_names.reserve(state.container.size());
    for (auto const &it : state.container) {
      container_names.emplace_back(it.first);
    }
    return container_names;
  }

  void do_set_container(
    server_state &state,
    std::string const &name,
    std::string const &path,
    std::string type,
    db_cfg cfg) {
    if (state.msync) {
      state.msync->set_container(name, path, type, cfg);
      return;
    }
    if (path.empty()) {
      throw std::runtime_error(fmt::format("Empty path not supported"));
    }
    if (type != "ts_db" && type != "ts_rdb") {
      throw std::runtime_error(fmt::format(R"(Type {} not supported, supported types are ["ts_db", "ts_rdb"])", type));
    }
    // find root container:
    auto pc = container_find(state, "");

    if (pc == state.container.end()) {
      throw std::runtime_error(fmt::format("Root container does not exist"));
    }
    auto exists_with_same_name = container_find(state, name);
    if (exists_with_same_name != pc) {
      throw std::runtime_error(fmt::format("Attempting to add a container name that exists via client."));
    }

    fs::path root_dir{pc->second->root_dir()};
    fs::path new_path = root_dir;
    // NOTE: assign should not be nessecary, but lexically_normal does not get applied if run through python
    // because of some reason
    new_path = new_path.append(path).lexically_normal();

    if (fs::exists(new_path)) {
      throw std::runtime_error(fmt::format("There already exists a container at path {}", new_path.string()));
    }
    if (!std::equal(root_dir.begin(), root_dir.end(), new_path.begin())) {
      throw std::runtime_error(
        fmt::format(
          "Absolute container path {} has to be a subpath of the root path {}", new_path.string(), root_dir.string()));
    }

    add_container(state, name, new_path.string(), type, cfg);
  }

  void do_remove_container(server_state &state, std::string const &container_url, bool remove_from_disk) {
    if (state.msync) {
      state.msync->remove_container(container_url, remove_from_disk);
      return;
    }
    if (container_url.empty()) {
      throw std::runtime_error("Removing an emtpy url is not supported");
    }
    remove_container(state, container_url, remove_from_disk);
  }

  void do_swap_container(server_state &state, std::string const &a, std::string const &b) {
    if (state.msync) {
      state.msync->swap_container(a, b);
      return;
    }
    swap_container(state, a, b);
  }
}
