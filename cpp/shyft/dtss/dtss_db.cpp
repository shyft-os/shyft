
#include <algorithm>
#include <cstring>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <regex>
#include <thread>
#include <utility>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/regex.hpp>

#include <shyft/core/fs_compat.h>
#include <shyft/dtss/detail/make_aligned_ts.h>
#include <shyft/dtss/dtss_db.h>
#include <shyft/dtss/dtss_db_time_io.h>
#include <shyft/dtss/dtss_mutex.h>

namespace shyft::dtss {
  using std::move;
  using std::vector;
  using std::map;
  using std::shared_ptr;
  using std::make_shared;
  using std::unique_ptr;
  using std::make_unique;
  using std::runtime_error;
  using std::string;
  using std::size_t;
  using std::fopen;
  using std::fclose;
  using std::fwrite;
  using std::fread;
  using std::FILE;
#ifdef _WIN32
// windows have 32bit ints on standard, but
// 64bits versions exists.
#define ftell _ftelli64
#define fseek _fseeki64
#else
  using std::fseek;
#endif

  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using shyft::core::utctimespan;
  using shyft::core::no_utctime;
  using shyft::core::max_utctime;
  using shyft::core::min_utctime;
  using shyft::core::calendar;
  using shyft::core::deltahours;
  using shyft::core::to_seconds64;
  using shyft::core::seconds;
  using shyft::time_series::ts_point_fx;
  using gta_t = shyft::time_axis::generic_dt;
  using ta_generic_type = shyft::time_axis::generic_dt::generic_type;
  using gts_t = shyft::time_series::point_ts<gta_t>;
  using namespace std::chrono_literals;

  /**
   * @brief ts_file_header
   * @details
   * Contains enough to verify/check time-axis compatibility
   */
  struct ts_file_header {
    utctime t0{no_utctime};
    utctime dt{0l};
    std::string tz;
    time_series::ts_point_fx point_fx{time_series::POINT_AVERAGE_VALUE};       ///< point_fx feature
    time_axis::generic_dt::generic_type ta_type{time_axis::generic_dt::FIXED}; ///< time-axis type
  };

  /** native-time io, using direct write/read utctime, that is micro seconds,
   * zero overhead.
   */


  void native_time_io::write(FILE *fh, utctime const &t) {
    if (fwrite(&t, sizeof(utctime), 1, fh) != 1)
      throw runtime_error("dtss_store: failed to write time-point to disk");
  }

  void native_time_io::write(FILE *fh, vector<utctime> const &t) {
    if (fwrite(t.data(), sizeof(utctime), t.size(), fh) != t.size())
      throw runtime_error("dtss_store: failed to write time-points to disk");
  }

  void native_time_io::read(FILE *fh, utctime &t) {
    if (fread(&t, sizeof(utctime), 1, fh) != 1)
      throw runtime_error("dtss_store: failed to read time-point from from disk");
  }

  void native_time_io::read(FILE *fh, vector<utctime> &t) {
    if (fread(t.data(), sizeof(utctime), t.size(), fh) != t.size())
      throw runtime_error("dtss_store: failed to read time-points  from disk");
  }

  /** seconds-based -time io, needs conversion/to/from us/seconds
   * computational  overhead.
   */
  void seconds_time_io::write(FILE *fh, utctime const &t_us) {
    int64_t t = to_seconds64(t_us);
    if (fwrite(&t, sizeof(int64_t), 1, fh) != 1)
      throw runtime_error("dtss_store: failed to write time-point to disk");
  }

  void seconds_time_io::write(FILE *fh, vector<utctime> const &t_us) {
    vector<int64_t> t;
    t.reserve(t_us.size()); // have to alloc and convert
    for (auto const &tx : t_us)
      t.emplace_back(to_seconds64(tx));
    if (fwrite(t.data(), sizeof(int64_t), t.size(), fh) != t.size())
      throw runtime_error("dtss_store: failed to write time-points to disk");
  }

  void seconds_time_io::read(FILE *fh, utctime &t_us) {
    int64_t t;
    if (fread(&t, sizeof(int64_t), 1, fh) != 1)
      throw runtime_error("dtss_store: failed to read time-point from from disk");
    t_us = seconds(t);
  }

  void seconds_time_io::read(FILE *fh, vector<utctime> &t_us) {
    vector<int64_t> t;
    t.resize(t_us.size());
    if (fread(t.data(), sizeof(int64_t), t.size(), fh) != t.size())
      throw runtime_error("dtss_store: failed to read time-points from disk");
    for (size_t i = 0; i < t.size(); ++i)
      t_us[i] = seconds(t[i]);
  }

  //------------------ ts_db_impl --------
  struct ts_db_impl {
    using fclose_fx_t = int (*)(FILE *); // need to avoid using decltype on attributed std::fclose,
    using scoped_file = unique_ptr<FILE, fclose_fx_t>;
    map<string, shared_ptr<calendar>> calendars; ///< fast lookup of all calendars
    string root_dir;                             ///< root_dir points to the top of the container
    file_lock_manager f_mx;                      ///< keeps thread-scope locks for activity on files
    bool time_format_micro_seconds =
      true; ///< for testing only, force save in old seconds format, to ensure we can read it.

    ts_db_impl() = delete;
    ts_db_impl(ts_db_impl const &) = delete;
    ts_db_impl(ts_db_impl &&) = delete;
    ts_db_impl &operator=(ts_db_impl const &) = delete;
    ts_db_impl &operator=(ts_db_impl &&) = delete;

    explicit ts_db_impl(string const &root_dir)
      : root_dir{root_dir} {
      if (!fs::is_directory(root_dir)) {
        if (!fs::exists(root_dir)) {
          if (!fs::create_directories(root_dir)) {
            throw runtime_error(string("ts_db: failed to create root directory :") + root_dir);
          }
        } else {
          throw runtime_error(string("ts_db: designated root directory is not a directory:") + root_dir);
        }
      }
      make_calendar_lookups();
    }

    // annotate path to ease solving problems with storage system
    static string add_file_path(string src, string const &fp) {
      static string const from{"'?'"}; // pattern, if any, that should be the file
      size_t start_pos = src.find(from);
      if (start_pos == std::string::npos)
        src += ", filepath=" + fp;
      else
        src.replace(start_pos, from.length(), fp);
      return src;
    }

    template <class CFX> // CFX callable (optional<gts_t>..)
    void save(string_view fn, gts_t const &ts, bool overwrite, bool strict_alignment, CFX &&fx_cache) {
      if (boost::algorithm::ends_with(fn, its_db::reserved_extension))
        throw runtime_error(fmt::format("shyft save time-series to illegal extension attempted:{}", fn));
      string ffp = make_full_path(fn, true);
      try {
        writer_file_lock lck(f_mx, ffp);
        scoped_file fh(nullptr, &fclose); // zero-initializes deleter
        ts_db_header old_header;
        bool do_merge = false;
        auto const _write_new_ts = [&](gts_t const &ts) {
          if (time_format_micro_seconds)
            write_ts<native_time_io>(fh.get(), ts);
          else
            write_ts<seconds_time_io>(fh.get(), ts);
        };

        std::optional<gts_t> cts; // in case we need to transform before (over)write.

        if (!overwrite && save_path_exists(fn)) {
          fh.reset(fopen(ffp.c_str(), "r+b"));
          old_header = read_header(fh.get());
          if (!old_header.is_valid())
            throw runtime_error("Problem reading ts_db header,'?', remove and recreate ts to resolve the issue.");
          if (old_header.data_period.empty() || ts.total_period().contains(old_header.data_period)) {
            // old data is completely contained in the new => start the file anew
            //  - reopen, as there is no simple way to truncate an open file...
            // fseek(fh.get(), 0, SEEK_SET);
            // ensure we keep resolution/type etc...

            size_t ignored{};
            auto old_ta = old_header.is_seconds()
                          ? read_time_axis<seconds_time_io>(fh.get(), old_header, old_header.data_period, ignored)
                          : read_time_axis<native_time_io>(fh.get(), old_header, old_header.data_period, ignored);
            cts = check_ta_alignment(strict_alignment, old_header, old_ta, ts); // catch errors here.
            fh.reset(fopen(ffp.c_str(), "wb")); // ok semantically, then clear the file and do the write.
            // TODO: on non-strict aligned, is there a case where cts could not be covering the totals?
            _write_new_ts(cts ? *cts : ts);
            auto tp = cts ? (*cts).total_period() : ts.total_period(); // ensure aligned period
            fx_cache(std::move(cts), tp);
            return; // we are done
          } else {
            do_merge = true;
          }
        } else { // overwrite(aka recreate ),or missing ts, so we are on scratch
          fh.reset(fopen(ffp.c_str(), "wb"));
        }
        utcperiod total_period{ts.total_period()};
        if (!do_merge) { // a straight fwd. write new ts.
          _write_new_ts(ts);
        } else { // existing ts, not covering all points, so a merge.
          size_t ignored{};
          auto old_ta = old_header.is_seconds()
                        ? read_time_axis<seconds_time_io>(fh.get(), old_header, old_header.data_period, ignored)
                        : read_time_axis<native_time_io>(fh.get(), old_header, old_header.data_period, ignored);
          total_period = old_header.data_period;
          cts = check_ta_alignment(strict_alignment, old_header, old_ta, ts);
          if (!cts || (cts && (*cts).size())) { // only write for non-empty ts or cts, skip noop.
            if (old_header.is_seconds())
              total_period = merge_ts<seconds_time_io>(fh.get(), old_header, old_ta, cts ? *cts : ts);
            else
              total_period = merge_ts<native_time_io>(fh.get(), old_header, old_ta, cts ? *cts : ts);
          }
        }
        fx_cache(std::move(cts), total_period);
      } catch (runtime_error const &re) {
        throw runtime_error(add_file_path(re.what(), ffp));
      }
    }

    tuple<gts_t, utcperiod> read(string_view fn, utcperiod p) {
      if (boost::algorithm::ends_with(fn, its_db::reserved_extension))
        throw runtime_error(fmt::format("shyft read time-series from illegal extension attempted:{}", fn));
      string ffp = make_full_path(fn);
      try {
        reader_file_lock lck(f_mx, ffp);
        scoped_file fh{fopen(ffp.c_str(), "rb"), &fclose};
        if (!fh.get()) {
          throw runtime_error(string("shyft-read time-series internal: Could not open file ") + ffp);
        }
        return read_ts(fh.get(), p);
      } catch (runtime_error const &re) {
        throw runtime_error(add_file_path(re.what(), ffp));
      }
    }

    void remove(string_view fn) {
      if (boost::algorithm::ends_with(fn, its_db::reserved_extension))
        return; // ignore silent
      auto fp = make_full_path(fn);
      writer_file_lock lck(f_mx, fp);

      for (size_t retry = 0; retry < 10; ++retry) {
        try {
          fs::remove(fp);
          return;
        } catch (...) { // windows usually fails, due to delayed file-close/file-release, so we retry 10 x 0.3 seconds
          std::this_thread::sleep_for(300ms);
        }
      }
      throw runtime_error("failed to remove file '" + fp + "' after 10 repeated attempts lasting for 3 seconds");
    }

    ts_info get_ts_info(string_view fn) {
      if (boost::algorithm::ends_with(fn, its_db::reserved_extension))
        throw runtime_error(fmt::format("shyft get info  time-series from illegal extension attempted:{}", fn));
      auto ffp = make_full_path(fn);
      reader_file_lock lck(f_mx, ffp);

      if (save_path_exists(fn)) {
        scoped_file fh{fopen(ffp.c_str(), "rb"), &fclose};
        auto h = read_header(fh.get());
        if (!h.is_valid())
          throw runtime_error("Invalid ts db header,'?', (re)move and recreate the ts");
        ts_info i;
        i.name = fn;
        i.point_fx = h.point_fx;
        i.modified = fs_to_utctime(fs::last_write_time(ffp));
        i.data_period = h.data_period;
        utctime t0, dt;
        switch (h.ta_type) {
        case gta_t::CALENDAR:
        case gta_t::FIXED: {

          if (h.is_seconds()) {
            seconds_time_io::read(fh.get(), t0);
            seconds_time_io::read(fh.get(), dt);
          } else {
            native_time_io::read(fh.get(), t0);
            native_time_io::read(fh.get(), dt);
          }
          i.delta_t = dt;
          if (h.ta_type == gta_t::CALENDAR) {
            // read tz_info
            uint32_t sz{0};
            read(fh.get(), static_cast<void *>(&sz), sizeof(uint32_t));
            string tz(sz, '\0');
            {
              unique_ptr<char[]> tmp_ptr = make_unique<char[]>(sz);
              read(fh.get(), static_cast<void *>(tmp_ptr.get()), sz);
              tz.replace(0, sz, tmp_ptr.get(), sz);
            }
            i.olson_tz_id = tz;
          }
        } break;
        case gta_t::POINT: {
          //-- we do not extract any more info here
        } break;
        }
        // consider time-axis type info, dt as well
        return i;
      } else {
        throw runtime_error(fmt::format("ts_db: no ts named: {}", fn));
      }
    }

    vector<ts_info> find(string_view match_) {
      fs::path root(root_dir);
      vector<ts_info> r;
      string match(match_);
      boost::regex r_match(match, boost::regex_constants::ECMAScript | boost::regex_constants::icase);
      fs::recursive_directory_iterator dir(root), end;
      for (; dir != end; ++dir) {
        if (
          fs::is_regular_file(dir->path())
          && dir->path().extension() != its_db::reserved_extension) { // do not find anything with reserved extension
          string fn = dir->path().lexically_relative(root).generic_string(); // dir.path() except root-part
          if (boost::regex_match(fn, r_match)) {
            r.push_back(get_ts_info(fn)); // TODO: maybe multi-core this into a job-queue
          }
        } else if (fs::is_directory(dir->path())) {
          string dn = dir->path().lexically_relative(root).generic_string();
          if (0 == boost::regex_match(dn + "/", r_match, boost::match_default | boost::match_partial))
            dir.disable_recursion_pending(); // no partial match on this path, avoid further recursion
        }
      }
      return r;
    }

    shared_ptr<calendar> lookup_calendar(string const &tz) const {
      auto it = calendars.find(tz);
      if (it == calendars.end())
        return make_shared<calendar>(tz);
      return it->second;
    }

    void make_calendar_lookups() {
      // todo: make detail/calendars.h common defs, or  make cal construct of known olson tz id faster
      for (int hour = -11; hour < 12; hour++) {
        auto c = make_shared<calendar>(deltahours(hour));
        calendars[c->tz_info->name()] = c;
      }
      for (auto const &tz_id : calendar::region_id_list()) { // ensure we have the std list available
        calendars[tz_id] = make_shared<calendar>(tz_id);
      }
    }

    bool save_path_exists(string_view fn) const {
      fs::path fn_path{string{fn}}, root_path{root_dir};
      if (fn_path.is_relative()) {
        fn_path = root_path / fn_path;
      } else {
        // questionable: should we allow outside container specs?
        // return false;
      }
      return fs::is_regular_file(fn_path);
    }

    string make_full_path(string_view fn, bool create_paths = false) const {
      fs::path fn_path{string{fn}}, root_path{root_dir};
      // determine path type
      if (fn_path.is_relative()) {
        fn_path = root_path / fn_path;
      } else { // fn_path.is_absolute()
               // questionable: should we allow outside container specs?
               //  - if determined to be fully allowed: remove this branch or throw
      }
      // not a directory and create missing path
      if (fs::is_directory(fn_path)) {
        throw runtime_error(fn_path.string() + " is a directory. Should be a file.");
      } else if (!fs::exists(fn_path) && create_paths) {
        fs::path rp = fn_path.parent_path();
        if (rp.compare(root_path) > 0) { // if fn contains subdirectory, we have to check that it exits
          if (!fs::is_directory(rp)) {
            fs::create_directories(rp);
          }
        }
      }
      // -----
      return fn_path.string();
    }

    template <class T>
    ts_db_header mk_header(ts_point_fx pfx, ta_generic_type gt, size_t n, utcperiod p) const {
      if (T::version() == '1') {
        p.start /= utctime::period::den;
        p.end /= utctime::period::den;
      }
      return ts_db_header{pfx, gt, uint32_t(n), p, T::version()};
    }

    template <class T>
    ts_db_header mk_header(gts_t const &ts) const {
      return mk_header<T>(ts.point_interpretation(), ts.time_axis().gt(), ts.size(), ts.total_period());
    }

    // ----------
    static inline size_t compute_n_points(utcperiod const &p, utctimespan dt) noexcept {
      return p.valid() ? p.timespan() / dt : 0u;
    }

    static inline size_t compute_n_points(calendar const &cal, utcperiod p, utctimespan dt) noexcept {
      return p.valid() ? cal.diff_units(p.start, p.end, dt) : 0u;
    }

    static inline void write(FILE *fh, void const *d, size_t sz) {
      if (fwrite(d, sizeof(char), sz, fh) != sz)
        throw runtime_error("dtss_store: failed to write do disk");
    }

    template <class T>
    void write_header(FILE *fh, gts_t const &ats) const {
      ts_db_header h = mk_header<T>(ats);
      write(fh, static_cast<void const *>(&h), sizeof(h));
    }

    template <class T>
    void write_time_axis(FILE *fh, gta_t const &ta) const {
      switch (ta.gt()) {
      case time_axis::generic_dt::FIXED: {
        T::write(fh, ta.f().t);
        T::write(fh, ta.f().dt);
      } break;
      case time_axis::generic_dt::CALENDAR: {
        T::write(fh, ta.c().t);
        T::write(fh, ta.c().dt);
        string tz = ta.c().cal->tz_info->name();
        auto sz = static_cast<uint32_t>(tz.size()); //
        write(fh, static_cast<void const *>(&sz), sizeof(uint32_t));
        write(fh, static_cast<void const *>(tz.c_str()), sz);
      } break;
      case time_axis::generic_dt::POINT: {
        T::write(fh, ta.p().t_end);
        T::write(fh, ta.p().t);
      } break;
      }
    }

    static inline void write_values(FILE *fh, vector<double> const &v) {
      write(fh, static_cast<void const *>(v.data()), sizeof(double) * v.size());
    }

    template <class T>
    void write_ts(FILE *fh, gts_t const &ats) const {
      if (ats.size() > std::numeric_limits<uint32_t>::max() && !ats.time_axis().is_fixed_dt()) {
        throw std::runtime_error(
          "dtss_store: point-dt time-series is current limited to max size "
          + std::to_string(std::numeric_limits<uint32_t>::max()));
      }
      write_header<T>(fh, ats);
      write_time_axis<T>(fh, ats.ta);
      write_values(fh, ats.v);
    }

    // ----------

    template <class T>
    utcperiod
      do_merge(FILE *fh, ts_db_header const &old_header, time_axis::generic_dt const &old_ta, gts_t const &new_ts)
        const {

      // assume the two time-axes have the same type and are aligned

      auto const old_p = old_header.data_period, new_p = new_ts.ta.total_period();
      utcperiod total_period{new_p};
      if (old_p.valid() && !old_p.empty()) {
        total_period.start = std::min(old_p.start, new_p.start);
        total_period.end = std::max(old_p.end, new_p.end);
      }

      switch (old_header.ta_type) {
      case time_axis::generic_dt::FIXED: {
        utctime t0, tn;        // first/last time point in the merged data
        size_t keep_old_v = 0, // number of old values to keep
          nan_v_before = 0,    // number of NaN values to insert between new and old
          nan_v_after = 0;     // number of NaN values to insert between old and new
        vector<double> old_tail(0, 0.);
        auto old_n = compute_n_points(old_p, new_ts.ta.f().dt); // compute n_points from period, do not trust 32bit int
        // determine first time-axis
        if (old_p.start <= new_p.start) { // old start before new
          t0 = old_p.start;

          // is there a gap between?
          if (old_p.end < new_p.start) {
            nan_v_after = (new_p.start - old_p.end) / new_ts.ta.f().dt;
          }

          // determine number of old values to keep
          keep_old_v = (new_p.start - old_p.start) / new_ts.ta.f().dt - nan_v_after;
        } else { // new start before old
          t0 = new_p.start;

          // is there a gap between?
          if (new_p.end < old_p.start) {
            nan_v_before = (old_p.start - new_p.end) / new_ts.ta.f().dt;
          }

          // is there is a tail of old values
          if (new_p.end < old_p.end) {
            size_t count = (old_p.end - new_p.end) / new_ts.ta.f().dt - nan_v_before;
            old_tail.resize(count);
            fseek(
              fh,
              sizeof(ts_db_header)                  // header
                + 2 * sizeof(int64_t)               // + fixed_dt time-axis
                + (old_n - count) * sizeof(double), // + values except count last
              SEEK_SET);
            read(fh, static_cast<void *>(old_tail.data()), count * sizeof(double));
          }
        }

        // determine last time-axis
        if (old_p.end <= new_p.end) { // old end before new
          tn = new_p.end;
        } else {
          tn = old_p.end;
        }

        // write header
        auto new_header = mk_header<T>(
          new_ts.fx_policy,
          old_header.ta_type,
          static_cast<uint32_t>(
            (tn - t0) / new_ts.ta.f().dt), // we do n0t use it, due to 32bit limitation, but keep it for now
          utcperiod{t0, tn});
        // -----
        fseek(fh, 0, SEEK_SET); // seek to beginning
        write(fh, static_cast<void const *>(&new_header), sizeof(ts_db_header));

        // write time-axis
        T::write(fh, t0);

        // write values
        //  - seek past old values to keep
        fseek(fh, sizeof(int64_t) + keep_old_v * sizeof(double), SEEK_CUR);
        //  - if gap after new -> write NaN
        if (nan_v_after > 0) {
          vector<double> tmp(nan_v_after, shyft::nan);
          write(fh, static_cast<void const *>(tmp.data()), nan_v_after * sizeof(double));
        }
        //  - write new values
        write(fh, static_cast<void const *>(new_ts.v.data()), new_ts.v.size() * sizeof(double));
        //  - if gap before old -> write NaN
        if (nan_v_before > 0) {
          vector<double> tmp(nan_v_before, shyft::nan);
          write(fh, static_cast<void const *>(tmp.data()), nan_v_before * sizeof(double));
        }
        //  - if old tail values -> write them back
        if (old_tail.size() > 0) {
          write(fh, static_cast<void const *>(old_tail.data()), old_tail.size() * sizeof(double));
        }
      } break;
      case time_axis::generic_dt::CALENDAR: {
        utctime t0, tn;        // first/last time point in the merged data
        size_t keep_old_v = 0, // number of old values to keep
          nan_v_before = 0,    // number of NaN values to insert between new and old
          nan_v_after = 0;     // number of NaN values to insert between old and new
        vector<double> old_tail(0, 0.);
        // determine first time-axis
        auto old_n = compute_n_points(
          *new_ts.ta.c().cal, old_p, new_ts.ta.c().dt); // compute n_points from period, do not trust 32bit int

        if (old_p.start <= new_p.start) { // old start before new
          t0 = old_p.start;

          // is there a gap between?
          if (old_p.end < new_p.start) {
            nan_v_after = new_ts.ta.c().cal->diff_units(old_p.end, new_p.start, new_ts.ta.c().dt);
          }

          // determine number of old values to keep
          keep_old_v = new_ts.ta.c().cal->diff_units(old_p.start, new_p.start, new_ts.ta.c().dt) - nan_v_after;
        } else { // new start before old
          t0 = new_p.start;

          // is there a gap between?
          if (new_p.end < old_p.start) {
            nan_v_before = new_ts.ta.c().cal->diff_units(new_p.end, old_p.start, new_ts.ta.c().dt);
          }

          // is there is a tail of old values
          if (new_p.end < old_p.end) {
            size_t count = new_ts.ta.c().cal->diff_units(new_p.end, old_p.end, new_ts.ta.c().dt) - nan_v_before;
            old_tail.resize(count);
            fseek(
              fh,
              sizeof(ts_db_header)     // header
                + 2 * sizeof(int64_t), // + first part of calendar_dt time-axis
              SEEK_SET);
            uint32_t tz_sz{};
            read(fh, static_cast<void *>(&tz_sz), sizeof(tz_sz)); // read size of tz_name
            fseek(
              fh,
              tz_sz * sizeof(uint8_t)               // + second part of calendar_dt time-axis
                + (old_n - count) * sizeof(double), // values to overwrite
              SEEK_CUR);
            read(fh, static_cast<void *>(old_tail.data()), count * sizeof(double));
          }
        }

        // determine last time-axis
        if (old_p.end <= new_p.end) { // old end before new
          tn = new_p.end;
        } else {
          tn = old_p.end;
        }

        // write header
        auto new_header = mk_header<T>(
          new_ts.fx_policy,
          old_header.ta_type,
          static_cast<uint32_t>(new_ts.ta.c().cal->diff_units(t0, tn, new_ts.ta.c().dt)),
          utcperiod{t0, tn});
        // -----
        fseek(fh, 0, SEEK_SET); // seek to beginning
        write(fh, static_cast<void const *>(&new_header), sizeof(ts_db_header));

        // update time-axis
        T::write(fh, t0);
        fseek(fh, sizeof(int64_t), SEEK_CUR);
        {
          uint32_t tz_sz{};
          read(fh, static_cast<void *>(&tz_sz), sizeof(tz_sz)); // read size of calendar str
          fseek(fh, tz_sz * sizeof(uint8_t), SEEK_CUR);         // seek past tz_name
        }

        // write values
        //  - seek past old values to keep
        fseek(fh, keep_old_v * sizeof(double), SEEK_CUR);
        //  - if gap after new -> write NaN
        if (nan_v_after > 0) {
          vector<double> tmp(nan_v_after, shyft::nan);
          write(fh, static_cast<void const *>(tmp.data()), nan_v_after * sizeof(double));
        }
        //  - write new values
        write(fh, static_cast<void const *>(new_ts.v.data()), new_ts.v.size() * sizeof(double));
        //  - if gap before old -> write NaN
        if (nan_v_before > 0) {
          vector<double> tmp(nan_v_before, shyft::nan);
          write(fh, static_cast<void const *>(tmp.data()), nan_v_before * sizeof(double));
        }
        //  - if old tail values -> write them back
        if (old_tail.size() > 0) {
          write(fh, static_cast<void const *>(old_tail.data()), old_tail.size() * sizeof(double));
        }
      } break;
      case time_axis::generic_dt::POINT: {
        vector<utctime> merged_t;
        merged_t.reserve(old_ta.size() + new_ts.size() + 1); // guaranteed large enough
                                                             // -----
        vector<double> merged_v(0u, 0.);
        merged_v.reserve(old_ta.size() + new_ts.size() + 1); // guaranteed large enough
                                                             // -----
        size_t const old_v_offset = sizeof(ts_db_header) + (old_header.n + 1) * sizeof(int64_t);

        // new start AFTER  =>  start at old
        if (!old_p.empty() && old_p.start < new_p.start) {

          // get iterator into old time-axis at first where p.start >= old.start
          //  - [old_ta.begin(), old_end) is the part of old we want to keep
          auto old_end = std::lower_bound(old_ta.p().t.cbegin(), old_ta.p().t.cend(), new_ts.total_period().start);

          // store portion of old time-points to keep
          merged_t.insert(merged_t.end(), old_ta.p().t.cbegin(), old_end);
          // store portion of old values to keep
          size_t const to_insert = std::distance(old_ta.p().t.cbegin(), old_end);
          auto it = merged_v.insert(merged_v.end(), to_insert, 0.);
          fseek(fh, old_v_offset, SEEK_SET);
          read(fh, static_cast<void *>(&(*it)), to_insert * sizeof(double));

          // if NEW start truly after OLD include the end point
          if (old_end == old_ta.p().t.cend() && new_p.start > old_ta.p().t_end) {
            merged_t.emplace_back(old_ta.p().t_end); // include the end point
            merged_v.emplace_back(shyft::nan);       // nan for the gap
          }
        }

        // read new into merge_ts
        if (new_ts.ta.gt() == time_axis::generic_dt::POINT) {
          merged_t.insert(merged_t.end(), new_ts.ta.p().t.cbegin(), new_ts.ta.p().t.cend());
        } else { // deal with merge fixed/cal into point, it should work!!
          for (size_t i = 0; i < new_ts.ta.size(); ++i)
            merged_t.emplace_back(new_ts.ta.time(i));
        }
        merged_t.emplace_back(new_ts.ta.total_period().end);
        merged_v.insert(merged_v.end(), new_ts.v.cbegin(), new_ts.v.cend());
        // if new end BEFORE start of old  =>  insert NaN
        if (!old_p.empty() && new_p.end < old_p.start) {
          merged_v.emplace_back(shyft::nan);
        }

        // new end BEFORE end of old  =>  read more from old
        if (!old_p.empty() && new_p.end < old_p.end) {

          // determine first period in old NOT CONTAINING new.end
          auto old_begin = std::upper_bound(old_ta.p().t.cbegin(), old_ta.p().t.cend(), new_ts.ta.total_period().end);

          // store any trailing old time-points
          merged_t.insert(merged_t.end(), old_begin, old_ta.p().t.cend());
          merged_t.emplace_back(old_ta.p().t_end);
          // store portion of old values to keep
          size_t to_insert = std::distance(old_begin, old_ta.p().t.cend());
          // new end INSIDE of AT start of old  =>  insert value from old where new end
          if (new_p.end >= old_p.start) {
            to_insert += 1;
          }
          auto it = merged_v.insert(merged_v.end(), to_insert, 0.);
          fseek(fh, old_v_offset + (old_header.n - to_insert) * sizeof(double), SEEK_SET);
          read(fh, static_cast<void *>(&(*it)), to_insert * sizeof(double));
        }
        size_t const max_points_limit = std::numeric_limits<int32_t>::max();
        if (merged_t.size() > max_points_limit) {
          throw runtime_error(
            "shyft::db_ts : max_points_limit " + std::to_string(max_points_limit)
            + " exceeded, n_points=" + std::to_string(merged_t.size()));
        }
        // write header
        auto new_header = mk_header<T>(
          new_ts.fx_policy,
          old_header.ta_type,
          static_cast<uint32_t>(merged_t.size() - 1),
          utcperiod{merged_t.at(0), merged_t.at(merged_t.size() - 1)});
        // -----
        fseek(fh, 0, SEEK_SET); // seek to beginning
        write(fh, static_cast<void const *>(&new_header), sizeof(ts_db_header));

        // write time-axis
        T::write(fh, merged_t.at(merged_t.size() - 1));
        merged_t.pop_back();
        T::write(fh, merged_t);
        // write values
        write(fh, static_cast<void const *>(merged_v.data()), merged_v.size() * sizeof(double));
      } break;
      }
      return total_period;
    }

    std::optional<gts_t>
      check_ta_alignment(bool strict_alignment, ts_db_header const &old_header, gta_t const &old_ta, gts_t const &ats)
        const {
      ts_file_header oh{
        .t0 = old_ta.total_period().start,
        .dt = old_ta.dt(),
        .tz =
          (old_ta.gt() == time_axis::generic_dt::CALENDAR
             ? (old_ta.c().cal ? old_ta.c().cal->get_tz_name() : "UTC")
             : ""),
        .point_fx = old_header.point_fx,
        .ta_type = old_header.ta_type};
      auto cts = detail::make_aligned_ts(
        strict_alignment,
        oh,
        [&](auto const &tz) {
          return lookup_calendar(tz);
        },
        ats);

      // extra check needed determine compatibility related to pre u-sec era
      switch (old_header.ta_type) {
      case time_axis::generic_dt::FIXED: {
        // refuse to merge into old second base storage. Old series should be converted
        if (old_header.is_seconds() && ats.ta.f().t.count() % core::seconds{1l}.count() != 0) {
          throw runtime_error("dtss_store: cannot merge microseconds to old seconds based storage ts-file");
        }
      } break;
      case time_axis::generic_dt::CALENDAR: {
        if (ats.ta.c().cal->tz_info->tz.tz_name == old_ta.c().cal->tz_info->tz.tz_name) {
          utctimespan remainder;
          (void) ats.ta.c().cal->diff_units(ats.ta.c().t, seconds{0l}, seconds{1l}, remainder);
          if (old_header.is_seconds() && remainder.count() != 0) {
            throw runtime_error("dtss_store: cannot merge microseconds to old seconds based storage ts-file");
          }
        }
      } break;
      case time_axis::generic_dt::POINT:
        if (old_header.is_seconds()) { // check if us resolution in new time-axis, throw if
          for (size_t i = 0; i < ats.ta.size(); ++i) {
            auto t = ats.ta.time(i);
            if (t != seconds(to_seconds64(t)))
              throw runtime_error("dtss_store: can not merge us resolution to old seconds based ts-file");
          }
          auto t_end = ats.ta.total_period().end;
          if (t_end != no_utctime && (t_end != seconds(to_seconds64(t_end))))
            throw runtime_error("dtss_store: can not merge us resolution to old seconds based ts-file");
        }
        break;
      }

      return cts;
    }

    template <class T>
    utcperiod merge_ts(FILE *fh, ts_db_header const &old_header, gta_t const &old_ta, gts_t const &ats) const {
      return do_merge<T>(fh, old_header, old_ta, ats);
    }

    // ----------

    static inline void read(FILE *fh, void *d, size_t sz) {
      size_t rsz = fread(d, sizeof(char), sz, fh);
      if (rsz != sz) {
        string fn{"?"};
        throw runtime_error(
          "dtss_store: failed to read '" + fn + "'from disk expected size=" + std::to_string(sz)
          + "!=" + std::to_string(rsz));
      }
    }

    static ts_db_header read_header(FILE *fh) {
      ts_db_header h;
      fseek(fh, 0, SEEK_SET);
      read(fh, static_cast<void *>(&h), sizeof(ts_db_header));
      if (h.is_seconds()) {
        h.data_period.start *= utctime::period::den;
        h.data_period.end *= utctime::period::den;
      }
      return h;
    }

    template <class T>
    gta_t read_time_axis(FILE *fh, ts_db_header const &h, utcperiod const p, size_t &skip_n) const {

      // seek to beginning of time-axis
      fseek(fh, sizeof(ts_db_header), SEEK_SET);

      skip_n = 0;
      utctime t0 = no_utctime;

      gta_t ta;
      ta.set_gt(h.ta_type);
      if (h.ta_type == time_axis::generic_dt::CALENDAR) {
        // must read complete here, because early exit
        // should provide valid calendar for empty frags
        // read start & step
        T::read(fh, t0);
        T::read(fh, ta.c().dt);
        // read tz_info
        uint32_t sz{0};
        read(fh, static_cast<void *>(&sz), sizeof(uint32_t));
        string tz(sz, '\0');
        {
          unique_ptr<char[]> tmp_ptr = make_unique<char[]>(sz);
          read(fh, static_cast<void *>(tmp_ptr.get()), sz);
          tz.replace(0, sz, tmp_ptr.get(), sz);
        }
        ta.c().cal = lookup_calendar(tz);
        ta.c().t = t0;
      } else if (h.ta_type == time_axis::generic_dt::FIXED) {
        // read start & step at this point.
        T::read(fh, t0);
        T::read(fh, ta.f().dt);
        ta.f().t = t0;
      }

      utctime t_start = p.start;
      utctime t_end = p.end;
      if (t_start == no_utctime)
        t_start = min_utctime;
      if (t_end == no_utctime)
        t_end = max_utctime;

      // no overlap?
      if (h.data_period.end <= t_start || h.data_period.start >= t_end) {
        return ta;
      }

      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        auto h_n = compute_n_points(h.data_period, ta.f().dt); // important! we can not use uint32_t h.n
        // handle various overlapping periods
        if (t_start <= h.data_period.start && t_end >= h.data_period.end) {
          // fully around or exact
          ta.f().t = t0;
          ta.f().n = h_n;
        } else {
          size_t drop_n = 0;
          if (t_start > h.data_period.start) // start inside
            skip_n = (t_start - h.data_period.start) / ta.f().dt;
          ta.f().t = t0 + ta.f().dt * skip_n;
          if (t_end < h.data_period.end) { // end inside
            if (h.data_period.end - t_end > ta.f().dt)
              drop_n = (h.data_period.end - t_end) / ta.f().dt - 1; // -1, keep surrounding value
          }
          // -----
          ta.f().n = h_n - skip_n - drop_n;
        }
      } break;
      case time_axis::generic_dt::CALENDAR: {

        auto h_n = compute_n_points(
          *ta.c().cal, h.data_period, ta.c().dt); // important! we can not use h.n due to uint32_t limitations
        // handle various overlapping periods
        if (t_start <= h.data_period.start && t_end >= h.data_period.end) {
          // fully around or exact
          ta.c().t = t0;
          ta.c().n = h_n;
        } else {
          size_t drop_n = 0;
          if (t_start > h.data_period.start) // start inside
            skip_n = ta.c().cal->diff_units(h.data_period.start, t_start, ta.c().dt);
          ta.c().t = ta.c().cal->add(t0, ta.c().dt, skip_n);
          if (t_end < h.data_period.end) { // end inside
            if (h.data_period.end > ta.c().cal->add(t_end, ta.c().dt, 1))
              drop_n = ta.c().cal->diff_units(t_end, h.data_period.end, ta.c().dt)
                     - 1; // drop one less, surrounding read
          }
          // -----
          ta.c().n = h_n - skip_n - drop_n;
        }
      } break;
      case time_axis::generic_dt::POINT: {
        // TODO: compute the file-size,
        //    number of points = (file_size - sizeof(header) - sizeof(int64_t))/(2*sizeof(int64_t)
        //
        if (t_start <= h.data_period.start && t_end >= h.data_period.end) {
          // fully around or exact
          ta.p().t.resize(h.n);
          T::read(fh, ta.p().t_end);
          T::read(fh, ta.p().t);
        } else {
          utctime f_time{seconds{0l}};
          vector<utctime> tmp(h.n, seconds{0l});
          T::read(fh, f_time);
          T::read(fh, tmp);
          // -----
          auto it_b = tmp.begin();
          if (t_start > h.data_period.start) {
            it_b = std::upper_bound(tmp.begin(), tmp.end(), t_start, std::less());
            if (it_b != tmp.begin()) {
              std::advance(it_b, -1); // surrounding start point
            }
          }
          // -----
          auto it_e = tmp.end();
          if (t_end < h.data_period.end) {
            it_e = std::upper_bound(it_b, tmp.end(), t_end, std::less<>());
            if (it_e != tmp.end()) {
              std::advance(it_e, 1); // keep surrounding end point.
              if (it_e != tmp.end())
                f_time = *it_e;
            }
          }
          // -----
          skip_n = std::distance(tmp.begin(), it_b);
          ta.p().t.reserve(std::distance(it_b, it_e));
          ta.p().t.assign(it_b, it_e);
          ta.p().t_end = f_time;
        }
      } break;
      }
      return ta;
    }

    vector<double> read_values(FILE *fh, ts_db_header const &h, gta_t const &ta, size_t const skip_n) const {

      // seek to beginning of values
      fseek(fh, sizeof(ts_db_header), SEEK_SET);
      switch (h.ta_type) {
      case time_axis::generic_dt::FIXED: {
        fseek(fh, 2 * sizeof(int64_t), SEEK_CUR);
      } break;
      case time_axis::generic_dt::CALENDAR: {
        fseek(fh, 2 * sizeof(int64_t), SEEK_CUR);
        uint32_t sz{};
        read(fh, static_cast<void *>(&sz), sizeof(uint32_t));
        fseek(fh, sz * sizeof(uint8_t), SEEK_CUR);
      } break;
      case time_axis::generic_dt::POINT: {
        fseek(fh, (h.n + 1) * sizeof(int64_t), SEEK_CUR);
      } break;
      }

      size_t const points_n = ta.size();
      vector<double> val(points_n, 0.);
      fseek(fh, sizeof(double) * skip_n, SEEK_CUR);
      read(fh, static_cast<void *>(val.data()), sizeof(double) * points_n);
      return val;
    }

    tuple<gts_t, utcperiod> read_ts(FILE *fh, utcperiod const p) const {
      size_t skip_n = 0u;
      ts_db_header h = read_header(fh);
      if (!h.is_valid())
        throw runtime_error("Invalid ts_db header in file,'?',(re)move and recreate the ts");
      gta_t ta = h.is_seconds() ? read_time_axis<seconds_time_io>(fh, h, p, skip_n)
                                : read_time_axis<native_time_io>(fh, h, p, skip_n);
      vector<double> v = read_values(fh, h, ta, skip_n);
      return std::make_tuple(gts_t{std::move(ta), std::move(v), h.point_fx}, h.data_period);
    }
  };

  //------------------ ts_db -------------


  ts_db::ts_db(string const &root_dir)
    : impl(new ts_db_impl(root_dir)) {
  }

  ts_db::~ts_db() = default;

  diags_t ts_db::save(string_view fn, gts_t const &ts, fx_cache_t fx_cache, store_policy const &policy) {
    return save(
      1u,
      [&](std::uint64_t) {
        return ts_item_t{fn, ts};
      },
      fx_cache,
      policy);
  }

  namespace {
    /**
     * @brief simple concurrent exec fx(i), i [0..n)
     * @param n number of fx(i) calls
     * @param fx threadsafe callable fx(size_t i)->void, there is no guarantee of order.
     * @param n_threads default 0, uses std::thread::hardware_concurrency()
     */
    constexpr auto concurrent_exec = [](size_t n, auto &&fx, size_t n_threads = 0u) -> void {
      vector<std::future<void>> workers;
      std::atomic_int64_t ai; // ensure secure access
      n_threads = n_threads != 0 ? n_threads : std::thread::hardware_concurrency();
      for (size_t i = 0; i < n_threads; ++i) {
        workers.emplace_back(std::async(std::launch::async, [&]() {
          for (size_t i = ai++; i < n; i = ai++) { // pick initial atomic ++ i, then pick another atomic ++i until done
            fx(i);
          }
        }));
      }
      for (auto &w : workers)
        w.get(); // wait until all done
    };
  }

  diags_t ts_db::save(size_t n, fx_ts_item_t const &fx_item, fx_cache_t fx_cache, store_policy const &policy) {

    diags_t diags;
    std::mutex diags_mx; // concurrency, so we need mx in error handling
    auto const save_item = [&](size_t i) {
      auto [fn, ts] = fx_item(i);
      try {
        impl->save(fn, ts, policy.recreate, policy.strict, [&](std::optional<gts_t> ats, utcperiod const &tp) {
          if (fx_cache) {
            fx_cache(i, std::move(ats), tp);
          }
        });
      } catch (std::runtime_error const &ex) {
        if (policy.best_effort) {
          std::lock_guard lock(diags_mx);
          if (!diags) // assumption: currently any runtime error is miss-matched resolution
            diags.emplace();
          (*diags).emplace_back(i, ts_diagnostics::miss_matched_resolution);
        } else
          throw;
      }
    };

    size_t const concurrent_limit = 100; // if less than this, just use ordinary save.
    size_t const n_worker_threads = 4;   // somewhat arbitrary, after all, it should be limited by io.
    if (n < concurrent_limit) {
      for (size_t i = 0; i < n; ++i) {
        save_item(i);
      }
    } else {
      concurrent_exec(n, save_item, n_worker_threads);
    }
    return diags;
  }

  tuple<gts_t, utcperiod> ts_db::read(string_view fn, utcperiod p) {
    return impl->read(fn, p);
  }

  void ts_db::remove(string_view fn) {
    return impl->remove(fn);
  }

  ts_info ts_db::get_ts_info(string_view fn) {
    return impl->get_ts_info(fn);
  }

  vector<ts_info> ts_db::find(string_view match) {
    return impl->find(match);
  }

  string ts_db::root_dir() const {
    return impl->root_dir;
  }

  void ts_db::time_format_micro_seconds(bool use_micro_seconds) {
    impl->time_format_micro_seconds = use_micro_seconds;
  }

  void ts_db::mark_for_deletion() {
    delete_db_action.arm(impl->root_dir);
  }
}
