#pragma once

#include <shyft/core/protocol.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/protocol/message_dispatcher.h>
#include <shyft/dtss/protocol/messages.h>
#include <shyft/srv/fast_server_iostream.h>

namespace shyft::dtss {

  struct server_config {
    srv::fast_server_iostream_config config;
  };

  struct binary_server : protocols::basic_server<protocol, dispatcher> {
    binary_server(dtss::server_state& s, server_config config = {})
      : basic_server({.s = s}, config.config) {
    }
  };

}