#include <ranges>

#include <boost/algorithm/string.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/thread/sync_bounded_queue.hpp>

#include <shyft/dtss/dtss.h>
#include <shyft/web_api/web_api_generator.h> // looking for the geo ts url generator

namespace shyft::dtss {

  /** detects if a geo db is using internal time-series, using the prefix shyft:// */
  static bool is_internal_geo(string const &prefix) {
    return boost::starts_with(prefix, "shyft://");
  }

  /**
   * @brief compute_slice
   * @details
   * Simply try to figure out the slice period for ts forecast fc,
   * using the semantics of the passed args.
   * @param skip_dt how much of fc to skip
   * @param ts_dt the wanted length (max_dt if 0 is specified)
   * @param max_dt the maximum assumed length of the fc
   * @return the utcperiod [skip_dt, skip_dt+ts_dt) interpreting the args, limited to max_dt
   */
  static utcperiod compute_slice(utctime skip_dt, utctime ts_dt, utctime max_dt) {
    auto s = std::min(std::max(skip_dt, utctime{0l}), max_dt); // skip_dt [0..max_dt]
    auto dt = ts_dt > utctime{0l} ? ts_dt : max_dt;            // ts_dt==0 means max_dt,..
    if (s + dt > max_dt)                                       // limit s+dt < max_dt
      dt = max_dt - s;
    return {s, s + dt};
  }

  geo::geo_ts_matrix
    do_geo_evaluate(server_state &server, geo::eval_args const &ea, bool use_cache, bool update_cache) {
    using geo::detail::ix_calc;
    using geo::detail::geo_ix;

    std::shared_ptr<geo::ts_db_config> gdb;
    if (server.msync) {
      auto gdbs = server.msync->get_geo_ts_db_info();
      for (auto const &g : gdbs) {
        if (g->name == ea.geo_ts_db_id)
          gdb = g;
      }
      if (!gdb)
        throw std::runtime_error("geo_evaluate: no geo-db configured for " + ea.geo_ts_db_id);
      if (!is_internal_geo(gdb->prefix))                                // for external, deprecated, we do not support
        return server.msync->geo_evaluate(ea, use_cache, update_cache); // any performance
    } else {
      // copy gdb, consider: make multiread-singlewrite lock
      /** protected access on lockup, but should we take a reader-lock, or just copy the gdb-structure ? */ {
        std::unique_lock<mutex> sl(server.c_mx);
        auto f = server.geo.find(ea.geo_ts_db_id);
        if (f == server.geo.end())
          throw std::runtime_error("geo_evaluate: no geo-db configured for " + ea.geo_ts_db_id);
        gdb = std::make_shared<geo::ts_db_config>(
          *f->second); // make a copy we keep during the evaluation, considered cheap relative  other work
      }
    }
    auto internal_gdb = is_internal_geo(gdb->prefix);
    if (!internal_gdb && !server.callbacks.geo_read)
      throw runtime_error("dtss: no geo read callback set, geo-extensions not available");

    // validate variables and build the geo_ts_set
    // refactor to function taking query params, gdb.cfg -> geo_ts_set or similar
    auto gx = gdb->compute(ea); // gx is a fully specified slice into the geo ts-db
    if (gx.size() == 0)
      return {};

    //
    //-- (1) create the indexed result structure, with empty time-series (to be filled in later)
    //

    auto rv = gdb->create_geo_ts_matrix(gx); // keep this as result structure, supporting pure-indexing
    //
    //-- (2) do the cache-lookups and fill in references from ts-cache, while
    //       we at the same time build the __residual gross query__
    //
    //       that we will use to query the backend-store to fill in
    //       those not matched.
    //
    //       the cache.get(...) uses a linear indexing approach,
    //       doing a callback to us for each of the
    //         ts_id(i),period(i) we search
    //       and for after lookup, it will call us with
    //         found_cb(i,bool found, ts_t const&ts_found_ref)
    //
    //       so we need a fast computed size_t ix -> tuple of (t0,v,e,g) indicies matching the above structure
    //        (lets generate it in the loop above)
    //    -- then
    //       our plan is to use the
    //         found_cb(i,found=false,ts=none) -> extend the __residual gross query__
    //        and
    //         found_cb(i,found=true,ts=found_ts) -> i->tuple of(t0,v,e,g) rv[t0][v][e][g].ts=found_ts
    //
    // Invoke the ts_cache with suitable callbacks to us.

    ix_calc rx(gx.t.size(), gx.v.size(), gx.e.size(), gx.g.size()); // rx result index flat<-->ndim conversions by math
    auto t0_ta = gdb->t0_time_axis();
    vector<size_t> cache_misses;
    cache_misses.reserve(rx.size());
    auto p_slice = compute_slice(ea.cc_dt0, ea.ts_dt, gdb->dt);
    gx.skip_dt = p_slice.start;
    gx.ts_dt = p_slice.timespan();
    string ts_url;
    ts_url.reserve(500); // assumed reasonable max-limit(not hard, only performance)
    auto ts_url_sink = std::back_inserter(ts_url);
    web_api::generator::geo_ts_url_generator<decltype(ts_url_sink)> geo_ts_url_(
      fmt::format("{}{}/", gdb->prefix, gdb->name));
    geo::ts_id url_ix;
    url_ix.geo_db = ea.geo_ts_db_id; // assign only once, invariant for this query
    if (use_cache) {
      server.ts_cache.get(
        rx.size(), //  this is the number of items we request,
        [&gx, &rx, &geo_ts_url_, &ts_url, &ts_url_sink, &url_ix](
          size_t i) -> string { // this will be how we generate the ts-url
          auto const ix = rx.ix(i);
          ts_url.resize(0);       // resize,keep memory
          url_ix.v = gx.v[ix.v];  // important, translate to cfg.referenced indexes
          url_ix.e = gx.e[ix.e];  //  --!--
          url_ix.g = gx.g[ix.g];  //  --!--
          url_ix.t = gx.t[ix.t0]; // get the real-time from ix.t0, so no translate to cfg needed
          boost::spirit::karma::generate(ts_url_sink, geo_ts_url_, url_ix);
          return ts_url; // here we want a single copy string op(it has to be one)

        },
        [&gx, &rx](size_t i) -> utcperiod { // this will return the wanted (minimum) period requested from cache
          auto t0 = gx.t[rx.ix(i).t0]; // t0_ta.time(rx.ix(i).t0);// the start of this 'forecast', using the t0_ta as
                                       // the correct reference
          return {t0 + gx.skip_dt, t0 + gx.skip_dt + gx.ts_dt}; // the total covering period for this 'forecast'
        },
        [&rx, &rv, &cache_misses](
          size_t i, bool found, ts_frag f_ts, utcperiod /*tp*/) -> void { // we get called back for each item here.
          if (found) {
            auto const ix = rx.ix(i);
            rv._set_ts(
              ix.t0,
              ix.v,
              ix.e,
              ix.g,
              apoint_ts{
                std::move(f_ts)}); // notice that here we use ix. directly to correctly address the resulting structure
          } else {
            cache_misses.emplace_back(i);
          }
        });
    } else {
      for (size_t i = 0; i < rx.size(); ++i) // fill in all misses
        cache_misses.emplace_back(i);
    }

    // -- (3) Analyze the result(hits,misses) from above, and if needed call to backend to refill the missing.
    //
    //    pre:The call to backend should return exactly what is requested,
    //        so the request need to be precise, and let's say we
    //        use indices referenced to the geo cfg (so that t0-idx is i'th t0 =cfg.t0[t0_idx],
    //            and v-idx is ref to the i'th variables as cfg.variables[v_idx] etc.)
    //        then the request is minimal, precise, and welldefined.
    //
    //        For the returned result-type, In python, it could be convenient to address this as
    //        GeoTsRequestResult that maps to c++ geo_ts_request_result
    //        E.g:
    //
    //          r=GeoTsRequestResult(n_t0,n_var,n_ens,n_geo)
    //          then
    //          r.assign_ts(t0,var,ens,geo,ts), where t0,var,ens,geo are all indexes as pr. result-indexing space
    //          r.ts(t0,var,ens,geo,ts) -> gives the TimeSeries at specified result index.
    //                 .. so we can range-check t0,var,ens,geo against the specified range at construction.
    //                    to minimize mixing up indexes-errors.
    //
    //        internally rep as ats_vector[n_t0*n_var*n_ens*n_geo]
    //          ats_vector[i], where i is computed as (n_t0*n_var*n_ens*n_geo)*t0 + (n_var*n_ens*n_geo)*v +
    //          (n_ens*n_geo)*e + g
    //                          reversed compute is g= i%n_geo, .. e=.., v=.., t0=..
    //
    //   post:When handling the result of the callback request, we need to map
    //        the python GeoTsResult (alias c++ geo_ts_request_result)
    //        back into the result structure as provided/returned by this routine.
    //
    //        recall r[t0-ix][var-ix][ens_idx]::type geo_ts_vector, where the i'th member of geo_ts-vector represents
    //        ith point'
    //
    //        The most efficient manner is index-lookup-translations:
    //          r_map_t0[t0_request_idx] gives directly ix that goes into the rv[ix]
    //          r_map_v[v_request_idx] gives directly the ix that goes into the rv[t0_ix][v_ix]
    //
    if (cache_misses.size() > 0) {
      // prep the call to backend assume we should read all:
      auto b_gx = gx;
      auto b_ta = ea.ta.size() ? ea.ta : gdb->t0_time_axis(); // empty means all
      // for speed, we need a direct lookup map from callback indicies to result-set indicies
      vector<size_t> t_map; // if all timepoints, then t_map[i] would be 0,1,.. n-1  etc.
      vector<size_t> v_map; // if all  variables then v_map[i] would be 0,1,..n-1 etc.
      vector<size_t> e_map;
      vector<size_t> g_map;

      if (
        cache_misses.size()
        < rx.size()) { // optimize away if all missing, in this case: reduce what we read into strictly needed
        using boost::dynamic_bitset;
        t_map.reserve(gx.t.size()); // if all timepoints, then t_map[i] would be 0,1,.. n-1  etc.
        v_map.reserve(gx.v.size()); // if all  variables then v_map[i] would be 0,1,..n-1 etc.
        e_map.reserve(gx.e.size());
        g_map.reserve(gx.g.size());
        b_gx.size_to_zero();

        // scan and register missing, using bit-map approach, note ix positions refers to rx indexing space (result, not
        // cfg)
        // TODO: we could keep these, and be 100% exact on fetch/store, but
        // for now we accept we get the union of the slices missing(.e.g if one var is missing tx, and another missing
        // ty, then we read both vars at tx,ty)
        dynamic_bitset<> xvar{gdb->variables.size()};
        dynamic_bitset<> xgeo{gdb->grid.points.size()};
        dynamic_bitset<> xens{(size_t) gdb->n_ensembles};
        dynamic_bitset<> xt0{t0_ta.size()};
        for (auto cm : cache_misses) {
          auto const x = rx.ix(cm);
          xvar[x.v] = true;
          xens[x.e] = true; // i'th index ref to arg ens_ix[i]
          xgeo[x.g] = true;
          xt0[x.t0] = true; // i'th index referring to t0_ta time-points
        }
        // construct arguments to pass to geo read callback routine
        // they are either by value, or by ix-ref to cfg. tables
        for (size_t i = 0; i < xvar.size(); ++i)
          if (xvar[i]) {
            b_gx.v.push_back(gx.v[i]);
            v_map.push_back(i);
          } // gx.v[i] ref to cfg.variables[i]
        for (size_t i = 0; i < xens.size(); ++i)
          if (xens[i]) {
            b_gx.e.push_back(gx.e[i]);
            e_map.push_back(i);
          } // gx.e[i] ref to arg ens_idx, by value.
        for (size_t i = 0; i < xgeo.size(); ++i)
          if (xgeo[i]) {
            b_gx.g.push_back(gx.g[i]);
            g_map.push_back(i);
          } // gx.g[i] ref. to positions found by  geo-query
        for (size_t i = 0; i < xt0.size(); ++i)
          if (xt0[i]) {
            b_gx.t.push_back(gx.t[i]);
            t_map.push_back(i);
          } // gx.t[i] its directly time.
        b_ta = gta_t(b_gx.t, t0_ta.total_period().end); // the end-point is not important
      }
      //
      // here we perform the call to backend, getting the missing results back in a dense ats_vector format
      //                           cfg.v    cfg.points   by val   ta

      auto [br, br_tp] = internal_gdb ? do_internal_geo_read(server, gdb, b_gx, use_cache)
                                      : do_external_geo_read(server, gdb, b_gx); // backend result

      // unwind the result from  the backend, and put it into the missing entries of the result structure
      // br[var_idx][geo_idx][ens_idx]::type ats_vector, keeping all results, ordered by t0
      // __x means final result-index reference
      // _i  means cb result reference (they might differ)

      vector<string> c_id;    // cache these ts urls with ts below
      vector<ts_frag> c_ts;   // cache these ts
      vector<utcperiod> c_tp; // total period as from read.
      if (update_cache) {     // ensure to make room for result to cache
        size_t c_size = b_gx.size();
        c_id.reserve(c_size);
        c_ts.reserve(c_size);
        c_tp.reserve(c_size);
      }
      for (size_t ti = 0; ti < b_ta.size(); ++ti) {
        size_t tix = t_map.size() ? t_map[ti] : ti; // if all t, then 1-1 map, else pick result-index from t_map.
        for (size_t vi = 0; vi < b_gx.v.size(); ++vi) {
          size_t vix = v_map.size() ? v_map[vi] : vi; // if all v, then 1-1 map, else pick result index from v_map.
          for (size_t ei = 0; ei < b_gx.e.size(); ++ei) {
            size_t eix = e_map.size() ? e_map[ei] : ei; // if all e, then 1-1 map, else pick result index from e_map.
            for (size_t gi = 0; gi < b_gx.g.size(); ++gi) {
              size_t gix = g_map.size() ? g_map[gi] : gi; // if all e, then 1-1 map, else pick result index from e_map.
              auto const &rts = br._ts(ti, vi, ei, gi);
              rv._set_ts(tix, vix, eix, gix, rts);
              if (update_cache) {     // collect stuff to cache here.(avoid multiple calls for mutex)
                ts_url.resize(0);     // resize,keep memory
                url_ix.v = gx.v[vix]; // important, translate to cfg.referenced indexes
                url_ix.e = gx.e[eix]; //  --!--
                url_ix.g = gx.g[gix]; //  --!--
                url_ix.t = gx.t[tix]; //
                boost::spirit::karma::generate(ts_url_sink, geo_ts_url_, url_ix);
                c_id.push_back(ts_url);
                c_ts.emplace_back(std::dynamic_pointer_cast<gpoint_ts const>(rts.ts)); // FIXME: incref +1
                c_tp.emplace_back(br_tp[br.shape.flat(ti, vi, ei, gi)]);
              }
            }
          }
        }
      }
      if (update_cache && c_id.size()) {
        server.ts_cache.add(std::views::zip(c_id, c_ts, c_tp));
      }
    }

    //
    // (4) if concat, execute that here
    //
    if (ea.concat) {
      // take care! the concat preconditions is that
      // each ts in the rv starts at t0_time_axis timepoints and are at least
      // [t0.. +  ts_dt) long!
      // The concatenate aim to extract, and join together
      // multiple slices [t0+cc_dt0,t0+ts_dt )
      // thus, when reading with slicing(skipping cc_dt0), or not,
      // we need to ensure that
      // this is true, prior to calling the algo.
      // NOTE: if we provided t0-axis, or first t0,  to the concat algo, that would be better!
      // At this point the time-seres could have
      // any length and if not providing t0 time-axis
      // we need to ensure pre-condidtions are met.
      //      (1) since we read with ea.cc_dt0, we set cc_dt0 =0 here
      //      (2) and ensure that all ts starts at t0_time_axis[i]+original.cc_dt0
      //
      auto skip_dt = gx.skip_dt;
      // compute the delta t between the forecast we want to concat.
      // notice there currently is no explicit parameter for this, other than the
      // ea.ta (time-axis), that specifies the fc.t0
      // thus in a perfect world this would be a fixed 6h interval
      // for for example arome.
      // Since the ea.ta need to specify ONLY the t0 of the
      // forecasts that really exists, it means that
      // for a realistic example there might several
      // fc missing, materialized as t0 sequence
      //                00  12 18 00 06 12 18 00
      //  missing 06 here!^
      // So the semantically correct ts_dt is the minimum
      // 'timespan' found in the ea.ta periods.
      // It would make no semantic difference on the
      // resulting concat if for example we are missing
      // every second t0, e.g.  sequence : 00 12 00 12 00 00
      // would concat to the same sequence with dt =12h, as 06h
      // given this: the definition is to extract
      auto ts_dt = ea.ta.period(0).timespan();
      for (auto i = 1u; i < ea.ta.size(); ++i)
        ts_dt = std::min(ts_dt, ea.ta.period(i).timespan());

      for (auto t = 0u; t < rv.shape.n_t0; ++t) {
        for (auto v = 0u; v < rv.shape.n_v; ++v) {
          for (auto e = 0u; e < rv.shape.n_e; ++e) {
            for (auto g = 0u; g < rv.shape.n_g; ++g) {
              auto ix = rv.shape.flat(t, v, e, g);
              // need to find real t0 for the t-index,
              //  t0_time_axis.time(some-index)
              if (t >= gx.t.size())
                throw std::runtime_error(fmt::format("ix {}: t {} is > gx.t.size {}", ix, t, gx.t.size()));
              auto t0 = gx.t[t]; // this index is directly the gx.t[t]
              if (rv.tsv[ix].ts.size()) {
                auto &ts = rv.tsv[ix].ts;
                auto a_dt = skip_dt;
                if (ts.time(0) < t0 + a_dt) {
                  // then take action and clip so t0 starts at right value
                  auto i0 = ts.index_of(t0 + a_dt);
                  auto n = ts.size();
                  if (i0 != std::string::npos) {
                    ts = ts.slice(i0, n - i0);
                  }
                }
              }
            }
          }
        }
      }
      skip_dt = utctime{0l};
      rv = rv.concatenate(skip_dt, ts_dt);
    }

    //
    // (5) if expressions, evaluate them here
    //
    if (!ea.ts_expressions.empty())
      rv = rv.transform(ea.ts_expressions);

    return rv;
  }

  vector<geo::ts_db_config_> do_get_geo_info(server_state &state) {
    if (state.msync) {
      return state.msync->get_geo_ts_db_info();
    } else {
      std::unique_lock<mutex> sl(state.c_mx); // ensure we got geo stable
      vector<geo::ts_db_config_> r;
      r.reserve(state.geo.size());
      for (auto const &kv : state.geo)
        r.emplace_back(make_shared<geo::ts_db_config>(*kv.second));
      return r;
    }
  }

  void add_geo_ts_db(server_state &state, geo::ts_db_config_ const &cfg) {
    if (!cfg)
      throw runtime_error("add_gs_ts_db must have a non-null argument");
    if (state.msync) {
      state.msync->add_geo_ts_db(cfg);
    } else {
      std::unique_lock<mutex> sl(state.c_mx); // ensure we got geo stable
      if (is_internal_geo(cfg->prefix)) {
        auto f = container_find(state, cfg->name);
        if (f->first == "") {                                     // create a new container
          auto nc_root = f->second->root_dir() + "/" + cfg->name; // new container path is relative to root container
          if (state.default_geo_db_type == "ts_db")
            state.container[cfg->name] = std::make_unique<ts_db>(nc_root);
          else // rocksdb as default and not matching any of the above
            state.container[cfg->name] = std::make_unique<ts_db_rocks>(nc_root, state.default_geo_db_cfg);
          state.geo[cfg->name] = cfg;
          f = container_find(state, cfg->name);
        } else if (f == state.container.end()) // container not configured also no root container found
          throw runtime_error("dtss.add_geo_ts_db: could not find or create ts-store container for name=" + cfg->name);
        do_geo_ts_cfg_store(state, f->second->root_dir(), cfg);
      }
      state.geo[cfg->name] = std::make_shared<geo::ts_db_config>(*cfg);
    }
  }

  void remove_geo_ts_db(server_state &state, std::string const &geo_db_name) {
    if (state.msync) {
      state.msync->remove_geo_ts_db(geo_db_name);
    } else {
      std::unique_lock<mutex> sl(state.c_mx); // ensure we got geo stable
      auto f = state.geo.find(geo_db_name);
      if (f != state.geo.end()) {
        // remove any cache-entries
        state.ts_cache.flush();       // for now just empty all, later be specific, remove items selective
        auto gdb = f->second;         // keep a ref
        state.geo.erase(geo_db_name); // get rid of the entry
        if (
          is_internal_geo(gdb->prefix)) { // possibly cleanup internal stuff, could take a loong time if a lot of files!
          its_db &ts_db = internal(state, gdb->name);
          fs::path gdb_root = fs::path(ts_db.root_dir());
          state.container.erase(geo_db_name);
          fs::remove_all(gdb_root); // the last thing, otherwise win will fail on locked files
        }
      } // silently ignore attemt to remove non-existing geo_db_name ? The post-condition is the same
    }
  }

  void do_geo_ts_cfg_store(server_state &state, std::string const &root, geo::ts_db_config_ const &gdb) {
    auto cfg_path = fs::path(root) / geo_cfg_file;
    std::ofstream o(cfg_path.string(), std::ios::binary);
    core::core_oarchive oa(o, core::core_arch_flags);
    oa << core::core_nvp("cfg", gdb);
  }

  void do_update_geo_db(
    server_state &server,
    string const &geo_db_name,
    string const &description,
    string const &json,
    string const &origin_proj4) {
    if (server.msync) {
      server.msync->update_geo_db(geo_db_name, description, json, origin_proj4);
    } else {
      std::unique_lock<mutex> sl(server.c_mx);
      auto f = server.geo.find(geo_db_name);
      if (f == server.geo.end()) // here we could scan & load internal geo-db, instead of failing
        throw std::runtime_error("update_geo_db: geo db not found:" + geo_db_name);
      f->second->descr = description;
      f->second->json = json;
      f->second->origin_proj4 = origin_proj4;
      auto internal_gdb = is_internal_geo(f->second->prefix);
      if (internal_gdb) { // if it's internal we take responsibility to store the most recent geo_ts_db_config
        do_geo_ts_cfg_store(server, internal(server, f->second->name).root_dir(), f->second);
      }
    }
  }

  void do_geo_store(
    server_state &server,
    std::string const &geo_db_name,
    geo::ts_matrix const &tsm,
    bool replace,
    bool cache) {
    using std::to_string;

    if (tsm.shape.size() == 0)
      throw runtime_error("dtss geo store: no geo matrix data supplied");
    if (geo_db_name.empty())
      throw runtime_error("dtss geo store: no geo db-name is specified");
    if (server.msync) {
      server.msync->geo_store(geo_db_name, tsm, replace, cache);
    } else {
      shared_ptr<geo::ts_db_config> gdb;
      /** protected access on lockup,  copy the gdb-structure */ {
        std::unique_lock<mutex> sl(server.c_mx);
        auto f = server.geo.find(geo_db_name);
        if (f == server.geo.end()) // here we could scan & load internal geo-db, instead of failing
          throw std::runtime_error("geo_evaluate: no geo-db configured for " + geo_db_name);

        gdb = make_shared<geo::ts_db_config>(
          *f->second); // make a copy we keep during the evaluation, considered cheap relative  other work
      }
      auto internal_gdb = is_internal_geo(gdb->prefix);
      if (!internal_gdb && !server.callbacks.geo_store)
        throw runtime_error("dtss: no geo store callback set, geo-extensions not available");
      // verify correct shape, we need it twice, so a local lambda helps us doing that:
      auto verify_shape_vs_gdb = [](geo::detail::ix_calc const &shape, geo::ts_db_config const &gdb) {
        if (shape.n_v != gdb.variables.size())
          throw runtime_error(
            "dtss geo store: variable dimension " + to_string(shape.n_v) + " differs from required "
            + to_string(gdb.variables.size()));
        if (int64_t(shape.n_e) != gdb.n_ensembles)
          throw runtime_error(
            "dtss geo store: ensemble dimension " + to_string(shape.n_e) + " differs from required "
            + to_string(gdb.n_ensembles));
        if (shape.n_g != gdb.grid.points.size())
          throw runtime_error(
            "dtss geo store: geo point  dimension " + to_string(shape.n_g) + " differs from required "
            + to_string(gdb.grid.points.size()));
      };
      verify_shape_vs_gdb(tsm.shape, *gdb);
      vector<utcperiod> tsm_tp;
      if (internal_gdb)
        tsm_tp = do_internal_geo_store(server, gdb, tsm, replace);
      else {
        server.callbacks.geo_store(gdb, tsm, replace); // do the store operation here
        tsm_tp.resize(tsm.shape.size());               // non periods for external db
      }
      // figure out if we got more t0-versions, and update accordingly
      vector<utctime> t0x;
      t0x.reserve(tsm.shape.n_t0);
      auto t0_ta = gdb->t0_time_axis();
      for (size_t i = 0; i < tsm.shape.n_t0; ++i) {
        auto t0 = gdb->get_associated_t0(tsm._ts(i, 0, 0, 0).time(0), !replace); // important if concat (alias !replace)
        size_t t0_idx = t0_ta.index_of(t0);
        if (t0_idx == string::npos || t0_ta.time(t0_idx) != t0) {
          t0x.push_back(t0);
        }
      }

      std::unique_lock<mutex> sl(server.c_mx);
      if (t0x.size()) { // new points arrived, we update the t0 of  geo-db using scoped lock
        auto f = server.geo.find(geo_db_name);
        if (f == server.geo.end())
          throw std::runtime_error("geo_evaluate: geo-db removed during write for " + geo_db_name);
        // here we could compare f->second vs gdb  to ensure the defs is consistent
        verify_shape_vs_gdb(
          tsm.shape, *f->second); // relax requirement, just  verify equal in v,e,g dimensions, allow t0 changes.
        auto need_sort = f->second->t0_times.size() ? t0x.front() < f->second->t0_times.back() : false;
        for (auto t : t0x)
          f->second->t0_times.push_back(t);
        if (need_sort) {
          auto &t0v = f->second->t0_times;
          std::sort(t0v.begin(), t0v.end());
          auto last = std::unique(t0v.begin(), t0v.end());
          if (last != t0v.end()) {
            t0v.erase(last, t0v.end()); // get rid of possible duplicates when updating existing t0
          }
        }
        if (internal_gdb) { // if it's internal we take responsibility to store the most recent geo_ts_db_config
          do_geo_ts_cfg_store(server, internal(server, f->second->name).root_dir(), f->second);
        }
      }
      if (cache || replace) { // if replace, flush old items.
        // update/replace cache items stored.
        //  this might take some time for large payloads (like million)
        string ts_url;
        ts_url.reserve(500); // assumed reasonable max-limit(not hard, only performance)
        auto ts_url_sink = std::back_inserter(ts_url);
        web_api::generator::geo_ts_url_generator<decltype(ts_url_sink)> geo_ts_url_(
          fmt::format("{}{}/", gdb->prefix, gdb->name));
        geo::ts_id url_ix;
        url_ix.geo_db = geo_db_name; // assign only once, invariant for this query
        auto update_range = std::views::transform(std::views::iota(0u, tsm.shape.size()), [&](size_t i) {
          auto ix = tsm.shape.ix(i);
          ts_url.resize(0); // resize,keep memory
          url_ix.v = ix.v;
          url_ix.e = ix.e;
          url_ix.g = ix.g;
          url_ix.t = gdb->get_associated_t0(
            tsm.tsv[i].time(0), !replace); // again, ensure we use correct t0 in case of concat
          boost::spirit::karma::generate(ts_url_sink, geo_ts_url_, url_ix);
          auto gts = std::dynamic_pointer_cast<gpoint_ts const>(tsm.tsv[i].ts);
          if (!gts)
            throw std::runtime_error("dtss::do_read_geo error");
          return std::make_tuple(ts_url, gts, tsm_tp[i]);
        });

        server.ts_cache.update(
          update_range,
          cache,
          false // hmm. we use replace as concat, so  let's just add to cache for now
        );
      }
    }
  }

  /** simple pod */
  namespace {
    struct ts_store_pkg {
      string ts_name;
      gpoint_ts const *gts{nullptr}; // not owning, scope-life time kept by store-worker
      utcperiod *tp_result{nullptr}; // points to memory where to store new total period
    };
  }

  static size_t compute_n_workers(size_t n_ts) {
    auto hw_threads = std::thread::hardware_concurrency();
    if (hw_threads < 2)
      hw_threads = 2;
    return n_ts < hw_threads ? n_ts : hw_threads;
  }

  vector<utcperiod>
    do_internal_geo_store(server_state &server, geo::ts_db_config_ cfg, geo::ts_matrix const &tsm, bool replace) {
    string ts_path;
    ts_path.reserve(100);
    auto sink = std::back_inserter(ts_path);
    geo::ts_id tidx{cfg->name, 0, 0, 0, utctime(0l)}; // reuse this in loop, reset ts_path
    web_api::generator::geo_ts_url_generator<decltype(sink)> url_gen(
      "");                                       // drop the prefix, we want naked urls in this case
    its_db &ts_db = internal(server, cfg->name); // kept ref. hmm. could consider shared something here
    core::calendar utc;
    using boost::queue_op_status;
    using ts_store_pkgs = vector<ts_store_pkg>;
    using s_queue = boost::sync_bounded_queue<ts_store_pkgs>;
    using std::async;
    using std::launch;

    size_t batch_size = 10000u; // assume this is suitable, or  minimizes the overhead substantially
    size_t n_in_flight = compute_n_workers(1 + (tsm.shape.size() / batch_size));
    s_queue wq(n_in_flight * 2); // add some slack in the queue
    vector<std::future<int>> wt;
    vector<utcperiod> tp_vec(tsm.shape.size());

    for (size_t i = 0; i < n_in_flight; ++i) {
      wt.emplace_back(
        async(launch::async, [&wq, &ts_db, replace]() -> int { // note, this is the logic for the queue worker thread
          int error_count = 0;
          bool got_exception{false};
          runtime_error re{""};
          ts_store_pkgs pkg;
          while (queue_op_status::closed != wq.wait_pull_front(pkg)) {

            if (!pkg.empty()) {
              try { // avoid harakiri and writer wait-forever on run-time errors, continue as long as possible
                (void) ts_db.save(
                  pkg.size(),
                  [&](size_t i) {
                    return ts_item_t{pkg[i].ts_name, pkg[i].gts->rep};
                  },
                  [&](size_t i, optional<gts_t>, utcperiod tp) {
                    if (pkg[i].tp_result)
                      *(pkg[i].tp_result) = tp;
                    // currently no support for store policy flexible
                    // that would require us to do cache at this point
                    // We could consider doing the cache here, but that would lock x n on cache
                  },
                  store_policy{.recreate = replace, .cache = true});
              } catch (runtime_error const &e) {
                ++error_count;
                re = e;
              }
            } else
              ++error_count; // not very likely,
          }
          if (got_exception)
            throw std::runtime_error{re};
          return error_count;
        }));
    }
    ts_store_pkgs pkgs;
    pkgs.reserve(batch_size);
    for (size_t i = 0; i < tsm.shape.n_t0; ++i) {
      for (size_t v = 0; v < tsm.shape.n_v; ++v) {
        tidx.v = v;
        for (size_t e = 0; e < tsm.shape.n_e; ++e) {
          tidx.e = e;
          for (size_t g = 0; g < tsm.shape.n_g; ++g) {
            ts_path.resize(0); // reset to 0, keep mem
            tidx.g = g;
            apoint_ts const &ts = tsm._ts(i, v, e, g);
            auto gts = dynamic_cast<gpoint_ts const *>(ts.ts.get());
            tidx.t = cfg->get_associated_t0(ts.time(0), !replace); // pr. def
            generate(std::back_inserter(ts_path), url_gen, tidx);
            pkgs.emplace_back(ts_path, gts, tp_vec.data() + tsm.shape.flat(i, v, e, g));
            if (pkgs.size() == batch_size) {
              wq.push_back(pkgs);
              pkgs.resize(0); // keep mem
            }
          }
        }
      }
    }
    if (!pkgs.empty())
      wq.push_back(pkgs); // push last of batch that might be less than batch size
    wq.close();           // signal end of story
    // ensure to collect all errors/threads
    int sum_errors{0};
    bool got_exception{false};
    runtime_error re{""}; // We accept that this one might be overwritten, last exception counts
    for (auto &w : wt) {
      try {
        sum_errors += w.get();
      } catch (runtime_error const &e) { // is this ok for now?
        re = e;
        got_exception = true;
      } catch (std::exception const &e) { // if we get another exception.. should we just collect it, and
        got_exception = true;
        re = runtime_error(e.what());
      } catch (...) {
        got_exception = true;
        re = runtime_error("unknown exception from storage-worker");
      }
    }
    if (got_exception)
      throw std::runtime_error{re};
    if (sum_errors)
      throw runtime_error("Failed to store all series, number of failures is " + std::to_string(sum_errors));
    return tp_vec;
  }

  std::pair<geo::ts_matrix, vector<utcperiod>>
    do_external_geo_read(server_state &server, geo::ts_db_config_ const &cfg, geo::slice const &gs) {
    return std::make_pair(server.callbacks.geo_read(cfg, gs), vector<utcperiod>{gs.size()});
  }

  namespace {
    /** simple pod for read queue operation */
    struct ts_read_pkg {
      string ts_name;
      utcperiod read_period;
      apoint_ts *ats{nullptr}; // target read assign, like *ats=read-result
      utcperiod *total_period; // similar for the ts.total_period required for caching
    };
  }

  std::pair<geo::ts_matrix, vector<utcperiod>>
    do_internal_geo_read(server_state &server, geo::ts_db_config_ const &cfg, geo::slice const &gs, bool use_cache) {
    geo::ts_matrix r = cfg->create_ts_matrix(gs); // create the result.
    vector<utcperiod> r_tp{r.shape.size()};       // make space for the total period as we need them for cache updates.
    auto r_tp_ptr = [&](size_t i, size_t v, size_t e, size_t g) {
      return r_tp.data() + r.shape.flat(i, v, e, g);
    };
    string ts_path;
    ts_path.reserve(100); // we will regenerate this mill times, avoid malloc between
    auto sink = std::back_inserter(ts_path);
    geo::ts_id tidx{cfg->name, 0, 0, 0, utctime(0l)}; // reuse this in loop, reset ts_path
    auto p_slice = compute_slice(gs.skip_dt, gs.ts_dt, cfg->dt);
    if (server.msync) {
      web_api::generator::geo_ts_url_generator<decltype(sink)> url_gen(
        fmt::format("shyft://{}/", cfg->name)); // drop the prefix, we want naked urls in this case      auto p_slice =
                                                // compute_slice(gs.skip_dt, gs.ts_dt, cfg->dt);
      for (size_t i = 0; i < gs.t.size(); ++i) {
        utcperiod read_period{gs.t[i] + p_slice.start, gs.t[i] + p_slice.end}; // maybe utc.add(ti,cfg->dt,1)
        // v,e,g dimensions share read-period
        tidx.t = gs.t[i];
        id_vector_t ts_ids;
        ts_ids.reserve(gs.v.size() * gs.e.size() * gs.g.size());
        for (size_t v = 0; v < gs.v.size(); ++v) {
          tidx.v = gs.v[v];
          for (size_t e = 0; e < gs.e.size(); ++e) {
            tidx.e = gs.e[e];
            for (size_t g = 0; g < gs.g.size(); ++g) {
              ts_path.resize(0); // reset to 0, keep mem
              tidx.g = gs.g[g];
              generate(std::back_inserter(ts_path), url_gen, tidx);
              ts_ids.emplace_back(ts_path);
            }
          }
        }
        auto [ts_frags, tps, errors] = server.msync->try_read(ts_ids, read_period, use_cache);
        if (!errors.empty()) {
          throw std::runtime_error(
            fmt::format(
              "Failed to read all series, number of failures is {}, error code first {}",
              errors.size(),
              errors.front()));
        }
        /** unpack result into matrix */ {
          size_t j = 0;
          for (size_t v = 0; v < gs.v.size(); ++v) {
            for (size_t e = 0; e < gs.e.size(); ++e) {
              for (size_t g = 0; g < gs.g.size(); ++g) {
                *r_tp_ptr(i, v, e, g) = tps[j];
                *r._ts_pointer(i, v, e, g) = std::move(apoint_ts(std::move(ts_frags[j])));
                ++j;
              }
            }
          }
        }
      }
    } else {
      web_api::generator::geo_ts_url_generator<decltype(sink)> url_gen(
        "");                                       // drop the prefix, we want naked urls in this case
      its_db &ts_db = internal(server, cfg->name); // assume it succeeds here


      using boost::queue_op_status;
      using r_queue = boost::sync_bounded_queue<ts_read_pkg>;
      using std::async;
      using std::launch;
      size_t n_in_flight = compute_n_workers(gs.size());
      r_queue rq(n_in_flight * 2); // add some slack in the queue
      vector<std::future<int>> wt;
      for (size_t i = 0; i < n_in_flight; ++i) {
        wt.emplace_back(async(launch::async, [&rq, &ts_db]() -> int {
          int error_count = 0;
          ts_read_pkg pkg;
          while (queue_op_status::closed != rq.wait_pull_front(pkg)) {

            if (pkg.ats) {
              try { // no harakiri on run-time errors, continue as long as possible
                auto [pts, ts_tp] = ts_db.read(pkg.ts_name, pkg.read_period);
                auto ts = std::make_shared<gpoint_ts const>(std::move(pts));
                apoint_ts rts(std::move(ts));
                *pkg.ats = std::move(rts);
                *pkg.total_period = ts_tp;
              } catch (runtime_error const &e) {
                // TODO: Notify, but it might already be logged in db driver.
                ++error_count;
              }
            } else
              ++error_count; // not very likely,
          }
          return error_count;
        }));
      }

      for (size_t i = 0; i < gs.t.size(); ++i) {
        utcperiod read_period{
          gs.t[i] + p_slice.start, gs.t[i] + p_slice.end}; // maybe utc.add(ti,cfg->dt,1) to support yearly/monthly?
        // v,e,g dimensions share read-period
        tidx.t = gs.t[i];
        for (size_t v = 0; v < gs.v.size(); ++v) {
          tidx.v = gs.v[v];
          for (size_t e = 0; e < gs.e.size(); ++e) {
            tidx.e = gs.e[e];
            for (size_t g = 0; g < gs.g.size(); ++g) {
              ts_path.resize(0); // reset to 0, keep mem
              tidx.g = gs.g[g];
              generate(std::back_inserter(ts_path), url_gen, tidx);
              rq.push_back(ts_read_pkg{ts_path, read_period, r._ts_pointer(i, v, e, g), r_tp_ptr(i, v, e, g)});
            }
          }
        }
      }
      rq.close(); // signal end of story, so that our read-workers quits the loop
      // ensure to collect all errors/threads
      int sum_errors{0};
      bool got_exception{false};
      runtime_error re{""}; // We accept that this one might be overwritten, last exception counts
      for (auto &w : wt) {
        try {
          sum_errors += w.get();
        } catch (runtime_error const &e) { // is this ok for now?
          re = e;
          got_exception = true;
        } catch (std::exception const &e) { // if we get another exception.. should we just collect it, and
          got_exception = true;
          re = runtime_error(e.what());
        } catch (...) {
          got_exception = true;
          re = runtime_error("unknown exception from read-worker");
        }
      }
      if (got_exception)
        throw std::runtime_error{re};
      if (sum_errors)
        throw runtime_error("geo read: read failure for a number of requests " + std::to_string(sum_errors));
    }
    return std::make_pair(r, r_tp);
  }

  std::optional<geo::ts_db_config_ > geo_ts_db_scan(server_state &state, fs::path root) {
    if (fs::exists(root) && fs::is_directory(root)) {
      auto cfg_path = root / geo_cfg_file;
      if (fs::exists(cfg_path) && fs::is_regular_file(cfg_path)) {
        try {
          std::ifstream is(cfg_path.c_str(), std::ios::binary);
          core::core_iarchive ia(is, core::core_arch_flags);
          geo::ts_db_config_ cfg;
          ia >> cfg;
          if (cfg)
            return cfg;
        } catch (...) {
          return {};
        }
      }
    }
    return {};
  }

}
