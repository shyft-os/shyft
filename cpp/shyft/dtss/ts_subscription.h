#pragma once
/** This file is part of Shyft. Copyright 2015-2019 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <shyft/core/subscription.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::subscription {
  using std::shared_ptr;
  using std::vector;
  using std::string;
  using std::end;
  using std::unordered_map;
  using std::array;
  using std::recursive_mutex;
  using std::scoped_lock;
  using std::begin;
  using std::end;
  using std::atomic_int64_t;

  using namespace shyft::core;
  using shyft::core::subscription::observer_base;
  using shyft::core::subscription::manager_;
  using shyft::core::subscription::observable_;
  using id_vector_t = vector<string>;

  /** @brief POD to keep  published version and period monitored as pair */
  struct ts_o {
    std::int64_t v{0}; ///< the version published
    utcperiod p{};     ///< the period that we monitor for this specific time-series (grand union period)
    observable_ o; ///< the shared_ptr to the dtss manager maintained observable,
                   ///< so the o->v is the observed change counter
  };

  /**
   * @brief Subscribed observer for stored time-series
   *
   * @details
   * Used for  dtss master-slave sync, master side of relation, as well as dtss/transfer
   * mechanics using subs.
   *
   * Given a subscription_manager (provided by the dtss, or as part of the dstm server ),
   * provide monitoring of changes in the time-series, so that
   * they can be propagated from the master dtss to the slave dtss (usually part of dstm).
   *
   * Typically, this is used in the web-api (part of web-socket context)
   * to keep track of changes to expression/time-series.
   *
   * @note
   * The initial construction of ts_observer snapshots the
   * global change counter, and add_subs also set published version
   * to 0.
   * So prior to any changes of the time-series, with an active sub,
   * there are no changes.
   * The notify change have to appear *after* the subs are
   * added, to make ts_observer detect changes.
   * (e.g: it does not know about changes *prior* to subscription)
   *
   */
  struct ts_observer : observer_base {
    ts_observer(manager_ const & sm, string const & request_id);
    ~ts_observer() override;

    /**
     * @brief add/extend subscriptions for  specified ts_ids  for specified period
     * @details
     * If a single item is already on subscription, extend the period with the new one
     * otherwise, insert it into the subscription manager list, set published version
     * to the initial version for that specific ts
     */
    void subscribe_to(id_vector_t const & ts_ids, utcperiod const & read_period);

    /**
     * @brief remove subscriptions for the specified ts_ids
     * @details
     * The supplied ts-ids are removed from the subscription manager list
     * if it not monitored by any other observer. It is removed from the
     * local published_version list regardless.
     * If item not found, it is a noop (no exception).
     */
    void unsubscribe(id_vector_t const & ts_ids);

    size_t size() const { return published_versions.size();}

    /** @brief recalculate definition, that in this case is a noop */
    bool recalculate() override;

    /**
     * @brief find changed ts_ids that are changed since last check
     * @details
     * Searches through the published_version  list, and compares
     * the published version against the terminal version (as managed by the subscription manager).
     * If it find an item changed, then it is inserted into the unordered_map
     * into individual lists for each of the periods that is changed.
     *  -AND- its published version is set to the current terminal version.
     * The idea is to produce a set of lists suitable for reading, so that
     * the read results can be propagated to the slave dtss in a msync relation.
     * A second call to find_changed_ts() would return an empty list.
     * The cost of the call is quite high, since it needs to loop through the
     * hashed set of time-series. For an empty case, no allocs occur.
     */
    unordered_map<utcperiod, id_vector_t, utcperiod_hasher> find_changed_ts();
    unordered_map<utcperiod, std::vector<ts_o*>, utcperiod_hasher> find_changed_ts_o();

   private:
    using observer_base::has_changed;
    using observer_base::recalculate;
    unordered_map<std::string, ts_o> published_versions; ///< since we have a strong promise to only send if it is
                                                         ///< changed we need to keep the last values
    std::int64_t global_change_version{0l};              ///< keep a snapshot of last ever change,
                                                         ///< to save cpu/lookup during no change conditions
  };

}
