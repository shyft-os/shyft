/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <functional>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::dtss {
  using gts_t = time_series::point_ts<time_axis::generic_dt>;

  /**
   * @brief url_ts_frag
   * @details
   * Just a simple POD time-series fragment with the url,
   * that serializes without boost serialize tracking, thus fast with many(mill) items!
   * Used in the context of storing/transferring concrete/terminal ts.
   * Intended/original use is between dtss client/server (not exposed)
   */
  struct url_ts_frag {
    std::string url; ///< the url of the ts fragment, like shyft://xyz/some.ts
    gts_t ts;        ///< the ts fragment (might replace/extend/modify the target)
    auto operator<=>(url_ts_frag const &) const = default;
    x_serialize_decl();
  };

  time_series::dd::ats_vector from_fragment(std::vector<url_ts_frag> fragments);
  std::vector<url_ts_frag> to_fragments(time_series::dd::ats_vector tsv);

}
x_serialize_export_key(shyft::dtss::url_ts_frag)