/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <ranges>
#include <string>
#include <unordered_set>
#include <vector>

#include <dlib/logger.h>
#include <fmt/core.h>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/master_slave_sync.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::dtss {

  dlib::logger master_slave_sync::dlog{"msync"};

  using shyft::time_series::dd::apoint_ts;

  using shyft::core::from_seconds;
  using shyft::core::utctime_now;
  using shyft::core::utcperiod;
  using shyft::core::no_utctime;
  using detail::str_ptr_eq;
  using detail::str_ptr_hash;
  using c_string_hash = std::unordered_set<std::string const *, str_ptr_hash, str_ptr_eq>;

  namespace {
    struct subs_fixer {
      master_slave_sync &mss;
      std::size_t reconnect_count{0};

      subs_fixer(master_slave_sync &mss)
        : mss{mss} {
        reconnect_count = mss.master->reconnect_count();
      }

      ~subs_fixer() {
        if (reconnect_count != mss.master->reconnect_count())
          mss.dlog << dlib::LERROR
                   << fmt::format(
                        "connection loss detected, reconnect_count: {} -> {}",
                        reconnect_count,
                        mss.master->reconnect_count()); // notify when detected
      }
    };
  }

  master_slave_sync::master_slave_sync(
    server_state &slf,
    std::string ip,
    int port,
    double master_poll_time,
    std::size_t unsubscribe_min_threshold,
    double unsubscribe_max_delay,
    version_type protocol_version)
    : state(slf)
    , max_defer_update_time{unsubscribe_max_delay}
    , unsubscribe_minimum_count{unsubscribe_min_threshold}
    , master_poll_time(master_poll_time) {
    master = std::make_unique<client>(fmt::format("{}:{}", ip, port), 1000, protocol_version);
    subs_updated = utctime_now();
    capture_worker = std::async(std::launch::async, [this]() {
      this->worker();
    });
  }

  void master_slave_sync::repair_subs(bool keep_trying) {
    if (state.terminate) {
      return;
    }
    std::scoped_lock _(mx_subs); // we freeze any operation on these until we have recovery?
    dlog << dlib::LWARN << fmt::format("connection lost, attempt to repair {} subscriptions", subs.size());
    std::vector<id_vector_t> r_ts_ids; // keep results here
    std::vector<utcperiod> r_p;

    {
      auto period_partitions = make_same_period_partitions(subs); // [period]->vector<ts_sub_item const*>
      r_ts_ids.reserve(period_partitions.size());
      r_p.reserve(period_partitions.size());
      // fill up what to read:
      auto i = 0u;
      for (auto const &pp : period_partitions) {
        std::vector<std::string> ids;
        ids.reserve(pp.second.size());
        for (auto const sub_item : pp.second)
          ids.emplace_back(sub_item->id);
        r_ts_ids.emplace_back(std::move(ids));
        r_p.emplace_back(pp.first);
        ++i;
      }
    }
    std::vector<msync_read_result> mr;
    mr.reserve(r_ts_ids.size());
    std::vector<std::vector<utcperiod>> tsv_tp;
    tsv_tp.reserve(r_ts_ids.size());

    bool succeeded = false;
    auto t_giveup = utctime_now()
                  + from_seconds(60); // unless keep_trying, this is the time a client fail will retry connect.
    dlog << dlib::LWARN << fmt::format("repair {} subs partitions , try take lock", r_ts_ids.size());
    std::scoped_lock _mio(mx_master); // ensure we own it while trying.
    do {
      try {
        // 2. read  them
        dlog << dlib::LWARN
             << fmt::format("repair {} subs partitions  attempt failed, will retry after 1 sec", r_ts_ids.size());
        if (!r_ts_ids.empty()) {
          // recover all partitions, collect into mr
          mr.clear();
          for (auto i = 0u; i < r_ts_ids.size(); ++i) {
            mr.emplace_back(master->try_read(r_ts_ids[i], r_p[i], true, true)); // read cache and subscribe
          }
        } else {
          auto v = master->get_server_version();
          dlog << dlib::LWARN << fmt::format("repaired for connection to master v {}", v);
        }
        succeeded = true;
      } catch (dlib::socket_error const &se) {
        dlog << dlib::LERROR << fmt::format("repair subs  socket error occurred: {}", se.what());
      } catch (std::exception const &e) {
        dlog << dlib::LERROR << fmt::format("repair subs  other error occurred: {}", e.what());
      } catch (...) {
        dlog << dlib::LERROR << fmt::format("repair subs  unknown error occurred");
      }

      if (!succeeded) {
        dlog << dlib::LWARN
             << fmt::format("repair {} subs partitions attempt failed, will retry after 1 sec", r_ts_ids.size());
        std::this_thread::sleep_for(from_seconds(1.0));
      }
    } while (!state.terminate && !succeeded && (keep_trying || (!keep_trying && utctime_now() < t_giveup)));
    if (!succeeded) {
      dlog << dlib::LERROR << fmt::format("repair subs given up, after many retry");
      return; // or throw??
    }
    dlog << dlib::LWARN << fmt::format("Successfully repaired {} partitions", r_ts_ids.size());
    // 3. re-add successfully read items to cache (ensure that cache is correct)
    state.ts_cache.flush(); // remove cache, we have to assume its invalid
    if (!r_ts_ids.empty()) {
      for (auto i = 0u; i < r_ts_ids.size(); ++i) {
        if (mr[i].errors.empty()) {
          state.ts_cache.add(std::views::zip(r_ts_ids[i], mr[i].result, mr[i].result_tp));
          state.sm->notify_change(r_ts_ids[i]);
        } else { // prune out problematic, not existing, ts from subs
          for (auto e : mr[i].errors)
            subs.erase(&r_ts_ids[i][e.index]);
          state.ts_cache.add(
            std::views::filter(std::views::zip(r_ts_ids[i], mr[i].result, mr[i].result_tp), [](auto &&pack) {
              return static_cast<bool>(std::get<1>(pack)); // checks ts null that is the response for bad ts read
            }));
          state.sm->notify_change(r_ts_ids[i]); // this one is really questionable, as we can not force out sm subs
        }
      }
      // 4. notify about change(can trigger re-evaluate, and we are happy to have it in cache)
    }
    reconnect_count = master->reconnect_count();
  }

  static void add_to_cache_and_notify(master_slave_sync &mss, std::vector<aref_ts> r, std::vector<utcperiod> tp) {
    if (r.size()) {
      id_vector_t ids;
      ids.reserve(r.size());
      vector<ts_frag> tsv; // cache consume ts_frag, so we need to fix that.
      tsv.reserve(r.size());
      auto removed_ts_count = 0u;
      for (auto const &ts : r) {
        if (ts.rep) {
          ids.emplace_back(ts.id);
          tsv.emplace_back(std::move(ts.rep));
        } else {                            // a subs has become invalidated at server, thus
          mss.state.ts_cache.remove(ts.id); // remove it from this cache
          std::scoped_lock _lock(mss.mx_subs);
          mss.subs.erase(&ts.id); // and remove it from subs.
          ++removed_ts_count;
        }
      }
      mss.state.ts_cache.add(std::views::zip(ids, tsv, tp)); // in this case we wanted a move of ts_frag
      mss.state.sm->notify_change(ids);
      if (removed_ts_count)
        mss.dlog << dlib::LWARN << fmt::format("{} time-series appears removed at dtss main server", removed_ts_count);
    }
  }

  namespace detail {
    bool should_skip(master_slave_sync &mss) {
      std::scoped_lock _(mss.mx_subs);
      if (mss.subs.empty()) {
        mss.subs_updated = utctime_now();
        mss.reconnect_count = mss.master->reconnect_count(); // reset the reconnect count to current, as we have nothing
        return true;
      }
      if (utctime_now() - mss.subs_updated < from_seconds(mss.master_poll_time)) {
        // FIXME: if we have a communication loss, should we say false here?
        //        to immediately start recovering the connection and the subs?
        return true;
      }
      return false;
    }

    bool update_subs(master_slave_sync &mss) {

      try {
        std::pair<std::vector<time_series::dd::aref_ts>, std::vector<utcperiod>> r;
        /** ensure master lock is as short as possible */ {
          size_t reconnect_count{0l};
          {
            std::scoped_lock _m(mss.mx_master);
            reconnect_count = mss.master->reconnect_count();
          }
          if (reconnect_count != mss.reconnect_count) { // we might already be 'dead'
            mss.repair_subs(true);                      // connection since last time was lost, need to update
          }
          bool needs_repair{false};
          {
            std::scoped_lock _m(mss.mx_master);
            r = mss.master->read_subscription();
            needs_repair = reconnect_count != mss.master->reconnect_count(); // figure out if we failed here
          }
          if (needs_repair) { // we lost while updating, repair!
            mss.repair_subs(true);
            return false;
          }
        }
        add_to_cache_and_notify(mss, std::get<0>(r), std::get<1>(r));
        // update/extend the (grand) period of the subs with ts.total_period() that  might be larger due to surround
        // read, ref #1233
        for (auto &&ts : std::get<0>(r)) {
          if (ts.rep) // only add valid updates, and extend period with the now read/observed returned period.
            mss.add_subs(std::views::single(std::make_tuple(ts.id, ts.total_period())), ts.total_period());
        }
      } catch (...) {
        mss.repair_subs(true);
        return false;
      }
      return true;
    }

    auto get_subs_to_remove(master_slave_sync &mss, std::size_t subs_to_remove_estimate, c_string_hash &c_hkeys) {
      // assume lock on the subs!
      id_vector_t subs_to_remove;
      subs_to_remove.reserve(subs_to_remove_estimate);
      constexpr auto missing = [](auto &&key, auto &&container) noexcept {
        return container.find(key) == std::end(container);
      };
      auto missing_keys = std::views::transform(
        std::views::filter(
          mss.subs,
          [&](auto const &e) {
            return (missing(e.first, c_hkeys) && missing(*e.first, mss.state.sm->active));
          }),
        [](auto const &e) {
          auto const &[key, _] = e;
          return *key;
        });

      std::scoped_lock sm_lock(mss.state.sm->mx); // notice lock on dtss.sm.mx because we uses dtss.sm.active list
      std::ranges::copy(missing_keys, std::back_inserter(subs_to_remove));
      return subs_to_remove;
    }

    void remove_subs_and_evict_cache(master_slave_sync &mss, std::size_t subs_to_remove_estimate) {
      std::scoped_lock _(mss.mx_subs); // notice lock order subs->master
      auto c_keys = mss.state.ts_cache.get_keys();
      c_string_hash c_hkeys;
      c_hkeys.reserve(c_keys.size());
      for (auto &&e : c_keys)
        c_hkeys.insert(&e);
      {
        auto subs_to_remove = get_subs_to_remove(mss, subs_to_remove_estimate, c_hkeys);
        for (auto const &s : subs_to_remove)
          mss.subs.erase(&s);
        {
          std::scoped_lock _mm(mss.mx_master); // only now we lock master, but keep subs lock!
          try {
            mss.master->unsubscribe(subs_to_remove);
          } catch (dlib::socket_error const &se) {
            mss.dlog << dlib::LERROR << fmt::format("Error during unsubscribe {}", se.what());
          } catch (std::exception const &e) {
            mss.dlog << dlib::LERROR << fmt::format("Error during unsubscribe {}", e.what());
          }
        }
      }
      if (mss.master->reconnect_count() > mss.reconnect_count)
        mss.repair_subs(true);
      // evict cached entries without subscription
      std::erase_if(c_keys, [&](auto const &k) {
        return mss.subs.contains(&k);
      });
      if (!c_keys.empty()) {
        master_slave_sync::dlog
          << dlib::LWARN
          << fmt::format(
               "evicting {} entries from cache due to missing subscription, first series was {}",
               c_keys.size(),
               c_keys.front());
        mss.state.ts_cache.remove(c_keys);
      }
    }
  }

  /**
   * @brief the master slave sync worker thread
   * @details
   * Work in progress:
   * (1) We need to factor out some of the algos here to facilitate testing
   * (2) Parameterization needs to be external, with good defaults
   * (3)  .. possibly with some adaptive adjustment within limits ..
   */
  void master_slave_sync::worker() {

    utctime t_unsub_detected = no_utctime; // time when we detected that we could do some unsubscribe work.
    size_t ev_count = state.ts_cache.get_total_evicted_count() + state.sm->total_unsubscribe_count.load();
    while (!state.terminate) {
      std::this_thread::sleep_for(from_seconds(master_poll_time));
      if (state.terminate)
        break;
      if (detail::should_skip(*this) || !detail::update_subs(*this))
        continue;
      if (state.terminate)
        break;
      auto t_now = utctime_now();
      auto new_ev_count = state.ts_cache.get_total_evicted_count() + state.sm->total_unsubscribe_count.load();
      if (t_unsub_detected == no_utctime && new_ev_count > 0)
        t_unsub_detected = t_now;
      if (
        (new_ev_count <= ev_count + unsubscribe_minimum_count)
        && ((t_unsub_detected == no_utctime) || ((t_now - t_unsub_detected) <= from_seconds(max_defer_update_time))))
        continue;
      detail::remove_subs_and_evict_cache(*this, new_ev_count - ev_count);
      t_unsub_detected = no_utctime; // clear out detection timer
      ev_count = new_ev_count;       // reset water-mark
      subs_updated = utctime_now();  // also mark time when we updated it
    }
  }

  namespace {
    /**
     * @brief do msync io
     * @details
     * Using the master/mx scoped lock,
     * with subscription immediate repair at
     * exit if we detect communication loss.
     *
     */
    template <class FX>
    auto msync_io(master_slave_sync &me, FX &&fx) {
      subs_fixer sf(me);
      /* */ {
        std::scoped_lock _(me.mx_master);
        return fx();
      }
    }
  }

  std::tuple<vector<ts_frag>, vector<utcperiod>, std::vector<read_error>>
    master_slave_sync::try_read(id_vector_t const &ts_urls, utcperiod p, bool use_ts_cached_read) {
    constexpr auto as_utcperiod_range = std::views::transform([](auto &&el) {
      return el ? el->total_period() : utcperiod{};
    });
    auto add_successful_subs = [&](auto const &r) {
      if (r.errors.empty()) {
        add_subs(std::views::zip(ts_urls, r.result | as_utcperiod_range), p);
        return;
      }
      auto successful_reads = std::views::elements<1>(std::views::filter(
        std::views::enumerate(std::views::zip(ts_urls, r.result | as_utcperiod_range)), [&](auto const &elements) {
          auto const &[index, ts_url] = elements;
          return std::ranges::any_of(r.errors, [&](auto const &err) {
            return err.index != static_cast<std::size_t>(index);
          });
        }));
      add_subs(successful_reads, p);
    };

    auto r = msync_io(*this, [&]() {
      return master->try_read(ts_urls, p, use_ts_cached_read, true);
    });
    add_successful_subs(r);
    {
      std::scoped_lock _lock(mx_subs);
      subs_updated = utctime_now();
    } // update timestamp so that worker thread can skip its update cycle.
    add_to_cache_and_notify(*this, r.updates, r.updates_tp); // could remove non valid updates.
    // subscribe/extend subscription period for all read items (assume they are cached, or subscribed, pruned out later,
    // if not)
    for (auto &&ts : r.updates) {
      if (ts.rep) // only add valid updates, and extend period with the now read/observed returned period.
        add_subs(std::views::single(std::make_tuple(ts.id, ts.total_period())), ts.total_period());
    }
    return std::make_tuple(std::move(r.result), std::move(r.result_tp), std::move(r.errors));
  }

  ts_info_vector_t master_slave_sync::find(std::string const &search_expression) {
    return msync_io(*this, [&]() {
      return master->find(search_expression);
    });
  }

  void master_slave_sync::store_ts(ts_vector_t tsv, store_policy p) {
    // consider NOT using msync_io, and subs fixer here, as it does not change any subs
    msync_io(*this, [&]() {
      (void) store(to_fragments(std::move(tsv)), p);
    });
  }

  diags_t master_slave_sync::store(std::vector<url_ts_frag> fragments, store_policy p) {
    return msync_io(*this, [&]() {
      return master->store(std::move(fragments), p);
    });
  }

  void master_slave_sync::merge_store_ts(ts_vector_t const &tsv, bool cache_on_write) {
    return msync_io(*this, [&]() {
      return master->merge_store_ts(tsv, cache_on_write);
    });
  }

  ts_info master_slave_sync::get_ts_info(std::string const &ts_url) {
    return msync_io(*this, [&]() {
      return master->get_ts_info(ts_url);
    });
  }

  void master_slave_sync::remove(std::string const &name) {
    id_vector_t to_remove{name};
    state.ts_cache.remove(to_remove);
    {
      std::scoped_lock _(mx_subs);
      // TODO: forced remove from server.sm ?;
      auto f = subs.find(&name);
      if (f != subs.end()) {
        subs.erase(f);
      }
    }
    return msync_io(*this, [&]() {
      return master->remove(name);
    });
  }

  bool master_slave_sync::has_subscription(std::string_view container_url) {
    std::scoped_lock _(mx_subs); // assume that we already have a lock on mx_master!
    for (auto const &sub : subs) {
      std::string_view sub_url = *sub.first;
      if (sub_url.starts_with(container_url)) {
        return true;
      }
    }
    return false;
  }

  geo::geo_ts_matrix master_slave_sync::geo_evaluate(geo::eval_args const &ea, bool use_cache, bool update_cache) {
    return msync_io(*this, [&]() {
      return master->geo_evaluate(ea, use_cache, update_cache);
    });
  }

  void master_slave_sync::geo_store(
    std::string const &geo_db_name,
    geo::ts_matrix const &tsm,
    bool replace,
    bool update_cache) {
    msync_io(*this, [&]() {
      master->geo_store(geo_db_name, tsm, replace, update_cache);
    });
  }

  vector<geo::ts_db_config_> master_slave_sync::get_geo_ts_db_info() {
    return msync_io(*this, [&]() {
      return master->get_geo_ts_db_info();
    });
  }

  void master_slave_sync::update_geo_db(
    string const &geo_db_name,
    string const &description,
    string const &json,
    string const &origin_proj4) {
    msync_io(*this, [&]() {
      master->update_geo_ts_db(geo_db_name, description, json, origin_proj4);
    });
  }

  void master_slave_sync::add_geo_ts_db(geo::ts_db_config_ const &gdb) {
    msync_io(*this, [&]() {
      master->add_geo_ts_db(gdb);
    });
  }

  void master_slave_sync::remove_geo_ts_db(std::string const &geo_db_name) {
    msync_io(*this, [&]() {
      master->remove_geo_ts_db(geo_db_name);
    });
  }

  void
    master_slave_sync::set_container(std::string const &name, std::string const &path, std::string type, db_cfg cfg) {
    msync_io(*this, [&]() {
      master->set_container(name, path, type, cfg);
    });
  }

  void master_slave_sync::remove_container(std::string const &container_url, bool remove_from_disk) {
    if (has_subscription(container_url)) {
      throw std::runtime_error(
        fmt::format(
          "Trying to remove container {} that has active subscriptions on dtss master server", container_url));
    }
    msync_io(*this, [&]() {
      master->remove_container(container_url, remove_from_disk);
    });
  }

  void master_slave_sync::swap_container(std::string const &a, std::string const &b) {
    msync_io(*this, [&]() {
      master->swap_container(a, b);
    });
  }

}
