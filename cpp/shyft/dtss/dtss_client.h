/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <concepts>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <ranges>
#include <string>
#include <type_traits>
#include <vector>

#include <dlib/misc_api.h>

#include <shyft/core/dlib_utils.h>
#include <shyft/core/protocol.h>
#include <shyft/dtss/db_cfg.h>
#include <shyft/dtss/diagnostics.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_error.h>
#include <shyft/dtss/exchange/protocol.h>
#include <shyft/dtss/geo.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/protocol/messages.h>
#include <shyft/dtss/queue_msg.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/url_ts_frag.h>
#include <shyft/srv/fast_iosockstream.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/geo_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>

namespace shyft::dtss {

  struct msync_read_result {
    std::vector<ts_frag> result;
    std::vector<core::utcperiod> result_tp;
    std::vector<time_series::dd::aref_ts> updates;
    std::vector<core::utcperiod> updates_tp;
    std::vector<read_error> errors;
  };

  /** @brief a dtss client
   *
   * @details
   * This class implements the client side functionality of the dtss client-server.
   *
   * A client can connect to 1..n dtss and distribute the calculations
   * among these.
   * Precondition is that these servers are equally configured
   * for the operations performed, e.g. same containers etc.
   * (Alt: that they are just caching proxies, slaves of the primary)
   * For write operations, the first connection in srv_con is used.
   *
   */

  struct client {
    version_type version = internal_version;
    std::vector<protocols::basic_client<protocols::io_with_retry>> srv_con;

    bool compress_expressions{true}; ///< compress expressions to gain speed

    client(
      std::string const &host_port,
      int timeout_ms = 1000,
      version_type v = internal_version,
      int operation_timeout_ms = 0);

    client(
      std::vector<std::string> const &host_ports,
      int timeout_ms,
      version_type v = internal_version,
      int operation_timeout_ms = 0);

    [[nodiscard]] std::size_t reconnect_count() const;
    void reopen(int timeout_ms = 1000);
    void close(int timeout_ms = 1000);

    int get_operation_timeout() const {
      if (srv_con.empty())
        return 0;
      return srv_con[0].connection.operation_timeout_ms;
    }

    void set_operation_timeout(int operation_timeout_ms) {
      for (auto &c : srv_con)
        c.connection.operation_timeout_ms = operation_timeout_ms;
    }

    void configure_retry_policies(
      std::size_t max_reconnect,
      std::size_t max_retry,
      core::utctime delay_before_new_attempt);

    std::string get_server_version();
    std::array<version_type, 2> get_supported_protocols();
    [[nodiscard]] std::vector<ts_info> find(std::string search_expression);
    [[nodiscard]] ts_info get_ts_info(std::string ts_url);

    [[nodiscard]] std::vector<time_series::dd::apoint_ts> percentiles(
      time_series::dd::ats_vector tsv,
      core::utcperiod p,
      time_axis::generic_dt ta,
      std::vector<std::int64_t> percentile_spec,
      bool use_ts_cached_read,
      bool update_ts_cache);
    [[nodiscard]] std::vector<time_series::dd::apoint_ts> evaluate(
      time_series::dd::ats_vector tsv,
      core::utcperiod p,
      bool use_ts_cached_read,
      bool update_ts_cache,
      core::utcperiod clip_result = utcperiod{});

    [[nodiscard]] diags_t store(std::vector<url_ts_frag> fragments, store_policy p);
    [[nodiscard]] diags_t store_ts(time_series::dd::ats_vector tsv, store_policy p);
    void merge_store_ts(time_series::dd::ats_vector tsv, bool cache_on_write);
    void remove(std::string name);

    void cache_flush();
    [[nodiscard]] cache_stats get_cache_stats();

    [[nodiscard]] msync_read_result
      try_read(std::vector<std::string> ts_urls, core::utcperiod p, bool use_ts_cached_read, bool subscribe);
    [[nodiscard]] std::pair<std::vector<time_series::dd::aref_ts>, std::vector<core::utcperiod>> read_subscription();
    void unsubscribe(std::vector<std::string> ts_urls);

    [[nodiscard]] std::vector<std::string> get_container_names();
    void set_container(std::string name, std::string rel_path, std::string type = "ts_db", db_cfg dc = {});
    void remove_container(std::string container_url, bool remove_from_disk = false);
    void swap_container(std::string container_name_a, std::string container_name_b);

    [[nodiscard]] geo::geo_ts_matrix geo_evaluate(geo::eval_args ea, bool use_cache, bool update_cache);
    void geo_store(std::string geo_db_name, geo::ts_matrix tsm, bool replace, bool update_cache);
    [[nodiscard]] std::vector<std::shared_ptr<geo::ts_db_config>> get_geo_ts_db_info();
    void add_geo_ts_db(std::shared_ptr<geo::ts_db_config> gdb);
    void remove_geo_ts_db(std::string geo_db_name);
    void update_geo_ts_db(std::string geo_db_name, std::string description, std::string json, std::string origin_proj4);

    [[nodiscard]] std::vector<std::string> q_list(); ///< provide a list of all available queues
    [[nodiscard]] queue::msg_info q_msg_info(std::string q_name, std::string msg_id);
    [[nodiscard]] std::vector<queue::msg_info> q_msg_infos(std::string q_name);
    void q_put(
      std::string q_name,
      std::string msg_id,
      std::string descript,
      core::utctime ttl,
      time_series::dd::ats_vector tsv);
    [[nodiscard]] std::shared_ptr<queue::tsv_msg> q_get(std::string q_name, core::utctime max_wait);
    [[nodiscard]] std::shared_ptr<queue::tsv_msg> q_find(std::string q_name, std::string msg_id);
    void q_ack(std::string q_name, std::string msg_id, std::string diag);
    [[nodiscard]] size_t q_size(std::string q_name);
    void q_add(std::string q_name);
    void q_remove(std::string q_name);
    void q_maintain(std::string q_name, bool keep_ttl_items, bool flush_all = false);

    void start_transfer(transfer::configuration cfg);
    [[nodiscard]] std::vector<transfer::configuration> get_transfers();
    [[nodiscard]] transfer::status get_transfer_status(std::string name, bool clear_status);
    void stop_transfer(std::string name, utctime max_wait);

    void start_q_bridge(q_bridge::configuration cfg);
    [[nodiscard]] std::vector<q_bridge::configuration> get_q_bridges();
    [[nodiscard]] q_bridge::status get_q_bridge_status(std::string name, bool clear_status);
    void stop_q_bridge(std::string name, utctime max_wait);

    template <version_type V, messages<V>::tags tag>
    using request = protocols::request<protocols::versioned_message<protocol, V>{tag}>;

    template <typename return_type>
    auto with_version(auto &&action) {
      return protocols::with_version_or<protocol>(
        version,
        [&](auto version) -> return_type {
          using tags = messages<version>::tags;
          return action.template operator()<version, tags>();
        },
        [](auto error) -> return_type {
          auto error_message = [&] {
            if constexpr (get_error_tag(error) == protocols::protocol_stream_error_tag::invalid_version)
              return fmt::format("Version {} not supported by client", error.read_version);
            else
              return fmt::format("invalid error tag");
          }();
          throw std::runtime_error(error_message);
        });
    }
  };

} // shyft::dtss
