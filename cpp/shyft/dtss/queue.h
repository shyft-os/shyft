#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <cstdint>
#include <future>
#include <memory>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

#include <shyft/dtss/queue_msg.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

namespace shyft::dtss::queue {

  /**
   * @brief a queue for time-series vector messages
   * @details
   * A nonblocking, almost endless sized fifo-queue for time-series messages.
   * The producers put messages into the queue with enforced unique message-ids,
   * and the consumers gets these messages out of the queue, and acknowledge
   * the message by a 'done' when consumed.
   * The life-cycle for a message is as follows:
   * producer -> put    ; state=created,  .created=utctime_now() , placed in the queue and in the q.msg-map.
   * consumer -> try_get; state=fetched,  .fetched=utctime_now() , removed from the queue,(but still i q.msg-map)
   * consumer -> done   ; state=done,     .done=utctime_now()    , .. still in the q.msg-map ...
   * maintenance: if created+ttl < tnow: removed from the q.msg-map -> destroyed
   *
   */
  struct tsv_queue {

    std::string const name;

    explicit tsv_queue(std::string name = "")
      : name{std::move(name)} {
    }

    /**
     * @brief get all message infos in the queue
     * @returns a list of msg_info objects, new, fetched and done messages.
     */
    std::vector<msg_info> get_msg_infos() const;

    /**
     * @brief get info for a specific msg_id
     * @param msg_id of the wanted message
     * @returns msg_info for msg_id, or throws if it's not there
     */
    msg_info get_msg_info(std::string const &msg_id) const;

    /**
     * @brief put message into the queue
     * @details
     * Put message into the queue with the specified info and payload.
     * The message info.created gets the time-stamp(currently down to micro second) of entering the queue.
     * @param msg_id unique id for the current scope of living messages in the queue
     * @param descript any description, json recommended
     * @param ttl time to live for the message, if 'done' it will be automatically removed after create+ttl.
     * @param tsv the time-series vector payload. Usually this is a bound/evaluated ts-vector.
     * @throws runtime_error if msg_id already exists in the queue
     */
    void put(std::string const &msg_id, std::string const &descript, utctime ttl, ats_vector const &tsv);

    /**
     * @brief try get a message from the queue
     * @details
     * If not queue empty, the first available element is fetched out, and the message info.fetched is set to
     * utctime_now() A shared pointer is returned, so that the message is still within the queue, but in the 'fetched'
     * state.
     * @returns nullptr or first available queue message.
     */
    std::shared_ptr<tsv_msg const> try_get();

    std::shared_ptr<tsv_msg const> find(std::string const &msg_id) const;

    std::size_t get_full_size() const;

    /** @brief returns the current active(just created) elements waiting in the queue */
    std::size_t size() const;

    /**
     * @brief consumer mark the message as done
     * @details
     * Set the message info.done=utctime_now(), and the diagnostics to supplied parameters.
     * The message is now eligible for maintenance according to the ttl strategy for the message.
     * @param msg_id the message id
     * @param diag the diagnostics to be attached to the message(visible to the publisher)
     */
    void done(std::string const &msg_id, std::string const &diag);

    /**
     * @brief clear and reset queue to its empty state
     */
    void reset();

    /**
     * @brief maintain the queue, flush done items
     */
    std::size_t flush_done_items(bool keep_ttl_items, utctime x_now = no_utctime);

   private:
    mutable std::mutex mx;                   ///< mutex to ensure threadsafe access to class members
    std::queue<std::shared_ptr<tsv_msg>> mq; ///< the fifo queue with refs to tsv_msgs
    std::unordered_map<std::string, std::shared_ptr<tsv_msg>> all; ///< key is .info.msg_id, all kept/traced messages.
  };

  /**
   * @brief queue manager
   * @details
   * Provide the needed functionality for providing the dtss with
   * multiple named queues, add/remove, list and access named queues.
   *
   */
  struct q_mgr {

    void add(std::string const &q_name);

    void remove(std::string const &q_name);

    bool exists(std::string const &q_name);

    std::size_t size() const;

    std::vector<std::string> queue_names() const;

    std::shared_ptr<tsv_queue> operator()(std::string const &q_name) const;

   private:
    mutable std::mutex mx;
    std::map<std::string, std::shared_ptr<tsv_queue>> q;
  };
}
