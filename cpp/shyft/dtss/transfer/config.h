#pragma once
#include <string>

#include <dlib/logger.h>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/dtss/diagnostics.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_error.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::dtss::transfer {

  /**
   * @brief configuration
   * @details
   * Contains all features we want to configure
   * related to the ts transfer.
   */
  struct configuration {
    std::string name; ///< for conv. and orchestration from remote
    std::string json; ///< for arbitrary usefulness

    struct time_series_spec {      ///< what ts to transfer, and how to translate
      std::string search_pattern;  ///< like shyft://stm/scada/.*realised
      std::string replace_pattern; ///< applied to the container/concrete_ts_name
      auto operator<=>(time_series_spec const &) const = default;
      SHYFT_DEFINE_STRUCT(time_series_spec, (), (search_pattern, replace_pattern));
      x_serialize_decl();
    };

    struct period_spec {              ///< what time-period of the time-series should be transferred
      core::utctime now_relative{0l}; ///< if 0, spec is absolute, otherwise t.now().trim(now_relative)
      core::utcperiod spec{};         ///< absolute period, or t_now.trim(dt) relative period
      auto operator<=>(period_spec const &) const = default;
      SHYFT_DEFINE_STRUCT(period_spec, (), (now_relative, spec));

      /**
       * @brief period_spec.real(t)->utcperiod
       * @details
       * Interpret the period_spec and compute the real period,
       * also taking into account `t_now` if the spec is
       * t_now relative
       * @param t_now core::utctime_now() to be used as t-now reference
       * @return the real period as of t-now and specifications
       *
       */
      core::utcperiod real([[maybe_unused]] core::utctime t_now) const {
        if (now_relative == core::utctime{0l})
          return spec;             // transfer exactly the spec
        static core::calendar utc; // it is a cost for the utc creation, so static
        auto x_now = utc.trim(t_now, now_relative);
        constexpr auto add_offset = [](core::utctime t, core::utctime dt) {
          return dt > core::min_utctime && dt < core::max_utctime ? t + dt : dt; // only offset for ordinary dt
        };
        return {add_offset(x_now, spec.start), add_offset(x_now, spec.end)};
      }

      x_serialize_decl();
    };

    struct remote_spec { ///< remote party
      std::string host;  ///< ip or name
      std::int64_t port; ///< port
      // maybe protocol binary/web
      auto operator<=>(remote_spec const &) const = default;

      SHYFT_DEFINE_STRUCT(remote_spec, (), (host, port));
      x_serialize_decl();
    };

    struct when_spec {
      shyft::time_axis::generic_dt ta; ///< fixed time-points when to transfer
      bool changed;                    ///< can use subscription, of last time of modify strategy
      core::utctime poll_interval{std::chrono::milliseconds{500l}}; ///< poll for changes (subs) so often
      core::utctime change_linger_time{
        std::chrono::milliseconds{1000l}}; ///< max time to delay a transfer after change detected.
      auto operator<=>(when_spec const &) const = default;
      SHYFT_DEFINE_STRUCT(when_spec, (), (ta, changed, poll_interval, change_linger_time));
      x_serialize_decl();
    };

    struct xfer_spec {
      std::int64_t retries{10u}; ///< when dtss client fails, before giving up attempt
      core::utctime sleep_before_retry{
        core::seconds{1l}};                ///< wait a bit before retries to allow netw/con recovery/restart
      std::uint64_t partition_size{1000u}; ///< split job into partitions of this max-size
      auto operator<=>(xfer_spec const &) const = default;
      SHYFT_DEFINE_STRUCT(xfer_spec, (), (retries, sleep_before_retry, partition_size));
      x_serialize_decl();
    };

    time_series_spec what{};       ///< the regexpr,or similar that gives what to transfer
    remote_spec where{};           ///< remote connection spec goes here.
    period_spec xfer_period{};     ///< related to 'what', the time-period spec.
    bool read_remote{false};       ///< e.g. direction of transfer.
    bool read_updates_cache{true}; ///< let read update the cache
    store_policy write_policy{.recreate = false, .strict = false, .cache = true, .best_effort = true};
    when_spec when{};                            ///< to perform transfer use time-axis, or when.changed
    xfer_spec how{};                             ///< - to repair/fix problems on remote
    version_type protocol_version = min_version; // min_version default, keep compatible as long as possible

    auto operator<=>(configuration const &) const = default;
    SHYFT_DEFINE_STRUCT(
      configuration,
      (),
      (name,
       json,
       what,
       where,
       xfer_period,
       read_remote,
       read_updates_cache,
       write_policy,
       when,
       how,
       protocol_version));
    x_serialize_decl();
  };


}

x_serialize_export_key_nt(shyft::dtss::transfer::configuration::time_series_spec);
x_serialize_export_key_nt(shyft::dtss::transfer::configuration::period_spec);
x_serialize_export_key_nt(shyft::dtss::transfer::configuration::when_spec);
x_serialize_export_key_nt(shyft::dtss::transfer::configuration::remote_spec);
x_serialize_export_key_nt(shyft::dtss::transfer::configuration::xfer_spec);
x_serialize_export_key(shyft::dtss::transfer::configuration);


SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::configuration::time_series_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::configuration::period_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::configuration::when_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::configuration::remote_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::configuration::xfer_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::configuration);
