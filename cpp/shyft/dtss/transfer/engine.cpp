/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <boost/thread/sync_bounded_queue.hpp>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/transfer/config.h>
#include <shyft/dtss/transfer/engine.h>
#include <shyft/dtss/transfer/logger.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::transfer {
  /**
   * @brief scoped_true for atomic bool
   * @details
   * To ensure we have a consistent use of is_writing/reading etc.
   */
  struct scoped_true {
    std::atomic_bool &f;

    explicit scoped_true(std::atomic_bool &f_)
      : f(f_) {
      f.store(true);
    }

    ~scoped_true() {
      f.store(false);
    }

    scoped_true(scoped_true const &) = delete;
    scoped_true &operator=(scoped_true const &) = delete;
  };

  struct queue_pkg {
    // got some problems with valgrind, so
    // trying to enforce exactly how queue_pkg is used for bounded queue (move only)
    queue_pkg(queue_pkg const &) = delete;
    queue_pkg(queue_pkg &&) = default;
    queue_pkg &operator=(queue_pkg const &) = delete;
    queue_pkg &operator=(queue_pkg &&) = default;
    queue_pkg() = default;

    void reset() {
      ts_ids.clear();
      ts_frags.clear();
    }

    queue_pkg(id_vector_t &&n, std::vector<ts_frag> &&v)
      : ts_ids{std::move(n)}
      , ts_frags{std::move(v)} {
    }

    id_vector_t ts_ids;
    std::vector<ts_frag> ts_frags; // ts_frag is a shared ptr. possibly from cache(ref count2), or from db( ref 1),
  };

  using write_queue_t = boost::sync_bounded_queue<queue_pkg>;

  struct engine_impl {
    using id_partition_map_t = std::vector<std::tuple<id_vector_t, subscription::ts_observer>>;
    engine_impl(engine &&) = delete;
    engine_impl(engine const &) = delete;
    engine_impl &operator=(engine &&) = delete;
    engine_impl &operator=(engine const &) = delete;

    server_state &server; ///< the dtss server we are pushing from
    client remote;        ///< the remote dtss server we write to through client
    logger log;           ///< the log we report progress,issues etc. to
    configuration cfg;    ///< the configuration used for the transfer
    // while running:
    // we need the strings ts-urls we read from, and IF we do translation
    // also the target ts-url strings (kept in the to_remote map).
    // so without translation, no map, and no extra string for the destination.
    id_partition_map_t partitions;                               ///< current partitions
    std::unordered_map<std::string_view, std::string> to_remote; // needed to translate partial changes
    utcperiod read_period;                                       ///< current active read period
    std::shared_ptr<write_queue_t> wq; ///< the queue where reader stash its read, and writer fetches what to write
    std::future<void> write_worker;
    std::future<void> read_worker;
    std::atomic_bool terminate{false};    ///< set to signal reader/writer should stop working
    std::atomic_bool is_writing{false};   ///< true while writer writing to remote
    std::atomic_bool is_reading{false};   ///< true while reader is active reading
    std::atomic_bool writer_alive{false}; ///< true while writer alive
    std::atomic_bool reader_alive{false}; ///< true while reader alive

    engine_impl(server_state &dtss, configuration c)
      : server{dtss}
      , remote(fmt::format("{}:{}", c.where.host, c.where.port), 1000, c.protocol_version)
      , cfg{std::move(c)} {
      // notice that we let the client be a bit configurable when it comes to how it retries.
      if (cfg.read_remote)
        throw std::runtime_error(fmt::format("illegal config: read from remote not yet implemented"));
      if (cfg.what.search_pattern.empty())
        throw std::runtime_error(fmt::format("illegal config: search pattern needs to be something"));
      if (cfg.where.host.empty())
        throw std::runtime_error(fmt::format("illegal config: remote host must be specified"));
      if (cfg.where.port <= 0)
        throw std::runtime_error(fmt::format("illegal config: remote port must be specified >0 "));
      if (cfg.when.poll_interval < core::from_seconds(0.001))
        throw std::runtime_error(fmt::format("illegal config: poll interval should be reasonable, like > 1ms"));
      remote.configure_retry_policies(cfg.how.retries, cfg.how.retries, cfg.how.sleep_before_retry);
    }

    ~engine_impl() {
      stop();
    }

    void start() {
      stop(); // any existing
      logger::dlog << dlib::LINFO << "Starting transfer: " << cfg.name;
      terminate = false;
      wq = std::make_shared<write_queue_t>(2); // two items max in queue, reader will wait for writer to flush
      write_worker = std::async(std::launch::async, [&]() {
        writer();
      });
      read_worker = std::async(std::launch::async, [&]() {
        reader();
      });
    }

    void stop(utctime graceful_close = utctime{0l}) {
      terminate = true; // signal we want to stop
      if (wq) {
        logger::dlog << dlib::LINFO << "Stopping transfer: " << cfg.name;
        if (graceful_close > utctime{0l}) {
          utctime const dt{core::from_seconds(0.001)}; // 1ms sleep steps
          while (((!wq->closed() && !wq->empty()) || (is_writing || is_reading)) && graceful_close > utctime{0l}) {
            std::this_thread::sleep_for(dt);
            graceful_close -= dt;
          }
        }
        if (!wq->closed())
          wq->close();
      }
      if (write_worker.valid())
        write_worker.get();
      if (read_worker.valid())
        read_worker.get();
      if (wq)
        wq.reset();
      remote.close();     // will auto open later if re-used.
      to_remote.clear();  // remove views,
      partitions.clear(); // then contents
    }

    bool is_running() const {
      return (wq && !wq->closed()) || is_writing || is_reading;
    }

    bool should_terminate() const {
      return terminate || server.terminate;
    }

    bool find_ts_to_transfer() {
      read_period = cfg.xfer_period.real(core::utctime_now());
      partitions.clear(); // generate fresh partitions of time-series to transfer
      auto ts_infos = do_find_ts(server, cfg.what.search_pattern);
      if (ts_infos.empty()) {
        logger::dlog << dlib::LWARN << fmt::format("Found no timeseries matching '{}'", cfg.what.search_pattern);
        log.find_completed(0);
        return false;
      }
      log.find_completed(ts_infos.size());
      logger::dlog << dlib::LINFO
                   << fmt::format("Found {} timeseries matching {}", ts_infos.size(), cfg.what.search_pattern);
      logger::dlog << dlib::LINFO << fmt::format("Transfer period evaluated to {}", read_period);

      auto const shyft_container = extract_shyft_url_container(cfg.what.search_pattern);
      auto url_prefix_container = "shyft://" + shyft_container;
      // if external.non shyft, the find returns fully qualified urls
      // for internal, the un-prefixed name is returned
      if (cfg.how.partition_size == 0) // interpret 0 as no partitions, or default based on estimated size?
        cfg.how.partition_size = ts_infos.size();
      partitions.reserve(1 + ts_infos.size() / cfg.how.partition_size);
      for (std::size_t i = 0; i < ts_infos.size(); i += cfg.how.partition_size) {
        id_vector_t from_ts_ids;
        for (std::size_t j = i; j < std::min<std::size_t>(i + cfg.how.partition_size, ts_infos.size()); ++j) {
          from_ts_ids.push_back(
            shyft_container.empty() ? ts_infos[j].name : fmt::format("{}/{}", url_prefix_container, ts_infos[j].name));
          if (!cfg.what.replace_pattern.empty()) {
            std::regex re_match(cfg.what.search_pattern);
            auto const s = std::regex_replace(
              from_ts_ids.back(), re_match, cfg.what.replace_pattern, std::regex_constants::match_default);
            to_remote[from_ts_ids.back()] = s;
          }
        }
        subscription::ts_observer subs{server.sm, "transfer"};
        if (cfg.when.changed) // only subs work if asked for
          subs.subscribe_to(from_ts_ids, read_period);
        partitions.emplace_back(std::move(from_ts_ids), std::move(subs));
      }
      return true;
    }

    void reader() {
      scoped_true alive{reader_alive};

      auto const mwq = wq; // keep alive shared ptr while reader use it
      // until we find something to transfer, we wait in intervals of 1sec
      while (!find_ts_to_transfer() && !should_terminate())
        std::this_thread::sleep_for(cfg.how.sleep_before_retry + core::from_seconds(0.001));

      bool first_time{true}; // using subs, the first time read must be enforced
      while (mwq && !mwq->closed() && !should_terminate()) {
        for (auto &partition : partitions) {
          scoped_true _(is_reading);
          auto const &from_ts_ids = std::get<0>(partition);
          auto &subs = std::get<1>(partition); // subs are modified.
          std::unordered_map<utcperiod, id_vector_t, core::utcperiod_hasher> change_map;
          if (first_time)
            change_map[read_period] = id_vector_t{from_ts_ids}; // make a copy, so we can move below
          else
            change_map = subs.find_changed_ts(); // will only detect changes after subs are added!
          for (auto const &[changed_period, changed_tsids] : change_map) {
            if (should_terminate()) {
              mwq->close(); // close queue exit
              return;
            }
            auto t_b = core::utctime_now();
            std::tuple<vector<ts_frag>, vector<utcperiod>, vector<read_error>> vpe;
            vpe = try_read(server, changed_tsids, changed_period, true, cfg.read_updates_cache);

            auto &[r_tsv, r_tp, r_errors] = vpe;
            auto const t_used = core::to_seconds(core::utctime_now() - t_b);
            if (!r_tsv.empty()) {
              std::size_t pts = 0u;
              for (auto const &ts : r_tsv)
                pts += ts ? ts->size() : 0u;
              log.read_completed(t_used, pts);
              try {
                // we could have a std::vector<std::string const&>
                // since that is the only requirement for the writer *and* we guarantee that lifetime of the ref
                // are one of the to_remote[view]->string, or the changed set.
                id_vector_t to_ids;
                to_ids.reserve(changed_tsids.size());
                if (!to_remote.empty()) { // only translate if needed
                  for (auto const &f_id : changed_tsids)
                    to_ids.emplace_back(to_remote[f_id]);
                } else {
                  for (auto &f_id : changed_tsids)
                    to_ids.emplace_back(f_id); // ok! its acopy
                }
                mwq->push_back(queue_pkg(std::move(to_ids), std::move(r_tsv)));
              } catch (boost::sync_queue_is_closed const &) {
                return; // queue was closed, it is a stop order.
              }
            }
            if (!r_errors.empty()) { // notice that if we get a stop order, on push : we do not report this
              for (auto const &[idx, code] : r_errors) {
                log.read_error(from_ts_ids[idx], code);
              }
            }
          }
        }
        first_time = false; // for the entire loop above, partitions.

        if ((cfg.when.ta.empty() && !cfg.when.changed) || should_terminate())
          break; // it was a one-shot
        std::this_thread::sleep_for(cfg.when.poll_interval);
      }
      if (mwq && !mwq->closed())
        mwq->close(); // signal end of story
    }

    void writer() {
      scoped_true alive{writer_alive};
      queue_pkg pkg;
      auto const mwq = wq; // keep own ref alive
      while (mwq && boost::queue_op_status::closed != mwq->wait_pull_front(pkg) && !should_terminate()) {
        if (!pkg.ts_ids.empty()) {
          if (pkg.ts_ids.size() != pkg.ts_frags.size())
            throw std::runtime_error("program error");
          do {
            try {
              scoped_true _{is_writing};
              std::size_t pts = 0u;
              for (auto const &ts : pkg.ts_frags)
                pts += ts ? ts->size() : 0u;
              auto t_b = core::utctime_now();
              gts_t empty_ts;
              std::vector<url_ts_frag> fragments(pkg.ts_ids.size());
              std::ranges::copy(
                std::views::transform(
                  std::views::zip(pkg.ts_ids, pkg.ts_frags),
                  [&](auto &&item) {
                    auto &[ts_id, ts_frag] = item;
                    if (ts_frag)
                      return url_ts_frag{ts_id, ts_frag->rep};
                    return url_ts_frag{ts_id, empty_ts};
                  }),
                fragments.data());
              auto diags = remote.store(std::move(fragments), cfg.write_policy);
              log.write_completed(core::to_seconds(core::utctime_now() - t_b), pts);
              if (diags.has_value()) { // just record/log what individuals failed
                for (auto const &[ix, diag] : *diags)
                  log.write_error(pkg.ts_ids[ix], diag);
              }
              pkg.reset(); //{};// clear out mem? something very wrong somewhere.
              break;       // success at least storing something remote, take next pkg.
            } catch (std::exception const &e) {
              log.remote_error(e.what()); // in this case we do not know how much was stored, assume nothing
            }
            std::this_thread::sleep_for(core::from_seconds(0.1)); // wait a bit before retry
          } while (true);
        } /// empty, is a no-op
      }
    }
  };

  //---

  engine::engine(server_state &state, configuration cfg)
    : pimpl{std::make_unique<engine_impl>(state, std::move(cfg))} {
  }

  engine::~engine() = default;

  void engine::start() {
    pimpl->start();
  }

  void engine::stop(utctime graceful_close) {
    pimpl->stop(graceful_close);
  }

  bool engine::is_running() const {
    return pimpl->is_running();
  }

  logger &engine::get_logger() const {
    return pimpl->log;
  }

  configuration const &engine::get_configuration() const {
    return pimpl->cfg;
  }

  status engine::get_status(bool clear) {
    status r;
    auto r_e = pimpl->log.get_read_errors(clear);
    auto w_e = pimpl->log.get_write_errors(clear);
    auto io_e = pimpl->log.get_remote_errors(clear);
    for (auto const &[ts_url, code] : r_e)
      r.read_errors.emplace_back(ts_url, code);
    for (auto const &[ts_url, code] : w_e)
      r.write_errors.emplace_back(ts_url, code);
    if (!io_e.empty())
      r.remote_errors.emplace_back(io_e);
    r.writer_alive = pimpl->writer_alive;
    r.reader_alive = pimpl->reader_alive;
    r.last_activity = pimpl->log.last_activity.load();
    auto stats = pimpl->log.get_statistics(clear);
    constexpr double u_zero = 1e-12; // assume io takes longer than this
    r.read_speed = stats.read_time_used > u_zero ? static_cast<double>(stats.total_read) / stats.read_time_used : 0.0;
    r.write_speed =
      stats.write_time_used > u_zero ? static_cast<double>(stats.total_written) / stats.write_time_used : 0.0;
    r.total_transferred = stats.total_written;
    r.n_ts_found = stats.n_ts_found;
    return r;
  }

  engine::engine(engine &&) noexcept = default;
  engine &engine::operator=(engine &&) noexcept = default;


}