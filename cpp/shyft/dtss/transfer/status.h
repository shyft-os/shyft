/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>

#include <shyft/dtss/diagnostics.h>
#include <shyft/dtss/dtss_error.h>

namespace shyft::dtss::transfer {

  /**
   * @brief transfer status
   * @details
   * Information to transfer to client/observers of transfers
   * Errors are of interest, and later also statistics
   */
  struct status {

    struct read_error {
      std::string ts_url;
      lookup_error code;
      auto operator<=>(read_error const &) const = default;
      SHYFT_DEFINE_STRUCT(read_error, (), (ts_url, code));
      x_serialize_decl();
    };

    struct write_error {
      std::string ts_url;
      ts_diagnostics code;
      auto operator<=>(write_error const &) const = default;
      SHYFT_DEFINE_STRUCT(write_error, (), (ts_url, code));
      x_serialize_decl();
    };

    std::vector<read_error> read_errors;

    std::vector<write_error> write_errors;

    std::vector<std::string> remote_errors;

    bool reader_alive{false};
    bool writer_alive{false};
    double read_speed{0.0};            ///< points/sec
    double write_speed{0.0};           ///< points/sec
    std::int64_t total_transferred{0}; ///< points
    std::size_t n_ts_found{0u};        ///< number of ts found, that is part of transfer set, 0 until finding something

    core::utctime last_activity; ///< read or write (attempt)

    auto operator<=>(status const &) const = default;
    // statistics !

    SHYFT_DEFINE_STRUCT(
      status,
      (),
      (read_errors,
       write_errors,
       remote_errors,
       reader_alive,
       writer_alive,
       read_speed,
       write_speed,
       total_transferred,
       n_ts_found,
       last_activity));
    x_serialize_decl();
  };
}

x_serialize_export_key_nt(shyft::dtss::transfer::status::write_error);
x_serialize_export_key_nt(shyft::dtss::transfer::status::read_error);
x_serialize_export_key(shyft::dtss::transfer::status);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::status::write_error);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::status::read_error);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::transfer::status);