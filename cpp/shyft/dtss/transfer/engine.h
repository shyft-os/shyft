/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <string>

#include <shyft/dtss/transfer/config.h>
#include <shyft/dtss/transfer/status.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss {
  struct server_state; // fwd
}

namespace shyft::dtss::transfer {

  struct logger;      // fwd
  struct engine_impl; // fwd to impl so we get compile speed

  /**
   * @brief transfer engine
   *
   * @details
   * Currently implementing push transfer from one
   * dtss server to a remote via the dtss/client api.
   * The transfer can be configured through the
   * transfer::configuration.
   * The initial version aims at regexpr, oneshot and subscription
   * based transfer.
   * Later we can add other transfers,
   * and also enable bi-directional transfer
   *
   * @note
   * The dtss::server in constructor must outlive the lifetime of the engine.
   * The engine tests for dtss::server.terminate at regular/configured intervals,
   * and will self terminate internal threads.
   * The IO operations involved could take some time, so preferably this should
   * be done using the stop(graceful_close=some-suitable-time) to ensure clean
   * shutdown.
   */
  struct engine {

    /**
     * Construct engine with supplied parameters,
     * stuff might be validated, but no action is taken until .start()
     * is run.
     */
    engine(server_state &state, transfer::configuration cfg);
    ~engine();

    // no cp/assign
    engine(engine const &) = delete;
    engine &operator=(engine const &) = delete;
    // move only
    engine(engine &&) noexcept;
    engine &operator=(engine &&) noexcept;

    /**
     * @brief start transfer
     *
     */
    void start();

    /**
     * @brief stop (any) transfer activity
     *
     */
    void stop(core::utctime graceful_close = core::utctime{0l});

    status get_status(bool clear);

    logger &get_logger() const;

    configuration const &get_configuration() const;

    /**
     * @brief is_running
     * @return true if there is ongoing activity
     *
     */
    bool is_running() const;

   private:
    std::unique_ptr<engine_impl> pimpl;
  };
};