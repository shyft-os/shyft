/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>

#include <dlib/logger.h>

#include <shyft/dtss/diagnostics.h>
#include <shyft/dtss/dtss_error.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::transfer {

  /**
   * @brief logger
   * @details
   * Can log events (using shyft std logging),
   * as well as keep last observed problem
   * so that we can query the service for its current
   * state regarding problem items.
   * Also possible to just count communication errors
   * etc.
   */
  struct logger {
    static dlib::logger dlog;
    // fx to dlib logging (configurable)
    // maybe also:
    // vector<std::tuple<utctime,string>> remote_errors; ///< record of all comm errors
    std::mutex w_mx;
    std::unordered_map<std::string, ts_diagnostics> write_errors; ///< record of latest write error pr. ts

    std::mutex r_mx;
    std::unordered_map<std::string, lookup_error> read_errors; ///< record of latest read error pr. ts

    std::mutex mx;
    std::string remote_diag;

    std::atomic<core::utctime> last_activity{core::no_utctime};

    struct statistics {
      std::int64_t n_ts_found{0};
      std::int64_t write_cycles{0};
      std::int64_t read_cycles{0};
      std::int64_t total_read{0l};
      std::int64_t total_written{0l};
      double read_time_used{0.0};
      double write_time_used{0.0};
    };

    statistics stats;

    void write_error(std::string_view ts_url, ts_diagnostics diagnostics) {
      std::scoped_lock _(w_mx);
      write_errors[std::string(ts_url)] = diagnostics;
      // pipe out to dlib logger here
      dlog << dlib::LWARN << fmt::format("Failed to write to '{}' diagnostics code:{}", ts_url, diagnostics);
    }

    void read_error(std::string_view ts_url, lookup_error error_code) {
      std::scoped_lock _(r_mx);
      read_errors[std::string(ts_url)] = error_code;
      dlog << dlib::LERROR << fmt::format("Failed to read from '{}' diagnostics code:{}", ts_url, error_code);
      // pipe out to dlib logger here
    }

    void remote_error(std::string_view diag) {
      std::scoped_lock _(mx);
      remote_diag = std::string(diag);
      // pipe out to dlib logger here
      dlog << dlib::LWARN << fmt::format("Remote connection error:{}", diag);
    }

    auto get_read_errors(bool clear_it) {
      std::scoped_lock _(r_mx);
      auto r = read_errors;
      if (clear_it)
        read_errors.clear();
      return r;
    }

    auto get_write_errors(bool clear_it) {
      std::scoped_lock _(w_mx);
      auto r = write_errors;
      if (clear_it)
        write_errors.clear();
      return r;
    }

    statistics get_statistics(bool clear_it) {
      statistics r;
      {
        std::scoped_lock safe_read(r_mx);
        std::scoped_lock safe_write(w_mx);
        r = stats;
        if (clear_it)
          stats = {};
      }
      return r;
    }

    auto get_remote_errors(bool clear_it) {
      std::scoped_lock _(mx);
      auto r = remote_diag;
      if (clear_it)
        remote_diag = "";
      return r;
    }

    void find_completed(std::size_t n_ts) {
      std::scoped_lock _(r_mx);
      stats.n_ts_found = n_ts;
      last_activity = core::utctime_now();
    }

    void read_completed(double rd_time_used, size_t points_read) {
      {
        std::scoped_lock _(r_mx);
        ++stats.read_cycles;
        stats.read_time_used += rd_time_used;
        stats.total_read += points_read;
      }
      last_activity = core::utctime_now();
      if (dlog.level() >= dlib::LTRACE)
        dlog << dlib::LTRACE << fmt::format("read completed: {} pts {} s used", points_read, rd_time_used);
    }

    void write_completed(double write_time_used, size_t points_written) {
      {
        std::scoped_lock _(w_mx);
        ++stats.write_cycles;
        stats.write_time_used += write_time_used;
        stats.total_written += points_written;
      }
      last_activity = core::utctime_now();
      if (dlog.level() >= dlib::LTRACE)
        dlog << dlib::LTRACE << fmt::format("write completed: {} pts {} s used", points_written, write_time_used);
    }
  };
}