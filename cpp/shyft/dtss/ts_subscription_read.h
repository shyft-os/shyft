/** This file is part of Shyft. Copyright 2015-2024 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/dtss/ts_subscription.h>
#include <shyft/time_series/dd/aref_ts.h>
#pragma once
namespace shyft::dtss::subscription {

  using ts_frag=shared_ptr<gpoint_ts const>; ///< ts frags are immutables
  /**
   * @brief a callable to read time-series and their current total period
   * @details
   *
   * Provides slightly clearer interface to the read_changed_subs
   *
   * @tparam FX
   */
  template <typename FX>
  concept read_time_series_callable = requires(FX f, id_vector_t const & ids, utcperiod const & p) {
    { f(ids, p) } -> std::convertible_to<std::pair<std::vector<ts_frag>, std::vector<utcperiod>>>;
  };

  /**
   * @brief read_changed_subs
   * @details
   * refresh the subs, earlier registered with subs.subscribe_to(ts_ids,p),
   * that are different from last observation.
   * Notice that the change is any write change to the time-series,
   * that could imply re-create(with potentially other time-axis etc. defs)
   * If the read operation ends with surrounding read, a period larger than asked for,
   * then update the subs ts_o.p, period object to cover the larger period.
   * @note
   * We could rework this function to something that applies a
   * function to the changed items, including read/stash result.
   * Thus allowing the 'user' of the function to
   * take care of the form of what is going to happen
   * with each of the items that are changed.
   * As it is now, it dictates a structured result(very useful indeed).
   */
  template <read_time_series_callable FX>
  std::pair<vector<aref_ts>, vector<utcperiod>> read_changed_subs(ts_observer& subs, FX&& read_time_series) {
    auto rr = subs.find_changed_ts_o(); // efficiency:this provides bulked up reads pr. period spec!
    if (rr.empty())
      return {};
    vector<aref_ts> r;
    vector<utcperiod> r_p;
    r.reserve(rr.size());
    r_p.reserve(rr.size());
    for (auto const & i : rr) {
      std::vector<std::string> ts_ids;
      ts_ids.reserve(i.second.size());
      for (auto const & id : i.second) {
        ts_ids.emplace_back(id->o->id);
      }
      auto [pr, pr_tp] = read_time_series(ts_ids, i.first);
      for (size_t j = 0; j < pr.size(); ++j) {
        if (pr[j]) { // ref issue #1233, update subs.p for surrounding reads, only if non-null then update ts_o period,
          auto const read_p = pr[j]->total_period();
          if (read_p.valid()) {
            if (read_p.start < i.second[j]->p.start)
              i.second[j]->p.start = read_p.start;
            if (read_p.end > i.second[j]->p.end)
              i.second[j]->p.end = read_p.end;
          }
        }
        r.emplace_back(std::move(ts_ids[j]), std::move(pr[j])); // ts-name,ts-values (that might be a nullptr)
        r_p.emplace_back(pr_tp[j]);                    // ts-total period
      }
    }
    return std::make_pair(r, r_p);
  };
}