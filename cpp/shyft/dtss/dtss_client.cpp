/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <chrono>
#include <cstring>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <numeric>
#include <regex>
#include <utility>

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/serialize_blob.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/dtss_url.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/protocol/messages.h>
#include <shyft/dtss/q_bridge/config.h>
#include <shyft/dtss/q_bridge/status.h>
#include <shyft/dtss/transfer/config.h>
#include <shyft/dtss/transfer/status.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/expression_serialization.h>

namespace shyft::dtss {

  namespace {
    void verify_store_tsv(time_series::dd::ats_vector const & tsv) {
      for (auto const & ats : tsv) {
        auto rts = dynamic_cast<time_series::dd::aref_ts const *>(ats.ts.get());
        if (!rts)
          throw std::runtime_error(fmt::format("attempt to store a null ts"));
        if (rts->needs_bind())
          throw std::runtime_error(fmt::format("attempt to store unbound ts:{}", rts->id));
      }
    }
  }

  client::client(std::string const & host_port, int timeout_ms, version_type v, int operation_timeout_ms)
    : version(v) {
    if (!protocols::is_valid_version<protocol>(version))
      throw std::runtime_error(fmt::format("Version {} not supported by client", version));
    srv_con.push_back({
      .connection = core::srv_connection{host_port, timeout_ms, operation_timeout_ms}
    });
  }

  client::client(std::vector<std::string> const & host_ports, int timeout_ms, version_type v, int operation_timeout_ms)
    : version(v) {
    if (host_ports.empty())
      throw std::runtime_error("host_ports must contain at least one element");
    for (auto const & hp : host_ports)
      srv_con.push_back({
        .connection = core::srv_connection{hp, timeout_ms, operation_timeout_ms}
      });
  }

  std::size_t client::reconnect_count() const {
    std::size_t s{0u};
    for (auto const & c : srv_con)
      s += c.connection.reconnect_count;
    return s;
  }

  void client::reopen(int timeout_ms) {
    for (auto& sc : srv_con)
      sc.connection.reopen(timeout_ms);
  }

  void client::close(int timeout_ms) {
    bool rethrow = false;
    std::runtime_error rt_re("");
    for (auto& sc : srv_con) {
      try {
        sc.connection.close(timeout_ms);
      } catch (std::exception const & re) { // ensure we try to close all
        // TODO: why only the last exception?
        rt_re = std::runtime_error(re.what());
        rethrow = true;
      }
    }
    if (rethrow)
      throw rt_re;
  }

  void client::configure_retry_policies(
    std::size_t max_reconnect,
    std::size_t max_retry,
    core::utctime delay_before_new_attempt) {
    for (auto& c : srv_con) {
      c.connection.max_connect_attempts = max_reconnect;
      c.connection.max_retry_attempts = max_retry;
      c.connection.delay_before_new_attempt = delay_before_new_attempt;
    }
  }

  std::string client::get_server_version() {
    return with_version<std::string>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::GET_VERSION>{}).shyft_version;
    });
  }

  std::array<version_type, 2> client::get_supported_protocols() {
    return with_version<std::array<version_type, 2>>([&]<version_type V, typename tags> {
      auto versions = srv_con[0].send(request<V, tags::PROTOCOL_VERSIONS>{});
      return std::array{versions.minimum, versions.maximum};
    });
  }

  std::vector<ts_info> client::find(std::string search_expression) {
    return with_version<std::vector<ts_info>>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::FIND_TS>{.search_expression = std::move(search_expression)}).infos;
    });
  }

  ts_info client::get_ts_info(std::string ts_url) {
    return with_version<ts_info>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::GET_TS_INFO>{.url = std::move(ts_url)}).info;
    });
  }

  std::vector<time_series::dd::apoint_ts> client::percentiles(
    time_series::dd::ats_vector tsv,
    core::utcperiod p,
    time_axis::generic_dt ta,
    std::vector<std::int64_t> percentile_spec,
    bool use_ts_cached_read,
    bool update_ts_cache) {
    return with_version<std::vector<time_series::dd::apoint_ts>>(
      [&]<version_type V, typename tags>() -> std::vector<time_series::dd::apoint_ts> {
        if constexpr (!protocols::is_internal_version<protocol>(V)) {
          throw std::runtime_error(
            fmt::format("Requested version {} does not support percentiles, use internal client", V));
        } else {
          if (tsv.empty())
            throw std::runtime_error("percentiles requires a source ts-vector with more than 0 time-series");
          if (percentile_spec.empty())
            throw std::runtime_error("percentile function require more than 0 percentiles specified");
          if (!p.valid())
            throw std::runtime_error("percentiles require a valid period-specification");
          if (ta.size() == 0)
            throw std::runtime_error("percentile function require a time-axis with more than 0 steps");

          if (srv_con.size() == 1 || tsv.size() < srv_con.size()) {
            if (compress_expressions)
              return srv_con[0]
                .send(
                  request<V, tags::EVALUATE_EXPRESSION_PERCENTILES>{
                    .period = std::move(p),
                    .compressed_expression = time_series::dd::expression_compressor::compress(std::move(tsv)),
                    .ta = std::move(ta),
                    .percentile_spec = std::move(percentile_spec),
                    .use_ts_cached_read = use_ts_cached_read,
                    .update_ts_cache = update_ts_cache})
                .percentiles;
            ;
            return srv_con[0]
              .send(
                request<V, tags::EVALUATE_TS_VECTOR_PERCENTILES>{
                  .period = std::move(p),
                  .tsv = std::move(tsv),
                  .ta = std::move(ta),
                  .percentile_spec = std::move(percentile_spec),
                  .use_ts_cached_read = use_ts_cached_read,
                  .update_ts_cache = update_ts_cache})
              .percentiles;
          } else {
            std::vector<int64_t> p_spec;
            // in case we are searching for min-max extreme, we can not do server-side average
            bool can_do_server_side_average = true;
            for (long i : percentile_spec) {
              p_spec.push_back(int(i));
              if (
                i == time_series::statistics_property::MIN_EXTREME
                || i == time_series::statistics_property::MAX_EXTREME)
                can_do_server_side_average = false;
            }
            auto atsv = evaluate(
              can_do_server_side_average ? tsv.average(ta) : std::move(tsv),
              std::move(p),
              use_ts_cached_read,
              update_ts_cache); // get the result we can do percentiles on
            return shyft::time_series::dd::percentiles(std::move(atsv), std::move(ta), std::move(p_spec));
          }
        }
      });
  }

  std::vector<time_series::dd::apoint_ts> client::evaluate(
    time_series::dd::ats_vector tsv,
    core::utcperiod p,
    bool use_ts_cached_read,
    bool update_ts_cache,
    core::utcperiod result_clip) {
    return with_version<std::vector<time_series::dd::apoint_ts>>(
      [&]<version_type V, typename tags>() -> std::vector<time_series::dd::apoint_ts> {
        if constexpr (!protocols::is_internal_version<protocol>(V)) {
          throw std::runtime_error(
            fmt::format("Requested version {} does not support evaluate, use internal client", V));
        } else {
          if (tsv.empty())
            throw std::runtime_error("evaluate requires a source ts-vector with more than 0 time-series");
          if (!p.valid())
            throw std::runtime_error("evaluate require a valid period-specification");
          // local lambda to ensure one definition of communication with the server
          auto eval_io =
            [&](
              auto& bc,
              time_series::dd::ats_vector tsv,
              core::utcperiod p,
              bool use_ts_cached_read,
              bool update_ts_cache,
              core::utcperiod result_clip) -> std::vector<time_series::dd::apoint_ts> {
            if (compress_expressions) {
              if (result_clip.valid()) {
                return bc
                  .send(
                    request<V, tags::EVALUATE_EXPRESSION_CLIP>{
                      .period = std::move(p),
                      .compressed_expression = time_series::dd::expression_compressor::compress(std::move(tsv)),
                      .use_ts_cached_read = use_ts_cached_read,
                      .update_ts_cache = update_ts_cache,
                      .clip_period = std::move(result_clip)})
                  .result;
              }
              return bc
                .send(
                  request<V, tags::EVALUATE_EXPRESSION>{
                    .period = std::move(p),
                    .compressed_expression = time_series::dd::expression_compressor::compress(std::move(tsv)),
                    .use_ts_cached_read = use_ts_cached_read,
                    .update_ts_cache = update_ts_cache})
                .result;
            }
            if (result_clip.valid()) {
              return bc
                .send(
                  request<V, tags::EVALUATE_TS_VECTOR_CLIP>{
                    .period = std::move(p),
                    .tsv = std::move(tsv),
                    .use_ts_cached_read = use_ts_cached_read,
                    .update_ts_cache = update_ts_cache,
                    .clip_period = std::move(result_clip)})
                .result;
            }
            return bc
              .send(
                request<V, tags::EVALUATE_TS_VECTOR>{
                  .period = std::move(p),
                  .tsv = std::move(tsv),
                  .use_ts_cached_read = use_ts_cached_read,
                  .update_ts_cache = update_ts_cache})
              .result;
          };
          if (srv_con.size() == 1 || tsv.size() == 1) { // one server, or just one ts, do it easy
            return eval_io(
              srv_con[0], std::move(tsv), std::move(p), use_ts_cached_read, update_ts_cache, std::move(result_clip));
          } else {
            time_series::dd::ats_vector rt(tsv.size()); // make place for the result contributions from threads
            // lamda to eval partition on server
            auto eval_partition = [&rt, &tsv, &eval_io, p, use_ts_cached_read, update_ts_cache, result_clip](
                                    auto& sc, std::size_t i0, std::size_t n) {
              time_series::dd::ats_vector ptsv;
              ptsv.reserve(tsv.size());
              for (std::size_t i = i0; i < i0 + n; ++i)
                ptsv.push_back(tsv[i]);
              auto pt = eval_io(sc, std::move(ptsv), p, use_ts_cached_read, update_ts_cache, result_clip);
              for (std::size_t i = 0; i < pt.size(); ++i)
                rt[i0 + i] = pt[i]; // notice how se stash data into common result variable, safe because each thread
                                    // using it's own preallocated space
            };
            std::size_t n_ts_pr_server = tsv.size() / srv_con.size();
            std::size_t remainder = tsv.size() - n_ts_pr_server * srv_con.size();
            std::vector<std::future<void>> calcs;
            std::size_t b = 0;
            for (std::size_t i = 0; i < srv_con.size() && b < tsv.size(); ++i) {
              std::size_t e = b + n_ts_pr_server;
              if (remainder > 0) {
                e += 1; // spread remainder equally between servers
                --remainder;
              } else if (e > tsv.size()) {
                e = tsv.size();
              }
              std::size_t n = e - b;
              calcs.push_back(std::async(std::launch::async, [this, i, b, n, &eval_partition]() {
                eval_partition(srv_con[i], b, n);
              }));
              b = e;
            }
            for (auto& f : calcs)
              f.get();
            return rt;
          }
        }
      });
  }

  diags_t client::store(std::vector<url_ts_frag> fragments, store_policy p) {
    if (fragments.size() <= 0)
      return {};
    return with_version<diags_t>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::STORE_TS_2>{.policy = p, .fragments = std::move(fragments)}).diagnostics;
    });
  }

  diags_t client::store_ts(time_series::dd::ats_vector tsv, store_policy p) {
    if (tsv.empty())
      return {};
    verify_store_tsv(tsv);
    return store(to_fragments(std::move(tsv)), p);
  }

  void client::merge_store_ts(time_series::dd::ats_vector tsv, bool cache_on_write) {
    if (tsv.empty())
      return;
    verify_store_tsv(tsv);
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (V >= 1) {
        srv_con[0].send(
          request<V, tags::MERGE_STORE_TS>{.fragments = to_fragments(std::move(tsv)), .cache = cache_on_write});
      } else {
        srv_con[0].send(request<V, tags::MERGE_STORE_TS>{.tsv = std::move(tsv), .cache = cache_on_write});
      }
    });
  }

  void client::remove(std::string ts_url) {
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(request<V, tags::REMOVE_TS>{.url = ts_url});
    });
  }

  void client::cache_flush() {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support cache_flush, use internal client", V));
      } else {
        for (auto& sc : srv_con)
          sc.send(request<V, tags::CACHE_FLUSH>{});
      }
    });
  }

  cache_stats client::get_cache_stats() {
    return with_version<cache_stats>([&]<version_type V, typename tags>() -> cache_stats {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support get_cache_stats, use internal client", V));
      } else {
        cache_stats s;
        for (auto& sc : srv_con)
          s = s + sc.send(request<V, tags::CACHE_STATS>{}).stats;
        return s;
      }
    });
  }

  msync_read_result
    client::try_read(std::vector<std::string> tsv, core::utcperiod p, bool use_ts_cached_read, bool subscribe) {
    if (tsv.empty())
      throw std::runtime_error("(slave)read requires a source tsid-vector with more than 0 ts ids");
    if (!p.valid())
      throw std::runtime_error("evaluate require a valid period-specification");
    return with_version<msync_read_result>([&]<version_type V, typename tags> {
      auto result = srv_con[0].send(
        request<V, tags::TRY_SLAVE_READ>{
          .ids = std::move(tsv), .period = std::move(p), .cache = use_ts_cached_read, .subscribe = subscribe});
      return msync_read_result{
        .result = std::move(result.fragments),
        .result_tp = std::move(result.periods),
        .updates = std::move(result.fragment_updates),
        .updates_tp = std::move(result.period_updates),
        .errors = std::move(result.errors)};
    });
  }

  std::pair<std::vector<time_series::dd::aref_ts>, std::vector<core::utcperiod>> client::read_subscription() {
    return with_version<std::pair<std::vector<time_series::dd::aref_ts>, std::vector<core::utcperiod>>>(
      [&]<version_type V, typename tags> {
        auto result = srv_con[0].send(request<V, tags::SLAVE_READ_SUBSCRIPTION>{});
        return std::make_pair(std::move(result.series), std::move(result.periods));
      });
  }

  void client::unsubscribe(std::vector<std::string> tsv) {
    if (tsv.empty())
      return;
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(request<V, tags::SLAVE_UNSUBSCRIBE>{.ids = tsv});
    });
  }

  std::vector<std::string> client::get_container_names() {
    return with_version<std::vector<std::string>>([&]<version_type V, typename tags>() {
      return srv_con[0].send(request<V, tags::GET_CONTAINERS>{}).names;
    });
  }

  void client::set_container(std::string name, std::string rel_path, std::string type, db_cfg database_c) {
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(
        request<V, tags::SET_CONTAINER>{
          .name = std::move(name),
          .path = std::move(rel_path),
          .type = std::move(type),
          .config = std::move(database_c)});
    });
  }

  void client::remove_container(std::string container_url, bool remove_from_disk) {
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(
        request<V, tags::REMOVE_CONTAINER>{.url = std::move(container_url), .remove_from_disk = remove_from_disk});
    });
  }

  void client::swap_container(std::string container_name_a, std::string container_name_b) {
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(
        request<V, tags::SWAP_CONTAINER>{.name_a = std::move(container_name_a), .name_b = std::move(container_name_b)});
    });
  }

  geo::geo_ts_matrix client::geo_evaluate(geo::eval_args ea, bool use_cache, bool update_cache) {
    return with_version<geo::geo_ts_matrix>([&]<version_type V, typename tags>() -> geo::geo_ts_matrix {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support geo_evaluate, use internal client", V));
      } else {
        return srv_con[0]
          .send(
            request<V, tags::EVALUATE_GEO>{
              .arguments = std::move(ea), .cache_read = use_cache, .cache_write = update_cache})
          .matrix;
      }
    });
  }

  void client::geo_store(std::string geo_db_name, geo::ts_matrix tsm_expr, bool replace, bool update_cache) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support geo_store, use internal client", V));
      } else {
        geo::ts_matrix tsm; // make sure to deflate/compute the expressions prior to serialization tsm.shape
        tsm.shape = std::move(tsm_expr.shape);
        tsm.tsv = time_series::dd::deflate_ts_vector<time_series::dd::apoint_ts>(std::move(tsm_expr.tsv));
        srv_con[0].send(
          request<V, tags::STORE_GEO>{
            .db_name = std::move(geo_db_name),
            .matrix = std::move(tsm),
            .replace = replace,
            .cache_write = update_cache});
      }
    });
  }

  std::vector<std::shared_ptr<geo::ts_db_config>> client::get_geo_ts_db_info() {
    return with_version<std::vector<geo::ts_db_config_>>(
      [&]<version_type V, typename tags>() -> std::vector<geo::ts_db_config_> {
        if constexpr (!protocols::is_internal_version<protocol>(V)) {
          throw std::runtime_error(
            fmt::format("Requested version {} does not support geo_store, use internal client", V));
        } else {
          std::vector<std::shared_ptr<geo::ts_db_config>> s;
          for (auto& sc : srv_con)
            std::ranges::copy(sc.send(request<V, tags::GET_GEO_INFO>{}).configs, std::back_inserter(s));
          return s;
        }
      });
  }

  void client::add_geo_ts_db(std::shared_ptr<geo::ts_db_config> gdb) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support add_geo_ts_db, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::ADD_GEO_DB>{.config = std::move(gdb)});
      }
    });
  }

  void client::remove_geo_ts_db(std::string geo_db_name) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support remove_geo_ts_db, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::REMOVE_GEO_DB>{.name = std::move(geo_db_name)});
      }
    });
  }

  void client::update_geo_ts_db(
    std::string geo_db_name,
    std::string description,
    std::string json,
    std::string origin_proj4) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support remove_geo_ts_db, use internal client", V));
      } else {
        srv_con[0].send(
          request<V, tags::UPDATE_GEO_DB>{
            .name = std::move(geo_db_name),
            .description = std::move(description),
            .json = std::move(json),
            .origin = std::move(origin_proj4)});
      }
    });
  }

  std::vector<std::string> client::q_list() {
    return with_version<std::vector<std::string>>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::Q_LIST>{}).queues;
    });
  }

  queue::msg_info client::q_msg_info(std::string q_name, std::string msg_id) {
    return with_version<queue::msg_info>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::Q_INFO>{.name = std::move(q_name), .message_id = std::move(msg_id)}).info;
    });
  }

  std::vector<queue::msg_info> client::q_msg_infos(std::string q_name) {
    return with_version<std::vector<queue::msg_info>>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::Q_INFOS>{.name = std::move(q_name)}).infos;
    });
  }

  void client::q_put(
    std::string q_name,
    std::string msg_id,
    std::string descript,
    core::utctime ttl,
    time_series::dd::ats_vector tsv) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (V >= 1) {
        srv_con[0].send(
          request<V, tags::Q_PUT>{
            .name = std::move(q_name),
            .message_id = std::move(msg_id),
            .description = std::move(descript),
            .ttl = std::move(ttl),
            .fragments = to_fragments(std::move(tsv))});
      } else {
        srv_con[0].send(
          request<V, tags::Q_PUT>{
            .name = std::move(q_name),
            .message_id = std::move(msg_id),
            .description = std::move(descript),
            .ttl = std::move(ttl),
            .tsv = std::move(tsv)});
      }
    });
  }

  std::shared_ptr<queue::tsv_msg> client::q_get(std::string q_name, core::utctime max_wait) {
    return with_version<std::shared_ptr<queue::tsv_msg>>([&]<version_type V, typename tags> {
      if constexpr (V >= 1) {
        auto rep = srv_con[0].send(request<V, tags::Q_GET>{.name = std::move(q_name), .max_wait = std::move(max_wait)});
        auto msg = std::make_shared<queue::tsv_msg>();
        msg->info = std::move(rep.info);
        msg->tsv = from_fragment(std::move(rep.fragments));
        return msg;
      } else {
        return srv_con[0]
          .send(request<V, tags::Q_GET>{.name = std::move(q_name), .max_wait = std::move(max_wait)})
          .message;
      }
    });
  }

  std::shared_ptr<queue::tsv_msg> client::q_find(std::string q_name, std::string msg_id) {
    return with_version<std::shared_ptr<queue::tsv_msg>>([&]<version_type V, typename tags> {
      if constexpr (requires { tags::Q_FIND; }) {
        auto rep = srv_con[0].send(
          request<V, tags::Q_FIND>{.name = std::move(q_name), .message_id = std::move(msg_id)});
        if (rep.info)
          return std::make_shared<queue::tsv_msg>(std::move(*rep.info), from_fragment(std::move(rep.fragments)));
      }
      return std::shared_ptr<queue::tsv_msg>(nullptr);
    });
  }

  void client::q_ack(std::string q_name, std::string msg_id, std::string diag) {
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(
        request<V, tags::Q_ACK>{
          .name = std::move(q_name), .message_id = std::move(msg_id), .diagnosis = std::move(diag)});
    });
  }

  std::size_t client::q_size(std::string q_name) {
    return with_version<std::size_t>([&]<version_type V, typename tags> {
      return srv_con[0].send(request<V, tags::Q_SIZE>{.name = std::move(q_name)}).size;
    });
  }

  void client::q_add(std::string q_name) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(fmt::format("Requested version {} does not support q_add, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::Q_ADD>{.name = std::move(q_name)});
      }
    });
  }

  void client::q_remove(std::string q_name) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(fmt::format("Requested version {} does not support q_add, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::Q_REMOVE>{.name = std::move(q_name)});
      }
    });
  }

  void client::q_maintain(std::string q_name, bool keep_ttl_items, bool flush_all) {
    with_version<void>([&]<version_type V, typename tags> {
      srv_con[0].send(
        request<V, tags::Q_MAINTAIN>{.name = std::move(q_name), .keep_ttl_items = keep_ttl_items, .flush = flush_all});
    });
  }

  void client::start_transfer(transfer::configuration cfg) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support start_transfer, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::START_TRANSFER>{.config = std::move(cfg)});
      }
    });
  }

  std::vector<transfer::configuration> client::get_transfers() {
    return with_version<std::vector<transfer::configuration>>(
      [&]<version_type V, typename tags>() -> std::vector<transfer::configuration> {
        if constexpr (!protocols::is_internal_version<protocol>(V)) {
          throw std::runtime_error(
            fmt::format("Requested version {} does not support get_transfers, use internal client", V));
        } else {
          return srv_con[0].send(request<V, tags::GET_TRANSFERS>{}).configurations;
        }
      });
  }

  transfer::status client::get_transfer_status(std::string name, bool clear_status) {
    return with_version<transfer::status>([&]<version_type V, typename tags>() -> transfer::status {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support get_transfer_status, use internal client", V));
      } else {
        return srv_con[0]
          .send(request<V, tags::GET_TRANSFER_STATUS>{.name = std::move(name), .clear_status = clear_status})
          .status;
      }
    });
  }

  void client::stop_transfer(std::string name, core::utctime max_wait) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support stop_transfer, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::STOP_TRANSFER>{.name = std::move(name), .max_wait_time = std::move(max_wait)});
      }
    });
  }

  void client::start_q_bridge(q_bridge::configuration cfg) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support start_q_bridge, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::START_Q_BRIDGE>{.config = std::move(cfg)});
      }
    });
  }

  std::vector<q_bridge::configuration> client::get_q_bridges() {
    return with_version<std::vector<q_bridge::configuration>>(
      [&]<version_type V, typename tags>() -> std::vector<q_bridge::configuration> {
        if constexpr (!protocols::is_internal_version<protocol>(V)) {
          throw std::runtime_error(
            fmt::format("Requested version {} does not support get_q_bridges, use internal client", V));
        } else {
          return srv_con[0].send(request<V, tags::GET_Q_BRIDGES>{}).configurations;
        }
      });
  }

  q_bridge::status client::get_q_bridge_status(std::string name, bool clear_status) {
    return with_version<q_bridge::status>([&]<version_type V, typename tags>() -> q_bridge::status {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support get_q_bridge_status, use internal client", V));
      } else {
        return srv_con[0]
          .send(request<V, tags::GET_Q_BRIDGE_STATUS>{.name = std::move(name), .clear_status = clear_status})
          .status;
      }
    });
  }

  void client::stop_q_bridge(std::string name, core::utctime max_wait) {
    with_version<void>([&]<version_type V, typename tags> {
      if constexpr (!protocols::is_internal_version<protocol>(V)) {
        throw std::runtime_error(
          fmt::format("Requested version {} does not support stop_q_bridge, use internal client", V));
      } else {
        srv_con[0].send(request<V, tags::STOP_Q_BRIDGE>{.name = std::move(name), .max_wait_time = std::move(max_wait)});
      }
    });
  }

}
