/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <optional>
#include <stdexcept>

#include <fmt/core.h>

#include <shyft/time_series/fx_make_aligned.h>

namespace shyft::dtss::detail {

  /**
   * @brief make_aligned_ts
   * @details
   * When storing ts fragments, this function ensure the fragment is conformant
   * with the target ts time-axis specs,
   * according to the passed in `strict_alignment` policy.
   * For strict alignment requirement, it throws if not possible.
   * If non strict alignment, it will if needed, create a new aligned ts and
   * return it.
   *
   * If the returned optional is set, this is the ts to store/cache,
   * otherwise the passed in  `ats` is valid for storage.
   *
   * @note
   * In the case of non strict alignment, the result could be an empty ts!
   * So make sure to check for this case when using it in db impl.
   *
   * @see fx_make_aligned
   *
   * @tparam T Target require: .ta_type, .t0,.dt,.tz
   * @tparam CFX Calendar Fx(string)->shared ptr cal object
   * @param strict_alignment require strict alignment of `ats` to target T
   * @param tgt Target ts time-axis description
   * @param lookup_calendar if tgt is calendar, this is used to lookup the calendar object
   * @param ats the source ts
   * @return optional gts_t, filled in if `ats` require a mod to be aligned
   */
  template <class T, class CFX>
  std::optional<time_series::gts_t>
    make_aligned_ts(bool strict_alignment, T const & tgt, CFX&& lookup_calendar, time_series::gts_t const & ats) {
    using time_series::fx_make_aligned;
    using time_series::time_axis_fixed_dt;
    using time_series::time_axis_calendar_dt;
    using core::calendar, core::utctime, core::utctimespan;
    if (ats.ta.gt() != tgt.ta_type && tgt.ta_type != time_axis::generic_dt::POINT) {
      if (strict_alignment)
        throw std::runtime_error(fmt::format(
          "dtss_store: cannot merge with different ta type old {} != new {}", (int) tgt.ta_type, (int) ats.ta.gt()));
      // to calendar/fixed dt, from any ats.
      if (tgt.ta_type == time_axis::generic_dt::FIXED)
        return fx_make_aligned(time_axis_fixed_dt{}, tgt.t0, tgt.dt, tgt.point_fx, ats);
      else
        return fx_make_aligned(time_axis_calendar_dt{lookup_calendar(tgt.tz)}, tgt.t0, tgt.dt, tgt.point_fx, ats);
    } else {
      // parse specific ta data to determine compatibility
      constexpr auto diag_string = [](char const * preamble, utctime ta, utctime tb, utctime da, utctime db) {
        calendar utc;
        return fmt::format(
          "{} old t0={},dt={} vs. new t0={},dt={}",
          preamble,
          utc.to_string(ta),
          shyft::core::to_seconds(da),
          utc.to_string(tb),
          shyft::core::to_seconds(db));
      };
      switch (tgt.ta_type) {
      case time_axis::generic_dt::FIXED: {
        if (tgt.dt != ats.ta.f().dt || (tgt.t0 - ats.ta.f().t).count() % tgt.dt.count() != 0) {
          if (strict_alignment)
            throw std::runtime_error(
              diag_string("dtss_store: cannot merge unaligned fixed_dt:", tgt.t0, ats.ta.f().t, tgt.dt, ats.ta.f().dt));
          return fx_make_aligned(time_axis_fixed_dt{}, tgt.t0, tgt.dt, tgt.point_fx, ats); // fixed->fixed
        }
      } break;
      case time_axis::generic_dt::CALENDAR: {
        if (ats.ta.c().cal->get_tz_name() == tgt.tz) {
          utctimespan remainder;
          (void) ats.ta.c().cal->diff_units(tgt.t0, ats.ta.c().t, tgt.dt, remainder);
          if (tgt.dt != ats.ta.c().dt || remainder != utctimespan{0l}) {
            if (strict_alignment)
              throw std::runtime_error(diag_string(
                "dtss_store: cannot merge unaligned calendar_dt:", tgt.t0, ats.ta.c().t, tgt.dt, ats.ta.c().dt));
            return fx_make_aligned(
              time_axis_calendar_dt{lookup_calendar(tgt.tz)},
              tgt.t0,
              tgt.dt,
              tgt.point_fx,
              ats); // calendar_dt ->calendar_dt
          }
        } else {
          if (strict_alignment)
            throw std::runtime_error(fmt::format(
              "dtss_store: cannot merge calendar_dt with different calendars, old={},new={}",
              tgt.tz,
              ats.ta.c().cal->get_tz_name()));
          return fx_make_aligned(
            time_axis_calendar_dt{lookup_calendar(tgt.tz)},
            tgt.t0,
            tgt.dt,
            tgt.point_fx,
            ats); // calendar_dt ->calendar_dt
        }
      } break;
      case time_axis::generic_dt::POINT: {
        // point and point timeaxis are always compatible
        if (ats.ta.gt() != time_axis::generic_dt::POINT)
          return time_series::gts_t{
            time_axis::generic_dt{time_axis::convert_to_point_dt(ats.ta)}, ats.v, ats.point_interpretation()};
      }
      }
    }
    return {}; // passed all checks, so no transformed ts done
  }
}
