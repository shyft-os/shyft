/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/diagnostics.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/point_ts.h>

namespace shyft::dtss {

  using std::vector;
  using std::map;
  using std::unique_ptr;
  using std::string;
  using std::string_view;
  using std::size_t;
  using std::tuple;
  using shyft::core::utctime;
  using shyft::core::utcperiod;
  using gta_t = shyft::time_axis::generic_dt;
  using gts_t = shyft::time_series::point_ts<gta_t>;

  using ts_item_t = std::tuple<string_view, gts_t const &>; ///< a tuple of reference to name,ts
  using fx_ts_item_t = std::function< ts_item_t(size_t)>;   ///< callable fx that should return ref to path,gts_t
  using fx_cache_t = std::function< ///< callable to update `i` in cache, total_period(ref??) of ts(update it in cache)
    void(size_t i, std::optional<gts_t> ts, utcperiod tot_period)>; ///< ,w.optional projected content


  /**
   * @brief The abstract ts-db container interface
   *
   * @details
   * The dtss::server passes request to save/read/remove or find to instances
   * that implements this interface.
   *
   */
  struct its_db {
    virtual ~its_db() = default;

    /**
     * @brief Save a time-series to a db
     *
     * @details
     * If missing, then create, or just update the existing part of ts as covered by the passed ts-fragment.
     *
     * @param fn  'Path-name' to save the time-series at, relative the container name, so if shyft://container/a/b/c,
     * then a/b/c.
     * @param ts  Time-series to save.
     * @param overwrite  if true, replace existing stored ts with new (disregard any of previous stored content.
     * @param fx_cache callable to provide cache of successfully written,and possibly transformed elements
     * @param policy provide the policy like strict align, recreate, best_effort etc.

     *
     */
    [[nodiscard]] virtual diags_t
      save(string_view fn, gts_t const &ts, fx_cache_t fx_cache, store_policy const &policy) = 0;

    /**
     * @brief Save multiple time-series to db
     * @details
     * Allow implementation to efficiently save multiple time-series to db, using multithread/batch or even
     * transaction like mechanisms to improve throughput.
     * The the callable fx_item(i), will be invoked n-times(unless exception), and should return the i-th fx_ts_item_t.
     * The fx_ts_item_t is a tuple with refs to the name and ts that should be stored, so that we avoid
     * copy of the elements.
     * The lifetime of the returned tuple refs should of course be similar to the lifetime of the call.
     * In case of exception, the behaviour is implementation specific.
     *
     * Usually the time-series up to the exception occurred is stored (best effort).
     *
     * @param n number of time-series that should be saved.
     * @param fx_item callable that provides the reference to (name,ts) pair to be stored.
     * @param fx_cache callable to provide cache of successfully written,and possibly transformed elements
     * @param policy provide the policy like strict align, recreate, best_effort etc.
     * time-axis
     */
    [[nodiscard]] virtual diags_t
      save(size_t n, fx_ts_item_t const &fx_item, fx_cache_t fx_cache, store_policy const &policy) = 0;

    /**
     * @brief  read a ts
     * @details
     * Read enough part of ts to allow it to be evaluated over the period p.
     * This has the implication that the time-point preceding the period p is included, if there
     * is no time-point exactly at p.start.
     * For linear point of series, an extra point might be included if there is not
     * time-point exactly at p.end.
     * @param fn name of ts
     * @param p period to read, empty means read entire ts
     * @returns the resulting read time-series and its total_period tuple
     */
    [[nodiscard]] virtual tuple<gts_t, utcperiod> read(string_view fn, utcperiod p) = 0;

    /** @brief removes a ts from the container */
    virtual void remove(string_view fn) = 0;

    /** @brief get minimal ts-information from specified fn */
    [[nodiscard]] virtual ts_info get_ts_info(string_view fn) = 0;

    /**
     * @brief find ts matching criteria
     * @details
     * Find all ts_info s that matches the specified regexp match string
     *
     * e.g.: match= 'hydmet_station/.*_id/temperature'
     *    would find all time-series /hydmet_station/xxx_id/temperature
     * @param match regular expression to match
     */
    [[nodiscard]] virtual vector<ts_info> find(string_view match) = 0;

    /** @brief return the root_dir string, if applicable, default to empty string */
    [[nodiscard]] virtual string root_dir() const {
      return "";
    }

    inline static string const reserved_extension{".cfg"}; ///< return reserved extension,
                                                           /// like .cfg, that are disallowed for  ts file-names

    /**
     * @brief mark the container for disk deletion when the destructor is called
     */
    virtual void mark_for_deletion() = 0;
  };

}
