#include <algorithm>
#include <cstdint>
#include <functional>
#include <memory>
#include <ranges>
#include <vector>

#include <shyft/dtss/url_ts_frag.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

namespace shyft::dtss {
  time_series::dd::ats_vector from_fragment(std::vector<dtss::url_ts_frag> fragments) {
    time_series::dd::ats_vector tsv;
    tsv.reserve(fragments.size());
    std::ranges::copy(
      std::views::transform(
        fragments,
        [](auto& fragment) {
          auto as_apoint = time_series::dd::apoint_ts(
            std::make_shared<time_series::dd::gpoint_ts const>(std::move(fragment.ts)));
          return time_series::dd::apoint_ts(std::move(fragment.url), std::move(as_apoint));
        }),
      std::back_inserter(tsv));
    return tsv;
  }

  std::vector<dtss::url_ts_frag> to_fragments(time_series::dd::ats_vector tsv) {
    std::vector<dtss::url_ts_frag> fragments(tsv.size());
    std::ranges::copy(
      std::views::transform(
        tsv,
        [](auto& ats) {
          auto rts = time_series::dd::ts_as<time_series::dd::aref_ts>(ats.ts);
          if (rts == nullptr)
            throw std::runtime_error("fatal: to_fragments invoked with not supported types");
          return url_ts_frag(rts->id, rts->core_ts());
        }),
      fragments.data());
    return fragments;
  }
}