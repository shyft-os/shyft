#pragma once
#include <cstdint>
#include <mutex>
#include <string>
#include <string_view>

#include <dlib/logger.h>

#include <shyft/dtss/q_bridge/status.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::q_bridge {

  /**
   * @brief logger for q-bridge
   * @details
   * Responsible for providing statistics and also logs, if enabled.
   * The logger should have constant minimal cpu/memory when in normal operation.
   * When logging is enabled, provide useful information for problem-solving.
   * In normal operation, provide useful counters and latest error detected.
   */
  struct logger {
    static dlib::logger dlog;
    std::mutex mutex;
    std::size_t fetched{0ul};
    std::size_t acked_msgs{0ul};
    error_entry ack_error;
    error_entry fetch_error;
    std::size_t ack_errors{0ul};
    std::size_t fetch_errors{0ul};
    utctime last_activity{no_utctime};

    void record_alive();
    void record_msg_ack_success(std::string_view msg_info);
    void record_msg_ack_error(std::string_view err);
    void record_msg_fetch_success(std::string_view msg_info, std::string_view local_msg_id);
    void record_msg_fetch_error(std::string_view err);
  };

}