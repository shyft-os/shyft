#include <shyft/dtss/q_bridge/logger.h>

namespace shyft::dtss::q_bridge {

  dlib::logger logger::dlog = dlib::logger("q_bridge");

  void logger::record_alive() {
    std::scoped_lock lock(mutex);
    last_activity = core::utctime_now();
  }

  void logger::record_msg_ack_error(std::string_view err) {
    std::scoped_lock lock(mutex);
    last_activity = core::utctime_now();
    ++ack_errors;
    ack_error = {.code = std::string(err), .time = last_activity};
    dlog << dlib::LERROR << fmt::format("Acknowledge handling problem:{}", err);
  }

  void logger::record_msg_fetch_error(std::string_view err) {
    std::scoped_lock lock(mutex);
    last_activity = core::utctime_now();
    ++fetch_errors;
    fetch_error = {.code = std::string(err), .time = last_activity};
    dlog << dlib::LERROR << fmt::format("Writing to local queue error:{}", err);
  }

  void logger::record_msg_ack_success(std::string_view msg_info) {
    std::scoped_lock lock(mutex);
    last_activity = core::utctime_now();
    ++acked_msgs;
    if (dlog.level() >= dlib::LTRACE)
      dlog << dlib::LTRACE << fmt::format("Message with id {} acknowledged(completed)", msg_info);
  }

  void logger::record_msg_fetch_success(std::string_view msg_info, std::string_view local_msg_id) {
    std::scoped_lock lock(mutex);
    ++fetched; // maybe consider atomic and no lock
    if (dlog.level() >= dlib::LTRACE)
      dlog << dlib::LTRACE
           << fmt::format(
                "Message with id {} fetched from remote to local queue as {}, awaiting ack", msg_info, local_msg_id);
  }

}
