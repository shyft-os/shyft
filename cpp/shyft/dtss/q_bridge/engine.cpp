#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/q_bridge/engine.h>
#include <shyft/dtss/q_bridge/logger.h>
#include <shyft/web_api/dtss_client.h>

namespace shyft::dtss::q_bridge {
  namespace {
    struct shared_models_manager {
      struct shared_models {
        mutable std::mutex mutex;
        std::vector<std::string> models; ///< the model is just the msg_ids, produced by msg-worker, consumed by ack-w
      };

      decltype(auto) mutate(auto &&f) {
        std::scoped_lock lock{shared_models.mutex};
        return std::invoke(SHYFT_FWD(f), shared_models.models);
      }

      decltype(auto) observe(auto &&f) const {
        std::scoped_lock lock{shared_models.mutex};
        return std::invoke(SHYFT_FWD(f), shared_models.models);
      }

      shared_models shared_models;
    };

    auto create_remote_msg_id(auto const &engine_impl, std::string const &msg_id) {
      return fmt::format(
        "[{}:{}:{}]{}",
        engine_impl.conf.where.host,
        engine_impl.conf.where.port,
        engine_impl.conf.where.queue_name,
        msg_id);
    }

  }

  struct engine_impl {
    engine_impl(server_state &server, configuration config_);
    ~engine_impl();
    void start();
    status get_status(bool clear);
    void stop(utctime graceful_close);
    configuration get_configuration() const;

    void q_msg_worker(std::stop_token stopToken);

    void q_ack_msg_worker(std::stop_token stopToken);

    bool should_terminate() const;

    server_state &server;               ///< ref to dtss server, that have the .terminate atomic flag we must monitor
    logger log;                         ///< the log/statistics functionality
    std::atomic_bool is_running{false}; ///< we need to know if we are running or not (start/stop logic)
    utctime max_graceful_stop_time{core::from_seconds(1.0)}; ///< possible usage for forced stop
    configuration conf{};                                    ///< the q_bridge configuration
    std::stop_source stop_source;                            ///< the stop_source handle for the ack and msg j-threads
    std::jthread q_msg_thread;                               ///< msg thread worker
    std::jthread q_ack_msg_thread;                           ///< ack msg thread worker
    shared_models_manager shared_models_manager_;            ///< used for the worker shared vector of msg_ids
  };

  bool engine_impl::should_terminate() const {
    return server.terminate;
  }

  engine_impl::engine_impl(server_state &server, configuration config_)
    : server{server}
    , conf(std::move(config_)) {
  }

  engine_impl::~engine_impl() {
    (void) stop_source.request_stop();
  }

  void engine_impl::start() {
    if (is_running) {
      log.dlog << dlib::LWARN << "q_bridge: already running";
      return;
    }
    log.dlog << dlib::LINFO << "Starting queue bridge: " << conf.name;
    if (conf.what.queue_name.empty() || conf.where.queue_name.empty()) {
      auto diag = fmt::format(
        "Failed to start '{}' due to missing queue names: local='{}', remote='{}'",
        conf.name,
        conf.what.queue_name,
        conf.where.queue_name);
      log.dlog << dlib::LERROR << diag;
      throw std::runtime_error(diag);
    }
    if (!server.queue_manager.exists(conf.what.queue_name)) {
      server.queue_manager.add(conf.what.queue_name);
      log.dlog << dlib::LWARN
               << fmt::format(
                    "q_bridge: {}, local queue '{}' missing, adding it to repair: ", conf.name, conf.what.queue_name);
    }

    q_ack_msg_thread = std::jthread(&engine_impl::q_ack_msg_worker, std::ref(*this), stop_source.get_token());
    q_msg_thread = std::jthread(&engine_impl::q_msg_worker, std::ref(*this), stop_source.get_token());
    is_running = true;
  }

  status engine_impl::get_status(bool clear) {
    status status;
    std::scoped_lock lock{log.mutex};
    status.ack_errors = log.ack_errors;
    status.last_ack_error = log.ack_error;
    status.fetch_errors = log.fetch_errors;
    status.last_fetch_error = log.fetch_error;
    status.last_activity = log.last_activity;
    status.name = conf.name;

    status.completed = log.acked_msgs;
    status.fetched = log.fetched;

    if (clear) {
      log.acked_msgs = 0;
      log.ack_errors = 0;
      log.fetch_errors = 0;
      log.fetched = 0;
      // also last_ack_error, last_fetch_error ?
    }
    return status;
  }

  void engine_impl::stop(utctime graceful_close) {
    max_graceful_stop_time = graceful_close;
    if (is_running) {
      log.dlog << dlib::LINFO << "Stopping queue bridge: " << conf.name;
      (void) stop_source.request_stop();
      if (q_msg_thread.joinable())
        q_ack_msg_thread.join();
      if (q_ack_msg_thread.joinable())
        q_msg_thread.join();
      is_running = false;
      log.dlog << dlib::LINFO << "Queue bridge " << conf.name << " stopped.";
    }
  }

  configuration engine_impl::get_configuration() const {
    return conf;
  }

  namespace {
    /**
     * @brief io_timeout_guard
     * @details
     * Utilize dlib socket library to set a max time for the operation (write message, wait , read message)
     * So that if for any reason the socket/io/or similar gets broken, and do not reply within
     * time-limits, then the operation is cancelled.
     *
     * The mechanism is introduced after testing and observations when disconnecting, or breaking
     * network connections, where we in this context needs to notify and log the event.
     *
     * This is work in progress, so it might better be integrated into the client library,
     * so that we can control the behavior related to operational context.
     * E.g.:
     *  - When sending messages, once we get connection established, we want to ensure it is not stuck in write
     *  - When starting reading message back, we want to pull out(not wait anymore), if time-limit exceeds expectation.
     *
     * The defaults for this must be reasonable and safe, not breaking any operation in HPC usage context
     * where message-sizes could be Gigabytes, and answer-times (server computing result), could take
     * considerable time.
     *
     */
    template <class F>
    auto io_timeout_guard(client &c, utctime const max_operation_time, utctime const keep_alive_time, F &&f) {
      auto const socket_timeout_ms = static_cast<unsigned long>(core::to_seconds(max_operation_time) * 1000);
      auto const socket_keep_ms = static_cast<unsigned long>(core::to_seconds(keep_alive_time) * 1000);
      c.srv_con[0].connection.io->terminate_connection_after_timeout(socket_timeout_ms);
      if constexpr (std::is_void_v<decltype(f())>) {
        f();
        c.srv_con[0].connection.io->terminate_connection_after_timeout(socket_keep_ms);
      } else {
        auto r = f();
        c.srv_con[0].connection.io->terminate_connection_after_timeout(socket_keep_ms);
        return r;
      }
    }
  }

  /**
   * @brief q msg worker
   *
   * @details
   * Consumes messages from the remote q, translate the remote msg id to a local unique msg id using the
   * remote connection information, then post it to the local designated queue.
   * When posting it to local queue, the q.msg_id is also posted to the shared state,
   * so that the q_msg_ack worker can start monitor the local-q, to ensure that it will
   * propagate any ack/done/diagnostics messages from the local q, to the remote.
   *
   * The goal is to minimize the time-delay taken over the queue bridge.
   *
   * @param stop_token
   */
  void engine_impl::q_msg_worker(std::stop_token stop_token) {

    auto const local_queue = server.queue_manager(conf.what.queue_name);
    auto const remote_q_name = conf.where.queue_name;
    // notice that we create separate clients for msg and the ack stream, to ensure they operate independently.

    client remote{fmt::format("{}:{}", conf.where.host, conf.where.port)};
    remote.configure_retry_policies(conf.how.retries, conf.how.retries, conf.how.sleep_before_retry);

    auto const q_max_wait_on_remote = conf.how.queue_pick_up_max_wait;
    auto const keep_alive_remote = q_max_wait_on_remote * 2 + conf.how.sleep_before_retry * 2 + core::from_seconds(5);
    while (!stop_token.stop_requested() && !should_terminate()) {

      std::shared_ptr<queue::tsv_msg> queue_msg;

      try {
        // remote.srv_con[0].connection.io->terminate_connection_after_timeout(socket_timeout_ms);
        queue_msg = io_timeout_guard(
          remote, q_max_wait_on_remote * 2 + conf.how.io_operation_timeout, keep_alive_remote, [&] {
            return remote.q_get(remote_q_name, q_max_wait_on_remote);
          }); // we hang in here most of the time.
        // remote.srv_con[0].connection.io->terminate_connection_after_timeout(socket_timeout_ms*1000);
        if (!queue_msg) { // normal condition, there is no new messages.
          log.record_alive();
          continue; // to check if there is a stop pending
        }
      } catch (std::exception const &err) {
        log.record_msg_fetch_error(err.what());
        if (!stop_token.stop_requested())
          std::this_thread::sleep_for(conf.how.sleep_before_retry + core::from_seconds(0.001));
        continue; //
      }

      auto remote_tag = create_remote_msg_id(*this, queue_msg->info.msg_id);
      try {
        local_queue->put(remote_tag, queue_msg->info.description, queue_msg->info.ttl, queue_msg->tsv);
        shared_models_manager_.mutate([&](auto &&q_msg_ids) {
          q_msg_ids.emplace_back(queue_msg->info.msg_id); // fwd the msg id to the q_ack_msg worker to monitor it.
        });
        log.record_msg_fetch_success(queue_msg->info.msg_id, remote_tag);
      } catch (std::exception const &err) {
        // TODO: what is the best action here?
        //  a local write to a local memory queue is almost impossible
        log.record_msg_fetch_error(err.what());
      }
    }
  }

  /**
   * @brief ack message worker ensures ack/diags are read from local queue and posted to remote q.
   * @details
   *
   * The q bridge msg_worker posts the q.msg_id that is fetches from remote and successfully post
   * on the local queue to the shared model, simply a string list.
   * The ack-worker copies these, and clears the list, and keep a local working-set of q.msg_ids to process/monitor.
   * At regular intervals, when no more progress on the working-set, fetches more from the shared state.
   *
   * With the local copy of q.msg_ids, it checks(polls) if the local q.msg_id is done (or has vanished).
   *
   * If it is:
   *
   *  - done: it posts the ack/diag to the remote-queue, and if success, remove it from the working set.
   *          if it *fails* to post it to the remote-queue, due to comm error the item is kept
   *          if it *fails* to post due to logic error (the remote server/q manager refuses to accept it)
   *          then it is reasonable to remove the item from the working-set.
   *  - missing from the local queue: Kind of strange, but possible, suggested solution is to log,
   *    and remote it from the working-set.
   *
   *  Goals for this worker is to be as fast as precise as possible, minimizing the
   *  time it locks shared resources.
   *
   */
  void engine_impl::q_ack_msg_worker(std::stop_token stop_token) {
    std::vector<std::string> msg_ids_to_ack; // local working-set of q msg_ids to monitor and process.
    auto const local_q = server.queue_manager(conf.what.queue_name);
    auto const local_q_poll_interval = conf.what.ack_poll_interval + core::from_seconds(0.001);
    auto const remote_q_name = conf.where.queue_name;
    client remote{fmt::format("{}:{}", conf.where.host, conf.where.port)};
    remote.configure_retry_policies(conf.how.retries, conf.how.retries, conf.how.sleep_before_retry);
    auto const q_max_wait_on_remote = conf.how.io_operation_timeout + core::from_seconds(0.001);
    auto const keep_alive_remote = core::from_seconds(24 * 3600 * 10); /// 10 years,we do not assume traffic

    while (!stop_token.stop_requested() && !should_terminate()) {
      auto const work_item_count = msg_ids_to_ack.size();
      shared_models_manager_.mutate([&](auto &&q_msg_ids) {
        // just grab the entire shared model of msg_ids produced by the q-msg fetcher
        std::ranges::copy(q_msg_ids, std::back_inserter(msg_ids_to_ack));
        q_msg_ids.clear(); // and clear the shared state, since we will keep it locally
      });

      if (work_item_count == msg_ids_to_ack.size()) { // we are empty, or count did not change, so we sleep a bit
        log.record_alive();
        std::this_thread::sleep_for(
          local_q_poll_interval); // FIXME: rather use flag/semaphore or bounded queue for msg-ids
      }

      // now work through the work item set, (if any), again.
      for (auto work_item = msg_ids_to_ack.begin(); work_item != msg_ids_to_ack.end();) {
        auto remote_id = *work_item;
        auto local_id = create_remote_msg_id(*this, remote_id);
        if (auto const find = local_q->find(local_id); !find) {
          // vanished from local q, so we must give up
          work_item = msg_ids_to_ack.erase(work_item);
          log.record_msg_ack_error(fmt::format("Item vanished from local queue,  msg_id: {}", remote_id));
        } else if (find->info.done == core::no_utctime) {
          // nothing yet(normal, and we do not keep age yet), just skip item for now poll next time.
          ++work_item; // advance to next item.
        } else {
          // auto reconnect_count = remote.reconnect_count(); // to help us decide if it is comm error
          try { // we must
            io_timeout_guard(remote, q_max_wait_on_remote, keep_alive_remote, [&]() {
              remote.q_ack(remote_q_name, remote_id, find->info.diagnostics);
            });
            work_item = msg_ids_to_ack.erase(work_item); // done with this one, remove and next item
            log.record_msg_ack_success(remote_id);
          } catch (std::exception const &err) {
            log.record_msg_ack_error(err.what());
            // Analyze the error, and figure out if we should keep it, or drop it!
            // Rules: if logic error, then drop it.
            //        if comm error, then keep it.
            try {
              (void) io_timeout_guard(remote, q_max_wait_on_remote, keep_alive_remote, [&]() {
                return remote.get_server_version();
              }); // try to reconnect, if possible.
              // if we end here, it is most likely logic error, so we drop the item.
              log.record_msg_fetch_error(
                fmt::format("Give up ack, remote do not accept msg_id: {}, error: {}", remote_id, err.what()));
              work_item = msg_ids_to_ack.erase(work_item); // give up with this one, remove and next item
            } catch (std::exception const &reconnect_err) {
              // in this case, it is connection error, so we try to keep the item
              ++work_item; // keep the item for now(will poll next time.)
            }
          }
        }
      }
    }
  };

  //-- engine
  engine::engine(server_state &server, configuration config_)
    : impl{std::make_unique<engine_impl>(server, std::move(config_))} {
  }

  engine::~engine() = default;

  void engine::start() {
    impl->start();
  }

  status engine::get_status(bool clear) {
    return impl->get_status(clear);
  }

  void engine::stop(utctime graceful_close) {
    impl->stop(graceful_close);
  }

  configuration engine::get_configuration() const {
    return impl->get_configuration();
  };

}
