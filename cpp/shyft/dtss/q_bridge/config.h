#pragma once

#include <cstdint>
#include <string>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/dtss/exchange/protocol.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::q_bridge {
  using core::utctime;

  struct configuration {

    struct remote_spec {
      std::string host;       ///< the host dns/ip
      std::int64_t port{0};   ///< the port at the host
      std::string queue_name; ///< the queue-name (allowing several queues at the same remote host)
      auto operator<=>(remote_spec const &) const = default;

      SHYFT_DEFINE_STRUCT(remote_spec, (), (host, port, queue_name));
      x_serialize_decl();
    };

    /**
     * @brief how to deal with communication errors
     * @details
     * How to deal with remote communication errors, and
     * how long to wait in io-operation on the remote queue.
     * The latter influences how fast the dtss server that
     * hosts the exchange/q_bridge will react to a shut-down/restart.
     * @note
     * The io_operation_timeout parameter controls for how
     * long an io operation is allowed to take before
     * it is timed out, connection closed and
     * reported, then reconnection cycle started.
     * When waiting for queue messages, the two
     * timeouts are added.
     * The minimum value would be the time it takes to:
     * (1) send the message
     * (2) server to compute the answer
     * (3) send the answer back
     * Due to the nature of network, a large slack should be
     * added.
     */
    struct xfer_spec {
      std::int64_t retries{10u}; ///< number of retries at the dtss client connection layer before give up
      utctime sleep_before_retry{core::seconds{1l}};     ///< how long to sleep before each new attempt
      utctime queue_pick_up_max_wait{core::seconds{1l}}; ///< time to wait on server for new q-messages each attempt
      utctime io_operation_timeout{core::seconds{60}};   ///< timeout operation that takes more than this(report)
      auto operator<=>(xfer_spec const &) const = default;
      SHYFT_DEFINE_STRUCT(xfer_spec, (), (retries, sleep_before_retry, queue_pick_up_max_wait, io_operation_timeout));
      x_serialize_decl();
    };

    struct queue_spec {
      std::string queue_name; ///< name of the local queue, if different from remote_spec.queue_name
      utctime ack_poll_interval{core::seconds{1l}}; ///< how often to poll the local q for ack messages
      auto operator<=>(queue_spec const &) const = default;
      SHYFT_DEFINE_STRUCT(queue_spec, (), (queue_name, ack_poll_interval))
      x_serialize_decl();
    };

    std::string name;    ///< the unique name of the configuration
    std::string json;    ///< useful data to attach to the configuration
    remote_spec where{}; ///< remote server::port::queue
    xfer_spec how{};     ///< timing/how tight we try to bridge the queues.
    queue_spec what{};   ///< the name of local/remote queues to be linked.

    auto operator<=>(configuration const &) const = default;

    SHYFT_DEFINE_STRUCT(configuration, (), (name, json, where, how, what));
    x_serialize_decl();
  };

}

x_serialize_export_key_nt(shyft::dtss::q_bridge::configuration::xfer_spec);
x_serialize_export_key_nt(shyft::dtss::q_bridge::configuration::queue_spec);
x_serialize_export_key_nt(shyft::dtss::q_bridge::configuration::remote_spec);
x_serialize_export_key(shyft::dtss::q_bridge::configuration);


SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::q_bridge::configuration::xfer_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::q_bridge::configuration::queue_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::q_bridge::configuration::remote_spec);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::q_bridge::configuration);