#pragma once

#include <shyft/dtss/q_bridge/config.h>
#include <shyft/dtss/q_bridge/status.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss {
  struct server_state; // fwd
}

namespace shyft::dtss::q_bridge {
  struct engine_impl;

  /**
   * @brief q_bridge::engine
   * @details
   * The q_bridge ensure to fetch q-messages from the remote q,
   * passing them to the locally configured queue, with a msg_id that
   * is unique by prepending the  remote:port:queue.
   * When the message is consumed, and acknowledged at the local queue
   * the q_msg worker forwards the acknowledged message to the remote queue.
   *
   * For an observer at the remote queue, it will appear as the messages
   * are consumed locally.
   *
   * The engine must resolve communication errors, temporary failures,
   * and keep information, and resume operation when remote link and
   * system is up and working.
   *
   * The engine provides standard semantic functions
   * - 'start' start a configured q-bridge
   * - 'stop' stops a running q-bridge activity
   * - 'get_status' provides performance/error statistics and diagnostics
   * - 'get_configuration' provides the current configuration as passed to constructor.
   *
   * The engine is managed by the exchange::manager, as part of the dtss::server_state
   * utilizing somewhat similar operations.
   *
   * @note
   * The effective stop-time, might be influenced by the configured parameters,
   * since the workers of the engines might be busy with io-operations.
   * A realistic configuration of max io operational time when waiting for
   * remote q messages must be set.
   * Also notice, that stop/destructor times might even be longer, depending
   * on how network and io-operations times out at operating-system level.
   */
  struct engine {
    engine(server_state &server, configuration config_);
    ~engine();
    void start();
    status get_status(bool clear);
    void stop(utctime graceful_close);
    configuration get_configuration() const;
   private:
    std::unique_ptr<engine_impl> impl;
  };

}
