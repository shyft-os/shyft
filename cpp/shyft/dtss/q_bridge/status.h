#pragma once

#include <mutex>
#include <string>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::dtss::q_bridge {
  using core::utctime, core::no_utctime;

  struct error_entry {
    std::string code;         ///< some kind of diagnstics, or error message
    utctime time{no_utctime}; ///< when it was recorded
    auto operator<=>(error_entry const &) const = default;
    SHYFT_DEFINE_STRUCT(error_entry, (), (code, time));
    x_serialize_decl();
  };

  struct status {
    std::string name;             ///< name of the q_bridge
    std::size_t ack_errors{0};    ///< number of ack related errors
    error_entry last_ack_error;   ///< last ack error
    std::size_t fetch_errors{0};  ///< number of fetch msg related errors
    error_entry last_fetch_error; ///< last known fetch msg related error
    utctime last_activity;        ///< last known activity of workers
    std::size_t completed{0};     ///< number of completed msg,ack pairs
    std::size_t fetched{0};       ///< number of msg pulled from remote and pushed local

    auto operator<=>(status const &) const = default;

    SHYFT_DEFINE_STRUCT(
      status,
      (),
      (name, ack_errors, last_ack_error, fetch_errors, last_fetch_error, last_activity, completed, fetched));
    x_serialize_decl();
  };


}

x_serialize_export_key(shyft::dtss::q_bridge::error_entry);
x_serialize_export_key(shyft::dtss::q_bridge::status);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::q_bridge::error_entry);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::dtss::q_bridge::status);
