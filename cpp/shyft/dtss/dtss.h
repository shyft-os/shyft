/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <algorithm>
#include <atomic>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <functional>
#include <map>
#include <memory>
#include <regex>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <dlib/logger.h>
#include <dlib/misc_api.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/fs_compat.h>
#include <shyft/dtss/container_config.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_db.h>
#include <shyft/dtss/dtss_db_rocks.h>
#include <shyft/dtss/dtss_error.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/dtss/dtss_url.h>
#include <shyft/dtss/exchange/managers.h>
#include <shyft/dtss/geo.h>
#include <shyft/dtss/master_slave_sync.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/queue.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/url_ts_frag.h>
#include <shyft/srv/fast_iosockstream.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/expression_serialization.h>

namespace shyft::dtss {
  struct master_slave_sync;

  using ts_vector_t = shyft::time_series::dd::ats_vector;
  using ts_info_vector_t = std::vector<ts_info>;
  using id_vector_t = std::vector<std::string>;

  //==============================

  /**
   * @brief support flexible string/string_view unordered_map
   */
  struct string_hash {
    using hash_type = std::hash<std::string_view>;
    using is_transparent = void;

    std::size_t operator()(char const * str) const {
      return hash_type{}(str);
    }

    std::size_t operator()(std::string_view str) const {
      return hash_type{}(str);
    }

    std::size_t operator()(std::string const & str) const {
      return hash_type{}(str);
    }
  };

  using movable_ts_item_t = std::tuple<string_view, gts_t&>;             ///< we need a movable ts-item (ts_url, gts_t&)
  using fx_movable_ts_item_t = std::function<movable_ts_item_t(size_t)>; ///< and a callable that returns a movable
  inline static std::string const geo_cfg_file{"geo.cfg"}; ///< are stored at root of internal geo-db containers.

  /**
   * @brief A dtss server with time-series server-side functions
   * @details
   * The dtss server listens on a port, receives messages, interpret them
   * and ship the response back to the client.
   *
   * Callbacks are provided for extending/delegating find/read_ts/store_ts/remove_container,
   * as well as internal implementation of storing time-series
   * using plain binary files stored in containers(directory).
   *
   * Time-series are named with url's, and all request involving 'shyft://'
   * like
   *   shyft://<container>/<local_ts_name>
   * resolves to the internal implementation.
   *
   * geo-evaluate calls goes through the geo_evaluate engine,
   *   that uses the ordinary cache to get cached responses.
   *
   */

  struct server_callbacks {
    /// called to read non shyft:// unbound ts
    std::function<ts_vector_t(id_vector_t const & ts_ids, utcperiod p)> bind_ts;
    /// called for all non shyft:// find operations
    std::function<ts_info_vector_t(std::string search_expression)> find_ts;
    /// called for all non shyft:// store operations
    std::function<void(ts_vector_t const &)> store_ts;
    /// called for removing non shyft:// container urls on remove
    std::function<void(std::string const &, bool)> remove_external;

    /**
     * @brief geo_read
     *
     * @details
     * Returns a complete response, according to the parameters and geo_ts_db_config.
     * The first parameter provide keys/info for idx passed in next args, and the
     * second parameter specifies the slice to read.
     *
     * The 'question' parameters are needed to  interpret the
     * response.
     *
     * E.g. if you ask for only   temperature, the var-dimension will be 1,
     *      note that ghe geo_scope is the only, not specified dimension.
     *      but it can be computed using the cfg and find the exact points that
     *      are within  the geo-scope.
     *
     */
    std::function<geo::ts_matrix(std::shared_ptr<geo::ts_db_config> const &, geo::slice const &)> geo_read;
    /**
     * @brief geo_store
     *
     * @details
     * Function signature  for storing to a geo_ts_db. Notice that we only support complete consistent
     * ts_matrix, where all the dimensions v,e,g must be consistent  with the database.  We support storing multiple
     * t0's at once, as long as each v,e,g dimension is complete
     * The first parameter provide keys/info for idx passed in next args, the second parameter specifies the
     * geo matrix to store, and the last parameter specifies whether to replace if true or merge if false.
     */
    std::function<void(std::shared_ptr<geo::ts_db_config>, geo::ts_matrix const &, bool)> geo_store;
  };

  struct server_state {
    explicit server_state(server_callbacks const & callbacks);
    server_state();
    ~server_state();

    server_callbacks callbacks;
    std::mutex c_mx;
    std::string cfg_file; ///< if configured, the container list is persisted/maintained here.
    using container_t = std::unordered_map<std::string, std::unique_ptr<its_db>, string_hash, std::equal_to<>>;
    container_t container;                                    ///< mapping of internal shyft <container>
    std::unordered_map<std::string, geo::ts_db_config_ > geo; ///< mapping of geo_evaluate requests goes through this
    db_cfg default_geo_db_cfg; ///< the level/rocks db cfg for defaulted geo databases, created by clients.
    std::string default_geo_db_type{"ts_rdb"}; ///< the default db type to create
    cache ts_cache{1000000};                   ///< default 1 mill ts in cache

    bool cache_all_reads{false};
    bool can_remove{false};
    std::atomic_bool terminate{false};
    std::unique_ptr<master_slave_sync> msync;
    subscription::manager_ sm = make_shared<core::subscription::manager>();
    std::atomic_size_t alive_connections{0};
    std::atomic_size_t failed_connections{0};
    queue::q_mgr queue_manager;
    exchange::manager exchange_manager;
  };

  //-- persistent container config storage functions.
  /**
   * @brief give the dtss memory of containers
   * @details
   * If called early, and non-empty file, will restore
   * the containers as pr file content.
   * If called later, .. , update, add containers
   *  as pr. file content.
   * Then persist updated.
   * In all cases, any add/remove of containers
   * also update the contents.
   */
  void set_container_cfg(server_state& state, std::string const & cfg_path);
  /**
   * @brief used to reconstruct correct db at location
   */
  void reconstruct_db_at(
    server_state& state,
    std::string const & c_name,
    std::string const & c_pth,
    db_cfg const & default_cfg);

  /**
   * @brief scan container for and read a geo-ts-db configuration
   * @details scan a container directory for a geo.cfg file and attempts to load it.
   * @param root points to the container directory.
   * @return optionally returns a  geo-ts-db config if file found and load successful
   */
  std::optional<geo::ts_db_config_> geo_ts_db_scan(server_state& state, fs::path root);

  //-- container management, that you can optionally override,
  void add_container(
    server_state& state,
    std::string const & container_name,
    std::string const & root_dir,
    std::string container_type = std::string{},
    db_cfg cfg = db_cfg{});

  /**
   * @brief Run maintenance on specified container.
   * @details
   * The current backend rocksdb supports running compaction and re-arrangement
   * of the files and indexes of it backend.
   * This function perform maintenance on the 'info' parts as well as the 'data' parts
   * according to supplied flags.
   * The operation might take a long time depending on the size of the DB, potentially rewriting
   * all the files. So if DB total size is 1 TB, then expect 1 TB read and one TB write operations.
   * @param container_name the name of the container (must exist, and must be supported backend kind)
   * @param info_db run maintenance on info_db parts (e.g.lookup ts-name to ts-id/meta-info)
   * @param data_db run maintenance on data_db parts (the key/value store for ts-fragments)
   */
  void maintain_backend_container(
    server_state& state,
    std::string const & container_name,
    bool const info_db,
    bool const data_db);


  // removes internal or external container. If remove_from_disk is set, the contained data is permanently deleted.
  void remove_container(server_state& state, std::string const & container_url, bool remove_from_disk);


  /**
   * @brief swap_container
   * @details
   * Given two containers, swap the name link to the backend.
   *
   * So a container 'a' -> backend db *a and
   * and a second container 'b' -> backend db *b.
   *
   * After the atomic operation:
   *  'a' -> backend *b
   *  'b' -> backend *a
   *
   * Preconditions:
   *
   *  - the content of  'a' and 'b' is identical at the time when calling swap
   *  - the caller is responsible for the above condition
   *  - the cache content if 'a'
   *
   * Intended usage:
   *
   * When migrating immutable(for the period of migration) containers from one storage/format
   * to another.
   *
   * (1) first copy 'a' to 'b', ensure that 'b' is consistent with 'a', for large amount of data, also write to 'b'
   * with no cache
   *
   * (2) call swap_container ('a', 'b')
   *
   * (3) eventually remove_container 'b' (because it denotes the old 'a' !)
   *
   * Limitiations and notes:
   *
   *  - only work for shyft interal containers, external db might have other techniques to achieve the same
   *
   *  - the cache of 'a' items (and b-items if any) is still valid given precondition is fulfilled
   *
   *
   * @param container_name_a[in] the name of the shyft container, like 'hydrology'
   * @param container_name_b[in] the name of the other shyft container, like 'hydrology_tmp'
   */
  void swap_container(server_state& state, std::string const & container_name_a, std::string const & container_name_b);

  server_state::container_t::iterator container_find(server_state& state, std::string_view container_name);
  its_db& internal(server_state& state, std::string_view container_name);
  its_db* internal_ptr(server_state& state, std::string_view container_name);


  ts_info_vector_t do_find_ts(server_state& state, std::string const & search_expression);
  ts_info do_get_ts_info(server_state& state, std::string const & ts_url);

  void do_cache_update_on_write(server_state& state, ts_vector_t tsv, vector<utcperiod> const & tsv_tp, bool recreate);

  void do_store_ts(server_state& state, ts_vector_t tsv, store_policy p);

  /**
   * @brief do_local_store main worker function
   * @details
   * This function is the main function, that the other do_store_xx
   * reflects to.
   * Based on the policy `p`, the `n` ts_items as returned by the callable fx
   * is stored to the backend.
   * The storage strategy is default throw on first error,
   * but the store_policy.best_effort can be used to rather continue
   * storing time-series and report failures for those that failed instead.
   *
   * @param n number of ts items that should be stored
   * @param fx a callable (size_t i)->movable_ts_item_t (string_view, gts_t &) for all n
   * @param p store policy, including best-effort, recreate, etc.
   * @returns diags_t, optional, empty if all ok, otherwise diags for the failed items.
   */
  diags_t do_local_store(server_state& state, size_t n, fx_movable_ts_item_t const & fx, store_policy p);

  void do_merge_store_ts(server_state& state, ts_vector_t tsv, bool cache_on_write);


  /**
   * @brief Best effort read shyft:// time-series for specified period
   *
   * @param ts_ids identifiers, url form, where shyft://.. is specially filtered
   * @param p the period to read
   * @param use_ts_cached_read allow reading results from already existing cached results
   * @param update_ts_cache when reading, also update the ts-cache with the results
   * @return tuple, read ts-vector in the order of the ts_ids (null if failed), error with index and error code
   */
  tuple<vector<ts_frag>, vector<utcperiod>, vector<read_error>> try_read(
    server_state& state,
    id_vector_t const & ts_ids,
    utcperiod p,
    bool use_ts_cached_read,
    bool update_ts_cache);

  void do_remove_ts(server_state& server, std::string const & ts_url);
  void do_bind_ts(
    server_state& state,
    utcperiod bind_period,
    ts_vector_t& atsv,
    bool use_ts_cached_read,
    bool update_ts_cache);
  ts_vector_t do_evaluate_ts_vector(
    server_state& state,
    utcperiod bind_period,
    ts_vector_t& atsv,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period);
  ts_vector_t do_evaluate_percentiles(
    server_state& state,
    utcperiod bind_period,
    ts_vector_t& atsv,
    gta_t const & ta,
    std::vector<int64_t> const & percentile_spec,
    bool use_ts_cached_read,
    bool update_ts_cache);


  std::tuple<vector<ts_frag>, vector<utcperiod>, vector<read_error>>
    try_slave_read(server_state& state, id_vector_t const & ts_ids, utcperiod p, bool use_ts_cached_read);

  /**
   * @brief assign master dtss for this dtss
   * @details
   * This changes the mode of operation for this dtss instance.
   *
   * All physical IO requests, and GEO requests are forwarded to the
   * designated master dtss as specified with the ip and port number.
   *
   * The local dtss cache and subscriptions are kept in sync with the master
   * with the given parameters.
   *
   * @param ip          the ip address of the master dtss
   * @param port        the port number of the master dtss instance
   * @param master_poll_time the maximum number of seconds to wait between polling the master for relevant
   * changes(subscriptions)
   * @param unsubscribe_min_threshold the minimum number of unsubscription events before propagating it to unsubscribe
   * from master
   * @param unsubscribe_max_delay the maximum time to wait, regardless count, before unsubscribeing wasted items
   * @param protocol_version version to run the sync over, defaults to internal protocol
   */
  void set_master(
    server_state& state,
    std::string ip,
    int port,
    double master_poll_time,
    size_t unsubscribe_min_threshold,
    double unsubscribe_max_delay,
    version_type protocol_version = internal_version);

  /**
   * @brief cleanly shuts down master-slave sync, and destroys the master object
   */
  void clear_master(server_state& state);

  /**
   * @brief geo_evaluate
   *
   * Peforms the geo-evaluate server side work
   *
   * @param eval_args specifies the scope of geo-evaluation
   * @param use_cache use the dtss cache, if available
   * @param update_cache stash any new reads into cache
   * @return a vector of the results, of ta.size() length, one for each t0, or in case of concat just one
   */
  geo::geo_ts_matrix
    do_geo_evaluate(server_state& server, geo::eval_args const & eval_args, bool use_cache, bool update_cache);

  /**
   * @brief do_geo_store
   *
   * @details
   * Stores the supplied ts-matrix to the backend.
   *
   * @param geo_db_name the name of the geo data-base
   * @param tsm 1 or more t0 dimensions of complete variable,ens, geo dimensions to store
   * @param replace if there already exis forecasts/time-series, this will replace, merge with previous
   * @param cache if true, also put the time-series to in-memory cache for fast retrieval
   *
   */
  void do_geo_store(
    server_state& server,
    std::string const & geo_db_name,
    geo::ts_matrix const & tsm,
    bool replace,
    bool cache);

  vector<utcperiod>
    do_internal_geo_store(server_state& server, geo::ts_db_config_ cfg, geo::ts_matrix const & tsm, bool replace);

  std::pair<geo::ts_matrix, vector<utcperiod>>
    do_internal_geo_read(server_state& server, geo::ts_db_config_ const & cfg, geo::slice const & gs, bool use_cache);

  std::pair<geo::ts_matrix, vector<utcperiod>>
    do_external_geo_read(server_state& state, geo::ts_db_config_ const & cfg, geo::slice const & gs);

  /**
   * @brief update geo ts db meta info
   * @details
   * Update the 'meta' info part of the geo database.
   * with the supplied parameters.
   *
   */
  void do_update_geo_db(
    server_state& state,
    std::string const & geo_db_name,
    std::string const & description,
    std::string const & json,
    std::string const & origin_proj4);

  /** @brief get geo_ts info available on the server */
  vector<geo::ts_db_config_> do_get_geo_info(server_state& state);

  /** @brief get name of all containers set on the server */
  id_vector_t do_get_container_names(server_state& state);


  /**
   * @brief add a geo_ts db container/service
   *
   * @details
   * adds a geo-ts db according to the
   * instance passed.
   * If there is already registered, exception is thrown.
   * We require that user first call remove_geo_ts_db
   *
   */
  void add_geo_ts_db(server_state& state, geo::ts_db_config_ const & cfg);

  /** @brief remove a geo_ts_db, unregister/flush cache */
  void remove_geo_ts_db(server_state& state, std::string const & geo_db_name);


  /**
   * @brief store current state of internal geo_ts_cfg
   * @details store the geo::ts_db_config to specified root dir so that
   * it can be found later, and used to bootstrap the geo-part of the dtss.
   * It overwrites/replaces any other file at the location.
   *
   * @param root the root of the ts_db container, the full filename is formed using root/name/geo_cfg_file
   * @param cfg the geo::ts_db_config to store
   */

  void do_geo_ts_cfg_store(server_state& state, std::string const & root, geo::ts_db_config_ const & cfg);

  /**
   * @brief add container on this or master
   */
  void do_set_container(
    server_state& state,
    std::string const & name,
    std::string const & path,
    std::string type,
    db_cfg cfg);
  /**
   * @brief remove container from this or master
   */
  void do_remove_container(server_state& state, std::string const & container_url, bool remove_from_disk);

  /**
   * @brief swap container from this or master
   */
  void do_swap_container(server_state& state, std::string const & a, std::string const & b);


} // shyft::dtss
