/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <mutex>
#include <ranges>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include <dlib/logger.h>

#include <shyft/dtss/db_cfg.h>
#include <shyft/dtss/diagnostics.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_error.h>
#include <shyft/dtss/geo.h>
#include <shyft/dtss/protocol/message_tags.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/url_ts_frag.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/geo_ts.h>

namespace shyft::dtss {
  using shyft::core::utcperiod;
  using shyft::core::no_utctime;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::dd::aref_ts;
  using id_vector_t = std::vector<std::string>;
  using ts_info_vector_t = std::vector<ts_info>;
  using ts_vector_t = shyft::time_series::dd::ats_vector;


  // fwd decl.
  struct client;
  using client_ = std::shared_ptr<client>;
  struct server_state;

  /**
   * @brief compute_gross_period
   * @details given `a`, extend it with `b`
   * Ensure to use the valid of the two, and disregard no_utctime
   * @param a current gross-period
   * @param b possible extension to gross-period
   * @return gross-period
   */
  inline utcperiod compute_gross_period(utcperiod const &a, utcperiod const &b) noexcept {
    auto r = a.valid() ? a : b; // regardless, use the valid period
    if (a.start != no_utctime && b.start != no_utctime)
      r.start = std::min(a.start, b.start);
    if (a.end != no_utctime && b.end != no_utctime)
      r.end = std::max(a.end, b.end);
    return r;
  };

  /** @brief client side master-slave sync item */
  struct ts_sub_item {
    utcperiod p;          ///< keep track of grand period we are subscribing to
    std::string const id; // need this to promise it is never changing after created.

    template <class S>
    ts_sub_item(utcperiod p, S &&s)
      : p{p}
      , id{std::forward<S>(s)} {
    }

    auto operator<=>(ts_sub_item const &) const = default;

    void extend_period(utcperiod const &x) noexcept {
      p = compute_gross_period(p, x);
    }
  };

  namespace detail { // stash we need to operate transparently with guaranteed pointer to string instead of strings

    struct str_ptr_hash {
      inline std::size_t operator()(std::string const *s) const noexcept {
        return std::hash<std::string>{}(*s);
      }
    };

    struct str_ptr_eq {
      inline bool operator()(std::string const *lhs, std::string const *rhs) const noexcept {
        return *lhs == *rhs;
      }
    };
  }

  /**
   * @brief ts subscription map
   * @details
   * The ts_sub_map is used at the dtss client slave to keep a local registry of subscriptions
   * We use unordered_map, with the keys being stored in the members of the map, avoiding to copy
   * strings (that could be long). This is similar to the subscription manager strategy(have a look at sm.active
   * map).
   */
  using ts_sub_map =
    std::unordered_map<std::string const *, std::unique_ptr<ts_sub_item>, detail::str_ptr_hash, detail::str_ptr_eq>;

  using ts_sub_period_partition_map =
    std::unordered_map<utcperiod, std::vector<ts_sub_item const *>, core::utcperiod_hasher>;

  inline ts_sub_period_partition_map make_same_period_partitions(ts_sub_map const &m) {
    ts_sub_period_partition_map r;
    for (auto const &e : m)
      r[e.second->p].push_back(e.second.get());
    return r;
  };

  /**
   * @brief dtss master slave sync
   *
   * @details
   * Keeps the dtss slave in sync with the master for the minimal
   * list of time-series known to the users of the slave dtss.
   *
   * The minimal list of time-series is
   *
   *   (a) the ones with active subscription (through subscription-manager, typically
   *       setup on the web-api on websockets.)
   *   (b) any time-series in the cache(regardless subscription).
   *       (these needs to be in sync with master in the case there are read requests
   *       that will be satisfied using the cached value)
   *
   *
   * This class implements the vital parts of orchestrating this logic.
   *
   * Things to know:
   *
   * (1) The slave forwards all io- operations to the master,
   *   but takes care of all computation and eventual caching  of
   *   the time-series as read from the master.
   *
   * (2) Since the slave have local memory it needs to be notified, and
   *   updated if there are any changes to the time-series at the master.
   *
   * (3) On the master side, this is kept as a local subscription kept
   *   at lifetime equal to the connection lifetime. Thus we rely on long(er)
   *   or stable connections between master and slaves. The slave, regularly polls the master
   *   about the changes in the subscribed set of timeseries.
   *   Since this is kept on the master, there is minimal communication overhead.
   *
   * (4) The Client ALSO keeps a list of the subscribed time-series (along with the grand period
   *   of interest in case of several fragments(currently not supporting smart frags))
   *   This allow the client on its own to figure out if it should unsubscribe
   *   timeseries on the events of web-api unsubscribe, or later a cache-eviction of
   *   a cached timeseries (both of them require a live subscription to be valid and working).
   *   A nice side-effect of the client side register is that the client is capable of
   *   restoring the subscriptions in the case of communication failure, or server restart.
   *   Special notice as found in issue #1233, the client and server subs must incorporate
   *   the possible surrounding read period that will occur for break-points.
   *
   * (5) The direct cost of the subscription in terms of memory is the size of the ts-ids plus 8 bytes,
   *   applies to both server and client (both have a copy).
   *
   * (6) The computational cost for the writer is the hash-lookup and the atomic increment of the version
   *   number. The computation cost for the observer, is recomputing/comparing published view against
   *   the new current value of the view (for time-series version, this boils down to checking the ts
   *   version number).
   *
   * (7) There is only one raw socket connection between the master..slave. It is shared between
   *   the master-slave-sync thread worker, and ALL other threads that does ts terminal-IO ops
   *   (e.g. reading/writing finding/removing etc.)
   *   This single rawsocket connection, is protected by a mutex, ensuring sequential access.
   *   (it is possible to have several sockets, but we doubt the benefit, and it would require
   *   a raw-socket protocol similar to websocket(a subject agreed to by both parties that serve as
   *   the mailbox on the serverside).
   *
   *(8) The master-slave sync thread worker performs these tasks:
   *   a. polls for changes at regular intervals (we could piggyback changes on other requests..)
   *   b. try to minimize the master subscription by monitoring slave-side un-subscriptions
   *      and cache evictions.
   *   c. keeps the slave side structures(with mx protection).
   *
   *(9) The OTHER threads, that do terminal IO as described above, also uses the same socket
   *    to satisfy basic io requirements and
   *   a. update sthe master-slave-sync with *NEW* subscriptions when writing
   *   b. DO NOT cache locals writes, rather let read/subscription from dtss master update if/when done
   *      to ensure that what is observed on the local cache is always the truth propagated from
   *      the main server. (we can wait for it, it can take some millisecs)
   *   c. We have a piggyback feature, also take care of processing the payload from
   *      the server, e.g. push updates to local cache, and notify subscriptions.
   *
   * (10) mutex and thread locks considerations
   *   a. order of mutex acquire (if multiple) must be guaranteed to avoid deadlocks (worker vs. clients)
   *   b. `master_mx` for the shared dts client `master` (worker and clients)
   *   c. `subs_mx` for the shared access to the subs we maintain (worker and client)
   *
   *   In addition, be aware of the dtss.cache (that are thread safe)
   *   and also dtss.sm.mx (the dtss subscription manager do have a mutex, and in certain parts, we might need
   *   to grab this, for example when efficiently figuring out what the active subscription set are)
   *
   *   Client communication can take long time (compared to anything else).
   *   Part of the work, especially for the 'worker' using subs are short/fast local memory operations.
   *
   *   The 'worker' typically first inspects the 'subs', then do IO using the master dts_client.
   *   The 'client's, the opposite, first using master, then when finished updating the 'subs'.
   *
   *   The client could update subs first, but, if the read operation fails, the subs would be in an
   *   invalid state (e.g keeping refs. to time-series that does not exist).
   *
   *   Regardless state updating, the 'subs' could be invalidated by actions at the remote
   *   master dtss, e.g. time-series are removed (bad habit), thus invalidating the
   *   'subs' that would try to update/read during the 'repair_subs' action.
   *   We have tried to make this work by using try_read, and to skip the 'not working' items.
   *
   *   One strategy could be, if an operation require both `master` and `subs`, (as for the client),
   *   first to acquire `master` only, then release it, then acquire the `subs` for the subs operation.
   *   E.g. not acquiring both in an operation.
   *   This opens for glitches in state between the remote and the local subs.
   *   The other strategy is to always use order `lock {master,subs}` if acquire both,
   *   otherwise just one of them at a time (release master before taking subs, for clients).
   */
  struct master_slave_sync {
    static dlib::logger dlog; ///< Since we do have interface/io, we keep log capability

    master_slave_sync(
      server_state &slf,
      std::string ip,
      int port,
      double master_poll_time,
      size_t unsubscribe_min_threshold,
      double unsubscribe_max_delay,
      version_type protocol_version = internal_version);
    server_state &state;
    // from server, we are interested in
    // A. minimum cost event indicators:
    //  (1) the cache (knowing about evicted items)
    //  (2) the sm (knowing about unsubscribed items)
    // B. computing exact updated subscription scope reduction
    //   based on
    //    (1) self.cache items (all of them are subject to subs)
    //    (2) the active sm
    // Assuming our own subs and the above is same size(except when the one are zero)
    //  .. collect the set of ts_ids from subs and cache
    //  .. the  section of subs and (union of self.subs, self.cache)
    //  ..    can be removed..

    // data we keep
    std::recursive_mutex mx_subs; ///< mx for the subs(at least..)
    ts_sub_map subs; ///< these are the one we are keeping track of at the client side, so that we can unsubscribe, or
                     ///< recover lost io conn
    utctime subs_updated{no_utctime}; ///< last time subs was updated from master, protected by mx_subs
    std::recursive_mutex mx_master;   ///< mx for the master(at least..)
    client_ master; ///< the raw socket interface to the master (we should allow this to fail, with repair).
    std::atomic_uint_fast64_t reconnect_count{
      0l}; ///< keep track of lost connections to dtss (because we must repair subs if it happens)

    // -- parameters for the worker
    double max_defer_update_time{1.0};     ///< maximum time to defer a poll-cycle(if few subs)
    size_t unsubscribe_minimum_count{100}; ///< minimum unsubscribed entities before direct un-subscribe is done
    double master_poll_time{0.01};         ///< maximum delay before worker thread synchronizes with master
    std::future<void> capture_worker;
    void worker(); ///< the worker thread main routine

    /**
     * @brief repair subs
     * @details
     * If the connection to the master was broken (as in really lost, and
     * reconnected).
     * The repair_subs(), simply does a
     * full read of the existing subs,
     * cache and notify.
     */
    void repair_subs(bool keep_trying = false);

    /**
     * @brief safely add subs for specified period
     * @param id_ps a tuple of id,read_period
     * @param p the read request period (the read_period could be larger due to surrounding read)
     * */
    template <typename R>
    requires std::ranges::input_range<R>
          && std::convertible_to<std::ranges::range_reference_t<R>, std::tuple<std::string, utcperiod>>
    void add_subs(R &&id_ps, utcperiod p) {
      std::scoped_lock _(mx_subs);
      std::ranges::for_each(SHYFT_FWD(id_ps), [&](auto &&pack) {
        auto const &[id, id_p] = pack;
        auto gross_p = compute_gross_period(id_p, p); // use the larger of read-period and effective read period
        auto f = subs.find(&id);
        if (f == subs.end()) {
          auto i = std::make_unique<ts_sub_item>(gross_p, SHYFT_FWD(id));
          subs[&i->id] = std::move(i); // important to move
        } else {
          f->second->extend_period(gross_p);
        }
      });
    }

    /**
     * Let all interaction with the master dtss go through this class
     * so that we easily can control the local subs, as well as doing
     * smart recovery of lost connections.
     */
    std::tuple<std::vector<ts_frag>, std::vector<utcperiod>, std::vector<read_error>>
      try_read(id_vector_t const &ts_urls, utcperiod p, bool use_ts_cached_read);

    ts_info_vector_t find(std::string const &search_expression);
    void store_ts(ts_vector_t tsv, store_policy p);
    diags_t store(std::vector<url_ts_frag> fragments, store_policy p);

    void merge_store_ts(ts_vector_t const &tsv, bool cache_on_write);
    ts_info get_ts_info(std::string const &ts_url);
    void remove(std::string const &name);
    bool has_subscription(std::string_view container_url);

    //-- geo related, currently just pass through, to ensure that it still works
    geo::geo_ts_matrix geo_evaluate(geo::eval_args const &ea, bool use_cache, bool update_cache);
    void geo_store(std::string const &geo_db_name, geo::ts_matrix const &tsm, bool replace, bool update_cache);
    std::vector<geo::ts_db_config_> get_geo_ts_db_info();
    void add_geo_ts_db(geo::ts_db_config_ const &gdb);
    void remove_geo_ts_db(std::string const &geo_db_name);
    void update_geo_db(
      string const &geo_db_name,
      string const &description,
      string const &json,
      string const &origin_proj4);
    void set_container(std::string const &name, std::string const &path, std::string type, db_cfg cfg);
    void remove_container(std::string const &container_url, bool remove_from_disk);
    void swap_container(std::string const &a, std::string const &b);
  };
}
