#pragma once

#include <cstdint>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>

namespace shyft::core {
#define SHYFT_SERIALIZATION_CHOICE (all, stripped)
  SHYFT_DEFINE_ENUM(serialization_choice, std::uint8_t, SHYFT_SERIALIZATION_CHOICE);
}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::core::serialization_choice);
