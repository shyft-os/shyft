
#include <shyft/core/boost_wrapped_binary_archive.h>
#include <shyft/core/serialization_choice.h>

// explicitly instantiate for this type of stream
#include <boost/archive/impl/archive_serializer_map.ipp>
#include <boost/archive/impl/basic_binary_iarchive.ipp>
#include <boost/archive/impl/basic_binary_iprimitive.ipp>
#include <boost/archive/impl/basic_binary_oarchive.ipp>
#include <boost/archive/impl/basic_binary_oprimitive.ipp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include "reflection/serialization.h"

using shyft::core::serialization_choice;

namespace shyft::core::detail {
  /**
   * @brief peek out the magic
   * @details
   * peeking & read byte by byte, and unput if no match.
   * Try to return stream in the state/position it was if not a magic.
   * (proven hard to fix for socket kind of stream, so only for string/file streams)
   * @param is the input stream
   * @return 0 or magic_value if magic read
   */
  std::int32_t read_magic(std::istream& is) {
    static std::int32_t const magic = archive_info::magic_value;
    static auto const expected = reinterpret_cast<char const *>(&magic); // The expected bytes
    char c{0};
    for (int i = 0; i < 4; ++i) {
      if (is.peek() == expected[i]) {
        is.read(&c, 1);
      } else {
        for (int j = 0; j < i; ++j) { // Read failed: unget the already read characters, if necessary
          is.unget();
        }
        return 0; // no magic, return unmodified stream(given stringstream fstream family)
      }
    }
    return magic; // Successfully read and matched complete magic
  }

  /**
   * @brief load archive info
   * @details
   *
   * Probe and if magic, load the archive info.
   * If it does not start with a magic, then false is returned,
   * and the input-stream is at its start position.
   * The `ai` magic is 0 if it fails.
   *
   * The input stream must be able to take at most 3 istream.unget().
   * This seems to be correct for sstream and fstream.
   *
   * @param is the input stream, that should provide stable .unget()
   * @param ai the read archive info.
   * @return true if archive info was successfully read(implies a correct magic), otherwise false
   */
  bool load_archive_info(std::istream& is, archive_info& ai) {
    ai.magic = read_magic(is); // 0 is returned if no magic was read, stream is unmodified in this case
    if (ai.magic == 0)         // not an error, so return false
      return false;
    is.read(reinterpret_cast<char*>(&ai.shyft_version), sizeof(ai.shyft_version));
    if (is.fail())
      throw boost::archive::archive_exception(
        boost::archive::archive_exception::input_stream_error, "invalid archive header read: shyft_version");
    is.read(reinterpret_cast<char*>(&ai.boost_version), sizeof(ai.boost_version));
    if (is.fail())
      throw boost::archive::archive_exception(
        boost::archive::archive_exception::input_stream_error, "invalid archive header read: boost_version");
    is.read(reinterpret_cast<char*>(&ai.optionals), sizeof(ai.optionals));
    if (is.fail())
      throw boost::archive::archive_exception(
        boost::archive::archive_exception::input_stream_error, "invalid archive header read: optionals");
    return true;
  }

  /**
   * @brief save archive info
   *
   * @details
   * The format is enforced
   * using forced format of 4 x 4byte ints(we are currently only on small-endian, so no check for that)
   * It throws if os.fail() after writing the bytes.
   * @see load_archive_info
   * @param os stream to write to
   * @param ai tha archive info to write
   */
  void save_archive_info(std::ostream& os, archive_info const & ai) {
    os.write(reinterpret_cast<char const *>(&ai.magic), sizeof(ai.magic));
    os.write(reinterpret_cast<char const *>(&ai.shyft_version), sizeof(ai.shyft_version));
    os.write(reinterpret_cast<char const *>(&ai.boost_version), sizeof(ai.boost_version));
    os.write(reinterpret_cast<char const *>(&ai.optionals), sizeof(ai.optionals));
    if (os.fail())
      throw boost::archive::archive_exception(
        boost::archive::archive_exception::output_stream_error, "failed to write archive info header");
  }

}

namespace boost::archive {
  // implement constructors
  template <serialization_choice M>
  shyft_wrapped_bin_oarch<M>::shyft_wrapped_bin_oarch(std::ostream& os, unsigned int flags)
    : boost::archive::
        binary_oarchive_impl<shyft_wrapped_bin_oarch<M>, std::istream::char_type, std::istream::traits_type>(
          os,
          (flags & ~shyft::core::archive_info::archive_info_flag)) {
    if (0 == (flags & boost::archive::no_header))
      boost::archive::
        binary_oarchive_impl<shyft_wrapped_bin_oarch<M>, std::istream::char_type, std::istream::traits_type>::init(
          flags);
    if (0 != (flags & shyft::core::archive_info::archive_info_flag)) {
      shyft::core::detail::save_archive_info(os, shyft::core::archive_info::current());
    }
  }

  template <serialization_choice M>
  shyft_wrapped_bin_iarch<M>::shyft_wrapped_bin_iarch(std::istream& is, unsigned int flags)
    : boost::archive::
        binary_iarchive_impl<shyft_wrapped_bin_iarch<M>, std::istream::char_type, std::istream::traits_type>(
          is,
          (flags & ~shyft::core::archive_info::archive_info_flag)) {
    if (0 == (flags & boost::archive::no_header))
      boost::archive::
        binary_iarchive_impl<shyft_wrapped_bin_iarch<M>, std::istream::char_type, std::istream::traits_type>::init(
          flags);
    if (0 != (flags & shyft::core::archive_info::archive_info_flag)) {
      shyft::core::archive_info v;
      if (shyft::core::detail::load_archive_info(is, v)) {
        this->archive_info = v;
      } else {
        this->archive_info.magic = 0; // invalidate magic
      }
    }
  }
}

#define SHYFT_INSTANTIATE_OSTREAM_LAMBDA(r, data, elem) \
  namespace boost::archive { \
    template class detail::archive_serializer_map<shyft_wrapped_bin_oarch<serialization_choice::elem>>; \
    template class basic_binary_oprimitive< \
      shyft_wrapped_bin_oarch<serialization_choice::elem>, \
      std::istream::char_type, \
      std::istream::traits_type>; \
    template class basic_binary_oarchive<shyft_wrapped_bin_oarch<serialization_choice::elem>>; \
    template class binary_oarchive_impl< \
      shyft_wrapped_bin_oarch<serialization_choice::elem>, \
      std::istream::char_type, \
      std::istream::traits_type>; \
  } \
  template struct boost::archive::shyft_wrapped_bin_oarch<serialization_choice::elem>;
BOOST_PP_LIST_FOR_EACH(SHYFT_INSTANTIATE_OSTREAM_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_SERIALIZATION_CHOICE))
#undef SHYFT_INSTANTIATE_OSTREAM_LAMBDA
#define SHYFT_INSTANTIATE_ISTREAM_LAMBDA(r, data, elem) \
  namespace boost::archive { \
    template class detail::archive_serializer_map<shyft_wrapped_bin_iarch<serialization_choice::elem>>; \
    template class basic_binary_iprimitive< \
      shyft_wrapped_bin_iarch<serialization_choice::elem>, \
      std::istream::char_type, \
      std::istream::traits_type>; \
    template class basic_binary_iarchive<shyft_wrapped_bin_iarch<serialization_choice::elem>>; \
    template class binary_iarchive_impl< \
      shyft_wrapped_bin_iarch<serialization_choice::elem>, \
      std::istream::char_type, \
      std::istream::traits_type>; \
  } \
  template struct boost::archive::shyft_wrapped_bin_iarch<serialization_choice::elem>;
BOOST_PP_LIST_FOR_EACH(SHYFT_INSTANTIATE_ISTREAM_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_SERIALIZATION_CHOICE))
#undef SHYFT_INSTANTIATE_ISTREAM_LAMBDA
