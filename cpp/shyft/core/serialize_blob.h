#pragma once
#include <boost/serialization/binary_object.hpp>

#include <shyft/core/core_serialization.h>

namespace shyft::core {

  // utility to store/load T that we are binary layout

  template <class Archive, class T>
  void store_blob(Archive &oa, std::vector<T> const &v) {
    oa << core_nvp("n", static_cast<std::uint64_t>(v.size()))
       << core_nvp("b",boost::serialization::make_binary_object(v.data(), sizeof(T) * v.size()));
  }

  template <class Archive, class T>
  void load_blob(Archive &ia,std::vector<T> &v) {
    std::uint64_t n{0u};
    ia>>core_nvp("n",n);v.resize(n);
    ia>>core_nvp("b",boost::serialization::make_binary_object(v.data(), sizeof(T) * v.size()));
  }

}