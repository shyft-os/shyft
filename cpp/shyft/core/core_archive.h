/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <utility>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/boost_wrapped_binary_archive.h>

namespace shyft::core {

  ///< flag for all archive, turns off boost/os header, so that archive can be read on lin/win amd64(+rules)
  inline constexpr auto core_arch_flags = boost::archive::archive_flags::no_header;

  ///< flag for archives that are stored to disk/backend, adds magic/header info
  inline constexpr auto arch_info_flags = core_arch_flags | archive_info::archive_info_flag;


  using core_iarchive = boost::archive::shyft_wrapped_bin_iarch<serialization_choice::all>;
  using core_oarchive = boost::archive::shyft_wrapped_bin_oarch<serialization_choice::all>;

  using core_iarchive_stripped = boost::archive::shyft_wrapped_bin_iarch<serialization_choice::stripped>;
  using core_oarchive_stripped = boost::archive::shyft_wrapped_bin_oarch<serialization_choice::stripped>;

  // note that no_header is required when doing compatible-enough
  // binary archives between the ms c++ compiler and gcc
  // The difference is that ms c++ have 32bits ints (!)
  //

  // core_nvp is a stand-in for make_nvp (could resolve to one)
  // that ensures that the serialization using core_nvp
  // can enforce use of properly sized integers enough to
  // provide serialization compatibility between ms c++ and gcc.
  // ref. different int-sizes, the strategy is to
  // forbid use of int which is different.

  template <class T>
  inline T&& core_nvp(char const *, T&& t) {
    static_assert(
      !std::is_same_v<std::int32_t, T>, "serialization require non-32bits ints(to ensure win/linux compat)");
    return std::forward<T>(t);
  }

// register all archives:
#define x_arch(T) \
  x_serialize_archive(T, shyft::core::core_oarchive, shyft::core::core_iarchive); \
  x_serialize_archive(T, shyft::core::core_oarchive_stripped, shyft::core::core_iarchive_stripped)
/** Use this macro in implementation files to register classes and instantiate templates.
 */
#define x_poly_serialize_instantiate_and_register(C) \
  x_poly_serialize_implement(C); \
  x_arch(C);

#define x_serialize_instantiate_and_register(C) \
  x_serialize_implement(C); \
  x_arch(C);

}
