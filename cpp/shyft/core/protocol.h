#pragma once

#include <chrono>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <thread>
#include <variant>

#include <boost/container/flat_map.hpp>
#include <boost/mp11/algorithm.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/vector.hpp>
#include <fmt/core.h>

#include <shyft/core/boost_serialization_flat_map.h>
#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/boost_serialization_std_string_view.h>
#include <shyft/core/boost_serialization_std_variant.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/core/optional.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/srv/fast_server_iostream.h>

namespace shyft::protocols {

  struct basic_io {
    core::srv_connection &connection;

    auto operator()(auto &&func) {
      if (!connection.is_open)
        connection.open();
      return std::invoke(SHYFT_FWD(func), connection);
    }
  };

  struct io_with_retry {
    core::srv_connection &connection;
    core::scoped_connect scoped_connect{connection};

    auto operator()(auto &&func) {
      return core::do_io_with_repair_and_retry(connection, SHYFT_FWD(func));
    }
  };

  template <reflected_enum message_tag>
  struct basic_protocol {
    auto operator<=>(basic_protocol const &) const = default;
  };

  template <typename version_type, typename message_tag_type, template <version_type> class versioned_message_tag>
  struct versioned_protocol {
    version_type internal_version, min_version, num_versions;
    message_tag_type error_tag = 0x0;
    bool compatability_mode = false;

    auto operator<=>(versioned_protocol const &) const = default;
  };

  namespace detail {
    template <typename protocol>
    struct header_impl;

    template <reflected_enum message_tag>
    struct header_impl<basic_protocol<message_tag>> {
      using type = std::underlying_type_t<message_tag>;
    };

    template <typename vtype, typename message_tag_type, template <vtype> class versioned_message_tag>
    struct header_impl<versioned_protocol<vtype, message_tag_type, versioned_message_tag>> {
      using type = message_tag_type;
    };
  }

  template <auto protocol>
  using header_type = typename detail::header_impl<decltype(protocol)>::type;

  namespace detail {
    template <typename protocol>
    struct messages_impl;

    template <reflected_enum mt>
    struct messages_impl<basic_protocol<mt>> {
      using type = mt;
    };
  }

  template <auto protocol>
  using messages = typename detail::messages_impl<decltype(protocol)>::type;

  template <auto protocol>
  inline constexpr bool compatibility = [] {
    if constexpr (requires { protocol.compatability_mode; })
      return protocol.compatability_mode;
    return false;
  }();

  namespace detail {
    template <typename protocol>
    struct version_type_impl;

    template <typename v_type, typename message_tag_type, template <v_type> class versioned_message_tag>
    struct version_type_impl<versioned_protocol<v_type, message_tag_type, versioned_message_tag>> {
      using type = v_type;
    };
  }

  template <auto protocol>
  using version_type = typename detail::version_type_impl<decltype(protocol)>::type;

  template <auto protocol>
  inline constexpr bool is_versioned = requires { typename version_type<protocol>; };

  namespace detail {
    template <typename protocol, auto V>
    struct version_messages_impl {
      static_assert(false,"version impl needs specialization for this protocol");
    };

    template <typename v_type, typename message_tag_type, template <v_type> class versioned_message_tag, v_type V>
    struct version_messages_impl<versioned_protocol<v_type, message_tag_type, versioned_message_tag>, V> {
      using type = typename versioned_message_tag<V>::tags;
    };
  }

  template <auto protocol, version_type<protocol> V>
  using versioned_messages = typename detail::version_messages_impl<decltype(protocol), V>::type;

  template <auto protocol>
  inline constexpr auto internal_version = protocol.internal_version;

  template <auto protocol>
  constexpr bool is_internal_version(version_type<protocol> const v) {
    return v == internal_version<protocol>;
  }

  template <auto message>
  inline constexpr bool is_internal_message = is_internal_version<message.protocol>(message.version);

  template <auto protocol>
  constexpr bool is_valid_version(version_type<protocol> const v) {
    if (v == internal_version<protocol>)
      return true;
    if (v >= protocol.min_version && v < (protocol.min_version + protocol.num_versions))
      return true;
    return false;
  }

  template <auto protocol>
  constexpr version_type<protocol> version_to_index(version_type<protocol> const v) {
    if (v == internal_version<protocol>)
      return 0;
    return v - protocol.min_version + 1;
  }

  template <auto protocol>
  constexpr version_type<protocol> version_from_index(version_type<protocol> const i) {
    if (i == 0)
      return internal_version<protocol>;
    return protocol.min_version + i - 1;
  }

  namespace detail {
    template <auto protocol>
    inline constexpr header_type<protocol> most_significant_bit =
      static_cast<header_type<protocol>>(0x1) << ((sizeof(header_type<protocol>) - 1) * 8 + 7);
  }

  template <auto protocol>
  constexpr header_type<protocol> add_versioning_bit(header_type<protocol> const header) {
    return header | detail::most_significant_bit<protocol>;
  }

  template <auto protocol>
  constexpr header_type<protocol> remove_versioning_bit(header_type<protocol> const header) {
    return header & (~detail::most_significant_bit<protocol>);
  }

  template <auto protocol>
  constexpr bool versioned_header(header_type<protocol> const header) {
    return header & detail::most_significant_bit<protocol>;
  }

  template <auto protocol>
  inline constexpr auto error_tag = [] {
    if constexpr (is_versioned<protocol>) {
      return protocol.error_tag;
    } else {
      return header_type<protocol>{enumerator_count<messages<protocol>>};
    }
  }();

  template <auto P>
  struct message {
    static constexpr auto protocol = P;

    messages<P> tag;

    auto operator<=>(message const &) const = default;
  };

  template <auto P, version_type<P> V>
  struct versioned_message {
    static constexpr auto protocol = P;
    static constexpr auto version = V;

    versioned_messages<P, V> tag;
    auto operator<=>(versioned_message const &) const = default;
  };

  template <auto message>
  struct request {
    SHYFT_DEFINE_STRUCT(request, (), ());

    auto operator<=>(request const &) const = default;
  };

  template <auto message>
  struct reply {
    SHYFT_DEFINE_STRUCT(reply, (), ());

    auto operator<=>(reply const &) const = default;
  };

  template <auto message>
  constexpr auto get_message(request<message> const &) {
    return message;
  }

  template <auto message>
  constexpr auto get_message(reply<message> const &) {
    return message;
  }

  template <auto message>
  inline constexpr auto message_reply_tag = etoi(message.tag);

  template <auto message>
  void serialize(auto &archive, reply<message> &m, unsigned int const v) {
    reflection::serialize(archive, m, v);
  }

  template <auto message>
  void serialize(auto &archive, request<message> &m, unsigned int const v) {
    reflection::serialize(archive, m, v);
  }

  template <auto protocol>
  auto read_header(std::istream &stream) {
    header_type<protocol> tag;
    if (!reflection::read_blob<compatibility<protocol>>(stream, tag))
      return none<header_type<protocol>>;
    return just(tag);
  }

  template <auto message>
  bool write_header(std::ostream &stream) {
    if constexpr (is_versioned<message.protocol>) {
      if constexpr (message.version == internal_version<message.protocol>) {
        return reflection::write_blob<compatibility<message.protocol>>(stream, etoi(message.tag));
      } else
        return reflection::write_blob<compatibility<message.protocol>>(
          stream, add_versioning_bit<message.protocol>(etoi(message.tag)));
    } else {
      return reflection::write_blob(stream, etoi(message.tag));
    }
  }

  template <auto protocol>
  bool write_reply_header(std::ostream &stream, header_type<protocol> tag) {
    return reflection::write_blob<compatibility<protocol>>(stream, tag);
  }

  template <auto protocol>
  requires(is_versioned<protocol>)
  auto read_version(auto &stream) {
    version_type<protocol> version;
    if (!reflection::read_blob(stream, version))
      return none<version_type<protocol>>;
    return just(version);
  }

  template <auto message>
  bool write_version(std::ostream &stream) {
    if constexpr (is_versioned<message.protocol>)
      if (message.version != internal_version<message.protocol>)
        return reflection::write_blob(stream, message.version);
    return true;
  }

  template <auto protocol>
  auto write_error(auto &stream, std::string_view msg) {
    if (!write_reply_header<protocol>(stream, error_tag<protocol>))
      return false;
    return !reflection::write_string<compatibility<protocol>>(stream, msg);
  }

  template <auto message>
  struct protocol_archives {
    static auto make_oarchive(std::ostream &stream) {
      return core::core_oarchive{stream, core::core_arch_flags};
    }

    static auto make_iarchive(std::istream &stream) {
      return core::core_iarchive{stream, core::core_arch_flags};
    }
  };

  SHYFT_DEFINE_ENUM(protocol_stream_error_tag, std::uint8_t, (header, read_version, invalid_version));

  template <auto protocol, protocol_stream_error_tag e>
  struct protocol_stream_error { };

  template <auto protocol>
  struct protocol_stream_error<protocol, protocol_stream_error_tag::invalid_version> {
    version_type<protocol> read_version;
  };

  template <auto protocol, auto protocol_stream_error_tag>
  constexpr auto get_error_tag(protocol_stream_error<protocol, protocol_stream_error_tag> const &) {
    return protocol_stream_error_tag;
  }

  template <auto protocol>
  constexpr auto with_version_or(version_type<protocol> v, auto &&action, auto &&fallback) {
    auto const i = version_to_index<protocol>(v);
    if (i > protocol.num_versions)
      return std::invoke(
        SHYFT_FWD(fallback), protocol_stream_error<protocol, protocol_stream_error_tag::invalid_version>{v});
    return boost::mp11::mp_with_index<protocol.num_versions + 1>(i, [&](auto i) {
      return std::invoke(SHYFT_FWD(action), constant_v<version_from_index<protocol>(i())>);
    });
  }

  template <auto protocol>
  auto with_message_or(std::istream &in, header_type<protocol> header, auto &&action, auto &&fallback) {
    if constexpr (is_versioned<protocol>) {
      if (versioned_header<protocol>(header)) {
        auto const version = read_version<protocol>(in);
        if (!version)
          return std::invoke(
            SHYFT_FWD(fallback), protocol_stream_error<protocol, protocol_stream_error_tag::read_version>{});
        return with_version_or<protocol>(
          *version,
          [&](auto version) {
            auto const typed = versioned_messages<protocol, version()>{remove_versioning_bit<protocol>(header)};
            return with_enum_or(
              typed,
              [&](auto header_tag) {
                return std::invoke(SHYFT_FWD(action), constant_v<versioned_message<protocol, version()>{header_tag()}>);
              },
              [&] {
                std::invoke(SHYFT_FWD(fallback), protocol_stream_error<protocol, protocol_stream_error_tag::header>{});
              });
          },
          SHYFT_FWD(fallback));
      } else {
        return with_enum_or(
          versioned_messages<protocol, internal_version<protocol>>{header},
          [&](auto header_tag) {
            return std::invoke(
              SHYFT_FWD(action), constant_v<versioned_message<protocol, internal_version<protocol>>{header_tag()}>);
          },
          [&] {
            std::invoke(SHYFT_FWD(fallback), protocol_stream_error<protocol, protocol_stream_error_tag::header>{});
          });
      }
    } else {
      return with_enum_or(
        messages<protocol>{header},
        [&](auto message_tag) {
          return std::invoke(SHYFT_FWD(action), constant_v<message<protocol>{message_tag()}>);
        },
        [&] {
          std::invoke(SHYFT_FWD(fallback), protocol_stream_error<protocol, protocol_stream_error_tag::header>{});
        });
    }
  }

  template <typename io_handler = basic_io>
  struct basic_client {
    core::srv_connection connection;

    void close() {
      connection.close();
    }

    auto send(auto out) { // Consider all throws on top
      static constexpr auto message = get_message(out);
      reply<message> in;
      io_handler handler(connection);
      handler([&](auto &c) {
        auto &stream = *c.io;
        if (!write_header<message>(stream))
          throw dlib::socket_error(fmt::format("failed writing message type"));
        if (!write_version<message>(stream))
          throw dlib::socket_error(fmt::format("failed writing version tag"));
        {
          auto oarchive = protocol_archives<message>::make_oarchive(stream);
          oarchive << out;
        }
        auto maybe_reply_tag = read_header<message.protocol>(stream);
        if (!maybe_reply_tag)
          throw dlib::socket_error("failed reading reply tag");
        if (auto const &reply_tag = *maybe_reply_tag; reply_tag != message_reply_tag<message>) {
          if (reply_tag != error_tag<message.protocol>)
            throw std::runtime_error(
              fmt::format("unexpected message tag: expected {}, got {}", message.tag, reply_tag));
          auto error = reflection::try_read_string<compatibility<message.protocol>>(stream);
          if (!error)
            throw dlib::socket_error("failed reading error message");
          throw std::runtime_error(*error);
        }
        auto iarchive = protocol_archives<message>::make_iarchive(stream);
        iarchive >> in;
      });
      return in;
    }
  };

  template <auto protocol, typename Dispatch>
  struct basic_server : srv::fast_server_iostream {

    Dispatch dispatch;

    basic_server(Dispatch dispatch = {}, srv::fast_server_iostream_config cfg = {})
      : srv::fast_server_iostream{std::move(cfg)}
      , dispatch{std::move(dispatch)} {
    }

    ~basic_server() {
      dlib::server::clear();
    }

    int start_server() {
      if (get_listening_port() == 0) {
        start_async();
        while (is_running() && get_listening_port() == 0)
          std::this_thread::sleep_for(std::chrono::milliseconds(10l));
      } else {
        start_async();
      }
      auto port_num = get_listening_port();
      return port_num;
    }

    void on_connect(
      std::istream &in,
      std::ostream &out,
      std::string const &foreign_ip,
      [[maybe_unused]] std::string const &local_ip,
      unsigned short foreign_port,
      [[maybe_unused]] unsigned short local_port,
      [[maybe_unused]] dlib::uint64 connection_id,
      [[maybe_unused]] srv::stale_connection_sensor &scs) {

      auto session = dispatch(foreign_ip, foreign_port);

      try {
        while (!in.eof()) {
          scs.alive_tick();
          auto header = read_header<protocol>(in);
          if (!header) {
            session.log(dlib::LTRACE, "unable to read tag");
            return;
          }
          with_message_or<protocol>(
            in,
            *header,
            [&]<auto message>(constant_t<message> msg) {
              request<message> req;
              {
                auto iarchive = protocol_archives<message>::make_iarchive(in);
                iarchive >> req;
              }
              try {
                srv::scoped_processing _sp{scs};
                auto rep = session(std::move(req));
                if (!write_reply_header<message.protocol>(out, message_reply_tag<message>)) {
                  session.log(dlib::LTRACE, fmt::format("Not able to write tag {} to output.", message.tag));
                  throw dlib::socket_error("not able to write tag");
                }
                auto out_archive = protocol_archives<message>::make_oarchive(out);
                out_archive << rep;
              } catch (std::exception const &error) {
                session.log(dlib::LTRACE, error.what());
                if (!write_error<protocol>(out, error.what())) {
                  session.log(dlib::LTRACE, "Not able to write error to output");
                  throw dlib::socket_error("Not able to write error to output");
                }
              }
            },
            [&](auto error) {
              auto error_message = [&] {
                if constexpr (get_error_tag(error) == protocol_stream_error_tag::read_version)
                  return "unable to read version from stream\n";
                else if constexpr (get_error_tag(error) == protocol_stream_error_tag::invalid_version)
                  return fmt::format("unexpected version tag: got {}", error.read_version);
                else
                  return fmt::format("unexpected message tag: got {}", *header);
              }();
              if (
                get_error_tag(error) != protocol_stream_error_tag::read_version
                && !write_error<protocol>(out, error_message))
                session.log(dlib::LTRACE, "Not able to write error to output\n");
              throw dlib::socket_error(error_message);
            });
        }
      } catch (dlib::socket_error const &error) {
        session.log(dlib::LTRACE, fmt::format("unhandled exception: {}\n", error.what()));
      } catch (...) {
        session.log(dlib::LTRACE, "unhandled unknown exception\n");
        if constexpr (requires { dispatch.failed_connection(); })
          dispatch.failed_connection();
      }
    }
  };

}

template <auto message, typename Char>
struct fmt::formatter<shyft::protocols::request<message>, Char>
  : shyft::reflection::struct_formatter<shyft::protocols::request<message>, Char> { };

template <auto message, typename Char>
struct fmt::formatter<shyft::protocols::reply<message>, Char>
  : shyft::reflection::struct_formatter<shyft::protocols::reply<message>, Char> { };
