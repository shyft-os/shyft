#pragma once
#include <cstdint>
#include <string_view>

#include <boost/config.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/tracking.hpp>

#include <shyft/core/core_archive.h>

namespace boost::serialization {
  template <class Archive>
  requires(
    std::is_same_v<Archive, shyft::core::core_oarchive> || std::is_same_v<Archive, shyft::core::core_oarchive_stripped>)
  void save(
    Archive& ar,
    std::string_view const & t,
    unsigned int const /*version*/
  ) {
    auto l = t.size();
    ar << l;
    ar.save_binary(t.data(), l);
  }

  template <class Archive>
  requires(
    std::is_same_v<Archive, shyft::core::core_oarchive> || std::is_same_v<Archive, shyft::core::core_oarchive_stripped>)
  void serialize(Archive& ar, std::string_view& t, unsigned int const version) {
    static_assert(Archive::is_saving::value);
    save(ar, t, version);
  }
}
BOOST_CLASS_IMPLEMENTATION(std::string_view, boost::serialization::object_serializable)
BOOST_CLASS_TRACKING(std::string_view, boost::serialization::track_never)