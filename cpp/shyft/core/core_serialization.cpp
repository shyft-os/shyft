/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>
#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/serialize_blob.h>
//
// 1. first include std stuff and the headers for
// files with serializeation support
//

#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/methods/gamma_snow.h>
#include <shyft/hydrology/methods/hbv_actual_evapotranspiration.h>
#include <shyft/hydrology/methods/hbv_physical_snow.h>
#include <shyft/hydrology/methods/hbv_snow.h>
#include <shyft/hydrology/methods/hbv_soil.h>
#include <shyft/hydrology/methods/hbv_tank.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/methods/skaugen.h>
#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/hydrology/mstack_param.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/stacks/pt_gs_k.h>
#include <shyft/hydrology/stacks/pt_hps_k.h>
#include <shyft/hydrology/stacks/pt_hs_k.h>
#include <shyft/hydrology/stacks/pt_ss_k.h>
#include <shyft/hydrology/stacks/pt_st_hbv.h>
#include <shyft/hydrology/stacks/pt_st_k.h>
#include <shyft/hydrology/stacks/r_pm_gs_k.h>
#include <shyft/hydrology/stacks/r_pm_st_k.h>
#include <shyft/hydrology/stacks/r_pmv_st_k.h>
#include <shyft/hydrology/stacks/r_pt_gs_k.h>
#include <shyft/hydrology/vegetation_cell_data.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
// then include stuff you need like vector,shared, base_obj,nvp etc.

#include <shyft/dtss/db_cfg.h>
#include <shyft/dtss/diagnostics.h>
#include <shyft/dtss/q_bridge/config.h>
#include <shyft/dtss/q_bridge/status.h>
#include <shyft/dtss/queue_msg.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/dtss/transfer/config.h>
#include <shyft/dtss/transfer/status.h>
#include <shyft/dtss/url_ts_frag.h>
#include <shyft/srv/model_info.h>
#include <shyft/version.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
//
// 2. Then implement each class serialization support
//

namespace shyft {
  _SHYFT_BEGIN_VERSION_NAMESPACE
  std::string _version_string() {
    return fmt::format("{}.{}.{}", shyft::_version.major, shyft::_version.minor, shyft::_version.patch);
  }

  std::string _release_string() {
    return fmt::format("{}", shyft::_release);
  }

  _SHYFT_END_VERSION_NAMESPACE
}

using namespace shyft::core;
using boost::serialization::base_object;

//-- utctime_utilities.h
namespace boost::archive {
  template <class Archive>
  void load(Archive& ar, utctime& tp, unsigned) {
    utctimespan::rep dt;
    ar & dt; // make_nvp("t_utc_us", dt);
    tp = utctime(utctimespan(dt));
  }

  template <class Archive>
  void save(Archive& ar, utctime const & tp, unsigned) {
    utctimespan::rep dt = tp.count();
    ar & dt; // make_nvp("t_utc_us",dt);
  }

  template <class Archive>
  void serialize(Archive& ar, utctime& tp, unsigned version) {
    boost::serialization::split_free(ar, tp, version);
  }
}

template <class Archive>
void shyft::core::utcperiod::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("start", start) & core_nvp("end", end);
  ;
}

template <class Archive>
void shyft::core::time_zone::tz_table::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("start_year", start_year) & core_nvp("tz_name", tz_name) & core_nvp("dst", dst) & core_nvp("dt", dt);
}

template <class Archive>
void shyft::core::time_zone::tz_info_t::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("base_tz", base_tz) & core_nvp("tz", tz);
  ;
}

template <class Archive>
void shyft::core::calendar::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("tz_info", tz_info);
}

//-- time_axis.h

template <class Archive>
void shyft::time_axis::fixed_dt::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("t", t) & core_nvp("dt", dt) & core_nvp("n", n);
}

template <class Archive>
void shyft::time_axis::calendar_dt::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("cal", cal) & core_nvp("t", t) & core_nvp("dt", dt) & core_nvp("n", n);
}

template <class Archive>
void shyft::time_axis::point_dt::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  if (version == 0) {
    ar& core_nvp("t", t) & core_nvp("dt", t_end);
  } else {
    if constexpr (Archive::is_saving::value) {
      shyft::core::store_blob(ar, t);
    } else {
      shyft::core::load_blob(ar, t);
    }
    ar& core_nvp("dt", t_end);
  }
}

template <class Archive>
void shyft::time_axis::generic_dt::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  auto _gt = gt();
  ar& core_nvp("gt", _gt);
  if constexpr (Archive::is_saving::value) {
    switch (_gt) {
    case generic_type::FIXED:
      ar& core_nvp("f", f());
      break;
    case generic_type::CALENDAR:
      ar& core_nvp("c", c());
      break;
    case generic_type::POINT:
      ar& core_nvp("p", p());
      break;
    }
  } else {
    switch (_gt) {
    case generic_type::FIXED: {
      fixed_dt ta;
      ar& core_nvp("f", ta);
      impl = ta;
    } break;
    case generic_type::CALENDAR: {
      calendar_dt ta;
      ar& core_nvp("c", ta);
      impl = ta;
    } break;
    case generic_type::POINT: {
      point_dt ta;
      ar& core_nvp("p", ta);
      impl = std::move(ta);
    } break;
    }
  }
}

//-- basic geo stuff
template <class Archive>
void shyft::core::geo_point::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("x", x) & core_nvp("y", y) & core_nvp("z", z);
}

template <class Archive>
void shyft::core::land_type_fractions::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("glacier_", glacier_) & core_nvp("lake_", lake_) & core_nvp("reservoir_", reservoir_)
    & core_nvp("forest_", forest_);
}

template <class Archive>
void shyft::core::vegetation_cell_data::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar
      //& core_nvp("lai_",lai_)
      //& core_nvp("albedo_",albedo_)
      & core_nvp("hveg_", hveg_)
    & core_nvp("lai_max_", lai_max_) & core_nvp("vegetation_", vegetation_);
}

template <class Archive>
void shyft::core::routing_info::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("id", id) & core_nvp("distance", distance);
}

template <class Archive>
void shyft::core::geo_cell_data::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("mid_point_", mid_point_) & core_nvp("area_m2", area_m2) & core_nvp("catchment_id_", catchment_id_)
    & core_nvp("radiation_slope_factor_", radiation_slope_factor_) & core_nvp("fractions", fractions)
    & core_nvp("routing", routing);
  if (version > 0) {
    ar& core_nvp("v1", v1) & core_nvp("v2", v2) & core_nvp("v3", v3) & core_nvp("epsg_id", epsg_id);
  }
  if (version > 1) {
    ar& core_nvp("vegetation_parameter", vegetation_parameter);
  }
}

template <class Archive>
void shyft::core::gcd_model::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("id", id) & core_nvp("gcd", gcd);
  if (version > 0) {
    ar& core_nvp("json", json) & core_nvp("epsg_id", epsg_id) & core_nvp("polygon", polygon);
  }
}

template <class Archive>
void shyft::core::bayesian_kriging::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("gradient_sd", gradient_sd) & core_nvp("sill_value", sill_value) & core_nvp("nug_value", nug_value)
    & core_nvp("range_value", range_value) & core_nvp("zscale_value", zscale_value);
}

template <class Archive>
void shyft::core::inverse_distance::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("max_members", max_members) & core_nvp("max_distance", max_distance)
    & core_nvp("distance_measure_factor", distance_measure_factor) & core_nvp("zscale", zscale);
}

template <class Archive>
void shyft::core::inverse_distance::temperature_parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("base", base_object<shyft::core::inverse_distance::parameter>(*this))
    & core_nvp("default_gradient", default_temp_gradient) & core_nvp("default_gradient", gradient_by_equation);
  if (version > 0) {
    ar& core_nvp("minimum_z_distance", minimum_z_distance) & core_nvp("gradient_min", gradient_min)
      & core_nvp("gradient_max", gradient_max);
  }
}

template <class Archive>
void shyft::core::inverse_distance::precipitation_parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("base", base_object<shyft::core::inverse_distance::parameter>(*this))
    & core_nvp("default_gradient", scale_factor);
}

template <class Archive>
void shyft::core::snow_tiles::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("shape", shape) & core_nvp("area_fractions", area_fractions) & core_nvp("multiply", multiply)
    & core_nvp("tx", tx) & core_nvp("cx", cx) & core_nvp("ts", ts) & core_nvp("lwmax", lwmax) & core_nvp("cfr", cfr);
}

template <class Archive>
void shyft::core::penman_monteith::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("height_ws", height_ws) & core_nvp("height_t", height_t) & core_nvp("height_veg", height_veg)
    & core_nvp("rl", rl) & core_nvp("full_model", full_model) // Hmm win vs. lin compat?
    ;
}

template <class Archive>
void shyft::core::penman_monteith_vegetation::parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("height_ws", height_ws) & core_nvp("height_t", height_t);
}

template <class Archive>
void shyft::core::mstack_parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("r_drf", reservoir_direct_response_fraction);
}

template <class Archive>
void shyft::core::actual_evapotranspiration::parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("ae", ae_scale_factor);
}

template <class Archive>
void shyft::core::routing::uhg_parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("velocity", velocity) & core_nvp("alpha", alpha) & core_nvp("beta", beta);
}

template <class Archive>
void shyft::core::glacier_melt::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("dtf", dtf) & core_nvp("direct_response", direct_response);
}

template <class Archive>
void shyft::core::precipitation_correction::parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("scale_factor", scale_factor);
}

template <class Archive>
void shyft::core::interpolation_parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("temperature", temperature) & core_nvp("temperature_idw", temperature_idw)
    & core_nvp("use_idw_for_temperature", use_idw_for_temperature) & core_nvp("precipitation", precipitation)
    & core_nvp("wind_speed", wind_speed) & core_nvp("radiation", radiation) & core_nvp("rel_hum", rel_hum);
}

//-- state serialization
template <class Archive>
void shyft::core::hbv_snow::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("sp", sp) & core_nvp("sw", sw) & core_nvp("swe", swe) & core_nvp("sca", sca);
}

template <class Archive>
void shyft::core::snow_tiles::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("fw", fw) & core_nvp("lw", lw);
}

template <class Archive>
void shyft::core::hbv_snow::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("tx", tx) & core_nvp("cx", cx) & core_nvp("ts", ts) & core_nvp("lw", lw) & core_nvp("cfr", cfr);
}

template <class Archive>
void shyft::core::hbv_physical_snow::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("sp", sp) & core_nvp("sw", sw) & core_nvp("albedo", albedo) & core_nvp("iso_pot_energy", iso_pot_energy)
    & core_nvp("surface_heat", surface_heat) & core_nvp("swe", swe) & core_nvp("sca", sca);
}

template <class Archive>
void shyft::core::hbv_physical_snow::parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("s", s) & core_nvp("intervals", intervals) & core_nvp("tx", tx) & core_nvp("lw", lw)
    & core_nvp("cfr", cfr) & core_nvp("wind_scale", wind_scale) & core_nvp("wind_const", wind_const)
    & core_nvp("surface_magnitude", surface_magnitude) & core_nvp("max_albedo", max_albedo)
    & core_nvp("min_albedo", min_albedo) & core_nvp("fast_albedo_decay_rate", fast_albedo_decay_rate)
    & core_nvp("slow_albedo_decay_rate", slow_albedo_decay_rate)
    & core_nvp("snowfall_reset_depth", snowfall_reset_depth)
    & core_nvp("calculate_iso_pot_energy", calculate_iso_pot_energy);
}

template <class Archive>
void shyft::core::hbv_soil::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("sm", sm);
}

template <class Archive>
void shyft::core::hbv_soil::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("fc", fc) & core_nvp("lpdel", beta) & core_nvp("beta", beta) & core_nvp("infmax", beta);
}

template <class Archive>
void shyft::core::hbv_tank::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("uz", uz) & core_nvp("lz", lz);
}

template <class Archive>
void shyft::core::hbv_tank::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("uz1", uz1) & core_nvp("uz2", uz2) & core_nvp("kuz0", kuz0) & core_nvp("kuz1", kuz1)
    & core_nvp("kuz2", kuz2) & core_nvp("perc", perc) & core_nvp("klz", klz) & core_nvp("ce", ce)
    & core_nvp("cevpl", cevpl);
}

template <class Archive>
void shyft::core::hbv_actual_evapotranspiration::parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("lp", lp);
}

template <class Archive>
void shyft::core::kirchner::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("q", q);
}

template <class Archive>
void shyft::core::q_adjust_result::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("q_0", q_0) & core_nvp("q_r", q_r) & core_nvp("diagnostics", diagnostics);
}

template <class Archive>
void shyft::core::kirchner::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("c1", c1) & core_nvp("c2", c2) & core_nvp("c3", c3);
}

template <class Archive>
void shyft::core::gamma_snow::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("albedo", albedo) & core_nvp("lwc", lwc) & core_nvp("surface_heat", surface_heat)
    & core_nvp("alpha", alpha) & core_nvp("sdc_melt_mean", sdc_melt_mean) & core_nvp("acc_melt", acc_melt)
    & core_nvp("iso_pot_energy", iso_pot_energy) & core_nvp("temp_swe", temp_swe);
}

template <class Archive>
void shyft::core::gamma_snow::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("winter_end_day_of_year", winter_end_day_of_year)             // = 100;///< approx 10th april
    & core_nvp("initial_bare_ground_fraction", initial_bare_ground_fraction) // = 0.04;
    & core_nvp("snow_cv", snow_cv) & core_nvp("tx", tx) & core_nvp("wind_scale", wind_scale)
    & core_nvp("wind_const", wind_const) & core_nvp("max_water", max_water)
    & core_nvp("surface_magnitude", surface_magnitude) & core_nvp("max_albedo", max_albedo)
    & core_nvp("min_albedo", min_albedo) & core_nvp("fast_albedo_decay_rate", fast_albedo_decay_rate)
    & core_nvp("slow_albedo_decay_rate", slow_albedo_decay_rate)
    & core_nvp("snowfall_reset_depth", snowfall_reset_depth) & core_nvp("glacier_albedo", glacier_albedo)
    & core_nvp("calculate_iso_pot_energy", calculate_iso_pot_energy)
    & core_nvp("snow_cv_forest_facto", snow_cv_forest_factor)
    & core_nvp("snow_cv_altitude_facto", snow_cv_altitude_factor) & core_nvp("n_winter_days", n_winter_days);
}

template <class Archive>
void shyft::core::skaugen::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("nu", nu) & core_nvp("alpha", alpha) & core_nvp("sca", sca) & core_nvp("swe", swe)
    & core_nvp("free_water", free_water) & core_nvp("residual", residual) & core_nvp("num_units", num_units);
}

template <class Archive>
void shyft::core::skaugen::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("alpha_0", alpha_0) & core_nvp("d_range", d_range) & core_nvp("unit_size", unit_size)
    & core_nvp("max_water_fraction", max_water_fraction) & core_nvp("tx", tx) & core_nvp("cx", cx) & core_nvp("ts", ts)
    & core_nvp("cfr", cfr);
}

template <class Archive>
void shyft::core::radiation::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("albedo", albedo) & core_nvp("turbidity", turbidity) & core_nvp("al", al) & core_nvp("bl", bl)
    & core_nvp("ac", ac) & core_nvp("bc", bc) & core_nvp("as", as) & core_nvp("bs", bs);
}

template <class Archive>
void shyft::core::pt_gs_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("gs", gs) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::r_pm_gs_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("gs", gs) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::r_pm_st_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("snow", snow) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::r_pmv_st_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("snow", snow) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::r_pt_gs_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("gs", gs) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::pt_ss_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("snow", snow) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::pt_hs_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("snow", snow) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::pt_st_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("snow", snow) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::pt_st_hbv::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("snow", snow) & core_nvp("soil", soil) & core_nvp("tank", tank);
}

template <class Archive>
void shyft::core::pt_hps_k::state::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("hps", hps) & core_nvp("kirchner", kirchner);
}

template <class Archive>
void shyft::core::priestley_taylor::parameter::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("albedo", albedo) & core_nvp("alpha", alpha);
}

// mstack parameters

template <class Archive>
void shyft::core::pt_gs_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("pt", pt) & core_nvp("gs", gs) & core_nvp("ae", ae) & core_nvp("kirchner", kirchner)
    & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing) & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::pt_ss_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("pt", pt) & core_nvp("ss", ss) & core_nvp("ae", ae) & core_nvp("kirchner", kirchner)
    & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing) & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::pt_st_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("pt", pt) & core_nvp("st", st) & core_nvp("ae", ae) & core_nvp("kirchner", kirchner)
    & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing) & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::pt_st_hbv::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("pt", pt) & core_nvp("st", st) & core_nvp("soil", soil) & core_nvp("tank", tank)
    & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing) & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::r_pm_st_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("rad", rad) & core_nvp("pm", pm) & core_nvp("st", st) & core_nvp("ae", ae)
    & core_nvp("kirchner", kirchner) & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing)
    & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::r_pmv_st_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("rad", rad) & core_nvp("pm", pm) & core_nvp("st", st) & core_nvp("ae", ae)
    & core_nvp("kirchner", kirchner) & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing)
    & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::pt_hps_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("pt", pt) & core_nvp("hps", hps) & core_nvp("ae", ae) & core_nvp("kirchner", kirchner)
    & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing) & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::pt_hs_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("pt", pt) & core_nvp("hs", hs) & core_nvp("ae", ae) & core_nvp("kirchner", kirchner)
    & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing) & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::r_pm_gs_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("rad", rad) & core_nvp("pm", pm) & core_nvp("gs", gs) & core_nvp("ae", ae)
    & core_nvp("kirchner", kirchner) & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing)
    & core_nvp("msp", msp);
}

template <class Archive>
void shyft::core::r_pt_gs_k::parameter::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("rad", rad) & core_nvp("pt", pt) & core_nvp("gs", gs) & core_nvp("ae", ae)
    & core_nvp("kirchner", kirchner) & core_nvp("p_corr", p_corr) & core_nvp("gm", gm) & core_nvp("routing", routing)
    & core_nvp("msp", msp);
}

// Utctime is instantiated and registered separarately
x_serialize_implement(shyft::core::utctime);

namespace boost::archive {
  namespace sc = shyft::core;
  // instantiate archive templates
  template void load<core_iarchive >(core_iarchive& ar, sc::utctime& tp, unsigned);
  template void load<core_iarchive_stripped >(core_iarchive_stripped& ar, sc::utctime& tp, unsigned);
  template void save<core_oarchive >(core_oarchive& ar, sc::utctime const & tp, unsigned);
  template void save<core_oarchive_stripped >(core_oarchive_stripped& ar, sc::utctime const & tp, unsigned);
  template void serialize<core_iarchive>(core_iarchive& ar, sc::utctime& tp, unsigned version);
  template void serialize<core_iarchive_stripped >(core_iarchive_stripped& ar, sc::utctime& tp, unsigned version);
  template void serialize<core_oarchive>(core_oarchive& ar, sc::utctime& tp, unsigned version);
  template void serialize<core_oarchive_stripped >(core_oarchive_stripped& ar, sc::utctime& tp, unsigned version);
}

template <class Archive>
void shyft::srv::model_info::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& boost::serialization::make_nvp("id", id) & boost::serialization::make_nvp("name", name)
    & boost::serialization::make_nvp("created", created) & boost::serialization::make_nvp("json", json);
}

template <class Archive>
void shyft::dtss::queue::msg_info::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("msg_id", msg_id) & core_nvp("description", description) & core_nvp("ttl", ttl)
    & core_nvp("created", created) & core_nvp("fetched", fetched) & core_nvp("done", done)
    & core_nvp("diagnostics", diagnostics);
}

template <class Archive>
void shyft::dtss::queue::tsv_msg::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("info", info) & core_nvp("tsv", tsv);
}

template <class Archive>
void shyft::dtss::store_policy::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("recreate", recreate) & core_nvp("strict", strict) & core_nvp("cache", cache);
  if (file_version > 0)
    ar& core_nvp("best_effort", best_effort);
}

template <class Archive>
void shyft::dtss::diagnostics::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("ix", ix) & core_nvp("diag", diag);
}

template <class Archive>
void shyft::dtss::url_ts_frag::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("url", url) & core_nvp("ts", ts);
}

///----- transfer stuff
///----- transfer stuff
template <class Archive>
void shyft::dtss::transfer::configuration::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("name", name) & core_nvp("json", json) & core_nvp("what", what) & core_nvp("where", where)
    & core_nvp("xfer_period", xfer_period) & core_nvp("read_remote", read_remote)
    & core_nvp("read_updates_cache", read_updates_cache) & core_nvp("write_policy", write_policy)
    & core_nvp("when", when) & core_nvp("how", how);
}

template <class Archive>
void shyft::dtss::transfer::configuration::time_series_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("search_pattern", search_pattern) & core_nvp("replace_pattern", replace_pattern);
}

template <class Archive>
void shyft::dtss::transfer::configuration::period_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("now_relative", now_relative) & core_nvp("spec", spec);
}

template <class Archive>
void shyft::dtss::transfer::configuration::remote_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("host", host) & core_nvp("port", port);
}

template <class Archive>
void shyft::dtss::transfer::configuration::when_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("ta", ta) & core_nvp("changed", changed) & core_nvp("poll_interval", poll_interval)
    & core_nvp("change_linger_time", change_linger_time);
}

template <class Archive>
void shyft::dtss::transfer::configuration::xfer_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("retries", retries) & core_nvp("sleep_before_retry", sleep_before_retry)
    & core_nvp("partition_size", partition_size);
}

template <class Archive>
void shyft::dtss::transfer::status::write_error::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("ts_url", ts_url) & core_nvp("code", code);
}

template <class Archive>
void shyft::dtss::transfer::status::read_error::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("ts_url", ts_url) & core_nvp("code", code);
}

template <class Archive>
void shyft::dtss::transfer::status::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("n_ts_found", n_ts_found) & core_nvp("read_errors", read_errors) & core_nvp("write_errors", write_errors)
    & core_nvp("remote_errors", remote_errors) & core_nvp("reader_alive", reader_alive)
    & core_nvp("writer_alive", writer_alive) & core_nvp("read_speed", read_speed) & core_nvp("write_speed", write_speed)
    & core_nvp("total_transferred", total_transferred) & core_nvp("last_activity", last_activity);
}

template <class Archive>
void dtss::q_bridge::configuration::remote_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("host", host) & core_nvp("port", port) & core_nvp("queue_name", queue_name);
}

template <class Archive>
void dtss::q_bridge::configuration::xfer_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("retries", retries) & core_nvp("sleep_before_retry", sleep_before_retry)
    & core_nvp("queue_pick_up_max_wait", queue_pick_up_max_wait);
}

template <class Archive>
void dtss::q_bridge::configuration::queue_spec::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("queue_name", queue_name) & core_nvp("ack_poll_inteval", ack_poll_interval);
}

template <class Archive>
void dtss::q_bridge::error_entry::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("code", code) & core_nvp("time", time);
}

template <class Archive>
void dtss::q_bridge::status::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("name", name) & core_nvp("ack_errors", ack_errors) & core_nvp("last_ack_error", last_ack_error)
    & core_nvp("fetch_errors", fetch_errors) & core_nvp("last_fetch_error", last_fetch_error)
    & core_nvp("last_activity", last_activity) & core_nvp("fetched", fetched) & core_nvp("completed", completed);
}

template <class Archive>
void dtss::q_bridge::configuration::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("name", name) & core_nvp("json", json) & core_nvp("where", where) & core_nvp("how", how)
    & core_nvp("what", what);
}

template <class Archive>
void dtss::db_cfg::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("compression", compression) & core_nvp("max_file_size", max_file_size)
    & core_nvp("write_buffer_size", write_buffer_size) & core_nvp("log_level", log_level)
    & core_nvp("test_mode", test_mode) & core_nvp("ix_cache", ix_cache) & core_nvp("ts_cache", ts_cache);
}

template <class Archive>
void dtss::read_error::serialize(Archive& ar, [[maybe_unused]] unsigned int const file_version) {
  ar& core_nvp("index", index) & core_nvp("code", code);
}

///---
x_serialize_instantiate_and_register(shyft::dtss::read_error);
x_serialize_instantiate_and_register(shyft::dtss::db_cfg);
x_serialize_instantiate_and_register(shyft::dtss::transfer::status::read_error);
x_serialize_instantiate_and_register(shyft::dtss::transfer::status::write_error);
x_serialize_instantiate_and_register(shyft::dtss::transfer::status);

x_serialize_instantiate_and_register(shyft::dtss::transfer::configuration::time_series_spec);
x_serialize_instantiate_and_register(shyft::dtss::transfer::configuration::period_spec);
x_serialize_instantiate_and_register(shyft::dtss::transfer::configuration::when_spec);
x_serialize_instantiate_and_register(shyft::dtss::transfer::configuration::remote_spec);
x_serialize_instantiate_and_register(shyft::dtss::transfer::configuration::xfer_spec);
x_serialize_instantiate_and_register(shyft::dtss::transfer::configuration);

x_serialize_instantiate_and_register(shyft::dtss::q_bridge::configuration::remote_spec);
x_serialize_instantiate_and_register(shyft::dtss::q_bridge::configuration::xfer_spec);
x_serialize_instantiate_and_register(shyft::dtss::q_bridge::configuration::queue_spec);

x_serialize_instantiate_and_register(shyft::dtss::q_bridge::error_entry);
x_serialize_instantiate_and_register(shyft::dtss::q_bridge::status);
x_serialize_instantiate_and_register(shyft::dtss::q_bridge::configuration);

x_serialize_instantiate_and_register(shyft::dtss::url_ts_frag);
x_serialize_instantiate_and_register(shyft::dtss::diagnostics);
x_serialize_instantiate_and_register(shyft::dtss::store_policy);

x_serialize_instantiate_and_register(shyft::dtss::queue::msg_info);
x_serialize_instantiate_and_register(shyft::dtss::queue::tsv_msg);

x_serialize_instantiate_and_register(shyft::srv::model_info);

x_serialize_instantiate_and_register(shyft::core::geo_point);
x_serialize_instantiate_and_register(shyft::core::land_type_fractions);
x_serialize_instantiate_and_register(shyft::core::routing_info);
x_serialize_instantiate_and_register(shyft::core::geo_cell_data);
x_serialize_instantiate_and_register(shyft::core::gcd_model);
x_serialize_instantiate_and_register(shyft::core::utcperiod);
x_serialize_instantiate_and_register(shyft::core::time_zone::tz_info_t);
x_serialize_instantiate_and_register(shyft::core::time_zone::tz_table);
x_serialize_instantiate_and_register(shyft::core::calendar);
x_serialize_instantiate_and_register(shyft::time_axis::fixed_dt);
x_serialize_instantiate_and_register(shyft::time_axis::calendar_dt);
x_serialize_instantiate_and_register(shyft::time_axis::point_dt);
x_serialize_instantiate_and_register(shyft::time_axis::generic_dt);
x_serialize_instantiate_and_register(shyft::core::hbv_snow::state);
x_serialize_instantiate_and_register(shyft::core::snow_tiles::state);
x_serialize_instantiate_and_register(shyft::core::hbv_physical_snow::state);
x_serialize_instantiate_and_register(shyft::core::hbv_soil::state);
x_serialize_instantiate_and_register(shyft::core::hbv_tank::state);
x_serialize_instantiate_and_register(shyft::core::gamma_snow::state);
x_serialize_instantiate_and_register(shyft::core::skaugen::state);
x_serialize_instantiate_and_register(shyft::core::kirchner::state);
x_serialize_instantiate_and_register(shyft::core::q_adjust_result);
x_serialize_instantiate_and_register(shyft::core::actual_evapotranspiration::parameter);
x_serialize_instantiate_and_register(shyft::core::precipitation_correction::parameter);
x_serialize_instantiate_and_register(shyft::core::penman_monteith::parameter);
x_serialize_instantiate_and_register(shyft::core::penman_monteith_vegetation::parameter);
x_serialize_instantiate_and_register(shyft::core::radiation::parameter);
x_serialize_instantiate_and_register(shyft::core::hbv_snow::parameter);
x_serialize_instantiate_and_register(shyft::core::hbv_physical_snow::parameter);
x_serialize_instantiate_and_register(shyft::core::hbv_soil::parameter);
x_serialize_instantiate_and_register(shyft::core::hbv_tank::parameter);
x_serialize_instantiate_and_register(shyft::core::hbv_actual_evapotranspiration::parameter);
x_serialize_instantiate_and_register(shyft::core::gamma_snow::parameter);
x_serialize_instantiate_and_register(shyft::core::skaugen::parameter);
x_serialize_instantiate_and_register(shyft::core::snow_tiles::parameter);
x_serialize_instantiate_and_register(shyft::core::kirchner::parameter);
x_serialize_instantiate_and_register(shyft::core::interpolation_parameter);
x_serialize_instantiate_and_register(shyft::core::pt_gs_k::state);
x_serialize_instantiate_and_register(shyft::core::pt_gs_k::parameter);
x_serialize_instantiate_and_register(shyft::core::r_pm_gs_k::state);
x_serialize_instantiate_and_register(shyft::core::r_pm_gs_k::parameter);
x_serialize_instantiate_and_register(shyft::core::r_pt_gs_k::state);
x_serialize_instantiate_and_register(shyft::core::r_pt_gs_k::parameter);
x_serialize_instantiate_and_register(shyft::core::pt_hs_k::state);
x_serialize_instantiate_and_register(shyft::core::pt_hs_k::parameter);
x_serialize_instantiate_and_register(shyft::core::pt_hps_k::state);
x_serialize_instantiate_and_register(shyft::core::pt_hps_k::parameter);
x_serialize_instantiate_and_register(shyft::core::pt_ss_k::state);
x_serialize_instantiate_and_register(shyft::core::pt_ss_k::parameter);
x_serialize_instantiate_and_register(shyft::core::pt_st_k::state);
x_serialize_instantiate_and_register(shyft::core::pt_st_k::parameter);
x_serialize_instantiate_and_register(shyft::core::r_pm_st_k::state);
x_serialize_instantiate_and_register(shyft::core::r_pm_st_k::parameter);
x_serialize_instantiate_and_register(shyft::core::r_pmv_st_k::state);
x_serialize_instantiate_and_register(shyft::core::r_pmv_st_k::parameter);
x_serialize_instantiate_and_register(shyft::core::pt_st_hbv::state);
x_serialize_instantiate_and_register(shyft::core::pt_st_hbv::parameter);
x_serialize_instantiate_and_register(shyft::core::priestley_taylor::parameter);
x_serialize_instantiate_and_register(shyft::core::vegetation_cell_data);
x_serialize_instantiate_and_register(shyft::core::mstack_parameter);
x_serialize_instantiate_and_register(shyft::core::routing::uhg_parameter);
x_serialize_instantiate_and_register(shyft::core::glacier_melt::parameter);
x_serialize_instantiate_and_register(shyft::core::inverse_distance::parameter);
x_serialize_instantiate_and_register(shyft::core::inverse_distance::precipitation_parameter);
x_serialize_instantiate_and_register(shyft::core::inverse_distance::temperature_parameter);
x_serialize_instantiate_and_register(shyft::core::bayesian_kriging::parameter);
