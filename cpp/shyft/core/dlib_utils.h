/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <atomic>
#include <cstdint>
#include <exception>
#include <string>
#include <thread>

#include <dlib/logger.h>

#include <shyft/srv/fast_iosockstream.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::core {
  using std::vector;
  using std::string;
  using std::string_view;
  using std::to_string;
  using std::runtime_error;
  using std::exception;
  using std::chrono::seconds;
  using std::chrono::milliseconds;
  using std::unique_ptr;
  using std::shared_ptr;
  using std::make_unique;
  using std::max;
  using std::this_thread::sleep_for;

  /**
   * @brief resilient server connection
   *
   * @details
   * Helper class to connect/reconnect dlib::iosockstream connections
   * that could break if server is rebooted, network is temporary out.
   *
   * The `operation_timeout_ms` helps to break out of network, or remote
   * operations that are stuck.
   *
   * It must be used with care, and adapted to the different
   * needs.
   * For services that might have large msg sizes, e.g. dtss/drms,
   * it must be set to allow for the complete roundtrip
   * sending the message, the server operation time, and then
   * returning the message.
   * For services that have small/medium payloads,
   * like hydrology/family (except drms),
   * the limit can be smaller.
   * Also for clients, of any kind used
   * in callbacks from python, it is reasonable to
   * ensure they terminate within a specified maximum
   * limit. Most operations should be small.
   * Notice that when idle, this mechanism will close down
   * the socket, and allow resources on the server to
   * be recycled.
   */
  struct srv_connection {
    string host_port;                                    ///< like ip:port, or name:port
    int timeout_ms{1000};                                ///< timout for operations, def.1s
    int operation_timeout_ms{0};                         ///< life-time or max operation time if set>0
    unique_ptr<srv::fast_iosockstream> io;               ///< the io stream
    bool is_open{false};                                 ///< current observed/concluded state of the srv_connection
    std::size_t reconnect_count{0u};                     ///< reconnects resolved creating new connection to host
    utctime delay_before_new_attempt{from_seconds(0.1)}; ///< time to sleep before next reconnect attempt
    size_t max_connect_attempts{5u}; ///< if fail on connect, sleep/retry this number of times before give-up
    size_t max_retry_attempts{3u};   ///< if fail during message exchange, retry this number of times

    explicit srv_connection(string host_port, int const timeout_ms=1000, int const operation_timeout_ms = 0)
      : host_port{std::move(host_port)}
      , timeout_ms{timeout_ms}
      , operation_timeout_ms{operation_timeout_ms}
      , io{make_unique<srv::fast_iosockstream>()} {
    }

    srv_connection()=delete;// just to be clear, all ct through the above
    // Deleted copy constructor and assignment operator
    srv_connection(srv_connection const &) = delete;
    srv_connection& operator=(srv_connection const &) = delete;

    // Default move constructor and move assignment
    srv_connection(srv_connection&&) noexcept = default;
    srv_connection& operator=(srv_connection&&) noexcept = default;

    void set_operation_timeout() {
      if (operation_timeout_ms > 0)
        io->terminate_connection_after_timeout(operation_timeout_ms);
    }

    void open(int a_timeout_ms = 1000) {
      io->open(host_port, max(a_timeout_ms, timeout_ms));
      set_operation_timeout();
      is_open = true;
    }

    void close(int a_timeout_ms = 1000) {
      is_open = false; // if next line throws, consider closed anyway
      io->close(max(a_timeout_ms, timeout_ms));
    }

    /** reopen after failure, count it, so master sync can resubscribe */
    void reopen(int a_timeout_ms = 1000) {
      ++reconnect_count;
      open(a_timeout_ms);
    }

  };

  /**
   * @brief helper-class to enable 'autoconnect', lazy connect.
   *
   * Do some extra effort to establish connection, in case it is temporary down
   *
   */
  struct scoped_connect {
    srv_connection& sc;

    explicit scoped_connect(srv_connection& srv_con)
      : sc(srv_con) {
      if (!sc.is_open) { // auto-connect, and put some effort into succeeding
        bool rethrow = false;
        std::string rt_re;
        bool attempt_more;
        int retry_count = sc.max_connect_attempts;
        do {
          try {
            sc.open(); // either we succeed and finishes, or we get an exception
            attempt_more = false;
          } catch (dlib::socket_error const & se) { // capture socket error that might go away
                                                    // if we stay alive a little bit more
            if (--retry_count > 0 && strstr(se.what(), "unable to connect")) {
              attempt_more = true;
              sleep_for(sc.delay_before_new_attempt);
            } else {
              rt_re = se.what();
              rethrow = true;
              attempt_more = false;
            }
          } catch (exception const & re) { // just give up this type of error,
            rt_re = re.what();
            rethrow = true;
            attempt_more = false;
          }
        } while (attempt_more);
        if (rethrow)
          throw runtime_error(rt_re);
      } else {
        sc.set_operation_timeout(); // if we are already open, set timeout
        // note: there is possible for a glitch here, if sc.io terminated just before the above line
        // then it will be closed. This will however be handled in the retry-send, and socket reopened.
        // so we should be ok.
      }
    }

    ~scoped_connect() noexcept = default;

    scoped_connect(scoped_connect const &) = delete;
    scoped_connect(scoped_connect&&) = delete;
    scoped_connect& operator=(scoped_connect const &) = delete;
    scoped_connect() = delete;
    scoped_connect& operator=(scoped_connect&&) = delete;
  };

  /**
   * @brief utility to retry an io-operation that fails
   *
   * The socket connection created from the client stays connected from the first use until explicitly
   * closed by client.
   * The lifetime of the underlying socket *after* the close is ~ 120 seconds(windows, similar linux).
   * If the server is restarted we would enjoy that this layer 'auto-repair' with the server
   * if possible.
   * This routine ensures that this can happen for any io function sequence F.
   *
   * @note that there is a requirement that f(sc) can be invoked several times
   * without unwanted effects.
   * @tparam F a callable that accepts a srv_connection
   *
   * @param sc srv_connection that's repairable
   * @param f the callable
   */
  template <class F>
  auto do_io_with_repair_and_retry(srv_connection& sc, F&& f) {
    for (std::size_t retry = 0; retry < sc.max_retry_attempts; ++retry) {
      try {
        sc.set_operation_timeout(); // just before invoking send//receive sequence
        return f(sc);
      } catch (dlib::socket_error const &) {
        sleep_for(sc.delay_before_new_attempt);
        sc.reopen(); // dlib discover the socket problem, nice, but look for more below
      } catch (std::exception const & x) {
        // explanation: if the error is detected by stream lib while flush
        // we get here, so a reopen/retry might work
        // TODO: might be that we could use more targeted exceptions
        //
        if (!std::string(x.what()).contains("stream error"))
          throw; // if output stream error, it might be a broken connection,so lets try
        sleep_for(sc.delay_before_new_attempt);
        sc.reopen();
      }
    }
    throw runtime_error("Failed to establish connection with " + sc.host_port);
  }

  /** simple helper class to safely count live-connections, ref. on_connect */
  struct scoped_count {
    std::atomic_size_t& c; ///< a ref to the atomic size we use for counting

    explicit scoped_count(std::atomic_size_t& c)
      : c{c} {
      ++c;
    } // add count at construct

    ~scoped_count() {
      --c;
    } // decrement when leaving scope

    //-- ensure safe use
    scoped_count() = delete;
    scoped_count(scoped_count const &) = delete;
    scoped_count(scoped_count&&) = delete;
    scoped_count& operator=(scoped_count const &) = delete;
    scoped_count& operator=(scoped_count&&) = delete;
  };

  /** simple helper class to set dlib global logging properties */
  struct logging {
    /**
     * @brief ties all log output to fname
     * @details
     * if fname "" then standard output is used,
     * current file is flushed/closed prior to opening a new file.
     *
     * @param fname name of the file, make sure there are room, and make sure its on separate partitions for
     * critical/sensitive systems
     */
    static void output_file(std::string const & fname);
    /**
     * @brief set global log level
     * @details
     * Set the global log level, for all the loggers.
     * @param ll log-level as pr. dlib
     *   -100= TRACE
     *    0  = DEBUG
     *  100  = INFO
     *  200  = WARN
     *  300  = ERR
     *  400  = FATAL
     */
    static void level(dlib::log_level const ll);
  };

}
