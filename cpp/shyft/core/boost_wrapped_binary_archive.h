#pragma once
#include <boost/archive/binary_iarchive_impl.hpp>
#include <boost/archive/binary_oarchive_impl.hpp>
#include <boost/archive/detail/register_archive.hpp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/serialization_choice.h>
#include <shyft/version.h>

namespace shyft::core {

  /*
   * @brief archive_info is a preamble on all new *stored* archives
   * @detail
   * It allows us to discard/notify on archive streams, stored,
   * that is not supported.
   * A conservative approach is to only support *_version <= *_current_version,
   * but that is too strict in many context, since
   * boost serialization allows us to read old streams
   */
  struct archive_info {
    static constexpr unsigned int archive_info_flag =
      boost::archive::flags_last * 2 * 2 * 2;      /// allow some extra boost flags
    static constexpr int magic_value = 0x54464853; ///< SHFT small endian order
    std::int32_t magic{magic_value};               ///< S
    std::int32_t shyft_version{0};                 ///< shyft.major*100*1000, shyft.minor*100, shyft.patch
    std::int32_t boost_version{0};                 ///< BOOST_VERSION
    std::int32_t optionals{0};                     ///< flag bits we can use

    _version_ get_shyft_version() const {
      return {.major = shyft_version / 100000, .minor = shyft_version / 100 % 100, .patch = shyft_version % 100};
    };

    void set_shyft_version(_version_ const v) {
      shyft_version = v.major * 100000 + v.minor * 100 + v.patch;
    };

    bool is_valid() const {
      return magic == magic_value;
    }

    bool is_same_as_current_version() const {
      auto const v = get_shyft_version();
      return v.major == _version.major && v.minor == _version.minor && v.patch == _version.patch
          && BOOST_VERSION == boost_version;
    }

    auto operator<=>(archive_info const &) const = default;

    static archive_info current() {
      return {
        .magic = magic_value,
        .shyft_version = _version.major * 100000 + _version.minor * 100 + _version.patch,
        .boost_version = BOOST_VERSION};
    }
  };

  namespace detail {
    std::int32_t read_magic(std::istream &is);
    void save_archive_info(std::ostream &os, archive_info const &ai);
    bool load_archive_info(std::istream &is, archive_info &ai);
  }
}

namespace boost::archive {
  template <shyft::core::serialization_choice M>
  struct shyft_wrapped_bin_oarch
    : boost::archive::
        binary_oarchive_impl<shyft_wrapped_bin_oarch<M>, std::istream::char_type, std::istream::traits_type> {
    static constexpr auto mode = M;

    shyft_wrapped_bin_oarch(std::ostream &os, unsigned int flags = 0);
  };

  template <shyft::core::serialization_choice M>
  struct shyft_wrapped_bin_iarch
    : boost::archive::
        binary_iarchive_impl<shyft_wrapped_bin_iarch<M>, std::istream::char_type, std::istream::traits_type> {
    static constexpr auto mode = M;

    shyft_wrapped_bin_iarch(std::istream &os, unsigned int flags = 0);
    shyft::core::archive_info archive_info;
  };
}

// required by export
#define SHYFT_LAMBDA(r, data, elem) \
  BOOST_SERIALIZATION_REGISTER_ARCHIVE( \
    boost::archive::shyft_wrapped_bin_oarch<shyft::core::serialization_choice::elem>) \
  BOOST_SERIALIZATION_USE_ARRAY_OPTIMIZATION( \
    boost::archive::shyft_wrapped_bin_oarch<shyft::core::serialization_choice::elem>) \
  BOOST_SERIALIZATION_REGISTER_ARCHIVE( \
    boost::archive::shyft_wrapped_bin_iarch<shyft::core::serialization_choice::elem>) \
  BOOST_SERIALIZATION_USE_ARRAY_OPTIMIZATION( \
    boost::archive::shyft_wrapped_bin_iarch<shyft::core::serialization_choice::elem>)
BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_SERIALIZATION_CHOICE))
#undef SHYFT_LAMBDA
