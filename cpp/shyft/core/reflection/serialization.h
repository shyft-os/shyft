#pragma once

#include <concepts>
#include <cstdint>
#include <iosfwd>
#include <type_traits>

#include <boost/describe/bases.hpp>
#include <boost/describe/members.hpp>
#include <boost/mp11/algorithm.hpp>
#include <fmt/compile.h>
#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/optional.h>
#include <shyft/core/reflection.h>

namespace shyft::reflection {

  template <typename T, bool compat_mode>
  using portable_type_t = std::
    conditional_t<compat_mode, std::int32_t, std::conditional_t<std::signed_integral<T>, std::int64_t, std::uint64_t>>;

  template <typename T, bool compat_mode>
  constexpr bool has_portable_type = (sizeof(T) <= sizeof(portable_type_t<T, compat_mode>));

  template <bool compat_mode = false, typename T>
  constexpr bool read_blob(std::istream &s, T &t) {
    static_assert(has_portable_type<T, compat_mode>);
    using V = portable_type_t<T, compat_mode>;
    V v{};
    s.read(reinterpret_cast<char *>(&v), sizeof(V));
    if (s.fail())
      return false;
    t = static_cast<T>(v);
    return true;
  }

  template <bool compat_mode = false, typename T>
  constexpr bool write_blob(std::ostream &s, T const &t) {
    static_assert(has_portable_type<T, compat_mode>);
    using V = portable_type_t<T, compat_mode>;
    V const v = static_cast<V const>(t);
    s.write(reinterpret_cast<char const *>(&v), sizeof(V));
    return !s.fail();
  }

  template <bool backwards_compatible>
  using portable_integer = std::conditional_t<backwards_compatible, std::int32_t, std::uint64_t>;

  template <bool backwards_compatible = false>
  auto write_string(std::ostream &s, std::string_view o) {
    portable_integer<backwards_compatible> n = o.size();
    s.write((char const *) &n, sizeof(n));
    s.write(o.data(), o.size());
    return s.fail();
  }

  template <bool backwards_compatible = false>
  auto try_read_string(std::istream &s) {
    portable_integer<backwards_compatible> n{0};
    s.read((char *) &n, sizeof(n));
    if (!s)
      return none<std::string>;
    std::string e(n, '\0');
    s.read(e.data(), n);
    if (!s)
      return none<std::string>;
    return just(e);
  }

  template <typename Archive, reflected_struct T>
  void serialize(Archive &a, T &t, unsigned int const) {

    using bases = boost::describe::describe_bases<T, boost::describe::mod_any_access>;
    using base_indices = boost::mp11::mp_iota<boost::mp11::mp_size<bases>>;
    boost::mp11::mp_for_each<base_indices>([&]<typename base_index>(base_index) {
      using B = typename boost::mp11::mp_at<bases, base_index>::type;
      if constexpr (boost::mp11::mp_size<bases>::value == 1)
        a &core::core_nvp("base", static_cast<B &>(t));
      else {
        static constexpr auto base_name = [] {
          // NOTE:
          //   consider asserting that these identifiers do not collide with
          //   any of the member-names.
          //   - jeh
          auto result = std::array<char, 4 + std::numeric_limits<std::size_t>::max_digits10 + 1>();
          result.fill('\0');
          fmt::format_to(result.data(), FMT_COMPILE("base{}"), base_index::value);
          return result;
        }();
        a &core::core_nvp(base_name.data(), static_cast<B &>(t));
      }
    });

    using members = boost::describe::describe_members<T, boost::describe::mod_any_access>;
    boost::mp11::mp_for_each<members>([&](auto m) {
      a &core::core_nvp(m.name, t.*m.pointer);
    });
  }

  template <reflected_struct T>
  inline constexpr bool empty_struct = boost::mp11::mp_empty<
    boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>>::value;


#define SHYFT_DEFINE_SERIALIZE(C) \
  template <typename A> \
  friend void serialize(A &a, C &x, const unsigned int v) { \
    ::shyft::reflection::serialize(a, x, v); \
  }

}
