#include <fstream>
#include <iostream>

#include <shyft/core/dlib_utils.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::core {

  namespace {

    /**
     * @brief Configuration class for Dlib logger:
     */
    struct server_log_hook {
      std::ofstream fout;                         ///< if configured, as in open, make logs flow into this
      std::string log_file;                       ///< name of file to log to
      dlib::log_level current_level{dlib::LWARN}; ///< only log to file for these levels

      void log(
        std::string const & logger_name,
        dlib::log_level const & ll,
        dlib::uint64 const thread_id,
        char const * message_to_log);
      static void set_file(std::string const & fname);
      static void set_level(dlib::log_level const ll);
    };

    void server_log_hook::log(
      string const & logger_name,
      dlib::log_level const & ll,
      dlib::uint64 const thread_id,
      char const * message_to_log) {
      static calendar utc{}; // make it once, keep it
      if (ll >= current_level) {
        std::ostream* log_output = fout.is_open() ? &fout : &std::cout;
        (*log_output) << utc.to_string(utctime_now()) << " " << ll << " [" << thread_id << "] " << logger_name << ": "
                      << message_to_log << std::endl;
      }
    }

    static server_log_hook l; // one hook, and dlib ensures thread safety before calling log

    void server_log_hook::set_file(std::string const & fname) {
      if (l.fout.is_open()) {
        l.fout.flush();
        l.fout.close();
      } // close previous stream
      // prepare the global static logger,
      l.log_file = fname;
      if (fname.size()) // set blank to log to stdout (default)
        l.fout.open(l.log_file, std::ofstream::out | std::ofstream::app);
      // connect to dlib
      dlib::set_all_logging_output_hooks(l);
    }

    void server_log_hook::set_level(dlib::log_level const ll) {
      l.current_level = ll;
      dlib::set_all_logging_levels(ll);
    }
  }

  void logging::output_file(std::string const & fname) {
    server_log_hook::set_file(fname);
  }

  void logging::level(dlib::log_level const ll) {
    server_log_hook::set_level(ll);
  }
}
