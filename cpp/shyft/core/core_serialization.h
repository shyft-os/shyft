/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#ifndef _shyft_core_serialize_h
#define _shyft_core_serialize_h
#pragma once

#if defined(_WINDOWS)
#pragma warning(disable : 4267)
#pragma warning(disable : 4244)
#pragma warning(disable : 4503)
#endif

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/is_bitwise_serializable.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/version.hpp>

#include <shyft/core/core_archive.h>
/** in the header-files:
 * x_serialize_decl to be used as the *last*
 * line in the class/struct declaration
 */
#define x_serialize_decl() \
 private: \
  friend class boost::serialization::access; \
  template <class Archive> \
  void serialize(Archive &ar, const unsigned int file_version)


/** in the header-files:
 *  x_serialize_export_key to be used *outside*
 *  namespace at the *end* of the header file
 *  Compile speedups:
 *  The extern template decl for archives that we *know* we are using
 *  it did not have large effect, since we already where quite clean
 *  about where we used serialization, that depends on
 *  ADL (so only thing we can do is to make sure compile can find candidate early).
 */

#define x_decl_archives_external(T) \
  extern template void T::serialize<shyft::core::core_iarchive>( \
    shyft::core::core_iarchive & ar, const unsigned int version); \
  extern template void T::serialize<shyft::core::core_iarchive_stripped>( \
    shyft::core::core_iarchive_stripped & ar, const unsigned int version); \
  extern template void T::serialize<shyft::core::core_oarchive>( \
    shyft::core::core_oarchive & ar, const unsigned int version); \
  extern template void T::serialize<shyft::core::core_oarchive_stripped>( \
    shyft::core::core_oarchive_stripped & ar, const unsigned int version);

#define x_serialize_export_key2(T, N) \
  static_assert(!boost::is_polymorphic<T>::value, "use x_poly_serialize_export, for now polymorphic class:" N); \
  x_decl_archives_external(T)

#define x_poly_serialize_export_key2(T, N) \
  static_assert(boost::is_polymorphic<T>::value, "use x_serialize_export, for now non polymorphic classe:" N); \
  BOOST_CLASS_EXPORT_KEY2(T, N); \
  x_decl_archives_external(T)


#define x_serialize_export_key(T) x_serialize_export_key2(T, BOOST_PP_STRINGIZE(T))
#define x_poly_serialize_export_key(T) x_poly_serialize_export_key2(T, BOOST_PP_STRINGIZE(T))

// no tracking implies no poly as a first approach
#define x_serialize_export_key_nt(T) \
  x_serialize_export_key(T); \
  BOOST_CLASS_TRACKING(T, boost::serialization::track_never)

#define x_serialize_binary(T) \
  BOOST_IS_BITWISE_SERIALIZABLE(T); \
  BOOST_CLASS_IMPLEMENTATION(T, boost::serialization::primitive_type)
/** in the library implementation files:
 *  x_serialize_implement to be use in the library
 *  implementation file, *after*
 *  template<class Archive>
 *     namespace::cls::serialize(Archive&ar,const int v) {} impl
 */
#define x_serialize_implement(T) \
  static_assert(!boost::is_polymorphic<T>::value, "must be not be polymorphic:" BOOST_PP_STRINGIZE(T));

#define x_poly_serialize_implement(T) \
  static_assert(boost::is_polymorphic<T>::value, "must be be polymorphic:" BOOST_PP_STRINGIZE(T)); \
  BOOST_CLASS_EXPORT_IMPLEMENT(T)


/** in the library implementation files after x_serialize_implement
 */
#define x_serialize_archive(T, AO, AI) \
  template void T::serialize(AO &, const unsigned int); \
  template void T::serialize(AI &, const unsigned int);
#endif
