// kept for ref: we might want to let ui have a separate library
// #include <shyft/core/core_archive.h>
#include <shyft/energy_market/ui/ui_core.h>

#if 0
using namespace shyft::core;

template <class Archive>
void shyft::energy_market::ui::layout_info::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("id", id) & core_nvp("name", name) & core_nvp("json", json);
}

x_serialize_instantiate_and_register(shyft::energy_market::ui::layout_info);
#endif