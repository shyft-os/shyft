#pragma once
#include <memory>
#include <stdexcept>
#include <string>

#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/srv/client.h>
#include <shyft/srv/msg_defs.h>

namespace shyft::energy_market::ui {

  struct config_client : srv::client<layout_info> {
    using super = srv::client<layout_info>;
    config_client() = delete;

    config_client(std::string host_port, int timeout_ms = 1000, int operation_timeout_ms = 0)
      : super{host_port, timeout_ms, operation_timeout_ms} {
    }

    /** @brief read model from server
     * If args and name are given, will try to generate new layout
     * if unable to find layout with provided  ID.
     *
     * @param mid: Model ID to read
     * @param name: Name of layout to generate
     * @param args: Arguments to generate layout (If not given, uses defaults)
     * @return read, or generated, layout.
     */
    auto read_model_with_args(
      std::int64_t mid,
      std::string const & layout_name,
      std::string const & args,
      bool store_layout = false) {
      core::scoped_connect sc(c);
      std::shared_ptr<layout_info> r;
      core::do_io_with_repair_and_retry(c, [&mid, &layout_name, &args, &r, store_layout](core::srv_connection& c) {
        auto& io = *c.io;
        if (!reflection::write_blob<true>(io, srv::message_type::MODEL_READ_ARGS))
          throw dlib::socket_error("failed writing tag");
        core::core_oarchive oa(io, core::core_arch_flags);
        oa << mid << layout_name << args << store_layout;
        c.set_operation_timeout(); // refresh timeout if set
        srv::message_type::type response_type{srv::message_type::SERVER_EXCEPTION};
        if (!reflection::read_blob<true>(io, response_type))
          throw dlib::socket_error("failed reading reply tag");
        if (response_type == srv::message_type::SERVER_EXCEPTION) {
          auto msg = reflection::try_read_string<true>(io);
          if (!msg)
            throw dlib::socket_error(fmt::format("failed reading string"));
          throw std::runtime_error(*msg);
        } else if (response_type == srv::message_type::MODEL_READ_ARGS) {
          core::core_iarchive ia(io, core::core_arch_flags);
          ia >> r;
        } else {
          throw std::runtime_error(fmt::format("Got unexpected response: {}", response_type));
        }
      });
      return r;
    }
  };

}
