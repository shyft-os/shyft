#include <shyft/energy_market/stm/evaluate.h>

namespace shyft::energy_market::stm {

  // FIXME:
  //   1) should split out nan-filling into separate algo.
  //   2) should just do a post-order local rebind.
  //   - jeh
  bool rebind_expression(stm_system& model, std::string_view model_id, bool nan_fill_missing) {

    auto const model_prefix = url_fmt_root(model_id);
    auto const missing_fill = std::make_shared<time_series::dd::gpoint_ts>(model.run_params.run_time_axis, shyft::nan);

    auto const rebind_ts = [&](auto& root_ts) {
      std::vector<std::pair<time_series::dd::aref_ts*, std::shared_ptr<time_series::dd::gpoint_ts const>>> rebinds;

      auto const is_external = [&](auto const * ts) {
        return !ts->id.starts_with(model_prefix);
      };

      with_reference_ts(model_id, model, root_ts.ts, ignore, [&](time_series::dd::aref_ts& ts) {
        if (std::ranges::contains(std::views::elements<0>(rebinds), &ts))
          return;
        rebinds.emplace_back(&ts, [&] {
          if (is_external(&ts))
            return ts.rep;
          if (!ts.rep)
            return std::shared_ptr<time_series::dd::gpoint_ts const>{};
          return url_get_ts(model, ts.id)
            .transform([&](auto&& ts) {
              return time_series::dd::ensure_gpoint(ts.ts);
            })
            .or_else(boost::hof::always(just(std::shared_ptr<time_series::dd::gpoint_ts const>{})))
            .value();
        }());
      });

      if (std::ranges::all_of(std::views::elements<0>(rebinds), is_external))
        return false;

      root_ts.do_unbind();
      bool const all_bound = std::ranges::fold_left(rebinds, true, [&](auto all_bound, auto&& rebind) {
        auto&& [ref, rep] = rebind;
        if (!is_external(ref) && !rep && nan_fill_missing)
          rep = missing_fill;
        ref->rep = std::move(rep);
        return all_bound && !ref->needs_bind();
      });
      if (all_bound)
        root_ts.do_bind();
      return true;
    };

    bool was_rebound = false;
    url_with_all(model_id, model, [&]<typename T>(ignore_t, T& attr, ignore_t) {
      if constexpr (std::is_same_v<time_series::dd::apoint_ts, T>)
        was_rebound |= rebind_ts(attr);
    });
    url_with_custom(model_id, model, [&]<typename T>(ignore_t, T& attr, ignore_t) {
      if constexpr (std::is_same_v<time_series::dd::apoint_ts, T>)
        was_rebound |= rebind_ts(attr);
    });
    return was_rebound;
  }
}
