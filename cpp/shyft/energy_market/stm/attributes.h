#pragma once
#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>
#include <tuple>

#include <boost/mp11/algorithm.hpp>
#include <fmt/chrono.h>

#include <shyft/core/math_utilities.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/unit_group_type.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::core::utctimespan;
  using shyft::core::calendar;
  using shyft::core::geo_point;
  using shyft::time_series::dd::apoint_ts;
  using shyft::time_series::POINT_AVERAGE_VALUE;
  using shyft::time_axis::generic_dt;

  template <typename T>
  using t_T = std::shared_ptr<std::map<utctime, T>>; ///< all attributes are time-dependent (as in 'from that time, the value of
                                           ///< this attribute should be...')

  //-- these are the basic types (in addition to the already existing value-types from time_series library)
  using t_xy_ = t_T<std::shared_ptr<hydro_power::xy_point_curve>>;                   ///< given t: y=f(x)
  using t_xyz_ = t_T<std::shared_ptr<hydro_power::xy_point_curve_with_z>>;           ///< given t: y=f(x,z)
  using t_xyz_list_ = t_T<std::shared_ptr<std::vector<hydro_power::xy_point_curve_with_z>>>; ///< given t: give z, -> xy
  using t_turbine_description_ = t_T<std::shared_ptr<hydro_power::turbine_description>>;

  using attr_types = std::tuple<
    bool,
    double,
    std::int64_t,
    std::uint64_t,
    time_series::dd::apoint_ts,
    std::shared_ptr<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>,
    std::shared_ptr<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve_with_z>>>,
    std::shared_ptr<std::map<utctime, std::shared_ptr<std::vector<hydro_power::xy_point_curve_with_z>>>>,
    std::shared_ptr<std::map<utctime, std::shared_ptr<hydro_power::turbine_description>>>,
    std::string,
    time_series::dd::ats_vector,
    energy_market::stm::unit_group_type,
    time_axis::generic_dt,
    geo_point>;

  template <typename T>
  constexpr bool is_attr_v = boost::mp11::mp_contains<attr_types, T>::value;

  using any_attr = boost::mp11::mp_rename<attr_types, std::variant>;

  any_attr clone_attr(any_attr const & attr);

  template <typename T>
  inline constexpr bool is_any_attr_v = std::is_same_v<std::remove_cvref_t<T>, any_attr>;

}
