#pragma once
#include <tuple>
#include <type_traits>

#include <boost/mp11/algorithm.hpp>
#include <boost/mp11/list.hpp>

#include <shyft/core/reflection.h>
#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/attributes.h>

namespace shyft::energy_market::stm {

#define SHYFT_STM_COMPONENT(name, bases, members) \
  SHYFT_DEFINE_STRUCT(name, bases, members); \
  using am_component = void;

  template <typename T>
  concept component = requires { typename std::remove_cvref_t<T>::am_component; };;

  template <typename T>
  inline constexpr bool is_component_v = component<T>;


#if !defined(_MSC_VER)

  namespace detail {

    template <typename T, typename S>
    struct paths_of_impl {
      using type = std::tuple<>;
    };

    template <typename T, typename S>
    requires is_attr_v<T>
    struct paths_of_impl<T, S> {
      using type = std::tuple<S>;
    };

    template <typename T, typename S>
    requires is_component_v<T>
    struct paths_of_impl<T, S> {

      template <typename U, typename M>
      using fn = boost::mp11::mp_append<
        U,
        typename paths_of_impl<
          member_type_t<M::pointer>,
          boost::mp11::mp_push_back<S, std::integral_constant<decltype(auto(M::pointer)), M::pointer>>>::type>;

      using type = boost::mp11::mp_fold<members_of<T>, std::tuple<>, fn>;
    };

  }

  template <typename T, typename S = std::tuple<>>
  using paths_of = typename detail::paths_of_impl<std::remove_cvref_t<T>, S>::type;

  template<typename... P>
  constexpr decltype(auto) walk_path(auto &&c,std::tuple<P...> p){
    if constexpr(sizeof...(P) == 0)
      return SHYFT_FWD(c);
    else
      return [&]<std::size_t... I>(std::index_sequence<I...>) -> decltype(auto){
        return walk_path(SHYFT_FWD(c).*(std::get<0>(p)()), std::make_tuple(std::get<I + 1>(p)...));
      }(std::make_index_sequence<sizeof...(P) - 1>{});
  }

  template<typename T>
  auto equal_attributes(const T &t0,const T &t1){
    if constexpr(std::is_same_v<T, double>)
      return core::nan_equal(t0, t1);
    else if constexpr(is_std_shared_ptr<T>)
      return t0 == t1 || (t0 && t1 && equal_attributes(*t0, *t1));
    else if constexpr(is_std_vector<T>)
      return std::ranges::equal(t0, t1, [](const auto &u0, const auto &u1){ return equal_attributes(u0, u1); });
    else if constexpr(is_std_map<T>)
      return std::ranges::equal(t0, t1, [](const auto &u0, const auto &u1){ return u0.first == u1.first && equal_attributes(u0.second, u1.second); });
    else{
      return t0 == t1;
    }
  }

  template<component C>
  auto equal_component(const C &c0, const C &c1) {
    return std::apply(
      [&](auto... P){
        return (equal_attributes(walk_path(c0, P), walk_path(c1, P)) && ...);
      },
      paths_of<C>{});
  }
  template<component C>
  auto equal_component_ptr(const std::shared_ptr<C> &c0, const std::shared_ptr<C> &c1) {
    return (c0 == c1) || (c0 && c1 && equal_component(*c0, *c1));
  }
  template<component C>
  auto equal_component_ptrs(const std::vector<std::shared_ptr<C>> &C0, const std::vector<std::shared_ptr<C>> &C1) {
    return std::ranges::equal(C0, C1, &equal_component_ptr<C>);
  }

#endif

}
