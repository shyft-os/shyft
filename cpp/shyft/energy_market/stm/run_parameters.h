#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <cstdint>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/energy_market/stm/reflection.h>

namespace shyft::energy_market::stm {

  using core::utctime;

  struct stm_system;

  /**
   * @brief run parameters
   *
   * @details
   * Only plays a  role in the context of stm_system, keeping
   * parameters for the last applied, or to be applied
   * run.
   * Due to its role, there are separate calls in web-api for inspecting
   * the content of the run parameters.
   * Serialization is only via named attribute (reflection attribute serializer).
   * The Url generation is explicit set to address the stm_model, as a sub-part of the
   * model
   */
  struct run_parameters {

    /**
     * @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    stm_system* mdl{nullptr}; ///< _not_ owned ref to the owning model that is _required_ to outlive the scope of the
                              ///< run_parameters,  The pointer should be const, but the model can be modified.

    run_parameters();
    run_parameters(stm_system* mdl);

    bool operator==(run_parameters const & other) const;

    bool operator!=(run_parameters const & other) const {
      return !(*this == other);
    }

    auto operator<=>(run_parameters const & other) const = default;

    std::vector<std::string> all_urls(std::string const &) const;

    // Attributes:
    BOOST_HANA_DEFINE_STRUCT(
      run_parameters,
      (std::uint16_t, n_inc_runs),  ///< Number of runs with incremental
      (std::uint16_t, n_full_runs), ///< Number of full runs
      (bool, head_opt),             ///< head optimization on/off
      (generic_dt, run_time_axis),  ///< the run_time_axis for optimization/simulation/computation
      (std::vector<std::pair<utctime, std::string>>, fx_log) ///< the logs as collected from the algorithm execution
    );

    SHYFT_STM_COMPONENT(run_parameters, (), (n_inc_runs, n_full_runs, head_opt, run_time_axis, fx_log));
    // notice: no x_serialize_decl(), its serialized as part of attribute-set of stm_system.run_parameters
  };

  using run_parameters_ = std::shared_ptr<run_parameters>;
  using run_parameters__ = std::weak_ptr<run_parameters>;
}

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::run_parameters);
SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::run_parameters);
