#include <cstdint>
#include <iostream>
#include <memory>
#include <string>

#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/srv/task/server.h>

namespace shyft::energy_market::stm::srv::task {

  using shyft::srv::model_info;

  bool server::message_dispatch(
    std::istream& in,
    std::ostream& out,
    message_type::type msg_type,
    shyft::srv::stale_connection_sensor& scs) {
    using core::core_iarchive;
    using core::core_oarchive;
    using core::core_arch_flags;

    if (!base::message_dispatch(in, out, msg_type, scs)) {
      core_iarchive ia(in, core_arch_flags);
      switch (msg_type) {
      case message_type::ADD_CASE: {
        std::shared_ptr<stm_case> run;
        std::int64_t mid;
        ia >> mid >> run;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        session->add_case(run);
        model_info mi;
        if (!db.try_get_info_item(mid, mi)) {
          mi = model_info(session->id, session->name, session->created, session->json);
        }
        db.store_model(session, mi);
        if (!reflection::write_blob<true>(out, message_type::ADD_CASE))
          throw dlib::socket_error("failed writing tag");
      } break;
      case message_type::REMOVE_CASE_ID: {
        std::int64_t mid;
        std::int64_t rid;
        ia >> mid >> rid;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto result = session->remove_case(rid);
        model_info mi;
        if (!db.try_get_info_item(mid, mi)) {
          mi = model_info(session->id, session->name, session->created, session->json);
        }
        db.store_model(session, mi);
        if (!reflection::write_blob<true>(out, message_type::REMOVE_CASE_ID))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << result;
      } break;
      case message_type::REMOVE_CASE_NAME: {
        std::int64_t mid;
        std::string rname;
        ia >> mid >> rname;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto result = session->remove_case(rname);
        model_info mi;
        if (!db.try_get_info_item(mid, mi)) {
          mi = model_info(session->id, session->name, session->created, session->json);
        }
        db.store_model(session, mi);
        if (!reflection::write_blob<true>(out, message_type::REMOVE_CASE_NAME))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << result;
      } break;
      case message_type::GET_CASE_ID: {
        std::int64_t mid;
        std::int64_t rid;
        ia >> mid >> rid;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto result = session->get_case(rid);
        if (!reflection::write_blob<true>(out, message_type::GET_CASE_ID))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << result;
      } break;
      case message_type::GET_CASE_NAME: {
        std::int64_t mid;
        std::string rname;
        ia >> mid >> rname;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto result = session->get_case(rname);
        if (!reflection::write_blob<true>(out, message_type::GET_CASE_NAME))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << result;
      } break;
      case message_type::UPDATE_CASE: {
        std::int64_t mid;
        stm_case c;
        ia >> mid >> c;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        if (session->update_case(c)) {
          model_info mi;
          if (!db.try_get_info_item(mid, mi)) {
            mi = model_info(session->id, session->name, session->created, session->json);
          }
          db.store_model(session, mi);
        }
        if (!reflection::write_blob<true>(out, message_type::UPDATE_CASE))
          throw dlib::socket_error("failed writing tag");
      } break;
      case message_type::ADD_MODEL_REF: {
        std::int64_t mid;
        std::int64_t rid;
        std::shared_ptr<model_ref> mr;
        ia >> mid >> rid >> mr;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto run = session->get_case(rid);
        if (run) { // If we found the run, we can add to it
          run->add_model_ref(mr);
          model_info mi;
          if (!db.try_get_info_item(mid, mi)) {
            mi = model_info(session->id, session->name, session->created, session->json);
          }
          db.store_model(session, mi);
        }
        if (!reflection::write_blob<true>(out, message_type::ADD_MODEL_REF))
          throw dlib::socket_error("failed writing tag");
      } break;
      case message_type::UPDATE_MODEL_REF: {
        std::int64_t mid;
        std::int64_t rid;
        model_ref_ mr;
        ia >> mid >> rid >> mr;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto run = session->get_case(rid);
        if (run) { // If we found the run, we can add to it
          run->update_model_ref(mr);
          model_info mi;
          if (!db.try_get_info_item(mid, mi)) {
            mi = model_info(session->id, session->name, session->created, session->json);
          }
          db.store_model(session, mi);
        }
        if (!reflection::write_blob<true>(out, message_type::UPDATE_MODEL_REF))
          throw dlib::socket_error("failed writing tag");
      } break;
      case message_type::REMOVE_MODEL_REF: {
        std::int64_t mid;
        std::int64_t rid;
        std::string mkey;
        bool result = false;
        ia >> mid >> rid >> mkey;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto run = session->get_case(rid);
        if (run)
          result = run->remove_model_ref(mkey);
        if (result) { // If model ref was successully removed, we must update session
          model_info mi;
          if (!db.try_get_info_item(mid, mi)) {
            mi = model_info(session->id, session->name, session->created, session->json);
          }
          db.store_model(session, mi);
        }
        if (!reflection::write_blob<true>(out, message_type::REMOVE_MODEL_REF))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << result;
      } break;
      case message_type::GET_MODEL_REF: {
        std::int64_t mid;
        std::int64_t rid;
        std::string mkey;
        model_ref_ mr = nullptr;
        ia >> mid >> rid >> mkey;
        shyft::srv::scoped_processing sp{scs};
        auto session = db.read_model(mid);
        auto run = session->get_case(rid);
        if (run)
          mr = run->get_model_ref(mkey);
        if (!reflection::write_blob<true>(out, message_type::GET_MODEL_REF))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << mr;
      } break;
      case message_type::FX: {
        std::int64_t mid;
        std::string args;
        ia >> mid >> args;
        shyft::srv::scoped_processing sp{scs};
        auto result = try_call(callback, mid, args).value_or(false);
        if (!reflection::write_blob<true>(out, message_type::FX))
          throw dlib::socket_error("failed writing tag");
        core_oarchive oa(out, core_arch_flags);
        oa << result;
      } break;
      // other
      default:
        return false;
      }
    }
    return true;
  }


}
