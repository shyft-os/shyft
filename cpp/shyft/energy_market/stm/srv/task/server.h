#pragma once

#include <cstdint>
#include <functional>
#include <string>

#include <shyft/core/core_archive.h>
#include <shyft/energy_market/stm/srv/task/msg_defs.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/srv/db.h>
#include <shyft/srv/server.h>

namespace shyft::energy_market::stm::srv::task {
  struct server : shyft::srv::server<shyft::srv::db<stm_task>> {
    using base = shyft::srv::server<shyft::srv::db<stm_task>>;
    using base::base;

    std::function<bool(std::int64_t, std::string)> callback;

   protected:
    virtual bool message_dispatch(
      std::istream& in,
      std::ostream& out,
      message_type::type msg_type,
      shyft::srv::stale_connection_sensor& scs) override;
  };
}
