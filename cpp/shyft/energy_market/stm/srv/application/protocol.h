#pragma once

#include <cstdint>
#include <iosfwd>
#include <memory>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

#include <boost/describe/members.hpp>
#include <boost/mp11/list.hpp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>
#include <fmt/core.h>

#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/protocol.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft {
  namespace energy_market::stm::srv::experimental::application {
#define SHYFT_STM_APPLICATION_MODELS (task, layout)

    SHYFT_DEFINE_ENUM(model_tag, std::uint8_t, SHYFT_STM_APPLICATION_MODELS);


#define SHYFT_STM_APPLICATION_PROTOCOL \
  (store_layout, \
   read_layout, \
   read_layouts, \
   remove_layout, \
   store_task, \
   read_task, \
   read_tasks, \
   remove_task, \
   read_layout_with_args, \
   add_case, \
   remove_case_by_id, \
   remove_case_by_name, \
   get_case_by_id, \
   get_case_by_name, \
   update_case, \
   add_task_ref, \
   remove_task_ref, \
   get_task_ref, \
   get_task_infos, \
   get_layout_infos, \
   update_task_info, \
   update_layout_info, \
   task_fx)

    SHYFT_DEFINE_ENUM(message_tag, std::uint8_t, SHYFT_STM_APPLICATION_PROTOCOL);

    namespace detail {
      template <message_tag tag>
      struct to_model_tag_impl { };

      template <message_tag tag>
      requires(
        tag
        == any_of(
          message_tag::store_layout,
          message_tag::read_layout,
          message_tag::read_layouts,
          message_tag::remove_layout,
          message_tag::get_layout_infos,
          message_tag::update_layout_info))
      struct to_model_tag_impl<tag> {
        constexpr static inline auto value = model_tag::layout;
      };

      template <message_tag tag>
      requires(
        tag
        == any_of(
          message_tag::store_task,
          message_tag::read_task,
          message_tag::read_tasks,
          message_tag::remove_task,
          message_tag::get_task_infos,
          message_tag::update_task_info))
      struct to_model_tag_impl<tag> {
        constexpr static inline auto value = model_tag::task;
      };

      template <auto>
      struct model_type_impl { };

      template <>
      struct model_type_impl<model_tag::layout> {
        using type = ui::layout_info;
      };

      template <>
      struct model_type_impl<model_tag::task> {
        using type = stm_task;
      };

      template <auto m>
      requires(std::is_same_v<message_tag, decltype(m)>)
      struct model_type_impl<m> : model_type_impl<to_model_tag_impl<m>::value> { };
    }

    template <auto tag>
    using model_type = typename detail::model_type_impl<tag>::type;

    template <message_tag tag>
    constexpr static inline auto to_model_tag = detail::to_model_tag_impl<tag>::value;

    inline constexpr auto application_protocol = protocols::basic_protocol<message_tag>{};

    template <message_tag msg>
    inline constexpr auto application_message = protocols::message<application_protocol>{.tag = msg};

#define SHYFT_LAMBDA(r, data, elem) inline constexpr auto elem##_msg = application_message<message_tag::elem>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_APPLICATION_PROTOCOL))
#undef SHYFT_LAMBDA

    inline constexpr auto store_model_msg = any_of(store_layout_msg, store_task_msg);
    inline constexpr auto read_model_msg = any_of(read_layout_msg, read_task_msg);
    inline constexpr auto read_models_msg = any_of(read_layouts_msg, read_tasks_msg);
    inline constexpr auto remove_model_msg = any_of(remove_layout_msg, remove_task_msg);
    inline constexpr auto get_model_infos_msg = any_of(get_task_infos_msg, get_layout_infos_msg);
    inline constexpr auto update_model_info_msg = any_of(update_task_info_msg, update_layout_info_msg);
  }

  namespace protocols {
    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::get_model_infos_msg)
    struct request<msg> {

      std::vector<std::int64_t> model_ids;
      std::optional<core::utcperiod> period{};

      SHYFT_DEFINE_STRUCT(request, (), (model_ids, period));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::update_model_info_msg)
    struct request<msg> {

      std::optional<std::int64_t> model_id;
      shyft::srv::model_info model_info;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, model_info));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::store_model_msg)
    struct request<msg> {

      energy_market::stm::srv::experimental::application::model_type<msg.tag> model;
      std::optional<shyft::srv::model_info> model_info;

      SHYFT_DEFINE_STRUCT(request, (), (model, model_info));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::read_model_msg)
    struct request<msg> {

      std::int64_t model_id;

      SHYFT_DEFINE_STRUCT(request, (), (model_id));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::read_models_msg)
    struct request<msg> {

      std::vector<std::int64_t> model_ids;

      SHYFT_DEFINE_STRUCT(request, (), (model_ids));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::remove_model_msg)
    struct request<msg> {

      std::int64_t model_id;

      SHYFT_DEFINE_STRUCT(request, (), (model_id));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::experimental::application::add_case_msg> {

      std::int64_t model_id;
      std::shared_ptr<energy_market::stm::srv::stm_case> cse;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, cse));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of(
        energy_market::stm::srv::experimental::application::get_case_by_id_msg,
        energy_market::stm::srv::experimental::application::remove_case_by_id_msg))
    struct request<msg> {

      std::int64_t model_id;
      std::int64_t case_id;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, case_id));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of(
        energy_market::stm::srv::experimental::application::get_case_by_name_msg,
        energy_market::stm::srv::experimental::application::remove_case_by_name_msg))
    struct request<msg> {

      std::int64_t model_id;
      std::string case_name;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, case_name));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::experimental::application::update_case_msg> {

      std::int64_t mode_id;
      std::shared_ptr<energy_market::stm::srv::stm_case> case_;

      SHYFT_DEFINE_STRUCT(request, (), (mode_id, case_));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::experimental::application::add_task_ref_msg> {

      std::int64_t model_id;
      std::int64_t case_id;
      std::shared_ptr<energy_market::stm::srv::model_ref> model_ref;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, case_id, model_ref));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of(
        energy_market::stm::srv::experimental::application::remove_task_ref_msg,
        energy_market::stm::srv::experimental::application::get_task_ref_msg))
    struct request<msg> {

      std::int64_t model_id;
      std::int64_t case_id;
      std::string model_key;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, case_id, model_key));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::experimental::application::task_fx_msg> {

      std::int64_t model_id;
      std::string args;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, args));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::experimental::application::read_layout_with_args_msg> {

      std::int64_t model_id;
      std::string layout_name;
      std::string args;
      bool store_layout;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, layout_name, args, store_layout));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::get_model_infos_msg)
    struct reply<msg> {

      std::vector<srv::model_info> model_infos;

      SHYFT_DEFINE_STRUCT(reply, (), (model_infos));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::store_model_msg)
    struct reply<msg> {

      std::int64_t model_id;
      SHYFT_DEFINE_STRUCT(reply, (), (model_id));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::read_model_msg)
    struct reply<msg> {

      energy_market::stm::srv::experimental::application::model_type<msg.tag> model;

      SHYFT_DEFINE_STRUCT(reply, (), (model));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::read_models_msg)
    struct reply<msg> {

      std::vector<energy_market::stm::srv::experimental::application::model_type<msg.tag>> models;

      SHYFT_DEFINE_STRUCT(reply, (), (models));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of(
        energy_market::stm::srv::experimental::application::remove_model_msg,
        energy_market::stm::srv::experimental::application::update_model_info_msg,
        energy_market::stm::srv::experimental::application::add_case_msg,
        energy_market::stm::srv::experimental::application::task_fx_msg,
        energy_market::stm::srv::experimental::application::remove_case_by_id_msg,
        energy_market::stm::srv::experimental::application::remove_case_by_name_msg,
        energy_market::stm::srv::experimental::application::remove_task_ref_msg,
        energy_market::stm::srv::experimental::application::add_task_ref_msg))
    struct reply<msg> {

      bool result;
      SHYFT_DEFINE_STRUCT(reply, (), (result));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of(
        energy_market::stm::srv::experimental::application::get_case_by_name_msg,
        energy_market::stm::srv::experimental::application::get_case_by_id_msg))
    struct reply<msg> {

      std::shared_ptr<energy_market::stm::srv::stm_case> case_;

      SHYFT_DEFINE_STRUCT(reply, (), (case_));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(msg == energy_market::stm::srv::experimental::application::get_task_ref_msg)
    struct reply<msg> {

      std::shared_ptr<energy_market::stm::srv::model_ref> ref;
      SHYFT_DEFINE_STRUCT(reply, (), (ref));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::experimental::application::read_layout_with_args_msg> {

      energy_market::ui::layout_info layout;

      SHYFT_DEFINE_STRUCT(reply, (), (layout));

      auto operator<=>(reply const &) const = default;
    };
  }

  namespace energy_market::stm::srv::experimental::application {
#define SHYFT_LAMBDA(r, data, elem) using elem##_request = protocols::request<application_message<message_tag::elem>>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_APPLICATION_PROTOCOL))
#undef SHYFT_LAMBDA

#define SHYFT_LAMBDA(r, data, elem) using elem##_reply = protocols::reply<application_message<message_tag::elem>>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_APPLICATION_PROTOCOL))
#undef SHYFT_LAMBDA
  }

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::experimental::application::message_tag);
