#include <iterator>
#include <memory>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

#include <fmt/core.h>

#include <shyft/core/reflection.h>
#include <shyft/core/subscription.h>
#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/srv/application/message_handler.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/energy_market/stm/srv/application/request_handler.h>
#include <shyft/mp/parser_utils.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_emit.h>
#include <shyft/web_api/generators/layout_info.h>
#include <shyft/web_api/json_struct.h>
#include <shyft/web_api/web_diag_response.h>

namespace shyft::web_api::energy_market::srv::experimental::application {

  template <typename M>
  auto get(json const & data) {
    using member_type = member_type_t<M::pointer>;
    if constexpr (is_std_shared_ptr<member_type>) {
      using element_type = typename member_type::element_type;
      return std::make_shared<element_type>(data.required<element_type>(M::name));
    } else if constexpr (is_std_optional<member_type> || is_std_vector<member_type>) {
      using stored_type =
        std::conditional_t<is_std_optional<member_type>, typename member_type::value_type, member_type>;
      auto const maybe_value = data.optional<stored_type>(M::name);
      return maybe_value.is_initialized() ? member_type{*maybe_value} : member_type{};
    } else {
      return data.required<member_type>(M::name);
    }
  }

  template <reflected_struct T>
  T parse(json const & data) {
    T out{};
    for_each_member<T>([&]<typename M>(M) {
      std::invoke(M::pointer, out) = get<M>(data);
    });
    return out;
  }

  template <reflected_struct T>
  std::string make_serialized_reply(std::string const & req_id, T const & rep) {
    auto serialized_rep = fmt::format("{{\"{}\":\"{}\",\"{}\":", "request_id", req_id, "result");
    auto sink = std::back_inserter(serialized_rep);
    for_each_member<T>([&](auto m) {
      generator::emit(sink, std::invoke(m.pointer, rep));
    });
    sink = '}';
    return serialized_rep;
  }

  namespace app = shyft::energy_market::stm::srv::experimental::application;

  request_handler::request_handler(app::model_manager& manager)
    : handler(manager) {
    for_each_enum<app::message_tag>([&](auto e) {
      mapped_enums.emplace(e.name, [&](std::string const & req_id, request const & json) {
        auto const req = parse<protocols::request<app::application_message<e.value>>>(json.request_data);
        auto const rep = handler(std::move(req));
        return make_serialized_reply(req_id, std::move(rep));
      });
    });
  }

  bg_work_result request_handler::do_the_work(std::string const & req_str) {
    auto const req_id = [&] {
      auto id = recover_request_id(req_str);
      return id.empty() ? std::string{request_id_not_found} : id;
    }();

    try {
      request req;
      grammar::request_grammar<char const *> grammar;
      if (!mp::grammar::phrase_parser(req_str, grammar, req))
        return bg_work_result{fmt::format("not understood: {}", req_str)};

      auto const it = mapped_enums.find(req.keyword);
      if (it == mapped_enums.end()) {
        return bg_work_result{gen_diagnostics_response(req_id, fmt::format("Unknown keyword: {}", req.keyword))};
      }

      return bg_work_result{it->second(req_id, req)};
    } catch (std::runtime_error const & re) {
      return bg_work_result{gen_diagnostics_response(req_id, re.what())};
    }
  }

  bg_work_result
    request_handler::do_subscription_work(std::shared_ptr<shyft::core::subscription::observer_base> const & o) {
    return {};
  }
}
