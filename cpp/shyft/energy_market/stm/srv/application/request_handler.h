#pragma once

#include <functional>
#include <map>
#include <memory>
#include <string>

#include <shyft/core/subscription.h>
#include <shyft/energy_market/stm/srv/application/message_handler.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/json_struct.h>

namespace shyft::web_api::energy_market::srv::experimental::application {

  struct request_handler : base_request_handler {
    request_handler(shyft::energy_market::stm::srv::experimental::application::model_manager& manager);

    bg_work_result do_the_work(std::string const & req_str) override;
    bg_work_result do_subscription_work(std::shared_ptr<shyft::core::subscription::observer_base> const & o) override;

   private:
    shyft::energy_market::stm::srv::experimental::application::message_handler handler;
    std::map<std::string, std::function<std::string(std::string const &, request const &)>> mapped_enums;
  };
}
