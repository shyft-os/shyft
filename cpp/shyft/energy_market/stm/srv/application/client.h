#pragma once

#include <utility>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/protocol.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>

namespace shyft::energy_market::stm::srv::experimental::application {

  struct client : protocols::basic_client<> {
#define SHYFT_LAMBDA(r, data, elem) \
  elem##_reply elem(elem##_request request) { \
    return send(std::move(request)); \
  }
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_APPLICATION_PROTOCOL))
#undef SHYFT_LAMBDA
  };

}
