#pragma once

#include <functional>
#include <string>
#include <string_view>

#include <dlib/logger.h>

#include <shyft/core/protocol.h>
#include <shyft/energy_market/stm/srv/application/message_handler.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>

namespace shyft::energy_market::stm::srv::experimental::application {

  struct server_handler : message_handler {
    void log(dlib::log_level, std::string_view) const {};
  };

  struct server_dispatch {
    model_manager& manager;

    server_handler operator()(std::string const & /*ip*/, unsigned short /*port*/) {
      return server_handler{manager};
    };
  };

  struct server : protocols::basic_server<application_protocol, server_dispatch> {
    server()
      : basic_server{{manager}} {
    }

    model_manager manager;
  };
}
