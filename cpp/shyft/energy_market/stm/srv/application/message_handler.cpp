#include <algorithm>
#include <functional>
#include <iterator>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <tuple>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/srv/application/message_handler.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/srv/model_info.h>

namespace shyft::energy_market::stm::srv::experimental::application {

  template <auto msg>
  requires(msg == read_models_msg)
  auto handle_common_model_requests(model_manager& manager, protocols::request<msg> const & req) {
    protocols::reply<msg> rep;
    manager.observe<to_model_tag<msg.tag>>([&](auto&& models) {
      std::ranges::move(
        req.model_ids | std::ranges::views::transform([&](auto&& id) {
          return models.find(id);
        }) | std::ranges::views::filter([&](auto&& it) {
          return it != models.end();
        }) | std::ranges::views::transform([](auto&& it) {
          return std::get<model_type<msg.tag>>(it->second);
        }),
        std::back_inserter(rep.models));
    });
    return rep;
  }

  template <auto msg>
  requires(msg == remove_model_msg)
  protocols::reply<msg> handle_common_model_requests(model_manager& manager, protocols::request<msg> const & req) {
    auto const was_erased = manager.mutate<to_model_tag<msg.tag>>([&](auto&& models) {
      return models.erase(req.model_id) == 1;
    });
    return {!was_erased};
  }

  template <auto msg>
  requires(msg == read_model_msg)
  auto handle_common_model_requests(model_manager& manager, protocols::request<msg> const & req) {
    protocols::reply<msg> rep{};
    manager.observe<to_model_tag<msg.tag>>(req.model_id, [&](auto&& /*info*/, auto&& model) {
      rep.model = model;
    });
    return rep;
  }

  template <auto msg>
  requires(msg == update_model_info_msg)
  protocols::reply<msg> handle_common_model_requests(model_manager& manager, protocols::request<msg> const & req) {
    manager.mutate<to_model_tag<msg.tag>>(req.model_info.id, [&](auto&& info, auto&& /*model*/) {
      info.name = req.model_info.name;
      info.json = req.model_info.json;
      info.created = req.model_info.created;
    });
    return {true};
  }

  template <auto msg>
  requires(msg == get_model_infos_msg)
  protocols::reply<msg> handle_common_model_requests(model_manager& manager, protocols::request<msg> const & req) {
    protocols::reply<msg> rep{};
    manager.observe<to_model_tag<msg.tag>>([&](auto&& models) {
      auto fulfilled_ids = req.model_ids;
      if (fulfilled_ids.empty()) {
        std::ranges::copy(std::views::keys(models), std::back_inserter(fulfilled_ids));
      }

      auto valid_ids = std::views::filter(fulfilled_ids, [&](auto&& mid) {
        auto const it = models.find(mid);
        if (it == models.end())
          return false;

        if (!req.period.has_value())
          return true;

        return req.period.value().contains(std::get<shyft::srv::model_info>(it->second).created);
      });

      std::ranges::transform(valid_ids, std::back_inserter(rep.model_infos), [&](auto mid) {
        return std::get<shyft::srv::model_info>(models.at(mid));
      });
    });
    return rep;
  }

#define SHYFT_LAMBDA(r, data, elem) \
  elem##_reply message_handler::operator()(elem##_request const & req) { \
    return handle_common_model_requests(manager, req); \
  }

  BOOST_PP_LIST_FOR_EACH(
    SHYFT_LAMBDA,
    _,
    BOOST_PP_TUPLE_TO_LIST(
      (read_task,
       read_layout,
       read_tasks,
       read_layouts,
       remove_task,
       remove_layout,
       update_task_info,
       update_layout_info,
       get_task_infos,
       get_layout_infos)))
#undef SHYFT_LAMBDA

  int find_next_key(auto&& keys) {
    auto out = 1;
    for (auto const key : keys) {
      if (key != 0 && key == out)
        out++;
      else
        break;
    }
    return out;
  }

  store_layout_reply message_handler::operator()(store_layout_request const & req) {
    auto mid = manager.mutate<model_tag::layout>([&](auto&& models) {
      int mid = req.model_info.has_value() ? req.model_info.value().id : 0;
      if (mid <= 0) {
        mid = find_next_key(std::views::keys(models));
      }

      auto mdl = req.model;
      mdl.id = mid;
      models.emplace(
        mid, std::make_tuple(req.model_info.value_or(shyft::srv::model_info{mid, mdl.name, {}, mdl.json}), mdl));
      return mid;
    });
    return {mid};
  }

  store_task_reply message_handler::operator()(store_task_request const & req) {
    auto const mid = manager.mutate<model_tag::task>([&](auto&& models) {
      int mid = req.model_info.has_value() ? req.model_info.value().id : 0;
      if (mid <= 0) {
        mid = find_next_key(std::views::keys(models));
      }

      auto mdl = req.model;
      mdl.id = mid;

      auto get_name_and_json = [](auto&& e) {
        return std::tie(e.name, e.json);
      };

      auto&& [name, json] = req.model_info.transform(get_name_and_json).value_or(get_name_and_json(mdl));
      models.emplace(mid, std::make_tuple(shyft::srv::model_info{mid, name, mdl.created, json}, mdl));

      return mid;
    });
    return {mid};
  }

  add_case_reply message_handler::operator()(add_case_request const & req) {
    return {manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      task.add_case(req.cse);
    })};
  }

  remove_case_by_id_reply message_handler::operator()(remove_case_by_id_request const & req) {
    remove_case_by_id_reply rep{false};
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      rep.result = task.remove_case(req.case_id);
    });
    return rep;
  }

  remove_case_by_name_reply message_handler::operator()(remove_case_by_name_request const & req) {
    remove_case_by_name_reply rep{false};
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      rep.result = task.remove_case(req.case_name);
    });
    return rep;
  }

  get_case_by_id_reply message_handler::operator()(get_case_by_id_request const & req) {
    get_case_by_id_reply rep;
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      rep.case_ = task.get_case(req.case_id);
    });
    return rep;
  }

  get_case_by_name_reply message_handler::operator()(get_case_by_name_request const & req) {
    get_case_by_name_reply rep;
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      rep.case_ = task.get_case(req.case_name);
    });
    return rep;
  }

  update_case_reply message_handler::operator()(update_case_request const & req) {
    manager.mutate<model_tag::task>(req.mode_id, [&](auto&& /*info*/, auto&& task) {
      return task.update_case(*req.case_);
    });

    return {};
  }

  add_task_ref_reply message_handler::operator()(add_task_ref_request const & req) {
    add_task_ref_reply rep{false};
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      if (auto csr = task.get_case(req.case_id)) {
        csr->add_model_ref(req.model_ref);
        rep.result = true;
      }
    });
    return rep;
  }

  remove_task_ref_reply message_handler::operator()(remove_task_ref_request const & req) {
    remove_task_ref_reply rep{false};
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      if (auto csr = task.get_case(req.case_id)) {
        rep.result = csr->remove_model_ref(req.model_key);
      }
    });
    return rep;
  }

  get_task_ref_reply message_handler::operator()(get_task_ref_request const & req) {
    get_task_ref_reply rep{};
    manager.mutate<model_tag::task>(req.model_id, [&](auto&& /*info*/, auto&& task) {
      if (auto cs = task.get_case(req.case_id))
        rep.ref = cs->get_model_ref(req.model_key);
    });
    return rep;
  }

  task_fx_reply message_handler::operator()(task_fx_request const & req) {
    return {manager.callback<model_tag::task>(req.model_id, req.args)};
  }

  read_layout_with_args_reply message_handler::operator()(read_layout_with_args_request const & req) {
    read_layout_with_args_reply rep{};
    auto const was_found = manager.observe<model_tag::layout>(req.model_id, [&](auto&& /*info*/, auto&& layout) {
      rep.layout = layout;
    });

    if (was_found)
      return rep;

    std::string const layout = manager.callback<model_tag::layout>(req.layout_name, req.args);
    if (layout.empty()) {
      throw std::runtime_error(
        "message_handler<read_layout_with_args>: Did not find layout, and unable to generate new");
    }

    return {
      model_type<model_tag::layout>{-1, req.layout_name, layout}
    };
  }
}
