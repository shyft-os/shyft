#pragma once

#include <cstdint>
#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <tuple>

#include <boost/mp11/utility.hpp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/reflection.h>
#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/srv/model_info.h>

namespace shyft::energy_market::stm::srv::experimental::application {

  namespace detail {
    template <model_tag T>
    struct model_callback_impl;

    template <>
    struct model_callback_impl<model_tag::layout> {
      using type = std::function<std::string(std::string const &, std::string const &)>;
    };

    template <>
    struct model_callback_impl<model_tag::task> {
      using type = std::function<bool(std::int64_t, std::string)>;
    };
  }

  template <model_tag T>
  using model_callback = typename detail::model_callback_impl<T>::type;

  struct model_manager {
    template <model_tag M>
    struct model_container {
      std::map<int, std::tuple<shyft::srv::model_info, model_type<M>>> models;
      model_callback<M> callback;
      mutable std::mutex mx;
    };

    using containers_type = boost::mp11::mp_rename<tagged_types_t<model_tag, model_container>, std::tuple>;

    template <model_tag T>
    auto &callback() {
      return container<T>().callback;
    }

    template <model_tag T>
    auto const &callback() const {
      return container<T>().callback;
    }

    template <model_tag T>
    auto callback(auto &&...args) const {
      auto const &cb = callback<T>();
      return cb ? cb(SHYFT_FWD(args)...) : typename model_callback<T>::result_type{};
    }

    template <model_tag T>
    decltype(auto) mutate(auto &&f) {
      auto &[models, _, mx] = container<T>();
      std::scoped_lock lckd{mx};
      return std::invoke(SHYFT_FWD(f), models);
    }

    template <model_tag T>
    bool mutate(int key, auto &&f) {
      return mutate<T>([&](auto &models) {
        auto it = models.find(key);
        if (it != models.end()) {
          std::apply(SHYFT_FWD(f), it->second);
          return true;
        }
        return false;
      });
    }

    template <model_tag T>
    decltype(auto) observe(auto &&f) const {
      auto &[models, _, mx] = container<T>();
      std::scoped_lock lckd{mx};
      return std::invoke(SHYFT_FWD(f), models);
    }

    template <model_tag T>
    bool observe(int key, auto &&f) const {
      return observe<T>([&](auto const &models) {
        auto it = models.find(key);
        if (it != models.end()) {
          std::apply(SHYFT_FWD(f), it->second);
          return true;
        }
        return false;
      });
    }

   private:
    template <model_tag T>
    auto const &container() const noexcept {
      return std::get<model_container<T>>(containers);
    }

    template <model_tag T>
    auto &container() noexcept {
      return std::get<model_container<T>>(containers);
    }

    containers_type containers{};
  };

  struct message_handler {
    application::model_manager &manager;

#define SHYFT_LAMBDA(r, data, elem) elem##_reply operator()(elem##_request const &request);
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_APPLICATION_PROTOCOL))
#undef SHYFT_LAMBDA
  };
}
