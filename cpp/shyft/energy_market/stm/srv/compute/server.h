#pragma once

#include <atomic>
#include <concepts>
#include <cstdint>
#include <exception>
#include <iostream>
#include <memory>
#include <ranges>
#include <shared_mutex>
#include <stdexcept>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <dlib/uintn.h>

#include <shyft/core/protocol.h>
#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::compute {

  void reset_model_output(std::string_view, stm_system &);

  struct server_config : shyft::srv::fast_server_iostream_config {
    bool clean_run_files;
    std::string persistent_storage_path;

    SHYFT_DEFINE_STRUCT(
      server_config,
      (shyft::srv::fast_server_iostream_config),
      (clean_run_files, persistent_storage_path));
  };

  struct server_worker {
    std::string model_id;
    std::shared_ptr<stm_system> model;
    std::vector<log_entry> log;
    std::exception_ptr exception;
    std::jthread thread;
  };

  struct server_state {
    std::shared_mutex mutex{};
    compute::state state{compute::state::idle};
    server_worker worker{};
  };

  struct server_handler {
    std::shared_ptr<server_state> server;
    std::string ip;
    unsigned short port;
    server_config config;

    void log(dlib::log_level, std::string_view) const;

    template <auto msg>
    static auto expect_server_state(
      server_state &server,
      protocols::request<msg> const &,
      std::same_as<state> auto... expected_states) {
      if (state actual_state = server.state; ((actual_state != expected_states) && ...))
        throw std::runtime_error(fmt::format(
          "Unexpected state for {} request, expected one of '{}', got '{}'.",
          msg.tag,
          std::array{expected_states...},
          actual_state));
    }

    static auto handle_worker_error(server_state &server) {
      if (server.worker.exception)
        std::rethrow_exception(std::exchange(server.worker.exception, nullptr));
    }

    static auto expect_model(std::string_view model_id, std::shared_ptr<stm_system> const &model) {
      if (!model)
        throw std::runtime_error(fmt::format("Invalid model '{}'.", model_id));
    }

    start_reply operator()(start_request &&request) {
      expect_model(request.model_id, request.model);
      stm::unbind_ts(*request.model);
      std::unique_lock lock{server->mutex};
      expect_server_state(*server, request, state::idle);
      server->state = state::started;
      server->worker = server_worker{
        .model_id = std::move(request.model_id),
        .model = std::move(request.model),
        .log = {},
        .exception = {},
        .thread = {}};
      return {};
    }

    get_status_reply operator()(get_status_request const &request) {
      std::shared_lock lock{server->mutex};
      get_status_reply r{.state = server->state, .log{}};
      if (request.log_index)
        std::ranges::copy(
          std::views::drop(server->worker.log, std::min(server->worker.log.size(), *request.log_index)),
          std::back_inserter(r.log));
      return r;
    }

    get_attrs_reply operator()(get_attrs_request const &request) {
      std::shared_lock lock{server->mutex};
      expect_server_state(*server, request, state::started, state::done);
      handle_worker_error(*server);
      return {.attrs = stm::url_get_attrs(*server->worker.model, request.urls)};
    }

    set_attrs_reply operator()(set_attrs_request const &request) {
      std::unique_lock lock(server->mutex);
      expect_server_state(*server, request, state::started, state::done);
      handle_worker_error(*server);
      return {.attrs = stm::url_set_attrs(*server->worker.model, request.attrs)};
    }

    stop_reply operator()(stop_request const &request) {
      std::unique_lock lock{server->mutex};
      expect_server_state(*server, request, state::idle, state::started, state::done);
      server->state = state::idle;
      server->worker = server_worker{};
      return {};
    }

    plan_reply operator()(plan_request &&);

    get_plan_reply operator()(get_plan_request const &request) {
      std::shared_lock lock{server->mutex};
      expect_server_state(*server, request, state::done);
      handle_worker_error(*server);
      return {.summary = server->worker.model->summary};
    }
  };

  struct server_dispatch {
    server_config config;
    std::shared_ptr<server_state> state = std::make_shared<server_state>();

    server_handler operator()(std::string const &ip, unsigned short port) & {
      return {state, ip, port, config};
    }
  };

  using server = protocols::basic_server<compute_protocol, server_dispatch>;

}
