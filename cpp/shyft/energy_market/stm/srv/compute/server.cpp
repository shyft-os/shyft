#include <algorithm>
#include <chrono>
#include <concepts>
#include <csignal>
#include <exception>
#include <filesystem>
#include <fstream>
#include <future>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <thread>
#include <utility>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/outcome/result.hpp>
#include <boost/serialization/optional.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/config.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/core/optional.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/energy_market/stm/shop/shop_problem.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <experimental/scope>

#if defined(SHYFT_WITH_SHOP)
#include <shyft/energy_market/stm/shop/shop_system.h>
#endif

namespace shyft::energy_market::stm::srv::compute {

  namespace {
    dlib::logger logger{"compute"};
  }

  void server_handler::log(dlib::log_level l, std::string_view m) const {
    logger << l << "[dstm-compute]:" << fmt::format("from client {}:{} - {}", ip, port, m);
  }

  auto make_watchdog_task(server_handler handler, auto watchdog_timeout, auto worker_task) {
    return [handler = std::move(handler),
            watchdog_timeout = std::move(watchdog_timeout),
            worker_task = std::move(worker_task)]() mutable {
      std::promise<void> worker_promise{};
      auto worker_future = worker_promise.get_future();
      std::jthread([&] {
        try {
          worker_task();
        } catch (...) {
          {
            std::unique_lock lock{handler.server->mutex};
            handler.server->worker.exception = std::current_exception();
          }
        }
        worker_promise.set_value();
      }).detach();
      if (worker_future.wait_for(watchdog_timeout) == std::future_status::timeout) {
        handler.log(dlib::LERROR, "Timeout reached, terminating.");
        std::terminate();
      }
      {
        std::unique_lock lock{handler.server->mutex};
        handler.server->state = state::done;
      }
    };
  }

  inline auto expect_model_input(server_handler &handler, std::string_view model_id, stm_system &model) {
    std::vector<std::string> unbound_attrs;
    url_with_planning_inputs(model_id, model, [&]<typename Attr>(ignore_t, Attr const &attr, std::string_view url) {
      if constexpr (std::is_same_v<Attr, time_series::dd::apoint_ts>)
        if (attr.needs_bind())
          unbound_attrs.push_back(std::string(url));
    });
    if (!unbound_attrs.empty())
      handler.log(dlib::LWARN, fmt::format("Unbound model '{}': {}.", model_id, unbound_attrs));
  }

  void reset_model_output(std::string_view model_id, stm_system &model) {
    url_with_planning_outputs(
      model_id, model, [&]<typename Attr>(ignore_t, Attr &attr, [[maybe_unused]] std::string_view url) {
        if constexpr (std::is_same_v<Attr, time_series::dd::apoint_ts>)
          attr = time_series::dd::apoint_ts{};
      });
  }

  void set_missing_model_output(std::string_view model_id, stm_system &model) {
    url_with_planning_outputs(
      model_id, model, [&]<typename Attr>(ignore_t, Attr &attr, [[maybe_unused]] std::string_view url) {
        if constexpr (std::is_same_v<Attr, apoint_ts>) {
          if (!attr.ts)
            attr = apoint_ts{model.run_params.run_time_axis, nan, POINT_AVERAGE_VALUE};
        }
      });
  }

  void write_to_disk(shop::problem const &problem, std::filesystem::path fpath) {
    std::ofstream o(fpath, std::ios::binary | std::ios::trunc);
    core::core_oarchive a(o);
    a << problem;
  }

  struct plan_task {

    server_state &server;
    server_config const &config;
    time_axis::generic_dt time_axis;
    std::vector<shop::shop_command> commands;

    void operator()() {

      auto const persisted_path = [&] {
        if (config.persistent_storage_path.empty()) {
          return none<std::filesystem::path>;
        }

        auto const fpath = std::filesystem::path{fmt::format(
          "{}_{}.bin", config.persistent_storage_path, std::chrono::system_clock::now().time_since_epoch().count())};
        write_to_disk(shop::problem{time_axis, commands, server.worker.model}, fpath);
        return just(fpath);
      }();

      auto const tmp_dir = [&] {
        if (!config.clean_run_files) {
          return none<std::filesystem::path>;
        }

        auto const tmp_dir = std::filesystem::current_path() / boost::filesystem::unique_path().c_str();
        std::filesystem::create_directory(tmp_dir);
        std::filesystem::current_path(tmp_dir);
        return just(tmp_dir);
      }();

      std::experimental::scope_exit cleanup{[&] {
        if (persisted_path)
          std::filesystem::remove(*persisted_path);

        if (tmp_dir) {
          std::filesystem::current_path(tmp_dir->parent_path());
          std::filesystem::remove_all(*tmp_dir);
        }
      }};

#if !defined(SHYFT_WITH_SHOP)
      throw std::runtime_error("plan requests are not supported by this server");
#else
      shop::shop_system shop_model{time_axis};
      auto collect_shop_logs = [&] {
        // NOTE: assume shop get_log_buffer call is thread safe - jeh
        auto shop_log = shop_model.get_log_buffer();
        if (shop_log.empty())
          return;
        std::unique_lock lock{server.mutex};
        auto &stm_log = server.worker.log;
        std::ranges::copy(shop_log, std::back_inserter(stm_log));
      };

      auto &stm_model = *server.worker.model;

      {
        auto wants_single_operating_zone = [](auto const &command) {
          return command.keyword == "print" && command.specifier == "bp_curves" && command.has_nohikkemikk();
        };
        shop_model.adapter.enforce_single_operating_zone = std::ranges::any_of(commands, wants_single_operating_zone);
      }
      shop_model.emit(stm_model);

      collect_shop_logs();
      for (auto &command : commands) {
        command.remove_nohikkemikk();
        shop_model.commander.execute(command);
        collect_shop_logs();
      }
      shop_model.collect(stm_model);
      shop_model.complete(stm_model);
      collect_shop_logs();
      set_missing_model_output(server.worker.model_id, *server.worker.model);
#endif
    }
  };

  plan_reply server_handler::operator()(plan_request &&request) {
    plan_reply reply;
    {
      std::unique_lock lock{server->mutex};
      expect_server_state(*server, request, state::started, state::done);
      server->state = state::running;
      server->worker.log.clear();
    }
    expect_model_input(*this, server->worker.model_id, *server->worker.model);
    reset_model_output(server->worker.model_id, *server->worker.model);
    auto const timeout = calc_suggested_timelimit(request.commands);
    server->worker.thread = std::jthread(make_watchdog_task(
      *this,
      timeout,
      plan_task{
        .server = *server,
        .config = config,
        .time_axis = std::move(request.time_axis),
        .commands = std::move(request.commands)}));
    return reply;
  }
}
