#pragma once

#include <string_view>
#include <vector>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/protocol.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::srv::compute {


  struct client : protocols::basic_client<> {

#define SHYFT_LAMBDA(r, data, elem) \
  elem##_reply elem(elem##_request request) { \
    return send(std::move(request)); \
  }
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA
  };

  // FIXME: drop redundant model-ids - jeh
  get_attrs_request get_plan_attrs_request(std::string_view, stm_system const &, generic_dt const &time_axis);
  shyft::core::utctime send_input_and_plan(
    dtss::server_state &,
    std::string_view,
    stm_system &,
    client &,
    generic_dt const &,
    std::vector<shop::shop_command> const &);

  struct plan_result {
    std::shared_ptr<optimization_summary> summary;
    std::vector<std::string> attr_urls;
    std::vector<std::optional<stm::any_attr>> attrs;
  };

  plan_result get_plan_result(std::string_view, stm_system const &, client &, generic_dt const &);

  void assign_plan_result(
    std::string_view,
    stm_system &,
    shyft::core::subscription::manager *,
    shyft::core::subscription::manager &,
    plan_result);
}
