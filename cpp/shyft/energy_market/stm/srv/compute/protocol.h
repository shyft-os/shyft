#pragma once

#include <cstdint>
#include <iostream>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <fmt/chrono.h>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/boost_serialization_std_variant.h>
#include <shyft/core/fmt_std_opt.h>
#include <shyft/core/protocol.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

namespace shyft {
  namespace energy_market::stm::srv::compute {

#define SHYFT_STM_COMPUTE_STATE (idle, started, running, done)

    SHYFT_DEFINE_ENUM(state, std::uint8_t, SHYFT_STM_COMPUTE_STATE)

#define SHYFT_STM_COMPUTE_PROTOCOL (start, get_status, get_attrs, set_attrs, stop, plan, get_plan)

    SHYFT_DEFINE_ENUM(message_tag, std::uint8_t, SHYFT_STM_COMPUTE_PROTOCOL);

    inline constexpr auto compute_protocol = protocols::basic_protocol<message_tag>{};

    template <message_tag msg>
    inline constexpr auto compute_message = protocols::message<compute_protocol>{.tag = msg};

#define SHYFT_LAMBDA(r, data, elem) inline constexpr auto elem##_msg = compute_message<message_tag::elem>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA
  }

  namespace protocols {
    template <>
    struct request<energy_market::stm::srv::compute::get_status_msg> {

      std::optional<std::size_t> log_index;

      SHYFT_DEFINE_STRUCT(request, (), (log_index));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::compute::start_msg> {

      std::string model_id;
      std::shared_ptr<energy_market::stm::stm_system> model;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, model));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::compute::plan_msg> {

      time_axis::generic_dt time_axis;
      std::vector<energy_market::stm::shop::shop_command> commands;

      SHYFT_DEFINE_STRUCT(request, (), (time_axis, commands));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::compute::get_attrs_msg> {

      std::vector<std::string> urls;

      SHYFT_DEFINE_STRUCT(request, (), (urls));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::compute::set_attrs_msg> {

      std::vector<std::pair<std::string, energy_market::stm::any_attr>> attrs;

      SHYFT_DEFINE_STRUCT(request, (), (attrs));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::compute::get_status_msg> {

      energy_market::stm::srv::compute::state state;
      std::vector<energy_market::stm::log_entry> log;

      SHYFT_DEFINE_STRUCT(reply, (), (state, log));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::compute::get_attrs_msg> {

      std::vector<std::optional<energy_market::stm::any_attr>> attrs;

      SHYFT_DEFINE_STRUCT(reply, (), (attrs));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::compute::set_attrs_msg> {

      std::vector<bool> attrs;

      SHYFT_DEFINE_STRUCT(reply, (), (attrs));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::compute::get_plan_msg> {

      std::shared_ptr<energy_market::stm::optimization_summary>
        summary; // NOTE: use shared_ptr to get a regular type - jeh

      SHYFT_DEFINE_STRUCT(reply, (), (summary));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct protocol_archives<shyft::energy_market::stm::srv::compute::start_msg> {
      static auto make_oarchive(std::ostream &stream) {
        return core::core_oarchive_stripped{stream, core::core_arch_flags};
      }

      static auto make_iarchive(std::istream &stream) {
        return shyft::core::core_iarchive_stripped(stream, core::core_arch_flags);
      };
    };
  }

  namespace energy_market::stm::srv::compute {

#define SHYFT_LAMBDA(r, data, elem) using elem##_request = protocols::request<elem##_msg>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA
#define SHYFT_LAMBDA(r, data, elem) using elem##_reply = protocols::reply<elem##_msg>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA

    template <message_tag t>
    using compute_request = protocols::request<compute_message<t>>;
    template <message_tag t>
    using compute_reply = protocols::reply<compute_message<t>>;

    using any_request = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, compute_request>>;
    using any_reply = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, compute_reply>>;

  }

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::compute::state);
SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::compute::message_tag);
