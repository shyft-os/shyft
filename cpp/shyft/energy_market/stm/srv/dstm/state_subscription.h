#pragma once
#include <functional>
#include <memory>
#include <string>

#include <shyft/core/subscription.h>
#include <shyft/web_api/bg_work_result.h>

namespace shyft::energy_market::stm::subscription {

  using core::subscription::observer_base;
  using web_api::bg_work_result;

  /**
   * @brief state observer class for stm_system context
   * @details
   * The purpose of this class is to establish publish//subscribe observer,
   * so that we can support web-api with subscription for the model.state (idle,running etc).
   * It specializes generic observer to fit the minimal purpose.
   * The class life-time is usually controlled by the web-api socket(boost beast) lifetime,
   * and the ref to the server is likewise assumed to outlive the lifetime of the subscription.
   */
  struct state_observer : observer_base {

    ///< signature for the log emit handler (decouple formatting from logic of when)
    using response_cb_t = std::function<bg_work_result(std::string const &, std::string const &)>;

    std::string model_key;     ///< the model id we are working on, ref. shared_models(map string->model ctx)
    response_cb_t response_cb; ///< the function that generates the web_request

    state_observer(
      std::shared_ptr<core::subscription::manager> sm,
      std::string const &model_key,
      std::string const &request_id,
      response_cb_t cb)
      : observer_base(std::move(sm), request_id)
      , model_key{model_key}
      , response_cb{std::move(cb)} {
    }

    bool recalculate() override {
      auto const updated = has_changed();
      published_version = terminal_version();
      return updated;
    }

    bg_work_result re_emit_response() {
      return response_cb(model_key, request_id);
    }
  };

}
