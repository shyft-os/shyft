#pragma once

#include <algorithm>
#include <ranges>
#include <string>
#include <utility>
#include <vector>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>

#include <shyft/core/protocol.h>
#include <shyft/energy_market/stm/srv/dstm/protocol.h>

namespace shyft::energy_market::stm::srv::dstm {

  struct client : protocols::basic_client<protocols::io_with_retry> {

#define SHYFT_LAMBDA(r, data, elem) \
  elem##_reply elem(elem##_request request = {}) { \
    return send(std::move(request)); \
  }
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA

    auto create_model(std::string const &model_id) {
      return add_model({.model_id = model_id, .model = nullptr}).status;
    }

    auto rename_model(std::string const &old_model_id, std::string const &new_model_id) {
      return clone_model({old_model_id, new_model_id}).status && remove_model({old_model_id}).status;
    }

    auto get_model_ids() {
      std::vector<std::string> model_ids;
      std::ranges::copy(std::views::keys(get_model_infos().infos), std::back_inserter(model_ids));
      return model_ids;
    }
  };

}
