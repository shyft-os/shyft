#pragma once
#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

#include <shyft/core/protocol.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/reflection/serialization.h>
#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

namespace shyft {
  namespace energy_market::stm::srv {
#define SHYFT_STM_PROTOCOL \
  (get_version_info, \
   add_model, \
   set_model, \
   remove_model, \
   clone_model, \
   reset_model, \
   patch_model, \
   get_model_infos, \
   get_model, \
   get_state, \
   get_log, \
   evaluate_model, \
   evaluate_ts, \
   get_attrs, \
   set_attrs, \
   get_ts, \
   set_ts, \
   add_compute_server, \
   compute_server_status, \
   start_tune, \
   optimize, \
   tune, \
   stop_tune, \
   fx, \
   cache_flush, \
   cache_stats, \
   get_model_stripped)

    SHYFT_DEFINE_ENUM(message_tag, std::uint8_t, SHYFT_STM_PROTOCOL)

    inline constexpr auto dstm_protocol = protocols::basic_protocol<message_tag>{};

    template <message_tag msg>
    inline constexpr auto dstm_message = protocols::message<dstm_protocol>{.tag = msg};

#define SHYFT_LAMBDA(r, data, elem) inline constexpr auto elem##_msg = dstm_message<message_tag::elem>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA
  }

  namespace protocols {
    template <auto msg>
    requires(msg == any_of_weak(energy_market::stm::srv::add_model_msg, energy_market::stm::srv::set_model_msg))
    struct request<msg> {
      std::string model_id;
      std::shared_ptr<energy_market::stm::stm_system> model;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, model));

      auto operator<=>(request const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of_weak(
        energy_market::stm::srv::remove_model_msg,
        energy_market::stm::srv::reset_model_msg,
        energy_market::stm::srv::get_model_msg,
        energy_market::stm::srv::get_log_msg,
        energy_market::stm::srv::get_state_msg,
        energy_market::stm::srv::start_tune_msg,
        energy_market::stm::srv::stop_tune_msg,
        energy_market::stm::srv::get_model_stripped_msg))
    struct request<msg> {

      std::string model_id;

      SHYFT_DEFINE_STRUCT(request, (), (model_id));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::clone_model_msg> {

      std::string old_model_id, new_model_id;
      bool sparse = false;

      SHYFT_DEFINE_STRUCT(request, (), (old_model_id, new_model_id, sparse));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::patch_model_msg> {

      std::string model_id;
      energy_market::stm::stm_patch_op op;
      std::shared_ptr<energy_market::stm::stm_system> model;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, op, model));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::evaluate_model_msg> {

      std::string model_id;
      core::utcperiod bind_period;
      bool use_ts_cached_read = true, update_ts_cache = true;
      core::utcperiod clip_period = {};

      SHYFT_DEFINE_STRUCT(request, (), (model_id, bind_period, use_ts_cached_read, update_ts_cache, clip_period));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::evaluate_ts_msg> {

      time_series::dd::ats_vector time_series;
      core::utcperiod bind_period;
      bool use_ts_cached_read = true, update_ts_cache = true;
      core::utcperiod clip_period{};

      SHYFT_DEFINE_STRUCT(request, (), (time_series, bind_period, use_ts_cached_read, update_ts_cache, clip_period));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::get_attrs_msg> {

      std::vector<std::string> attrs;

      SHYFT_DEFINE_STRUCT(request, (), (attrs));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::set_attrs_msg> {

      std::vector<std::pair<std::string, energy_market::stm::any_attr>> attrs;

      SHYFT_DEFINE_STRUCT(request, (), (attrs));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::get_ts_msg> {

      std::string model_id;
      std::vector<std::string> urls;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, urls));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::set_ts_msg> {

      std::string model_id;
      time_series::dd::ats_vector time_series;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, time_series));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::add_compute_server_msg> {

      std::string address;

      SHYFT_DEFINE_STRUCT(request, (), (address));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::optimize_msg> {

      std::string model_id;
      time_axis::generic_dt time_axis;
      std::vector<energy_market::stm::shop::shop_command> commands;
      bool optimize_only = false;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, time_axis, commands, optimize_only));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::tune_msg> {

      std::string model_id;
      time_axis::generic_dt time_axis;
      std::vector<energy_market::stm::shop::shop_command> commands;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, time_axis, commands));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct request<energy_market::stm::srv::fx_msg> {

      std::string model_id, fx_arg;

      SHYFT_DEFINE_STRUCT(request, (), (model_id, fx_arg));

      auto operator<=>(request const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::get_version_info_msg> {

      std::string version;

      SHYFT_DEFINE_STRUCT(reply, (), (version));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(
      msg
      == any_of_weak(
        energy_market::stm::srv::add_model_msg,
        energy_market::stm::srv::set_model_msg,
        energy_market::stm::srv::clone_model_msg,
        energy_market::stm::srv::reset_model_msg,
        energy_market::stm::srv::remove_model_msg,
        energy_market::stm::srv::patch_model_msg,
        energy_market::stm::srv::evaluate_model_msg,
        energy_market::stm::srv::add_compute_server_msg,
        energy_market::stm::srv::optimize_msg,
        energy_market::stm::srv::start_tune_msg,
        energy_market::stm::srv::tune_msg,
        energy_market::stm::srv::stop_tune_msg,
        energy_market::stm::srv::fx_msg))
    struct reply<msg> {

      bool status;

      SHYFT_DEFINE_STRUCT(reply, (), (status));

      auto operator<=>(reply const &l) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::get_model_infos_msg> {

      std::map<std::string, shyft::srv::model_info, std::less<>> infos;

      SHYFT_DEFINE_STRUCT(reply, (), (infos));

      auto operator<=>(reply const &) const = default;
    };

    template <auto msg>
    requires(
      msg == any_of_weak(energy_market::stm::srv::get_model_msg, energy_market::stm::srv::get_model_stripped_msg))
    struct reply<msg> {

      std::shared_ptr<energy_market::stm::stm_system> model;

      SHYFT_DEFINE_STRUCT(reply, (), (model));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::get_state_msg> {

      energy_market::stm::model_state state;

      SHYFT_DEFINE_STRUCT(reply, (), (state));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::get_log_msg> {

      std::vector<energy_market::stm::log_entry> log;

      SHYFT_DEFINE_STRUCT(reply, (), (log));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::evaluate_ts_msg> {

      std::vector<energy_market::stm::evaluate_ts_result> time_series;

      SHYFT_DEFINE_STRUCT(reply, (), (time_series));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::get_attrs_msg> {

      std::vector<std::variant<energy_market::stm::any_attr, energy_market::stm::url_resolve_error>> attrs;

      SHYFT_DEFINE_STRUCT(reply, (), (attrs));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::set_attrs_msg> {

      std::vector<std::optional<energy_market::stm::url_resolve_error>> attrs;

      SHYFT_DEFINE_STRUCT(reply, (), (attrs));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::get_ts_msg> {

      time_series::dd::ats_vector time_series;

      SHYFT_DEFINE_STRUCT(reply, (), (time_series));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::compute_server_status_msg> {

      std::vector<energy_market::stm::srv::compute::managed_server_status> status;

      SHYFT_DEFINE_STRUCT(reply, (), (status));

      auto operator<=>(reply const &) const = default;
    };

    template <>
    struct reply<energy_market::stm::srv::cache_stats_msg> {
      dtss::cache_stats cache_stats;
      SHYFT_DEFINE_STRUCT(reply, (), (cache_stats));
      auto operator<=>(reply const &) const = default;
    };
  }

  namespace energy_market::stm::srv {
#define SHYFT_LAMBDA(r, data, elem) using elem##_request = protocols::request<elem##_msg>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA

#define SHYFT_LAMBDA(r, data, elem) using elem##_reply = protocols::reply<elem##_msg>;
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA

    template <message_tag m>
    using dstm_request = protocols::request<dstm_message<m>>;
    template <message_tag m>
    using dstm_reply = protocols::reply<dstm_message<m>>;

    using any_request = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, dstm_request>>;
    using any_reply = boost::mp11::mp_apply<std::variant, tagged_types_t<message_tag, dstm_reply>>;
  }
}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::srv::message_tag);
