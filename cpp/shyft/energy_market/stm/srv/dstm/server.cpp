#include <algorithm>
#include <functional>
#include <iterator>
#include <map>
#include <memory>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include <boost/hof/unpack.hpp>
#include <boost/serialization/utility.hpp>
#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/version.h>

namespace shyft::energy_market::stm::srv::dstm {
  namespace {
    dlib::logger logger{"dstm"};
  }

  server_state::~server_state() {
    compute_janitor.stop();
    execution_context.join();
  }

  server_state::server_state(compute::janitor_config const & j_config)
    : execution_context{}
    , models{make_shared_models(execution_context.executor())}
    , dtss{std::make_unique<dtss::server_state>()}
    , compute_manager{}
    , compute_janitor{compute::make_janitor(execution_context.executor(), j_config)} {
    compute_janitor.start(compute_manager);
  }

  void server_log(dlib::log_level l, std::string_view m) {
    logger << l << m;
  }

  void server_handler::log(dlib::log_level l, std::string_view m) const {
    server_log(l, fmt::format("[dstm-compute]: from client {}:{} - {}", ip, port, m));
  }

  get_version_info_reply handle_request(auto& handler, get_version_info_request&&) {
    return {fmt::format("{}.{}.{}", shyft::_version.major, shyft::_version.minor, shyft::_version.patch)};
  }

  add_model_reply handle_request(auto& handler, add_model_request&& request) {
    handler.log(dlib::LTRACE, fmt::format("add_model: {}", request.model_id));
    if (!request.model)
      return {handler.state.models.add(request.model_id).get()};
    if (causes_cycle(handler.state.models, request.model_id, request.model))
      throw std::runtime_error(fmt::format("add_model would cause cycle: {}", request.model_id));
    return {handler.state.models.add(request.model_id, request.model).get()};
  }

  set_model_reply handle_request(auto& handler, set_model_request&& request) {
    handler.log(dlib::LTRACE, fmt::format("set_model: {}", request.model_id));
    if (!request.model)
      return {handler.state.models.set(request.model_id, request.model).get()};
    if (causes_cycle(handler.state.models, request.model_id, request.model))
      throw std::runtime_error(fmt::format("set_model would cause cycle: {}", request.model_id));
    return {handler.state.models.set(request.model_id, request.model).get()};
  }

  remove_model_reply handle_request(auto& handler, remove_model_request&& request) {
    return {handler.state.models.remove(request.model_id).get()};
  }

  clone_model_reply handle_request(auto& handler, clone_model_request&& request) {
    return {handler.state.models.copy(request.old_model_id, request.new_model_id, request.sparse).get()};
  }

  reset_model_reply handle_request(auto& handler, reset_model_request&& request) {
    auto result =
      handler.state.models
        .mutate_or(
          request.model_id,
          [&](auto&& view) {
            unbind_ts(*view.model);
            return true;
          },
          +[] {
            return false;
          })
        .get();
    return {result};
  }

  patch_model_reply handle_request(auto& handler, patch_model_request&& request) {
    if (!request.model)
      return {false};
    if (request.op == stm_patch_op::add)
      if (causes_cycle(handler.state.models, request.model_id, request.model))
        throw std::runtime_error(fmt::format("patch causes cycle: {}", request.model_id));
    return {handler.state.models
              .mutate_or_throw(
                request.model_id,
                [&](auto&& view) {
                  return patch(view.model, request.op, *request.model);
                })
              .get()};
  }

  get_model_infos_reply handle_request(auto& handler, [[maybe_unused]] get_model_infos_request&& reqeuest) {
    using srv_model_info = shyft::srv::model_info;
    auto const make_info = boost::hof::unpack([&](auto const & key, auto const & shared_model) {
      auto model = shared_model->model.load();
      auto info = shared_model->info.load();
      return std::make_pair(key, srv_model_info(model->id, model->name, info->created, model->json));
    });
    auto const copy_infos = [&](auto const & models) {
      // FIXME: insert_range when __cpp_lib_containers_ranges is supported - jeh
      std::map<std::string, srv_model_info, std::less<>> infos;
      std::ranges::copy(std::views::transform(models, make_info), std::inserter(infos, infos.end()));
      return infos;
    };
    return {handler.state.models.observe(copy_infos).get()};
  }

  get_model_reply handle_request(auto& handler, get_model_request&& request) {
    auto&& [model_id] = request;
    auto model = handler.state.models.get(request.model_id).get();
    if (!model)
      throw std::runtime_error(fmt::format("invalid model_id: {}", model_id));
    return {model};
  }

  get_model_stripped_reply handle_request(auto& handler, get_model_stripped_request&& request) {
    auto&& [model_id] = request;
    auto model = handler.state.models.get(request.model_id).get();
    if (!model)
      throw std::runtime_error(fmt::format("invalid model_id: {}", model_id));
    auto model_stripped = stm_system::from_blob_strip(stm_system::to_blob_strip(model));
    return {model_stripped};
  }

  get_state_reply handle_request(auto& handler, get_state_request&& request) {
    return {handler.state.models
              .observe_or_throw(
                request.model_id,
                [](auto&& view) {
                  return view.info->state;
                })
              .get()};
  }

  get_log_reply handle_request(auto& handler, get_log_request&& request) {
    return {handler.state.models
              .observe_or_throw(
                request.model_id,
                [](auto&& view) {
                  return view.info->log;
                })
              .get()};
  }

  evaluate_model_reply handle_request(auto& handler, evaluate_model_request&& request) {

    auto const& [model_id, bind_period, use_ts_cached_read, update_ts_cache, clip_period] = request;

    auto model = handler.state.models.get(model_id).get();
    if (!model)
      return {false};

    std::vector<time_series::dd::apoint_ts*> ts_attrs;
    std::vector<time_series::dd::aref_ts*> ts_refs;
    {
      auto collect_attr = [&]<typename T>(T& attr) -> void {
        if constexpr (std::is_same_v<time_series::dd::apoint_ts, T>) {
          if (!attr.ts || !attr.needs_bind())
            return;
          ts_attrs.push_back(&attr);
          stm::with_reference_ts(
            std::string_view{model_id},
            *model,
            attr.ts,
            [&](aref_ts& ts) {
              ts_refs.push_back(&ts);
            },
            ignore);
        }
      };
      url_with_all(model_id, *model, [&]<typename T>(ignore_t, T& attr, ignore_t) {
        collect_attr(attr);
      });
      url_with_custom(model_id, *model, [&]<typename T>(ignore_t, T& attr, ignore_t) {
        collect_attr(attr);
      });
    }
    if (ts_attrs.empty())
      return {false};

    auto dstm_ts_refs = std::ranges::stable_partition(ts_refs, [&](auto ts_ref) {
      return stm::url_peek_model_id(ts_ref->id).empty();
    });
    auto dtss_ts_refs = std::ranges::subrange(std::ranges::begin(ts_refs), std::ranges::begin(dstm_ts_refs));
    { // NOTE: bind dtss references
      std::vector<std::string> dtss_ts_ref_ids(std::ranges::size(dtss_ts_refs));
      std::ranges::copy(
        std::views::transform(
          dtss_ts_refs,
          [](auto ts_ref) {
            return ts_ref->id;
          }),
        dtss_ts_ref_ids.data());
      auto&& [dtss_ts_reps, dtss_ts_period, dtss_ts_errs] = dtss::try_read(
        *handler.state.dtss, dtss_ts_ref_ids, bind_period, use_ts_cached_read, update_ts_cache);
      if (!dtss_ts_errs.empty()) {
        std::string err_msg = "dtss read error:";
        std::ranges::copy(
          std::views::join(
            std::views::transform(
              dtss_ts_errs,
              [&](auto const & dtss_ts_err) {
                return fmt::format(" ({},{}),", dtss_ts_ref_ids[dtss_ts_err.index], dtss_ts_err.code);
              })),
          std::back_inserter(err_msg));
        err_msg.pop_back();
        throw std::runtime_error(err_msg);
      }
      for (auto&& [ts_ref, ts] : std::views::zip(dtss_ts_refs, dtss_ts_reps))
        ts_ref->rep = std::move(ts);
    }
    { // NOTE: bind dstm references
      time_series::dd::ats_vector dstm_ts_reqs(std::ranges::size(dstm_ts_refs));
      std::ranges::copy(
        std::views::transform(
          dstm_ts_refs,
          [&](auto ts_ref) {
            return apoint_ts(ts_ref->id);
          }),
        dstm_ts_reqs.data());
      auto dstm_ts_reps = evaluate_ts<true>(
        std::bind_front(&dtss::try_read, std::ref(*handler.state.dtss)),
        handler.state.models,
        dstm_ts_reqs,
        bind_period,
        use_ts_cached_read,
        update_ts_cache,
        clip_period);
      for (auto&& [ts_ref, ts] : std::views::zip(dstm_ts_refs, dstm_ts_reps))
        ts_ref->rep = std::dynamic_pointer_cast<time_series::dd::gpoint_ts const>(evaluate_ts_get(std::move(ts)).ts);
    }
    std::ranges::for_each(ts_attrs, &time_series::dd::apoint_ts::do_bind);

    std::vector<std::string> unit_group_urls;
    {
      auto model_prefix = fmt::format("dstm://M{}", model_id);
      for (auto const & g : model->unit_groups) {
        g->update_sum_expressions();
        std::ranges::copy(g->all_urls(model_prefix), std::back_inserter(unit_group_urls));
      }
    }

    auto ok = handler.state.models.set(model_id, model).get();
    if (!ok)
      return {false};

    handler.state.dtss->sm->notify_change(unit_group_urls);
    return {ok};
  }

  evaluate_ts_reply handle_request(auto& handler, evaluate_ts_request&& request) {
    auto& [time_series, bind_period, use_ts_cached_read, update_ts_cache, clip_period] = request;
    if (!bind_period.valid())
      throw std::runtime_error("evaluate requires a valid period-specification");
    return {evaluate_ts(
      std::bind_front(&dtss::try_read, std::ref(*handler.state.dtss)),
      handler.state.models,
      time_series,
      bind_period,
      use_ts_cached_read,
      update_ts_cache,
      clip_period)};
  }

  get_attrs_reply handle_request(auto& handler, get_attrs_request&& request) {
    return {get_attrs(handler.state.models, request.attrs)};
  }

  get_ts_reply handle_request(auto& handler, get_ts_request&& request) {

    auto attrs = get_attrs(handler.state.models, request.urls);

    std::vector<time_series::dd::apoint_ts> result(attrs.size());
    std::ranges::copy(
      std::views::transform(
        std::views::as_rvalue(attrs),
        std::bind_front(
          BOOST_HOF_LIFT(std::visit),
          boost::hof::match(
            [&](any_attr&& attr) -> time_series::dd::apoint_ts&& {
              return std::get<time_series::dd::apoint_ts>(std::move(attr));
            },
            [&](url_resolve_error&& error) -> time_series::dd::apoint_ts&& {
              throw std::runtime_error(error.what);
            }))),
      result.data());

    return {result};
  }

  set_attrs_reply handle_request(auto& handler, set_attrs_request&& request) {
    auto& [attrs] = request;
    auto ts_resolver = [&](std::shared_ptr<time_series::dd::ipoint_ts const> const &, std::string_view url)
      -> std::shared_ptr<time_series::dd::ipoint_ts const> {
      if (stm::url_peek_model_id(url).empty())
        return nullptr;
      auto find = std::ranges::find_if(attrs, [&](auto&& comp) {
        return url == comp.first;
      });
      if (find != attrs.end()) {
        auto const & attr_found = find->second;
        auto attr_ts = std::get_if<time_series::dd::apoint_ts>(&attr_found);
        return attr_ts ? attr_ts->ts : nullptr;
      }
      return get_ts(handler.state.models, url);
    };

    auto attr_is_cyclic = [&](auto&& attr) {
      if (auto ts = std::get_if<time_series::dd::apoint_ts>(&attr.second))
        return time_series::dd::is_cyclic(ts_resolver, *ts);
      return false;
    };
    if (std::ranges::any_of(attrs, attr_is_cyclic))
      throw std::runtime_error("set_attrs causes cycle");

    auto results = set_attrs(handler.state.models, attrs);
    std::vector<std::string> changed_urls;
    for (auto&& [result, attr] : std::views::zip(results, attrs))
      if (!result.has_value())
        changed_urls.emplace_back(attr.first);
    // notify so subscribers get updates
    handler.state.sm->notify_change(changed_urls);
    if (handler.state.dtss)
      handler.state.dtss->sm->notify_change(changed_urls);
    return {results};
  }

  set_ts_reply handle_request(auto& handler, set_ts_request&& request) {
    bool const rebind = true;
    auto set_ts = [&](auto&& view) {
      auto& model = view.model;
      for (auto const & tsx : request.time_series) {
        auto rts = std::dynamic_pointer_cast<time_series::dd::aref_ts const>(tsx.ts);
        if (!rts)
          throw std::runtime_error(
            fmt::format("Ts with no  ref. url can not be assigned, attempt on model id '{}'", request.model_id));
        url_invoke<time_series::dd::apoint_ts>(
          *model,
          rts->id,
          [&](auto& lhs) {
            if (!rts->rep)
              lhs = apoint_ts{};
            else
              ts_magic_merge_values(
                std::bind_front(dtss::do_merge_store_ts, std::ref(*handler.state.dtss)),
                std::bind_front(dtss::do_store_ts, std::ref(*handler.state.dtss)),
                lhs,
                apoint_ts{rts->rep},
                false,
                true,
                true);
          },
          [&] {
            throw std::runtime_error(fmt::format("Invalid url: '{}'", rts->id));
          });
        if (handler.state.dtss)
          handler.state.dtss->sm->notify_change(rts->id); // we notify on each item, because of possible exception
      }
      if (rebind)
        rebind_expression(*model, request.model_id, false);
    };
    handler.state.models.mutate_or_throw(request.model_id, set_ts).get();
    return {};
  }

  add_compute_server_reply handle_request(auto& handler, add_compute_server_request&& request) {
    return {handler.state.compute_manager.manage(request.address)};
  }

  compute_server_status_reply handle_request(auto& handler, [[maybe_unused]] compute_server_status_request&& request) {
    return {handler.state.compute_manager.status()};
  }

  start_tune_reply handle_request(auto& handler, start_tune_request&& request) {

    auto start_tune = [&](auto&& view) {
      auto&& [model, info] = view;
      if (info->state == model_state::running || info->state == model_state::tuning)
        return false;
      auto compute_server = handler.state.compute_manager.assign(request.model_id, model);
      if (!compute_server)
        return false;
      if (compute_server->is_busy()) {
        handler.log(
          dlib::LERROR, fmt::format("start_tune('{}'): Assigned compute node is already busy.", request.model_id));
        return false;
      }
      info->state = model_state::tuning;
      return true;
    };
    auto status = handler.state.models.mutate_or_throw(request.model_id, start_tune).get();
    if (!status)
      handler.log(dlib::LERROR, fmt::format("start_tune('{}'): Failed to assign compute node for.", request.model_id));
    return {status};
  }

  namespace detail {

    auto start_plan_task(
      dtss::server_state& dtss,
      auto& shared_model,
      std::string const & model_id,
      compute::client& client,
      generic_dt const & time_axis,
      std::vector<shop::shop_command> const & commands) {
      return shared_model
        .mutate([&](auto&& view) {
          return compute::send_input_and_plan(dtss, model_id, *view.model, client, time_axis, commands);
        })
        .get();
    }

    auto
      tick_plan_task(auto& sm, auto& shared_model, std::string const & model_id, std::vector<log_entry> const & log) {
      return shared_model
        .mutate([&](auto&& view) {
          if (!log.empty()) {
            std::ranges::copy(log, std::back_inserter(view.info->log));
            sm.notify_change(sub_model_log_topic(model_id));
          }
        })
        .get();
    }

    template <model_state next>
    auto done_plan_task(
      auto& dtss,
      auto& sub_manager,
      auto& shared_model,
      std::string const & model_id,
      compute::client& client,
      generic_dt const & time_axis) {
      static_assert(next == model_state::tuning || next == model_state::finished);
      auto result = shared_model
                      .observe([&](auto const && view) {
                        auto const& [model, info] = view;
                        return compute::get_plan_result(model_id, *model, client, time_axis);
                      })
                      .get();
      return shared_model
        .mutate([&](auto&& view) {
          auto& [model, info] = view;
          compute::assign_plan_result(
            model_id, *model, dtss ? dtss->sm.get() : nullptr, sub_manager, std::move(result));
          info->state = next;
          sub_manager.notify_change(sub_model_state_topic(model_id));
          return next == model_state::finished;
        })
        .get();
    }

    auto fail_plan_task(auto& sub_manager, auto& shared_model, std::string const & model_id, std::string_view what) {
      return shared_model
        .mutate([&](auto&& view) {
          server_log(dlib::LERROR, fmt::format("[dstm-compute] compute error: failed with {}", what));
          view.info->state = model_state::failed;
          sub_manager.notify_change(sub_model_state_topic(model_id));
        })
        .get();
    }

  }

  optimize_reply handle_request(auto& handler, optimize_request&& request) {
    if (!handler.state.dtss) {
      handler.log(dlib::LWARN, "optimize: Missing handler.state.dtss");
      return {false};
    }

    auto shared_model = handler.state.models.find(request.model_id).get();
    if (!shared_model) {
      handler.log(dlib::LWARN, fmt::format("optimize: Invalid model '{}'", request.model_id));
      return {false};
    }
    auto optimize = [&](auto&& view) {
      auto&& [model, info] = view;

      bool const needs_bind = stm::needs_bind(*model);

      if (needs_bind) {
        handler.log(
          dlib::LERROR,
          fmt::format("optimize('{}'): Cannot run optimization on model with unbound attributes.", request.model_id));
        return false;
      }

      if (info->state != model_state::idle) {
        handler.log(dlib::LWARN, fmt::format("optimize: Optimization is already running on '{}'", request.model_id));
        return false;
      }

      // NOTE:
      //     find an unassigned compute node...
      //     assign to model...
      //     poll for the result (for now)...
      //     - jeh
      auto compute_server = handler.state.compute_manager.assign(request.model_id, model);
      if (!compute_server) {
        handler.log(dlib::LERROR, fmt::format("optimize('{}'): No resources available.", request.model_id));
        return false;
      }
      if (compute_server->is_busy()) {
        handler.log(
          dlib::LERROR, fmt::format("optimize('{}'): Assigned compute node is already busy.", request.model_id));
        return false;
      }

      info->log_clear();
      info->state = model_state::running;
      handler.state.sm->notify_change(sub_model_state_topic(request.model_id));
      handler.state.sm->notify_change(sub_model_log_topic(request.model_id));
      compute_server->task(
        [shared_model,
         model_id = request.model_id,
         time_axis = request.time_axis,
         commands = request.commands,
         &dtss = handler.state.dtss,
         &sub_manager = *handler.state.sm,
         compute_server](auto compute_task) {
          using enum compute::task_tag;
          if constexpr (compute_task.tag == start)
            return detail::start_plan_task(*dtss, *shared_model, model_id, compute_task.client, time_axis, commands);
          else if constexpr (compute_task.tag == tick)
            return detail::tick_plan_task(sub_manager, *shared_model, model_id, compute_task.log);
          else if constexpr (compute_task.tag == done)
            return detail::done_plan_task<model_state::finished>(
              dtss, sub_manager, *shared_model, model_id, compute_task.client, time_axis);
          else
            return detail::fail_plan_task(sub_manager, *shared_model, model_id, compute_task.what);
        });
      return true;
    };
    auto status = shared_model->mutate(optimize).get();
    return {status};
  }

  namespace detail {
    std::shared_ptr<compute::managed_server>
      expect_compute_server(auto& handler, std::string_view model_id, auto& info) {

      auto compute_server = handler.state.compute_manager.assigned(model_id);
      if (!compute_server) {
        handler.log(dlib::LWARN, fmt::format("tune: No compute server for '{}'", model_id));
        info.state = model_state::failed;
        handler.state.sm->notify_change(sub_model_state_topic(model_id));
        return nullptr;
      }
      return compute_server;
    }

    auto expect_tuning_state(auto& handler, std::string_view model_id, auto& view) {
      if (view.state == model_state::tuning)
        return true;
      handler.log(dlib::LWARN, fmt::format("tune: No compute server for '{}'", model_id));
      return false;
    }
  }

  tune_reply handle_request(auto& handler, tune_request&& request) {
    if (!handler.state.dtss) {
      handler.log(dlib::LWARN, "tune: Missing handler.state.dtss");
      return {false};
    }

    auto shared_model = handler.state.models.find(request.model_id).get();
    if (!shared_model) {
      handler.log(dlib::LWARN, fmt::format("tune: Invalid model '{}'", request.model_id));
      return {false};
    }

    auto tune = [&](auto&& view) {
      auto&& [model, info] = view;

      if (!detail::expect_tuning_state(handler, request.model_id, *view.info))
        return false;

      auto compute_server = detail::expect_compute_server(handler, request.model_id, *view.info);
      if (!compute_server) { // We are in shared model mutate, so can not do_start_tune(model_id)
        // try to see if we can start it (a repair attempt)
        compute_server = handler.state.compute_manager.assign(request.model_id, model);
        if (!compute_server) {
          // it is already failed from expect..
          return false; // no.. failure, can not run
        }
        if (compute_server->is_busy())
          return false; // we are currently running something already
        info->state = model_state::tuning;
      }
      info->log_clear();
      info->state = model_state::running;
      handler.state.sm->notify_change(sub_model_state_topic(request.model_id));
      handler.state.sm->notify_change(sub_model_log_topic(request.model_id));

      compute_server->task(
        [time_axis = request.time_axis,
         commands = request.commands,
         compute_server,
         shared_model,
         &dtss = handler.state.dtss,
         &sub_manager = *handler.state.sm,
         model_id = std::string(request.model_id)](auto compute_task) {
          using enum compute::task_tag;
          if constexpr (compute_task.tag == start)
            return detail::start_plan_task(*dtss, *shared_model, model_id, compute_task.client, time_axis, commands);
          else if constexpr (compute_task.tag == tick)
            return detail::tick_plan_task(sub_manager, *shared_model, model_id, compute_task.log);
          else if constexpr (compute_task.tag == done)
            return detail::done_plan_task<model_state::tuning>(
              dtss, sub_manager, *shared_model, model_id, compute_task.client, time_axis);
          else
            return detail::fail_plan_task(sub_manager, *shared_model, model_id, compute_task.what);
        });
      return true;
    };

    return {shared_model->mutate(tune).get()};
  }

  stop_tune_reply handle_request(auto& handler, stop_tune_request&& request) {
    auto stop_tune = [&](auto&& view) {
      if (!detail::expect_tuning_state(handler, request.model_id, *view.info))
        return false;
      if (auto compute_server = detail::expect_compute_server(handler, request.model_id, *view.info))
        compute_server->unassign();
      view.info->state = model_state::finished;
      handler.state.sm->notify_change(sub_model_state_topic(request.model_id));

      return true;
    };
    return {handler.state.models.mutate_or_throw(request.model_id, stop_tune).get()};
  }

  fx_reply handle_request(auto& handler, fx_request&& request) {
    return {try_call(handler.state.callback, request.model_id, request.fx_arg).value_or(false)};
  }

  cache_flush_reply handle_request(auto& handler, cache_flush_request&& request) {
    if (!handler.state.dtss)
      return {};
    handler.state.dtss->ts_cache.flush();
    handler.state.dtss->ts_cache.clear_cache_stats();
    return {};
  }

  cache_stats_reply handle_request(auto& handler, cache_stats_request&& request) {
    if (!handler.state.dtss)
      return {};
    return {handler.state.dtss->ts_cache.get_cache_stats()};
  }

  template <auto msg>
  protocols::reply<msg> server_handler::operator()(protocols::request<msg>&& req) {
    return handle_request(*this, std::move(req));
  }

#define SHYFT_LAMBDA(r, data, elem) \
  template protocols::reply<elem##_msg> server_handler::operator()(protocols::request<elem##_msg>&&);
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA

}
