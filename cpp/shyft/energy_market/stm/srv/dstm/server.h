#pragma once

#include <algorithm>
#include <cstdint>
#include <exception>
#include <functional>
#include <future>
#include <memory>
#include <ranges>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

#include <boost/asio/thread_pool.hpp>
#include <boost/hof/lift.hpp>
#include <boost/hof/match.hpp>
#include <dlib/logger.h>

#include <shyft/core/fs_compat.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/protocol.h>
#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/url_parse.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/version.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::core::utcperiod;
  using shyft::core::utctime;
  using shyft::core::utctime_now;
  using subscription_manager = shyft::core::subscription::manager;

  // the type of callback provided, 1st arg model-id, 2nd arg fx-verb
  using callback_t = std::function<bool(std::string, std::string)>;

  void server_log(dlib::log_level l, std::string_view m);

  /**
   * @brief an in memory reactive stm model server
   * @details
   * The server provides dual interfaces, web-api and raw socket for py.
   * It keeps a list of named stm systems, descriptive energy market models, that can be
   * used/manipulated communicated with over the above mentioned interfaces.
   * The web api provides requests with subscription(notified when changed),
   * as well as setting set of attributes.
   * The py api provides functions for uploading/cloning/ new stm system models.
   * The server also provides scalable compute node capability, that harvest
   * the input from the descriptive, then sends the stm system and its input to
   * a compute node that when finished, provides results back to the
   * descriptive stm system model (decorated when finished).
   */
  struct server_state {

    using execution_context_type = boost::asio::thread_pool;
    using executor_type = execution_context_type::executor_type;
    using shared_model_type = stm::shared_model<executor_type>;

    server_state(compute::janitor_config const & j_config = {});
    ~server_state();

    callback_t callback;
    execution_context_type execution_context;
    stm::shared_models<executor_type> models;
    std::unique_ptr<dtss::server_state> dtss;
    std::shared_ptr<subscription_manager> sm = std::make_shared<subscription_manager>();
    utctime shared_lock_timeout{shyft::core::from_seconds(0.2)};
    compute::manager compute_manager;
    compute::janitor<executor_type> compute_janitor;
  };

  struct server_handler {
    server_state& state;
    std::string const & ip;
    unsigned short port;

    void log(dlib::log_level, std::string_view) const;

    template <auto msg>
    protocols::reply<msg> operator()(protocols::request<msg>&& request);
  };

#define SHYFT_LAMBDA(r, data, elem) \
  extern template protocols::reply<elem##_msg> server_handler::operator()(protocols::request<elem##_msg>&&);
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA

  struct server_dispatch {
    std::unique_ptr<server_state> state = std::make_unique<server_state>();

    void log(dlib::log_level, std::string_view) const;

    server_handler operator()(std::string const & ip, unsigned short port) & {
      return {*state, ip, port};
    }
  };

  using server = protocols::basic_server<dstm_protocol, server_dispatch>;

  inline auto make_server(compute::janitor_config j_config = {}) {
    return server({std::make_unique<server_state>(std::move(j_config))});
  }

#define SHYFT_LAMBDA(r, data, elem) \
  inline auto elem(server& s, auto&&... x) { \
    return s.dispatch({}, {})(elem##_request{SHYFT_FWD(x)...}); \
  }
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_PROTOCOL))
#undef SHYFT_LAMBDA

}
