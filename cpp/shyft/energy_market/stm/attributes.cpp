#include <sstream>

#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <fmt/core.h>

#include <shyft/core/boost_serialization_std_variant.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/attributes.h>

namespace shyft::energy_market::stm {

  using attr_types_v0 = std::tuple<
    bool,
    double,
    std::int64_t,
    std::uint64_t,
    apoint_ts,
    std::shared_ptr<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve>>>,
    std::shared_ptr<std::map<utctime, std::shared_ptr<hydro_power::xy_point_curve_with_z>>>,
    std::shared_ptr<std::map<utctime, std::shared_ptr<std::vector<hydro_power::xy_point_curve_with_z>>>>,
    std::shared_ptr<std::map<utctime, std::shared_ptr<hydro_power::turbine_description>>>,
    std::string,
    time_series::dd::ats_vector,
    unit_group_type,
    generic_dt,
    geo_point>;
  //-- this guard is here to ensure we do not change the attr_types, without also taking care
  //   to change the holder, or referencing classes involved in serialization
  //   The tuple is transformed into a variant, which is part of the serialization
  //   thus any change of order, or types will break persisted models, unless
  //   taken care of.
  static_assert(std::is_same_v<attr_types_v0, attr_types>, "Change of attr_types violates serialization");

  namespace detail {
    auto clone_attr_impl(auto const & attr) {
      return attr;
    }

    auto clone_attr_impl(auto const & attr)
    requires requires { attr.clone_expr(); }
    {
      return attr.clone_expr();
    }

    template < typename T>
    auto clone_attr_impl(std::shared_ptr<T> const & attr) {
      std::stringstream xml;
      {
        core::core_oarchive oa(xml, shyft::core::core_arch_flags);
        oa << shyft::core::core_nvp("attr", attr);
      }
      xml.flush();
      std::shared_ptr<T> res;
      {
        core::core_iarchive ia(xml, shyft::core::core_arch_flags);
        ia >> shyft::core::core_nvp("attr", res);
      }
      return res;
    }

  };

  any_attr clone_attr(any_attr const & attr) {
    return std::visit(
      [](auto const & arg) -> any_attr {
        return detail::clone_attr_impl(arg);
      },
      attr);
  }
}
