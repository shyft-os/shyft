#include <iterator>
#include <memory>
#include <ranges>
#include <string_view>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  bool transmission_line::operator==(transmission_line const & o) const {
    if (this == &o)
      return true;
    return equal_component_ptr(from_bb, o.from_bb) && equal_component_ptr(to_bb, o.to_bb) && super::operator==(o) && equal_component(*this, o);
  }

  void transmission_line::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = std::dynamic_pointer_cast<network>(net_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/t{o_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/t" + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }
}
