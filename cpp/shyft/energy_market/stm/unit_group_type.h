#pragma once
#include <array>
#include <cstdint>
#include <type_traits>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>

namespace shyft::energy_market::stm {

  SHYFT_DEFINE_ENUM(
    unit_group_type,
    std::uint16_t,
    (unspecified,
     //-- operational reserve groups
     fcr_n_up,
     fcr_n_down,
     fcr_d_up,
     fcr_d_down,
     afrr_up,
     afrr_down,
     mfrr_up,
     mfrr_down,
     ffr,
     rr_up,
     rr_down,
     //-- spinning
     commit,
     //-- with production requirement
     production));

  constexpr bool is_operational_reserve_type(unit_group_type e) {
    return unit_group_type::fcr_n_up <= e && e <= unit_group_type::ffr;
  }

  constexpr auto market_unit_group_types = std::array{
    unit_group_type::fcr_n_up,
    unit_group_type::fcr_n_down,
    unit_group_type::fcr_d_up,
    unit_group_type::fcr_d_down,
    unit_group_type::afrr_up,
    unit_group_type::afrr_down,
    unit_group_type::mfrr_up,
    unit_group_type::mfrr_down,
    unit_group_type::rr_up,
    unit_group_type::rr_down,
    unit_group_type::production};

}

SHYFT_DEFINE_ENUM_FORMATTER(shyft::energy_market::stm::unit_group_type);
