shyft_add_library(TARGET shyft-stm
  COMPONENT runtime
  INCLUDE_COMPONENT development
  attributes.cpp
  serialization.cpp
  serialize_v0_part1.cpp
  serialize_v0_part2.cpp
  market.cpp
  contract.cpp
  contract_portfolio.cpp
  network.cpp
  busbar.cpp
  transmission_line.cpp
  power_module.cpp
  reservoir.cpp
  reservoir_aggregate.cpp
  waterway.cpp
  unit.cpp
  power_plant.cpp
  run_parameters.cpp
  stm_system.cpp
  evaluate.cpp
  urls.cpp
  catchment.cpp
  unit_group.cpp
  optimization_summary.cpp
  price_delivery_convert.cpp
  urls.cpp
  stm_patch.cpp
  wind_farm.cpp
  wind_turbine.cpp
  srv/compute/server.cpp
  srv/compute/manager.cpp
  srv/compute/client.cpp
  srv/dstm/server.cpp
  srv/task/server.cpp
  srv/task/client.cpp
  srv/application/message_handler.cpp
  srv/application/request_handler.cpp
  ../../web_api/grammar/json_struct.cpp
  ../../web_api/energy_market/grammar/request.cpp
  ../../web_api/grammar/proxy_attr.cpp
  ../../web_api/energy_market/request_handler.cpp
  ../../web_api/energy_market/request_handler_set.cpp
  ../../web_api/energy_market/request_handler_get.cpp
  ../../web_api/energy_market/generators.cpp
  ../../web_api/energy_market/grammar/integer_list.cpp
  ../../web_api/energy_market/grammar/t_str.cpp
  ../../web_api/energy_market/srv/request_handler.cpp
  ../../web_api/energy_market/srv/grammar/model_info.cpp
  ../../web_api/energy_market/srv/grammar/model_ref.cpp
  ../../web_api/energy_market/srv/grammar/stm_case.cpp
  ../../web_api/energy_market/srv/grammar/stm_task.cpp
  ../../web_api/energy_market/stm/task/request_handler.cpp
  shop/shop_command.cpp
  shop/shop_serialization.cpp)

target_link_libraries(shyft-stm PUBLIC shyft-core shyft-energy_market)
if(SHYFT_WITH_SHOP)
  shyft_add_library(TARGET shyft-shop-wrap
          COMPONENT runtime
          INCLUDE_COMPONENT development
          shop/api/shop_api_wrap.cpp
  )
  target_link_libraries(shyft-shop-wrap PUBLIC "-Wl,--whole-archive" shop "-Wl,--no-whole-archive")
  target_link_options(shyft-shop-wrap PRIVATE
          "-Wl,--version-script=${CMAKE_CURRENT_SOURCE_DIR}/shop/api/shop_api.map"

  )

  target_sources(shyft-stm PRIVATE
    shop/shop_adapter.cpp
    shop/shop_api.cpp
    shop/shop_emitter.cpp
    shop/shop_init.cpp
    shop/shop_init.h
    shop/shop_log_hook.cpp
    shop/shop_post_process.cpp
    shop/shop_system.cpp
    shop/shop_serialization.cpp
    shop/shop_topology.cpp
    shop/export/shop_export_topology.cpp
    shop/export/shop_export_data.cpp
    shop/api/shop_proxy.cpp)
  target_link_libraries(shyft-stm PRIVATE shyft-shop-wrap)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  # note: gcc w. any debug fail, and retries without, so this just speeds up the case
  set_source_files_properties(serialization.cpp serialize_v0_part2.cpp serialize_v0_part1.cpp PROPERTIES COMPILE_FLAGS "-fno-var-tracking-assignments")
endif()

shyft_install_library(TARGET shyft-stm COMPONENT runtime)

if(SHYFT_WITH_SHOP)
  shyft_install_library(TARGET shyft-shop-wrap COMPONENT runtime)
  shyft_install_bundled_libdir(runtime SHYFT_SHOP_BUNDLE_PATH)
  if(SHYFT_SHOP_BUNDLE_ROOT)
    cmake_path(APPEND SHYFT_SHOP_BUNDLE_ROOT ${SHYFT_SHOP_BUNDLE_PATH} OUTPUT_VARIABLE SHYFT_SHOP_BUNDLE_PATH)
    set(SHYFT_SHOP_LICENSEDIR ${SHYFT_SHOP_BUNDLE_PATH})
    set(SHYFT_SHOP_LIBDIR ${SHYFT_SHOP_BUNDLE_PATH})
  else()
    set(SHYFT_SHOP_LICENSEDIR ${SHOP_LICENSE_DIR})
    set(SHYFT_SHOP_LIBDIR ${SHOP_LIBRARY_DIR})
  endif()
  set(SHYFT_SHOP_VERSION ${SHOP_FIND_VERSION})
  shyft_generate_source(shop/shop_bundle_path.h.in shyft/energy_market/stm/shop/shop_bundle_path.h)
  shyft_install_generated_headers(COMPONENT runtime)
endif()

if(SHYFT_WITH_SHOP)
  shyft_install_bundled_files(FILES ${SHOP_LICENSE_FILE} COMPONENT runtime)
  shyft_install_bundled_library(TARGET cplex COMPONENT runtime)
  shyft_install_bundled_library(TARGET shop_cplex COMPONENT runtime)
  shyft_install_bundled_libdir(runtime BUNDLED_LIBDIR)
  install(CODE "
    execute_process(COMMAND ${PATCHELF} --set-rpath \$ORIGIN \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${BUNDLED_LIBDIR}/${SHOP_CPLEX_NAME}
      OUTPUT_STRIP_TRAILING_WHITESPACE
      COMMAND_ERROR_IS_FATAL ANY)"
    COMPONENT runtime)
endif()

if(SHYFT_WITH_DEBUG)
  shyft_add_debug_symbols(TARGET shyft-stm)
  shyft_install_debug_symbols(TARGET shyft-stm COMPONENT runtime-debug)
  shyft_install_debug_sources(COMPONENT runtime-debug)
endif()
