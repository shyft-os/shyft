#include <shyft/energy_market/stm/wind_turbine.h>

namespace shyft::energy_market::stm {
  
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  wind_turbine::wind_turbine(int id, string const & name, string const & json, std::shared_ptr<wind_farm> farm)
    : super(id, name, json),farm{farm} {//height{0.0} {
    mk_url_fx(this);
  }

  wind_turbine::wind_turbine(){//:height{0.0} {
    mk_url_fx(this);
  }

  void wind_turbine::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = farm_();
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/T{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/T" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  bool wind_turbine::operator==(wind_turbine const & other) const {
    if (this == &other)
      return true;     // equal by addr.
    return id_base::operator==(other) && equal_component(*this, other);
  }
}
