#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/mp.h>

namespace shyft::energy_market::stm {

  optimization_summary::optimization_summary()
    : total{nan}
    , sum_penalties{nan}
    , minor_penalties{nan}
    , major_penalties{nan}
    , grand_total{nan} {
    mk_url_fx(this);
  }

  void
    optimization_summary::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (mdl) {
      mdl->generate_url(rbi, levels, template_levels);
      constexpr std::string_view part_name = ".summary";
      std::copy(std::begin(part_name), std::end(part_name), rbi);
    } else {
      constexpr std::string_view a = "O";
      std::copy(std::begin(a), std::end(a), rbi);
    }
  }

  optimization_summary& optimization_summary::operator=(optimization_summary const & o) {
    using namespace ::shyft::mp;

    id = o.id; // (1) copy the id_base members
    name = o.name;
    json = o.json;

    std::apply(
      [&](auto... P){
        ((walk_path(*this, P) = walk_path(o, P)),...);
      },
      paths_of<optimization_summary>{});

    return *this;
  }

  bool optimization_summary::operator==(optimization_summary const & other) const {
    if (this == &other)
      return true;
    return id_base::operator==(other) && equal_component(*this, other);
  };

  std::vector<string> optimization_summary::all_urls(std::string const & prefix) const {
    std::vector<std::string> r;
    string pre = prefix + ".summary.";
    hana::for_each(mp::leaf_accessor_map(hana::type_c<optimization_summary>), [&r, &pre](auto p) {
      r.push_back(pre + hana::first(p).c_str());
    });
    return r;
  }

}
