/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

// ensure to include boost serialization first
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/weak_ptr.hpp>

#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/serialization_choice.h>

// then other includes

#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/serialization_tools.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/wind_turbine.h>
// a dep extra here(consider move, and put dep to lib)
#include <shyft/energy_market/ui/ui_core.h>
// stuff needed to ensure vector<T>, map<K,V> etc. are automagically serializable
// #include <sstream>


// migration support from v0
#include <shyft/energy_market/stm/serialize_v0.h>


namespace stm = shyft::energy_market::stm;
namespace stm_v0 = shyft::energy_market::stm::v0;
namespace core = shyft::core;
namespace bs = boost::serialization;
namespace mp = shyft::mp;

template <class Archive>
void shyft::energy_market::stm::reservoir::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this));
  if (version < 1) {
    ar& core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this));
    stm_v0::reservoir r;
    stm::serialize_stm_attributes(r, ar);
    r.copy_to(*this);
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::reservoir_aggregate::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this)) & core::core_nvp("hps", hps)
    & core::core_nvp("reservoirs", reservoirs);
  if (version < 1) {
    stm_v0::reservoir_aggregate ra;
    stm::serialize_stm_attributes(ra, ar);
    ra.copy_to(*this);
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::unit::serialize(Archive& ar, unsigned int const version) {

  ar& core::core_nvp("super", bs::base_object<super>(*this));
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this));
      stm_v0::unit u;
      stm::serialize_stm_attributes(u, ar);
      u.copy_to(*this); // would throw.. if we did not trick the unit v0
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::power_plant::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this));
  if (version < 1) {
    ar& core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this));
    stm_v0::power_plant p;
    stm::serialize_stm_attributes(p, ar);
    p.copy_to(*this);
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::catchment::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this));
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this));
      stm_v0::catchment c;
      stm::serialize_stm_attributes(c, ar);
      c.copy_to(*this);
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::waterway::serialize(Archive& ar, unsigned int const version) {
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core::core_nvp("super", bs::base_object<super>(*this))
        & core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this));
      stm_v0::waterway w; // snapshot of what was v0 of waterway, we use it to correctly extract values from archive
      stm::serialize_stm_attributes(w, ar);
      w.copy_to(*this); // perform assing hana attrs from s to d, effectively d=s
    }
  } else {
    ar& core::core_nvp("super", bs::base_object<super>(*this));
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::gate::serialize(Archive& ar, unsigned int const version) {
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core::core_nvp("super", bs::base_object<super>(*this))
        & core::core_nvp("id_base", bs::base_object<shyft::energy_market::id_base>(*this));
      stm_v0::gate g;
      stm::serialize_stm_attributes(g, ar);
      g.copy_to(*this);
    }
  } else {
    ar& core::core_nvp("super", bs::base_object<super>(*this));
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::stm_hps::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) // core hydro power goes here(core model,topology)
    & core::core_nvp("reservoir_aggregates", reservoir_aggregates);
  if (version > 3) {
    if constexpr (Archive::is_loading::value) {
      stm_system_ s;
      ar& core::core_nvp("system", s);
      system = s;
    } else {
      stm_system_ s = system.lock();
      ar& core::core_nvp("system", s);
    }
  }
}

template <class Archive>
void shyft::energy_market::stm::unit_group::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("mdl", mdl)
    & core::core_nvp("members", members);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      stm_v0::unit_group ug;
      stm::serialize_stm_attributes(ug, ar);
      ug.copy_to(*this);
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::unit_group_member::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("group", group) & core::core_nvp("unit", unit);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      stm_v0::unit_group_member ugm;
      stm::serialize_stm_attributes(ugm, ar);
      ugm.copy_to(*this);
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::stm_system::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this));
  if (version < 3) {
    if constexpr (Archive::is_loading::value) {
      // OBS! care must be taken here, the members(unit_group, network) have uplink to this, as ptr and weak_ptr,
      // so v2 stream and simple assign will fail.
      // And since we do not have share_ptr of this, then we can not
      // repair it either (unless we also override shared_ptr/weakptr for the stm_system)
      // thus just mimic the v2 leaf node deserialize is safe,minimal, efficient and precise
      ar& core::core_nvp("hps", hps) & core::core_nvp("market", market) & core::core_nvp("contracts", contracts)
        & core::core_nvp("contract_portfolio", contract_portfolios) & core::core_nvp("networks", networks)
        & core::core_nvp("power_modules", power_modules)
        & core::core_nvp("run_params.n_inc_runs", run_params.n_inc_runs)
        & core::core_nvp("run_params.n_full_runs", run_params.n_full_runs)
        & core::core_nvp("run_params.head_opt", run_params.head_opt)
        & core::core_nvp("run_params.run_time_axis", run_params.run_time_axis)
        & core::core_nvp("run_params.fx_log", run_params.fx_log) & core::core_nvp("unit_groups", unit_groups)
        & core::core_nvp("summary", summary);
    }
  } else {
    stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::energy_market_area::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("sys", sys)
    & core::core_nvp("unit_groups", unit_groups) & core::core_nvp("contracts", contracts);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      stm_v0::energy_market_area ma;
      stm::serialize_stm_attributes(ma, ar);
      ma.copy_to(*this);
    }
  } else {
    ar& core::core_nvp("busbars", busbars);
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::contract::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this))
    & core::core_nvp("sys", sys)                    // weak_ptr Hmm. did that really work ..
    & core::core_nvp("power_plants", power_plants); // remember to stream out relations
  ;
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      stm_v0::contract c;
      stm::serialize_stm_attributes(c, ar);
      c.copy_to(*this);
    }
  } else {
    if (version >= 2)
      ar& core::core_nvp("wind_farms", wind_farms);

    ar& core::core_nvp("relations", relations); // remember to stream out relations
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::contract_relation::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("owner", owner) & core::core_nvp("id", id) & core::core_nvp("related", related);
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::contract_portfolio::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("sys", sys)
    & core::core_nvp("contracts", contracts);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      stm_v0::contract_portfolio cp;
      stm::serialize_stm_attributes(cp, ar);
      cp.copy_to(*this);
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::network::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("sys", sys) // weak_ptr
    ;
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_stm_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::power_module::serialize(Archive& ar, unsigned int const version) {

  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("sys", sys) // weak_ptr
    ;
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      shared_ptr<busbar> connected;
      ar& core::core_nvp("connected", connected); // TODO: figure out if we need to do more that discard this
      stm_v0::power_module m;
      stm::serialize_stm_attributes(m, ar);
      m.copy_to(*this);
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::transmission_line::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("net", net) // weak_ptr
    & core::core_nvp("from_bb", from_bb) & core::core_nvp("to_bb", to_bb);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      ar& core::core_nvp("capacity", capacity); // effectively what the old stm::serialize_stm_attributes did
    }
  } else {
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::busbar::serialize(Archive& ar, unsigned int const version) {
  ar // common from version 0:
      & core::core_nvp("super", bs::base_object<super>(*this))
    & core::core_nvp("net", net) // weak_ptr
    ;
  if (version < 1) {
    if constexpr (Archive::is_loading::value) { // bw compat read
      apoint_ts dummy;
      ar& core::core_nvp("dummy", dummy); // read away the dummy
    }
  } else {
    ar // common from version 1:
        & core::core_nvp("power_modules", power_modules)
      & core::core_nvp("units", units);
    if (version > 1) {
      ar // common from version 2:
        & core::core_nvp("wind_farms", wind_farms);
    }
    if constexpr (Archive::mode == core::serialization_choice::all)
      stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::power_module_member::serialize(
  Archive& ar,
  [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("owner", owner) & core::core_nvp("power_module", power_module);
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::unit_member::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("owner", owner) & core::core_nvp("unit", unit);
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::wind_farm_member::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("owner", owner) & core::core_nvp("farm", farm);
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::srv::model_ref::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("host", host) & core::core_nvp("port_num", port_num) & core::core_nvp("api_port_num", api_port_num)
    & core::core_nvp("model_key", model_key) & core::core_nvp("labels", labels);
  if (version >= 1)
    ar& core::core_nvp("stm_id", stm_id);
}

template <class Archive>
void shyft::energy_market::stm::srv::stm_case::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("id", id) & core::core_nvp("name", name) & core::core_nvp("json", json)
    & core::core_nvp("created", created) & core::core_nvp("labels", labels) & core::core_nvp("model_refs", model_refs);
}

template <class Archive>
void shyft::energy_market::stm::srv::stm_task::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("id", id) & core::core_nvp("name", name) & core::core_nvp("json", json)
    & core::core_nvp("created", created) & core::core_nvp("labels", labels) & core::core_nvp("runs", cases)
    & core::core_nvp("base_mdl", base_mdl) & core::core_nvp("task_name", task_name);
}

template <class Archive>
void shyft::energy_market::stm::optimization_summary::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("mdl", mdl);
  if (version < 1) {
    if constexpr (Archive::is_loading::value) {
      stm_v0::optimization_summary os;
      stm::serialize_stm_attributes(os, ar);
      os.copy_to(*this);
    }
  } else {
    stm::serialize_named_attributes(*this, ar);
  }
}

template <class Archive>
void shyft::energy_market::stm::wind_farm::serialize(Archive& ar, unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("sys", sys) // weak ptr
    ;
  if (version > 0) {
    ar& core::core_nvp("wind_turbines", wind_turbines);
  }
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_named_attributes(*this, ar);
}

template <class Archive>
void shyft::energy_market::stm::wind_turbine::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core::core_nvp("super", bs::base_object<super>(*this)) & core::core_nvp("farm", farm) // weak ptr
    ;
  if constexpr (Archive::mode == core::serialization_choice::all)
    stm::serialize_named_attributes(*this, ar);
}

template <class Archive>
void stm::srv::compute::managed_server_status::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("address", address) & core_nvp("state", state) & core_nvp("model_id", model_id)
    & core_nvp("last_send", last_send);
}

using shyft::core::core_nvp;

template <class Archive>
void shyft::energy_market::ui::layout_info::serialize(Archive& ar, [[maybe_unused]] unsigned int const version) {
  ar& core_nvp("id", id) & core_nvp("name", name) & core_nvp("json", json);
}

#define SERIALIZED_POLY_STM_TYPES (unit, power_plant, reservoir, catchment, waterway, gate, stm_hps)


#define SHYFT_INSTANTIATE_TYPES_LAMBDA(r, data, elem) \
  x_poly_serialize_instantiate_and_register(shyft::energy_market::stm::elem);

BOOST_PP_LIST_FOR_EACH(SHYFT_INSTANTIATE_TYPES_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SERIALIZED_POLY_STM_TYPES))

#undef SHYFT_INSTANTIATE_TYPES_LAMBDA
#undef SERIALIZED_POLY_STM_TYPES

#define SERIALIZED_STM_TYPES \
  (stm_system, \
   unit_group, \
   reservoir_aggregate, \
   unit_group_member, \
   energy_market_area, \
   contract, \
   contract_relation, \
   contract_portfolio, \
   network, \
   transmission_line, \
   busbar, \
   power_module, \
   srv::model_ref, \
   srv::stm_case, \
   srv::stm_task, \
   optimization_summary, \
   wind_farm, \
   wind_turbine, \
   srv::compute::managed_server_status, \
   unit_member, \
   power_module_member, \
   wind_farm_member)


#define SHYFT_INSTANTIATE_TYPES_LAMBDA(r, data, elem) \
  x_serialize_instantiate_and_register(shyft::energy_market::stm::elem);

BOOST_PP_LIST_FOR_EACH(SHYFT_INSTANTIATE_TYPES_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SERIALIZED_STM_TYPES))

#undef SHYFT_INSTANTIATE_TYPES_LAMBDA
#undef SERIALIZED_STM_TYPES
// ui : deps on stm etc, so we must consider even better library structure regarding dependencies.
x_serialize_instantiate_and_register(shyft::energy_market::ui::layout_info);

namespace shyft::energy_market::stm {

  /**
   * The polymorphic is an unfortunate left-over,
   * that we try to support, will be deprecated soon after 23.0.0.
   */
  template <class Archive>
  void register_types(Archive& a) {
    bool old_style_register = false;
    if constexpr (Archive::is_loading::value) {
      if (!a.archive_info.is_valid())
        old_style_register = true; // if it is pre arch-info, then we need the type registration to read it
    }
    if (old_style_register) {
      // original set of poly and non poly(no op??)
      a.template register_type<reservoir>();
      a.template register_type<reservoir_aggregate>();
      a.template register_type<waterway>();
      a.template register_type<unit>();
      a.template register_type<unit_group>();
      a.template register_type<gate>();
      a.template register_type<power_plant>();
      a.template register_type<catchment>();
      a.template register_type<stm_hps>();
      a.template register_type<stm_system>();
      a.template register_type<energy_market_area>();
      a.template register_type<contract>();
      a.template register_type<contract_portfolio>();
      a.template register_type<network>();
      a.template register_type<transmission_line>();
      a.template register_type<busbar>();
      a.template register_type<power_module>();
      a.template register_type<optimization_summary>();

      // adding types in order of appearance for the missing
      a.template register_type<hydro_power::reservoir>();
      a.template register_type<hydro_power::hydro_power_system>();
      a.template register_type<hydro_power::hydro_component>();
      // adding these breaks bw.compat, but might be needed
      // so additional types(non poly) seems to play a part
      // as well:
#ifdef __clang__
      // clang needs these
      a.template register_type<hydro_power::gate>();
      a.template register_type<hydro_power::waterway>();
      a.template register_type<hydro_power::power_plant>();
      a.template register_type<hydro_power::unit>();
#endif
    }
  }

  /**fx_to_blob simply serializes an object to a blob */
  template <class T>
  static auto fx_to_blob(std::shared_ptr<T> const & s) {
    std::ostringstream xmls(std::ios::binary);
    {
      core::core_oarchive oa(xmls, core::arch_info_flags);
      register_types(oa);
      oa << core::core_nvp("hps", s);
    }
    xmls.flush();
    return xmls.str();
  }

  /** fx_from_blob de-serializes a blob to a fully working object*/
  template <class T>
  static auto fx_from_blob(std::string const & xmls) {
    std::shared_ptr<T> s;
    std::istringstream xmli(xmls, std::ios::binary);
    {
      core::core_iarchive ia(xmli, core::arch_info_flags);
      register_types(ia);
      ia >> core::core_nvp("hps", s);
    }
    return s;
  }

  /**fx_to_blob simply serializes an object to a blob */
  template <class T>
  static auto fx_to_blob_strip(std::shared_ptr<T> const & s) {
    std::ostringstream xmls(std::ios::binary);
    {
      core::core_oarchive_stripped oa(xmls, core::arch_info_flags);
      register_types(oa);
      oa << core::core_nvp("hps", s);
    }
    xmls.flush();
    return xmls.str();
  }

  /** fx_from_blob de-serializes a blob to a fully working object*/
  template <class T>
  static auto fx_from_blob_strip(std::string const & xmls) {
    std::shared_ptr<T> s;
    std::istringstream xmli(xmls, std::ios::binary);
    {
      core::core_iarchive_stripped ia(xmli, core::arch_info_flags);
      register_types(ia);
      ia >> core::core_nvp("hps", s);
    }
    return s;
  }

  // implementation for main classes, using the templates above

  std::string stm_hps::to_blob(std::shared_ptr<stm_hps> const & s) {
    return fx_to_blob<stm_hps>(s);
  }

  std::shared_ptr<stm_hps> stm_hps::from_blob(std::string const & xmls) {
    return fx_from_blob<stm_hps>(xmls);
  }

  std::string stm_system::to_blob(std::shared_ptr<stm_system> const & s) {
    return fx_to_blob<stm_system>(s);
  }

  std::shared_ptr<stm_system> stm_system::from_blob(std::string const & xmls) {
    auto sys = fx_from_blob<stm_system>(xmls);
    stm_system::fix_uplinks(sys); // ensure uplinks are in place for any version
    return sys;
  }

  std::string stm_hps::to_blob_strip(std::shared_ptr<stm_hps> const & s) {
    return fx_to_blob_strip<stm_hps>(s);
  }

  std::shared_ptr<stm_hps> stm_hps::from_blob_strip(std::string const & xmls) {
    return fx_from_blob_strip<stm_hps>(xmls);
  }

  std::string stm_system::to_blob_strip(std::shared_ptr<stm_system> const & s) {
    return fx_to_blob_strip<stm_system>(s);
  }

  std::shared_ptr<stm_system> stm_system::from_blob_strip(std::string const & xmls) {
    auto sys = fx_from_blob_strip<stm_system>(xmls);
    stm_system::fix_uplinks(sys); // ensure uplinks are in place for any version
    return sys;
  }

}
