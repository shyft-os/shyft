/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/serialize_v0.h>
#include <shyft/mp/assign.h>

namespace shyft::energy_market::stm::v0 {
  namespace stm = shyft::energy_market::stm;
  namespace mp = shyft::mp;

// note part1 and part2 are just to get shorter compile times when compiling in parallel
#define STM_V0_TYPES (unit, unit_group, unit_group_member, power_plant, reservoir, reservoir_aggregate)

#define SHYFT_INSTANTIATE_TYPES_LAMBDA(r, data, elem) \
  void elem::copy_to(stm::elem &d) const { \
    mp::leaf_access_assign(d, *this); \
  }

  BOOST_PP_LIST_FOR_EACH(SHYFT_INSTANTIATE_TYPES_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(STM_V0_TYPES))

#undef SHYFT_INSTANTIATE_TYPES_LAMBDA
#undef STM_V0_TYPES

}
