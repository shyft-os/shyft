#include <iterator>
#include <memory>
#include <ranges>
#include <string>
#include <string_view>

#include <shyft/energy_market/stm/reservoir.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  reservoir::reservoir(int id, std::string const & name, std::string const & json, std::shared_ptr<stm_hps> const & hps)
    : super(id, name, json, hps) {
    mk_url_fx(this);
  }

  void reservoir::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = std::dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/R{o_id}";
      std::ranges::copy(a, rbi);
    } else {
      auto idstr = "/R" + std::to_string(id);
      std::ranges::copy(idstr, rbi);
    }
  }

  bool reservoir::operator==(reservoir const & other) const {
    if (this == &other)
      return true;
    return super::operator==(other) && equal_component(*this, other);
  }

}
