#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/reflection.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {

  using shyft::core::utctime;
  using shyft::core::geo_point;
  using shyft::time_series::dd::apoint_ts;

  /**
   * @brief wind_turbine
   * @details
   * Captures the essential useful properties of a wind-turbine,
   * @ref "https://en.wikipedia.org/wiki/Wind_turbine"
   * in the context of operational energy-management
   * process.
   * A wind turbine is part of a wind farm, and currently it is
   * the wind-farm level that connects the energy produced to the
   * bus-bar of the electrical distribution network.
   * The internal network of the wind-farm is currently not
   * modelled.
   */
  struct wind_turbine : id_base {
    using super = id_base;

    /** @brief Generate an almost unique, url-like string for this object.
     *
     * @param rbi Back inserter to store result.
     * @param levels How many levels of the url to include. Use value 0 to
     *     include only this level, negative value to include all levels (default).
     * @param template_levels From which level to start using placeholder instead of
     *     actual object ID. Use value 0 for all, negative value for none (default).
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    wind_turbine();
    wind_turbine(int id, std::string const & name, std::string const & json, std::shared_ptr<wind_farm> wf);
    wind_turbine(int id, std::string const & name, std::string const & json)
      : wind_turbine(id, name, json, nullptr){};

    bool operator==(wind_turbine const & other) const;

    bool operator!=(wind_turbine const & other) const {
      return !(*this == other);
    }
    std::weak_ptr<wind_farm> farm; ///< the wind-farm it belongs to, enable navigation

    auto farm_() const {
      return farm.lock();
    }

    struct production_ {
      struct constraint_ {
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min), ///< W as in lower part of range, at least?
          (apoint_ts, max)  ///< W as in upper part of range, maximum?
        );
        SHYFT_STM_COMPONENT(constraint_, (), (min, max));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        production_,             // units are W, watt
        (apoint_ts, schedule),   ///< W as in planned
        (apoint_ts, realised),   ///< W as in historical fact
        (constraint_, constraint),///< min/max constraint
        (apoint_ts, result)      ///< W as in simulated/forecasted result
      );
      SHYFT_STM_COMPONENT(production_, (), (schedule, realised, constraint, result));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    struct cost_ {
      BOOST_HANA_DEFINE_STRUCT(
        cost_,
        (apoint_ts, start), ///< money/#start
        (apoint_ts, stop)  ///< money/#stop
      );
      SHYFT_STM_COMPONENT(cost_, (), (start, stop));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    /** operational reserve, frequency balancing support */
    struct reserve_ { //

      /** reserve_.spec_ provides schedule, or min-max + result mode
       *  that is: if the schedule is provided, the optimizer will respect that
       *  otherwise, plan-mode: find .result within min..max so that external
       *  group requirement is satisfied
       */
      struct spec_ {
        BOOST_HANA_DEFINE_STRUCT(
          spec_,
          (apoint_ts, schedule), ///< W, if specified 'schedule-mode' and next members ignored
          (apoint_ts, min),      ///< W
          (apoint_ts, max),      ///< W
          (apoint_ts, cost),     ///< money
          (apoint_ts, result),   ///< W
          (apoint_ts, penalty),  ///< money
          (apoint_ts, realised)  ///< as in historical fact,possibly computed from production/min/max etc.
        );

        SHYFT_STM_COMPONENT(spec_, (), (schedule, min, max, cost, result, penalty, realised))
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      /** most reserve modes goes in up and down regulations */
      struct pair_ {
        BOOST_HANA_DEFINE_STRUCT(
          pair_,
          (spec_, up),  ///< up regulation reserve
          (spec_, down) ///< down regulation reserve
        );
        SHYFT_STM_COMPONENT(pair_, (), (up, down));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        reserve_,
        (pair_, fcr_n),              ///< FCR up,down
        (pair_, mfrr),               ///< mFRR up,down
        (pair_, fcr_d)               ///< FCR-D up/down
      );
      SHYFT_STM_COMPONENT(reserve_, (), (fcr_n, mfrr, fcr_d))
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    //-- class descriptive members
    BOOST_HANA_DEFINE_STRUCT(
      wind_turbine,
      (apoint_ts, height),         ///< meter, height from ground to center of turbine
      (geo_point, location),       ///< x,y,z, epsg as for the wind-farm it belongs to
      (t_xy_, power_curve),        ///< x=m/s, y= W, wind-speed to effect translation, gives min..max MW range?
      (apoint_ts, unavailability), ///< no-unit, 1== unavailable, (0,nan)-> available
      (production_, production),
      (cost_, cost),      ///< start/stop
      (reserve_, reserve) ///< operational reserve, frequency balancing support
    );
    SHYFT_STM_COMPONENT(wind_turbine, (), (height, power_curve, unavailability, production, cost, reserve));

    x_serialize_decl();
  };

  using wind_turbine_ = std::shared_ptr<wind_turbine>;

}

x_serialize_export_key(shyft::energy_market::stm::wind_turbine);
// BOOST_CLASS_VERSION(shyft::energy_market::stm::wind_turbine, 0);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_turbine::production_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_turbine::production_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_turbine::cost_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_turbine::reserve_::spec_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_turbine::reserve_::pair_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_turbine::reserve_);

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::wind_turbine, Char>
  : shyft::energy_market::id_base_formatter<shyft::energy_market::stm::wind_turbine, Char> { };

template <typename Char>
struct fmt::formatter<std::shared_ptr<shyft::energy_market::stm::wind_turbine>, Char>
  : shyft::ptr_formatter<shyft::energy_market::stm::wind_turbine, Char> { };
