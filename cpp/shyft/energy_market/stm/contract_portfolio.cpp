#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  using std::static_pointer_cast;
  using std::dynamic_pointer_cast;
  using std::make_shared;
  using std::runtime_error;

  void contract_portfolio::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels)
    const {
    if (levels) {
      auto tmp = sys_();
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/p{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto a = "/p" + std::to_string(id);
      std::copy(std::begin(a), std::end(a), rbi);
    }
  }

  bool contract_portfolio::operator==(contract_portfolio const & o) const {
    if (this == &o)
      return true; // equal by addr.
    // check contracts..:
    auto c_equal = equal_vector_ptr_content<contract>(contracts, o.contracts);
    return c_equal && super::operator==(o) && equal_component(*this, o);
  }

}
