#pragma once
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/mp.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {
  struct wind_turbine; // fwd.

  /**
   * @brief Wind Farm
   *
   * @details
   * Wind Farm, a grouping of wind turbines,
   * connected to the power-grid.
   * @ref "https://en.wikipedia.org/wiki/Wind_farm"
   *
   * It can be used as a single entity, with the
   * properties setup, or it can also be extended
   * with a number of wind turbines that captures
   * the individual properties of each turbine.
   *
   * When adding wind-turbines, the usual unique
   * constraints applies to the wind-turbine id
   * and wind-turbine names.
   *
   */
  struct wind_farm : id_base {
    using super = id_base;

    wind_farm()
      : epsg{0} {
      mk_url_fx(this);
    }

    wind_farm(int id, std::string const & name, std::string const & json, std::shared_ptr<stm_system> const & sys)
      : super{id, name, json, {}, {}, {}}
      , sys{sys}
      , epsg{0} {
      mk_url_fx(this);
    }

    bool operator==(wind_farm const & other) const;

    bool operator!=(wind_farm const & other) const {
      return !(*this == other);
    }

    auto sys_() const {
      return sys.lock();
    }

    /**
     * @brief create and add a wind-turbine to the wind-farm
     * @details
     * Create a wind-turbine with the specified parameters,
     * doing some validation of the `id` and `name` to ensure
     * it is unique within the wind-farm.
     * The turbine.farm is set to point to the wind-farm
     * @param id the wind-farm unique id, required to be >0 and unique for wind-farm
     * @param name the name, required to be unique for wind-farm
     * @param json a user specified json string
     * @param location x,y,z for the turbine, in epsg specs for the wind-farm
     * @param height time-series for the height
     * @param power_curve time-dependent power-curve
     * @return the wind-turbine created
     * @throws std::runtime_error if id,name does not meet criteria
     */
    [[nodiscard]] std::shared_ptr<wind_turbine> create_wind_turbine(
      int id,
      std::string const & name,
      std::string const & json,
      geo_point const & location,
      apoint_ts height,
      t_xy_ power_curve);

    /**
     * @brief add and own turbines passed
     * @details
     * Add set of wind turbines, ensuring the ownership is set.
     * Each of the turbine passed, is validated with respect to
     * requirement for `id` and `name` prior to changing the class.
     *
     * Notice that the turbine farm member will point to this
     * wind-farm after successful validation and assign.
     * @param turbines
     */
    void add_wind_turbines(std::vector<std::shared_ptr<wind_turbine>> const & turbines);


    std::weak_ptr<stm_system> sys;

    /** @brief generate an almost unique, url-like string for a wind farm.
     *
     * @param rbi: back inserter to store result
     * @param levels: How many levels of the url to include.
     * 		levels == 0 includes only this level. Use level < 0 to include all levels.
     * @param placeholders: The last element of the vector states wethers to use the wind farm ID
     * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
     * 		If the vector is empty, the function defaults to not using placeholders.
     * @return
     */
    void generate_url(std::back_insert_iterator<std::string>& rbi, int levels = -1, int template_levels = -1) const;

    struct production_ {
      struct constraint_ {
        BOOST_HANA_DEFINE_STRUCT(
          constraint_,
          (apoint_ts, min), ///< W as in lower part of range, at least?
          (apoint_ts, max)  ///< W as in upper part of range, maximum?
        );
        SHYFT_STM_COMPONENT(constraint_, (), (min, max));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        production_,               ///< W
        (apoint_ts, forecast),     ///< W, expected production, given some pre-conditions
        (apoint_ts, schedule),     ///< W, planned/committed production
        (apoint_ts, realised),     ///< W, historical fact
        (constraint_, constraint), ///< any production contraints, physical/logical/contextual
        (apoint_ts, result)        ///< W, result of simulation/plan/forecast
      );
      SHYFT_STM_COMPONENT(production_, (), (forecast, schedule, realised, constraint, result));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    struct reserve_ { //

      /** reserve_.spec_ provides schedule, or min-max + result mode
       *  that is: if the schedule is provided, the optimizer will respect that
       *  otherwise, plan-mode: find .result within min..max so that external
       *  group requirement is satisfied
       */
      struct spec_ {
        BOOST_HANA_DEFINE_STRUCT(
          spec_,
          (apoint_ts, schedule), ///< W, if specified 'schedule-mode' and next members ignored
          (apoint_ts, min),      ///< W
          (apoint_ts, max),      ///< W
          (apoint_ts, cost),     ///< money
          (apoint_ts, result),   ///< W
          (apoint_ts, penalty),  ///< money
          (apoint_ts, realised)  ///< as in historical fact,possibly computed from production/min/max etc.
        );

        SHYFT_STM_COMPONENT(spec_, (), (schedule, min, max, cost, result, penalty, realised))
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      /** most reserve modes goes in up and down regulations */
      struct pair_ {
        BOOST_HANA_DEFINE_STRUCT(
          pair_,
          (spec_, up),  ///< up regulation reserve
          (spec_, down) ///< down regulation reserve
        );
        SHYFT_STM_COMPONENT(pair_, (), (up, down));
        url_fx_t url_fx; // needed by .url(...) to python exposure
      };

      BOOST_HANA_DEFINE_STRUCT(
        reserve_,
        (pair_, fcr_n), ///< FCR up,down
        (pair_, mfrr),  ///< mFRR up,down
        (pair_, fcr_d));
      SHYFT_STM_COMPONENT(reserve_, (), (fcr_n, mfrr, fcr_d));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    struct cost_ {
      BOOST_HANA_DEFINE_STRUCT(
        cost_,
        (apoint_ts, break_even) ///< [money/J] price/energy required to break-even
      );
      SHYFT_STM_COMPONENT(cost_, (), (break_even));
      url_fx_t url_fx; // needed by .url(...) to python exposure
    };

    BOOST_HANA_DEFINE_STRUCT(
      wind_farm,
      (apoint_ts, height),         ///< meter, height from ground to center of turbines, average
      (geo_point, location),       ///< x,y,z, as in mid-point, and epsg according to spec
      (std::uint16_t, epsg),       ///< epsg for the location and wind-turbine locations
      (t_xy_, power_curve),        ///< x=m/s, y= W, wind-speed to effect translation, gives min..max MW range?
      (apoint_ts, unavailability), ///< range 0..1, 1=all unavailable, 0.0=all available,  in steps of 1/n -turbines for
                                   ///  equal sized turbines
      (apoint_ts, wake_loss),      ///< range 0.87..0.93, seasonal variations might be +-0.03
      (apoint_ts, power_correction), ///< range 0.7..1.3, air-density power-correction
      (apoint_ts, icing),            ///< range 0.6..1.0, 1.0 if no icing
      (production_, production),     ///< production attributes for the sum of wind-farm
      (reserve_, reserve),           ///< reserve attributes for the sum of wind-farm
      (cost_, cost));                ///< cost related, and/or, tactical attributes

    std::vector<std::shared_ptr<wind_turbine>> wind_turbines; ///< detailed wind-farm/turbine modelling
    SHYFT_STM_COMPONENT(
      wind_farm,
      (),
      (height,
       location,
       epsg,
       power_curve,
       unavailability,
       wake_loss,
       power_correction,
       icing,
       production,
       reserve,
       cost,
       wind_turbines));

    x_serialize_decl();
  };
}

x_serialize_export_key(shyft::energy_market::stm::wind_farm);
BOOST_CLASS_VERSION(shyft::energy_market::stm::wind_farm, 1);

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::production_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::production_::constraint_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::reserve_::spec_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::reserve_::pair_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::reserve_);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::energy_market::stm::wind_farm::cost_);

SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::energy_market::stm::wind_farm);
