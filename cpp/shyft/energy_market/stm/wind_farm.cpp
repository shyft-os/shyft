#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/wind_turbine.h>

namespace shyft::energy_market::stm {
  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  bool wind_farm::operator==(wind_farm const & other) const {
    if (this == &other) {
      return true; // equal by addr.
    }
    if (super::operator!=(other)) {
      return false; // neq by base class
    }
    auto equal_attributes = id_base::operator==(other) && equal_component(*this, other);
    return equal_attributes && (wind_turbines.size() == other.wind_turbines.size())
        && std::equal(
             wind_turbines.begin(),
             wind_turbines.end(),
             other.wind_turbines.begin(),
             [](auto const & a, auto const & b) {
               return a == b || *a == *b;
             });
  }

  void wind_farm::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_system>(sys_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    // W for Wind farm
    if (!template_levels) {
      constexpr std::string_view a = "/w{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/w" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  static void validate_wind_turbine_or_throw(
    std::shared_ptr<wind_turbine> const & x,                     ///< to be validated
    std::vector<std::shared_ptr<wind_turbine>> const & turbines, ///< against others for uniqueness
    bool skip_self) { ///< skip element if it already is in the turbines (self check)
    if (x == nullptr)
      throw std::runtime_error("WindFarm: attempt to add null wind-turbine object");
    // note: have to allow full-range id due to patching functionality.
    // ref. stm::patch(..), the new objects(if multiple), identifiers are unique less than zero
    if (x->name.empty())
      throw std::runtime_error(fmt::format("WindTurbine: name must be specified for item with id={}", x->id));
    for (auto const & t : turbines) {
      if (skip_self && t.get() == x.get())
        continue;
      if (x->id == t->id)
        throw std::runtime_error(fmt::format("WindFarm: WindTurbine id {} is not unique", x->id));
      if (x->name == t->name)
        throw std::runtime_error(fmt::format("WindFarm: WindTurbine name {} is not unique", x->name));
    }
  }

  void wind_farm::add_wind_turbines(std::vector<std::shared_ptr<wind_turbine>> const & turbines) {
    // (1) check preconditions, there is some, try to provide hints that guide a py user
    auto owner = sys_();
    if (owner == nullptr)
      throw std::runtime_error("WindFarm: must be owned by a StmSystem prior to adding turbines");
    auto me_it = std::ranges::find_if(owner->wind_farms, [this](auto const & x) {
      return x.get() == this;
    });
    if (me_it == std::end(owner->wind_farms))
      throw std::runtime_error(
        "WindFarm: WindFarm not found in owner's wind_farms, a wind-farm must be added to stm-system and owned by it "
        "prior to adding turbines");
    auto const & self = *me_it;
    // (2) validate inputs, and again try to be helpful
    for (auto const & t : turbines) // verify the set passed in first
      validate_wind_turbine_or_throw(t, turbines, true);

    for (auto const & t : turbines) // then verify against any existing
      validate_wind_turbine_or_throw(t, wind_turbines, false);
    // (3) all verification done, then just own them. (FIXME:silently ignoring that it might already be owned by wf2)
    for (auto const & t : turbines)
      t->farm = self;
    wind_turbines.insert(wind_turbines.end(), turbines.begin(), turbines.end());
  }

  std::shared_ptr<wind_turbine> wind_farm::create_wind_turbine(
    int id,
    std::string const & name,
    std::string const & json,
    geo_point const & location_,
    apoint_ts height_,
    t_xy_ power_curve_) {
    auto wt = std::make_shared<wind_turbine>(id, name, json, nullptr);
    wt->height = std::move(height_);
    wt->power_curve = std::move(power_curve_);
    wt->location = location_;
    add_wind_turbines({wt});
    return wt;
  }

}
