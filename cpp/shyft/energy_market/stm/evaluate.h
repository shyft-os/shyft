#pragma once

#include <memory>
#include <ranges>
#include <set>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include <boost/hof/unpack.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/functional.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/dd/is_cyclic.h>
#include <shyft/time_series/dd/resolve_ts.h>

namespace shyft::energy_market::stm {

  template <concepts::stm_system M>
  auto with_reference_ts(
    std::string_view model_id,
    M &&model,
    std::shared_ptr<time_series::dd::ipoint_ts const> const &ts,
    std::invocable<qual_like_t<time_series::dd::aref_ts, M &&>> auto &&pre_func,
    std::invocable<qual_like_t<time_series::dd::aref_ts, M &&>> auto &&post_func) {
    time_series::dd::traverse_ts<std::monostate>(ts, [&](auto &&event) {
      using node = time_series::dd::traverse_node_t<const std::shared_ptr<const time_series::dd::ipoint_ts> &>;
      using enum time_series::dd::traverse_event_tag;
      using event_type=std::decay_t<decltype(event)>;
      if constexpr (event_type::tag == pre)
        return time_series::dd::with_operation(
          *event.ts,
          [&]<typename T>(T &&ts) {
            if constexpr (std::is_same_v<std::remove_cvref_t<T>, time_series::dd::aref_ts>) {
              std::invoke(pre_func, ts);

              if (stm::url_peek_model_id(ts.id) == model_id) {
                auto ts_target = stm::url_invoke(
                  model,
                  std::string_view{ts.id},
                  [&]<typename A>(A &&attr) -> std::shared_ptr<const time_series::dd::ipoint_ts> {
                    if constexpr (std::is_same_v<std::remove_cvref_t<A>, time_series::dd::apoint_ts>)
                      return attr.ts;
                    else if constexpr (std::is_same_v<std::remove_cvref_t<A>, any_attr>)
                      if (auto ts = std::get_if<time_series::dd::apoint_ts>(&attr))
                        return ts->ts;
                    throw std::runtime_error("invalid url");
                  },
                  [&]() -> std::shared_ptr<const time_series::dd::ipoint_ts> {
                    throw std::runtime_error("invalid url");
                  });
                return time_series::dd::any_traverse_jump<std::monostate, node>(ts_target);
              }
            }
            return time_series::dd::any_traverse_cont<std::monostate, node>;
          },
          [] {
            return time_series::dd::any_traverse_cont<std::monostate, node>;
          });
      else if constexpr (event_type::tag == post) {
        time_series::dd::with_operation(
          *event.ts,
          [&]<typename T>(T &&ts) {
            if constexpr (std::is_same_v<std::remove_cvref_t<T>, time_series::dd::aref_ts>)
              std::invoke(post_func, ts);
          },
          ignore);
        return time_series::dd::traverse_cont{};
      } else
        return time_series::dd::traverse_ret<std::monostate>{};
    });
  }

  struct evaluate_ts_error {
    std::string what;
    SHYFT_DEFINE_STRUCT(evaluate_ts_error, (), (what))

    auto operator<=>(evaluate_ts_error const &) const = default;

    static auto cyclic_expression(std::string_view ts_as_str) {
      return evaluate_ts_error(fmt::format("ts {} contains a cyclic expression", ts_as_str));
    }

    static auto dstm_not_resolved(std::string_view ts_as_str) {
      return evaluate_ts_error(fmt::format("dstm timeseries {} did not resolve", ts_as_str));
    }

    static auto dstm_not_resolved(std::string_view ts_as_str, std::string_view err) {
      return evaluate_ts_error(fmt::format("dstm timeseries {} did not resolve with error: {}", ts_as_str, err));
    }

    static auto dtss_not_resolved(std::string_view url, std::string_view err) {
      return evaluate_ts_error(fmt::format("dtss timeseries with url {} did not resolve with error: {}", url, err));
    }

    auto &extend_dtss_not_resolved(std::string_view url, std::string_view err) {
      what.append(fmt::format(", dtss timeseries with url {} did not resolve with error: {}", url, err));
      return *this;
    }

    template <typename A>
    friend void serialize(A &archive, evaluate_ts_error &m, unsigned int const v) {
      reflection::serialize(archive, m, v);
    }
  };

  template <typename T>
  inline constexpr bool is_evaluate_ts_error_v = std::is_same_v<std::remove_cvref_t<T>, evaluate_ts_error>;

  using evaluate_ts_result = std::variant<time_series::dd::apoint_ts, evaluate_ts_error>;

  template <typename T>
  requires std::same_as<std::remove_cvref_t<T>, evaluate_ts_result>
  constexpr auto &&evaluate_ts_get(T &&t) {
    auto p = std::get_if<time_series::dd::apoint_ts>(&t);
    if (!p)
      throw std::runtime_error(std::get<evaluate_ts_error>(SHYFT_FWD(t)).what);
    return std::forward<qual_like_t<time_series::dd::apoint_ts, T &&>>(*p);
  }

  namespace detail {

    /**
     * @brief The implementation of resolving expression trees with sym-refs
     * @details
     *
     * Traverse the expression, collect dtss shyft refs that needs resolution.
     * The dtss shyft refs always resolves to a concrete point ts with values,- or error.
     * The dstm refs that needs resolution, might point to a concrete ts, or
     * it might point to an expression with new sym-refs.
     * This object is central part of the `evaluate_ts_impl` algorithm,
     * providing the sym-ref resolver.
     *
     * @note
     * `sym-refs` is aref_ts type keeping an id:str, and a  `rep` gpoint_ts const ptr(might be null)
     * We only resolve/bind those that have no `rep`
     * We keep cache of already resolved, so we can work efficiently with
     * expressions that references the same sym-ref,
     * - it will only be read, or computed once.
     *
     * @tparam clone_dstm
     *  If true, do clone the dstm symbol references,
     *  otherwise modify the inplace expressions.
     *
     * @tparam GetAttr
     *  A callable that transforms a dstm symbol ref to the ts it represents
     */
    template <bool clone_dstm, typename GetAttr>
    struct evaluate_ts_impl_resolver {

      struct ts_hash {
        using is_transparent = void;

        template <typename T>
        auto operator()(T const &t) const {
          return std::hash<T>{}(t);
        }
      };

      // keep ref. to dtss time-series by ptr to its symref payload,  aref_ts{.id,.rep}
      using ts_ptr = std::shared_ptr<time_series::dd::ipoint_ts const> const *;
      using deferred_dtss_resolves_type = std::unordered_map<
        std::string,
        std::vector<std::pair<ts_ptr, std::variant<apoint_ts, evaluate_ts_error> *>>,
        ts_hash,
        std::equal_to<>>;

      // keep ref. to dstm expressions by a sep shared pointer, that might be a clone of orig expr.
      using cached_dstm_resolves_type =
        std::unordered_map<std::string, std::shared_ptr<time_series::dd::ipoint_ts const>, ts_hash, std::equal_to<>>;

      GetAttr get_attr;                                     ///< how to transform dstm url to a ts
      deferred_dtss_resolves_type deferred_dtss_resolves{}; ///< the dtss resolved sym refs
      cached_dstm_resolves_type cached_dstm_resolves{};     ///< the dstm resolved sym refs

      void defer_dtss_resolve(
        std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url,
        evaluate_ts_result *res) { // notice: only resolve once, but keep track of all refs.
        auto it = deferred_dtss_resolves.find(url);
        if (it == deferred_dtss_resolves.end()) {
          deferred_dtss_resolves.insert({std::string(url), {{std::addressof(ref), res}}});
        } else {
          it->second.push_back({std::addressof(ref), res});
        }
      }

      auto get_ts_or_throw(std::string_view url) { // e.g. the dstm ref urls in the model
        auto result = get_attr(url);               // this is how we map url to ts or error if not found
        auto visitor = []<typename T>(T &&v) -> std::shared_ptr<time_series::dd::ipoint_ts const> {
          if constexpr (shyft::energy_market::stm::is_url_resolve_error_v<T>) {
            throw std::runtime_error(v.what);
          } else {
            static_assert(shyft::energy_market::stm::is_any_attr_v<T>);
            return std::get<time_series::dd::apoint_ts>(v).ts;
          }
        };
        return std::visit(visitor, result);
      }

      std::shared_ptr<time_series::dd::ipoint_ts const> *try_dstm_resolve(
        [[maybe_unused]] std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url) {
        if (stm::url_peek_model_id(url).empty())
          return nullptr;
        if (auto already_resolved = cached_dstm_resolves.find(url); already_resolved != cached_dstm_resolves.end())
          return std::addressof(already_resolved->second);
        auto result = get_ts_or_throw(url);
        if constexpr (clone_dstm) {
          if (result && result->needs_bind())
            result = result->clone_expr();
        }
        return std::addressof(cached_dstm_resolves.emplace(url, result).first->second);
      }

      using ignore_dtss_urls_t = std::false_type;
      using defer_dtss_urls_t = std::true_type;
      static constexpr ignore_dtss_urls_t ignore_dtss_urls{};
      static constexpr defer_dtss_urls_t defer_dtss_urls{};

      auto operator()(
        ignore_dtss_urls_t,
        std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url) -> std::shared_ptr<time_series::dd::ipoint_ts const> {
        if (stm::url_peek_model_id(url).empty() || !ref->needs_bind())
          return nullptr;
        if (auto resolved_dstm_ref = try_dstm_resolve(ref, url))
          return *resolved_dstm_ref;
        return ref;
      }

      auto const &operator()(
        defer_dtss_urls_t,
        evaluate_ts_result *res,
        std::shared_ptr<time_series::dd::ipoint_ts const> const &ref,
        std::string_view url) {
        if (!ref->needs_bind())
          return ref;
        if (auto resolved_dstm_ref = try_dstm_resolve(ref, url))
          return *resolved_dstm_ref;
        defer_dtss_resolve(ref, url, res);
        return ref;
      }
    };

    template <bool clone_dstm>
    auto evaluate_ts_impl(
      auto &&dtss,
      auto &&get_attr,
      time_series::dd::ats_vector &tsv,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {

      // FIXME: group tsv, ..., clip_period params into a dtss-query struct or similar - jeh
      std::vector<evaluate_ts_result> result(tsv.size(), time_series::dd::apoint_ts{});
      auto tsv_and_res = std::views::zip(tsv, result);
      evaluate_ts_impl_resolver<clone_dstm, decltype(get_attr)> resolver{.get_attr = SHYFT_FWD(get_attr)};

      for (auto &&[ts, res] : tsv_and_res) {
        try {
          if (time_series::dd::is_cyclic(std::bind_front(std::ref(resolver), resolver.ignore_dtss_urls), ts)) {
            res = evaluate_ts_error::cyclic_expression(ts.stringify());
            continue;
          }
          if (!time_series::dd::resolve_ts(std::bind_front(std::ref(resolver), resolver.defer_dtss_urls, &res), ts)) {
            res = evaluate_ts_error::dstm_not_resolved(ts.stringify());
          }
        } catch (std::runtime_error const &e) {
          res = evaluate_ts_error::dstm_not_resolved(ts.stringify(), e.what());
        }
      }

      std::vector<std::string> deferred_dtss_urls(resolver.deferred_dtss_resolves.size());
      std::ranges::copy(std::views::keys(resolver.deferred_dtss_resolves), deferred_dtss_urls.data());
      auto &&[dtss_tsv, dtss_tsv_tp, dtss_errs] = dtss(
        deferred_dtss_urls, bind_period, use_ts_cached_read, update_ts_cache);

      std::ranges::for_each(
        std::views::filter(
          std::views::zip(dtss_tsv, std::views::values(resolver.deferred_dtss_resolves)),
          boost::hof::unpack([&](auto &&rep, auto &&) -> bool {
            return rep != nullptr;
          })),
        boost::hof::unpack([&](auto &&rep, auto &&deferred_ts) {
          std::ranges::fill(
            std::views::transform(
              std::views::elements<0>(deferred_ts),
              [&](auto &&deferred_ref) -> auto & {
                return dynamic_cast<time_series::dd::aref_ts &>(
                         const_cast<time_series::dd::ipoint_ts &>(**deferred_ref))
                  .rep;
              }),
            rep);
        }));

      std::ranges::fold_left(
        dtss_errs,
        std::make_tuple(std::ranges::begin(resolver.deferred_dtss_resolves), std::size_t{0}),
        [&](auto deferred_dtss_resolve, auto const &error) {
          auto [deferred_dtss_resolve_it, last_error_index] = deferred_dtss_resolve;
          std::ranges::advance(deferred_dtss_resolve_it, error.index - last_error_index);
          auto &&[deferred_url, deferred_dtss_resolves] = *deferred_dtss_resolve_it;
          std::ranges::for_each(std::views::elements<1>(deferred_dtss_resolves), [&](auto deferred_result_ptr) {
            if (auto err = std::get_if<evaluate_ts_error>(deferred_result_ptr))
              err->extend_dtss_not_resolved(deferred_url, fmt::format("{}", error.code));
            else
              (*deferred_result_ptr) = evaluate_ts_error::dtss_not_resolved(
                deferred_url, fmt::format("{}", error.code));
          });
          return std::make_tuple(deferred_dtss_resolve_it, error.index);
        });

      auto valid_tsv_and_res = std::views::filter(tsv_and_res, [&](auto &&v) {
        auto &&[_1, res] = v;
        return std::holds_alternative<time_series::dd::apoint_ts>(res);
      });

      auto valid_inputs = std::views::elements<0>(valid_tsv_and_res);

      std::ranges::for_each(valid_inputs, &time_series::dd::apoint_ts::do_bind);
      time_series::dd::ats_vector tsv_results{
        time_series::dd::deflate_ts_vector<time_series::dd::apoint_ts>(valid_inputs)};
      if (clip_period.valid())
        tsv_results = clip_to_period(tsv_results, clip_period);

      std::ranges::move(
        tsv_results, std::ranges::begin(std::views::elements<1>(std::ranges::ref_view{valid_tsv_and_res})));
      return result;
    }

  }

  /**
   * @brief Evaluate time series expressions.
   * @param dtss a dtss resolver
   * @param model the dstm model that might be referred to in expressions
   * @param tsv the time series to evaluate, is mutated by this function
   * @param bind_period the period to read from dtss
   * @param use_ts_cached_read allow reading results from already existing cached results
   * @param update_ts_cache when reading, also update the ts-cache with the results
   * @param clip_period the period to clip the results against
   * @returns vector of evaluated time series
   */
  template <bool clone_dstm = true>
  auto evaluate_ts(
    auto &&dtss,
    stm_system const &model,
    time_series::dd::ats_vector &tsv,
    utcperiod bind_period,
    bool use_ts_cached_read = true,
    bool update_ts_cache = true,
    utcperiod clip_period = {}) {

    return detail::evaluate_ts_impl<clone_dstm>(
      SHYFT_FWD(dtss),
      [&](std::string_view url) {
        return url_try_get_attr(model, url);
      },
      tsv,
      bind_period,
      use_ts_cached_read,
      update_ts_cache,
      clip_period);
  }

  /**
   * @brief Evaluate time series expressions.
   * @param dtss a dtss resolver
   * @param models the dstm models that might be referred to in expressions
   * @param tsv the time series to evaluate, is mutated by this function
   * @param bind_period the period to read from dtss
   * @param use_ts_cached_read allow reading results from already existing cached results
   * @param update_ts_cache when reading, also update the ts-cache with the results
   * @param clip_period the period to clip the results against
   * @returns vector of evaluated time series
   */
  template <bool clone_dstm = true, typename Executor>
  auto evaluate_ts(
    auto &&dtss,
    shared_models<Executor> &models,
    time_series::dd::ats_vector &tsv,
    utcperiod bind_period,
    bool use_ts_cached_read = true,
    bool update_ts_cache = true,
    utcperiod clip_period = {}) {
    return detail::evaluate_ts_impl<clone_dstm>(
      SHYFT_FWD(dtss),
      [&](std::string_view url) {
        return get_attrs(models, std::views::single(url)).at(0);
      },
      tsv,
      bind_period,
      use_ts_cached_read,
      update_ts_cache,
      clip_period);
  }

  /**
   * @brief rebind_expression
   * @details
   *
   * This routine traverses all time-series of the locked and mutable stm_system mdl,
   * its purpose is to be used after optimization/computation have updated the result terminals
   * of the stm_system. In that case, any expressions refering to those terminals needs to be
   * properly recomputed/reconfigured for computation.
   *
   * The algorithm outline:
   * for each ts of the model, if it's an expression,
   *   find all the terminal nodes of the expression
   *     and save their bound time-series
   *   then IF there was 1 or more dstm://refs,
   *        unbind the expression (will wipe out bound entities)
   *        then put into place the saved bound time-series
   *        finally, re-evaluate/prepare the expression.
   *
   * @param mdl a locked exclusive mode stm_system
   * @param new_mkey the dstm m_key for the stm_system
   * @param nan_fill_missing if true, then replace empty/unbound terminals with nan ts using mdl. run_time_axis
   * @return true if any rebind occured (e.g. there was at least one dtstm://new_mkey/ ref. in the expressions)
   */
  bool rebind_expression(stm_system &mdl, std::string_view new_mkey, bool nan_fill_missing = false);

}

template <typename Char>
struct fmt::formatter<shyft::energy_market::stm::evaluate_ts_error, Char>
  : shyft::reflection::struct_formatter<shyft::energy_market::stm::evaluate_ts_error, Char> { };
