#pragma once
#include <algorithm>
#include <memory>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

#include <boost/hof/lift.hpp>
#include <boost/hof/unpack.hpp>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/shop/api/shop_api.h>
#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/energy_market/stm/shop/shop_init.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <shop_lib_interface.h>

namespace shyft::energy_market::stm::shop {
  template <typename T>
  constexpr auto type_str = [] {
    if constexpr (std::is_same_v<T, int>)
      return "int";
    if constexpr (std::is_same_v<T, double>)
      return "double";
    return "unknown";
  }();

  constexpr std::vector<std::time_t> get_shop_axis(time_axis::generic_dt const & dt) {
    if (dt.gt() != time_axis::generic_dt::POINT)
      return get_shop_axis(time_axis::generic_dt{time_axis::convert_to_point_dt(dt)});
    auto& pt = dt.p();
    std::vector<std::time_t> t_axis(pt.size() + 1);
    std::ranges::transform(pt.t, t_axis.data(), core::to_seconds64);
    t_axis.back() = core::to_seconds64(pt.t_end);
    return t_axis;
  }

  struct api {
    std::unique_ptr<ShopSystem, ShopSystemDeleter> c;
    std::vector<std::time_t> time_axis;
    bool time_axis_defined{false}; // a lot of stuff is not possible (like in crash), unless time-axis is set first

    api(std::string_view license_path = "");
    std::string get_version_info() const;

    void set_library_path(std::string_view path);
    void set_logging_to_stdstreams(bool on = true);
    void set_logging_to_files(bool on = true);

    std::vector<stm::log_entry> get_log_buffer(std::size_t limit = 1024);

    void set_time_axis(std::time_t t_begin, std::time_t t_end, std::time_t t_step);
    void set_time_axis(std::vector<std::time_t> const & t_axis);

    // TODO: assume all string_views are null-term, document
    void execute_cmd(std::string_view cmd, std::string_view opt = "", std::string_view obj = "");
    void execute_cmd(std::string_view cmd, std::vector<std::string_view> opt, std::vector<std::string_view> obj = {});
    void execute_cmd_string(std::string_view cmd);
    std::vector<std::string> get_executed_cmd_strings() const;

    void start_sim(std::string_view o);

    std::string dump_yaml(
      bool input_only = false,
      bool output_only = false,
      bool compress_txy = false,
      bool compress_connection = false,
      bool escape_ascii = true) const;

    /** for diagnostics */
    std::string obj_ref_str(int oid) const;
    std::string attr_ref_str(int oid, int aid) const;

    template <class T>
    T create(std::string_view name) {
      auto oid = create_object(T::t_id, name);
      return T{this, oid};
    }

    template <class T>
    T get(std::string_view name) const {
      auto oid = get_object(T::t_id, name);
      // FIXME: ugly
      return T{const_cast<shyft::energy_market::stm::shop::api*>(this), oid};
    }

    auto global_settings() const {
      return get<::shop::global_settings<api>>("global_settings");
    }

    auto average_objective() const {
      return get<::shop::objective<api>>("average_objective");
    }

    int create_object(int otype, std::string_view name);

    int get_object(int otype, std::string_view name) const;

    template <typename T>
    void connect_objects(T const & o1, typename T::relation r, int oid2) {
      if (!ShopAddRelation(c.get(), o1.id, T::relation_name(r), oid2))
        throw std::runtime_error(fmt::format(
          "failed to connect object {} and {} with role {}",
          obj_ref_str(o1.id),
          obj_ref_str(oid2),
          T::relation_name(r)));
    }

    bool exists(int oid, int aid, auto /*_x0*/ = nullptr) const {
      return ShopAttributeExists(c.get(), oid, aid);
    }

    bool is_default(int oid, int aid, auto /*_x0*/ = nullptr) const {
      return ShopAttributeIsDefault(c.get(), oid, aid);
    }

    template <class Ux, class Uy>
    int get(int oid, int aid, int* = nullptr) const {
      int r = std::numeric_limits<int>::min();
      if (!ShopGetIntAttribute(c.get(), oid, aid, r))
        throw std::runtime_error(fmt::format("int attr.get failed for: {}", attr_ref_str(oid, aid)));
      return Uy::to_base(r);
    }

    template <class Ux, class Uy>
    void set(int oid, int aid, int v, auto /*set_policy*/) {
      if (!ShopSetIntAttribute(c.get(), oid, aid, Uy::from_base(v)))
        throw std::runtime_error(fmt::format("int attr.set failed for: {}", attr_ref_str(oid, aid)));
    }

    template <class Ux, class Uy>
    double get(int oid, int aid, double* = nullptr) const {
      double r = 0;
      if (!ShopGetDoubleAttribute(c.get(), oid, aid, r))
        throw std::runtime_error(fmt::format("double attr.get failed for: {}", attr_ref_str(oid, aid)));
      return Uy::to_base(r);
    }

    template <class Ux, class Uy>
    void set(int oid, int aid, double v, auto /*set_policy*/) {
      if (!ShopSetDoubleAttribute(c.get(), oid, aid, Uy::from_base(v)))
        throw std::runtime_error(fmt::format("double attr.set failed for: {}", attr_ref_str(oid, aid)));
    }

    template <class Ux, class Uy>
    std::string get(int oid, int aid, std::string_view* = nullptr) const {
      std::string r(::shop::data::max_attribute_size, '\0');
      if (!ShopGetStringAttribute(c.get(), oid, aid, r))
        throw std::runtime_error(fmt::format("string attr.get failed for: {}", attr_ref_str(oid, aid)));
      if (auto i = r.find_first_of('\0'); i != std::string::npos)
        r.resize(i);
      return r;
    }

    template <class Ux, class Uy>
    void set(int oid, int aid, std::string_view v, auto /*set_policy*/) {
      if (!ShopSetStringAttribute(c.get(), oid, aid, std::string(v).c_str()))
        throw std::runtime_error(fmt::format("string attr.set failed for: {}", attr_ref_str(oid, aid)));
    }

    template <class Ux, class Uy, class T>
    requires std::is_same_v<T, int> || std::is_same_v<T, double>
    std::vector<T> get(int oid, int aid, std::vector<T>* = nullptr) {
      int n{0};
      if (!ShopGetDoubleArrayLength(c.get(), oid, aid, n))
        throw std::runtime_error(
          fmt::format("std::vector<{}> attr.get n failed for: {}", type_str<T>, attr_ref_str(oid, aid)));
      std::vector<T> r(n);
      if constexpr (std::is_same_v<T, int>) {
        if (!ShopGetIntArrayAttribute(c.get(), oid, aid, n, r.data()))
          throw std::runtime_error(fmt::format("std::vector<int> attr.get failed for: ", attr_ref_str(oid, aid)));
      } else if (!ShopGetDoubleArrayAttribute(c.get(), oid, aid, n, r.data()))
        throw std::runtime_error(fmt::format("std::vector<double> attr.get failed for: {}", attr_ref_str(oid, aid)));
      std::ranges::transform(r, std::ranges::begin(r), BOOST_HOF_LIFT(Uy::to_base));
      return r;
    }

    template <class Ux, class Uy, class T>
    requires std::is_same_v<T, int> || std::is_same_v<T, double>
    void set(int oid, int aid, std::vector<T> const & v, auto /*set_policy*/) {
      if (v.size() > std::numeric_limits<int>::max())
        throw std::runtime_error(
          fmt::format("std::vector<{}> attr.set failed for: {}", type_str<T>, attr_ref_str(oid, aid)));
      int n = static_cast<int>(v.size());
      auto u = [&] {
        if constexpr (Uy::is_base)
          return std::span{v};
        else {
          auto vs = v;
          std::ranges::transform(v, vs.begin(), BOOST_HOF_LIFT(Uy::from_base));
          return vs;
        }
      }();
      if constexpr (std::is_same_v<T, int>) {
        if (!ShopSetIntArrayAttribute(c.get(), oid, aid, n, u.data()))
          throw std::runtime_error(fmt::format("std::vector<int> attr.set failed for: {}", attr_ref_str(oid, aid)));
      }
      if constexpr (std::is_same_v<T, double>) {
        if (!ShopSetDoubleArrayAttribute(c.get(), oid, aid, n, u.data()))
          throw std::runtime_error(fmt::format("std::vector<double> attr.set failed for: {}", attr_ref_str(oid, aid)));
      }
    }

    template <class Ux, class Uy>
    auto get(int oid, int aid, hydro_power::xy_point_curve_with_z* = nullptr) const {
      int n;
      if (!ShopGetXyAttributeNPoints(c.get(), oid, aid, n))
        throw std::runtime_error(fmt::format("xy attr.get failed for: {}", attr_ref_str(oid, aid)));
      if (n == 0)
        return hydro_power::xy_point_curve_with_z{};
      std::vector<double> x(n), y(n);
      double ref;
      if (!ShopGetXyAttribute(c.get(), oid, aid, ref, n, x.data(), y.data()))
        throw std::runtime_error(fmt::format("xy attr.get failed for: {}", attr_ref_str(oid, aid)));
      return to_curve<Ux, Uy>(ref, x, y);
    }

    template <class Ux, class Uy>
    void set(int oid, int aid, hydro_power::xy_point_curve_with_z const & v, auto /*set_policy*/) {
      auto [x, y] = from_curve<Ux, Uy>(v);
      if (x.size() == 0)
        throw std::runtime_error(fmt::format("xy attr.set failed for: {} source data empty", attr_ref_str(oid, aid)));
      if (!ShopSetXyAttribute(c.get(), oid, aid, v.z, x.size(), x.data(), y.data()))
        throw std::runtime_error(fmt::format("xy attr.set failed for: {}", attr_ref_str(oid, aid)));
    }

    template <class Ux, class Uy>
    auto get(int oid, int aid, std::vector<hydro_power::xy_point_curve_with_z>* = nullptr) const {
      int n_curves, n_points;
      if (!ShopGetXyArrayDimensions(c.get(), oid, aid, n_curves, n_points))
        throw std::runtime_error(fmt::format("std::vector<xy> attr.get n failed for: {}", attr_ref_str(oid, aid)));
      if (n_curves == 0 || n_points == 0)
        return std::vector<hydro_power::xy_point_curve_with_z>{};
      std::vector<double> ref(n_curves);
      std::vector<int> n_pts(n_curves);
      std::vector<std::vector<double>> ax(n_curves, std::vector<double>(n_points)),
        ay(n_curves, std::vector<double>(n_points));
      std::vector<double*> vx(n_curves), vy(n_curves);
      {
        auto target = std::views::zip(vx, vy);
        std::ranges::transform(
          std::views::zip(ax, ay), std::ranges::begin(target), boost::hof::unpack([](auto&& x_vector, auto&& y_vector) {
            return std::make_pair(x_vector.data(), y_vector.data());
          }));
      }
      if (!ShopGetXyArrayAttribute(
            c.get(), oid, aid, static_cast<int>(n_curves), ref.data(), n_pts.data(), vx.data(), vy.data()))
        throw std::runtime_error(fmt::format("std::vector<xy> attr.get failed for: {}", attr_ref_str(oid, aid)));
      std::vector<hydro_power::xy_point_curve_with_z> result(n_curves);
      std::ranges::transform(
        std::views::zip(ref, n_pts, ax, ay),
        result.data(),
        boost::hof::unpack([](auto&& r, auto&& n, auto&& x, auto&& y) {
          x.resize(n);
          y.resize(n);
          return to_curve<Ux, Uy>(r, std::move(x), std::move(y));
        }));
      return result;
    }

    template <class Ux, class Uy>
    void set(int oid, int aid, std::vector<hydro_power::xy_point_curve_with_z> const & v, auto /*set_policy*/) {
      if (v.size() > std::numeric_limits<int>::max())
        throw std::runtime_error(fmt::format("std::vector<xy> attr.set failed for: {}", attr_ref_str(oid, aid)));
      int n_curves = static_cast<int>(v.size());
      if (n_curves == 0)
        throw std::runtime_error(
          fmt::format("std::vector<xy> attr.set failed for: {} source data empty", attr_ref_str(oid, aid)));
      std::vector<double> ref(n_curves);
      std::vector<int> n_pts(n_curves);

      std::vector<std::vector<double>> ax(n_curves), ay(n_curves);
      std::vector<double*> vx(n_curves), vy(n_curves);
      {
        auto target = std::views::zip(ref, n_pts, ax, vx, ay, vy);
        std::ranges::transform(v, std::ranges::begin(target), [](auto const & curve) {
          auto [x, y] = from_curve<Ux, Uy>(curve);
          auto n = x.size();
          return std::make_tuple(curve.z, n, std::move(x), x.data(), std::move(y), y.data());
        });
      }
      if (!ShopSetXyArrayAttribute(c.get(), oid, aid, n_curves, ref.data(), n_pts.data(), vx.data(), vy.data()))
        throw std::runtime_error(fmt::format("std::vector<xy> attr.set failed for: {}", attr_ref_str(oid, aid)));
    }

    template <class Ux, class Uy>
    auto get(int oid, int aid, std::map<std::time_t, hydro_power::xy_point_curve_with_z>* = nullptr) const {
      int n_curves{0};
      if (!ShopGetXytNTimes(c.get(), oid, aid, n_curves))
        throw std::runtime_error(fmt::format("xyt attr.get n failed for: {}", attr_ref_str(oid, aid)));
      if (n_curves > std::ranges::ssize(time_axis))
        throw std::runtime_error(fmt::format("xyt attr.get n>ta failed for: {}", attr_ref_str(oid, aid)));
      if (n_curves == 0)
        return std::map<std::time_t, hydro_power::xy_point_curve_with_z>{};
      std::vector<int> t_curves(n_curves);
      if (!ShopGetXytTimesIntArray(c.get(), oid, aid, n_curves, t_curves.data()))
        throw std::runtime_error(fmt::format("xyt attr.get t failed for: {}", attr_ref_str(oid, aid)));
      std::map<std::time_t, hydro_power::xy_point_curve_with_z> result;
      std::ranges::transform(
        std::views::zip(t_curves, time_axis), std::inserter(result, result.begin()), [&](auto&& pair) {
          auto const& [t, t_axis] = pair;
          int n_points{0};
          if (!ShopGetXytAttributeNPoints(c.get(), oid, aid, n_points, t))
            throw std::runtime_error(fmt::format("xyt attr.get np failed for: {}", attr_ref_str(oid, aid)));
          if (n_points == 0) {
            return std::make_pair<std::time_t, hydro_power::xy_point_curve_with_z>(
              static_cast<std::time_t>(t_axis), {});
          }
          double z;
          std::vector<double> x(n_points), y(n_points);
          if (!ShopGetXytAttribute(c.get(), oid, aid, z, n_points, x.data(), y.data(), t))
            throw std::runtime_error(fmt::format("xyt attr.get failed for: {}", attr_ref_str(oid, aid)));
          return std::make_pair(static_cast<std::time_t>(t_axis), to_curve<Ux, Uy>(z, x, y));
        });
      return result;
    }

    template <class Ux, class Uy>
    void
      set(int oid, int aid, std::map<std::time_t, hydro_power::xy_point_curve_with_z> const & v, auto /*set_policy*/) {
      if (v.size() > std::numeric_limits<int>::max())
        throw std::runtime_error(fmt::format("xyt attr.set failed for: {}", attr_ref_str(oid, aid)));
      int n_t_curves = std::ranges::size(v);
      if (n_t_curves <= 0)
        throw std::runtime_error(fmt::format("xyt attr.set failed for: {}", attr_ref_str(oid, aid)));

      std::vector<int> t, n_pts;
      t.reserve(n_t_curves);
      n_pts.reserve(n_t_curves);
      std::vector<std::vector<double>> a_x, a_y;
      a_x.reserve(n_t_curves);
      a_y.reserve(n_t_curves);
      std::vector<double*> v_x, v_y;
      v_x.reserve(n_t_curves);
      v_y.reserve(n_t_curves);
      auto add_curve = [&](auto&& x, auto&& y) {
        n_pts.push_back(x.size());
        a_x.push_back(std::move(x));
        v_x.push_back(a_x.back().data());
        a_y.push_back(std::move(y));
        v_y.push_back(a_y.back().data());
      };
      auto t_itr = std::ranges::begin(time_axis);
      std::ranges::for_each(v, boost::hof::unpack([&](auto const & t_v, auto const & curve) {
                              t_itr = std::ranges::find(t_itr, std::ranges::end(time_axis), t_v);
                              if (t_itr == std::ranges::end(time_axis))
                                return;
                              t.push_back(std::ranges::distance(time_axis.begin(), t_itr));
                              auto [x, y] = from_curve<Ux, Uy>(curve);
                              add_curve(std::move(x), std::move(y));
                            }));
      if (!ShopSetXytAttribute(c.get(), oid, aid, n_pts.size(), t.data(), n_pts.data(), v_x.data(), v_y.data()))
        throw std::runtime_error(fmt::format("xyt attr.set failed for: {}", attr_ref_str(oid, aid)));
    }

    template <class Ux, class Uy>
    time_series::dd::apoint_ts get(int oid, int aid, time_series::dd::apoint_ts* = nullptr) const {
      int n{0};
      int n_scen{0};
      ::shop::data::shop_time start_time(0);
      if (!ShopGetTxyAttributeDimensions(c.get(), oid, aid, start_time, n, n_scen))
        throw std::runtime_error(fmt::format("txy attr.get n failed for: {}", attr_ref_str(oid, aid)));
      std::vector<int> t(n);
      std::vector<double> y(n);
      auto yv = y.data();
      if (!ShopGetTxyAttribute(c.get(), oid, aid, n, n_scen, t.data(), &yv))
        throw std::runtime_error(fmt::format("txy attr.get failed for: {}", attr_ref_str(oid, aid)));
      auto time_unit = ::shop::data::shop_time_resolution_unit;
      auto unit_str = ShopGetTxyAttributeTimeUnit(c.get(), oid, aid);
      try {
        time_unit = ::shop::data::shop_time_unit::to_time_t(unit_str);
      } catch (std::runtime_error const &) {
        throw std::runtime_error(fmt::format(
          "txy ShopGetTxyAttributeTimeUnit returned invalid unit {} for : {}", unit_str, attr_ref_str(oid, aid)));
      }
      return to_ts<Uy>(time_axis, t, y, time_unit);
    }

    template <class Ux, class Uy>
    void set(int oid, int aid, time_series::dd::apoint_ts const & v, auto set_policy) {
      auto [start_time, t, y] = from_ts<Uy>(
        v, set_policy.clip_to_shop_period ? time_axis : get_shop_axis(v.time_axis()));
      if (t.empty())
        return;
      auto yp = y.data();
      if (!ShopSetTxyAttribute(c.get(), oid, aid, start_time, t.size(), 1, t.data(), &yp))
        throw std::runtime_error(fmt::format("txy attr.set failed for: ", attr_ref_str(oid, aid)));
    }
  };

  template <int T>
  using shop_object = ::shop::proxy::obj<api, T>;
  using shop_reservoir = ::shop::reservoir<api>;
  using shop_power_plant = ::shop::power_plant<api>;
  using shop_unit = ::shop::unit<api>;
  using shop_needle_combination = ::shop::needle_combination<api>;
  using shop_pump = ::shop::pump<api>;
  using shop_gate = ::shop::gate<api>;
  using shop_tunnel = ::shop::tunnel<api>;
  using shop_river = ::shop::river<api>;
  using shop_contract = ::shop::contract<api>;
  using shop_market = ::shop::market<api>;
  using shop_global_settings = ::shop::global_settings<api>;
  using shop_reserve_group = ::shop::reserve_group<api>;
  using shop_commit_group = ::shop::commit_group<api>;
  using shop_discharge_group = ::shop::discharge_group<api>;
  using shop_objective = ::shop::objective<api>;
  using shop_volume_constraint = ::shop::volume_constraint<api>;

}
