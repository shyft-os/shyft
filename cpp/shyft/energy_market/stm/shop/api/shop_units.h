#pragma once

namespace shop::proxy::unit {

  /**@brief shop::proxy:unit namespace
   *
   * Contains basic definition of value units used by shop attributes,
   * and functions for converting scaled units to and from its base,
   * which is generally the SI base unit.
   */
  struct exp10_scale {
    template <typename V>
    static constexpr V scale(V v, int e) {
      return e == 0 ? v : e < 0 ? static_cast<V>(1) / 10 * scale(v, e + 1) : static_cast<V>(10) * scale(v, e - 1);
    }
  };

  struct exp10_unit_exponent {
    static constexpr int nano = -9;
    static constexpr int micro = -6;
    static constexpr int milli = -3;
    static constexpr int base = 0;
    static constexpr int kilo = 3;
    static constexpr int mega = 6;
    static constexpr int giga = 9;
  };

  template <int exp = exp10_unit_exponent::base>
  struct exp10_scaled_unit {
    static constexpr bool is_base = exp == exp10_unit_exponent::base;

    template <typename V>
    static constexpr V to_base(V v) {
      return exp10_scale::scale(v, exp);
    };

    template <typename V>
    static constexpr V from_base(V v) {
      return exp10_scale::scale(v, -exp);
    };
  };

  template <int ratio = 1>
  struct fractional_unit {
    static constexpr bool is_base = ratio == 1;

    static constexpr auto to_base(auto v) {
      return v * ratio;
    };

    static constexpr auto from_base(auto v) {
      return v / ratio;
    };
  };

  using base_unit = exp10_scaled_unit<exp10_unit_exponent::base>;
  using mega_unit = exp10_scaled_unit<exp10_unit_exponent::mega>;

  using no_unit = base_unit;
  using invalid = base_unit; // There should not be any attributes with this unit, but sometimes there is because the
                             // real unit has not yet been added to shop
  using percent = base_unit;
  using meter = base_unit;
  using delta_meter = base_unit;
  using second = base_unit;
  using hour = base_unit;
  using minute = fractional_unit<60>; // new 15.2
  using nok = base_unit;
  using nok_per_h_per_m3_per_s = base_unit;
  using nok_per_m3_per_s = base_unit;
  using nok_per_meter = base_unit;
  using s2_per_m5 = base_unit;
  using m3_per_s = base_unit;
  using m3sec_hour = base_unit;
  using mwh_per_mm3 = base_unit;
  using m3sec_hour = base_unit;
  using meter_per_hour = base_unit;
  using nok_per_meter_hour = base_unit;

  using mw = mega_unit;
  using mm3 = mega_unit;
  using mm3_per_hour = mega_unit;
  using mwh = mega_unit;
  using mw_hour = mega_unit;

  using km2 = mega_unit;

  using kwh_per_mm3 = exp10_scaled_unit<exp10_unit_exponent::kilo - exp10_unit_exponent::mega>;
  using nok_per_mm3 = exp10_scaled_unit<-exp10_unit_exponent::mega>;
  using nok_per_mm3h = exp10_scaled_unit<-exp10_unit_exponent::mega>;
  using nok_per_mw = exp10_scaled_unit<-exp10_unit_exponent::mega>;
  using nok_per_mwh = exp10_scaled_unit<-exp10_unit_exponent::mega>;
}