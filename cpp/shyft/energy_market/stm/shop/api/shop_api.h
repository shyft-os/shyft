#pragma once
// auto genereated files based on active version of Sintef SHOP api dll
#include <shyft/energy_market/stm/shop/api/shop_proxy.h>
#include <shyft/energy_market/stm/shop/api/shop_units.h>

namespace shop {

  using proxy::obj;
  using proxy::rw;
  using proxy::ro;
  using namespace proxy::unit;

  template <class A>
  struct reservoir : obj<A, 0> {
    using super = obj<A, 0>;
    reservoir() = default;

    reservoir(A* s, int oid)
      : super(s, oid) {
    }

    reservoir(reservoir const & o)
      : super(o) {
    }

    reservoir(reservoir&& o) noexcept
      : super(std::move(o)) {
    }

    reservoir& operator=(reservoir const & o) {
      super::operator=(o);
      return *this;
    }

    reservoir& operator=(reservoir&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
      connection_spill,
      connection_bypass
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      case relation::connection_spill:
        return "connection_spill";
      case relation::connection_bypass:
        return "connection_bypass";
      default:
        return nullptr;
      }
    }

    ro<reservoir, 3, int, no_unit, no_unit> energy_value_converted{this};
    rw<reservoir, 4, double, no_unit, no_unit> latitude{this};
    rw<reservoir, 5, double, no_unit, no_unit> longitude{this};
    rw<reservoir, 6, double, mm3, mm3> max_vol{this};
    rw<reservoir, 7, double, meter, meter> lrl{this};
    rw<reservoir, 8, double, meter, meter> hrl{this};
    rw<reservoir, 9, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, mm3, meter> vol_head{this};
    rw<reservoir, 10, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, km2> head_area{this};
    rw<reservoir, 11, ::shyft::time_series::dd::apoint_ts, no_unit, meter> elevation_adjustment{this};
    rw<reservoir, 12, double, mm3, mm3> start_vol{this};
    rw<reservoir, 13, double, meter, meter> start_head{this};
    rw<reservoir, 14, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> inflow{this};
    rw<reservoir, 15, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> inflow_flag{this};
    rw<reservoir, 16, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> sim_inflow_flag{this};
    rw<reservoir, 17, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, m3_per_s> flow_descr{this};
    rw<reservoir, 18, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> overflow_mip_flag{this};
    rw<reservoir, 19, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> overflow_cost{this};
    rw<reservoir, 20, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> overflow_cost_flag{this};
    rw<reservoir, 21, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> min_vol_constr{this};
    rw<reservoir, 22, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_vol_constr_flag{this};
    rw<reservoir, 23, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> max_vol_constr{this};
    rw<reservoir, 24, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_vol_constr_flag{this};
    rw<reservoir, 25, ::shyft::time_series::dd::apoint_ts, no_unit, meter> min_head_constr{this};
    rw<reservoir, 26, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_head_constr_flag{this};
    rw<reservoir, 27, ::shyft::time_series::dd::apoint_ts, no_unit, meter> max_head_constr{this};
    rw<reservoir, 28, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_head_constr_flag{this};
    rw<reservoir, 29, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> tactical_limit_min{this};
    rw<reservoir, 30, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> tactical_limit_min_flag{this};
    rw<reservoir, 31, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3h> tactical_cost_min{this};
    rw<reservoir, 32, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> tactical_cost_min_flag{this};
    rw<reservoir, 33, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> tactical_limit_max{this};
    rw<reservoir, 34, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> tactical_limit_max_flag{this};
    rw<reservoir, 35, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3h> tactical_cost_max{this};
    rw<reservoir, 36, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> tactical_cost_max_flag{this};
    rw<reservoir, 37, ::shyft::time_series::dd::apoint_ts, no_unit, meter> tactical_level_limit_min{this};
    rw<reservoir, 38, ::shyft::time_series::dd::apoint_ts, no_unit, meter> tactical_level_limit_max{this};
    rw<reservoir, 39, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> upper_slack{this};
    rw<reservoir, 40, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> lower_slack{this};
    rw<reservoir, 41, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> schedule{this};
    rw<reservoir, 42, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> schedule_flag{this};
    rw<reservoir, 43, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> volume_schedule{this};
    rw<reservoir, 44, ::shyft::time_series::dd::apoint_ts, no_unit, meter> level_schedule{this};
    rw<reservoir, 45, ::shyft::time_series::dd::apoint_ts, no_unit, mm3_per_hour> volume_ramping_up{this};
    rw<reservoir, 46, ::shyft::time_series::dd::apoint_ts, no_unit, mm3_per_hour> volume_ramping_down{this};
    rw<reservoir, 47, ::shyft::time_series::dd::apoint_ts, no_unit, meter_per_hour> level_ramping_up{this};
    rw<reservoir, 48, ::shyft::time_series::dd::apoint_ts, no_unit, meter_per_hour> level_ramping_down{this};
    rw<reservoir, 49, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mm3, nok_per_mm3>
      water_value_input{this};
    rw<reservoir, 50, double, nok_per_mwh, nok_per_mwh> energy_value_input{this};
    rw<reservoir, 51, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, mm3, nok_per_mm3>
      peak_volume_cost_curve{this};
    rw<reservoir, 52, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, mm3, nok_per_mm3>
      flood_volume_cost_curve{this};
    rw<reservoir, 53, ::shyft::time_series::dd::apoint_ts, no_unit, meter_per_hour> evaporation_rate{this};
    rw<reservoir, 54, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_meter> level_rolling_ramping_penalty_cost{
      this};
    rw<reservoir, 55, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_meter> level_period_ramping_penalty_cost{
      this};
    rw<reservoir, 56, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      average_level_rolling_ramping_up{this};
    rw<reservoir, 57, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      average_level_rolling_ramping_down{this};
    rw<reservoir, 58, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      limit_level_rolling_ramping_up{this};
    rw<reservoir, 59, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      limit_level_rolling_ramping_down{this};
    rw<reservoir, 60, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      average_level_period_ramping_up{this};
    rw<reservoir, 61, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      average_level_period_ramping_down{this};
    rw<reservoir, 62, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      limit_level_period_ramping_up{this};
    rw<reservoir, 63, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      limit_level_period_ramping_down{this};
    rw<reservoir, 64, ::shyft::time_series::dd::apoint_ts, no_unit, meter> historical_level{this};
    //--TODO: rw<reservoir,65,sy,minute,minute> average_level_period_ramping_up_offset{this};
    //--TODO: rw<reservoir,66,sy,minute,minute> average_level_period_ramping_down_offset{this};
    //--TODO: rw<reservoir,67,sy,minute,minute> limit_level_period_ramping_up_offset{this};
    //--TODO: rw<reservoir,68,sy,minute,minute> limit_level_period_ramping_down_offset{this};
    rw<reservoir, 69, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, mm3>
      volume_nonseq_ramping_limit_up{this};
    rw<reservoir, 70, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, mm3>
      volume_nonseq_ramping_limit_down{this};
    rw<reservoir, 71, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, mm3>
      volume_amplitude_ramping_limit_up{this};
    rw<reservoir, 72, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, mm3>
      volume_amplitude_ramping_limit_down{this};
    rw<reservoir, 73, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      level_nonseq_ramping_limit_up{this};
    rw<reservoir, 74, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      level_nonseq_ramping_limit_down{this};
    rw<reservoir, 75, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      level_amplitude_ramping_limit_up{this};
    rw<reservoir, 76, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, meter>
      level_amplitude_ramping_limit_down{this};
    ro<reservoir, 77, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> storage{this};
    ro<reservoir, 78, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> sim_storage{this};
    ro<reservoir, 79, ::shyft::time_series::dd::apoint_ts, no_unit, meter> head{this};
    ro<reservoir, 80, ::shyft::time_series::dd::apoint_ts, no_unit, km2> area{this};
    ro<reservoir, 81, ::shyft::time_series::dd::apoint_ts, no_unit, meter> sim_head{this};
    ro<reservoir, 82, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_inflow{this};
    ro<reservoir, 83, ::shyft::time_series::dd::apoint_ts, mm3, mm3> endpoint_penalty{this};
    ro<reservoir, 84, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> penalty{this};
    ro<reservoir, 85, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> tactical_penalty_up{this};
    ro<reservoir, 86, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> tactical_penalty_down{this};
    ro<reservoir, 87, ::shyft::time_series::dd::apoint_ts, no_unit, nok> end_penalty{this};
    ro<reservoir, 88, ::shyft::time_series::dd::apoint_ts, no_unit, nok> penalty_nok{this};
    ro<reservoir, 89, ::shyft::time_series::dd::apoint_ts, no_unit, nok> tactical_penalty{this};
    ro<reservoir, 90, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> water_value_global_result{this};
    ro<reservoir, 91, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> water_value_local_result{this};
    ro<reservoir, 92, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> energy_value_local_result{this};
    ro<reservoir, 93, ::shyft::time_series::dd::apoint_ts, no_unit, nok> end_value{this};
    ro<reservoir, 94, ::shyft::time_series::dd::apoint_ts, no_unit, nok> change_in_end_value{this};
    ro<reservoir, 95, ::shyft::time_series::dd::apoint_ts, no_unit, nok> vow_in_transit{this};
    ro<reservoir, 96, double, nok_per_mm3, nok_per_mm3> calc_global_water_value{this};
    ro<reservoir, 97, double, mwh_per_mm3, mwh_per_mm3> energy_conversion_factor{this};
    ro<reservoir, 98, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mm3, nok_per_mm3>
      water_value_cut_result{this};
    ro<reservoir, 99, int, no_unit, no_unit> added_to_network{this};
    ro<reservoir, 100, int, no_unit, no_unit> network_no{this};
    ro<reservoir, 101, ::shyft::time_series::dd::apoint_ts, no_unit, nok> peak_volume_penalty{this};
    ro<reservoir, 102, ::shyft::time_series::dd::apoint_ts, no_unit, nok> flood_volume_penalty{this};
    ro<reservoir, 103, ::shyft::time_series::dd::apoint_ts, no_unit, meter> linearized_level{this};
  };

  template <class A>
  struct power_plant : obj<A, 1> {
    using super = obj<A, 1>;
    power_plant() = default;

    power_plant(A* s, int oid)
      : super(s, oid) {
    }

    power_plant(power_plant const & o)
      : super(o) {
    }

    power_plant(power_plant&& o) noexcept
      : super(std::move(o)) {
    }

    power_plant& operator=(power_plant const & o) {
      super::operator=(o);
      return *this;
    }

    power_plant& operator=(power_plant&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<power_plant, 108, int, no_unit, no_unit> num_gen{this};
    ro<power_plant, 109, int, no_unit, no_unit> num_pump{this};
    rw<power_plant, 112, double, no_unit, no_unit> less_distribution_eps{this};
    rw<power_plant, 119, double, no_unit, no_unit> latitude{this};
    rw<power_plant, 120, double, no_unit, no_unit> longitude{this};
    rw<power_plant, 121, double, no_unit, no_unit> power_head_optimization_factor{this};
    rw<power_plant, 122, double, percent, percent> ownership{this};
    rw<power_plant, 123, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> prod_area{this};
    rw<power_plant, 124, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> prod_area_flag{this};
    rw<power_plant, 125, double, kwh_per_mm3, kwh_per_mm3> prod_factor{this};
    rw<power_plant, 126, double, meter, meter> outlet_line{this};
    rw<power_plant, 127, ::shyft::time_series::dd::apoint_ts, no_unit, meter> intake_line{this};
    rw<power_plant, 128, std::vector<double>, s2_per_m5, s2_per_m5> main_loss{this};
    rw<power_plant, 129, std::vector<double>, s2_per_m5, s2_per_m5> penstock_loss{this};
    rw<power_plant, 130, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, meter>
      tailrace_loss{this};
    rw<power_plant, 131, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> tailrace_loss_from_bypass_flag{this};
    rw<power_plant, 132, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, meter>
      intake_loss{this};
    rw<power_plant, 133, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> intake_loss_from_bypass_flag{this};
    rw<power_plant, 134, ::shyft::time_series::dd::apoint_ts, no_unit, delta_meter> tides{this};
    rw<power_plant, 135, int, no_unit, no_unit> time_delay{this};
    rw<power_plant, 136, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, hour, no_unit> shape_discharge{
      this};
    rw<power_plant, 137, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> discharge_fee{this};
    rw<power_plant, 138, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> discharge_fee_flag{this};
    rw<power_plant, 139, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, m3_per_s, nok_per_h_per_m3_per_s>
      discharge_cost_curve{this};
    rw<power_plant, 140, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> feeding_fee{this};
    rw<power_plant, 141, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> feeding_fee_flag{this};
    rw<power_plant, 142, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> production_fee{this};
    rw<power_plant, 143, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> production_fee_flag{this};
    rw<power_plant, 144, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> consumption_fee{this};
    rw<power_plant, 145, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> consumption_fee_flag{this};
    rw<power_plant, 146, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> linear_startup_flag{this};
    rw<power_plant, 147, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> maintenance_flag{this};
    rw<power_plant, 148, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> mip_flag{this};
    rw<power_plant, 149, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> mip_length{this};
    rw<power_plant, 150, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> mip_length_flag{this};
    rw<power_plant, 151, std::vector<int>, no_unit, no_unit> gen_priority{this};
    rw<power_plant, 152, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> n_seg_down{this};
    rw<power_plant, 153, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> n_seg_up{this};
    rw<power_plant, 154, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> n_mip_seg_down{this};
    rw<power_plant, 155, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> n_mip_seg_up{this};
    rw<power_plant, 156, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> dyn_pq_seg_flag{this};
    rw<power_plant, 157, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> dyn_mip_pq_seg_flag{this};
    rw<power_plant, 158, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit>
      build_original_pq_curves_by_discharge_limits{this};
    rw<power_plant, 159, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_p_constr{this};
    rw<power_plant, 160, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_p_constr_flag{this};
    rw<power_plant, 161, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_p_penalty_flag{this};
    rw<power_plant, 162, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> min_p_penalty_cost{this};
    rw<power_plant, 163, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_p_penalty_cost_flag{this};
    rw<power_plant, 164, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_p_constr{this};
    rw<power_plant, 165, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_p_constr_flag{this};
    rw<power_plant, 166, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_p_penalty_flag{this};
    rw<power_plant, 167, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> max_p_penalty_cost{this};
    rw<power_plant, 168, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_p_penalty_cost_flag{this};
    rw<power_plant, 169, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> min_q_constr{this};
    rw<power_plant, 170, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_constr_flag{this};
    rw<power_plant, 171, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_penalty_flag{this};
    rw<power_plant, 172, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> min_q_penalty_cost{this};
    rw<power_plant, 173, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_penalty_cost_flag{this};
    rw<power_plant, 174, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_q_constr{this};
    rw<power_plant, 175, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_constr_flag{this};
    rw<power_plant, 176, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_penalty_flag{this};
    rw<power_plant, 177, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> max_q_penalty_cost{this};
    rw<power_plant, 178, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_penalty_cost_flag{this};
    rw<power_plant, 179, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, m3_per_s>
      max_q_limit_rsv_up{this};
    rw<power_plant, 180, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, m3_per_s>
      max_q_limit_rsv_down{this};
    rw<power_plant, 181, ::shyft::time_series::dd::apoint_ts, no_unit, mw> production_schedule{this};
    rw<power_plant, 182, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> production_schedule_flag{this};
    rw<power_plant, 183, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> discharge_schedule{this};
    rw<power_plant, 184, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> discharge_schedule_flag{this};
    rw<power_plant, 185, ::shyft::time_series::dd::apoint_ts, no_unit, mw> consumption_schedule{this};
    rw<power_plant, 186, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> consumption_schedule_flag{this};
    rw<power_plant, 187, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> upflow_schedule{this};
    rw<power_plant, 188, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> upflow_schedule_flag{this};
    rw<power_plant, 189, double, nok, nok> sched_penalty_cost_down{this};
    rw<power_plant, 190, double, nok, nok> sched_penalty_cost_up{this};
    rw<power_plant, 191, ::shyft::time_series::dd::apoint_ts, no_unit, mw_hour> power_ramping_up{this};
    rw<power_plant, 192, ::shyft::time_series::dd::apoint_ts, no_unit, mw_hour> power_ramping_down{this};
    rw<power_plant, 193, ::shyft::time_series::dd::apoint_ts, no_unit, m3sec_hour> discharge_ramping_up{this};
    rw<power_plant, 194, ::shyft::time_series::dd::apoint_ts, no_unit, m3sec_hour> discharge_ramping_down{this};
    rw<power_plant, 195, ::shyft::time_series::dd::apoint_ts, no_unit, mw> block_merge_tolerance{this};
    rw<power_plant, 196, ::shyft::time_series::dd::apoint_ts, no_unit, minute> block_generation_mwh{this};
    rw<power_plant, 197, ::shyft::time_series::dd::apoint_ts, no_unit, minute> block_generation_m3s{this};
    rw<power_plant, 198, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_min{this};
    rw<power_plant, 199, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_max{this};
    rw<power_plant, 200, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_min{this};
    rw<power_plant, 201, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_max{this};
    rw<power_plant, 202, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_min{this};
    rw<power_plant, 203, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_min{this};
    rw<power_plant, 204, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_symmetric_flag{this};
    rw<power_plant, 205, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> bp_dyn_wv_flag{this};
    ro<power_plant, 206, ::shyft::time_series::dd::apoint_ts, no_unit, mw> ref_prod{this};
    rw<power_plant, 207, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> plant_unbalance_recommit{this};
    rw<power_plant, 208, ::shyft::time_series::dd::apoint_ts, no_unit, mw> spinning_reserve_up_max{this};
    rw<power_plant, 209, ::shyft::time_series::dd::apoint_ts, no_unit, mw> spinning_reserve_down_max{this};
    rw<power_plant, 210, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, mw> ramping_steps{
      this};
    rw<power_plant, 211, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_m3_per_s>
      discharge_rolling_ramping_penalty_cost{this};
    rw<power_plant, 212, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_m3_per_s>
      discharge_period_ramping_penalty_cost{this};
    rw<power_plant, 213, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      average_discharge_rolling_ramping_up{this};
    rw<power_plant, 214, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      average_discharge_rolling_ramping_down{this};
    rw<power_plant, 215, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      limit_discharge_rolling_ramping_up{this};
    rw<power_plant, 216, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      limit_discharge_rolling_ramping_down{this};
    rw<power_plant, 217, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      average_discharge_period_ramping_up{this};
    rw<power_plant, 218, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      average_discharge_period_ramping_down{this};
    rw<power_plant, 219, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      limit_discharge_period_ramping_up{this};
    rw<power_plant, 220, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, minute, m3_per_s>
      limit_discharge_period_ramping_down{this};
    rw<power_plant, 221, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> historical_discharge{this};
    //--TODO: rw<power_plant,222,sy,minute,minute> average_discharge_period_ramping_up_offset{this};
    //--TODO: rw<power_plant,223,sy,minute,minute> average_discharge_period_ramping_down_offset{this};
    //--TODO: rw<power_plant,224,sy,minute,minute> limit_discharge_period_ramping_up_offset{this};
    //--TODO: rw<power_plant,225,sy,minute,minute> limit_discharge_period_ramping_down_offset{this};
    rw<power_plant, 226, double, hour, hour> fcr_n_up_activation_time{this};
    rw<power_plant, 227, double, hour, hour> fcr_n_down_activation_time{this};
    rw<power_plant, 228, double, hour, hour> fcr_d_up_activation_time{this};
    rw<power_plant, 229, double, hour, hour> fcr_d_down_activation_time{this};
    rw<power_plant, 230, double, hour, hour> frr_up_activation_time{this};
    rw<power_plant, 231, double, hour, hour> frr_down_activation_time{this};
    rw<power_plant, 232, double, hour, hour> rr_up_activation_time{this};
    rw<power_plant, 233, double, hour, hour> rr_down_activation_time{this};
    rw<power_plant, 234, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_up_activation_factor{this};
    rw<power_plant, 235, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_down_activation_factor{this};
    rw<power_plant, 236, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_activation_factor{this};
    rw<power_plant, 237, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_activation_factor{this};
    rw<power_plant, 238, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_up_activation_factor{this};
    rw<power_plant, 239, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_down_activation_factor{this};
    rw<power_plant, 240, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_up_activation_factor{this};
    rw<power_plant, 241, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_down_activation_factor{this};
    rw<power_plant, 242, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> fcr_n_up_activation_penalty_cost{
      this};
    rw<power_plant, 243, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> fcr_n_down_activation_penalty_cost{
      this};
    rw<power_plant, 244, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> fcr_d_up_activation_penalty_cost{
      this};
    rw<power_plant, 245, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> fcr_d_down_activation_penalty_cost{
      this};
    rw<power_plant, 246, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> frr_up_activation_penalty_cost{
      this};
    rw<power_plant, 247, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> frr_down_activation_penalty_cost{
      this};
    rw<power_plant, 248, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> rr_up_activation_penalty_cost{this};
    rw<power_plant, 249, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> rr_down_activation_penalty_cost{
      this};
    rw<power_plant, 250, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sum_reserve_up_max{this};
    rw<power_plant, 251, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sum_reserve_down_max{this};
    rw<power_plant, 252, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_q_reserve_constr{this};
    rw<power_plant, 253, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> min_q_reserve_constr{this};
    rw<power_plant, 254, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s>
      max_q_reserve_constr_penalty_cost{this};
    rw<power_plant, 255, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s>
      min_q_reserve_constr_penalty_cost{this};
    rw<power_plant, 256, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> sum_reserve_up_max_penalty_cost{
      this};
    rw<power_plant, 257, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> sum_reserve_down_max_penalty_cost{
      this};
    rw<power_plant, 258, double, no_unit, no_unit> reserve_tactical_activation_cost_scaling{this};
    rw<power_plant, 259, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> coupled_fcr_activation_flag{this};
    rw<power_plant, 260, ::shyft::time_series::dd::apoint_ts, m3_per_s, no_unit> ramping_step_reference_discharge{this};
    rw<power_plant, 261, int, minute, minute> min_uptime{this};
    rw<power_plant, 262, int, minute, minute> min_downtime{this};
    rw<power_plant, 263, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_uptime_flag{this};
    rw<power_plant, 264, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_downtime_flag{this};
    rw<power_plant, 265, ::shyft::time_series::dd::apoint_ts, no_unit, mw> historical_production{this};
    rw<power_plant, 266, ::shyft::time_series::dd::apoint_ts, no_unit, mw> historical_consumption{this};
    rw<power_plant, 267, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> simultanous_generating_pumping_flag{
      this};
    rw<power_plant, 268, std::string, no_unit, no_unit> max_prod_reserve_strategy{this};
    rw<power_plant, 269, std::string, no_unit, no_unit> min_prod_reserve_strategy{this};
    rw<power_plant, 270, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_prod_reserve_penalty_cost{this};
    rw<power_plant, 271, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_prod_reserve_penalty_cost{this};
    rw<power_plant, 272, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> strict_pq_uploading_flag{this};
    rw<power_plant, 273, int, no_unit, no_unit> strict_pq_uploading_upon_detection{this};
    ro<power_plant, 274, ::shyft::time_series::dd::apoint_ts, no_unit, mw> production{this};
    ro<power_plant, 275, ::shyft::time_series::dd::apoint_ts, no_unit, mw> solver_production{this};
    ro<power_plant, 276, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sim_production{this};
    ro<power_plant, 277, ::shyft::time_series::dd::apoint_ts, no_unit, mw> prod_unbalance{this};
    ro<power_plant, 278, ::shyft::time_series::dd::apoint_ts, no_unit, mw> consumption{this};
    ro<power_plant, 279, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sim_consumption{this};
    ro<power_plant, 280, ::shyft::time_series::dd::apoint_ts, no_unit, mw> solver_consumption{this};
    ro<power_plant, 281, ::shyft::time_series::dd::apoint_ts, no_unit, mw> cons_unbalance{this};
    ro<power_plant, 282, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> discharge{this};
    ro<power_plant, 283, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> solver_discharge{this};
    ro<power_plant, 284, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_discharge{this};
    ro<power_plant, 285, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> upflow{this};
    ro<power_plant, 286, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_upflow{this};
    ro<power_plant, 287, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> solver_upflow{this};
    ro<power_plant, 288, ::shyft::time_series::dd::apoint_ts, no_unit, meter> gross_head{this};
    ro<power_plant, 289, ::shyft::time_series::dd::apoint_ts, no_unit, meter> eff_head{this};
    ro<power_plant, 290, ::shyft::time_series::dd::apoint_ts, no_unit, meter> head_loss{this};
    ro<power_plant, 291, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_p_penalty{this};
    ro<power_plant, 292, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_p_penalty{this};
    ro<power_plant, 293, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> min_q_penalty{this};
    ro<power_plant, 294, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> max_q_penalty{this};
    ro<power_plant, 295, ::shyft::time_series::dd::apoint_ts, no_unit, nok> p_constr_penalty{this};
    ro<power_plant, 296, ::shyft::time_series::dd::apoint_ts, no_unit, nok> q_constr_penalty{this};
    ro<power_plant, 297, ::shyft::time_series::dd::apoint_ts, no_unit, nok> schedule_up_penalty{this};
    ro<power_plant, 298, ::shyft::time_series::dd::apoint_ts, no_unit, nok> schedule_down_penalty{this};
    ro<power_plant, 299, ::shyft::time_series::dd::apoint_ts, no_unit, nok> schedule_penalty{this};
    ro<power_plant, 300, ::shyft::time_series::dd::apoint_ts, mw, mw> max_prod_all{this};
    ro<power_plant, 301, ::shyft::time_series::dd::apoint_ts, mw, mw> max_prod_available{this};
    ro<power_plant, 302, ::shyft::time_series::dd::apoint_ts, mw, mw> max_prod_spinning{this};
    ro<power_plant, 303, ::shyft::time_series::dd::apoint_ts, mw, mw> min_prod_spinning{this};
    ro<power_plant, 304, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, m3_per_s>
      best_profit_q{this};
    ro<power_plant, 305, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, nok_per_mw>
      best_profit_mc{this};
    ro<power_plant, 306, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, nok_per_mw>
      best_profit_ac{this};
    ro<power_plant, 307, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, nok>
      best_profit_commitment_cost{this};
    ro<power_plant, 308, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, nok_per_mw>
      best_profit_bid_matrix{this};
    ro<power_plant, 309, int, no_unit, no_unit> times_of_wrong_pq_uploading{this};
    ro<power_plant, 310, ::shyft::time_series::dd::apoint_ts, mw, mw> max_prod_reserve_violation{this};
    ro<power_plant, 311, ::shyft::time_series::dd::apoint_ts, mw, mw> min_prod_reserve_violation{this};
  };

  template <class A>
  struct unit : obj<A, 2> {
    using super = obj<A, 2>;
    unit() = default;

    unit(A* s, int oid)
      : super(s, oid) {
    }

    unit(unit const & o)
      : super(o) {
    }

    unit(unit&& o) noexcept
      : super(std::move(o)) {
    }

    unit& operator=(unit const & o) {
      super::operator=(o);
      return *this;
    }

    unit& operator=(unit&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<unit, 319, int, no_unit, no_unit> type{this};
    ro<unit, 320, int, no_unit, no_unit> num_needle_comb{this};
    rw<unit, 321, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> prod_area{this};
    rw<unit, 322, int, no_unit, no_unit> initial_state{this};
    rw<unit, 323, int, no_unit, no_unit> penstock{this};
    rw<unit, 324, int, no_unit, no_unit> separate_droop{this};
    rw<unit, 325, double, mw, mw> p_min{this};
    rw<unit, 326, double, mw, mw> p_max{this};
    rw<unit, 327, double, mw, mw> p_nom{this};
    rw<unit, 328, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, mw, percent> gen_eff_curve{this};
    rw<unit, 329, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, percent>
      turb_eff_curves{this};
    rw<unit, 330, int, no_unit, no_unit> affinity_eq_flag{this};
    rw<unit, 331, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> maintenance_flag{this};
    rw<unit, 332, ::shyft::time_series::dd::apoint_ts, no_unit, nok> startcost{this};
    rw<unit, 333, ::shyft::time_series::dd::apoint_ts, no_unit, nok> stopcost{this};
    rw<unit, 334, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, m3_per_s, nok_per_h_per_m3_per_s>
      discharge_cost_curve{this};
    rw<unit, 335, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> priority{this};
    rw<unit, 336, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> committed_in{this};
    rw<unit, 337, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> committed_flag{this};
    rw<unit, 338, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_p_constr{this};
    rw<unit, 339, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_p_constr_flag{this};
    rw<unit, 340, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_p_constr{this};
    rw<unit, 341, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_p_constr_flag{this};
    rw<unit, 342, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> min_q_constr{this};
    rw<unit, 343, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_constr_flag{this};
    rw<unit, 344, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_q_constr{this};
    rw<unit, 345, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_constr_flag{this};
    rw<unit, 346, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, m3_per_s> max_q_limit_rsv_down{
      this};
    rw<unit, 347, ::shyft::time_series::dd::apoint_ts, no_unit, meter> upstream_min{this};
    rw<unit, 348, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> upstream_min_flag{this};
    rw<unit, 349, ::shyft::time_series::dd::apoint_ts, no_unit, meter> downstream_max{this};
    rw<unit, 350, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> downstream_max_flag{this};
    rw<unit, 351, ::shyft::time_series::dd::apoint_ts, no_unit, mw> production_schedule{this};
    rw<unit, 352, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> production_schedule_flag{this};
    rw<unit, 353, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> discharge_schedule{this};
    rw<unit, 354, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> discharge_schedule_flag{this};
    rw<unit, 355, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_mip_flag{this};
    rw<unit, 356, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_min{this};
    rw<unit, 357, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_n_min{this};
    rw<unit, 358, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_d_min{this};
    rw<unit, 359, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_max{this};
    rw<unit, 360, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_n_max{this};
    rw<unit, 361, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_d_max{this};
    rw<unit, 362, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_frr_min{this};
    rw<unit, 363, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_frr_max{this};
    rw<unit, 364, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_rr_min{this};
    rw<unit, 365, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_min{this};
    rw<unit, 366, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_max{this};
    rw<unit, 367, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_min{this};
    rw<unit, 368, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_max{this};
    rw<unit, 369, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_min{this};
    rw<unit, 370, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_max{this};
    rw<unit, 371, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_min{this};
    rw<unit, 372, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_max{this};
    rw<unit, 373, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_min{this};
    rw<unit, 374, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_max{this};
    rw<unit, 375, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_min{this};
    rw<unit, 376, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_max{this};
    rw<unit, 377, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_min{this};
    rw<unit, 378, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_max{this};
    rw<unit, 379, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_min{this};
    rw<unit, 380, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_max{this};
    rw<unit, 381, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_schedule{this};
    rw<unit, 382, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_up_schedule_flag{this};
    rw<unit, 383, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_schedule{this};
    rw<unit, 384, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_down_schedule_flag{this};
    rw<unit, 385, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_schedule{this};
    rw<unit, 386, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_schedule_flag{this};
    rw<unit, 387, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_schedule{this};
    rw<unit, 388, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_schedule_flag{this};
    rw<unit, 389, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_schedule{this};
    rw<unit, 390, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_up_schedule_flag{this};
    rw<unit, 391, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_schedule{this};
    rw<unit, 392, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_down_schedule_flag{this};
    rw<unit, 393, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_schedule{this};
    rw<unit, 394, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_up_schedule_flag{this};
    rw<unit, 395, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_schedule{this};
    rw<unit, 396, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_down_schedule_flag{this};
    rw<unit, 397, ::shyft::time_series::dd::apoint_ts, no_unit, nok> droop_cost{this};
    rw<unit, 398, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fixed_droop{this};
    rw<unit, 399, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fixed_droop_flag{this};
    rw<unit, 400, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> droop_min{this};
    rw<unit, 401, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> droop_max{this};
    rw<unit, 402, std::vector<double>, no_unit, no_unit> discrete_droop_values{this};
    rw<unit, 403, std::vector<double>, no_unit, no_unit> fcr_n_discrete_droop_values{this};
    rw<unit, 404, std::vector<double>, no_unit, no_unit> fcr_d_up_discrete_droop_values{this};
    rw<unit, 405, std::vector<double>, no_unit, no_unit> fcr_d_down_discrete_droop_values{this};
    rw<unit, 406, ::shyft::time_series::dd::apoint_ts, no_unit, nok> reserve_ramping_cost_up{this};
    rw<unit, 407, ::shyft::time_series::dd::apoint_ts, no_unit, nok> reserve_ramping_cost_down{this};
    rw<unit, 408, ::shyft::time_series::dd::apoint_ts, no_unit, mw> ref_production{this};
    rw<unit, 409, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> schedule_deviation_flag{this};
    rw<unit, 410, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> gen_turn_off_limit{this};
    rw<unit, 411, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_up_cost{this};
    rw<unit, 412, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_down_cost{this};
    rw<unit, 413, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_up_cost{this};
    rw<unit, 414, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_down_cost{this};
    rw<unit, 415, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_up_cost{this};
    rw<unit, 416, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_down_cost{this};
    rw<unit, 417, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_up_cost{this};
    rw<unit, 418, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_down_cost{this};
    rw<unit, 419, ::shyft::time_series::dd::apoint_ts, no_unit, mw> spinning_reserve_up_max{this};
    rw<unit, 420, ::shyft::time_series::dd::apoint_ts, no_unit, mw> spinning_reserve_down_max{this};
    rw<unit, 421, ::shyft::time_series::dd::apoint_ts, no_unit, nok> fcr_n_droop_cost{this};
    rw<unit, 422, ::shyft::time_series::dd::apoint_ts, no_unit, nok> fcr_d_up_droop_cost{this};
    rw<unit, 423, ::shyft::time_series::dd::apoint_ts, no_unit, nok> fcr_d_down_droop_cost{this};
    rw<unit, 424, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_fixed_droop{this};
    rw<unit, 425, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_fixed_droop{this};
    rw<unit, 426, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_fixed_droop{this};
    rw<unit, 427, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_droop_min{this};
    rw<unit, 428, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_droop_min{this};
    rw<unit, 429, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_droop_min{this};
    rw<unit, 430, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_droop_max{this};
    rw<unit, 431, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_droop_max{this};
    rw<unit, 432, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_droop_max{this};
    rw<unit, 433, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> couple_fcr_delivery_flag{this};
    rw<unit, 434, ::shyft::time_series::dd::apoint_ts, no_unit, mw> historical_production{this};
    rw<unit, 435, int, minute, minute> min_uptime{this};
    rw<unit, 436, int, minute, minute> min_downtime{this};
    rw<unit, 437, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_uptime_flag{this};
    rw<unit, 438, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_downtime_flag{this};
    rw<unit, 439, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_down_stop_mip_flag{this};
    rw<unit, 440, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, m3_per_s> min_discharge{this};
    rw<unit, 441, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, m3_per_s> max_discharge{this};
    ro<unit, 442, ::shyft::time_series::dd::apoint_ts, no_unit, meter> eff_head{this};
    ro<unit, 443, ::shyft::time_series::dd::apoint_ts, no_unit, meter> sim_eff_head{this};
    ro<unit, 444, ::shyft::time_series::dd::apoint_ts, no_unit, meter> head_loss{this};
    ro<unit, 445, ::shyft::time_series::dd::apoint_ts, no_unit, mw> production{this};
    ro<unit, 446, ::shyft::time_series::dd::apoint_ts, no_unit, mw> solver_production{this};
    ro<unit, 447, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sim_production{this};
    ro<unit, 448, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> discharge{this};
    ro<unit, 449, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> solver_discharge{this};
    ro<unit, 450, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_discharge{this};
    ro<unit, 451, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> committed_out{this};
    ro<unit, 452, ::shyft::time_series::dd::apoint_ts, no_unit, mw> production_schedule_penalty{this};
    ro<unit, 453, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> discharge_schedule_penalty{this};
    ro<unit, 454, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_delivery{this};
    ro<unit, 455, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_delivery{this};
    ro<unit, 456, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_delivery{this};
    ro<unit, 457, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_delivery{this};
    ro<unit, 458, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_delivery{this};
    ro<unit, 459, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_delivery{this};
    ro<unit, 460, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_delivery{this};
    ro<unit, 461, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_delivery{this};
    ro<unit, 462, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_delivery_physical{this};
    ro<unit, 463, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_delivery_physical{this};
    ro<unit, 464, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_delivery_physical{this};
    ro<unit, 465, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_delivery_physical{this};
    ro<unit, 466, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_schedule_penalty{this};
    ro<unit, 467, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_schedule_penalty{this};
    ro<unit, 468, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_schedule_penalty{this};
    ro<unit, 469, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_schedule_penalty{this};
    ro<unit, 470, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_schedule_penalty{this};
    ro<unit, 471, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_schedule_penalty{this};
    ro<unit, 472, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_schedule_penalty{this};
    ro<unit, 473, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_schedule_penalty{this};
    ro<unit, 474, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> droop_result{this};
    ro<unit, 475, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, m3_per_s>
      best_profit_q{this};
    ro<unit, 476, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, mw> best_profit_p{
      this};
    ro<unit, 477, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, m3_per_s>
      best_profit_dq_dp{this};
    ro<unit, 478, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, no_unit>
      best_profit_needle_comb{this};
    ro<unit, 479, ::shyft::time_series::dd::apoint_ts, no_unit, nok> startup_cost_mip_objective{this};
    ro<unit, 480, ::shyft::time_series::dd::apoint_ts, no_unit, nok> startup_cost_total_objective{this};
    ro<unit, 481, ::shyft::time_series::dd::apoint_ts, no_unit, nok> discharge_fee_objective{this};
    ro<unit, 482, ::shyft::time_series::dd::apoint_ts, no_unit, nok> feeding_fee_objective{this};
    ro<unit, 483, ::shyft::time_series::dd::apoint_ts, no_unit, nok> schedule_penalty{this};
    ro<unit, 484, ::shyft::time_series::dd::apoint_ts, no_unit, nok> market_income{this};
    ro<unit, 485, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, mw>
      original_pq_curves{this};
    ro<unit, 486, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, mw>
      convex_pq_curves{this};
    ro<unit, 487, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, mw>
      final_pq_curves{this};
    ro<unit, 488, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_prod_individual{this};
    ro<unit, 489, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_prod_all{this};
    ro<unit, 490, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_prod_available{this};
    ro<unit, 491, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_prod_spinning{this};
    ro<unit, 492, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_prod_individual{this};
    ro<unit, 493, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_prod_spinning{this};
    ro<unit, 494, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_droop_result{this};
    ro<unit, 495, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_droop_result{this};
    ro<unit, 496, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_droop_result{this};
  };

  template <class A>
  struct needle_combination : obj<A, 3> {
    using super = obj<A, 3>;
    needle_combination() = default;

    needle_combination(A* s, int oid)
      : super(s, oid) {
    }

    needle_combination(needle_combination const & o)
      : super(o) {
    }

    needle_combination(needle_combination&& o) noexcept
      : super(std::move(o)) {
    }

    needle_combination& operator=(needle_combination const & o) {
      super::operator=(o);
      return *this;
    }

    needle_combination& operator=(needle_combination&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<needle_combination, 499, double, mw, mw> p_max{this};
    rw<needle_combination, 500, double, mw, mw> p_min{this};
    rw<needle_combination, 501, double, mw, mw> p_nom{this};
    rw<needle_combination, 502, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, percent>
      turb_eff_curves{this};
    rw<needle_combination, 503, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_min{this};
    rw<needle_combination, 504, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_n_min{this};
    rw<needle_combination, 505, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_d_min{this};
    rw<needle_combination, 506, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_max{this};
    rw<needle_combination, 507, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_n_max{this};
    rw<needle_combination, 508, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_d_max{this};
    rw<needle_combination, 509, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_frr_min{this};
    rw<needle_combination, 510, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_frr_max{this};
    rw<needle_combination, 511, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, mw, nok_per_mw>
      production_cost{this};
    rw<needle_combination, 512, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, no_unit, no_unit>
      min_discharge{this};
    rw<needle_combination, 513, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, no_unit, no_unit>
      max_discharge{this};
    ro<
      needle_combination,
      514,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      m3_per_s,
      mw>
      original_pq_curves{this};
    ro<
      needle_combination,
      515,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      m3_per_s,
      mw>
      convex_pq_curves{this};
    ro<
      needle_combination,
      516,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      m3_per_s,
      mw>
      final_pq_curves{this};
    ro<needle_combination, 517, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_prod{this};
    ro<needle_combination, 518, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_prod{this};
  };

  template <class A>
  struct pump : obj<A, 4> {
    using super = obj<A, 4>;
    pump() = default;

    pump(A* s, int oid)
      : super(s, oid) {
    }

    pump(pump const & o)
      : super(o) {
    }

    pump(pump&& o) noexcept
      : super(std::move(o)) {
    }

    pump& operator=(pump const & o) {
      super::operator=(o);
      return *this;
    }

    pump& operator=(pump&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<pump, 524, int, no_unit, no_unit> initial_state{this};
    rw<pump, 525, int, no_unit, no_unit> penstock{this};
    rw<pump, 526, int, no_unit, no_unit> separate_droop{this};
    rw<pump, 527, double, mw, mw> p_min{this};
    rw<pump, 528, double, mw, mw> p_max{this};
    rw<pump, 529, double, mw, mw> p_nom{this};
    rw<pump, 530, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, mw, percent> gen_eff_curve{this};
    rw<pump, 531, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, percent>
      turb_eff_curves{this};
    rw<pump, 532, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> maintenance_flag{this};
    rw<pump, 533, ::shyft::time_series::dd::apoint_ts, no_unit, nok> startcost{this};
    rw<pump, 534, ::shyft::time_series::dd::apoint_ts, no_unit, nok> stopcost{this};
    rw<pump, 535, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> committed_in{this};
    rw<pump, 536, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> committed_flag{this};
    rw<pump, 537, ::shyft::time_series::dd::apoint_ts, no_unit, meter> upstream_max{this};
    rw<pump, 538, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> upstream_max_flag{this};
    rw<pump, 539, ::shyft::time_series::dd::apoint_ts, no_unit, meter> downstream_min{this};
    rw<pump, 540, ::shyft::time_series::dd::apoint_ts, no_unit, meter> downstream_min_flag{this};
    rw<pump, 541, ::shyft::time_series::dd::apoint_ts, no_unit, mw> consumption_schedule{this};
    rw<pump, 542, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> consumption_schedule_flag{this};
    rw<pump, 543, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> upflow_schedule{this};
    rw<pump, 544, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> upflow_schedule_flag{this};
    rw<pump, 545, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_mip_flag{this};
    rw<pump, 546, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_min{this};
    rw<pump, 547, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_fcr_max{this};
    rw<pump, 548, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_rr_min{this};
    rw<pump, 549, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_min{this};
    rw<pump, 550, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_max{this};
    rw<pump, 551, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_min{this};
    rw<pump, 552, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_max{this};
    rw<pump, 553, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_min{this};
    rw<pump, 554, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_max{this};
    rw<pump, 555, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_min{this};
    rw<pump, 556, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_max{this};
    rw<pump, 557, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_min{this};
    rw<pump, 558, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_max{this};
    rw<pump, 559, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_min{this};
    rw<pump, 560, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_max{this};
    rw<pump, 561, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_min{this};
    rw<pump, 562, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_max{this};
    rw<pump, 563, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_min{this};
    rw<pump, 564, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_max{this};
    rw<pump, 565, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_schedule{this};
    rw<pump, 566, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_up_schedule_flag{this};
    rw<pump, 567, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_schedule{this};
    rw<pump, 568, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_down_schedule_flag{this};
    rw<pump, 569, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_schedule{this};
    rw<pump, 570, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_schedule_flag{this};
    rw<pump, 571, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_schedule{this};
    rw<pump, 572, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_schedule_flag{this};
    rw<pump, 573, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_schedule{this};
    rw<pump, 574, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_up_schedule_flag{this};
    rw<pump, 575, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_schedule{this};
    rw<pump, 576, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> frr_down_schedule_flag{this};
    rw<pump, 577, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_schedule{this};
    rw<pump, 578, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_up_schedule_flag{this};
    rw<pump, 579, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_schedule{this};
    rw<pump, 580, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_down_schedule_flag{this};
    rw<pump, 581, ::shyft::time_series::dd::apoint_ts, no_unit, nok> droop_cost{this};
    rw<pump, 582, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fixed_droop{this};
    rw<pump, 583, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fixed_droop_flag{this};
    rw<pump, 584, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> droop_min{this};
    rw<pump, 585, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> droop_max{this};
    rw<pump, 586, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> reserve_ramping_cost_up{this};
    rw<pump, 587, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> reserve_ramping_cost_down{this};
    rw<pump, 588, std::vector<double>, no_unit, no_unit> discrete_droop_values{this};
    rw<pump, 589, std::vector<double>, no_unit, no_unit> fcr_n_discrete_droop_values{this};
    rw<pump, 590, std::vector<double>, no_unit, no_unit> fcr_d_up_discrete_droop_values{this};
    rw<pump, 591, std::vector<double>, no_unit, no_unit> fcr_d_down_discrete_droop_values{this};
    rw<pump, 592, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_up_cost{this};
    rw<pump, 593, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_down_cost{this};
    rw<pump, 594, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_up_cost{this};
    rw<pump, 595, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_down_cost{this};
    rw<pump, 596, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_up_cost{this};
    rw<pump, 597, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_down_cost{this};
    rw<pump, 598, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_up_cost{this};
    rw<pump, 599, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_down_cost{this};
    rw<pump, 600, ::shyft::time_series::dd::apoint_ts, no_unit, mw> spinning_reserve_up_max{this};
    rw<pump, 601, ::shyft::time_series::dd::apoint_ts, no_unit, mw> spinning_reserve_down_max{this};
    rw<pump, 602, ::shyft::time_series::dd::apoint_ts, no_unit, nok> fcr_n_droop_cost{this};
    rw<pump, 603, ::shyft::time_series::dd::apoint_ts, no_unit, nok> fcr_d_up_droop_cost{this};
    rw<pump, 604, ::shyft::time_series::dd::apoint_ts, no_unit, nok> fcr_d_down_droop_cost{this};
    rw<pump, 605, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_fixed_droop{this};
    rw<pump, 606, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_fixed_droop{this};
    rw<pump, 607, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_fixed_droop{this};
    rw<pump, 608, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_droop_min{this};
    rw<pump, 609, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_droop_min{this};
    rw<pump, 610, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_droop_min{this};
    rw<pump, 611, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_droop_max{this};
    rw<pump, 612, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_droop_max{this};
    rw<pump, 613, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_droop_max{this};
    rw<pump, 614, ::shyft::time_series::dd::apoint_ts, no_unit, mw> historical_consumption{this};
    rw<pump, 615, int, minute, minute> min_uptime{this};
    rw<pump, 616, int, minute, minute> min_downtime{this};
    rw<pump, 617, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_uptime_flag{this};
    rw<pump, 618, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_downtime_flag{this};
    rw<pump, 619, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> rr_up_stop_mip_flag{this};
    rw<pump, 620, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_p_constr{this};
    rw<pump, 621, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_p_constr_flag{this};
    rw<pump, 622, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_p_constr{this};
    rw<pump, 623, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_p_constr_flag{this};
    rw<pump, 624, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> min_q_constr{this};
    rw<pump, 625, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_constr_flag{this};
    rw<pump, 626, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_q_constr{this};
    rw<pump, 627, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_constr_flag{this};
    ro<pump, 628, ::shyft::time_series::dd::apoint_ts, no_unit, meter> eff_head{this};
    ro<pump, 629, ::shyft::time_series::dd::apoint_ts, no_unit, meter> head_loss{this};
    ro<pump, 630, ::shyft::time_series::dd::apoint_ts, no_unit, mw> consumption{this};
    ro<pump, 631, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sim_consumption{this};
    ro<pump, 632, ::shyft::time_series::dd::apoint_ts, no_unit, mw> solver_consumption{this};
    ro<pump, 633, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> upflow{this};
    ro<pump, 634, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_upflow{this};
    ro<pump, 635, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> solver_upflow{this};
    ro<pump, 636, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> committed_out{this};
    ro<pump, 637, ::shyft::time_series::dd::apoint_ts, no_unit, mw> consumption_schedule_penalty{this};
    ro<pump, 638, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> upflow_schedule_penalty{this};
    ro<pump, 639, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_delivery{this};
    ro<pump, 640, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_delivery{this};
    ro<pump, 641, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_delivery{this};
    ro<pump, 642, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_delivery{this};
    ro<pump, 643, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_delivery{this};
    ro<pump, 644, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_delivery{this};
    ro<pump, 645, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_delivery{this};
    ro<pump, 646, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_delivery{this};
    ro<pump, 647, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_delivery_physical{this};
    ro<pump, 648, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_delivery_physical{this};
    ro<pump, 649, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_delivery_physical{this};
    ro<pump, 650, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_delivery_physical{this};
    ro<pump, 651, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_schedule_penalty{this};
    ro<pump, 652, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_schedule_penalty{this};
    ro<pump, 653, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_schedule_penalty{this};
    ro<pump, 654, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_schedule_penalty{this};
    ro<pump, 655, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_schedule_penalty{this};
    ro<pump, 656, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_schedule_penalty{this};
    ro<pump, 657, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_schedule_penalty{this};
    ro<pump, 658, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_schedule_penalty{this};
    ro<pump, 659, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> droop_result{this};
    ro<pump, 660, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, mw>
      original_pq_curves{this};
    ro<pump, 661, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, mw>
      convex_pq_curves{this};
    ro<pump, 662, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, m3_per_s, mw>
      final_pq_curves{this};
    ro<pump, 663, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_cons{this};
    ro<pump, 664, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_cons{this};
    ro<pump, 665, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_n_droop_result{this};
    ro<pump, 666, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_up_droop_result{this};
    ro<pump, 667, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> fcr_d_down_droop_result{this};
  };

  template <class A>
  struct gate : obj<A, 5> {
    using super = obj<A, 5>;
    gate() = default;

    gate(A* s, int oid)
      : super(s, oid) {
    }

    gate(gate const & o)
      : super(o) {
    }

    gate(gate&& o) noexcept
      : super(std::move(o)) {
    }

    gate& operator=(gate const & o) {
      super::operator=(o);
      return *this;
    }

    gate& operator=(gate&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
      connection_spill,
      connection_bypass
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      case relation::connection_spill:
        return "connection_spill";
      case relation::connection_bypass:
        return "connection_bypass";
      default:
        return nullptr;
      }
    }

    ro<gate, 670, int, no_unit, no_unit> type{this};
    rw<gate, 672, int, no_unit, no_unit> time_delay{this};
    rw<gate, 673, int, no_unit, no_unit> add_slack{this};
    rw<gate, 674, double, m3_per_s, m3_per_s> max_discharge{this};
    rw<gate, 677, double, no_unit, no_unit> lin_rel_a{this};
    rw<gate, 678, double, mm3, mm3> lin_rel_b{this};
    rw<gate, 679, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, hour, no_unit> shape_discharge{this};
    rw<gate, 680, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, m3_per_s, nok_per_m3_per_s>
      spill_cost_curve{this};
    rw<gate, 681, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, m3_per_s, nok_per_m3_per_s>
      peak_flow_cost_curve{this};
    ro<gate, 682, ::shyft::time_series::dd::apoint_ts, no_unit, nok> peak_flow_penalty{this};
    rw<gate, 683, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, meter, m3_per_s>
      functions_meter_m3s{this};
    rw<gate, 684, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, delta_meter, m3_per_s>
      functions_deltameter_m3s{this};
    rw<gate, 685, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> min_flow{this};
    rw<gate, 686, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_flow_flag{this};
    rw<gate, 687, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_flow{this};
    rw<gate, 688, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_flow_flag{this};
    rw<gate, 689, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> schedule_m3s{this};
    rw<gate, 690, ::shyft::time_series::dd::apoint_ts, no_unit, percent> schedule_percent{this};
    rw<gate, 691, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> schedule_flag{this};
    rw<gate, 692, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> setting{this};
    rw<gate, 693, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> setting_flag{this};
    rw<gate, 694, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> discharge_fee{this};
    rw<gate, 695, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> discharge_fee_flag{this};
    rw<gate, 696, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> block_merge_tolerance{this};
    rw<gate, 697, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_penalty_flag{this};
    rw<gate, 698, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_penalty_flag{this};
    rw<gate, 699, ::shyft::time_series::dd::apoint_ts, no_unit, m3sec_hour> ramping_up{this};
    rw<gate, 700, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> ramping_up_flag{this};
    rw<gate, 701, ::shyft::time_series::dd::apoint_ts, no_unit, m3sec_hour> ramping_down{this};
    rw<gate, 702, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> ramping_down_flag{this};
    rw<gate, 703, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_m3_per_s> ramp_penalty_cost{this};
    rw<gate, 704, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> ramp_penalty_cost_flag{this};
    rw<gate, 705, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> max_q_penalty_cost{this};
    rw<gate, 706, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> min_q_penalty_cost{this};
    rw<gate, 707, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_q_penalty_cost_flag{this};
    rw<gate, 708, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_q_penalty_cost_flag{this};
    ro<gate, 709, ::shyft::time_series::dd::apoint_ts, no_unit, nok> min_q_penalty{this};
    ro<gate, 710, ::shyft::time_series::dd::apoint_ts, no_unit, nok> max_q_penalty{this};
    ro<gate, 711, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> discharge{this};
    ro<gate, 712, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_discharge{this};
  };

  template <class A>
  struct thermal : obj<A, 6> {
    using super = obj<A, 6>;
    thermal() = default;

    thermal(A* s, int oid)
      : super(s, oid) {
    }

    thermal(thermal const & o)
      : super(o) {
    }

    thermal(thermal&& o) noexcept
      : super(std::move(o)) {
    }

    thermal& operator=(thermal const & o) {
      super::operator=(o);
      return *this;
    }

    thermal& operator=(thermal&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<thermal, 1300, ::shyft::time_series::dd::apoint_ts, nok_per_mwh, nok_per_mwh> fuel_cost{this};
    rw<thermal, 1301, ::shyft::time_series::dd::apoint_ts, nok_per_mwh, nok_per_mwh> quadratic_fuel_cost{this};
    rw<thermal, 1302, int, no_unit, no_unit> n_segments{this};
    rw<thermal, 1303, ::shyft::time_series::dd::apoint_ts, mw, mw> min_prod{this};
    rw<thermal, 1304, ::shyft::time_series::dd::apoint_ts, mw, mw> max_prod{this};
    rw<thermal, 1305, ::shyft::time_series::dd::apoint_ts, nok, nok> startcost{this};
    rw<thermal, 1306, ::shyft::time_series::dd::apoint_ts, nok, nok> stopcost{this};
    ro<thermal, 1307, ::shyft::time_series::dd::apoint_ts, mw, mw> production{this};
  };

  template <class A>
  struct junction : obj<A, 7> {
    using super = obj<A, 7>;
    junction() = default;

    junction(A* s, int oid)
      : super(s, oid) {
    }

    junction(junction const & o)
      : super(o) {
    }

    junction(junction&& o) noexcept
      : super(std::move(o)) {
    }

    junction& operator=(junction const & o) {
      super::operator=(o);
      return *this;
    }

    junction& operator=(junction&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<junction, 726, int, no_unit, no_unit> junc_slack{this};
    rw<junction, 727, double, meter, meter> altitude{this};
    ro<junction, 728, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> tunnel_flow_1{this};
    ro<junction, 729, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> tunnel_flow_2{this};
    ro<junction, 730, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_tunnel_flow_1{this};
    ro<junction, 731, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_tunnel_flow_2{this};
    rw<junction, 732, double, s2_per_m5, s2_per_m5> loss_factor_1{this};
    rw<junction, 733, double, s2_per_m5, s2_per_m5> loss_factor_2{this};
    rw<junction, 734, ::shyft::time_series::dd::apoint_ts, no_unit, meter> min_pressure{this};
    ro<junction, 735, ::shyft::time_series::dd::apoint_ts, no_unit, meter> pressure_height{this};
    ro<junction, 736, ::shyft::time_series::dd::apoint_ts, no_unit, meter> sim_pressure_height{this};
    ro<junction, 737, ::shyft::time_series::dd::apoint_ts, no_unit, nok> incr_cost{this};
    ro<junction, 738, ::shyft::time_series::dd::apoint_ts, no_unit, nok> local_incr_cost{this};
  };

  template <class A>
  struct junction_gate : obj<A, 8> {
    using super = obj<A, 8>;
    junction_gate() = default;

    junction_gate(A* s, int oid)
      : super(s, oid) {
    }

    junction_gate(junction_gate const & o)
      : super(o) {
    }

    junction_gate(junction_gate&& o) noexcept
      : super(std::move(o)) {
    }

    junction_gate& operator=(junction_gate const & o) {
      super::operator=(o);
      return *this;
    }

    junction_gate& operator=(junction_gate&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<junction_gate, 715, int, no_unit, no_unit> add_slack{this};
    rw<junction_gate, 716, double, meter, meter> height_1{this};
    rw<junction_gate, 717, double, s2_per_m5, s2_per_m5> loss_factor_1{this};
    rw<junction_gate, 718, double, s2_per_m5, s2_per_m5> loss_factor_2{this};
    rw<junction_gate, 719, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> schedule{this};
    ro<junction_gate, 720, ::shyft::time_series::dd::apoint_ts, no_unit, meter> pressure_height{this};
    ro<junction_gate, 721, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> tunnel_flow_1{this};
    ro<junction_gate, 722, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> tunnel_flow_2{this};
  };

  template <class A>
  struct creek_intake : obj<A, 9> {
    using super = obj<A, 9>;
    creek_intake() = default;

    creek_intake(A* s, int oid)
      : super(s, oid) {
    }

    creek_intake(creek_intake const & o)
      : super(o) {
    }

    creek_intake(creek_intake&& o) noexcept
      : super(std::move(o)) {
    }

    creek_intake& operator=(creek_intake const & o) {
      super::operator=(o);
      return *this;
    }

    creek_intake& operator=(creek_intake&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard,
      connection_spill
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      case relation::connection_spill:
        return "connection_spill";
      default:
        return nullptr;
      }
    }

    rw<creek_intake, 741, double, meter, meter> net_head{this};
    rw<creek_intake, 742, double, m3_per_s, m3_per_s> max_inflow{this};
    rw<creek_intake, 743, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_inflow_dynamic{this};
    rw<creek_intake, 744, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> inflow{this};
    ro<creek_intake, 745, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_inflow{this};
    ro<creek_intake, 746, ::shyft::time_series::dd::apoint_ts, no_unit, meter> sim_pressure_height{this};
    rw<creek_intake, 747, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> inflow_percentage{this};
    rw<creek_intake, 748, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> overflow_cost{this};
    ro<creek_intake, 749, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> non_physical_overflow_flag{this};
  };

  template <class A>
  struct contract : obj<A, 10> {
    using super = obj<A, 10>;
    contract() = default;

    contract(A* s, int oid)
      : super(s, oid) {
    }

    contract(contract const & o)
      : super(o) {
    }

    contract(contract&& o) noexcept
      : super(std::move(o)) {
    }

    contract& operator=(contract const & o) {
      super::operator=(o);
      return *this;
    }

    contract& operator=(contract&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<contract, 1387, double, mw, mw> initial_trade{this};
    rw<contract, 1388, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, nok_per_mwh>
      trade_curve{this};
    rw<contract, 1389, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_trade{this};
    rw<contract, 1390, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_trade{this};
    rw<contract, 1391, ::shyft::time_series::dd::apoint_ts, no_unit, mw_hour> ramping_up{this};
    rw<contract, 1392, ::shyft::time_series::dd::apoint_ts, no_unit, mw_hour> ramping_down{this};
    rw<contract, 1393, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> ramping_up_penalty_cost{this};
    rw<contract, 1394, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> ramping_down_penalty_cost{this};
    ro<contract, 1395, ::shyft::time_series::dd::apoint_ts, no_unit, mw> trade{this};
    ro<contract, 1396, ::shyft::time_series::dd::apoint_ts, no_unit, nok> ramping_up_penalty{this};
    ro<contract, 1397, ::shyft::time_series::dd::apoint_ts, no_unit, nok> ramping_down_penalty{this};
  };

  template <class A>
  struct market : obj<A, 11> {
    using super = obj<A, 11>;
    market() = default;

    market(A* s, int oid)
      : super(s, oid) {
    }

    market(market const & o)
      : super(o) {
    }

    market(market&& o) noexcept
      : super(std::move(o)) {
    }

    market& operator=(market const & o) {
      super::operator=(o);
      return *this;
    }

    market& operator=(market&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<market, 750, int, no_unit, no_unit> prod_area{this};
    rw<market, 751, std::string, no_unit, no_unit> market_type{this};
    rw<market, 752, ::shyft::time_series::dd::apoint_ts, no_unit, mw> load{this};
    rw<market, 753, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_buy{this};
    rw<market, 754, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_sale{this};
    rw<market, 755, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> load_price{this};
    rw<market, 756, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> buy_price{this};
    rw<market, 757, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> sale_price{this};
    rw<market, 758, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> buy_delta{this};
    rw<market, 759, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> sale_delta{this};
    rw<market, 760, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> bid_flag{this};
    rw<market, 761, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> common_scenario{this};
    ro<market, 762, ::shyft::time_series::dd::apoint_ts, no_unit, mw> buy{this};
    ro<market, 763, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sale{this};
    ro<market, 764, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sim_sale{this};
    ro<market, 765, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sim_buy{this};
    ro<market, 766, ::shyft::time_series::dd::apoint_ts, no_unit, mw> reserve_obligation_penalty{this};
    ro<market, 767, ::shyft::time_series::dd::apoint_ts, no_unit, mw> load_penalty{this};
  };

  template <class A>
  struct global_settings : obj<A, 12> {
    using super = obj<A, 12>;
    global_settings() = default;

    global_settings(A* s, int oid)
      : super(s, oid) {
    }

    global_settings(global_settings const & o)
      : super(o) {
    }

    global_settings(global_settings&& o) noexcept
      : super(std::move(o)) {
    }

    global_settings& operator=(global_settings const & o) {
      super::operator=(o);
      return *this;
    }

    global_settings& operator=(global_settings&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    rw<global_settings, 961, int, no_unit, no_unit> load_penalty_flag{this};
    rw<global_settings, 962, int, no_unit, no_unit> rsv_penalty_flag{this};
    rw<global_settings, 963, int, no_unit, no_unit> volume_ramp_penalty_flag{this};
    rw<global_settings, 964, int, no_unit, no_unit> level_ramp_penalty_flag{this};
    rw<global_settings, 965, int, no_unit, no_unit> production_ramp_penalty_flag{this};
    rw<global_settings, 966, int, no_unit, no_unit> plant_min_q_penalty_flag{this};
    rw<global_settings, 967, int, no_unit, no_unit> plant_min_p_penalty_flag{this};
    rw<global_settings, 968, int, no_unit, no_unit> plant_max_q_penalty_flag{this};
    rw<global_settings, 969, int, no_unit, no_unit> plant_max_p_penalty_flag{this};
    rw<global_settings, 970, int, no_unit, no_unit> gate_min_q_penalty_flag{this};
    rw<global_settings, 971, int, no_unit, no_unit> gate_max_q_penalty_flag{this};
    rw<global_settings, 972, int, no_unit, no_unit> gate_ramp_penalty_flag{this};
    rw<global_settings, 973, int, no_unit, no_unit> plant_schedule_penalty_flag{this};
    rw<global_settings, 974, int, no_unit, no_unit> gen_discharge_schedule_penalty_flag{this};
    rw<global_settings, 975, int, no_unit, no_unit> pump_schedule_penalty_flag{this};
    rw<global_settings, 976, int, no_unit, no_unit> power_limit_penalty_flag{this};
    rw<global_settings, 977, double, nok_per_mwh, nok_per_mwh> power_limit_penalty_cost{this};
    rw<global_settings, 978, double, nok_per_mwh, nok_per_mwh> load_penalty_cost{this};
    rw<global_settings, 979, double, nok_per_mm3, nok_per_mm3> rsv_penalty_cost{this};
    rw<global_settings, 980, double, nok_per_mm3, nok_per_mm3> rsv_hard_limit_penalty_cost{this};
    rw<global_settings, 981, double, nok_per_mm3, nok_per_mm3> volume_ramp_penalty_cost{this};
    rw<global_settings, 982, double, nok_per_meter, nok_per_meter> level_ramp_penalty_cost{this};
    rw<global_settings, 983, double, nok_per_mwh, nok_per_mwh> production_ramp_penalty_cost{this};
    rw<global_settings, 984, double, nok_per_m3_per_s, nok_per_m3_per_s> plant_discharge_ramp_penalty_cost{this};
    rw<global_settings, 985, double, nok_per_mwh, nok_per_mwh> plant_soft_p_penalty{this};
    rw<global_settings, 986, double, nok_per_mm3, nok_per_mm3> plant_soft_q_penalty{this};
    rw<global_settings, 987, double, nok, nok> plant_sched_penalty_cost_up{this};
    rw<global_settings, 988, double, nok, nok> plant_sched_penalty_cost_down{this};
    rw<global_settings, 989, double, nok_per_mm3, nok_per_mm3> gen_discharge_sched_penalty_cost_up{this};
    rw<global_settings, 990, double, nok_per_mm3, nok_per_mm3> gen_discharge_sched_penalty_cost_down{this};
    rw<global_settings, 991, double, nok, nok> pump_sched_penalty_cost_up{this};
    rw<global_settings, 992, double, nok, nok> pump_sched_penalty_cost_down{this};
    rw<global_settings, 993, double, nok_per_m3_per_s, nok_per_m3_per_s> gate_ramp_penalty_cost{this};
    rw<global_settings, 994, double, nok_per_mm3, nok_per_mm3> discharge_group_penalty_cost{this};
    rw<global_settings, 995, double, nok_per_mw, nok_per_mw> reserve_schedule_penalty_cost{this};
    rw<global_settings, 996, double, nok_per_mw, nok_per_mw> reserve_group_penalty_cost{this};
    rw<global_settings, 997, double, nok_per_mm3, nok_per_mm3> bypass_cost{this};
    rw<global_settings, 998, double, nok_per_mm3, nok_per_mm3> gate_cost{this};
    rw<global_settings, 999, double, nok_per_mm3, nok_per_mm3> overflow_cost{this};
    rw<global_settings, 1000, double, nok_per_mm3h, nok_per_mm3h> overflow_cost_time_factor{this};
    rw<global_settings, 1001, double, nok_per_mw, nok_per_mw> gen_reserve_ramping_cost{this};
    rw<global_settings, 1002, double, nok_per_mw, nok_per_mw> pump_reserve_ramping_cost{this};
    rw<global_settings, 1003, double, nok, nok> reserve_contribution_cost{this};
    rw<global_settings, 1004, double, nok_per_m3_per_s, nok_per_m3_per_s> gate_ramp_cost{this};
    rw<global_settings, 1005, double, nok_per_mw, nok_per_mw> reserve_group_slack_cost{this};
    rw<global_settings, 1006, double, nok_per_mw, nok_per_mw> fcr_n_ramping_cost{this};
    rw<global_settings, 1007, double, nok_per_mw, nok_per_mw> fcr_d_ramping_cost{this};
    rw<global_settings, 1008, double, nok_per_mw, nok_per_mw> frr_ramping_cost{this};
    rw<global_settings, 1009, double, nok_per_mw, nok_per_mw> rr_ramping_cost{this};
    rw<global_settings, 1010, double, no_unit, no_unit> fcr_n_up_activation_factor{this};
    rw<global_settings, 1011, double, no_unit, no_unit> fcr_n_down_activation_factor{this};
    rw<global_settings, 1012, double, no_unit, no_unit> fcr_d_up_activation_factor{this};
    rw<global_settings, 1013, double, no_unit, no_unit> fcr_d_down_activation_factor{this};
    rw<global_settings, 1014, double, no_unit, no_unit> frr_up_activation_factor{this};
    rw<global_settings, 1015, double, no_unit, no_unit> frr_down_activation_factor{this};
    rw<global_settings, 1016, double, no_unit, no_unit> rr_up_activation_factor{this};
    rw<global_settings, 1017, double, no_unit, no_unit> rr_down_activation_factor{this};
    rw<global_settings, 1018, double, no_unit, no_unit> fcr_n_up_activation_time{this};
    rw<global_settings, 1019, double, hour, hour> fcr_n_down_activation_time{this};
    rw<global_settings, 1020, double, hour, hour> fcr_d_up_activation_time{this};
    rw<global_settings, 1021, double, hour, hour> fcr_d_down_activation_time{this};
    rw<global_settings, 1022, double, hour, hour> frr_up_activation_time{this};
    rw<global_settings, 1023, double, hour, hour> frr_down_activation_time{this};
    rw<global_settings, 1024, double, hour, hour> rr_up_activation_time{this};
    rw<global_settings, 1025, double, hour, hour> rr_down_activation_time{this};
    rw<global_settings, 1026, double, nok_per_mm3, nok_per_mm3> fcr_n_up_activation_penalty_cost{this};
    rw<global_settings, 1027, double, nok_per_mm3, nok_per_mm3> fcr_n_down_activation_penalty_cost{this};
    rw<global_settings, 1028, double, nok_per_mm3, nok_per_mm3> fcr_d_up_activation_penalty_cost{this};
    rw<global_settings, 1029, double, nok_per_mm3, nok_per_mm3> fcr_d_down_activation_penalty_cost{this};
    rw<global_settings, 1030, double, nok_per_mm3, nok_per_mm3> frr_up_activation_penalty_cost{this};
    rw<global_settings, 1031, double, nok_per_mm3, nok_per_mm3> frr_down_activation_penalty_cost{this};
    rw<global_settings, 1032, double, nok_per_mm3, nok_per_mm3> rr_up_activation_penalty_cost{this};
    rw<global_settings, 1033, double, nok_per_mm3, nok_per_mm3> rr_down_activation_penalty_cost{this};
    rw<global_settings, 1034, double, no_unit, no_unit> reserve_tactical_activation_cost_scaling{this};
    rw<global_settings, 1035, int, no_unit, no_unit> use_heuristic_basis{this};
    rw<global_settings, 1036, int, no_unit, no_unit> nodelog{this};
    rw<global_settings, 1037, int, no_unit, no_unit> max_num_threads{this};
    rw<global_settings, 1038, int, no_unit, no_unit> parallelmode{this};
    rw<global_settings, 1039, double, second, second> timelimit{this};
    rw<global_settings, 1040, double, no_unit, no_unit> mipgap_rel{this};
    rw<global_settings, 1041, double, nok, nok> mipgap_abs{this};
    rw<global_settings, 1042, double, no_unit, no_unit> inteps{this};
    rw<global_settings, 1043, std::string, no_unit, no_unit> input_basis_name{this};
    rw<global_settings, 1044, std::string, no_unit, no_unit> output_basis_name{this};
    rw<global_settings, 1045, std::string, no_unit, no_unit> solver_algorithm{this};
    rw<global_settings, 1046, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, no_unit, no_unit>
      cplex_int_params{this};
    rw<global_settings, 1047, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, no_unit, no_unit>
      cplex_double_params{this};
    rw<global_settings, 1048, int, no_unit, no_unit> n_seg_up{this};
    rw<global_settings, 1049, int, no_unit, no_unit> n_seg_down{this};
    rw<global_settings, 1050, int, no_unit, no_unit> n_mip_seg_up{this};
    rw<global_settings, 1051, int, no_unit, no_unit> n_mip_seg_down{this};
    rw<global_settings, 1052, int, no_unit, no_unit> dyn_pq_seg_flag{this};
    rw<global_settings, 1053, int, no_unit, no_unit> dyn_mip_pq_seg_flag{this};
    rw<global_settings, 1054, int, no_unit, no_unit> bypass_segments{this};
    rw<global_settings, 1055, int, no_unit, no_unit> gate_segments{this};
    rw<global_settings, 1056, int, no_unit, no_unit> overflow_segments{this};
    rw<global_settings, 1057, double, no_unit, no_unit> gravity{this};
    rw<global_settings, 1058, double, no_unit, no_unit> gen_reserve_min_free_cap_factor{this};
    rw<global_settings, 1059, double, no_unit, no_unit> fcr_n_band{this};
    rw<global_settings, 1060, double, no_unit, no_unit> fcr_d_band{this};
    rw<global_settings, 1061, int, no_unit, no_unit> universal_mip{this};
    rw<global_settings, 1062, int, no_unit, no_unit> universal_overflow_mip{this};
    rw<global_settings, 1063, int, no_unit, no_unit> universal_river_mip{this};
    rw<global_settings, 1064, int, no_unit, no_unit> linear_startup{this};
    rw<global_settings, 1065, int, no_unit, no_unit> dyn_flex_mip_steps{this};
    rw<global_settings, 1066, int, no_unit, no_unit> merge_blocks{this};
    rw<global_settings, 1067, int, no_unit, no_unit> power_head_optimization{this};
    rw<global_settings, 1068, double, no_unit, no_unit> droop_discretization_limit{this};
    rw<global_settings, 1069, double, no_unit, no_unit> droop_cost_exponent{this};
    rw<global_settings, 1070, double, no_unit, no_unit> droop_ref_value{this};
    rw<global_settings, 1071, int, no_unit, no_unit> disable_rsv_penalties{this};
    rw<global_settings, 1072, int, no_unit, no_unit> binary_reserve_limits{this};
    rw<global_settings, 1073, int, no_unit, no_unit> universal_strict_pq_uploading{this};
    rw<global_settings, 1074, int, no_unit, no_unit> create_cuts{this};
    rw<global_settings, 1075, int, no_unit, no_unit> print_sim_inflow{this};
    rw<global_settings, 1076, int, no_unit, no_unit> pump_head_optimization{this};
    rw<global_settings, 1077, int, no_unit, no_unit> save_pq_curves{this};
    rw<global_settings, 1078, int, no_unit, no_unit> build_original_pq_curves_by_discharge_limits{this};
    rw<global_settings, 1079, int, no_unit, no_unit> plant_unbalance_recommit{this};
    rw<global_settings, 1080, int, no_unit, no_unit> prefer_start_vol{this};
    rw<global_settings, 1081, int, no_unit, no_unit> bp_print_discharge{this};
    rw<global_settings, 1082, int, no_unit, no_unit> prod_from_ref_prod{this};
    rw<global_settings, 1083, int, no_unit, no_unit> bp_min_points{this};
    rw<global_settings, 1084, int, no_unit, no_unit> bp_mode{this};
    rw<global_settings, 1085, int, no_unit, no_unit> stop_cost_from_start_cost{this};
    rw<global_settings, 1086, int, no_unit, no_unit> ownership_scaling{this};
    rw<global_settings, 1087, std::string, no_unit, no_unit> time_delay_unit{this};
    rw<global_settings, 1088, int, no_unit, no_unit> bypass_loss{this};
    rw<global_settings, 1089, double, no_unit, no_unit> gen_turn_off_limit{this};
    rw<global_settings, 1090, double, no_unit, no_unit> pump_turn_off_limit{this};
    rw<global_settings, 1091, int, no_unit, no_unit> fcr_n_equality_flag{this};
    rw<global_settings, 1092, int, no_unit, no_unit> delay_valuation_mode{this};
    rw<global_settings, 1093, int, no_unit, no_unit> universal_affinity_flag{this};
    rw<global_settings, 1094, int, no_unit, no_unit> ramp_code{this};
    rw<global_settings, 1095, int, no_unit, no_unit> print_original_pq_curves{this};
    rw<global_settings, 1096, int, no_unit, no_unit> print_convex_pq_curves{this};
    rw<global_settings, 1097, int, no_unit, no_unit> print_final_pq_curves{this};
    rw<global_settings, 1098, std::string, no_unit, no_unit> shop_log_name{this};
    rw<global_settings, 1099, std::string, no_unit, no_unit> shop_yaml_log_name{this};
    rw<global_settings, 1100, std::string, no_unit, no_unit> solver_log_name{this};
    rw<global_settings, 1101, std::string, no_unit, no_unit> minimal_infeasible_problem_file{this};
    rw<global_settings, 1102, std::string, no_unit, no_unit> model_file_name{this};
    rw<global_settings, 1103, std::string, no_unit, no_unit> pq_curves_name{this};
    rw<global_settings, 1104, int, no_unit, no_unit> print_loss{this};
    rw<global_settings, 1105, int, no_unit, no_unit> shop_xmllog{this};
    rw<global_settings, 1106, int, no_unit, no_unit> print_optimized_startup_costs{this};
    rw<global_settings, 1107, int, no_unit, no_unit> get_duals_from_mip{this};
    rw<global_settings, 1108, int, no_unit, no_unit> bid_aggregation_level{this};
    rw<global_settings, 1109, int, no_unit, no_unit> simple_pq_recovery{this};
    rw<global_settings, 1110, int, no_unit, no_unit> rr_up_schedule_slack_flag{this};
    ro<global_settings, 1111, int, no_unit, no_unit> bp_ref_mc_from_market{this};
    rw<global_settings, 1112, int, no_unit, no_unit> bp_bid_matrix_points{this};
    rw<global_settings, 1113, double, no_unit, no_unit> ramp_scale_factor{this};
    rw<global_settings, 1114, double, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s> river_flow_penalty_cost{this};
    rw<global_settings, 1115, double, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s> river_flow_schedule_penalty_cost{
      this};
    rw<global_settings, 1116, int, no_unit, no_unit> recommit{this};
    rw<global_settings, 1117, double, nok, nok> droop_cost{this};
    rw<global_settings, 1118, int, no_unit, no_unit> rr_down_stop_mip{this};
    rw<global_settings, 1119, int, no_unit, no_unit> rr_up_stop_mip{this};
    rw<global_settings, 1120, std::string, no_unit, no_unit> plant_max_prod_reserve_strategy{this};
    rw<global_settings, 1121, std::string, no_unit, no_unit> plant_min_prod_reserve_strategy{this};
    ro<global_settings, 1162, std::string, no_unit, no_unit> shop_version{this};
    rw<global_settings, 1163, double, no_unit, no_unit> river_big_m_factor{this};
  };

  template <class A>
  struct reserve_group : obj<A, 13> {
    using super = obj<A, 13>;
    reserve_group() = default;

    reserve_group(A* s, int oid)
      : super(s, oid) {
    }

    reserve_group(reserve_group const & o)
      : super(o) {
    }

    reserve_group(reserve_group&& o) noexcept
      : super(std::move(o)) {
    }

    reserve_group& operator=(reserve_group const & o) {
      super::operator=(o);
      return *this;
    }

    reserve_group& operator=(reserve_group&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<reserve_group, 768, int, no_unit, no_unit> group_id{this};
    rw<reserve_group, 769, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_obligation{this};
    rw<reserve_group, 770, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_obligation{this};
    rw<reserve_group, 771, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_obligation{this};
    rw<reserve_group, 772, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_obligation{this};
    rw<reserve_group, 773, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_obligation{this};
    rw<reserve_group, 774, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_obligation{this};
    rw<reserve_group, 775, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_obligation{this};
    rw<reserve_group, 776, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_obligation{this};
    rw<reserve_group, 777, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_penalty_cost{this};
    rw<reserve_group, 778, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_penalty_cost{this};
    rw<reserve_group, 779, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_penalty_cost{this};
    rw<reserve_group, 780, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_penalty_cost{this};
    ro<reserve_group, 781, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_slack{this};
    ro<reserve_group, 782, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_slack{this};
    ro<reserve_group, 783, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_slack{this};
    ro<reserve_group, 784, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_slack{this};
    ro<reserve_group, 785, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_slack{this};
    ro<reserve_group, 786, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_slack{this};
    ro<reserve_group, 787, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_slack{this};
    ro<reserve_group, 788, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_slack{this};
    ro<reserve_group, 789, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_violation{this};
    ro<reserve_group, 790, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_violation{this};
    ro<reserve_group, 791, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_violation{this};
    ro<reserve_group, 792, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_violation{this};
    ro<reserve_group, 793, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_violation{this};
    ro<reserve_group, 794, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_violation{this};
    ro<reserve_group, 795, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_violation{this};
    ro<reserve_group, 796, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_violation{this};
  };

  template <class A>
  struct commit_group : obj<A, 14> {
    using super = obj<A, 14>;
    commit_group() = default;

    commit_group(A* s, int oid)
      : super(s, oid) {
    }

    commit_group(commit_group const & o)
      : super(o) {
    }

    commit_group(commit_group&& o) noexcept
      : super(std::move(o)) {
    }

    commit_group& operator=(commit_group const & o) {
      super::operator=(o);
      return *this;
    }

    commit_group& operator=(commit_group&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<commit_group, 842, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> deactivate_exclusion_flag{this};
  };

  template <class A>
  struct discharge_group : obj<A, 15> {
    using super = obj<A, 15>;
    discharge_group() = default;

    discharge_group(A* s, int oid)
      : super(s, oid) {
    }

    discharge_group(discharge_group const & o)
      : super(o) {
    }

    discharge_group(discharge_group&& o) noexcept
      : super(std::move(o)) {
    }

    discharge_group& operator=(discharge_group const & o) {
      super::operator=(o);
      return *this;
    }

    discharge_group& operator=(discharge_group&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<discharge_group, 797, double, mm3, mm3> initial_deviation_mm3{this};
    rw<discharge_group, 798, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> max_accumulated_deviation_mm3_up{this};
    rw<discharge_group, 799, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> max_accumulated_deviation_mm3_down{
      this};
    rw<discharge_group, 800, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> weighted_discharge_m3s{this};
    rw<discharge_group, 801, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> penalty_cost_up_per_mm3{this};
    rw<discharge_group, 802, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> penalty_cost_down_per_mm3{this};
    rw<discharge_group, 803, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> min_discharge_m3s{this};
    rw<discharge_group, 804, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> max_discharge_m3s{this};
    rw<discharge_group, 805, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s>
      min_discharge_penalty_cost{this};
    rw<discharge_group, 806, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s>
      max_discharge_penalty_cost{this};
    rw<discharge_group, 807, ::shyft::time_series::dd::apoint_ts, no_unit, m3sec_hour> ramping_up_m3s{this};
    rw<discharge_group, 808, ::shyft::time_series::dd::apoint_ts, no_unit, m3sec_hour> ramping_down_m3s{this};
    rw<discharge_group, 809, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s>
      ramping_up_penalty_cost{this};
    rw<discharge_group, 810, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s>
      ramping_down_penalty_cost{this};
    rw<
      discharge_group,
      811,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      minute,
      m3_per_s>
      min_average_discharge{this};
    rw<
      discharge_group,
      812,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      minute,
      m3_per_s>
      max_average_discharge{this};
    rw<discharge_group, 813, double, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s> min_average_discharge_penalty_cost{
      this};
    rw<discharge_group, 814, double, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s> max_average_discharge_penalty_cost{
      this};
    ro<discharge_group, 815, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> actual_discharge_m3s{this};
    ro<discharge_group, 816, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> accumulated_deviation_mm3{this};
    ro<discharge_group, 817, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> upper_penalty_mm3{this};
    ro<discharge_group, 818, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> lower_penalty_mm3{this};
    ro<discharge_group, 819, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> upper_slack_mm3{this};
    ro<discharge_group, 820, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> lower_slack_mm3{this};
    ro<discharge_group, 821, ::shyft::time_series::dd::apoint_ts, no_unit, nok> min_discharge_penalty{this};
    ro<discharge_group, 822, ::shyft::time_series::dd::apoint_ts, no_unit, nok> max_discharge_penalty{this};
    ro<discharge_group, 823, ::shyft::time_series::dd::apoint_ts, no_unit, nok> ramping_up_penalty{this};
    ro<discharge_group, 824, ::shyft::time_series::dd::apoint_ts, no_unit, nok> ramping_down_penalty{this};
  };

  template <class A>
  struct production_group : obj<A, 16> {
    using super = obj<A, 16>;
    production_group() = default;

    production_group(A* s, int oid)
      : super(s, oid) {
    }

    production_group(production_group const & o)
      : super(o) {
    }

    production_group(production_group&& o) noexcept
      : super(std::move(o)) {
    }

    production_group& operator=(production_group const & o) {
      super::operator=(o);
      return *this;
    }

    production_group& operator=(production_group&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<production_group, 825, double, mwh, mwh> energy_target{this};
    rw<production_group, 826, double, nok_per_mwh, nok_per_mwh> energy_penalty_cost_up{this};
    rw<production_group, 827, double, nok_per_mwh, nok_per_mwh> energy_penalty_cost_down{this};
    rw<production_group, 828, ::shyft::time_series::dd::apoint_ts, no_unit, mwh> energy_target_period_flag{this};
    rw<production_group, 829, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_p_limit{this};
    rw<production_group, 830, ::shyft::time_series::dd::apoint_ts, no_unit, mw> min_p_limit{this};
    rw<production_group, 831, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> max_p_penalty_cost{this};
    rw<production_group, 832, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> min_p_penalty_cost{this};
    ro<production_group, 833, ::shyft::time_series::dd::apoint_ts, no_unit, mw> sum_production{this};
    ro<production_group, 834, ::shyft::time_series::dd::apoint_ts, no_unit, nok> min_p_penalty{this};
    ro<production_group, 835, ::shyft::time_series::dd::apoint_ts, no_unit, nok> max_p_penalty{this};
    ro<production_group, 836, ::shyft::time_series::dd::apoint_ts, nok, nok> energy_penalty_up{this};
    ro<production_group, 837, ::shyft::time_series::dd::apoint_ts, nok, nok> energy_penalty_down{this};
  };

  template <class A>
  struct volume_constraint : obj<A, 17> {
    using super = obj<A, 17>;
    volume_constraint() = default;

    volume_constraint(A* s, int oid)
      : super(s, oid) {
    }

    volume_constraint(volume_constraint const & o)
      : super(o) {
    }

    volume_constraint(volume_constraint&& o) noexcept
      : super(std::move(o)) {
    }

    volume_constraint& operator=(volume_constraint const & o) {
      super::operator=(o);
      return *this;
    }

    volume_constraint& operator=(volume_constraint&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<volume_constraint, 838, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> min_vol{this};
    rw<volume_constraint, 839, ::shyft::time_series::dd::apoint_ts, no_unit, mm3> max_vol{this};
    rw<volume_constraint, 840, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> min_vol_penalty{this};
    rw<volume_constraint, 841, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mm3> max_vol_penalty{this};
  };

  template <class A>
  struct scenario : obj<A, 18> {
    using super = obj<A, 18>;
    scenario() = default;

    scenario(A* s, int oid)
      : super(s, oid) {
    }

    scenario(scenario const & o)
      : super(o) {
    }

    scenario(scenario&& o) noexcept
      : super(std::move(o)) {
    }

    scenario& operator=(scenario const & o) {
      super::operator=(o);
      return *this;
    }

    scenario& operator=(scenario&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    rw<scenario, 843, int, no_unit, no_unit> scenario_id{this};
    rw<scenario, 844, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> probability{this};
    rw<scenario, 845, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> common_scenario{this};
    rw<scenario, 846, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> common_history{this};
  };

  template <class A>
  struct objective : obj<A, 19> {
    using super = obj<A, 19>;
    objective() = default;

    objective(A* s, int oid)
      : super(s, oid) {
    }

    objective(objective const & o)
      : super(o) {
    }

    objective(objective&& o) noexcept
      : super(std::move(o)) {
    }

    objective& operator=(objective const & o) {
      super::operator=(o);
      return *this;
    }

    objective& operator=(objective&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    ro<objective, 847, std::string, no_unit, no_unit> solver_status{this};
    ro<objective, 848, int, no_unit, no_unit> times_of_wrong_pq_uploading{this};
    ro<objective, 849, double, nok, nok> grand_total{this};
    ro<objective, 850, double, nok, nok> sim_grand_total{this};
    ro<objective, 851, double, nok, nok> total{this};
    ro<objective, 852, double, nok, nok> sum_penalties{this};
    ro<objective, 853, double, nok, nok> minor_penalties{this};
    ro<objective, 854, double, nok, nok> major_penalties{this};
    ro<objective, 855, double, nok, nok> rsv_end_value{this};
    ro<objective, 856, double, nok, nok> sim_rsv_end_value{this};
    ro<objective, 857, double, nok, nok> rsv_end_value_relative{this};
    ro<objective, 858, double, nok, nok> vow_in_transit{this};
    ro<objective, 859, double, nok, nok> rsv_spill_vol_end_value{this};
    ro<objective, 860, double, nok, nok> market_sale_buy{this};
    ro<objective, 861, double, nok, nok> sim_market_sale_buy{this};
    ro<objective, 862, double, nok, nok> load_value{this};
    ro<objective, 863, double, nok, nok> reserve_sale_buy{this};
    ro<objective, 864, double, nok, nok> reserve_oblig_value{this};
    ro<objective, 865, double, nok, nok> contract_value{this};
    ro<objective, 866, double, nok, nok> startup_costs{this};
    ro<objective, 867, double, nok, nok> sim_startup_costs{this};
    ro<objective, 868, double, nok, nok> sum_feeding_fee{this};
    ro<objective, 869, double, nok, nok> sum_discharge_fee{this};
    ro<objective, 870, double, nok, nok> thermal_cost{this};
    ro<objective, 871, double, nok, nok> production_cost{this};
    ro<objective, 872, double, nok, nok> reserve_allocation_cost{this};
    ro<objective, 873, double, nok, nok> incurred_droop_cost{this};
    ro<objective, 874, double, nok, nok> rsv_tactical_penalty{this};
    ro<objective, 875, double, nok, nok> plant_p_constr_penalty{this};
    ro<objective, 876, double, nok, nok> plant_q_constr_penalty{this};
    ro<objective, 877, double, nok, nok> plant_schedule_penalty{this};
    ro<objective, 878, double, nok, nok> plant_rsv_q_limit_penalty{this};
    ro<objective, 879, double, nok, nok> gen_schedule_penalty{this};
    ro<objective, 880, double, nok, nok> pump_schedule_penalty{this};
    ro<objective, 881, double, nok, nok> gate_q_constr_penalty{this};
    ro<objective, 882, double, nok, nok> gate_discharge_cost{this};
    ro<objective, 883, double, nok, nok> bypass_cost{this};
    ro<objective, 884, double, nok, nok> gate_spill_cost{this};
    ro<objective, 885, double, nok, nok> physical_spill_cost{this};
    ro<objective, 886, double, mm3, mm3> physical_spill_volume{this};
    ro<objective, 887, double, nok, nok> nonphysical_spill_cost{this};
    ro<objective, 888, double, mm3, mm3> nonphysical_spill_volume{this};
    ro<objective, 889, double, nok, nok> gate_slack_cost{this};
    ro<objective, 890, double, nok, nok> gate_ramping_cost{this};
    ro<objective, 891, double, nok, nok> creek_spill_cost{this};
    ro<objective, 892, double, nok, nok> creek_physical_spill_cost{this};
    ro<objective, 893, double, nok, nok> creek_nonphysical_spill_cost{this};
    ro<objective, 894, double, nok, nok> junction_slack_cost{this};
    ro<objective, 895, double, nok, nok> reserve_violation_penalty{this};
    ro<objective, 896, double, nok, nok> reserve_slack_cost{this};
    ro<objective, 897, double, nok, nok> reserve_schedule_penalty{this};
    ro<objective, 898, double, nok, nok> rsv_peak_volume_penalty{this};
    ro<objective, 899, double, nok, nok> gate_peak_flow_penalty{this};
    ro<objective, 900, double, nok, nok> rsv_flood_volume_penalty{this};
    ro<objective, 901, double, nok, nok> river_peak_flow_penalty{this};
    ro<objective, 902, double, nok, nok> river_flow_penalty{this};
    ro<objective, 903, double, nok, nok> river_gate_adjustment_penalty{this};
    ro<objective, 904, double, nok, nok> plant_reserve_discharge_penalty{this};
    ro<objective, 905, double, nok, nok> solar_curtailment_cost{this};
    ro<objective, 906, double, nok, nok> wind_curtailment_cost{this};
    ro<objective, 907, double, nok, nok> plant_sum_reserve_penalty{this};
    ro<objective, 908, double, nok, nok> reserve_activation_penalty{this};
    ro<objective, 909, double, nok, nok> reserve_tactical_activation_penalty{this};
    ro<objective, 910, double, nok, nok> plant_reserve_strategy_penalty{this};
    ro<objective, 911, double, nok, nok> rsv_penalty{this};
    ro<objective, 912, double, nok, nok> rsv_hard_limit_penalty{this};
    ro<objective, 913, double, nok, nok> rsv_over_limit_penalty{this};
    ro<objective, 914, double, nok, nok> sim_rsv_penalty{this};
    ro<objective, 915, double, nok, nok> rsv_end_penalty{this};
    ro<objective, 916, double, nok, nok> load_penalty{this};
    ro<objective, 917, double, nok, nok> group_time_period_penalty{this};
    ro<objective, 918, double, nok, nok> group_time_step_penalty{this};
    ro<objective, 919, double, nok, nok> sum_ramping_penalty{this};
    ro<objective, 920, double, nok, nok> plant_ramping_penalty{this};
    ro<objective, 921, double, nok, nok> rsv_ramping_penalty{this};
    ro<objective, 922, double, nok, nok> gate_ramping_penalty{this};
    ro<objective, 923, double, nok, nok> contract_ramping_penalty{this};
    ro<objective, 924, double, nok, nok> group_ramping_penalty{this};
    ro<objective, 925, double, nok, nok> discharge_group_penalty{this};
    ro<objective, 926, double, nok, nok> discharge_group_ramping_penalty{this};
    ro<objective, 927, double, nok, nok> discharge_group_average_discharge_penalty{this};
    ro<objective, 928, double, nok, nok> production_group_energy_penalty{this};
    ro<objective, 929, double, nok, nok> production_group_power_penalty{this};
    ro<objective, 930, double, nok, nok> river_min_flow_penalty{this};
    ro<objective, 931, double, nok, nok> river_max_flow_penalty{this};
    ro<objective, 932, double, nok, nok> river_ramping_penalty{this};
    ro<objective, 933, double, nok, nok> river_gate_ramping_penalty{this};
    ro<objective, 934, double, nok, nok> river_flow_schedule_penalty{this};
    ro<objective, 935, double, nok, nok> level_rolling_ramping_penalty{this};
    ro<objective, 936, double, nok, nok> level_period_ramping_penalty{this};
    ro<objective, 937, double, nok, nok> discharge_rolling_ramping_penalty{this};
    ro<objective, 938, double, nok, nok> discharge_period_ramping_penalty{this};
    ro<objective, 939, double, nok, nok> rsv_nonseq_ramping_penalty{this};
    ro<objective, 940, double, nok, nok> rsv_amplitude_ramping_penalty{this};
    ro<objective, 941, double, nok, nok> common_decision_penalty{this};
    ro<objective, 942, double, nok, nok> bidding_penalty{this};
    ro<objective, 943, double, nok, nok> safe_mode_universal_penalty{this};
  };

  template <class A>
  struct bid_group : obj<A, 20> {
    using super = obj<A, 20>;
    bid_group() = default;

    bid_group(A* s, int oid)
      : super(s, oid) {
    }

    bid_group(bid_group const & o)
      : super(o) {
    }

    bid_group(bid_group&& o) noexcept
      : super(std::move(o)) {
    }

    bid_group& operator=(bid_group const & o) {
      super::operator=(o);
      return *this;
    }

    bid_group& operator=(bid_group&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<bid_group, 945, int, no_unit, no_unit> price_dimension{this};
    ro<bid_group, 946, int, no_unit, no_unit> time_dimension{this};
    ro<bid_group, 947, int, no_unit, no_unit> bid_start_interval{this};
    ro<bid_group, 948, int, no_unit, no_unit> bid_end_interval{this};
    ro<bid_group, 950, double, no_unit, no_unit> reduction_cost{this};
    ro<bid_group, 952, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, nok_per_mwh, mwh>
      bid_curves{this};
    ro<bid_group, 953, ::shyft::time_series::dd::apoint_ts, nok_per_mwh, mwh> bid_penalty{this};
    ro<bid_group, 954, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, nok_per_mwh, mwh>
      best_profit_bid_matrix{this};
  };

  template <class A>
  struct cut_group : obj<A, 21> {
    using super = obj<A, 21>;
    cut_group() = default;

    cut_group(A* s, int oid)
      : super(s, oid) {
    }

    cut_group(cut_group const & o)
      : super(o) {
    }

    cut_group(cut_group&& o) noexcept
      : super(std::move(o)) {
    }

    cut_group& operator=(cut_group const & o) {
      super::operator=(o);
      return *this;
    }

    cut_group& operator=(cut_group&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<cut_group, 955, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, no_unit, nok> rhs{this};
    ro<cut_group, 956, ::shyft::time_series::dd::apoint_ts, no_unit, nok> end_value{this};
    ro<cut_group, 957, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> binding_cut_up{this};
    ro<cut_group, 958, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> binding_cut_down{this};
  };

  template <class A>
  struct inflow_series : obj<A, 22> {
    using super = obj<A, 22>;
    inflow_series() = default;

    inflow_series(A* s, int oid)
      : super(s, oid) {
    }

    inflow_series(inflow_series const & o)
      : super(o) {
    }

    inflow_series(inflow_series&& o) noexcept
      : super(std::move(o)) {
    }

    inflow_series& operator=(inflow_series const & o) {
      super::operator=(o);
      return *this;
    }

    inflow_series& operator=(inflow_series&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<inflow_series, 959, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mm3, nok_per_mm3>
      cut_coeffs{this};
  };

  template <class A>
  struct opt_system : obj<A, 23> {
    using super = obj<A, 23>;
    opt_system() = default;

    opt_system(A* s, int oid)
      : super(s, oid) {
    }

    opt_system(opt_system const & o)
      : super(o) {
    }

    opt_system(opt_system&& o) noexcept
      : super(std::move(o)) {
    }

    opt_system& operator=(opt_system const & o) {
      super::operator=(o);
      return *this;
    }

    opt_system& operator=(opt_system&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    ro<opt_system, 960, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, nok, nok>
      cut_output_rhs{this};
  };

  template <class A>
  struct unit_combination : obj<A, 24> {
    using super = obj<A, 24>;
    unit_combination() = default;

    unit_combination(A* s, int oid)
      : super(s, oid) {
    }

    unit_combination(unit_combination const & o)
      : super(o) {
    }

    unit_combination(unit_combination&& o) noexcept
      : super(std::move(o)) {
    }

    unit_combination& operator=(unit_combination const & o) {
      super::operator=(o);
      return *this;
    }

    unit_combination& operator=(unit_combination&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    ro<unit_combination, 1164, std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>, mw, m3_per_s>
      discharge{this};
    ro<
      unit_combination,
      1165,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      mw,
      nok_per_mw>
      marginal_cost{this};
    ro<
      unit_combination,
      1166,
      std::map<int64_t, ::shyft::energy_market::hydro_power::xy_point_curve_with_z>,
      mw,
      nok_per_mw>
      average_cost{this};
  };

  template <class A>
  struct tunnel : obj<A, 25> {
    using super = obj<A, 25>;
    tunnel() = default;

    tunnel(A* s, int oid)
      : super(s, oid) {
    }

    tunnel(tunnel const & o)
      : super(o) {
    }

    tunnel(tunnel&& o) noexcept
      : super(std::move(o)) {
    }

    tunnel& operator=(tunnel const & o) {
      super::operator=(o);
      return *this;
    }

    tunnel& operator=(tunnel&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<tunnel, 1167, double, meter, meter> start_height{this};
    rw<tunnel, 1168, double, meter, meter> end_height{this};
    rw<tunnel, 1169, double, meter, meter> diameter{this};
    rw<tunnel, 1170, double, meter, meter> length{this};
    rw<tunnel, 1171, double, s2_per_m5, s2_per_m5> loss_factor{this};
    rw<tunnel, 1172, double, meter, meter> weir_width{this};
    rw<tunnel, 1173, int, no_unit, no_unit> time_delay{this};
    rw<tunnel, 1174, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, no_unit, no_unit> gate_opening_curve{
      this};
    rw<tunnel, 1175, ::shyft::time_series::dd::apoint_ts, nok, nok> gate_adjustment_cost{this};
    rw<tunnel, 1176, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> gate_opening_schedule{this};
    rw<tunnel, 1177, double, no_unit, no_unit> initial_opening{this};
    rw<tunnel, 1178, int, no_unit, no_unit> continuous_gate{this};
    ro<tunnel, 1179, ::shyft::time_series::dd::apoint_ts, meter, meter> end_pressure{this};
    ro<tunnel, 1180, ::shyft::time_series::dd::apoint_ts, meter, meter> sim_end_pressure{this};
    ro<tunnel, 1181, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> flow{this};
    ro<tunnel, 1182, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> physical_flow{this};
    ro<tunnel, 1183, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> sim_flow{this};
    ro<tunnel, 1184, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> gate_opening{this};
    ro<tunnel, 1185, int, no_unit, no_unit> network_no{this};
    rw<tunnel, 1186, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> min_flow{this};
    rw<tunnel, 1187, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> max_flow{this};
    rw<tunnel, 1188, ::shyft::time_series::dd::apoint_ts, nok, nok> min_flow_penalty_cost{this};
    rw<tunnel, 1189, ::shyft::time_series::dd::apoint_ts, nok, nok> max_flow_penalty_cost{this};
    rw<tunnel, 1190, ::shyft::time_series::dd::apoint_ts, meter, meter> min_start_pressure{this};
  };

  template <class A>
  struct interlock_constraint : obj<A, 26> {
    using super = obj<A, 26>;
    interlock_constraint() = default;

    interlock_constraint(A* s, int oid)
      : super(s, oid) {
    }

    interlock_constraint(interlock_constraint const & o)
      : super(o) {
    }

    interlock_constraint(interlock_constraint&& o) noexcept
      : super(std::move(o)) {
    }

    interlock_constraint& operator=(interlock_constraint const & o) {
      super::operator=(o);
      return *this;
    }

    interlock_constraint& operator=(interlock_constraint&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<interlock_constraint, 1191, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> min_open{this};
    rw<interlock_constraint, 1192, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> max_open{this};
    rw<interlock_constraint, 1193, double, no_unit, no_unit> forward_switch_time{this};
    rw<interlock_constraint, 1194, double, no_unit, no_unit> backward_switch_time{this};
  };

  template <class A>
  struct flow_constraint : obj<A, 27> {
    using super = obj<A, 27>;
    flow_constraint() = default;

    flow_constraint(A* s, int oid)
      : super(s, oid) {
    }

    flow_constraint(flow_constraint const & o)
      : super(o) {
    }

    flow_constraint(flow_constraint&& o) noexcept
      : super(std::move(o)) {
    }

    flow_constraint& operator=(flow_constraint const & o) {
      super::operator=(o);
      return *this;
    }

    flow_constraint& operator=(flow_constraint&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }
  };

  template <class A>
  struct lp_model : obj<A, 28> {
    using super = obj<A, 28>;
    lp_model() = default;

    lp_model(A* s, int oid)
      : super(s, oid) {
    }

    lp_model(lp_model const & o)
      : super(o) {
    }

    lp_model(lp_model&& o) noexcept
      : super(std::move(o)) {
    }

    lp_model& operator=(lp_model const & o) {
      super::operator=(o);
      return *this;
    }

    lp_model& operator=(lp_model&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    rw<lp_model, 1195, int, no_unit, no_unit> sim_mode{this};
    //--TODO: ro<lp_model,1196,string_array,no_unit,no_unit> var_type_names{this};
    //--TODO: ro<lp_model,1197,string_array,no_unit,no_unit> var_type_abbrev{this};
    ro<lp_model, 1198, std::vector<int>, no_unit, no_unit> var_type_index_type_beg{this};
    ro<lp_model, 1199, std::vector<int>, no_unit, no_unit> var_type_index_type_cnt{this};
    ro<lp_model, 1200, std::vector<int>, no_unit, no_unit> var_type_index_type_val{this};
    //--TODO: ro<lp_model,1201,string_array,no_unit,no_unit> row_type_names{this};
    ro<lp_model, 1202, std::vector<int>, no_unit, no_unit> row_type_index_type_beg{this};
    ro<lp_model, 1203, std::vector<int>, no_unit, no_unit> row_type_index_type_cnt{this};
    ro<lp_model, 1204, std::vector<int>, no_unit, no_unit> row_type_index_type_val{this};
    //--TODO: ro<lp_model,1205,string_array,no_unit,no_unit> index_type_names{this};
    ro<lp_model, 1206, std::vector<int>, no_unit, no_unit> index_type_desc_beg{this};
    ro<lp_model, 1207, std::vector<int>, no_unit, no_unit> index_type_desc_cnt{this};
    ro<lp_model, 1208, std::vector<int>, no_unit, no_unit> index_type_desc_index{this};
    //--TODO: ro<lp_model,1209,string_array,no_unit,no_unit> index_type_desc_val{this};
    ro<lp_model, 1210, std::vector<double>, no_unit, no_unit> AA{this};
    ro<lp_model, 1211, std::vector<int>, no_unit, no_unit> Irow{this};
    ro<lp_model, 1212, std::vector<int>, no_unit, no_unit> Jcol{this};
    ro<lp_model, 1213, std::vector<double>, no_unit, no_unit> rhs{this};
    ro<lp_model, 1214, std::vector<int>, no_unit, no_unit> sense{this};
    ro<lp_model, 1215, std::vector<double>, no_unit, no_unit> ub{this};
    ro<lp_model, 1216, std::vector<double>, no_unit, no_unit> lb{this};
    ro<lp_model, 1217, std::vector<double>, no_unit, no_unit> cc{this};
    ro<lp_model, 1218, std::vector<int>, no_unit, no_unit> bin{this};
    ro<lp_model, 1219, std::vector<double>, no_unit, no_unit> x{this};
    ro<lp_model, 1220, std::vector<double>, no_unit, no_unit> dual{this};
    ro<lp_model, 1221, std::vector<int>, no_unit, no_unit> var_type{this};
    ro<lp_model, 1222, std::vector<int>, no_unit, no_unit> var_index_beg{this};
    ro<lp_model, 1223, std::vector<int>, no_unit, no_unit> var_index_cnt{this};
    ro<lp_model, 1224, std::vector<int>, no_unit, no_unit> var_index_val{this};
    ro<lp_model, 1225, std::vector<int>, no_unit, no_unit> row_type{this};
    ro<lp_model, 1226, std::vector<int>, no_unit, no_unit> row_index_beg{this};
    ro<lp_model, 1227, std::vector<int>, no_unit, no_unit> row_index_cnt{this};
    ro<lp_model, 1228, std::vector<int>, no_unit, no_unit> row_index_val{this};
    rw<lp_model, 1229, int, no_unit, no_unit> add_row_type{this};
    rw<lp_model, 1230, std::vector<int>, no_unit, no_unit> add_row_index{this};
    rw<lp_model, 1231, std::vector<int>, no_unit, no_unit> add_row_variables{this};
    rw<lp_model, 1232, std::vector<double>, no_unit, no_unit> add_row_coeff{this};
    rw<lp_model, 1233, double, no_unit, no_unit> add_row_rhs{this};
    rw<lp_model, 1234, int, no_unit, no_unit> add_row_sense{this};
    ro<lp_model, 1235, int, no_unit, no_unit> add_row_last{this};
    rw<lp_model, 1236, int, no_unit, no_unit> add_var_type{this};
    rw<lp_model, 1237, std::vector<int>, no_unit, no_unit> add_var_index{this};
    rw<lp_model, 1238, double, no_unit, no_unit> add_var_ub{this};
    rw<lp_model, 1239, double, no_unit, no_unit> add_var_lb{this};
    rw<lp_model, 1240, double, no_unit, no_unit> add_var_cc{this};
    rw<lp_model, 1241, int, no_unit, no_unit> add_var_bin{this};
    ro<lp_model, 1242, int, no_unit, no_unit> add_var_last{this};
  };

  template <class A>
  struct river : obj<A, 29> {
    using super = obj<A, 29>;
    river() = default;

    river(A* s, int oid)
      : super(s, oid) {
    }

    river(river const & o)
      : super(o) {
    }

    river(river&& o) noexcept
      : super(std::move(o)) {
    }

    river& operator=(river const & o) {
      super::operator=(o);
      return *this;
    }

    river& operator=(river&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<river, 1243, double, meter, meter> length{this};
    rw<river, 1244, double, meter, meter> upstream_elevation{this};
    rw<river, 1245, double, meter, meter> downstream_elevation{this};
    rw<river, 1246, double, hour, hour> time_delay_const{this};
    rw<river, 1247, double, nok_per_mwh, nok_per_mwh> delayed_water_energy_value{this};
    rw<river, 1248, double, nok_per_mm3, nok_per_mm3> delayed_water_value{this};
    rw<river, 1249, double, no_unit, no_unit> initial_gate_opening{this};
    rw<river, 1250, double, m3_per_s, m3_per_s> linear_ref_flow{this};
    rw<river, 1251, double, no_unit, no_unit> implicit_factor{this};
    rw<river, 1252, int, no_unit, no_unit> main_river{this};
    rw<river, 1253, int, no_unit, no_unit> submerged_weir{this};
    rw<river, 1254, int, no_unit, no_unit> linear_submerged_weir_flow{this};
    rw<river, 1255, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, meter, meter> width_depth_curve{this};
    rw<river, 1256, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, m3_per_s, nok_per_h_per_m3_per_s>
      flow_cost_curve{this};
    rw<river, 1257, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, m3_per_s, nok_per_m3_per_s>
      peak_flow_cost_curve{this};
    rw<river, 1258, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, hour, m3_per_s>
      time_delay_curve{this};
    rw<river, 1259, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> past_upstream_flow{this};
    rw<river, 1260, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, meter, m3_per_s>
      up_head_flow_curve{this};
    rw<river, 1261, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, no_unit, meter> gate_opening_curve{
      this};
    rw<river, 1262, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, meter, m3_per_s>
      delta_head_ref_up_flow_curve{this};
    rw<river, 1263, std::vector<::shyft::energy_market::hydro_power::xy_point_curve_with_z>, meter, m3_per_s>
      delta_head_ref_down_flow_curve{this};
    rw<river, 1264, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> inflow{this};
    rw<river, 1265, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> min_flow{this};
    rw<river, 1266, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> max_flow{this};
    rw<river, 1267, ::shyft::time_series::dd::apoint_ts, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s>
      min_flow_penalty_cost{this};
    rw<river, 1268, ::shyft::time_series::dd::apoint_ts, nok_per_h_per_m3_per_s, nok_per_h_per_m3_per_s>
      max_flow_penalty_cost{this};
    rw<river, 1269, ::shyft::time_series::dd::apoint_ts, m3sec_hour, m3sec_hour> ramping_up{this};
    rw<river, 1270, ::shyft::time_series::dd::apoint_ts, m3sec_hour, m3sec_hour> ramping_down{this};
    rw<river, 1271, ::shyft::time_series::dd::apoint_ts, nok_per_m3_per_s, nok_per_m3_per_s> ramping_up_penalty_cost{
      this};
    rw<river, 1272, ::shyft::time_series::dd::apoint_ts, nok_per_m3_per_s, nok_per_m3_per_s> ramping_down_penalty_cost{
      this};
    rw<river, 1273, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> cost_curve_scaling{this};
    rw<river, 1274, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> flow_schedule{this};
    rw<river, 1275, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s> flow_schedule_penalty_cost{
      this};
    rw<river, 1276, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> gate_opening_schedule{this};
    rw<river, 1277, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> flow_block_merge_tolerance{this};
    rw<river, 1278, ::shyft::time_series::dd::apoint_ts, no_unit, meter_per_hour> gate_ramping{this};
    rw<river, 1279, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_meter_hour> gate_ramping_penalty_cost{this};
    rw<river, 1280, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_meter> gate_adjustment_cost{this};
    rw<river, 1281, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_h_per_m3_per_s> flow_cost{this};
    rw<river, 1282, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> mip_flag{this};
    ro<river, 1283, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> flow{this};
    ro<river, 1284, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> upstream_flow{this};
    ro<river, 1285, ::shyft::time_series::dd::apoint_ts, m3_per_s, m3_per_s> downstream_flow{this};
    ro<river, 1286, ::shyft::time_series::dd::apoint_ts, meter, meter> gate_height{this};
    ro<river, 1287, ::shyft::time_series::dd::apoint_ts, nok, nok> min_flow_penalty{this};
    ro<river, 1288, ::shyft::time_series::dd::apoint_ts, nok, nok> max_flow_penalty{this};
    ro<river, 1289, ::shyft::time_series::dd::apoint_ts, nok, nok> ramping_up_penalty{this};
    ro<river, 1290, ::shyft::time_series::dd::apoint_ts, nok, nok> ramping_down_penalty{this};
    ro<river, 1291, ::shyft::time_series::dd::apoint_ts, no_unit, nok> flow_penalty{this};
    ro<river, 1292, ::shyft::time_series::dd::apoint_ts, no_unit, nok> peak_flow_penalty{this};
    ro<river, 1293, ::shyft::time_series::dd::apoint_ts, no_unit, nok> gate_ramping_penalty{this};
    ro<river, 1294, ::shyft::time_series::dd::apoint_ts, no_unit, nok> gate_adjustment_penalty{this};
    ro<river, 1295, ::shyft::time_series::dd::apoint_ts, no_unit, nok> flow_schedule_penalty{this};
    ro<river, 1296, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> physical_flow{this};
    ro<river, 1297, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> initial_downstream_flow{this};
    ro<river, 1298, ::shyft::energy_market::hydro_power::xy_point_curve_with_z, hour, m3_per_s>
      distributed_past_upstream_flow{this};
    ro<river, 1299, ::shyft::time_series::dd::apoint_ts, no_unit, m3_per_s> sim_flow{this};
  };

  template <class A>
  struct busbar : obj<A, 30> {
    using super = obj<A, 30>;
    busbar() = default;

    busbar(A* s, int oid)
      : super(s, oid) {
    }

    busbar(busbar const & o)
      : super(o) {
    }

    busbar(busbar&& o) noexcept
      : super(std::move(o)) {
    }

    busbar& operator=(busbar const & o) {
      super::operator=(o);
      return *this;
    }

    busbar& operator=(busbar&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<busbar, 1308, ::shyft::time_series::dd::apoint_ts, no_unit, mw> load{this};
    //--TODO: rw<busbar,1309,sy,no_unit,no_unit> ptdf{this};
    ro<busbar, 1310, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mwh> energy_price{this};
    ro<busbar, 1311, ::shyft::time_series::dd::apoint_ts, no_unit, mwh> power_deficit{this};
    ro<busbar, 1312, ::shyft::time_series::dd::apoint_ts, no_unit, mwh> power_excess{this};
  };

  template <class A>
  struct ac_line : obj<A, 31> {
    using super = obj<A, 31>;
    ac_line() = default;

    ac_line(A* s, int oid)
      : super(s, oid) {
    }

    ac_line(ac_line const & o)
      : super(o) {
    }

    ac_line(ac_line&& o) noexcept
      : super(std::move(o)) {
    }

    ac_line& operator=(ac_line const & o) {
      super::operator=(o);
      return *this;
    }

    ac_line& operator=(ac_line&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<ac_line, 1313, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_forward_flow{this};
    rw<ac_line, 1314, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_backward_flow{this};
    rw<ac_line, 1315, int, no_unit, no_unit> n_loss_segments{this};
    rw<ac_line, 1316, double, no_unit, no_unit> loss_factor_linear{this};
    rw<ac_line, 1317, double, no_unit, no_unit> loss_factor_quadratic{this};
    ro<ac_line, 1318, ::shyft::time_series::dd::apoint_ts, no_unit, mw> flow{this};
    ro<ac_line, 1319, ::shyft::time_series::dd::apoint_ts, no_unit, mw> forward_loss{this};
    ro<ac_line, 1320, ::shyft::time_series::dd::apoint_ts, no_unit, mw> backward_loss{this};
  };

  template <class A>
  struct dc_line : obj<A, 32> {
    using super = obj<A, 32>;
    dc_line() = default;

    dc_line(A* s, int oid)
      : super(s, oid) {
    }

    dc_line(dc_line const & o)
      : super(o) {
    }

    dc_line(dc_line&& o) noexcept
      : super(std::move(o)) {
    }

    dc_line& operator=(dc_line const & o) {
      super::operator=(o);
      return *this;
    }

    dc_line& operator=(dc_line&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<dc_line, 1321, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_forward_flow{this};
    rw<dc_line, 1322, ::shyft::time_series::dd::apoint_ts, no_unit, mw> max_backward_flow{this};
    rw<dc_line, 1323, int, no_unit, no_unit> n_loss_segments{this};
    rw<dc_line, 1324, double, no_unit, no_unit> loss_constant{this};
    rw<dc_line, 1325, double, no_unit, no_unit> loss_factor_linear{this};
    rw<dc_line, 1326, double, no_unit, no_unit> loss_factor_quadratic{this};
    ro<dc_line, 1327, ::shyft::time_series::dd::apoint_ts, no_unit, mw> flow{this};
    ro<dc_line, 1328, ::shyft::time_series::dd::apoint_ts, no_unit, mw> forward_loss{this};
    ro<dc_line, 1329, ::shyft::time_series::dd::apoint_ts, no_unit, mw> backward_loss{this};
  };

  template <class A>
  struct gen_reserve_capability : obj<A, 33> {
    using super = obj<A, 33>;
    gen_reserve_capability() = default;

    gen_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    gen_reserve_capability(gen_reserve_capability const & o)
      : super(o) {
    }

    gen_reserve_capability(gen_reserve_capability&& o) noexcept
      : super(std::move(o)) {
    }

    gen_reserve_capability& operator=(gen_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    gen_reserve_capability& operator=(gen_reserve_capability&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<gen_reserve_capability, 1375, std::string, no_unit, no_unit> reserve_type_name{this};
    rw<gen_reserve_capability, 1376, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_extended{this};
    rw<gen_reserve_capability, 1377, ::shyft::time_series::dd::apoint_ts, no_unit, mw> schedule{this};
  };

  template <class A>
  struct pump_reserve_capability : obj<A, 34> {
    using super = obj<A, 34>;
    pump_reserve_capability() = default;

    pump_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    pump_reserve_capability(pump_reserve_capability const & o)
      : super(o) {
    }

    pump_reserve_capability(pump_reserve_capability&& o) noexcept
      : super(std::move(o)) {
    }

    pump_reserve_capability& operator=(pump_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    pump_reserve_capability& operator=(pump_reserve_capability&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<pump_reserve_capability, 1378, std::string, no_unit, no_unit> reserve_type_name{this};
    rw<pump_reserve_capability, 1379, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_extended{this};
    rw<pump_reserve_capability, 1380, ::shyft::time_series::dd::apoint_ts, no_unit, mw> schedule{this};
  };

  template <class A>
  struct plant_reserve_capability : obj<A, 35> {
    using super = obj<A, 35>;
    plant_reserve_capability() = default;

    plant_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    plant_reserve_capability(plant_reserve_capability const & o)
      : super(o) {
    }

    plant_reserve_capability(plant_reserve_capability&& o) noexcept
      : super(std::move(o)) {
    }

    plant_reserve_capability& operator=(plant_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    plant_reserve_capability& operator=(plant_reserve_capability&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<plant_reserve_capability, 1381, std::string, no_unit, no_unit> reserve_type_name{this};
    rw<plant_reserve_capability, 1382, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_gen_extended{this};
    rw<plant_reserve_capability, 1383, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_pump_extended{this};
    rw<plant_reserve_capability, 1384, ::shyft::time_series::dd::apoint_ts, no_unit, mw> schedule{this};
  };

  template <class A>
  struct needle_comb_reserve_capability : obj<A, 36> {
    using super = obj<A, 36>;
    needle_comb_reserve_capability() = default;

    needle_comb_reserve_capability(A* s, int oid)
      : super(s, oid) {
    }

    needle_comb_reserve_capability(needle_comb_reserve_capability const & o)
      : super(o) {
    }

    needle_comb_reserve_capability(needle_comb_reserve_capability&& o) noexcept
      : super(std::move(o)) {
    }

    needle_comb_reserve_capability& operator=(needle_comb_reserve_capability const & o) {
      super::operator=(o);
      return *this;
    }

    needle_comb_reserve_capability& operator=(needle_comb_reserve_capability&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<needle_comb_reserve_capability, 1385, std::string, no_unit, no_unit> reserve_type_name{this};
    rw<needle_comb_reserve_capability, 1386, ::shyft::time_series::dd::apoint_ts, no_unit, mw> p_extended{this};
  };

  template <class A>
  struct battery : obj<A, 37> {
    using super = obj<A, 37>;
    battery() = default;

    battery(A* s, int oid)
      : super(s, oid) {
    }

    battery(battery const & o)
      : super(o) {
    }

    battery(battery&& o) noexcept
      : super(std::move(o)) {
    }

    battery& operator=(battery const & o) {
      super::operator=(o);
      return *this;
    }

    battery& operator=(battery&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<battery, 1330, double, no_unit, no_unit> charge_efficiency{this};
    rw<battery, 1331, double, no_unit, no_unit> discharge_efficiency{this};
    rw<battery, 1332, double, mw, mw> max_charge_power{this};
    rw<battery, 1333, double, mw, mw> max_discharge_power{this};
    rw<battery, 1334, double, mwh, mwh> max_energy{this};
    rw<battery, 1335, double, mwh, mwh> initial_energy{this};
    rw<battery, 1336, double, nok_per_mwh, nok_per_mwh> discharge_cost{this};
    rw<battery, 1337, double, nok_per_mwh, nok_per_mwh> charge_cost{this};
    rw<battery, 1338, ::shyft::time_series::dd::apoint_ts, mw, mw> max_charge_power_constraint{this};
    rw<battery, 1339, ::shyft::time_series::dd::apoint_ts, mw, mw> max_discharge_power_constraint{this};
    rw<battery, 1340, ::shyft::time_series::dd::apoint_ts, mwh, mwh> max_energy_constraint{this};
    rw<battery, 1341, ::shyft::time_series::dd::apoint_ts, mwh, mwh> min_energy_constraint{this};
    rw<battery, 1342, ::shyft::time_series::dd::apoint_ts, no_unit, no_unit> charge_discharge_mip{this};
    rw<battery, 1343, ::shyft::time_series::dd::apoint_ts, mw, mw> charge_schedule{this};
    rw<battery, 1344, ::shyft::time_series::dd::apoint_ts, mw, mw> discharge_schedule{this};
    rw<battery, 1345, ::shyft::time_series::dd::apoint_ts, mw, mw> net_discharge_schedule{this};
    rw<battery, 1346, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_up_cost{this};
    rw<battery, 1347, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_n_down_cost{this};
    rw<battery, 1348, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_up_cost{this};
    rw<battery, 1349, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> fcr_d_down_cost{this};
    rw<battery, 1350, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_up_cost{this};
    rw<battery, 1351, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> frr_down_cost{this};
    rw<battery, 1352, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_up_cost{this};
    rw<battery, 1353, ::shyft::time_series::dd::apoint_ts, no_unit, nok_per_mw> rr_down_cost{this};
    ro<battery, 1354, ::shyft::time_series::dd::apoint_ts, mw, mw> power_charge{this};
    ro<battery, 1355, ::shyft::time_series::dd::apoint_ts, mw, mw> power_discharge{this};
    ro<battery, 1356, ::shyft::time_series::dd::apoint_ts, mw, mw> net_power_discharge{this};
    ro<battery, 1357, ::shyft::time_series::dd::apoint_ts, mwh, mwh> energy{this};
    ro<battery, 1358, ::shyft::time_series::dd::apoint_ts, nok_per_mwh, nok_per_mwh> energy_value{this};
    ro<battery, 1359, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_up_delivery{this};
    ro<battery, 1360, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_n_down_delivery{this};
    ro<battery, 1361, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_up_delivery{this};
    ro<battery, 1362, ::shyft::time_series::dd::apoint_ts, no_unit, mw> fcr_d_down_delivery{this};
    ro<battery, 1363, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_up_delivery{this};
    ro<battery, 1364, ::shyft::time_series::dd::apoint_ts, no_unit, mw> frr_down_delivery{this};
    ro<battery, 1365, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_up_delivery{this};
    ro<battery, 1366, ::shyft::time_series::dd::apoint_ts, no_unit, mw> rr_down_delivery{this};
  };

  template <class A>
  struct wind : obj<A, 38> {
    using super = obj<A, 38>;
    wind() = default;

    wind(A* s, int oid)
      : super(s, oid) {
    }

    wind(wind const & o)
      : super(o) {
    }

    wind(wind&& o) noexcept
      : super(std::move(o)) {
    }

    wind& operator=(wind const & o) {
      super::operator=(o);
      return *this;
    }

    wind& operator=(wind&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<wind, 1367, ::shyft::time_series::dd::apoint_ts, mw, mw> power_forecast{this};
    rw<wind, 1368, ::shyft::time_series::dd::apoint_ts, nok_per_mwh, nok_per_mwh> curtailment_cost{this};
    ro<wind, 1369, ::shyft::time_series::dd::apoint_ts, mw, mw> power_delivered{this};
    ro<wind, 1370, ::shyft::time_series::dd::apoint_ts, mw, mw> power_curtailed{this};
  };

  template <class A>
  struct solar : obj<A, 39> {
    using super = obj<A, 39>;
    solar() = default;

    solar(A* s, int oid)
      : super(s, oid) {
    }

    solar(solar const & o)
      : super(o) {
    }

    solar(solar&& o) noexcept
      : super(std::move(o)) {
    }

    solar& operator=(solar const & o) {
      super::operator=(o);
      return *this;
    }

    solar& operator=(solar&& o) noexcept {
      super::operator=(std::move(o));
      return *this;
    }

    // attributes
    enum relation {
      connection_standard
    };

    static constexpr char const * relation_name(relation R) {
      switch (R) {
      case relation::connection_standard:
        return "connection_standard";
      default:
        return nullptr;
      }
    }

    rw<solar, 1371, ::shyft::time_series::dd::apoint_ts, mw, mw> power_forecast{this};
    rw<solar, 1372, ::shyft::time_series::dd::apoint_ts, nok_per_mwh, nok_per_mwh> curtailment_cost{this};
    ro<solar, 1373, ::shyft::time_series::dd::apoint_ts, mw, mw> power_delivered{this};
    ro<solar, 1374, ::shyft::time_series::dd::apoint_ts, mw, mw> power_curtailed{this};
  };

}
