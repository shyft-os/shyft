#include <boost/phoenix.hpp>
#include <boost/spirit/include/qi.hpp>

#include <shyft/energy_market/stm/shop/api/shop_proxy.h>

namespace shop::data {
  namespace core = shyft::core;
  namespace qi = boost::spirit::qi;
  namespace phx = boost::phoenix;

  /***
   * @brief parses precisely a YYYYMMDDhhmmss000 format
   *
   * @param t17c
   * @return utctime interpreted or throws
   */
  core::utctime parse_17c(std::string_view const t17c) {
    using qi::_1, qi::_2, qi::_3, qi::_4, qi::_5, qi::_6, qi::lit;
    static qi::uint_parser<unsigned, 10, 4, 4> const d4_; ///< takes exactly dddd
    static qi::uint_parser<unsigned, 10, 2, 2> const d2_; ///< takes exactly dd
    static auto mk_time = [](unsigned Y, unsigned M, unsigned D, unsigned h, unsigned m, unsigned s) {
      static core::calendar utc;
      return utc.time(Y, M, D, h, m, s);
    };
    // a declarative stateless const grammar for the wanted format
    static qi::rule<std::string_view::const_iterator, core::utctime()> const t17c_grammar =
      (d4_ >> d2_ >> d2_ >> d2_ >> d2_ >> d2_ >> lit("000"))[qi::_val = phx::bind(mk_time, _1, _2, _3, _4, _5, _6)];
    core::utctime r;
    if (!qi::parse(t17c.begin(), t17c.end(), t17c_grammar, r))
      throw std::runtime_error(fmt::format("shop_time.rep invalid format:{}", t17c));
    return r;
  }

  shop_time::operator std::time_t() const {
    return core::to_seconds64(parse_17c(rep));
  }
}