#pragma once
#include <tuple>

// NOTE: auto generated file
namespace shop::enums {
  enum class shop_object_type {
    reservoir,
    plant,
    generator,
    needle_combination,
    pump,
    gate,
    thermal,
    junction,
    junction_gate,
    creek_intake,
    contract,
    market,
    global_settings,
    reserve_group,
    commit_group,
    discharge_group,
    production_group,
    volume_constraint,
    scenario,
    objective,
    bid_group,
    cut_group,
    inflow_series,
    opt_system,
    unit_combination,
    tunnel,
    interlock_constraint,
    flow_constraint,
    lp_model,
    river,
    busbar,
    ac_line,
    dc_line,
    gen_reserve_capability,
    pump_reserve_capability,
    plant_reserve_capability,
    needle_comb_reserve_capability,
    battery,
    wind,
    solar
  };
  using shop_object_type_info_t = std::tuple<shop_object_type, char const * const>;
  // clang-format off
static constexpr shop_object_type_info_t shop_object_type_info[] {
  { shop_object_type::reservoir, "reservoir"},
  { shop_object_type::plant, "plant"},
  { shop_object_type::generator, "generator"},
  { shop_object_type::needle_combination, "needle_combination"},
  { shop_object_type::pump, "pump"},
  { shop_object_type::gate, "gate"},
  { shop_object_type::thermal, "thermal"},
  { shop_object_type::junction, "junction"},
  { shop_object_type::junction_gate, "junction_gate"},
  { shop_object_type::creek_intake, "creek_intake"},
  { shop_object_type::contract, "contract"},
  { shop_object_type::market, "market"},
  { shop_object_type::global_settings, "global_settings"},
  { shop_object_type::reserve_group, "reserve_group"},
  { shop_object_type::commit_group, "commit_group"},
  { shop_object_type::discharge_group, "discharge_group"},
  { shop_object_type::production_group, "production_group"},
  { shop_object_type::volume_constraint, "volume_constraint"},
  { shop_object_type::scenario, "scenario"},
  { shop_object_type::objective, "objective"},
  { shop_object_type::bid_group, "bid_group"},
  { shop_object_type::cut_group, "cut_group"},
  { shop_object_type::inflow_series, "inflow_series"},
  { shop_object_type::opt_system, "system"},
  { shop_object_type::unit_combination, "unit_combination"},
  { shop_object_type::tunnel, "tunnel"},
  { shop_object_type::interlock_constraint, "interlock_constraint"},
  { shop_object_type::flow_constraint, "flow_constraint"},
  { shop_object_type::lp_model, "lp_model"},
  { shop_object_type::river, "river"},
  { shop_object_type::busbar, "busbar"},
  { shop_object_type::ac_line, "ac_line"},
  { shop_object_type::dc_line, "dc_line"},
  { shop_object_type::gen_reserve_capability, "gen_reserve_capability"},
  { shop_object_type::pump_reserve_capability, "pump_reserve_capability"},
  { shop_object_type::plant_reserve_capability, "plant_reserve_capability"},
  { shop_object_type::needle_comb_reserve_capability, "needle_comb_reserve_capability"},
  { shop_object_type::battery, "battery"},
  { shop_object_type::wind, "wind"},
  { shop_object_type::solar, "solar"}
};
  // clang-format on

  enum class shop_data_type {
    shop_double,
    shop_double_array,
    shop_int,
    shop_int_array,
    shop_string,
    shop_string_array,
    shop_sy,
    shop_txy,
    shop_xy,
    shop_xy_array,
    shop_xyt
  };
  using shop_data_type_info_t = std::tuple<shop_data_type, char const * const>;
  // clang-format off
static constexpr shop_data_type_info_t shop_data_type_info[] {
  { shop_data_type::shop_double, "double"},
  { shop_data_type::shop_double_array, "double_array"},
  { shop_data_type::shop_int, "int"},
  { shop_data_type::shop_int_array, "int_array"},
  { shop_data_type::shop_string, "string"},
  { shop_data_type::shop_string_array, "string_array"},
  { shop_data_type::shop_sy, "sy"},
  { shop_data_type::shop_txy, "txy"},
  { shop_data_type::shop_xy, "xy"},
  { shop_data_type::shop_xy_array, "xy_array"},
  { shop_data_type::shop_xyt, "xyt"}
};
  // clang-format on

  enum class shop_x_unit {
    percent,
    delta_meter,
    hour,
    kwh_per_mm3,
    m3_per_s,
    m3sec_hour,
    meter,
    minute,
    mm3,
    mw,
    mwh,
    mwh_per_mm3,
    nok,
    nok_per_h_per_m3_per_s,
    nok_per_m3_per_s,
    nok_per_meter,
    nok_per_mm3,
    nok_per_mm3h,
    nok_per_mw,
    nok_per_mwh,
    no_unit,
    s2_per_m5,
    second
  };
  using shop_x_unit_info_t = std::tuple<shop_x_unit, char const * const>;
  // clang-format off
static constexpr shop_x_unit_info_t shop_x_unit_info[] {
  { shop_x_unit::percent, "%"},
  { shop_x_unit::delta_meter, "DELTA_METER"},
  { shop_x_unit::hour, "HOUR"},
  { shop_x_unit::kwh_per_mm3, "KWH/MM3"},
  { shop_x_unit::m3_per_s, "M3/S"},
  { shop_x_unit::m3sec_hour, "M3SEC_HOUR"},
  { shop_x_unit::meter, "METER"},
  { shop_x_unit::minute, "MINUTE"},
  { shop_x_unit::mm3, "MM3"},
  { shop_x_unit::mw, "MW"},
  { shop_x_unit::mwh, "MWH"},
  { shop_x_unit::mwh_per_mm3, "MWH/MM3"},
  { shop_x_unit::nok, "NOK"},
  { shop_x_unit::nok_per_h_per_m3_per_s, "NOK/H/M3/S"},
  { shop_x_unit::nok_per_m3_per_s, "NOK/M3/S"},
  { shop_x_unit::nok_per_meter, "NOK/METER"},
  { shop_x_unit::nok_per_mm3, "NOK/MM3"},
  { shop_x_unit::nok_per_mm3h, "NOK/MM3H"},
  { shop_x_unit::nok_per_mw, "NOK/MW"},
  { shop_x_unit::nok_per_mwh, "NOK/MWH"},
  { shop_x_unit::no_unit, "NO_UNIT"},
  { shop_x_unit::s2_per_m5, "S2/M5"},
  { shop_x_unit::second, "SECOND"}
};
  // clang-format on

  enum class shop_y_unit {
    percent,
    delta_meter,
    hour,
    km2,
    kwh_per_mm3,
    m3_per_s,
    m3sec_hour,
    meter,
    meter_per_hour,
    minute,
    mm3,
    mm3_per_hour,
    mw,
    mwh,
    mwh_per_mm3,
    mw_hour,
    nok,
    nok_per_h_per_m3_per_s,
    nok_per_m3_per_s,
    nok_per_meter,
    nok_per_meter_hour,
    nok_per_mm3,
    nok_per_mm3h,
    nok_per_mw,
    nok_per_mwh,
    no_unit,
    s2_per_m5,
    second
  };
  using shop_y_unit_info_t = std::tuple<shop_y_unit, char const * const>;
  // clang-format off
static constexpr shop_y_unit_info_t shop_y_unit_info[] {
  { shop_y_unit::percent, "%"},
  { shop_y_unit::delta_meter, "DELTA_METER"},
  { shop_y_unit::hour, "HOUR"},
  { shop_y_unit::km2, "KM2"},
  { shop_y_unit::kwh_per_mm3, "KWH/MM3"},
  { shop_y_unit::m3_per_s, "M3/S"},
  { shop_y_unit::m3sec_hour, "M3SEC_HOUR"},
  { shop_y_unit::meter, "METER"},
  { shop_y_unit::meter_per_hour, "METER/HOUR"},
  { shop_y_unit::minute, "MINUTE"},
  { shop_y_unit::mm3, "MM3"},
  { shop_y_unit::mm3_per_hour, "MM3/HOUR"},
  { shop_y_unit::mw, "MW"},
  { shop_y_unit::mwh, "MWH"},
  { shop_y_unit::mwh_per_mm3, "MWH/MM3"},
  { shop_y_unit::mw_hour, "MW_HOUR"},
  { shop_y_unit::nok, "NOK"},
  { shop_y_unit::nok_per_h_per_m3_per_s, "NOK/H/M3/S"},
  { shop_y_unit::nok_per_m3_per_s, "NOK/M3/S"},
  { shop_y_unit::nok_per_meter, "NOK/METER"},
  { shop_y_unit::nok_per_meter_hour, "NOK/METER_HOUR"},
  { shop_y_unit::nok_per_mm3, "NOK/MM3"},
  { shop_y_unit::nok_per_mm3h, "NOK/MM3H"},
  { shop_y_unit::nok_per_mw, "NOK/MW"},
  { shop_y_unit::nok_per_mwh, "NOK/MWH"},
  { shop_y_unit::no_unit, "NO_UNIT"},
  { shop_y_unit::s2_per_m5, "S2/M5"},
  { shop_y_unit::second, "SECOND"}
};
  // clang-format on

  struct shop_attribute_info {
    shop_data_type data_type;
    shop_x_unit x_unit;
    shop_y_unit y_unit;
    bool is_object_parameter;
    bool is_input;
    bool is_output;
  };

  enum class shop_reservoir_attribute {
    energy_value_converted = 3,
    latitude = 4,
    longitude = 5,
    max_vol = 6,
    lrl = 7,
    hrl = 8,
    vol_head = 9,
    head_area = 10,
    elevation_adjustment = 11,
    start_vol = 12,
    start_head = 13,
    inflow = 14,
    inflow_flag = 15,
    sim_inflow_flag = 16,
    flow_descr = 17,
    overflow_mip_flag = 18,
    overflow_cost = 19,
    overflow_cost_flag = 20,
    min_vol_constr = 21,
    min_vol_constr_flag = 22,
    max_vol_constr = 23,
    max_vol_constr_flag = 24,
    min_head_constr = 25,
    min_head_constr_flag = 26,
    max_head_constr = 27,
    max_head_constr_flag = 28,
    tactical_limit_min = 29,
    tactical_limit_min_flag = 30,
    tactical_cost_min = 31,
    tactical_cost_min_flag = 32,
    tactical_limit_max = 33,
    tactical_limit_max_flag = 34,
    tactical_cost_max = 35,
    tactical_cost_max_flag = 36,
    tactical_level_limit_min = 37,
    tactical_level_limit_max = 38,
    upper_slack = 39,
    lower_slack = 40,
    schedule = 41,
    schedule_flag = 42,
    volume_schedule = 43,
    level_schedule = 44,
    volume_ramping_up = 45,
    volume_ramping_down = 46,
    level_ramping_up = 47,
    level_ramping_down = 48,
    water_value_input = 49,
    energy_value_input = 50,
    peak_volume_cost_curve = 51,
    flood_volume_cost_curve = 52,
    evaporation_rate = 53,
    level_rolling_ramping_penalty_cost = 54,
    level_period_ramping_penalty_cost = 55,
    average_level_rolling_ramping_up = 56,
    average_level_rolling_ramping_down = 57,
    limit_level_rolling_ramping_up = 58,
    limit_level_rolling_ramping_down = 59,
    average_level_period_ramping_up = 60,
    average_level_period_ramping_down = 61,
    limit_level_period_ramping_up = 62,
    limit_level_period_ramping_down = 63,
    historical_level = 64,
    average_level_period_ramping_up_offset = 65,
    average_level_period_ramping_down_offset = 66,
    limit_level_period_ramping_up_offset = 67,
    limit_level_period_ramping_down_offset = 68,
    volume_nonseq_ramping_limit_up = 69,
    volume_nonseq_ramping_limit_down = 70,
    volume_amplitude_ramping_limit_up = 71,
    volume_amplitude_ramping_limit_down = 72,
    level_nonseq_ramping_limit_up = 73,
    level_nonseq_ramping_limit_down = 74,
    level_amplitude_ramping_limit_up = 75,
    level_amplitude_ramping_limit_down = 76,
    storage = 77,
    sim_storage = 78,
    head = 79,
    area = 80,
    sim_head = 81,
    sim_inflow = 82,
    endpoint_penalty = 83,
    penalty = 84,
    tactical_penalty_up = 85,
    tactical_penalty_down = 86,
    end_penalty = 87,
    penalty_nok = 88,
    tactical_penalty = 89,
    water_value_global_result = 90,
    water_value_local_result = 91,
    energy_value_local_result = 92,
    end_value = 93,
    change_in_end_value = 94,
    vow_in_transit = 95,
    calc_global_water_value = 96,
    energy_conversion_factor = 97,
    water_value_cut_result = 98,
    added_to_network = 99,
    network_no = 100,
    peak_volume_penalty = 101,
    flood_volume_penalty = 102,
    linearized_level = 103
  };
  using reservoir_attribute_info_t = std::tuple<shop_reservoir_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr reservoir_attribute_info_t shop_reservoir_attribute_info[] {
  { shop_reservoir_attribute::energy_value_converted, "energy_value_converted", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::latitude, "latitude", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::longitude, "longitude", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::max_vol, "max_vol", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::lrl, "lrl", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::hrl, "hrl", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::vol_head, "vol_head", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::head_area, "head_area", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::km2, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::elevation_adjustment, "elevation_adjustment", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::start_vol, "start_vol", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::start_head, "start_head", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::inflow, "inflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::inflow_flag, "inflow_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::sim_inflow_flag, "sim_inflow_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::flow_descr, "flow_descr", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::overflow_mip_flag, "overflow_mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::overflow_cost, "overflow_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::overflow_cost_flag, "overflow_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::min_vol_constr, "min_vol_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::min_vol_constr_flag, "min_vol_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::max_vol_constr, "max_vol_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::max_vol_constr_flag, "max_vol_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::min_head_constr, "min_head_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::min_head_constr_flag, "min_head_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::max_head_constr, "max_head_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::max_head_constr_flag, "max_head_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_limit_min, "tactical_limit_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_limit_min_flag, "tactical_limit_min_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_cost_min, "tactical_cost_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3h, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_cost_min_flag, "tactical_cost_min_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_limit_max, "tactical_limit_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_limit_max_flag, "tactical_limit_max_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_cost_max, "tactical_cost_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3h, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_cost_max_flag, "tactical_cost_max_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_level_limit_min, "tactical_level_limit_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::tactical_level_limit_max, "tactical_level_limit_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::upper_slack, "upper_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::lower_slack, "lower_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::schedule, "schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::schedule_flag, "schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_schedule, "volume_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_schedule, "level_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_ramping_up, "volume_ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3_per_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_ramping_down, "volume_ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3_per_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_ramping_up, "level_ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter_per_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_ramping_down, "level_ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter_per_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::water_value_input, "water_value_input", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::energy_value_input, "energy_value_input", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::peak_volume_cost_curve, "peak_volume_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::flood_volume_cost_curve, "flood_volume_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::evaporation_rate, "evaporation_rate", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter_per_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_rolling_ramping_penalty_cost, "level_rolling_ramping_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_period_ramping_penalty_cost, "level_period_ramping_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::average_level_rolling_ramping_up, "average_level_rolling_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::average_level_rolling_ramping_down, "average_level_rolling_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::limit_level_rolling_ramping_up, "limit_level_rolling_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::limit_level_rolling_ramping_down, "limit_level_rolling_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::average_level_period_ramping_up, "average_level_period_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::average_level_period_ramping_down, "average_level_period_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::limit_level_period_ramping_up, "limit_level_period_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::limit_level_period_ramping_down, "limit_level_period_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::historical_level, "historical_level", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::average_level_period_ramping_up_offset, "average_level_period_ramping_up_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::average_level_period_ramping_down_offset, "average_level_period_ramping_down_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::limit_level_period_ramping_up_offset, "limit_level_period_ramping_up_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::limit_level_period_ramping_down_offset, "limit_level_period_ramping_down_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_nonseq_ramping_limit_up, "volume_nonseq_ramping_limit_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_nonseq_ramping_limit_down, "volume_nonseq_ramping_limit_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_amplitude_ramping_limit_up, "volume_amplitude_ramping_limit_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::volume_amplitude_ramping_limit_down, "volume_amplitude_ramping_limit_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_nonseq_ramping_limit_up, "level_nonseq_ramping_limit_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_nonseq_ramping_limit_down, "level_nonseq_ramping_limit_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_amplitude_ramping_limit_up, "level_amplitude_ramping_limit_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::level_amplitude_ramping_limit_down, "level_amplitude_ramping_limit_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reservoir_attribute::storage, "storage", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::sim_storage, "sim_storage", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::head, "head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::area, "area", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::km2, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::sim_head, "sim_head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::sim_inflow, "sim_inflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::endpoint_penalty, "endpoint_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::penalty, "penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::tactical_penalty_up, "tactical_penalty_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::tactical_penalty_down, "tactical_penalty_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::end_penalty, "end_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::penalty_nok, "penalty_nok", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::tactical_penalty, "tactical_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::water_value_global_result, "water_value_global_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::water_value_local_result, "water_value_local_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::energy_value_local_result, "energy_value_local_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::end_value, "end_value", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::change_in_end_value, "change_in_end_value", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::vow_in_transit, "vow_in_transit", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::calc_global_water_value, "calc_global_water_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::energy_conversion_factor, "energy_conversion_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mwh_per_mm3, .y_unit = shop_y_unit::mwh_per_mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::water_value_cut_result, "water_value_cut_result", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::added_to_network, "added_to_network", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::network_no, "network_no", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::peak_volume_penalty, "peak_volume_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::flood_volume_penalty, "flood_volume_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reservoir_attribute::linearized_level, "linearized_level", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_plant_attribute {
    num_gen = 108,
    num_pump = 109,
    less_distribution_eps = 112,
    latitude = 119,
    longitude = 120,
    power_head_optimization_factor = 121,
    ownership = 122,
    prod_area = 123,
    prod_area_flag = 124,
    prod_factor = 125,
    outlet_line = 126,
    intake_line = 127,
    main_loss = 128,
    penstock_loss = 129,
    tailrace_loss = 130,
    tailrace_loss_from_bypass_flag = 131,
    intake_loss = 132,
    intake_loss_from_bypass_flag = 133,
    tides = 134,
    time_delay = 135,
    shape_discharge = 136,
    discharge_fee = 137,
    discharge_fee_flag = 138,
    discharge_cost_curve = 139,
    feeding_fee = 140,
    feeding_fee_flag = 141,
    production_fee = 142,
    production_fee_flag = 143,
    consumption_fee = 144,
    consumption_fee_flag = 145,
    linear_startup_flag = 146,
    maintenance_flag = 147,
    mip_flag = 148,
    mip_length = 149,
    mip_length_flag = 150,
    gen_priority = 151,
    n_seg_down = 152,
    n_seg_up = 153,
    n_mip_seg_down = 154,
    n_mip_seg_up = 155,
    dyn_pq_seg_flag = 156,
    dyn_mip_pq_seg_flag = 157,
    build_original_pq_curves_by_discharge_limits = 158,
    min_p_constr = 159,
    min_p_constr_flag = 160,
    min_p_penalty_flag = 161,
    min_p_penalty_cost = 162,
    min_p_penalty_cost_flag = 163,
    max_p_constr = 164,
    max_p_constr_flag = 165,
    max_p_penalty_flag = 166,
    max_p_penalty_cost = 167,
    max_p_penalty_cost_flag = 168,
    min_q_constr = 169,
    min_q_constr_flag = 170,
    min_q_penalty_flag = 171,
    min_q_penalty_cost = 172,
    min_q_penalty_cost_flag = 173,
    max_q_constr = 174,
    max_q_constr_flag = 175,
    max_q_penalty_flag = 176,
    max_q_penalty_cost = 177,
    max_q_penalty_cost_flag = 178,
    max_q_limit_rsv_up = 179,
    max_q_limit_rsv_down = 180,
    production_schedule = 181,
    production_schedule_flag = 182,
    discharge_schedule = 183,
    discharge_schedule_flag = 184,
    consumption_schedule = 185,
    consumption_schedule_flag = 186,
    upflow_schedule = 187,
    upflow_schedule_flag = 188,
    sched_penalty_cost_down = 189,
    sched_penalty_cost_up = 190,
    power_ramping_up = 191,
    power_ramping_down = 192,
    discharge_ramping_up = 193,
    discharge_ramping_down = 194,
    block_merge_tolerance = 195,
    block_generation_mwh = 196,
    block_generation_m3s = 197,
    frr_up_min = 198,
    frr_up_max = 199,
    frr_down_min = 200,
    frr_down_max = 201,
    rr_up_min = 202,
    rr_down_min = 203,
    frr_symmetric_flag = 204,
    bp_dyn_wv_flag = 205,
    ref_prod = 206,
    plant_unbalance_recommit = 207,
    spinning_reserve_up_max = 208,
    spinning_reserve_down_max = 209,
    ramping_steps = 210,
    discharge_rolling_ramping_penalty_cost = 211,
    discharge_period_ramping_penalty_cost = 212,
    average_discharge_rolling_ramping_up = 213,
    average_discharge_rolling_ramping_down = 214,
    limit_discharge_rolling_ramping_up = 215,
    limit_discharge_rolling_ramping_down = 216,
    average_discharge_period_ramping_up = 217,
    average_discharge_period_ramping_down = 218,
    limit_discharge_period_ramping_up = 219,
    limit_discharge_period_ramping_down = 220,
    historical_discharge = 221,
    average_discharge_period_ramping_up_offset = 222,
    average_discharge_period_ramping_down_offset = 223,
    limit_discharge_period_ramping_up_offset = 224,
    limit_discharge_period_ramping_down_offset = 225,
    fcr_n_up_activation_time = 226,
    fcr_n_down_activation_time = 227,
    fcr_d_up_activation_time = 228,
    fcr_d_down_activation_time = 229,
    frr_up_activation_time = 230,
    frr_down_activation_time = 231,
    rr_up_activation_time = 232,
    rr_down_activation_time = 233,
    fcr_n_up_activation_factor = 234,
    fcr_n_down_activation_factor = 235,
    fcr_d_up_activation_factor = 236,
    fcr_d_down_activation_factor = 237,
    frr_up_activation_factor = 238,
    frr_down_activation_factor = 239,
    rr_up_activation_factor = 240,
    rr_down_activation_factor = 241,
    fcr_n_up_activation_penalty_cost = 242,
    fcr_n_down_activation_penalty_cost = 243,
    fcr_d_up_activation_penalty_cost = 244,
    fcr_d_down_activation_penalty_cost = 245,
    frr_up_activation_penalty_cost = 246,
    frr_down_activation_penalty_cost = 247,
    rr_up_activation_penalty_cost = 248,
    rr_down_activation_penalty_cost = 249,
    sum_reserve_up_max = 250,
    sum_reserve_down_max = 251,
    max_q_reserve_constr = 252,
    min_q_reserve_constr = 253,
    max_q_reserve_constr_penalty_cost = 254,
    min_q_reserve_constr_penalty_cost = 255,
    sum_reserve_up_max_penalty_cost = 256,
    sum_reserve_down_max_penalty_cost = 257,
    reserve_tactical_activation_cost_scaling = 258,
    coupled_fcr_activation_flag = 259,
    ramping_step_reference_discharge = 260,
    min_uptime = 261,
    min_downtime = 262,
    min_uptime_flag = 263,
    min_downtime_flag = 264,
    historical_production = 265,
    historical_consumption = 266,
    simultanous_generating_pumping_flag = 267,
    max_prod_reserve_strategy = 268,
    min_prod_reserve_strategy = 269,
    max_prod_reserve_penalty_cost = 270,
    min_prod_reserve_penalty_cost = 271,
    strict_pq_uploading_flag = 272,
    strict_pq_uploading_upon_detection = 273,
    production = 274,
    solver_production = 275,
    sim_production = 276,
    prod_unbalance = 277,
    consumption = 278,
    sim_consumption = 279,
    solver_consumption = 280,
    cons_unbalance = 281,
    discharge = 282,
    solver_discharge = 283,
    sim_discharge = 284,
    upflow = 285,
    sim_upflow = 286,
    solver_upflow = 287,
    gross_head = 288,
    eff_head = 289,
    head_loss = 290,
    min_p_penalty = 291,
    max_p_penalty = 292,
    min_q_penalty = 293,
    max_q_penalty = 294,
    p_constr_penalty = 295,
    q_constr_penalty = 296,
    schedule_up_penalty = 297,
    schedule_down_penalty = 298,
    schedule_penalty = 299,
    max_prod_all = 300,
    max_prod_available = 301,
    max_prod_spinning = 302,
    min_prod_spinning = 303,
    best_profit_q = 304,
    best_profit_mc = 305,
    best_profit_ac = 306,
    best_profit_commitment_cost = 307,
    best_profit_bid_matrix = 308,
    times_of_wrong_pq_uploading = 309,
    max_prod_reserve_violation = 310,
    min_prod_reserve_violation = 311
  };
  using plant_attribute_info_t = std::tuple<shop_plant_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr plant_attribute_info_t shop_plant_attribute_info[] {
  { shop_plant_attribute::num_gen, "num_gen", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::num_pump, "num_pump", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::less_distribution_eps, "less_distribution_eps", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::latitude, "latitude", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::longitude, "longitude", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::power_head_optimization_factor, "power_head_optimization_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::ownership, "ownership", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::percent, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::prod_area, "prod_area", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::prod_area_flag, "prod_area_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::prod_factor, "prod_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::kwh_per_mm3, .y_unit = shop_y_unit::kwh_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::outlet_line, "outlet_line", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::intake_line, "intake_line", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::main_loss, "main_loss", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::penstock_loss, "penstock_loss", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::tailrace_loss, "tailrace_loss", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::tailrace_loss_from_bypass_flag, "tailrace_loss_from_bypass_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::intake_loss, "intake_loss", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::intake_loss_from_bypass_flag, "intake_loss_from_bypass_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::tides, "tides", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::delta_meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::time_delay, "time_delay", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::shape_discharge, "shape_discharge", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_fee, "discharge_fee", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_fee_flag, "discharge_fee_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_cost_curve, "discharge_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::feeding_fee, "feeding_fee", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::feeding_fee_flag, "feeding_fee_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::production_fee, "production_fee", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::production_fee_flag, "production_fee_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::consumption_fee, "consumption_fee", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::consumption_fee_flag, "consumption_fee_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::linear_startup_flag, "linear_startup_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::maintenance_flag, "maintenance_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::mip_flag, "mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::mip_length, "mip_length", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::mip_length_flag, "mip_length_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::gen_priority, "gen_priority", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::n_seg_down, "n_seg_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::n_seg_up, "n_seg_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::n_mip_seg_down, "n_mip_seg_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::n_mip_seg_up, "n_mip_seg_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::dyn_pq_seg_flag, "dyn_pq_seg_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::dyn_mip_pq_seg_flag, "dyn_mip_pq_seg_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::build_original_pq_curves_by_discharge_limits, "build_original_pq_curves_by_discharge_limits", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_p_constr, "min_p_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_p_constr_flag, "min_p_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_p_penalty_flag, "min_p_penalty_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_p_penalty_cost, "min_p_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_p_penalty_cost_flag, "min_p_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_p_constr, "max_p_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_p_constr_flag, "max_p_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_p_penalty_flag, "max_p_penalty_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_p_penalty_cost, "max_p_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_p_penalty_cost_flag, "max_p_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_constr, "min_q_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_constr_flag, "min_q_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_penalty_flag, "min_q_penalty_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_penalty_cost, "min_q_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_penalty_cost_flag, "min_q_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_constr, "max_q_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_constr_flag, "max_q_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_penalty_flag, "max_q_penalty_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_penalty_cost, "max_q_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_penalty_cost_flag, "max_q_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_limit_rsv_up, "max_q_limit_rsv_up", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_limit_rsv_down, "max_q_limit_rsv_down", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::production_schedule, "production_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::production_schedule_flag, "production_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_schedule, "discharge_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_schedule_flag, "discharge_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::consumption_schedule, "consumption_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::consumption_schedule_flag, "consumption_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::upflow_schedule, "upflow_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::upflow_schedule_flag, "upflow_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::sched_penalty_cost_down, "sched_penalty_cost_down", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::sched_penalty_cost_up, "sched_penalty_cost_up", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::power_ramping_up, "power_ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::power_ramping_down, "power_ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_ramping_up, "discharge_ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_ramping_down, "discharge_ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::block_merge_tolerance, "block_merge_tolerance", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::block_generation_mwh, "block_generation_mwh", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::block_generation_m3s, "block_generation_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_up_min, "frr_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_up_max, "frr_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_down_min, "frr_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_down_max, "frr_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_up_min, "rr_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_down_min, "rr_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_symmetric_flag, "frr_symmetric_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::bp_dyn_wv_flag, "bp_dyn_wv_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::ref_prod, "ref_prod", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::plant_unbalance_recommit, "plant_unbalance_recommit", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::spinning_reserve_up_max, "spinning_reserve_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::spinning_reserve_down_max, "spinning_reserve_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::ramping_steps, "ramping_steps", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_rolling_ramping_penalty_cost, "discharge_rolling_ramping_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::discharge_period_ramping_penalty_cost, "discharge_period_ramping_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::average_discharge_rolling_ramping_up, "average_discharge_rolling_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::average_discharge_rolling_ramping_down, "average_discharge_rolling_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::limit_discharge_rolling_ramping_up, "limit_discharge_rolling_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::limit_discharge_rolling_ramping_down, "limit_discharge_rolling_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::average_discharge_period_ramping_up, "average_discharge_period_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::average_discharge_period_ramping_down, "average_discharge_period_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::limit_discharge_period_ramping_up, "limit_discharge_period_ramping_up", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::limit_discharge_period_ramping_down, "limit_discharge_period_ramping_down", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::historical_discharge, "historical_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::average_discharge_period_ramping_up_offset, "average_discharge_period_ramping_up_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::average_discharge_period_ramping_down_offset, "average_discharge_period_ramping_down_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::limit_discharge_period_ramping_up_offset, "limit_discharge_period_ramping_up_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::limit_discharge_period_ramping_down_offset, "limit_discharge_period_ramping_down_offset", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_n_up_activation_time, "fcr_n_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_n_down_activation_time, "fcr_n_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_d_up_activation_time, "fcr_d_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_d_down_activation_time, "fcr_d_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_up_activation_time, "frr_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_down_activation_time, "frr_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_up_activation_time, "rr_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_down_activation_time, "rr_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_n_up_activation_factor, "fcr_n_up_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_n_down_activation_factor, "fcr_n_down_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_d_up_activation_factor, "fcr_d_up_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_d_down_activation_factor, "fcr_d_down_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_up_activation_factor, "frr_up_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_down_activation_factor, "frr_down_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_up_activation_factor, "rr_up_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_down_activation_factor, "rr_down_activation_factor", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_n_up_activation_penalty_cost, "fcr_n_up_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_n_down_activation_penalty_cost, "fcr_n_down_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_d_up_activation_penalty_cost, "fcr_d_up_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::fcr_d_down_activation_penalty_cost, "fcr_d_down_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_up_activation_penalty_cost, "frr_up_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::frr_down_activation_penalty_cost, "frr_down_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_up_activation_penalty_cost, "rr_up_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::rr_down_activation_penalty_cost, "rr_down_activation_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::sum_reserve_up_max, "sum_reserve_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::sum_reserve_down_max, "sum_reserve_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_reserve_constr, "max_q_reserve_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_reserve_constr, "min_q_reserve_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_q_reserve_constr_penalty_cost, "max_q_reserve_constr_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_q_reserve_constr_penalty_cost, "min_q_reserve_constr_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::sum_reserve_up_max_penalty_cost, "sum_reserve_up_max_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::sum_reserve_down_max_penalty_cost, "sum_reserve_down_max_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::reserve_tactical_activation_cost_scaling, "reserve_tactical_activation_cost_scaling", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::coupled_fcr_activation_flag, "coupled_fcr_activation_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::ramping_step_reference_discharge, "ramping_step_reference_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_uptime, "min_uptime", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_downtime, "min_downtime", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_uptime_flag, "min_uptime_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_downtime_flag, "min_downtime_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::historical_production, "historical_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::historical_consumption, "historical_consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::simultanous_generating_pumping_flag, "simultanous_generating_pumping_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_prod_reserve_strategy, "max_prod_reserve_strategy", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_prod_reserve_strategy, "min_prod_reserve_strategy", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::max_prod_reserve_penalty_cost, "max_prod_reserve_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::min_prod_reserve_penalty_cost, "min_prod_reserve_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::strict_pq_uploading_flag, "strict_pq_uploading_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::strict_pq_uploading_upon_detection, "strict_pq_uploading_upon_detection", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_attribute::production, "production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::solver_production, "solver_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::sim_production, "sim_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::prod_unbalance, "prod_unbalance", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::consumption, "consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::sim_consumption, "sim_consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::solver_consumption, "solver_consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::cons_unbalance, "cons_unbalance", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::discharge, "discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::solver_discharge, "solver_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::sim_discharge, "sim_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::upflow, "upflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::sim_upflow, "sim_upflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::solver_upflow, "solver_upflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::gross_head, "gross_head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::eff_head, "eff_head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::head_loss, "head_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::min_p_penalty, "min_p_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::max_p_penalty, "max_p_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::min_q_penalty, "min_q_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::max_q_penalty, "max_q_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::p_constr_penalty, "p_constr_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::q_constr_penalty, "q_constr_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::schedule_up_penalty, "schedule_up_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::schedule_down_penalty, "schedule_down_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::schedule_penalty, "schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::max_prod_all, "max_prod_all", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::max_prod_available, "max_prod_available", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::max_prod_spinning, "max_prod_spinning", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::min_prod_spinning, "min_prod_spinning", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::best_profit_q, "best_profit_q", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::best_profit_mc, "best_profit_mc", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::best_profit_ac, "best_profit_ac", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::best_profit_commitment_cost, "best_profit_commitment_cost", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::best_profit_bid_matrix, "best_profit_bid_matrix", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::times_of_wrong_pq_uploading, "times_of_wrong_pq_uploading", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::max_prod_reserve_violation, "max_prod_reserve_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_plant_attribute::min_prod_reserve_violation, "min_prod_reserve_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_generator_attribute {
    type = 319,
    num_needle_comb = 320,
    prod_area = 321,
    initial_state = 322,
    penstock = 323,
    separate_droop = 324,
    p_min = 325,
    p_max = 326,
    p_nom = 327,
    gen_eff_curve = 328,
    turb_eff_curves = 329,
    affinity_eq_flag = 330,
    maintenance_flag = 331,
    startcost = 332,
    stopcost = 333,
    discharge_cost_curve = 334,
    priority = 335,
    committed_in = 336,
    committed_flag = 337,
    min_p_constr = 338,
    min_p_constr_flag = 339,
    max_p_constr = 340,
    max_p_constr_flag = 341,
    min_q_constr = 342,
    min_q_constr_flag = 343,
    max_q_constr = 344,
    max_q_constr_flag = 345,
    max_q_limit_rsv_down = 346,
    upstream_min = 347,
    upstream_min_flag = 348,
    downstream_max = 349,
    downstream_max_flag = 350,
    production_schedule = 351,
    production_schedule_flag = 352,
    discharge_schedule = 353,
    discharge_schedule_flag = 354,
    fcr_mip_flag = 355,
    p_fcr_min = 356,
    p_fcr_n_min = 357,
    p_fcr_d_min = 358,
    p_fcr_max = 359,
    p_fcr_n_max = 360,
    p_fcr_d_max = 361,
    p_frr_min = 362,
    p_frr_max = 363,
    p_rr_min = 364,
    frr_up_min = 365,
    frr_up_max = 366,
    frr_down_min = 367,
    frr_down_max = 368,
    fcr_n_up_min = 369,
    fcr_n_up_max = 370,
    fcr_n_down_min = 371,
    fcr_n_down_max = 372,
    fcr_d_up_min = 373,
    fcr_d_up_max = 374,
    fcr_d_down_min = 375,
    fcr_d_down_max = 376,
    rr_up_min = 377,
    rr_up_max = 378,
    rr_down_min = 379,
    rr_down_max = 380,
    fcr_n_up_schedule = 381,
    fcr_n_up_schedule_flag = 382,
    fcr_n_down_schedule = 383,
    fcr_n_down_schedule_flag = 384,
    fcr_d_up_schedule = 385,
    fcr_d_up_schedule_flag = 386,
    fcr_d_down_schedule = 387,
    fcr_d_down_schedule_flag = 388,
    frr_up_schedule = 389,
    frr_up_schedule_flag = 390,
    frr_down_schedule = 391,
    frr_down_schedule_flag = 392,
    rr_up_schedule = 393,
    rr_up_schedule_flag = 394,
    rr_down_schedule = 395,
    rr_down_schedule_flag = 396,
    droop_cost = 397,
    fixed_droop = 398,
    fixed_droop_flag = 399,
    droop_min = 400,
    droop_max = 401,
    discrete_droop_values = 402,
    fcr_n_discrete_droop_values = 403,
    fcr_d_up_discrete_droop_values = 404,
    fcr_d_down_discrete_droop_values = 405,
    reserve_ramping_cost_up = 406,
    reserve_ramping_cost_down = 407,
    ref_production = 408,
    schedule_deviation_flag = 409,
    gen_turn_off_limit = 410,
    fcr_n_up_cost = 411,
    fcr_n_down_cost = 412,
    fcr_d_up_cost = 413,
    fcr_d_down_cost = 414,
    frr_up_cost = 415,
    frr_down_cost = 416,
    rr_up_cost = 417,
    rr_down_cost = 418,
    spinning_reserve_up_max = 419,
    spinning_reserve_down_max = 420,
    fcr_n_droop_cost = 421,
    fcr_d_up_droop_cost = 422,
    fcr_d_down_droop_cost = 423,
    fcr_n_fixed_droop = 424,
    fcr_d_up_fixed_droop = 425,
    fcr_d_down_fixed_droop = 426,
    fcr_n_droop_min = 427,
    fcr_d_up_droop_min = 428,
    fcr_d_down_droop_min = 429,
    fcr_n_droop_max = 430,
    fcr_d_up_droop_max = 431,
    fcr_d_down_droop_max = 432,
    couple_fcr_delivery_flag = 433,
    historical_production = 434,
    min_uptime = 435,
    min_downtime = 436,
    min_uptime_flag = 437,
    min_downtime_flag = 438,
    rr_down_stop_mip_flag = 439,
    min_discharge = 440,
    max_discharge = 441,
    eff_head = 442,
    sim_eff_head = 443,
    head_loss = 444,
    production = 445,
    solver_production = 446,
    sim_production = 447,
    discharge = 448,
    solver_discharge = 449,
    sim_discharge = 450,
    committed_out = 451,
    production_schedule_penalty = 452,
    discharge_schedule_penalty = 453,
    fcr_n_up_delivery = 454,
    fcr_n_down_delivery = 455,
    fcr_d_up_delivery = 456,
    fcr_d_down_delivery = 457,
    frr_up_delivery = 458,
    frr_down_delivery = 459,
    rr_up_delivery = 460,
    rr_down_delivery = 461,
    fcr_n_up_delivery_physical = 462,
    fcr_n_down_delivery_physical = 463,
    fcr_d_up_delivery_physical = 464,
    fcr_d_down_delivery_physical = 465,
    fcr_n_up_schedule_penalty = 466,
    fcr_n_down_schedule_penalty = 467,
    fcr_d_up_schedule_penalty = 468,
    fcr_d_down_schedule_penalty = 469,
    frr_up_schedule_penalty = 470,
    frr_down_schedule_penalty = 471,
    rr_up_schedule_penalty = 472,
    rr_down_schedule_penalty = 473,
    droop_result = 474,
    best_profit_q = 475,
    best_profit_p = 476,
    best_profit_dq_dp = 477,
    best_profit_needle_comb = 478,
    startup_cost_mip_objective = 479,
    startup_cost_total_objective = 480,
    discharge_fee_objective = 481,
    feeding_fee_objective = 482,
    schedule_penalty = 483,
    market_income = 484,
    original_pq_curves = 485,
    convex_pq_curves = 486,
    final_pq_curves = 487,
    max_prod_individual = 488,
    max_prod_all = 489,
    max_prod_available = 490,
    max_prod_spinning = 491,
    min_prod_individual = 492,
    min_prod_spinning = 493,
    fcr_n_droop_result = 494,
    fcr_d_up_droop_result = 495,
    fcr_d_down_droop_result = 496
  };
  using generator_attribute_info_t = std::tuple<shop_generator_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr generator_attribute_info_t shop_generator_attribute_info[] {
  { shop_generator_attribute::type, "type", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::num_needle_comb, "num_needle_comb", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::prod_area, "prod_area", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::initial_state, "initial_state", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::penstock, "penstock", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::separate_droop, "separate_droop", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_min, "p_min", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_max, "p_max", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_nom, "p_nom", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::gen_eff_curve, "gen_eff_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::turb_eff_curves, "turb_eff_curves", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::affinity_eq_flag, "affinity_eq_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::maintenance_flag, "maintenance_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::startcost, "startcost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::stopcost, "stopcost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::discharge_cost_curve, "discharge_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::priority, "priority", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::committed_in, "committed_in", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::committed_flag, "committed_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_p_constr, "min_p_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_p_constr_flag, "min_p_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::max_p_constr, "max_p_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::max_p_constr_flag, "max_p_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_q_constr, "min_q_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_q_constr_flag, "min_q_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::max_q_constr, "max_q_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::max_q_constr_flag, "max_q_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::max_q_limit_rsv_down, "max_q_limit_rsv_down", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::upstream_min, "upstream_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::upstream_min_flag, "upstream_min_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::downstream_max, "downstream_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::downstream_max_flag, "downstream_max_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::production_schedule, "production_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::production_schedule_flag, "production_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::discharge_schedule, "discharge_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::discharge_schedule_flag, "discharge_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_mip_flag, "fcr_mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_fcr_min, "p_fcr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_fcr_n_min, "p_fcr_n_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_fcr_d_min, "p_fcr_d_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_fcr_max, "p_fcr_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_fcr_n_max, "p_fcr_n_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_fcr_d_max, "p_fcr_d_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_frr_min, "p_frr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_frr_max, "p_frr_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::p_rr_min, "p_rr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_up_min, "frr_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_up_max, "frr_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_down_min, "frr_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_down_max, "frr_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_up_min, "fcr_n_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_up_max, "fcr_n_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_down_min, "fcr_n_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_down_max, "fcr_n_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_min, "fcr_d_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_max, "fcr_d_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_min, "fcr_d_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_max, "fcr_d_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_up_min, "rr_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_up_max, "rr_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_down_min, "rr_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_down_max, "rr_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_up_schedule, "fcr_n_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_up_schedule_flag, "fcr_n_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_down_schedule, "fcr_n_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_down_schedule_flag, "fcr_n_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_schedule, "fcr_d_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_schedule_flag, "fcr_d_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_schedule, "fcr_d_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_schedule_flag, "fcr_d_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_up_schedule, "frr_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_up_schedule_flag, "frr_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_down_schedule, "frr_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_down_schedule_flag, "frr_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_up_schedule, "rr_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_up_schedule_flag, "rr_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_down_schedule, "rr_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_down_schedule_flag, "rr_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::droop_cost, "droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fixed_droop, "fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fixed_droop_flag, "fixed_droop_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::droop_min, "droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::droop_max, "droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::discrete_droop_values, "discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_discrete_droop_values, "fcr_n_discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_discrete_droop_values, "fcr_d_up_discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_discrete_droop_values, "fcr_d_down_discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::reserve_ramping_cost_up, "reserve_ramping_cost_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::reserve_ramping_cost_down, "reserve_ramping_cost_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::ref_production, "ref_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::schedule_deviation_flag, "schedule_deviation_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::gen_turn_off_limit, "gen_turn_off_limit", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_up_cost, "fcr_n_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_down_cost, "fcr_n_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_cost, "fcr_d_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_cost, "fcr_d_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_up_cost, "frr_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::frr_down_cost, "frr_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_up_cost, "rr_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_down_cost, "rr_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::spinning_reserve_up_max, "spinning_reserve_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::spinning_reserve_down_max, "spinning_reserve_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_droop_cost, "fcr_n_droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_droop_cost, "fcr_d_up_droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_droop_cost, "fcr_d_down_droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_fixed_droop, "fcr_n_fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_fixed_droop, "fcr_d_up_fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_fixed_droop, "fcr_d_down_fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_droop_min, "fcr_n_droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_droop_min, "fcr_d_up_droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_droop_min, "fcr_d_down_droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_n_droop_max, "fcr_n_droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_up_droop_max, "fcr_d_up_droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::fcr_d_down_droop_max, "fcr_d_down_droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::couple_fcr_delivery_flag, "couple_fcr_delivery_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::historical_production, "historical_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_uptime, "min_uptime", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_downtime, "min_downtime", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_uptime_flag, "min_uptime_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_downtime_flag, "min_downtime_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::rr_down_stop_mip_flag, "rr_down_stop_mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::min_discharge, "min_discharge", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::max_discharge, "max_discharge", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_generator_attribute::eff_head, "eff_head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::sim_eff_head, "sim_eff_head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::head_loss, "head_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::production, "production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::solver_production, "solver_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::sim_production, "sim_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::discharge, "discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::solver_discharge, "solver_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::sim_discharge, "sim_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::committed_out, "committed_out", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::production_schedule_penalty, "production_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::discharge_schedule_penalty, "discharge_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_up_delivery, "fcr_n_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_down_delivery, "fcr_n_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_up_delivery, "fcr_d_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_down_delivery, "fcr_d_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::frr_up_delivery, "frr_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::frr_down_delivery, "frr_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::rr_up_delivery, "rr_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::rr_down_delivery, "rr_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_up_delivery_physical, "fcr_n_up_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_down_delivery_physical, "fcr_n_down_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_up_delivery_physical, "fcr_d_up_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_down_delivery_physical, "fcr_d_down_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_up_schedule_penalty, "fcr_n_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_down_schedule_penalty, "fcr_n_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_up_schedule_penalty, "fcr_d_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_down_schedule_penalty, "fcr_d_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::frr_up_schedule_penalty, "frr_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::frr_down_schedule_penalty, "frr_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::rr_up_schedule_penalty, "rr_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::rr_down_schedule_penalty, "rr_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::droop_result, "droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::best_profit_q, "best_profit_q", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::best_profit_p, "best_profit_p", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::best_profit_dq_dp, "best_profit_dq_dp", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::best_profit_needle_comb, "best_profit_needle_comb", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::startup_cost_mip_objective, "startup_cost_mip_objective", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::startup_cost_total_objective, "startup_cost_total_objective", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::discharge_fee_objective, "discharge_fee_objective", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::feeding_fee_objective, "feeding_fee_objective", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::schedule_penalty, "schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::market_income, "market_income", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::original_pq_curves, "original_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::convex_pq_curves, "convex_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::final_pq_curves, "final_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::max_prod_individual, "max_prod_individual", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::max_prod_all, "max_prod_all", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::max_prod_available, "max_prod_available", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::max_prod_spinning, "max_prod_spinning", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::min_prod_individual, "min_prod_individual", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::min_prod_spinning, "min_prod_spinning", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_n_droop_result, "fcr_n_droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_up_droop_result, "fcr_d_up_droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_generator_attribute::fcr_d_down_droop_result, "fcr_d_down_droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_needle_combination_attribute {
    p_max = 499,
    p_min = 500,
    p_nom = 501,
    turb_eff_curves = 502,
    p_fcr_min = 503,
    p_fcr_n_min = 504,
    p_fcr_d_min = 505,
    p_fcr_max = 506,
    p_fcr_n_max = 507,
    p_fcr_d_max = 508,
    p_frr_min = 509,
    p_frr_max = 510,
    production_cost = 511,
    min_discharge = 512,
    max_discharge = 513,
    original_pq_curves = 514,
    convex_pq_curves = 515,
    final_pq_curves = 516,
    max_prod = 517,
    min_prod = 518
  };
  using needle_combination_attribute_info_t =
    std::tuple<shop_needle_combination_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr needle_combination_attribute_info_t shop_needle_combination_attribute_info[] {
  { shop_needle_combination_attribute::p_max, "p_max", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_min, "p_min", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_nom, "p_nom", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::turb_eff_curves, "turb_eff_curves", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_fcr_min, "p_fcr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_fcr_n_min, "p_fcr_n_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_fcr_d_min, "p_fcr_d_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_fcr_max, "p_fcr_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_fcr_n_max, "p_fcr_n_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_fcr_d_max, "p_fcr_d_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_frr_min, "p_frr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::p_frr_max, "p_frr_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::production_cost, "production_cost", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::min_discharge, "min_discharge", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::max_discharge, "max_discharge", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_combination_attribute::original_pq_curves, "original_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_needle_combination_attribute::convex_pq_curves, "convex_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_needle_combination_attribute::final_pq_curves, "final_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_needle_combination_attribute::max_prod, "max_prod", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_needle_combination_attribute::min_prod, "min_prod", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_pump_attribute {
    initial_state = 524,
    penstock = 525,
    separate_droop = 526,
    p_min = 527,
    p_max = 528,
    p_nom = 529,
    gen_eff_curve = 530,
    turb_eff_curves = 531,
    maintenance_flag = 532,
    startcost = 533,
    stopcost = 534,
    committed_in = 535,
    committed_flag = 536,
    upstream_max = 537,
    upstream_max_flag = 538,
    downstream_min = 539,
    downstream_min_flag = 540,
    consumption_schedule = 541,
    consumption_schedule_flag = 542,
    upflow_schedule = 543,
    upflow_schedule_flag = 544,
    fcr_mip_flag = 545,
    p_fcr_min = 546,
    p_fcr_max = 547,
    p_rr_min = 548,
    frr_up_min = 549,
    frr_up_max = 550,
    frr_down_min = 551,
    frr_down_max = 552,
    fcr_n_up_min = 553,
    fcr_n_up_max = 554,
    fcr_n_down_min = 555,
    fcr_n_down_max = 556,
    fcr_d_up_min = 557,
    fcr_d_up_max = 558,
    fcr_d_down_min = 559,
    fcr_d_down_max = 560,
    rr_up_min = 561,
    rr_up_max = 562,
    rr_down_min = 563,
    rr_down_max = 564,
    fcr_n_up_schedule = 565,
    fcr_n_up_schedule_flag = 566,
    fcr_n_down_schedule = 567,
    fcr_n_down_schedule_flag = 568,
    fcr_d_up_schedule = 569,
    fcr_d_up_schedule_flag = 570,
    fcr_d_down_schedule = 571,
    fcr_d_down_schedule_flag = 572,
    frr_up_schedule = 573,
    frr_up_schedule_flag = 574,
    frr_down_schedule = 575,
    frr_down_schedule_flag = 576,
    rr_up_schedule = 577,
    rr_up_schedule_flag = 578,
    rr_down_schedule = 579,
    rr_down_schedule_flag = 580,
    droop_cost = 581,
    fixed_droop = 582,
    fixed_droop_flag = 583,
    droop_min = 584,
    droop_max = 585,
    reserve_ramping_cost_up = 586,
    reserve_ramping_cost_down = 587,
    discrete_droop_values = 588,
    fcr_n_discrete_droop_values = 589,
    fcr_d_up_discrete_droop_values = 590,
    fcr_d_down_discrete_droop_values = 591,
    fcr_n_up_cost = 592,
    fcr_n_down_cost = 593,
    fcr_d_up_cost = 594,
    fcr_d_down_cost = 595,
    frr_up_cost = 596,
    frr_down_cost = 597,
    rr_up_cost = 598,
    rr_down_cost = 599,
    spinning_reserve_up_max = 600,
    spinning_reserve_down_max = 601,
    fcr_n_droop_cost = 602,
    fcr_d_up_droop_cost = 603,
    fcr_d_down_droop_cost = 604,
    fcr_n_fixed_droop = 605,
    fcr_d_up_fixed_droop = 606,
    fcr_d_down_fixed_droop = 607,
    fcr_n_droop_min = 608,
    fcr_d_up_droop_min = 609,
    fcr_d_down_droop_min = 610,
    fcr_n_droop_max = 611,
    fcr_d_up_droop_max = 612,
    fcr_d_down_droop_max = 613,
    historical_consumption = 614,
    min_uptime = 615,
    min_downtime = 616,
    min_uptime_flag = 617,
    min_downtime_flag = 618,
    rr_up_stop_mip_flag = 619,
    max_p_constr = 620,
    max_p_constr_flag = 621,
    min_p_constr = 622,
    min_p_constr_flag = 623,
    min_q_constr = 624,
    min_q_constr_flag = 625,
    max_q_constr = 626,
    max_q_constr_flag = 627,
    eff_head = 628,
    head_loss = 629,
    consumption = 630,
    sim_consumption = 631,
    solver_consumption = 632,
    upflow = 633,
    sim_upflow = 634,
    solver_upflow = 635,
    committed_out = 636,
    consumption_schedule_penalty = 637,
    upflow_schedule_penalty = 638,
    fcr_n_up_delivery = 639,
    fcr_n_down_delivery = 640,
    fcr_d_up_delivery = 641,
    fcr_d_down_delivery = 642,
    frr_up_delivery = 643,
    frr_down_delivery = 644,
    rr_up_delivery = 645,
    rr_down_delivery = 646,
    fcr_n_up_delivery_physical = 647,
    fcr_n_down_delivery_physical = 648,
    fcr_d_up_delivery_physical = 649,
    fcr_d_down_delivery_physical = 650,
    fcr_n_up_schedule_penalty = 651,
    fcr_n_down_schedule_penalty = 652,
    fcr_d_up_schedule_penalty = 653,
    fcr_d_down_schedule_penalty = 654,
    frr_up_schedule_penalty = 655,
    frr_down_schedule_penalty = 656,
    rr_up_schedule_penalty = 657,
    rr_down_schedule_penalty = 658,
    droop_result = 659,
    original_pq_curves = 660,
    convex_pq_curves = 661,
    final_pq_curves = 662,
    max_cons = 663,
    min_cons = 664,
    fcr_n_droop_result = 665,
    fcr_d_up_droop_result = 666,
    fcr_d_down_droop_result = 667
  };
  using pump_attribute_info_t = std::tuple<shop_pump_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr pump_attribute_info_t shop_pump_attribute_info[] {
  { shop_pump_attribute::initial_state, "initial_state", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::penstock, "penstock", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::separate_droop, "separate_droop", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::p_min, "p_min", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::p_max, "p_max", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::p_nom, "p_nom", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::gen_eff_curve, "gen_eff_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::turb_eff_curves, "turb_eff_curves", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::maintenance_flag, "maintenance_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::startcost, "startcost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::stopcost, "stopcost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::committed_in, "committed_in", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::committed_flag, "committed_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::upstream_max, "upstream_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::upstream_max_flag, "upstream_max_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::downstream_min, "downstream_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::downstream_min_flag, "downstream_min_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::consumption_schedule, "consumption_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::consumption_schedule_flag, "consumption_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::upflow_schedule, "upflow_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::upflow_schedule_flag, "upflow_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_mip_flag, "fcr_mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::p_fcr_min, "p_fcr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::p_fcr_max, "p_fcr_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::p_rr_min, "p_rr_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_up_min, "frr_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_up_max, "frr_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_down_min, "frr_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_down_max, "frr_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_up_min, "fcr_n_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_up_max, "fcr_n_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_down_min, "fcr_n_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_down_max, "fcr_n_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_min, "fcr_d_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_max, "fcr_d_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_min, "fcr_d_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_max, "fcr_d_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_up_min, "rr_up_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_up_max, "rr_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_down_min, "rr_down_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_down_max, "rr_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_up_schedule, "fcr_n_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_up_schedule_flag, "fcr_n_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_down_schedule, "fcr_n_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_down_schedule_flag, "fcr_n_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_schedule, "fcr_d_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_schedule_flag, "fcr_d_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_schedule, "fcr_d_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_schedule_flag, "fcr_d_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_up_schedule, "frr_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_up_schedule_flag, "frr_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_down_schedule, "frr_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_down_schedule_flag, "frr_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_up_schedule, "rr_up_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_up_schedule_flag, "rr_up_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_down_schedule, "rr_down_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_down_schedule_flag, "rr_down_schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::droop_cost, "droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fixed_droop, "fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fixed_droop_flag, "fixed_droop_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::droop_min, "droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::droop_max, "droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::reserve_ramping_cost_up, "reserve_ramping_cost_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::reserve_ramping_cost_down, "reserve_ramping_cost_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::discrete_droop_values, "discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_discrete_droop_values, "fcr_n_discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_discrete_droop_values, "fcr_d_up_discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_discrete_droop_values, "fcr_d_down_discrete_droop_values", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_up_cost, "fcr_n_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_down_cost, "fcr_n_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_cost, "fcr_d_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_cost, "fcr_d_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_up_cost, "frr_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::frr_down_cost, "frr_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_up_cost, "rr_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_down_cost, "rr_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::spinning_reserve_up_max, "spinning_reserve_up_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::spinning_reserve_down_max, "spinning_reserve_down_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_droop_cost, "fcr_n_droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_droop_cost, "fcr_d_up_droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_droop_cost, "fcr_d_down_droop_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_fixed_droop, "fcr_n_fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_fixed_droop, "fcr_d_up_fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_fixed_droop, "fcr_d_down_fixed_droop", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_droop_min, "fcr_n_droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_droop_min, "fcr_d_up_droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_droop_min, "fcr_d_down_droop_min", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_n_droop_max, "fcr_n_droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_up_droop_max, "fcr_d_up_droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::fcr_d_down_droop_max, "fcr_d_down_droop_max", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::historical_consumption, "historical_consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_uptime, "min_uptime", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_downtime, "min_downtime", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::minute, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_uptime_flag, "min_uptime_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_downtime_flag, "min_downtime_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::rr_up_stop_mip_flag, "rr_up_stop_mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::max_p_constr, "max_p_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::max_p_constr_flag, "max_p_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_p_constr, "min_p_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_p_constr_flag, "min_p_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_q_constr, "min_q_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::min_q_constr_flag, "min_q_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::max_q_constr, "max_q_constr", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::max_q_constr_flag, "max_q_constr_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_attribute::eff_head, "eff_head", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::head_loss, "head_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::consumption, "consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::sim_consumption, "sim_consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::solver_consumption, "solver_consumption", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::upflow, "upflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::sim_upflow, "sim_upflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::solver_upflow, "solver_upflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::committed_out, "committed_out", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::consumption_schedule_penalty, "consumption_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::upflow_schedule_penalty, "upflow_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_up_delivery, "fcr_n_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_down_delivery, "fcr_n_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_up_delivery, "fcr_d_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_down_delivery, "fcr_d_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::frr_up_delivery, "frr_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::frr_down_delivery, "frr_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::rr_up_delivery, "rr_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::rr_down_delivery, "rr_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_up_delivery_physical, "fcr_n_up_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_down_delivery_physical, "fcr_n_down_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_up_delivery_physical, "fcr_d_up_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_down_delivery_physical, "fcr_d_down_delivery_physical", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_up_schedule_penalty, "fcr_n_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_down_schedule_penalty, "fcr_n_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_up_schedule_penalty, "fcr_d_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_down_schedule_penalty, "fcr_d_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::frr_up_schedule_penalty, "frr_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::frr_down_schedule_penalty, "frr_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::rr_up_schedule_penalty, "rr_up_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::rr_down_schedule_penalty, "rr_down_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::droop_result, "droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::original_pq_curves, "original_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::convex_pq_curves, "convex_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::final_pq_curves, "final_pq_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::max_cons, "max_cons", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::min_cons, "min_cons", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_n_droop_result, "fcr_n_droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_up_droop_result, "fcr_d_up_droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_pump_attribute::fcr_d_down_droop_result, "fcr_d_down_droop_result", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_gate_attribute {
    type = 670,
    time_delay = 672,
    add_slack = 673,
    max_discharge = 674,
    lin_rel_a = 677,
    lin_rel_b = 678,
    shape_discharge = 679,
    spill_cost_curve = 680,
    peak_flow_cost_curve = 681,
    peak_flow_penalty = 682,
    functions_meter_m3s = 683,
    functions_deltameter_m3s = 684,
    min_flow = 685,
    min_flow_flag = 686,
    max_flow = 687,
    max_flow_flag = 688,
    schedule_m3s = 689,
    schedule_percent = 690,
    schedule_flag = 691,
    setting = 692,
    setting_flag = 693,
    discharge_fee = 694,
    discharge_fee_flag = 695,
    block_merge_tolerance = 696,
    min_q_penalty_flag = 697,
    max_q_penalty_flag = 698,
    ramping_up = 699,
    ramping_up_flag = 700,
    ramping_down = 701,
    ramping_down_flag = 702,
    ramp_penalty_cost = 703,
    ramp_penalty_cost_flag = 704,
    max_q_penalty_cost = 705,
    min_q_penalty_cost = 706,
    max_q_penalty_cost_flag = 707,
    min_q_penalty_cost_flag = 708,
    min_q_penalty = 709,
    max_q_penalty = 710,
    discharge = 711,
    sim_discharge = 712
  };
  using gate_attribute_info_t = std::tuple<shop_gate_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr gate_attribute_info_t shop_gate_attribute_info[] {
  { shop_gate_attribute::type, "type", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_gate_attribute::time_delay, "time_delay", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::add_slack, "add_slack", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::max_discharge, "max_discharge", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::lin_rel_a, "lin_rel_a", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::lin_rel_b, "lin_rel_b", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::shape_discharge, "shape_discharge", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::spill_cost_curve, "spill_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::peak_flow_cost_curve, "peak_flow_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::peak_flow_penalty, "peak_flow_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_gate_attribute::functions_meter_m3s, "functions_meter_m3s", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::functions_deltameter_m3s, "functions_deltameter_m3s", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::delta_meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::min_flow, "min_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::min_flow_flag, "min_flow_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::max_flow, "max_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::max_flow_flag, "max_flow_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::schedule_m3s, "schedule_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::schedule_percent, "schedule_percent", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::percent, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::schedule_flag, "schedule_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::setting, "setting", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::setting_flag, "setting_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::discharge_fee, "discharge_fee", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::discharge_fee_flag, "discharge_fee_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::block_merge_tolerance, "block_merge_tolerance", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::min_q_penalty_flag, "min_q_penalty_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::max_q_penalty_flag, "max_q_penalty_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::ramping_up, "ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::ramping_up_flag, "ramping_up_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::ramping_down, "ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::ramping_down_flag, "ramping_down_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::ramp_penalty_cost, "ramp_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::ramp_penalty_cost_flag, "ramp_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::max_q_penalty_cost, "max_q_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::min_q_penalty_cost, "min_q_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::max_q_penalty_cost_flag, "max_q_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::min_q_penalty_cost_flag, "min_q_penalty_cost_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gate_attribute::min_q_penalty, "min_q_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_gate_attribute::max_q_penalty, "max_q_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_gate_attribute::discharge, "discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_gate_attribute::sim_discharge, "sim_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_thermal_attribute {
    fuel_cost = 1300,
    quadratic_fuel_cost = 1301,
    n_segments = 1302,
    min_prod = 1303,
    max_prod = 1304,
    startcost = 1305,
    stopcost = 1306,
    production = 1307
  };
  using thermal_attribute_info_t = std::tuple<shop_thermal_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr thermal_attribute_info_t shop_thermal_attribute_info[] {
  { shop_thermal_attribute::fuel_cost, "fuel_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::quadratic_fuel_cost, "quadratic_fuel_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::n_segments, "n_segments", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::min_prod, "min_prod", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::max_prod, "max_prod", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::startcost, "startcost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::stopcost, "stopcost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_thermal_attribute::production, "production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_junction_attribute {
    junc_slack = 726,
    altitude = 727,
    tunnel_flow_1 = 728,
    tunnel_flow_2 = 729,
    sim_tunnel_flow_1 = 730,
    sim_tunnel_flow_2 = 731,
    loss_factor_1 = 732,
    loss_factor_2 = 733,
    min_pressure = 734,
    pressure_height = 735,
    sim_pressure_height = 736,
    incr_cost = 737,
    local_incr_cost = 738
  };
  using junction_attribute_info_t = std::tuple<shop_junction_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr junction_attribute_info_t shop_junction_attribute_info[] {
  { shop_junction_attribute::junc_slack, "junc_slack", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_attribute::altitude, "altitude", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_attribute::tunnel_flow_1, "tunnel_flow_1", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::tunnel_flow_2, "tunnel_flow_2", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::sim_tunnel_flow_1, "sim_tunnel_flow_1", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::sim_tunnel_flow_2, "sim_tunnel_flow_2", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::loss_factor_1, "loss_factor_1", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_attribute::loss_factor_2, "loss_factor_2", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_attribute::min_pressure, "min_pressure", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_attribute::pressure_height, "pressure_height", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::sim_pressure_height, "sim_pressure_height", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::incr_cost, "incr_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_attribute::local_incr_cost, "local_incr_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_junction_gate_attribute {
    add_slack = 715,
    height_1 = 716,
    loss_factor_1 = 717,
    loss_factor_2 = 718,
    schedule = 719,
    pressure_height = 720,
    tunnel_flow_1 = 721,
    tunnel_flow_2 = 722
  };
  using junction_gate_attribute_info_t =
    std::tuple<shop_junction_gate_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr junction_gate_attribute_info_t shop_junction_gate_attribute_info[] {
  { shop_junction_gate_attribute::add_slack, "add_slack", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_gate_attribute::height_1, "height_1", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_gate_attribute::loss_factor_1, "loss_factor_1", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_gate_attribute::loss_factor_2, "loss_factor_2", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_gate_attribute::schedule, "schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_junction_gate_attribute::pressure_height, "pressure_height", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_gate_attribute::tunnel_flow_1, "tunnel_flow_1", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_junction_gate_attribute::tunnel_flow_2, "tunnel_flow_2", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_creek_intake_attribute {
    net_head = 741,
    max_inflow = 742,
    max_inflow_dynamic = 743,
    inflow = 744,
    sim_inflow = 745,
    sim_pressure_height = 746,
    inflow_percentage = 747,
    overflow_cost = 748,
    non_physical_overflow_flag = 749
  };
  using creek_intake_attribute_info_t =
    std::tuple<shop_creek_intake_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr creek_intake_attribute_info_t shop_creek_intake_attribute_info[] {
  { shop_creek_intake_attribute::net_head, "net_head", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_creek_intake_attribute::max_inflow, "max_inflow", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_creek_intake_attribute::max_inflow_dynamic, "max_inflow_dynamic", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_creek_intake_attribute::inflow, "inflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_creek_intake_attribute::sim_inflow, "sim_inflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_creek_intake_attribute::sim_pressure_height, "sim_pressure_height", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_creek_intake_attribute::inflow_percentage, "inflow_percentage", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_creek_intake_attribute::overflow_cost, "overflow_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_creek_intake_attribute::non_physical_overflow_flag, "non_physical_overflow_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_contract_attribute {
    initial_trade = 1387,
    trade_curve = 1388,
    min_trade = 1389,
    max_trade = 1390,
    ramping_up = 1391,
    ramping_down = 1392,
    ramping_up_penalty_cost = 1393,
    ramping_down_penalty_cost = 1394,
    trade = 1395,
    ramping_up_penalty = 1396,
    ramping_down_penalty = 1397
  };
  using contract_attribute_info_t = std::tuple<shop_contract_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr contract_attribute_info_t shop_contract_attribute_info[] {
  { shop_contract_attribute::initial_trade, "initial_trade", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::trade_curve, "trade_curve", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::min_trade, "min_trade", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::max_trade, "max_trade", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::ramping_up, "ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::ramping_down, "ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::ramping_up_penalty_cost, "ramping_up_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::ramping_down_penalty_cost, "ramping_down_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_contract_attribute::trade, "trade", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_contract_attribute::ramping_up_penalty, "ramping_up_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_contract_attribute::ramping_down_penalty, "ramping_down_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_market_attribute {
    prod_area = 750,
    market_type = 751,
    load = 752,
    max_buy = 753,
    max_sale = 754,
    load_price = 755,
    buy_price = 756,
    sale_price = 757,
    buy_delta = 758,
    sale_delta = 759,
    bid_flag = 760,
    common_scenario = 761,
    buy = 762,
    sale = 763,
    sim_sale = 764,
    sim_buy = 765,
    reserve_obligation_penalty = 766,
    load_penalty = 767
  };
  using market_attribute_info_t = std::tuple<shop_market_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr market_attribute_info_t shop_market_attribute_info[] {
  { shop_market_attribute::prod_area, "prod_area", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::market_type, "market_type", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::load, "load", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::max_buy, "max_buy", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::max_sale, "max_sale", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::load_price, "load_price", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::buy_price, "buy_price", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::sale_price, "sale_price", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::buy_delta, "buy_delta", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::sale_delta, "sale_delta", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::bid_flag, "bid_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::common_scenario, "common_scenario", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_market_attribute::buy, "buy", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_market_attribute::sale, "sale", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_market_attribute::sim_sale, "sim_sale", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_market_attribute::sim_buy, "sim_buy", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_market_attribute::reserve_obligation_penalty, "reserve_obligation_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_market_attribute::load_penalty, "load_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_global_settings_attribute {
    load_penalty_flag = 961,
    rsv_penalty_flag = 962,
    volume_ramp_penalty_flag = 963,
    level_ramp_penalty_flag = 964,
    production_ramp_penalty_flag = 965,
    plant_min_q_penalty_flag = 966,
    plant_min_p_penalty_flag = 967,
    plant_max_q_penalty_flag = 968,
    plant_max_p_penalty_flag = 969,
    gate_min_q_penalty_flag = 970,
    gate_max_q_penalty_flag = 971,
    gate_ramp_penalty_flag = 972,
    plant_schedule_penalty_flag = 973,
    gen_discharge_schedule_penalty_flag = 974,
    pump_schedule_penalty_flag = 975,
    power_limit_penalty_flag = 976,
    power_limit_penalty_cost = 977,
    load_penalty_cost = 978,
    rsv_penalty_cost = 979,
    rsv_hard_limit_penalty_cost = 980,
    volume_ramp_penalty_cost = 981,
    level_ramp_penalty_cost = 982,
    production_ramp_penalty_cost = 983,
    plant_discharge_ramp_penalty_cost = 984,
    plant_soft_p_penalty = 985,
    plant_soft_q_penalty = 986,
    plant_sched_penalty_cost_up = 987,
    plant_sched_penalty_cost_down = 988,
    gen_discharge_sched_penalty_cost_up = 989,
    gen_discharge_sched_penalty_cost_down = 990,
    pump_sched_penalty_cost_up = 991,
    pump_sched_penalty_cost_down = 992,
    gate_ramp_penalty_cost = 993,
    discharge_group_penalty_cost = 994,
    reserve_schedule_penalty_cost = 995,
    reserve_group_penalty_cost = 996,
    bypass_cost = 997,
    gate_cost = 998,
    overflow_cost = 999,
    overflow_cost_time_factor = 1000,
    gen_reserve_ramping_cost = 1001,
    pump_reserve_ramping_cost = 1002,
    reserve_contribution_cost = 1003,
    gate_ramp_cost = 1004,
    reserve_group_slack_cost = 1005,
    fcr_n_ramping_cost = 1006,
    fcr_d_ramping_cost = 1007,
    frr_ramping_cost = 1008,
    rr_ramping_cost = 1009,
    fcr_n_up_activation_factor = 1010,
    fcr_n_down_activation_factor = 1011,
    fcr_d_up_activation_factor = 1012,
    fcr_d_down_activation_factor = 1013,
    frr_up_activation_factor = 1014,
    frr_down_activation_factor = 1015,
    rr_up_activation_factor = 1016,
    rr_down_activation_factor = 1017,
    fcr_n_up_activation_time = 1018,
    fcr_n_down_activation_time = 1019,
    fcr_d_up_activation_time = 1020,
    fcr_d_down_activation_time = 1021,
    frr_up_activation_time = 1022,
    frr_down_activation_time = 1023,
    rr_up_activation_time = 1024,
    rr_down_activation_time = 1025,
    fcr_n_up_activation_penalty_cost = 1026,
    fcr_n_down_activation_penalty_cost = 1027,
    fcr_d_up_activation_penalty_cost = 1028,
    fcr_d_down_activation_penalty_cost = 1029,
    frr_up_activation_penalty_cost = 1030,
    frr_down_activation_penalty_cost = 1031,
    rr_up_activation_penalty_cost = 1032,
    rr_down_activation_penalty_cost = 1033,
    reserve_tactical_activation_cost_scaling = 1034,
    use_heuristic_basis = 1035,
    nodelog = 1036,
    max_num_threads = 1037,
    parallelmode = 1038,
    timelimit = 1039,
    mipgap_rel = 1040,
    mipgap_abs = 1041,
    inteps = 1042,
    input_basis_name = 1043,
    output_basis_name = 1044,
    solver_algorithm = 1045,
    cplex_int_params = 1046,
    cplex_double_params = 1047,
    n_seg_up = 1048,
    n_seg_down = 1049,
    n_mip_seg_up = 1050,
    n_mip_seg_down = 1051,
    dyn_pq_seg_flag = 1052,
    dyn_mip_pq_seg_flag = 1053,
    bypass_segments = 1054,
    gate_segments = 1055,
    overflow_segments = 1056,
    gravity = 1057,
    gen_reserve_min_free_cap_factor = 1058,
    fcr_n_band = 1059,
    fcr_d_band = 1060,
    universal_mip = 1061,
    universal_overflow_mip = 1062,
    universal_river_mip = 1063,
    linear_startup = 1064,
    dyn_flex_mip_steps = 1065,
    merge_blocks = 1066,
    power_head_optimization = 1067,
    droop_discretization_limit = 1068,
    droop_cost_exponent = 1069,
    droop_ref_value = 1070,
    disable_rsv_penalties = 1071,
    binary_reserve_limits = 1072,
    universal_strict_pq_uploading = 1073,
    create_cuts = 1074,
    print_sim_inflow = 1075,
    pump_head_optimization = 1076,
    save_pq_curves = 1077,
    build_original_pq_curves_by_discharge_limits = 1078,
    plant_unbalance_recommit = 1079,
    prefer_start_vol = 1080,
    bp_print_discharge = 1081,
    prod_from_ref_prod = 1082,
    bp_min_points = 1083,
    bp_mode = 1084,
    stop_cost_from_start_cost = 1085,
    ownership_scaling = 1086,
    time_delay_unit = 1087,
    bypass_loss = 1088,
    gen_turn_off_limit = 1089,
    pump_turn_off_limit = 1090,
    fcr_n_equality_flag = 1091,
    delay_valuation_mode = 1092,
    universal_affinity_flag = 1093,
    ramp_code = 1094,
    print_original_pq_curves = 1095,
    print_convex_pq_curves = 1096,
    print_final_pq_curves = 1097,
    shop_log_name = 1098,
    shop_yaml_log_name = 1099,
    solver_log_name = 1100,
    minimal_infeasible_problem_file = 1101,
    model_file_name = 1102,
    pq_curves_name = 1103,
    print_loss = 1104,
    shop_xmllog = 1105,
    print_optimized_startup_costs = 1106,
    get_duals_from_mip = 1107,
    bid_aggregation_level = 1108,
    simple_pq_recovery = 1109,
    rr_up_schedule_slack_flag = 1110,
    bp_ref_mc_from_market = 1111,
    bp_bid_matrix_points = 1112,
    ramp_scale_factor = 1113,
    river_flow_penalty_cost = 1114,
    river_flow_schedule_penalty_cost = 1115,
    recommit = 1116,
    droop_cost = 1117,
    rr_down_stop_mip = 1118,
    rr_up_stop_mip = 1119,
    plant_max_prod_reserve_strategy = 1120,
    plant_min_prod_reserve_strategy = 1121,
    shop_version = 1162,
    river_big_m_factor = 1163
  };
  using global_settings_attribute_info_t =
    std::tuple<shop_global_settings_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr global_settings_attribute_info_t shop_global_settings_attribute_info[] {
  { shop_global_settings_attribute::load_penalty_flag, "load_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rsv_penalty_flag, "rsv_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::volume_ramp_penalty_flag, "volume_ramp_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::level_ramp_penalty_flag, "level_ramp_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::production_ramp_penalty_flag, "production_ramp_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_min_q_penalty_flag, "plant_min_q_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_min_p_penalty_flag, "plant_min_p_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_max_q_penalty_flag, "plant_max_q_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_max_p_penalty_flag, "plant_max_p_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_min_q_penalty_flag, "gate_min_q_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_max_q_penalty_flag, "gate_max_q_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_ramp_penalty_flag, "gate_ramp_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_schedule_penalty_flag, "plant_schedule_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gen_discharge_schedule_penalty_flag, "gen_discharge_schedule_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pump_schedule_penalty_flag, "pump_schedule_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::power_limit_penalty_flag, "power_limit_penalty_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::power_limit_penalty_cost, "power_limit_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::load_penalty_cost, "load_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rsv_penalty_cost, "rsv_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rsv_hard_limit_penalty_cost, "rsv_hard_limit_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::volume_ramp_penalty_cost, "volume_ramp_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::level_ramp_penalty_cost, "level_ramp_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_meter, .y_unit = shop_y_unit::nok_per_meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::production_ramp_penalty_cost, "production_ramp_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_discharge_ramp_penalty_cost, "plant_discharge_ramp_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_soft_p_penalty, "plant_soft_p_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_soft_q_penalty, "plant_soft_q_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_sched_penalty_cost_up, "plant_sched_penalty_cost_up", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_sched_penalty_cost_down, "plant_sched_penalty_cost_down", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gen_discharge_sched_penalty_cost_up, "gen_discharge_sched_penalty_cost_up", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gen_discharge_sched_penalty_cost_down, "gen_discharge_sched_penalty_cost_down", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pump_sched_penalty_cost_up, "pump_sched_penalty_cost_up", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pump_sched_penalty_cost_down, "pump_sched_penalty_cost_down", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_ramp_penalty_cost, "gate_ramp_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::discharge_group_penalty_cost, "discharge_group_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::reserve_schedule_penalty_cost, "reserve_schedule_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::reserve_group_penalty_cost, "reserve_group_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bypass_cost, "bypass_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_cost, "gate_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::overflow_cost, "overflow_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::overflow_cost_time_factor, "overflow_cost_time_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3h, .y_unit = shop_y_unit::nok_per_mm3h, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gen_reserve_ramping_cost, "gen_reserve_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pump_reserve_ramping_cost, "pump_reserve_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::reserve_contribution_cost, "reserve_contribution_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_ramp_cost, "gate_ramp_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::reserve_group_slack_cost, "reserve_group_slack_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_ramping_cost, "fcr_n_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_ramping_cost, "fcr_d_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_ramping_cost, "frr_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_ramping_cost, "rr_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_up_activation_factor, "fcr_n_up_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_down_activation_factor, "fcr_n_down_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_up_activation_factor, "fcr_d_up_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_down_activation_factor, "fcr_d_down_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_up_activation_factor, "frr_up_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_down_activation_factor, "frr_down_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_up_activation_factor, "rr_up_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_down_activation_factor, "rr_down_activation_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_up_activation_time, "fcr_n_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_down_activation_time, "fcr_n_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_up_activation_time, "fcr_d_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_down_activation_time, "fcr_d_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_up_activation_time, "frr_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_down_activation_time, "frr_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_up_activation_time, "rr_up_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_down_activation_time, "rr_down_activation_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_up_activation_penalty_cost, "fcr_n_up_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_down_activation_penalty_cost, "fcr_n_down_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_up_activation_penalty_cost, "fcr_d_up_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_down_activation_penalty_cost, "fcr_d_down_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_up_activation_penalty_cost, "frr_up_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::frr_down_activation_penalty_cost, "frr_down_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_up_activation_penalty_cost, "rr_up_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_down_activation_penalty_cost, "rr_down_activation_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::reserve_tactical_activation_cost_scaling, "reserve_tactical_activation_cost_scaling", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::use_heuristic_basis, "use_heuristic_basis", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::nodelog, "nodelog", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::max_num_threads, "max_num_threads", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::parallelmode, "parallelmode", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::timelimit, "timelimit", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::second, .y_unit = shop_y_unit::second, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::mipgap_rel, "mipgap_rel", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::mipgap_abs, "mipgap_abs", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::inteps, "inteps", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::input_basis_name, "input_basis_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::output_basis_name, "output_basis_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::solver_algorithm, "solver_algorithm", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::cplex_int_params, "cplex_int_params", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::cplex_double_params, "cplex_double_params", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::n_seg_up, "n_seg_up", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::n_seg_down, "n_seg_down", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::n_mip_seg_up, "n_mip_seg_up", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::n_mip_seg_down, "n_mip_seg_down", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::dyn_pq_seg_flag, "dyn_pq_seg_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::dyn_mip_pq_seg_flag, "dyn_mip_pq_seg_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bypass_segments, "bypass_segments", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gate_segments, "gate_segments", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::overflow_segments, "overflow_segments", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gravity, "gravity", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gen_reserve_min_free_cap_factor, "gen_reserve_min_free_cap_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_band, "fcr_n_band", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_d_band, "fcr_d_band", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::universal_mip, "universal_mip", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::universal_overflow_mip, "universal_overflow_mip", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::universal_river_mip, "universal_river_mip", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::linear_startup, "linear_startup", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::dyn_flex_mip_steps, "dyn_flex_mip_steps", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::merge_blocks, "merge_blocks", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::power_head_optimization, "power_head_optimization", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::droop_discretization_limit, "droop_discretization_limit", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::droop_cost_exponent, "droop_cost_exponent", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::droop_ref_value, "droop_ref_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::disable_rsv_penalties, "disable_rsv_penalties", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::binary_reserve_limits, "binary_reserve_limits", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::universal_strict_pq_uploading, "universal_strict_pq_uploading", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::create_cuts, "create_cuts", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::print_sim_inflow, "print_sim_inflow", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pump_head_optimization, "pump_head_optimization", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::save_pq_curves, "save_pq_curves", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::build_original_pq_curves_by_discharge_limits, "build_original_pq_curves_by_discharge_limits", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_unbalance_recommit, "plant_unbalance_recommit", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::prefer_start_vol, "prefer_start_vol", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bp_print_discharge, "bp_print_discharge", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::prod_from_ref_prod, "prod_from_ref_prod", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bp_min_points, "bp_min_points", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bp_mode, "bp_mode", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::stop_cost_from_start_cost, "stop_cost_from_start_cost", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::ownership_scaling, "ownership_scaling", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::time_delay_unit, "time_delay_unit", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bypass_loss, "bypass_loss", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::gen_turn_off_limit, "gen_turn_off_limit", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pump_turn_off_limit, "pump_turn_off_limit", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::fcr_n_equality_flag, "fcr_n_equality_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::delay_valuation_mode, "delay_valuation_mode", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::universal_affinity_flag, "universal_affinity_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::ramp_code, "ramp_code", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::print_original_pq_curves, "print_original_pq_curves", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::print_convex_pq_curves, "print_convex_pq_curves", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::print_final_pq_curves, "print_final_pq_curves", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::shop_log_name, "shop_log_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::shop_yaml_log_name, "shop_yaml_log_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::solver_log_name, "solver_log_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::minimal_infeasible_problem_file, "minimal_infeasible_problem_file", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::model_file_name, "model_file_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::pq_curves_name, "pq_curves_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::print_loss, "print_loss", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::shop_xmllog, "shop_xmllog", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::print_optimized_startup_costs, "print_optimized_startup_costs", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::get_duals_from_mip, "get_duals_from_mip", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bid_aggregation_level, "bid_aggregation_level", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::simple_pq_recovery, "simple_pq_recovery", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_up_schedule_slack_flag, "rr_up_schedule_slack_flag", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::bp_ref_mc_from_market, "bp_ref_mc_from_market", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_global_settings_attribute::bp_bid_matrix_points, "bp_bid_matrix_points", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::ramp_scale_factor, "ramp_scale_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::river_flow_penalty_cost, "river_flow_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_h_per_m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::river_flow_schedule_penalty_cost, "river_flow_schedule_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_h_per_m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::recommit, "recommit", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::droop_cost, "droop_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_down_stop_mip, "rr_down_stop_mip", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::rr_up_stop_mip, "rr_up_stop_mip", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_max_prod_reserve_strategy, "plant_max_prod_reserve_strategy", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::plant_min_prod_reserve_strategy, "plant_min_prod_reserve_strategy", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_global_settings_attribute::shop_version, "shop_version", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_global_settings_attribute::river_big_m_factor, "river_big_m_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_reserve_group_attribute {
    group_id = 768,
    fcr_n_up_obligation = 769,
    fcr_n_down_obligation = 770,
    fcr_d_up_obligation = 771,
    fcr_d_down_obligation = 772,
    frr_up_obligation = 773,
    frr_down_obligation = 774,
    rr_up_obligation = 775,
    rr_down_obligation = 776,
    fcr_n_penalty_cost = 777,
    fcr_d_penalty_cost = 778,
    frr_penalty_cost = 779,
    rr_penalty_cost = 780,
    fcr_n_up_slack = 781,
    fcr_n_down_slack = 782,
    fcr_d_up_slack = 783,
    fcr_d_down_slack = 784,
    frr_up_slack = 785,
    frr_down_slack = 786,
    rr_up_slack = 787,
    rr_down_slack = 788,
    fcr_n_up_violation = 789,
    fcr_n_down_violation = 790,
    fcr_d_up_violation = 791,
    fcr_d_down_violation = 792,
    frr_up_violation = 793,
    frr_down_violation = 794,
    rr_up_violation = 795,
    rr_down_violation = 796
  };
  using reserve_group_attribute_info_t =
    std::tuple<shop_reserve_group_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr reserve_group_attribute_info_t shop_reserve_group_attribute_info[] {
  { shop_reserve_group_attribute::group_id, "group_id", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_n_up_obligation, "fcr_n_up_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::fcr_n_down_obligation, "fcr_n_down_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::fcr_d_up_obligation, "fcr_d_up_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::fcr_d_down_obligation, "fcr_d_down_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::frr_up_obligation, "frr_up_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::frr_down_obligation, "frr_down_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::rr_up_obligation, "rr_up_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::rr_down_obligation, "rr_down_obligation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::fcr_n_penalty_cost, "fcr_n_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::fcr_d_penalty_cost, "fcr_d_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::frr_penalty_cost, "frr_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::rr_penalty_cost, "rr_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_reserve_group_attribute::fcr_n_up_slack, "fcr_n_up_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_n_down_slack, "fcr_n_down_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_d_up_slack, "fcr_d_up_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_d_down_slack, "fcr_d_down_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::frr_up_slack, "frr_up_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::frr_down_slack, "frr_down_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::rr_up_slack, "rr_up_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::rr_down_slack, "rr_down_slack", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_n_up_violation, "fcr_n_up_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_n_down_violation, "fcr_n_down_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_d_up_violation, "fcr_d_up_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::fcr_d_down_violation, "fcr_d_down_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::frr_up_violation, "frr_up_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::frr_down_violation, "frr_down_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::rr_up_violation, "rr_up_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_reserve_group_attribute::rr_down_violation, "rr_down_violation", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_commit_group_attribute {
    deactivate_exclusion_flag = 842
  };
  using commit_group_attribute_info_t =
    std::tuple<shop_commit_group_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr commit_group_attribute_info_t shop_commit_group_attribute_info[] {
  { shop_commit_group_attribute::deactivate_exclusion_flag, "deactivate_exclusion_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_discharge_group_attribute {
    initial_deviation_mm3 = 797,
    max_accumulated_deviation_mm3_up = 798,
    max_accumulated_deviation_mm3_down = 799,
    weighted_discharge_m3s = 800,
    penalty_cost_up_per_mm3 = 801,
    penalty_cost_down_per_mm3 = 802,
    min_discharge_m3s = 803,
    max_discharge_m3s = 804,
    min_discharge_penalty_cost = 805,
    max_discharge_penalty_cost = 806,
    ramping_up_m3s = 807,
    ramping_down_m3s = 808,
    ramping_up_penalty_cost = 809,
    ramping_down_penalty_cost = 810,
    min_average_discharge = 811,
    max_average_discharge = 812,
    min_average_discharge_penalty_cost = 813,
    max_average_discharge_penalty_cost = 814,
    actual_discharge_m3s = 815,
    accumulated_deviation_mm3 = 816,
    upper_penalty_mm3 = 817,
    lower_penalty_mm3 = 818,
    upper_slack_mm3 = 819,
    lower_slack_mm3 = 820,
    min_discharge_penalty = 821,
    max_discharge_penalty = 822,
    ramping_up_penalty = 823,
    ramping_down_penalty = 824
  };
  using discharge_group_attribute_info_t =
    std::tuple<shop_discharge_group_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr discharge_group_attribute_info_t shop_discharge_group_attribute_info[] {
  { shop_discharge_group_attribute::initial_deviation_mm3, "initial_deviation_mm3", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::max_accumulated_deviation_mm3_up, "max_accumulated_deviation_mm3_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::max_accumulated_deviation_mm3_down, "max_accumulated_deviation_mm3_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::weighted_discharge_m3s, "weighted_discharge_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::penalty_cost_up_per_mm3, "penalty_cost_up_per_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::penalty_cost_down_per_mm3, "penalty_cost_down_per_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::min_discharge_m3s, "min_discharge_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::max_discharge_m3s, "max_discharge_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::min_discharge_penalty_cost, "min_discharge_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::max_discharge_penalty_cost, "max_discharge_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::ramping_up_m3s, "ramping_up_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::ramping_down_m3s, "ramping_down_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::ramping_up_penalty_cost, "ramping_up_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::ramping_down_penalty_cost, "ramping_down_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::min_average_discharge, "min_average_discharge", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::max_average_discharge, "max_average_discharge", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::minute, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::min_average_discharge_penalty_cost, "min_average_discharge_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_h_per_m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::max_average_discharge_penalty_cost, "max_average_discharge_penalty_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_h_per_m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_discharge_group_attribute::actual_discharge_m3s, "actual_discharge_m3s", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::accumulated_deviation_mm3, "accumulated_deviation_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::upper_penalty_mm3, "upper_penalty_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::lower_penalty_mm3, "lower_penalty_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::upper_slack_mm3, "upper_slack_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::lower_slack_mm3, "lower_slack_mm3", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::min_discharge_penalty, "min_discharge_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::max_discharge_penalty, "max_discharge_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::ramping_up_penalty, "ramping_up_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_discharge_group_attribute::ramping_down_penalty, "ramping_down_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_production_group_attribute {
    energy_target = 825,
    energy_penalty_cost_up = 826,
    energy_penalty_cost_down = 827,
    energy_target_period_flag = 828,
    max_p_limit = 829,
    min_p_limit = 830,
    max_p_penalty_cost = 831,
    min_p_penalty_cost = 832,
    sum_production = 833,
    min_p_penalty = 834,
    max_p_penalty = 835,
    energy_penalty_up = 836,
    energy_penalty_down = 837
  };
  using production_group_attribute_info_t =
    std::tuple<shop_production_group_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr production_group_attribute_info_t shop_production_group_attribute_info[] {
  { shop_production_group_attribute::energy_target, "energy_target", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::energy_penalty_cost_up, "energy_penalty_cost_up", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::energy_penalty_cost_down, "energy_penalty_cost_down", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::energy_target_period_flag, "energy_target_period_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::max_p_limit, "max_p_limit", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::min_p_limit, "min_p_limit", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::max_p_penalty_cost, "max_p_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::min_p_penalty_cost, "min_p_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_production_group_attribute::sum_production, "sum_production", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_production_group_attribute::min_p_penalty, "min_p_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_production_group_attribute::max_p_penalty, "max_p_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_production_group_attribute::energy_penalty_up, "energy_penalty_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_production_group_attribute::energy_penalty_down, "energy_penalty_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_volume_constraint_attribute {
    min_vol = 838,
    max_vol = 839,
    min_vol_penalty = 840,
    max_vol_penalty = 841
  };
  using volume_constraint_attribute_info_t =
    std::tuple<shop_volume_constraint_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr volume_constraint_attribute_info_t shop_volume_constraint_attribute_info[] {
  { shop_volume_constraint_attribute::min_vol, "min_vol", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_volume_constraint_attribute::max_vol, "max_vol", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_volume_constraint_attribute::min_vol_penalty, "min_vol_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_volume_constraint_attribute::max_vol_penalty, "max_vol_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_scenario_attribute {
    scenario_id = 843,
    probability = 844,
    common_scenario = 845,
    common_history = 846
  };
  using scenario_attribute_info_t = std::tuple<shop_scenario_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr scenario_attribute_info_t shop_scenario_attribute_info[] {
  { shop_scenario_attribute::scenario_id, "scenario_id", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_scenario_attribute::probability, "probability", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_scenario_attribute::common_scenario, "common_scenario", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_scenario_attribute::common_history, "common_history", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_objective_attribute {
    solver_status = 847,
    times_of_wrong_pq_uploading = 848,
    grand_total = 849,
    sim_grand_total = 850,
    total = 851,
    sum_penalties = 852,
    minor_penalties = 853,
    major_penalties = 854,
    rsv_end_value = 855,
    sim_rsv_end_value = 856,
    rsv_end_value_relative = 857,
    vow_in_transit = 858,
    rsv_spill_vol_end_value = 859,
    market_sale_buy = 860,
    sim_market_sale_buy = 861,
    load_value = 862,
    reserve_sale_buy = 863,
    reserve_oblig_value = 864,
    contract_value = 865,
    startup_costs = 866,
    sim_startup_costs = 867,
    sum_feeding_fee = 868,
    sum_discharge_fee = 869,
    thermal_cost = 870,
    production_cost = 871,
    reserve_allocation_cost = 872,
    incurred_droop_cost = 873,
    rsv_tactical_penalty = 874,
    plant_p_constr_penalty = 875,
    plant_q_constr_penalty = 876,
    plant_schedule_penalty = 877,
    plant_rsv_q_limit_penalty = 878,
    gen_schedule_penalty = 879,
    pump_schedule_penalty = 880,
    gate_q_constr_penalty = 881,
    gate_discharge_cost = 882,
    bypass_cost = 883,
    gate_spill_cost = 884,
    physical_spill_cost = 885,
    physical_spill_volume = 886,
    nonphysical_spill_cost = 887,
    nonphysical_spill_volume = 888,
    gate_slack_cost = 889,
    gate_ramping_cost = 890,
    creek_spill_cost = 891,
    creek_physical_spill_cost = 892,
    creek_nonphysical_spill_cost = 893,
    junction_slack_cost = 894,
    reserve_violation_penalty = 895,
    reserve_slack_cost = 896,
    reserve_schedule_penalty = 897,
    rsv_peak_volume_penalty = 898,
    gate_peak_flow_penalty = 899,
    rsv_flood_volume_penalty = 900,
    river_peak_flow_penalty = 901,
    river_flow_penalty = 902,
    river_gate_adjustment_penalty = 903,
    plant_reserve_discharge_penalty = 904,
    solar_curtailment_cost = 905,
    wind_curtailment_cost = 906,
    plant_sum_reserve_penalty = 907,
    reserve_activation_penalty = 908,
    reserve_tactical_activation_penalty = 909,
    plant_reserve_strategy_penalty = 910,
    rsv_penalty = 911,
    rsv_hard_limit_penalty = 912,
    rsv_over_limit_penalty = 913,
    sim_rsv_penalty = 914,
    rsv_end_penalty = 915,
    load_penalty = 916,
    group_time_period_penalty = 917,
    group_time_step_penalty = 918,
    sum_ramping_penalty = 919,
    plant_ramping_penalty = 920,
    rsv_ramping_penalty = 921,
    gate_ramping_penalty = 922,
    contract_ramping_penalty = 923,
    group_ramping_penalty = 924,
    discharge_group_penalty = 925,
    discharge_group_ramping_penalty = 926,
    discharge_group_average_discharge_penalty = 927,
    production_group_energy_penalty = 928,
    production_group_power_penalty = 929,
    river_min_flow_penalty = 930,
    river_max_flow_penalty = 931,
    river_ramping_penalty = 932,
    river_gate_ramping_penalty = 933,
    river_flow_schedule_penalty = 934,
    level_rolling_ramping_penalty = 935,
    level_period_ramping_penalty = 936,
    discharge_rolling_ramping_penalty = 937,
    discharge_period_ramping_penalty = 938,
    rsv_nonseq_ramping_penalty = 939,
    rsv_amplitude_ramping_penalty = 940,
    common_decision_penalty = 941,
    bidding_penalty = 942,
    safe_mode_universal_penalty = 943
  };
  using objective_attribute_info_t = std::tuple<shop_objective_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr objective_attribute_info_t shop_objective_attribute_info[] {
  { shop_objective_attribute::solver_status, "solver_status", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::times_of_wrong_pq_uploading, "times_of_wrong_pq_uploading", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::grand_total, "grand_total", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sim_grand_total, "sim_grand_total", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::total, "total", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sum_penalties, "sum_penalties", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::minor_penalties, "minor_penalties", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::major_penalties, "major_penalties", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_end_value, "rsv_end_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sim_rsv_end_value, "sim_rsv_end_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_end_value_relative, "rsv_end_value_relative", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::vow_in_transit, "vow_in_transit", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_spill_vol_end_value, "rsv_spill_vol_end_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::market_sale_buy, "market_sale_buy", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sim_market_sale_buy, "sim_market_sale_buy", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::load_value, "load_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_sale_buy, "reserve_sale_buy", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_oblig_value, "reserve_oblig_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::contract_value, "contract_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::startup_costs, "startup_costs", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sim_startup_costs, "sim_startup_costs", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sum_feeding_fee, "sum_feeding_fee", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sum_discharge_fee, "sum_discharge_fee", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::thermal_cost, "thermal_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::production_cost, "production_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_allocation_cost, "reserve_allocation_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::incurred_droop_cost, "incurred_droop_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_tactical_penalty, "rsv_tactical_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_p_constr_penalty, "plant_p_constr_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_q_constr_penalty, "plant_q_constr_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_schedule_penalty, "plant_schedule_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_rsv_q_limit_penalty, "plant_rsv_q_limit_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gen_schedule_penalty, "gen_schedule_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::pump_schedule_penalty, "pump_schedule_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_q_constr_penalty, "gate_q_constr_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_discharge_cost, "gate_discharge_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::bypass_cost, "bypass_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_spill_cost, "gate_spill_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::physical_spill_cost, "physical_spill_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::physical_spill_volume, "physical_spill_volume", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::nonphysical_spill_cost, "nonphysical_spill_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::nonphysical_spill_volume, "nonphysical_spill_volume", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::mm3, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_slack_cost, "gate_slack_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_ramping_cost, "gate_ramping_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::creek_spill_cost, "creek_spill_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::creek_physical_spill_cost, "creek_physical_spill_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::creek_nonphysical_spill_cost, "creek_nonphysical_spill_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::junction_slack_cost, "junction_slack_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_violation_penalty, "reserve_violation_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_slack_cost, "reserve_slack_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_schedule_penalty, "reserve_schedule_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_peak_volume_penalty, "rsv_peak_volume_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_peak_flow_penalty, "gate_peak_flow_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_flood_volume_penalty, "rsv_flood_volume_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_peak_flow_penalty, "river_peak_flow_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_flow_penalty, "river_flow_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_gate_adjustment_penalty, "river_gate_adjustment_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_reserve_discharge_penalty, "plant_reserve_discharge_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::solar_curtailment_cost, "solar_curtailment_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::wind_curtailment_cost, "wind_curtailment_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_sum_reserve_penalty, "plant_sum_reserve_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_activation_penalty, "reserve_activation_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::reserve_tactical_activation_penalty, "reserve_tactical_activation_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_reserve_strategy_penalty, "plant_reserve_strategy_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_penalty, "rsv_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_hard_limit_penalty, "rsv_hard_limit_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_over_limit_penalty, "rsv_over_limit_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sim_rsv_penalty, "sim_rsv_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_end_penalty, "rsv_end_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::load_penalty, "load_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::group_time_period_penalty, "group_time_period_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::group_time_step_penalty, "group_time_step_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::sum_ramping_penalty, "sum_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::plant_ramping_penalty, "plant_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_ramping_penalty, "rsv_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::gate_ramping_penalty, "gate_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::contract_ramping_penalty, "contract_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::group_ramping_penalty, "group_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::discharge_group_penalty, "discharge_group_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::discharge_group_ramping_penalty, "discharge_group_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::discharge_group_average_discharge_penalty, "discharge_group_average_discharge_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::production_group_energy_penalty, "production_group_energy_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::production_group_power_penalty, "production_group_power_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_min_flow_penalty, "river_min_flow_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_max_flow_penalty, "river_max_flow_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_ramping_penalty, "river_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_gate_ramping_penalty, "river_gate_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::river_flow_schedule_penalty, "river_flow_schedule_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::level_rolling_ramping_penalty, "level_rolling_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::level_period_ramping_penalty, "level_period_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::discharge_rolling_ramping_penalty, "discharge_rolling_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::discharge_period_ramping_penalty, "discharge_period_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_nonseq_ramping_penalty, "rsv_nonseq_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::rsv_amplitude_ramping_penalty, "rsv_amplitude_ramping_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::common_decision_penalty, "common_decision_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::bidding_penalty, "bidding_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_objective_attribute::safe_mode_universal_penalty, "safe_mode_universal_penalty", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_bid_group_attribute {
    price_dimension = 945,
    time_dimension = 946,
    bid_start_interval = 947,
    bid_end_interval = 948,
    reduction_cost = 950,
    bid_curves = 952,
    bid_penalty = 953,
    best_profit_bid_matrix = 954
  };
  using bid_group_attribute_info_t = std::tuple<shop_bid_group_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr bid_group_attribute_info_t shop_bid_group_attribute_info[] {
  { shop_bid_group_attribute::price_dimension, "price_dimension", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::time_dimension, "time_dimension", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::bid_start_interval, "bid_start_interval", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::bid_end_interval, "bid_end_interval", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::reduction_cost, "reduction_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::bid_curves, "bid_curves", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::bid_penalty, "bid_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_bid_group_attribute::best_profit_bid_matrix, "best_profit_bid_matrix", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_cut_group_attribute {
    rhs = 955,
    end_value = 956,
    binding_cut_up = 957,
    binding_cut_down = 958
  };
  using cut_group_attribute_info_t = std::tuple<shop_cut_group_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr cut_group_attribute_info_t shop_cut_group_attribute_info[] {
  { shop_cut_group_attribute::rhs, "rhs", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_cut_group_attribute::end_value, "end_value", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_cut_group_attribute::binding_cut_up, "binding_cut_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_cut_group_attribute::binding_cut_down, "binding_cut_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_inflow_series_attribute {
    cut_coeffs = 959
  };
  using inflow_series_attribute_info_t =
    std::tuple<shop_inflow_series_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr inflow_series_attribute_info_t shop_inflow_series_attribute_info[] {
  { shop_inflow_series_attribute::cut_coeffs, "cut_coeffs", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_system_attribute {
    cut_output_rhs = 960
  };
  using system_attribute_info_t = std::tuple<shop_system_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr system_attribute_info_t shop_system_attribute_info[] {
  { shop_system_attribute::cut_output_rhs, "cut_output_rhs", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_unit_combination_attribute {
    discharge = 1164,
    marginal_cost = 1165,
    average_cost = 1166
  };
  using unit_combination_attribute_info_t =
    std::tuple<shop_unit_combination_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr unit_combination_attribute_info_t shop_unit_combination_attribute_info[] {
  { shop_unit_combination_attribute::discharge, "discharge", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_unit_combination_attribute::marginal_cost, "marginal_cost", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_unit_combination_attribute::average_cost, "average_cost", { .data_type = shop_data_type::shop_xyt, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_tunnel_attribute {
    start_height = 1167,
    end_height = 1168,
    diameter = 1169,
    length = 1170,
    loss_factor = 1171,
    weir_width = 1172,
    time_delay = 1173,
    gate_opening_curve = 1174,
    gate_adjustment_cost = 1175,
    gate_opening_schedule = 1176,
    initial_opening = 1177,
    continuous_gate = 1178,
    end_pressure = 1179,
    sim_end_pressure = 1180,
    flow = 1181,
    physical_flow = 1182,
    sim_flow = 1183,
    gate_opening = 1184,
    network_no = 1185,
    min_flow = 1186,
    max_flow = 1187,
    min_flow_penalty_cost = 1188,
    max_flow_penalty_cost = 1189,
    min_start_pressure = 1190
  };
  using tunnel_attribute_info_t = std::tuple<shop_tunnel_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr tunnel_attribute_info_t shop_tunnel_attribute_info[] {
  { shop_tunnel_attribute::start_height, "start_height", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::end_height, "end_height", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::diameter, "diameter", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::length, "length", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::loss_factor, "loss_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::s2_per_m5, .y_unit = shop_y_unit::s2_per_m5, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::weir_width, "weir_width", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::time_delay, "time_delay", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::gate_opening_curve, "gate_opening_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::gate_adjustment_cost, "gate_adjustment_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::gate_opening_schedule, "gate_opening_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::initial_opening, "initial_opening", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::continuous_gate, "continuous_gate", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::end_pressure, "end_pressure", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::sim_end_pressure, "sim_end_pressure", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::flow, "flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::physical_flow, "physical_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::sim_flow, "sim_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::gate_opening, "gate_opening", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::network_no, "network_no", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_tunnel_attribute::min_flow, "min_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::max_flow, "max_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::min_flow_penalty_cost, "min_flow_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::max_flow_penalty_cost, "max_flow_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_tunnel_attribute::min_start_pressure, "min_start_pressure", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_interlock_constraint_attribute {
    min_open = 1191,
    max_open = 1192,
    forward_switch_time = 1193,
    backward_switch_time = 1194
  };
  using interlock_constraint_attribute_info_t =
    std::tuple<shop_interlock_constraint_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr interlock_constraint_attribute_info_t shop_interlock_constraint_attribute_info[] {
  { shop_interlock_constraint_attribute::min_open, "min_open", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_interlock_constraint_attribute::max_open, "max_open", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_interlock_constraint_attribute::forward_switch_time, "forward_switch_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_interlock_constraint_attribute::backward_switch_time, "backward_switch_time", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_flow_constraint_attribute {
  };

  enum class shop_lp_model_attribute {
    sim_mode = 1195,
    var_type_names = 1196,
    var_type_abbrev = 1197,
    var_type_index_type_beg = 1198,
    var_type_index_type_cnt = 1199,
    var_type_index_type_val = 1200,
    row_type_names = 1201,
    row_type_index_type_beg = 1202,
    row_type_index_type_cnt = 1203,
    row_type_index_type_val = 1204,
    index_type_names = 1205,
    index_type_desc_beg = 1206,
    index_type_desc_cnt = 1207,
    index_type_desc_index = 1208,
    index_type_desc_val = 1209,
    AA = 1210,
    Irow = 1211,
    Jcol = 1212,
    rhs = 1213,
    sense = 1214,
    ub = 1215,
    lb = 1216,
    cc = 1217,
    bin = 1218,
    x = 1219,
    dual = 1220,
    var_type = 1221,
    var_index_beg = 1222,
    var_index_cnt = 1223,
    var_index_val = 1224,
    row_type = 1225,
    row_index_beg = 1226,
    row_index_cnt = 1227,
    row_index_val = 1228,
    add_row_type = 1229,
    add_row_index = 1230,
    add_row_variables = 1231,
    add_row_coeff = 1232,
    add_row_rhs = 1233,
    add_row_sense = 1234,
    add_row_last = 1235,
    add_var_type = 1236,
    add_var_index = 1237,
    add_var_ub = 1238,
    add_var_lb = 1239,
    add_var_cc = 1240,
    add_var_bin = 1241,
    add_var_last = 1242
  };
  using lp_model_attribute_info_t = std::tuple<shop_lp_model_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr lp_model_attribute_info_t shop_lp_model_attribute_info[] {
  { shop_lp_model_attribute::sim_mode, "sim_mode", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::var_type_names, "var_type_names", { .data_type = shop_data_type::shop_string_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_type_abbrev, "var_type_abbrev", { .data_type = shop_data_type::shop_string_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_type_index_type_beg, "var_type_index_type_beg", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_type_index_type_cnt, "var_type_index_type_cnt", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_type_index_type_val, "var_type_index_type_val", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_type_names, "row_type_names", { .data_type = shop_data_type::shop_string_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_type_index_type_beg, "row_type_index_type_beg", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_type_index_type_cnt, "row_type_index_type_cnt", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_type_index_type_val, "row_type_index_type_val", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::index_type_names, "index_type_names", { .data_type = shop_data_type::shop_string_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::index_type_desc_beg, "index_type_desc_beg", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::index_type_desc_cnt, "index_type_desc_cnt", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::index_type_desc_index, "index_type_desc_index", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::index_type_desc_val, "index_type_desc_val", { .data_type = shop_data_type::shop_string_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::AA, "AA", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::Irow, "Irow", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::Jcol, "Jcol", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::rhs, "rhs", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::sense, "sense", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::ub, "ub", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::lb, "lb", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::cc, "cc", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::bin, "bin", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::x, "x", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::dual, "dual", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_type, "var_type", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_index_beg, "var_index_beg", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_index_cnt, "var_index_cnt", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::var_index_val, "var_index_val", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_type, "row_type", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_index_beg, "row_index_beg", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_index_cnt, "row_index_cnt", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::row_index_val, "row_index_val", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::add_row_type, "add_row_type", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_row_index, "add_row_index", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_row_variables, "add_row_variables", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_row_coeff, "add_row_coeff", { .data_type = shop_data_type::shop_double_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_row_rhs, "add_row_rhs", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_row_sense, "add_row_sense", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_row_last, "add_row_last", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_lp_model_attribute::add_var_type, "add_var_type", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_var_index, "add_var_index", { .data_type = shop_data_type::shop_int_array, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_var_ub, "add_var_ub", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_var_lb, "add_var_lb", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_var_cc, "add_var_cc", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_var_bin, "add_var_bin", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_lp_model_attribute::add_var_last, "add_var_last", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_river_attribute {
    length = 1243,
    upstream_elevation = 1244,
    downstream_elevation = 1245,
    time_delay_const = 1246,
    delayed_water_energy_value = 1247,
    delayed_water_value = 1248,
    initial_gate_opening = 1249,
    linear_ref_flow = 1250,
    implicit_factor = 1251,
    main_river = 1252,
    submerged_weir = 1253,
    linear_submerged_weir_flow = 1254,
    width_depth_curve = 1255,
    flow_cost_curve = 1256,
    peak_flow_cost_curve = 1257,
    time_delay_curve = 1258,
    past_upstream_flow = 1259,
    up_head_flow_curve = 1260,
    gate_opening_curve = 1261,
    delta_head_ref_up_flow_curve = 1262,
    delta_head_ref_down_flow_curve = 1263,
    inflow = 1264,
    min_flow = 1265,
    max_flow = 1266,
    min_flow_penalty_cost = 1267,
    max_flow_penalty_cost = 1268,
    ramping_up = 1269,
    ramping_down = 1270,
    ramping_up_penalty_cost = 1271,
    ramping_down_penalty_cost = 1272,
    cost_curve_scaling = 1273,
    flow_schedule = 1274,
    flow_schedule_penalty_cost = 1275,
    gate_opening_schedule = 1276,
    flow_block_merge_tolerance = 1277,
    gate_ramping = 1278,
    gate_ramping_penalty_cost = 1279,
    gate_adjustment_cost = 1280,
    flow_cost = 1281,
    mip_flag = 1282,
    flow = 1283,
    upstream_flow = 1284,
    downstream_flow = 1285,
    gate_height = 1286,
    min_flow_penalty = 1287,
    max_flow_penalty = 1288,
    ramping_up_penalty = 1289,
    ramping_down_penalty = 1290,
    flow_penalty = 1291,
    peak_flow_penalty = 1292,
    gate_ramping_penalty = 1293,
    gate_adjustment_penalty = 1294,
    flow_schedule_penalty = 1295,
    physical_flow = 1296,
    initial_downstream_flow = 1297,
    distributed_past_upstream_flow = 1298,
    sim_flow = 1299
  };
  using river_attribute_info_t = std::tuple<shop_river_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr river_attribute_info_t shop_river_attribute_info[] {
  { shop_river_attribute::length, "length", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::upstream_elevation, "upstream_elevation", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::downstream_elevation, "downstream_elevation", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::time_delay_const, "time_delay_const", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::delayed_water_energy_value, "delayed_water_energy_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::delayed_water_value, "delayed_water_value", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mm3, .y_unit = shop_y_unit::nok_per_mm3, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::initial_gate_opening, "initial_gate_opening", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::linear_ref_flow, "linear_ref_flow", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::implicit_factor, "implicit_factor", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::main_river, "main_river", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::submerged_weir, "submerged_weir", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::linear_submerged_weir_flow, "linear_submerged_weir_flow", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::width_depth_curve, "width_depth_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::flow_cost_curve, "flow_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::peak_flow_cost_curve, "peak_flow_cost_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::time_delay_curve, "time_delay_curve", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::past_upstream_flow, "past_upstream_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::up_head_flow_curve, "up_head_flow_curve", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::gate_opening_curve, "gate_opening_curve", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::delta_head_ref_up_flow_curve, "delta_head_ref_up_flow_curve", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::delta_head_ref_down_flow_curve, "delta_head_ref_down_flow_curve", { .data_type = shop_data_type::shop_xy_array, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::inflow, "inflow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::min_flow, "min_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::max_flow, "max_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::min_flow_penalty_cost, "min_flow_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_h_per_m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::max_flow_penalty_cost, "max_flow_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_h_per_m3_per_s, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::ramping_up, "ramping_up", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3sec_hour, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::ramping_down, "ramping_down", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3sec_hour, .y_unit = shop_y_unit::m3sec_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::ramping_up_penalty_cost, "ramping_up_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::ramping_down_penalty_cost, "ramping_down_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_m3_per_s, .y_unit = shop_y_unit::nok_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::cost_curve_scaling, "cost_curve_scaling", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::flow_schedule, "flow_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::flow_schedule_penalty_cost, "flow_schedule_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::gate_opening_schedule, "gate_opening_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::flow_block_merge_tolerance, "flow_block_merge_tolerance", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::gate_ramping, "gate_ramping", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::meter_per_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::gate_ramping_penalty_cost, "gate_ramping_penalty_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_meter_hour, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::gate_adjustment_cost, "gate_adjustment_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_meter, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::flow_cost, "flow_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_h_per_m3_per_s, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::mip_flag, "mip_flag", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_river_attribute::flow, "flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::upstream_flow, "upstream_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::downstream_flow, "downstream_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::m3_per_s, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::gate_height, "gate_height", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::meter, .y_unit = shop_y_unit::meter, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::min_flow_penalty, "min_flow_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::max_flow_penalty, "max_flow_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::ramping_up_penalty, "ramping_up_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::ramping_down_penalty, "ramping_down_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::flow_penalty, "flow_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::peak_flow_penalty, "peak_flow_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::gate_ramping_penalty, "gate_ramping_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::gate_adjustment_penalty, "gate_adjustment_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::flow_schedule_penalty, "flow_schedule_penalty", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::physical_flow, "physical_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::initial_downstream_flow, "initial_downstream_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::distributed_past_upstream_flow, "distributed_past_upstream_flow", { .data_type = shop_data_type::shop_xy, .x_unit = shop_x_unit::hour, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_river_attribute::sim_flow, "sim_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::m3_per_s, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_busbar_attribute {
    load = 1308,
    ptdf = 1309,
    energy_price = 1310,
    power_deficit = 1311,
    power_excess = 1312
  };
  using busbar_attribute_info_t = std::tuple<shop_busbar_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr busbar_attribute_info_t shop_busbar_attribute_info[] {
  { shop_busbar_attribute::load, "load", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_busbar_attribute::ptdf, "ptdf", { .data_type = shop_data_type::shop_sy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_busbar_attribute::energy_price, "energy_price", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_busbar_attribute::power_deficit, "power_deficit", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_busbar_attribute::power_excess, "power_excess", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_ac_line_attribute {
    max_forward_flow = 1313,
    max_backward_flow = 1314,
    n_loss_segments = 1315,
    loss_factor_linear = 1316,
    loss_factor_quadratic = 1317,
    flow = 1318,
    forward_loss = 1319,
    backward_loss = 1320
  };
  using ac_line_attribute_info_t = std::tuple<shop_ac_line_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr ac_line_attribute_info_t shop_ac_line_attribute_info[] {
  { shop_ac_line_attribute::max_forward_flow, "max_forward_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_ac_line_attribute::max_backward_flow, "max_backward_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_ac_line_attribute::n_loss_segments, "n_loss_segments", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_ac_line_attribute::loss_factor_linear, "loss_factor_linear", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_ac_line_attribute::loss_factor_quadratic, "loss_factor_quadratic", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_ac_line_attribute::flow, "flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_ac_line_attribute::forward_loss, "forward_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_ac_line_attribute::backward_loss, "backward_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_dc_line_attribute {
    max_forward_flow = 1321,
    max_backward_flow = 1322,
    n_loss_segments = 1323,
    loss_constant = 1324,
    loss_factor_linear = 1325,
    loss_factor_quadratic = 1326,
    flow = 1327,
    forward_loss = 1328,
    backward_loss = 1329
  };
  using dc_line_attribute_info_t = std::tuple<shop_dc_line_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr dc_line_attribute_info_t shop_dc_line_attribute_info[] {
  { shop_dc_line_attribute::max_forward_flow, "max_forward_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_dc_line_attribute::max_backward_flow, "max_backward_flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_dc_line_attribute::n_loss_segments, "n_loss_segments", { .data_type = shop_data_type::shop_int, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_dc_line_attribute::loss_constant, "loss_constant", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_dc_line_attribute::loss_factor_linear, "loss_factor_linear", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_dc_line_attribute::loss_factor_quadratic, "loss_factor_quadratic", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_dc_line_attribute::flow, "flow", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_dc_line_attribute::forward_loss, "forward_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_dc_line_attribute::backward_loss, "backward_loss", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_gen_reserve_capability_attribute {
    reserve_type_name = 1375,
    p_extended = 1376,
    schedule = 1377
  };
  using gen_reserve_capability_attribute_info_t =
    std::tuple<shop_gen_reserve_capability_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr gen_reserve_capability_attribute_info_t shop_gen_reserve_capability_attribute_info[] {
  { shop_gen_reserve_capability_attribute::reserve_type_name, "reserve_type_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gen_reserve_capability_attribute::p_extended, "p_extended", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_gen_reserve_capability_attribute::schedule, "schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_pump_reserve_capability_attribute {
    reserve_type_name = 1378,
    p_extended = 1379,
    schedule = 1380
  };
  using pump_reserve_capability_attribute_info_t =
    std::tuple<shop_pump_reserve_capability_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr pump_reserve_capability_attribute_info_t shop_pump_reserve_capability_attribute_info[] {
  { shop_pump_reserve_capability_attribute::reserve_type_name, "reserve_type_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_reserve_capability_attribute::p_extended, "p_extended", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_pump_reserve_capability_attribute::schedule, "schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_plant_reserve_capability_attribute {
    reserve_type_name = 1381,
    p_gen_extended = 1382,
    p_pump_extended = 1383,
    schedule = 1384
  };
  using plant_reserve_capability_attribute_info_t =
    std::tuple<shop_plant_reserve_capability_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr plant_reserve_capability_attribute_info_t shop_plant_reserve_capability_attribute_info[] {
  { shop_plant_reserve_capability_attribute::reserve_type_name, "reserve_type_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_reserve_capability_attribute::p_gen_extended, "p_gen_extended", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_reserve_capability_attribute::p_pump_extended, "p_pump_extended", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_plant_reserve_capability_attribute::schedule, "schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_needle_comb_reserve_capability_attribute {
    reserve_type_name = 1385,
    p_extended = 1386
  };
  using needle_comb_reserve_capability_attribute_info_t =
    std::tuple<shop_needle_comb_reserve_capability_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr needle_comb_reserve_capability_attribute_info_t shop_needle_comb_reserve_capability_attribute_info[] {
  { shop_needle_comb_reserve_capability_attribute::reserve_type_name, "reserve_type_name", { .data_type = shop_data_type::shop_string, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_needle_comb_reserve_capability_attribute::p_extended, "p_extended", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } }
};
  // clang-format on

  enum class shop_battery_attribute {
    charge_efficiency = 1330,
    discharge_efficiency = 1331,
    max_charge_power = 1332,
    max_discharge_power = 1333,
    max_energy = 1334,
    initial_energy = 1335,
    discharge_cost = 1336,
    charge_cost = 1337,
    max_charge_power_constraint = 1338,
    max_discharge_power_constraint = 1339,
    max_energy_constraint = 1340,
    min_energy_constraint = 1341,
    charge_discharge_mip = 1342,
    charge_schedule = 1343,
    discharge_schedule = 1344,
    net_discharge_schedule = 1345,
    fcr_n_up_cost = 1346,
    fcr_n_down_cost = 1347,
    fcr_d_up_cost = 1348,
    fcr_d_down_cost = 1349,
    frr_up_cost = 1350,
    frr_down_cost = 1351,
    rr_up_cost = 1352,
    rr_down_cost = 1353,
    power_charge = 1354,
    power_discharge = 1355,
    net_power_discharge = 1356,
    energy = 1357,
    energy_value = 1358,
    fcr_n_up_delivery = 1359,
    fcr_n_down_delivery = 1360,
    fcr_d_up_delivery = 1361,
    fcr_d_down_delivery = 1362,
    frr_up_delivery = 1363,
    frr_down_delivery = 1364,
    rr_up_delivery = 1365,
    rr_down_delivery = 1366
  };
  using battery_attribute_info_t = std::tuple<shop_battery_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr battery_attribute_info_t shop_battery_attribute_info[] {
  { shop_battery_attribute::charge_efficiency, "charge_efficiency", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::discharge_efficiency, "discharge_efficiency", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::max_charge_power, "max_charge_power", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::max_discharge_power, "max_discharge_power", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::max_energy, "max_energy", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::initial_energy, "initial_energy", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::discharge_cost, "discharge_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::charge_cost, "charge_cost", { .data_type = shop_data_type::shop_double, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::max_charge_power_constraint, "max_charge_power_constraint", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::max_discharge_power_constraint, "max_discharge_power_constraint", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::max_energy_constraint, "max_energy_constraint", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::min_energy_constraint, "min_energy_constraint", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::charge_discharge_mip, "charge_discharge_mip", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::no_unit, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::charge_schedule, "charge_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::discharge_schedule, "discharge_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::net_discharge_schedule, "net_discharge_schedule", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::fcr_n_up_cost, "fcr_n_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::fcr_n_down_cost, "fcr_n_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::fcr_d_up_cost, "fcr_d_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::fcr_d_down_cost, "fcr_d_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::frr_up_cost, "frr_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::frr_down_cost, "frr_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::rr_up_cost, "rr_up_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::rr_down_cost, "rr_down_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::nok_per_mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_battery_attribute::power_charge, "power_charge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::power_discharge, "power_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::net_power_discharge, "net_power_discharge", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::energy, "energy", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mwh, .y_unit = shop_y_unit::mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::energy_value, "energy_value", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::fcr_n_up_delivery, "fcr_n_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::fcr_n_down_delivery, "fcr_n_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::fcr_d_up_delivery, "fcr_d_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::fcr_d_down_delivery, "fcr_d_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::frr_up_delivery, "frr_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::frr_down_delivery, "frr_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::rr_up_delivery, "rr_up_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_battery_attribute::rr_down_delivery, "rr_down_delivery", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::no_unit, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_wind_attribute {
    power_forecast = 1367,
    curtailment_cost = 1368,
    power_delivered = 1369,
    power_curtailed = 1370
  };
  using wind_attribute_info_t = std::tuple<shop_wind_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr wind_attribute_info_t shop_wind_attribute_info[] {
  { shop_wind_attribute::power_forecast, "power_forecast", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_wind_attribute::curtailment_cost, "curtailment_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_wind_attribute::power_delivered, "power_delivered", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_wind_attribute::power_curtailed, "power_curtailed", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_solar_attribute {
    power_forecast = 1371,
    curtailment_cost = 1372,
    power_delivered = 1373,
    power_curtailed = 1374
  };
  using solar_attribute_info_t = std::tuple<shop_solar_attribute, char const * const, shop_attribute_info>;
  // clang-format off
static constexpr solar_attribute_info_t shop_solar_attribute_info[] {
  { shop_solar_attribute::power_forecast, "power_forecast", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_solar_attribute::curtailment_cost, "curtailment_cost", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::nok_per_mwh, .y_unit = shop_y_unit::nok_per_mwh, .is_object_parameter = false, .is_input = true, .is_output = false } },
  { shop_solar_attribute::power_delivered, "power_delivered", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } },
  { shop_solar_attribute::power_curtailed, "power_curtailed", { .data_type = shop_data_type::shop_txy, .x_unit = shop_x_unit::mw, .y_unit = shop_y_unit::mw, .is_object_parameter = false, .is_input = false, .is_output = true } }
};
  // clang-format on

  enum class shop_reservoir_relation {
    connection_standard,
    connection_spill,
    connection_bypass
  };
  using shop_reservoir_relation_info_t = std::tuple<shop_reservoir_relation, char const * const>;
  // clang-format off
static constexpr shop_reservoir_relation_info_t shop_reservoir_relation_info[] {
  { shop_reservoir_relation::connection_standard, "connection_standard"},
  { shop_reservoir_relation::connection_spill, "connection_spill"},
  { shop_reservoir_relation::connection_bypass, "connection_bypass"}
};
  // clang-format on
  enum class shop_plant_relation {
    connection_standard
  };
  using shop_plant_relation_info_t = std::tuple<shop_plant_relation, char const * const>;
  // clang-format off
static constexpr shop_plant_relation_info_t shop_plant_relation_info[] {
  { shop_plant_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_generator_relation {
    connection_standard
  };
  using shop_generator_relation_info_t = std::tuple<shop_generator_relation, char const * const>;
  // clang-format off
static constexpr shop_generator_relation_info_t shop_generator_relation_info[] {
  { shop_generator_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_needle_combination_relation {
    connection_standard
  };
  using shop_needle_combination_relation_info_t = std::tuple<shop_needle_combination_relation, char const * const>;
  // clang-format off
static constexpr shop_needle_combination_relation_info_t shop_needle_combination_relation_info[] {
  { shop_needle_combination_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_pump_relation {
    connection_standard
  };
  using shop_pump_relation_info_t = std::tuple<shop_pump_relation, char const * const>;
  // clang-format off
static constexpr shop_pump_relation_info_t shop_pump_relation_info[] {
  { shop_pump_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_gate_relation {
    connection_standard,
    connection_spill,
    connection_bypass
  };
  using shop_gate_relation_info_t = std::tuple<shop_gate_relation, char const * const>;
  // clang-format off
static constexpr shop_gate_relation_info_t shop_gate_relation_info[] {
  { shop_gate_relation::connection_standard, "connection_standard"},
  { shop_gate_relation::connection_spill, "connection_spill"},
  { shop_gate_relation::connection_bypass, "connection_bypass"}
};
  // clang-format on
  enum class shop_thermal_relation {
    connection_standard
  };
  using shop_thermal_relation_info_t = std::tuple<shop_thermal_relation, char const * const>;
  // clang-format off
static constexpr shop_thermal_relation_info_t shop_thermal_relation_info[] {
  { shop_thermal_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_junction_relation {
    connection_standard
  };
  using shop_junction_relation_info_t = std::tuple<shop_junction_relation, char const * const>;
  // clang-format off
static constexpr shop_junction_relation_info_t shop_junction_relation_info[] {
  { shop_junction_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_junction_gate_relation {
    connection_standard
  };
  using shop_junction_gate_relation_info_t = std::tuple<shop_junction_gate_relation, char const * const>;
  // clang-format off
static constexpr shop_junction_gate_relation_info_t shop_junction_gate_relation_info[] {
  { shop_junction_gate_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_creek_intake_relation {
    connection_standard,
    connection_spill
  };
  using shop_creek_intake_relation_info_t = std::tuple<shop_creek_intake_relation, char const * const>;
  // clang-format off
static constexpr shop_creek_intake_relation_info_t shop_creek_intake_relation_info[] {
  { shop_creek_intake_relation::connection_standard, "connection_standard"},
  { shop_creek_intake_relation::connection_spill, "connection_spill"}
};
  // clang-format on
  enum class shop_contract_relation {
    connection_standard
  };
  using shop_contract_relation_info_t = std::tuple<shop_contract_relation, char const * const>;
  // clang-format off
static constexpr shop_contract_relation_info_t shop_contract_relation_info[] {
  { shop_contract_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_market_relation {
    connection_standard
  };
  using shop_market_relation_info_t = std::tuple<shop_market_relation, char const * const>;
  // clang-format off
static constexpr shop_market_relation_info_t shop_market_relation_info[] {
  { shop_market_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_reserve_group_relation {
    connection_standard
  };
  using shop_reserve_group_relation_info_t = std::tuple<shop_reserve_group_relation, char const * const>;
  // clang-format off
static constexpr shop_reserve_group_relation_info_t shop_reserve_group_relation_info[] {
  { shop_reserve_group_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_commit_group_relation {
    connection_standard
  };
  using shop_commit_group_relation_info_t = std::tuple<shop_commit_group_relation, char const * const>;
  // clang-format off
static constexpr shop_commit_group_relation_info_t shop_commit_group_relation_info[] {
  { shop_commit_group_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_discharge_group_relation {
    connection_standard
  };
  using shop_discharge_group_relation_info_t = std::tuple<shop_discharge_group_relation, char const * const>;
  // clang-format off
static constexpr shop_discharge_group_relation_info_t shop_discharge_group_relation_info[] {
  { shop_discharge_group_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_production_group_relation {
    connection_standard
  };
  using shop_production_group_relation_info_t = std::tuple<shop_production_group_relation, char const * const>;
  // clang-format off
static constexpr shop_production_group_relation_info_t shop_production_group_relation_info[] {
  { shop_production_group_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_volume_constraint_relation {
    connection_standard
  };
  using shop_volume_constraint_relation_info_t = std::tuple<shop_volume_constraint_relation, char const * const>;
  // clang-format off
static constexpr shop_volume_constraint_relation_info_t shop_volume_constraint_relation_info[] {
  { shop_volume_constraint_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_bid_group_relation {
    connection_standard
  };
  using shop_bid_group_relation_info_t = std::tuple<shop_bid_group_relation, char const * const>;
  // clang-format off
static constexpr shop_bid_group_relation_info_t shop_bid_group_relation_info[] {
  { shop_bid_group_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_cut_group_relation {
    connection_standard
  };
  using shop_cut_group_relation_info_t = std::tuple<shop_cut_group_relation, char const * const>;
  // clang-format off
static constexpr shop_cut_group_relation_info_t shop_cut_group_relation_info[] {
  { shop_cut_group_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_inflow_series_relation {
    connection_standard
  };
  using shop_inflow_series_relation_info_t = std::tuple<shop_inflow_series_relation, char const * const>;
  // clang-format off
static constexpr shop_inflow_series_relation_info_t shop_inflow_series_relation_info[] {
  { shop_inflow_series_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_unit_combination_relation {
    connection_standard
  };
  using shop_unit_combination_relation_info_t = std::tuple<shop_unit_combination_relation, char const * const>;
  // clang-format off
static constexpr shop_unit_combination_relation_info_t shop_unit_combination_relation_info[] {
  { shop_unit_combination_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_tunnel_relation {
    connection_standard
  };
  using shop_tunnel_relation_info_t = std::tuple<shop_tunnel_relation, char const * const>;
  // clang-format off
static constexpr shop_tunnel_relation_info_t shop_tunnel_relation_info[] {
  { shop_tunnel_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_interlock_constraint_relation {
    connection_standard
  };
  using shop_interlock_constraint_relation_info_t = std::tuple<shop_interlock_constraint_relation, char const * const>;
  // clang-format off
static constexpr shop_interlock_constraint_relation_info_t shop_interlock_constraint_relation_info[] {
  { shop_interlock_constraint_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_flow_constraint_relation {
    connection_standard
  };
  using shop_flow_constraint_relation_info_t = std::tuple<shop_flow_constraint_relation, char const * const>;
  // clang-format off
static constexpr shop_flow_constraint_relation_info_t shop_flow_constraint_relation_info[] {
  { shop_flow_constraint_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_river_relation {
    connection_standard
  };
  using shop_river_relation_info_t = std::tuple<shop_river_relation, char const * const>;
  // clang-format off
static constexpr shop_river_relation_info_t shop_river_relation_info[] {
  { shop_river_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_busbar_relation {
    connection_standard
  };
  using shop_busbar_relation_info_t = std::tuple<shop_busbar_relation, char const * const>;
  // clang-format off
static constexpr shop_busbar_relation_info_t shop_busbar_relation_info[] {
  { shop_busbar_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_ac_line_relation {
    connection_standard
  };
  using shop_ac_line_relation_info_t = std::tuple<shop_ac_line_relation, char const * const>;
  // clang-format off
static constexpr shop_ac_line_relation_info_t shop_ac_line_relation_info[] {
  { shop_ac_line_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_dc_line_relation {
    connection_standard
  };
  using shop_dc_line_relation_info_t = std::tuple<shop_dc_line_relation, char const * const>;
  // clang-format off
static constexpr shop_dc_line_relation_info_t shop_dc_line_relation_info[] {
  { shop_dc_line_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_gen_reserve_capability_relation {
    connection_standard
  };
  using shop_gen_reserve_capability_relation_info_t =
    std::tuple<shop_gen_reserve_capability_relation, char const * const>;
  // clang-format off
static constexpr shop_gen_reserve_capability_relation_info_t shop_gen_reserve_capability_relation_info[] {
  { shop_gen_reserve_capability_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_pump_reserve_capability_relation {
    connection_standard
  };
  using shop_pump_reserve_capability_relation_info_t =
    std::tuple<shop_pump_reserve_capability_relation, char const * const>;
  // clang-format off
static constexpr shop_pump_reserve_capability_relation_info_t shop_pump_reserve_capability_relation_info[] {
  { shop_pump_reserve_capability_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_plant_reserve_capability_relation {
    connection_standard
  };
  using shop_plant_reserve_capability_relation_info_t =
    std::tuple<shop_plant_reserve_capability_relation, char const * const>;
  // clang-format off
static constexpr shop_plant_reserve_capability_relation_info_t shop_plant_reserve_capability_relation_info[] {
  { shop_plant_reserve_capability_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_needle_comb_reserve_capability_relation {
    connection_standard
  };
  using shop_needle_comb_reserve_capability_relation_info_t =
    std::tuple<shop_needle_comb_reserve_capability_relation, char const * const>;
  // clang-format off
static constexpr shop_needle_comb_reserve_capability_relation_info_t shop_needle_comb_reserve_capability_relation_info[] {
  { shop_needle_comb_reserve_capability_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_battery_relation {
    connection_standard
  };
  using shop_battery_relation_info_t = std::tuple<shop_battery_relation, char const * const>;
  // clang-format off
static constexpr shop_battery_relation_info_t shop_battery_relation_info[] {
  { shop_battery_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_wind_relation {
    connection_standard
  };
  using shop_wind_relation_info_t = std::tuple<shop_wind_relation, char const * const>;
  // clang-format off
static constexpr shop_wind_relation_info_t shop_wind_relation_info[] {
  { shop_wind_relation::connection_standard, "connection_standard"}
};
  // clang-format on
  enum class shop_solar_relation {
    connection_standard
  };
  using shop_solar_relation_info_t = std::tuple<shop_solar_relation, char const * const>;
  // clang-format off
static constexpr shop_solar_relation_info_t shop_solar_relation_info[] {
  { shop_solar_relation::connection_standard, "connection_standard"}
};
  // clang-format on

  enum class shop_command {
    set_newgate,
    set_password,
    print_model,
    log_file,
    read_model,
    add_model,
    start_sim,
    start_shopsim,
    save_optres_for_shopsim,
    set_method,
    set_ramping,
    penalty_cost,
    penalty_flag,
    set_mipgap,
    set_nseg,
    set_mip_nseg,
    set_dyn_seg,
    set_nodelog,
    set_max_num_threads,
    set_parallel_mode,
    set_capacity,
    set_headopt_feedback,
    set_code,
    set_timelimit,
    set_dyn_flex_mip,
    set_universal_mip,
    set_universal_river_mip,
    set_merge,
    save_xmlseries,
    save_series,
    return_simres,
    set_com_dec_period,
    return_scenario_result_table,
    set_time_delay_unit,
    set_power_head_optimization,
    set_pump_head_optimization,
    set_bypass_loss,
    set_reserve_slack_cost,
    set_fcr_n_band,
    set_fcr_d_band,
    set_fcr_n_equality_flag,
    print_bid_matrix,
    interpolate_bid_matrix,
    set_linear_startup,
    set_bid_aggregation_level,
    set_delay_valuation,
    set_reserve_ramping_cost,
    set_solver,
    set_safe_mode,
    set_gen_turn_off_limit,
    print_bp_curves,
    set_prod_from_ref_prod,
    save_tunnelloss,
    set_startup_cost_printout,
    print_pqcurves,
    set_reserve_min_capacity,
    set_ownership_scaling,
    set_stop_cost_from_start_cost,
    set_simple_pq_recovery,
    set_plant_uploading,
    set_lp_info,
    save_xmlshopsimseries,
    set_sim_schedule_correction,
    save_case,
    set_xml_system_name,
    set_xmllog,
    expand_window,
    send_time,
    ignore_data,
    quit,
    data,
    set_mix_cut_vv,
    set_tuning,
    set_epsilon,
    read_optbasis,
    save_optbasis,
    set_test,
    set_optbasis,
    set_presim,
    set_inteps,
    set_contiffeassol,
    set_scale_mode,
    set_mipprogress,
    set_iteration_gap,
    individual_penalty,
    individual_endpenalty,
    save_tables,
    test_case,
    lose_spill,
    set_water_value,
    print_result,
    set_extra_file,
    set_universal_affinity,
    set_silent,
    set_gravity,
    set_rsv_storage,
    save_shopsimseries,
    return_shopsimres,
    set_reserve_schedule_penalty_cost,
    set_reserve_penalty_cost,
    set_dyn_juncloss,
    set_duals_from_mip,
    save_pq_curves,
    reset_ownership,
    set_overflow_cut_description,
    set_initial_reservoir,
    set_plant_unbalance_recommit,
    set_mip_progress_log,
    print_yaml,
    set_droop_discretization_limit,
    set_build_original_pq_curves_by_discharge_limits,
    set_pump_turn_off_limit,
    set_universal_overflow_mip,
    reset_lp_model,
    set_droop_cost_exponent,
    set_droop_ref_value,
    set_rr_up_schedule_slack_flag,
    set_lp_row,
    set_lp_var,
    print_bp_bid_matrix,
    set_ramp_scale_factor,
    set_recommit,
    set_rsv_hard_limit_penalty_cost,
    read_yaml,
    print_mc_curves,
    set_reserve_activation_penalty_cost,
    set_reserve_activation_time,
    set_reserve_activation_factor,
    set_reserve_tactical_activation_cost_scaling,
    create_bp_curves,
    print_ascii,
    set_plant_max_prod_reserve_strategy,
    set_plant_min_prod_reserve_strategy,
    set_simtimestep,
    set_universal_strict_pq_uploading
  };
  using shop_command_info_t = std::tuple<shop_command, char const * const>;
  // clang-format off
static constexpr shop_command_info_t shop_command_info[] {
  { shop_command::set_newgate, "set newgate"},
  { shop_command::set_password, "set password"},
  { shop_command::print_model, "print model"},
  { shop_command::log_file, "log file"},
  { shop_command::read_model, "read model"},
  { shop_command::add_model, "add model"},
  { shop_command::start_sim, "start sim"},
  { shop_command::start_shopsim, "start shopsim"},
  { shop_command::save_optres_for_shopsim, "save optres_for_shopsim"},
  { shop_command::set_method, "set method"},
  { shop_command::set_ramping, "set ramping"},
  { shop_command::penalty_cost, "penalty cost"},
  { shop_command::penalty_flag, "penalty flag"},
  { shop_command::set_mipgap, "set mipgap"},
  { shop_command::set_nseg, "set nseg"},
  { shop_command::set_mip_nseg, "set mip_nseg"},
  { shop_command::set_dyn_seg, "set dyn_seg"},
  { shop_command::set_nodelog, "set nodelog"},
  { shop_command::set_max_num_threads, "set max_num_threads"},
  { shop_command::set_parallel_mode, "set parallel_mode"},
  { shop_command::set_capacity, "set capacity"},
  { shop_command::set_headopt_feedback, "set headopt_feedback"},
  { shop_command::set_code, "set code"},
  { shop_command::set_timelimit, "set timelimit"},
  { shop_command::set_dyn_flex_mip, "set dyn_flex_mip"},
  { shop_command::set_universal_mip, "set universal_mip"},
  { shop_command::set_universal_river_mip, "set universal_river_mip"},
  { shop_command::set_merge, "set merge"},
  { shop_command::save_xmlseries, "save xmlseries"},
  { shop_command::save_series, "save series"},
  { shop_command::return_simres, "return simres"},
  { shop_command::set_com_dec_period, "set com_dec_period"},
  { shop_command::return_scenario_result_table, "return scenario_result_table"},
  { shop_command::set_time_delay_unit, "set time_delay_unit"},
  { shop_command::set_power_head_optimization, "set power_head_optimization"},
  { shop_command::set_pump_head_optimization, "set pump_head_optimization"},
  { shop_command::set_bypass_loss, "set bypass_loss"},
  { shop_command::set_reserve_slack_cost, "set reserve_slack_cost"},
  { shop_command::set_fcr_n_band, "set fcr_n_band"},
  { shop_command::set_fcr_d_band, "set fcr_d_band"},
  { shop_command::set_fcr_n_equality_flag, "set fcr_n_equality_flag"},
  { shop_command::print_bid_matrix, "print bid_matrix"},
  { shop_command::interpolate_bid_matrix, "interpolate bid_matrix"},
  { shop_command::set_linear_startup, "set linear_startup"},
  { shop_command::set_bid_aggregation_level, "set bid_aggregation_level"},
  { shop_command::set_delay_valuation, "set delay_valuation"},
  { shop_command::set_reserve_ramping_cost, "set reserve_ramping_cost"},
  { shop_command::set_solver, "set solver"},
  { shop_command::set_safe_mode, "set safe_mode"},
  { shop_command::set_gen_turn_off_limit, "set gen_turn_off_limit"},
  { shop_command::print_bp_curves, "print bp_curves"},
  { shop_command::set_prod_from_ref_prod, "set prod_from_ref_prod"},
  { shop_command::save_tunnelloss, "save tunnelloss"},
  { shop_command::set_startup_cost_printout, "set startup_cost_printout"},
  { shop_command::print_pqcurves, "print pqcurves"},
  { shop_command::set_reserve_min_capacity, "set reserve_min_capacity"},
  { shop_command::set_ownership_scaling, "set ownership_scaling"},
  { shop_command::set_stop_cost_from_start_cost, "set stop_cost_from_start_cost"},
  { shop_command::set_simple_pq_recovery, "set simple_pq_recovery"},
  { shop_command::set_plant_uploading, "set plant_uploading"},
  { shop_command::set_lp_info, "set lp_info"},
  { shop_command::save_xmlshopsimseries, "save xmlshopsimseries"},
  { shop_command::set_sim_schedule_correction, "set sim_schedule_correction"},
  { shop_command::save_case, "save case"},
  { shop_command::set_xml_system_name, "set xml_system_name"},
  { shop_command::set_xmllog, "set xmllog"},
  { shop_command::expand_window, "expand window"},
  { shop_command::send_time, "send time"},
  { shop_command::ignore_data, "ignore data"},
  { shop_command::quit, "quit"},
  { shop_command::data, "data"},
  { shop_command::set_mix_cut_vv, "set mix_cut_vv"},
  { shop_command::set_tuning, "set tuning"},
  { shop_command::set_epsilon, "set epsilon"},
  { shop_command::read_optbasis, "read optbasis"},
  { shop_command::save_optbasis, "save optbasis"},
  { shop_command::set_test, "set test"},
  { shop_command::set_optbasis, "set optbasis"},
  { shop_command::set_presim, "set presim"},
  { shop_command::set_inteps, "set inteps"},
  { shop_command::set_contiffeassol, "set contiffeassol"},
  { shop_command::set_scale_mode, "set scale_mode"},
  { shop_command::set_mipprogress, "set mipprogress"},
  { shop_command::set_iteration_gap, "set iteration_gap"},
  { shop_command::individual_penalty, "individual penalty"},
  { shop_command::individual_endpenalty, "individual endpenalty"},
  { shop_command::save_tables, "save tables"},
  { shop_command::test_case, "test case"},
  { shop_command::lose_spill, "lose spill"},
  { shop_command::set_water_value, "set water_value"},
  { shop_command::print_result, "print result"},
  { shop_command::set_extra_file, "set extra_file"},
  { shop_command::set_universal_affinity, "set universal_affinity"},
  { shop_command::set_silent, "set silent"},
  { shop_command::set_gravity, "set gravity"},
  { shop_command::set_rsv_storage, "set rsv_storage"},
  { shop_command::save_shopsimseries, "save shopsimseries"},
  { shop_command::return_shopsimres, "return shopsimres"},
  { shop_command::set_reserve_schedule_penalty_cost, "set reserve_schedule_penalty_cost"},
  { shop_command::set_reserve_penalty_cost, "set reserve_penalty_cost"},
  { shop_command::set_dyn_juncloss, "set dyn_juncloss"},
  { shop_command::set_duals_from_mip, "set duals_from_mip"},
  { shop_command::save_pq_curves, "save pq_curves"},
  { shop_command::reset_ownership, "reset ownership"},
  { shop_command::set_overflow_cut_description, "set overflow_cut_description"},
  { shop_command::set_initial_reservoir, "set initial_reservoir"},
  { shop_command::set_plant_unbalance_recommit, "set plant_unbalance_recommit"},
  { shop_command::set_mip_progress_log, "set mip_progress_log"},
  { shop_command::print_yaml, "print yaml"},
  { shop_command::set_droop_discretization_limit, "set droop_discretization_limit"},
  { shop_command::set_build_original_pq_curves_by_discharge_limits, "set build_original_pq_curves_by_discharge_limits"},
  { shop_command::set_pump_turn_off_limit, "set pump_turn_off_limit"},
  { shop_command::set_universal_overflow_mip, "set universal_overflow_mip"},
  { shop_command::reset_lp_model, "reset lp_model"},
  { shop_command::set_droop_cost_exponent, "set droop_cost_exponent"},
  { shop_command::set_droop_ref_value, "set droop_ref_value"},
  { shop_command::set_rr_up_schedule_slack_flag, "set rr_up_schedule_slack_flag"},
  { shop_command::set_lp_row, "set lp_row"},
  { shop_command::set_lp_var, "set lp_var"},
  { shop_command::print_bp_bid_matrix, "print bp_bid_matrix"},
  { shop_command::set_ramp_scale_factor, "set ramp_scale_factor"},
  { shop_command::set_recommit, "set recommit"},
  { shop_command::set_rsv_hard_limit_penalty_cost, "set rsv_hard_limit_penalty_cost"},
  { shop_command::read_yaml, "read yaml"},
  { shop_command::print_mc_curves, "print mc_curves"},
  { shop_command::set_reserve_activation_penalty_cost, "set reserve_activation_penalty_cost"},
  { shop_command::set_reserve_activation_time, "set reserve_activation_time"},
  { shop_command::set_reserve_activation_factor, "set reserve_activation_factor"},
  { shop_command::set_reserve_tactical_activation_cost_scaling, "set reserve_tactical_activation_cost_scaling"},
  { shop_command::create_bp_curves, "create bp_curves"},
  { shop_command::print_ascii, "print ascii"},
  { shop_command::set_plant_max_prod_reserve_strategy, "set plant_max_prod_reserve_strategy"},
  { shop_command::set_plant_min_prod_reserve_strategy, "set plant_min_prod_reserve_strategy"},
  { shop_command::set_simtimestep, "set simtimestep"},
  { shop_command::set_universal_strict_pq_uploading, "set universal_strict_pq_uploading"}
};
  // clang-format on

  enum class shop_severity {
    OK
  };
  using shop_severity_info_t = std::tuple<shop_severity, char const * const>;
  // clang-format off
static constexpr shop_severity_info_t shop_severity_info[] {
  { shop_severity::OK, "ok"}
};
  // clang-format on

  enum class shop_time_resolution_unit {
    hour,
    minute,
  };
  using shop_time_resolution_unit_info_t = std::tuple<shop_time_resolution_unit, char const * const>;
  static constexpr shop_time_resolution_unit_info_t shop_time_resolution_unit_info[]{
    {  shop_time_resolution_unit::hour,   "hour"},
    {shop_time_resolution_unit::minute, "minute"}
  };

}
