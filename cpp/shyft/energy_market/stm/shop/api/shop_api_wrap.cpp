//
// Created by sih on 2/14/25.
// Empty, but we can intercept functions here.
// used to extract the mangled syms:
// clang-format off
// nm -gu build/ci/cpp/shyft/energy_market/stm/CMakeFiles/shyft-shop-wrap.dir/shop/api/shop_api_wrap.cpp.o | grep Shop | cut -b 12- | sed -z -e 's/\n/;\n        /g' >/tmp/syms.txt
// clang-format on
//
#include <shop_lib_interface.h>

/**
 * @brief use of public shop functions to make them Undefined in the object file.
 * @details
 * The entire purpose of this file is to create an object file
 * that references all the public functions in shop-api.
 * When new versions of shop are released, that breaks the changes, then
 * this file  will need to be updated accordingly, and
 * the `shop_api.map` linker file to be updated with more, or changed symbols.
 * Notice that the symbols in the map file is mangled.
 * So any change to the signature, will require updates.
 *
 * The style/solution to references below is just fastest way to get it
 * referenced and compiled. It is never used, or called.
 * The entire purpose is to reference the used functions.
 */
void use_shop() {
  shop_sys* pSys = nullptr;
  ShopSystem* libSys = nullptr;
  int timeResNPoints = 0;
  int* timeResT = &timeResNPoints;
  char startTime[17], endTime[17], timeUnit[17];
  auto s = ShopInit(pSys);
  (void) ShopInit(true, true, "");
  (void) ShopInit("", true, true, std::string_view(""));
  (void) ShopFree(s);
  (void) ShopSetSilentConsole(s, false);
  (void) ShopSetSilentLog(s, true);

  // SHOP time
  double* timeResY = 0;
  (void) ShopGetTimeResolution(s, startTime, endTime, timeUnit);
  (void) ShopGetTimeResolution(libSys, startTime, endTime, timeUnit, timeResNPoints, timeResT, timeResY);
  // SHOP_DEPRECATED_DANGEROUS("possible buffer overflow") bool ShopGetTimeZone(ShopSystem *libSys, char *timeZone);
  // SHOP_DEPRECATED_BROKEN("startTime is not written to or used") bool GetTimeResolutionDimensions(ShopSystem *libSys,
  // char *startTime, int &nPoints);
  (void) ShopSetTimeResolution(libSys, startTime, endTime, timeUnit);
  (void) ShopSetTimeResolution(libSys, startTime, endTime, timeUnit, timeResNPoints, timeResT, timeResY);
  std::string filePath;
  ShopLogReadingFile(libSys, filePath);
  char const * timeZone = "Europe/Berlin";
  (void) ShopSetTimeZone(libSys, timeZone);
  char const * commandString = "";
  int nOptions = 0;
  char const * const * optionList = nullptr;
  int nObjects = 0;
  char const * const * objectList = nullptr;
  (void) ShopExecuteCommand(libSys, commandString, nOptions, optionList, nObjects, objectList);
  char const * fullCommand = nullptr;
  (void) ShopExecuteCommand(libSys, fullCommand);
  // SHOP_DEPRECATED_BROKEN("complicated usage and wastes memory and space") bool ShopGetExecutedCommands(ShopSystem*
  // libSys, int& nCommands, char** commandList, int maxCommands);
  std::vector<std::string> commands;
  (void) ShopGetExecutedCommands(libSys, commands);
  int nCommands = 0;
  char* commandTypes[] = {};
  (void) ShopGetCommandTypesInSystem(libSys, nCommands, commandTypes);
  char const * fileAsString = 0;
  (void) ShopSetAsciiFileString(libSys, fileAsString);
  (void) ShopReadAsciiStringFile(libSys);
  std::string command;
  std::vector<std::string> options;
  std::vector<std::string> objects;
  ShopLogCommand(libSys, command, options, objects);
  (void) ShopGetObjectTypeCount(libSys);
  int const objectTypeIndex = 0;
  (void) ShopGetObjectTypeName(libSys, objectTypeIndex);
  char const * objectName = nullptr;
  char objectType[10];
  int objectId = 0;
  (void) ShopAddObject(libSys, objectTypeIndex, objectName);
  (void) ShopAddObject(libSys, objectTypeIndex, objectName, objectId);
  (void) ShopAddObject(libSys, objectType, objectName);
  (void) ShopAddObject(libSys, objectType, objectName, objectId);
  (void) ShopGetObjectCount(libSys);
  (void) ShopGetObjectIndex(libSys, objectType, objectName);
  (void) ShopGetObjectIndex(libSys, objectId);
  // Return nullptr: on libSys nullptr, and if objectIndex is out of bounds
  int objectIndex = 0;
  (void) ShopGetObjectType(libSys, objectIndex);
  (void) ShopGetObjectName(libSys, objectIndex);
  // Returns -1 on unknown object type
  (void) ShopGetObjectTypeIndex(libSys, objectType);
  (void) ShopGetObjectId(libSys, objectIndex);
  int nAttributes = 0;
  int attributeIndexList[10];
  (void) ShopGetObjectTypeAttributeIndices(libSys, objectType, nAttributes, attributeIndexList);
  (void) ShopGetObjectTypeAttributeIndices(libSys, objectTypeIndex, nAttributes, attributeIndexList);
  int attributeIndex = 0;
  (void) ShopGetAttributeTypeDescription(libSys, attributeIndex);
  (void) ShopGetAttributeTypeDocumentationUrl(libSys, attributeIndex);
  (void) ShopGetAttributeTypeExample(libSys, attributeIndex);
  (void) ShopGetAttributeTypeExampleUrlPrefix(libSys);
  (void) ShopGetAttributeTypeVersionAdded(libSys, attributeIndex);
  (void) ShopGetAttributeDefaultValue(libSys, attributeIndex);
  (void) ShopGetAttributeIsRequired(libSys, attributeIndex);
  (void) ShopAttributeExists(libSys, objectIndex, attributeIndex);
  (void) ShopObjectIsDefined(libSys, objectType, objectName);

  (void) ShopGetAttributeCount(libSys);
  char attributeName[60];
  (void) ShopGetAttributeIndex(libSys, objectType, attributeName);
  (void) ShopGetAttributeIndex(libSys, objectIndex, attributeName);
  (void) ShopGetAttributeDatatype(libSys, attributeIndex);
  (void) ShopGetAttributeName(libSys, attributeIndex);
  (void) ShopGetAttributeFullName(libSys, attributeIndex);
  (void) ShopGetAttributeDataFuncName(libSys, attributeIndex);
  char xUnit[10];
  char yUnit[10];
  ShopGetAttributeXunit(libSys, attributeIndex, xUnit);
  ShopGetAttributeYunit(libSys, attributeIndex, yUnit);
  (void) ShopGetAttributeLicenseName(libSys, attributeIndex);
  (void) ShopGetAttributeInfoMaxLength(libSys, attributeIndex);
  (void) ShopGetObjectTypeIsInput(libSys, objectTypeIndex);
  (void) ShopGetObjectTypeIsOutput(libSys, objectTypeIndex);
  bool isObjectParam;
  char attributeDataFuncName[10], attributeDatatype[10];
  bool isInput, isOutput;
  ShopGetAttributeInfo(
    libSys,
    attributeIndex,
    objectType,
    isObjectParam,
    attributeName,
    attributeDataFuncName,
    attributeDatatype,
    xUnit,
    yUnit,
    isInput,
    isOutput);
  (void) ShopAttributeIsInput(libSys, attributeIndex);
  (void) ShopAttributeIsOutput(libSys, attributeIndex);
  (void) ShopAttributeIsRequired(libSys, attributeIndex);

  int attributeValue, length;
  (void) ShopGetIntAttribute(libSys, objectIndex, attributeIndex, attributeValue);
  (void) ShopSetIntAttribute(libSys, objectIndex, attributeIndex, attributeValue);
  (void) ShopGetIntArrayLength(libSys, objectIndex, attributeIndex, length);
  int nAttributeValues, attributeValues[10];
  (void) ShopGetIntArrayAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, attributeValues);
  (void) ShopSetIntArrayAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, attributeValues);
  double d_attributeValue;
  (void) ShopGetDoubleAttribute(libSys, objectIndex, attributeIndex, d_attributeValue);
  (void) ShopSetDoubleAttribute(libSys, objectIndex, attributeIndex, d_attributeValue);
  (void) ShopGetDoubleArrayLength(libSys, objectIndex, attributeIndex, length);
  double d_attributeValues[10];
  (void) ShopGetDoubleArrayAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, d_attributeValues);
  (void) ShopSetDoubleArrayAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, d_attributeValues);
  int nPoints = 0;
  (void) ShopGetXyAttributeNPoints(libSys, objectIndex, attributeIndex, nPoints);
  double refValue, d_x[10], d_y[10];
  (void) ShopGetXyAttribute(libSys, objectIndex, attributeIndex, refValue, nPoints, d_x, d_y);
  (void) ShopSetXyAttribute(libSys, objectIndex, attributeIndex, refValue, nPoints, d_x, d_y);

  (void) ShopGetSyAttributeNPoints(libSys, objectIndex, attributeIndex, nPoints);
  // SHOP_DEPRECATED_DANGEROUS("possible buffer overflow when assigning char pointers")
  // bool ShopGetSyAttribute(libSys, objectIndex, attributeIndex, int& nPoints, char** s, double* y);
  std::vector<std::pair<std::string, double>> vp_values;
  (void) ShopGetSyAttribute(libSys, objectIndex, attributeIndex, vp_values);
  char* sy[] = {};
  (void) ShopSetSyAttribute(libSys, objectIndex, attributeIndex, nPoints, sy, d_y);
#if defined(SHOP_HAVE_SPAN)
  std::span<const std::pair<std::string, double>> spv_values;
  (void) ShopSetSyAttribute(libSys, objectIndex, attributeIndex, spv_values);
#endif
  int arrayLength, maxPoints;
  (void) ShopGetXyArrayDimensions(libSys, objectIndex, attributeIndex, arrayLength, maxPoints);
  double dd_refValue[10];
  double *dd_x[10], *dd_y[10];
  (void) ShopGetXyArrayAttribute(
    libSys, objectIndex, attributeIndex, nAttributeValues, dd_refValue, &nPoints, dd_x, dd_y);
  (void) ShopSetXyArrayAttribute(
    libSys, objectIndex, attributeIndex, nAttributeValues, dd_refValue, &nPoints, dd_x, dd_y);
  int i_timeSteps[10], i_nPoints[10];
  (void) ShopSetXytAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, i_timeSteps, i_nPoints, dd_x, dd_y);
  char* timeStrings[10];
  (void) ShopSetXytAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, timeStrings, i_nPoints, dd_x, dd_y);

  int nScen = 0;
  (void) ShopGetTxyAttributeDimensions(libSys, objectIndex, attributeIndex, startTime, nPoints, nScen);
  (void) ShopGetTxyAttributeTimeUnit(libSys, objectIndex, attributeIndex);

  int i_t[10];
  (void) ShopGetTxyAttribute(libSys, objectIndex, attributeIndex, nPoints, nScen, i_t, dd_y);
  (void) ShopSetTxyAttribute(libSys, objectIndex, attributeIndex, startTime, nPoints, nScen, i_t, dd_y);

  char s_attributeValue[10];
  (void) ShopSetStringAttribute(libSys, objectIndex, attributeIndex, s_attributeValue);
  // SHOP_DEPRECATED_DANGEROUS("possible buffer overflow")
  //(void) ShopGetStringAttribute(libSys, objectIndex, attributeIndex, char* attributeValue);
  std::string s_attributeValue2;
  (void) ShopGetStringAttribute(libSys, objectIndex, attributeIndex, s_attributeValue2);

  (void) ShopGetStringArrayLength(libSys, objectIndex, attributeIndex, length);
  char* s_attributeValues[10];
  (void) ShopGetStringArrayAttribute(libSys, objectIndex, attributeIndex, nAttributeValues, s_attributeValues);

  (void) ShopGetXytAttributeNPoints(libSys, objectIndex, attributeIndex, nPoints, (int) 1);
  (void) ShopGetXytAttribute(libSys, objectIndex, attributeIndex, refValue, nPoints, &refValue, &refValue, (int) 1);
  int nTimes = 0;
  (void) ShopGetXytNTimes(libSys, objectIndex, attributeIndex, nTimes);
  int xytTimes[10];
  (void) ShopGetXytTimesIntArray(libSys, objectIndex, attributeIndex, nTimes, xytTimes);
  char* s_xytTimes[10] = {};
  (void) ShopGetXytTimesStringArray(libSys, objectIndex, attributeIndex, nTimes, s_xytTimes);
  char a_time[10];
  (void) ShopConvertStringToTimeIndex(libSys, a_time);

  (void) ShopCheckAttributeToSet(libSys, attributeIndex);
  (void) ShopCheckAttributeToGet(libSys, attributeIndex, false, true);

  int relatedIndex = 0, nRelations = 0, nRelationTypes = 0, maxStringLength = 0, relatedIndexList[10] = {};
  (void) ShopAddRelation(libSys, objectIndex, "char const * relationType", relatedIndex);
  (void) ShopGetInputRelations(libSys, objectIndex, "char const * relationType", nRelations, relatedIndexList);
  (void) ShopGetOutputRelations(libSys, objectIndex, "char const * relationType", nRelations, relatedIndexList);
  (void) ShopGetRelations(libSys, objectIndex, "char const * relationType", nRelations, relatedIndexList);
  (void) ShopGetRelationTypesDimensions(libSys, objectType, nRelationTypes, maxStringLength);
  char* relationTypes[10];
  (void) ShopGetRelationTypes(libSys, objectType, relationTypes);
  (void) ShopGetDefaultRelationType("char const * fromObjectType", "char const * toObjectType");
  char relationCategory[10];
  (void) ShopGetRelationCategory(libSys, (int) 1, (int) 2, relationCategory);
  int verticalPosition, horizontalPosition;
  (void) ShopGetVerticalPosition(libSys, objectIndex, verticalPosition);
  (void) ShopGetHorizontalPosition(libSys, objectIndex, horizontalPosition);

  int nProgress = 0, maxProgress = 0;
  char *descriptionList[] = {}, *severityList[] = {};
  int codeList[10] = {}, percentList[10] = {}, timestampList[10] = {};
  (void) ShopGetProgress(
    libSys, nProgress, descriptionList, percentList, timestampList, severityList, codeList, maxProgress);

  (void) ShopDeleteLibSys(libSys);
  (void) ShopFlushObjects(libSys);
  (void) ShopFreePSys(pSys);
  (void) ShopInitPSys(libSys);
  std::string_view const dllPath = "libshop.so";
  ShopAddDllPath(libSys, dllPath);
  ShopAddDllPath(libSys, dllPath.data());
  char interfacePath[10] = {}, solverPath[10] = {}, versionInfo[10] = {};
  int allocSize = 0;
  (void) ShopDiagnoseSolver(libSys, interfacePath, solverPath);
  (void) ShopGetVersionInfo(libSys, versionInfo, allocSize);
  (void) ShopAttributeIsDefault(libSys, objectIndex, attributeIndex);
  char yamlString[10] = {};
  (void) ShopDumpYamlString(libSys, yamlString, allocSize, true, true, true, true, true);
  // Callback parameters: Message, Level, CallerId

  std::function<void(std::string, std::string, std::string)> f = nullptr;
  std::string pythonCallerId = "";
  ShopRegisterPythonCallback(libSys, f, pythonCallerId);
  (void) ShopGetNumLicenses(libSys);
  (void) ShopGetMaxLicenseStringSize(libSys);
  char* licenseStrings[10] = {};
  ShopGetLicenses(libSys, licenseStrings);

  int codeNumber = 0, messageNumber = 0, messageTypeCode = 0;
  (void) ShopGetMessageCallCount(libSys, codeNumber);
  (void) ShopGetMessageText(libSys, messageNumber);
  (void) ShopGetMessageType(libSys, messageNumber);
  (void) ShopGetMessageTypeString(libSys, messageNumber);
  (void) ShopMessageTypeToString(messageTypeCode);

  int codes[10] = {}, types[10] = {}, callCounts[10] = {}, code = 0;
  char *textStrings[10] = {}, *messages[10] = {};
  ShopGetAllMessageCodes(libSys, codes);
  ShopGetAllMessageTexts(libSys, textStrings);
  ShopGetAllMessageTypes(libSys, types);
  ShopGetAllMessageCallCounts(libSys, callCounts);
  ShopGetPrintedMessagesForCode(libSys, code, messages);

  (void) ShopGetLongestMessageLen(libSys);
  (void) ShopGetNumMessages(libSys);
  (void) ShopGetMaxMessageCallCount(libSys);
  (void) ShopGetLongestPrintedMessageLen(libSys);
}
