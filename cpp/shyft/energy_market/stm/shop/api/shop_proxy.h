#pragma once
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <map>
#include <memory>
#include <mutex>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/shop/shop_init.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <shop_lib_interface.h>

namespace shop::proxy {

  /** @brief shop proxy object
   *
   * This class represents a shop object by proxy,
   * using a reference to the shop-lib handle
   * @tparam S shop handle, holding any operations that we would like to invoke
   * @tparam T native shop type index for this object
   *
   */
  template <class S, int T>
  struct obj {
    static constexpr int t_id = T; ///< the shop object type id
    obj() = default;

    obj(S* s, int oid)
      : s{s}
      , id{oid} {
    }

    S* s;   ///< upward reference to the shop-sys (proxy for the lib handle)
    int id; ///< the shop object index, as reported when object is created
  };

  /**
   * @brief shop object proxy attribute
   *
   * This base class implements read-only access. Some attributes are strictly
   * read-only, e.g. constants and results, and these are intances of this
   * class only. Other attributes are also writable, e.g. input, and are then
   * instances of subclass rw extending with write capabilities.
   *
   * Keeeping enough references to act as an ordinary proxy for the attribute.
   * allowing syntax like:
   *     reservoir.hrl=123.0
   * or:
   *     if (reservoir.hrl.exists()) ...
   *
   * @tparam O the parent class where the requirement is that p->s has:
   *             .get(oid,aid,V*) -> V
   *             .exists(oid,aid,V*) -> bool
   *             .is_default(oid,aid,V*) -> bool
   * @tparam a the attribute id as used by native shop-interface
   * @tparam V the value-type of the attribute
   * @see rw
   */
  template <class O, int a, class V, class Ux, class Uy>
  struct ro {
    using value_t = V;            ///< the value type, nice to have
    static constexpr int aid = a; ///< the attribute id, as required by the c-api
    using x_unit = Ux;
    using y_unit = Uy;
    O* p; ///< reference to the owning object

    ro(O* p)
      : p(p) {
    }

    bool exists() const {
      return p->s->exists(p->id, aid, (V*) nullptr);
    } /// probably not relevant for read only attribute?

    bool is_default() const {
      return p->s->is_default(p->id, aid, (V*) nullptr);
    } /// probably not relevant for read only attribute?

    V get() const {
      return p->s->template get<Ux, Uy>(p->id, aid, (V*) nullptr);
    } // pass a V* to help compiler select right return type

    operator V() const {
      return get();
    } // candy to allow cast syntax to extract value
  };

  template <class T>
  struct set_policy { };

  template <>
  struct set_policy<shyft::time_series::dd::apoint_ts> {
    bool clip_to_shop_period = true;
  };

  /**
   * @brief shop object proxy attribute that are writable (input)
   *
   * @tparam O the parent class where the requirement is that p->s has:
   *             .get(oid,aid,V*) -> V
   *             .exists(oid,aid,V*) -> bool
   *             .is_default(oid,aid,V*) -> bool
   *             .set(oid,aid,v)
   * @tparam a the attribute id as used by native shop-interface
   * @tparam V the value-type of the attribute
   * @see ro
   */
  template <class O, int a, class V, class Ux, class Uy>
  struct rw : ro<O, a, V, Ux, Uy> {
    rw(O* p)
      : ro<O, a, V, Ux, Uy>(p) {
    }

    void set(V v, set_policy<V> p = {}) {
      this->p->s->template set<Ux, Uy>(this->p->id, this->aid, v, p);
    }

    rw& operator=(V const & v) {
      set(v);
      return *this;
    } // candy to allow syntax r.flow_desc= ... and xy fd=r.flow_descr
  };

} // shop::proxy

/**
 * @brief shop::date namespace
 *
 * contains the c++ bare-bone *SAFE* representation of the c-types used by
 * the shop-interace library.
 *
 */
namespace shop::data {
  // YYYYMMDDhhmmssxxx0
  constexpr std::size_t shop_time_size{18};
  // time unit, currently longest values are "minute" and "second"
  constexpr std::size_t time_unit_sz{7};
  // full command string, guessed size
  constexpr std::size_t max_command_sz{1024};
  // option argument of commands, currently longest value is "overflow_time_adjust"
  constexpr std::size_t max_option_sz{21};
  constexpr std::size_t max_object_sz{260};
  // log entry message, maximum size returned by ShopGetProgress according to sintef
  constexpr std::size_t max_message_size{1024};
  // log entry severity, maximum size returned by ShopGetProgress according to sintef
  constexpr std::size_t max_severity_size{32};
  // value of attributes of type string, maximum theoretical size returned by ShopGetStringAttribute according to sintef
  constexpr std::size_t max_attribute_size{1024};

  struct shop_time_unit {
    static constexpr auto second{"second"};
    static constexpr auto minute{"minute"};
    static constexpr auto hour{"hour"};
    static constexpr auto day{"day"};
    static constexpr auto week{"week"};
    static constexpr auto month{"month"};
    static constexpr auto year{"year"};

    static auto from_time_t(std::time_t s) {
      switch (s) {
      case 3600LL:
        return shop_time_unit::hour;
      case 60LL:
        return shop_time_unit::minute;
      case 1LL:
        return shop_time_unit::second;
      case 3600 * 24LL:
        return shop_time_unit::day;
      case 7 * 3600 * 24LL:
        return shop_time_unit::week;
      case 30 * 3600 * 24LL:
        return shop_time_unit::month;
      case 365 * 3600 * 24LL:
        return shop_time_unit::year;
      }
      throw std::runtime_error(fmt::format("shop::data unsupported time unit{}", int(s)));
    }

    static auto to_time_t(std::string unit) {
      if (unit == second)
        return 1LL;
      if (unit == minute)
        return 60LL;
      if (unit == hour)
        return 3600LL;
      if (unit == day)
        return 3600 * 24LL;
      if (unit == week)
        return 7 * 3600 * 24LL;
      if (unit == month)
        return 30 * 3600 * 24LL;
      if (unit == year)
        return 365 * 3600 * 24LL;
      throw std::runtime_error(fmt::format("shop::data unsupported time unit{}", unit));
    }
  };

  // Unit in number of seconds that relative time integer values in Shop should represent. Must match one of the
  // shop_time_unit constants. Minute is minimum, as of shop.lib version 0.1.1 (2018.12.12) using second leads to
  // crash.
  static constexpr std::time_t shop_time_resolution_unit{60};

  /** shop_time, incredible 17c text representation */
  inline shyft::energy_market::stm::log_entry
    make_log_entry(std::time_t t, std::string_view s, int c, std::string_view m) {

    using shyft::energy_market::stm::log_severity;
    return {
      .severity = [&]() -> log_severity {
        if (s.starts_with("OK") || s.starts_with("Diagnosis OK"))
          return log_severity::information;
        else if (s.starts_with("Warning") || s.starts_with("Diagnosis warning"))
          return log_severity::warning;
        else if (s.starts_with("Error") || s.starts_with("Diagnosis error"))
          return log_severity::error;
        else
          throw std::runtime_error(fmt::format("unexpected severity string: {}", s));
      }(),
      .message = [&]() -> std::string {
        std::string message{
          std::ranges::find_if(
            m,
            [](int ch) {
              return !std::isspace(ch);
            }),
          std::ranges::find_if(std::views::reverse(m), [](int ch) {
            return !std::isspace(ch);
          }).base()};
        if (s.starts_with("Diagnosis"))
          message = fmt::format("Diagnosis: {}", message);
        return message;
      }(),
      .code = c,
      .time = shyft::core::from_seconds(t)};
  }

  struct shop_time { // 012345678901234570
    char rep[data::shop_time_size];

    shop_time(char const * x, bool check_it) {
      if (x == nullptr) {
        rep[0] = '\0';
      } else {
        *fmt::format_to_n(rep, sizeof(rep) - 1, "{}", x).out = '\0';
      }
      if (check_it)
        (void) operator std::time_t(); // force check
    }

    explicit shop_time(std::time_t x) {
      auto cal = shyft::core::calendar();
      auto c = cal.calendar_units(std::chrono::seconds(x));
      *fmt::format_to_n(
         rep,
         sizeof(rep) - 1,
         "{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}000",
         c.year,
         c.month,
         c.day,
         c.hour,
         c.minute,
         c.second)
         .out = '\0';
    }

    explicit operator std::time_t() const;

    operator char*() const {
      return (char*) (&rep[0]);
    }

    bool operator==(shop_time const & o) const {
      return strcmp(rep, o.rep) == 0;
    }

    bool operator!=(shop_time const & o) const {
      return !operator==(o);
    }
  };
}
