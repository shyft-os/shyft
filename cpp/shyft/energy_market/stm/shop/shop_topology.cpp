#include <shyft/energy_market/stm/shop/shop_topology.h>

namespace shyft::energy_market::stm::shop::topology {

  bool match_role_filter(hydro_power::connection_role role, role_filter filter) {
    switch (role) {
    case hydro_power::connection_role::main:
      return (std::uint8_t) filter & (std::uint8_t) role_filter::main;
    case hydro_power::connection_role::bypass:
      return (std::uint8_t) filter & (std::uint8_t) role_filter::bypass;
    case hydro_power::connection_role::flood:
      return (std::uint8_t) filter & (std::uint8_t) role_filter::flood;
    case hydro_power::connection_role::input:
      return (std::uint8_t) filter & (std::uint8_t) role_filter::input;
    }
    throw std::runtime_error("Unexpected connection role");
  }

  std::vector<hydro_power::hydro_connection> const &
    connections(hydro_power::hydro_component const & from, direction direction) {
    switch (direction) {
    case direction::upstream:
      return from.upstreams;
    case direction::downstream:
      return from.downstreams;
    }
    throw std::runtime_error("Unexpected topology direction");
  }

  std::shared_ptr<hydro_power::hydro_component>
    first_connected(hydro_power::hydro_component const & from, direction direction, role_filter roles) {
    for (auto const & connection : connections(from, direction))
      if (match_role_filter(connection.role, roles))
        return connection.target;
    return nullptr;
  }

  bool is_connected(
    hydro_power::hydro_component const & from,
    hydro_power::hydro_component const & to,
    direction direction,
    role_filter roles,
    int levels) {
    // Returns true if there is a topological connection between specified
    // components, in specified direction. Traversal through waterways can
    // be controlled by argument levels: Will always first check target of
    // connections, and then if any of them are waterways then may traverse
    // them if specified level is at least 1 and then check the waterway's
    // connection targets. Level 0 means no traversing of waterways,
    // <0 means no limit.
    for (auto const & connection : connections(from, direction)) {
      if (match_role_filter(connection.role, roles)) {
        if (auto const & other = connection.target) {
          if (*other == to)
            return true;
          if (levels != 0 && dynamic_cast<waterway*>(other.get())) {
            if (is_connected(*other, to, direction, roles, levels - 1))
              return true;
          }
        }
      }
    }
    return false;
  }

  std::shared_ptr<hydro_power::hydro_component> traverse(
    hydro_power::hydro_component const & from,
    direction direction,
    role_filter roles,
    int levels,
    std::vector<hydro_power::hydro_connection>* result_backtrack) {
    // Return first component connected with a role matching the role filter,
    // optionally traversing waterways, optionally in a limited number of levels.
    // Searches breadth first: Returning result with shortest possible path.
    // Optionally returning backtrack of the traversed path.
    auto const passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
    for (int pass = 0; pass < passes; ++pass) {
      for (auto const & connection : connections(from, direction)) {
        if (match_role_filter(connection.role, roles)) {
          if (auto const & to = connection.target) {
            if (pass == 0) {
              if (!dynamic_cast<waterway*>(to.get())) {
                if (result_backtrack)
                  result_backtrack->push_back(connection);
                return to;
              }
            } else if (auto const & found = traverse(*to, direction, roles, levels - 1, result_backtrack)) {
              if (result_backtrack)
                result_backtrack->push_back(connection);
              return found;
            }
          }
        }
      }
    }
    return nullptr;
  }

  waterway* get_tailrace(power_plant const & pl) {
    auto it = std::cbegin(pl.units);
    auto const it_end = std::cend(pl.units);
    if (it == it_end)
      return nullptr;
    auto ag_first = dynamic_cast<unit*>(it->get());
    if (!ag_first)
      return nullptr;
    auto ag_first_outlet = dynamic_cast<waterway*>(
      topology::first_connected(*ag_first, topology::direction::downstream).get());
    if (!ag_first_outlet)
      return nullptr;
    if (std::size(pl.units) == 1)
      return ag_first_outlet;
    // Check if all other units in plant are upstream of the outlet tunnel of the first unit
    ++it;
    if (std::all_of(it, it_end, [&ag_first_outlet](auto const & i) {
          return i ? topology::is_connected(
                   *i, *ag_first_outlet, topology::direction::downstream, topology::role_filter::all, 1)
                   : false;
        }))
      return ag_first_outlet;
    auto ag_second_outlet = dynamic_cast<waterway*>(
      topology::first_connected(*ag_first_outlet, topology::direction::downstream).get());
    if (!ag_second_outlet)
      return nullptr;
    if (std::all_of(it, it_end, [&ag_second_outlet](auto const & i) {
          return i ? topology::is_connected(
                   *i, *ag_second_outlet, topology::direction::downstream, topology::role_filter::all, 1)
                   : false;
        }))
      return ag_second_outlet;
    return nullptr;
  }

  waterway* get_penstock(unit const & agg, bool always_inlet) {
    if (always_inlet) {
      if (auto const & inlet = dynamic_cast<waterway*>(agg.upstream().get())) {
        if (
          auto penstock =
            inlet->upstreams.empty() ? nullptr : dynamic_cast<waterway*>(inlet->upstreams.front().target.get())) {
          return penstock;
        }
      }
      return nullptr;
    }
    if (auto first = dynamic_cast<waterway*>(agg.upstream().get())) {
      if (
        auto second =
          first->upstreams.empty() ? nullptr : dynamic_cast<waterway*>(first->upstreams.front().target.get())) {
        std::size_t siblings = 0; // Count sibling segments of first
        for (auto const & con : second->downstreams) {
          if (auto sibling_of_first = dynamic_cast<waterway*>(con.target.get())) {
            if (sibling_of_first != first) {
              ++siblings;
              for (auto const & con : sibling_of_first->downstreams) {
                if (auto const & target = con.target) {
                  if (dynamic_cast<waterway*>(target.get())) {
                    // The segment sibling to the first upstream from the aggregate leads into a deeper structure,
                    // is penstock
                    return first;
                  } else if (dynamic_cast<unit*>(target.get())) {
                    // Sibling leads into aggregate, possibly inlets from a shared penstock, or there are separate
                    // penstocks without inlets. See if there are branches one level above. If so, our current branch
                    // is  a shared penstock
                    if (
                      auto const & third = second->upstreams.empty()
                                           ? nullptr
                                           : dynamic_cast<waterway*>(second->upstreams.front().target.get())) {
                      for (auto const & con : third->downstreams) {
                        if (auto const & sibling_of_second = dynamic_cast<waterway*>(con.target.get())) {
                          if (sibling_of_second != second) {
                            return second;
                          }
                        }
                      }
                    }
                    return first;
                  } else
                    throw std::runtime_error(
                      fmt::format("Unexpected element in plant topology: {} [{}]", target->name, target->id));
                }
              }
            }
          }
        }
        if (siblings < 2) {
          // There were no siblings of the first segment, it is a penstock
          return first;
        }
      }
    }
    return nullptr;
  }

  bool is_plant_tailrace(waterway const & wtr) {
    if (
      auto const & ag = topology::traverse_find_type<unit>(
        wtr, topology::direction::upstream, topology::role_filter::all, 1)) {
      if (auto const & plant = dynamic_cast<power_plant*>(ag->pwr_station_().get())) {
        if (auto tailrace = get_tailrace(*plant)) {
          return *tailrace == wtr;
        }
      }
    }
    return false;
  }

  bool is_plant_outlet(waterway const & wtr) {
    if (
      auto const & ag = topology::traverse_find_type<unit>(
        wtr, topology::direction::upstream, topology::role_filter::all, 1)) {
      if (auto const & plant = dynamic_cast<power_plant*>(ag->pwr_station_().get())) {
        if (auto const & tailrace = get_tailrace(*plant)) {
          if (*tailrace == wtr)
            return true;
          return is_connected(wtr, *tailrace, topology::direction::downstream, topology::role_filter::all, 0);
        }
      }
    }
    return false;
  }
}
