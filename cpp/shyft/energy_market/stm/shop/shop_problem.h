#pragma once

#include <memory>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::shop {

  struct problem {

    time_axis::generic_dt time_axis;
    std::vector<shop::shop_command> commands;
    std::shared_ptr<stm::stm_system> system;

    SHYFT_DEFINE_STRUCT(problem, (), (time_axis, commands, system));
    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::energy_market::stm::shop::problem);