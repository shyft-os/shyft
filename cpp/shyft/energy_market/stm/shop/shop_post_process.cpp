#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_post_process.h>

namespace shyft::energy_market::stm::shop {

  namespace detail {
    // Check for any values, not considering nan or infinite.
    static bool has_values(time_series::dd::apoint_ts const & ts) {
      // False if time series object is empty or describes no points.
      if (ts.size() == 0)
        return false;
      // True if time series has any non-nan values.
      auto const values = ts.values(); // Note: Throws exception if unbound, check needs_bind() first to avoid it.
      return std::ranges::any_of(values, [](double const & v) {
        return std::isfinite(v);
      });
    };

    auto values_or_empty(time_series::dd::apoint_ts const & ts) {
      // Copy if it is a bound non-empty time series containing any non-nan values.
      if (!ts.needs_bind() && has_values(ts))
        return apoint_ts(ts.time_axis(), ts.values(), ts.point_interpretation());
      return apoint_ts{};
    }

    /**
     * Assigns flow if missing, returns if any change was performed
     */
    auto try_assign_gate_discharge(stm::waterway& w) {
      if (w.discharge.result.needs_bind() || has_values(w.discharge.result))
        return false;
      if (w.gates.empty())
        return false;
      apoint_ts computed_discharge;
      std::ranges::for_each(
        std::views::transform(
          w.gates,
          [&](auto& g) {
            auto* gg = dynamic_cast<stm::gate*>(g.get());
            if (!gg)
              return apoint_ts{};
            return values_or_empty(gg->discharge.result);
          }),
        [&](auto const & ts) {
          if (!ts.empty())
            computed_discharge = computed_discharge.ts ? computed_discharge + ts : ts;
        });
      if (!computed_discharge.empty()) {
        w.discharge.result = computed_discharge.evaluate();
        return true;
      }
      return false;
    }

    /**
     * @brief make waterway discharge complete
     * @details
     * Shop only fills in what is given to it, and barely that, so
     * we need to iterate over the hps, and compute the remaining flows
     * to make it easier for users to interpret the results.
     * There are some issues outstanding here, like time-delay
     * (when flow into a waterway is not instantly the same as goes out).
     */
    void compute_missing_waterway_discharges(stm_system& stm) {

      auto get_discharge = [](auto* o) {
        if (auto* x = dynamic_cast<stm::unit*>(o))
          return values_or_empty(x->discharge.result);
        if (auto* x = dynamic_cast<stm::waterway*>(o))
          return values_or_empty(x->discharge.result);
        return apoint_ts{};
      };
      for (auto const & hps : stm.hps) {
        // first, sum gates:
        std::ranges::for_each(hps->waterways, [](auto& w) {
          try_assign_gate_discharge(dynamic_cast<stm::waterway&>(*w.get()));
        });

        for (std::size_t r = 0; r < 5; ++r) { // 5 iterations: 1st, will fill in all 1st order simple cases, 2nd, and
                                              // 3rd,etc. will do more simple plus complex
          std::size_t n_wtr_discharge_missing{0}; // n wtr without discharge
          for (auto const & wx : hps->waterways) {
            auto& w = dynamic_cast<stm::waterway&>(*wx.get());
            // already resolved:
            if (w.discharge.result.needs_bind() || has_values(w.discharge.result))
              continue;
            ++n_wtr_discharge_missing;
            apoint_ts computed_discharge;
            // Check upstreams.
            // If single connect upstream.downstream, and has flow, take the flow from that (a waterway, or unit).
            if (w.upstreams.size() == 1) { // one single upstream connect
              auto us = w.upstreams.front().target;
              if (us && us->downstreams.size() == 1) { // single connect: upstream only feed to this
                computed_discharge = get_discharge(us.get());
                if (!computed_discharge.empty()) {
                  w.discharge.result = computed_discharge;
                  --n_wtr_discharge_missing;
                  continue;
                }
              }
            }

            // Check downstream.
            // If single connect downstream, take the flow from that (a waterway, or unit).
            if (w.downstreams.size() == 1) {
              auto ds = w.downstreams.front().target;
              if (ds && ds->upstreams.size() == 1) { // single connect: only feed to this
                computed_discharge = get_discharge(ds.get());
                if (!computed_discharge.empty()) {
                  w.discharge.result = computed_discharge;
                  --n_wtr_discharge_missing;
                  continue;
                }
              }
            }

            // Check if all (multiple>1) upstreams flows to this, and sum together.
            // Needed to cover topology cases:
            //  (a) R1..Rn .. flow to a junction, then to multiple units
            //  (b) U1..Un .. tailrace to common downstream river?
            for (auto const & hc : w.upstreams) {
              if (auto us = hc.target) {
                if (us->downstreams.size() == 1) {
                  auto ts = get_discharge(us.get());
                  if (ts.size() == 0) {               // this upstream is missing discharge
                    computed_discharge = apoint_ts{}; // don't use this sum, wait for all upstreams to be computed,
                                                      // then revisit later for a complete sum
                    break;
                  }
                  computed_discharge = computed_discharge.ts ? computed_discharge + ts : ts;
                } else {                            // this upstream is a split
                  computed_discharge = apoint_ts{}; // don't compute this one
                  break;
                }
              } // ignore null ptrs
            }
            if (!computed_discharge.empty()) {
              w.discharge.result = computed_discharge.evaluate();
              --n_wtr_discharge_missing;
            }
          }
          if (n_wtr_discharge_missing == 0)
            break;
        }
      }
    }
  }

  void post_process(stm_system& stm) {
    // Unit group sum expressions
    std::ranges::for_each(stm.unit_groups, [](auto const & group) {
      group->update_sum_expressions();
    });
    // Waterway discharges
    detail::compute_missing_waterway_discharges(stm);
  }

}