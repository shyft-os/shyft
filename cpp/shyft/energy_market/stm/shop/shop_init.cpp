#include <mutex>
#include <string>
#include <string_view>

#include <fmt/core.h>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/stm/shop/shop_bundle_path.h>
#include <shyft/energy_market/stm/shop/shop_init.h>

namespace shyft::energy_market::stm::shop {

  [[nodiscard]] ShopSystem* shop_init(std::string_view license_path_hint) {
    std::string effective_license_path{license_path_hint.empty() ? SHYFT_SHOP_LICENSEDIR : license_path_hint};
    if (std::getenv("SHYFT_SHOP_BUNDLE_DIR"))
      effective_license_path = std::getenv("SHYFT_SHOP_BUNDLE_DIR");
    else if (std::getenv("ICC_COMMAND_PATH"))
      effective_license_path = std::getenv("ICC_COMMAND_PATH");
    // verify path,
    auto license_file = fs::path(effective_license_path) / "SHOP_license.dat";
    if (!fs::exists(license_file) || !fs::is_regular_file(license_file))
      throw std::runtime_error(fmt::format("Failed to find license file at location {}", license_file.string()));

    static std::mutex mutex{};
    std::scoped_lock lock{mutex};
    return ShopInit(true, true, effective_license_path);
  }

  void ShopSystemDeleter::operator()(ShopSystem* s) const {
    (void) ShopFree(s);
  }
}
