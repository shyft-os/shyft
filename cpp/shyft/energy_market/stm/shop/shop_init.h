/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string_view>

#include <shop_lib_interface.h>

namespace shyft::energy_market::stm::shop {
  /**
   * @brief perform clever shop_init with license_path_hint
   * @details
   * Uses the passed in path, as path hint for licences.
   * If empty the path hint is created from the cmake generated SHYFT_SHOP_LICENSEDIR.
   * If the environment variable SHYFT_SHOP_BUNDLE_DIR is set it is used as override.
   * For bw compat it none of the above applies, the environment variable
   * ICC_COMMAND_PATH.
   *
   * @param license_path_hint
   * @return Shop system handle (opaque type), same as ShopInit(..) return
   */
  [[nodiscard]] ShopSystem* shop_init(std::string_view license_path_hint);

  struct ShopSystemDeleter {
    void operator()(ShopSystem* s) const;
  };
}