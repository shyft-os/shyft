#include <vector>

#include <boost/hof/lift.hpp>

#include <shyft/energy_market/stm/shop/shop_api.h>

namespace shyft::energy_market::stm::shop {
  api::api(std::string_view license_path)
    : c(shop_init(license_path)) {
  }

  std::string api::get_version_info() const {
    int len = 0;
    if (!ShopGetVersionInfo(c.get(), nullptr, len))
      return {};
    std::string res(len, '0');
    if (!ShopGetVersionInfo(c.get(), res.data(), len))
      return {};
    return res;
  }

  void api::set_library_path(std::string_view path) {
    ShopAddDllPath(c.get(), std::string(path).c_str());
  }

  void api::set_logging_to_stdstreams(bool on) {
    ShopSetSilentConsole(c.get(), !on);
  }

  void api::set_logging_to_files(bool on) {
    ShopSetSilentLog(c.get(), !on);
  }

  std::vector<stm::log_entry> api::get_log_buffer(std::size_t limit) {
    limit = std::clamp(limit, std::size_t{1}, std::size_t{1024});
    std::vector<int> percents(limit), timestamps(limit), codes(limit);
    std::vector<std::string> messages(limit, std::string(::shop::data::max_message_size, '0')),
      severities(limit, std::string(::shop::data::max_severity_size, '0'));
    std::vector<char *> v_messages(limit), v_severities(limit);
    {
      auto target = std::views::zip(v_messages, v_severities);
      std::ranges::transform(
        std::views::zip(messages, severities),
        std::ranges::begin(target),
        boost::hof::unpack([](auto &&message, auto &&severity) {
          return std::make_pair(message.data(), severity.data());
        }));
    }
    int n = 0;
    if (
      !ShopGetProgress(
        c.get(), n, v_messages.data(), percents.data(), timestamps.data(), v_severities.data(), codes.data(), limit)) {
      return {};
    }
    std::vector<stm::log_entry> r(n);
    std::ranges::for_each(messages, [](auto &r) {
      if (auto i = r.find_first_of('\0'); i != std::string::npos)
        r.resize(i);
    });
    auto from = std::views::take(std::views::zip(timestamps, severities, codes, messages), n);
    std::ranges::transform(from, std::ranges::begin(r), boost::hof::unpack(&::shop::data::make_log_entry));
    return r;
  }

  void api::set_time_axis(std::time_t t_begin, std::time_t t_end, std::time_t t_step) {
    if (t_begin >= t_end)
      throw std::runtime_error("set_time_axis called with t_begin >= t_end");
    if (t_step <= 0)
      throw std::runtime_error("set_time_axis called with t_step <= 0");
    auto n_steps{(t_end - t_begin) / t_step};
    if (n_steps <= 0)
      throw std::runtime_error("set_time_axis called with n_steps <= 0");
    std::vector<std::time_t> t_axis(n_steps + 1);
    std::ranges::transform(std::views::iota(0, n_steps + 1), t_axis.data(), [&](auto const i) {
      return t_begin + t_step * i;
    });
    set_time_axis(t_axis);
  }

  void api::set_time_axis(std::vector<std::time_t> const &t_axis) {
    if (t_axis.size() < 2)
      throw std::runtime_error(fmt::format("set_time_axis called with {} point(s), must be at least 2", t_axis.size()));
    auto ti{t_axis.cbegin()};
    std::time_t t{*ti++};
    std::time_t dt{*ti - t};
    if (dt <= 0)
      throw std::runtime_error("set_time_axis called with t_axis that is not increasing");
    auto const t_first{t};
    auto const ti_last{t_axis.cend() - 1};
    std::vector<int> steps_t{0};
    std::vector<double> steps_l{static_cast<double>(dt / ::shop::data::shop_time_resolution_unit)};
    std::time_t prev_dt{dt};
    while (ti < ti_last) {
      t = *ti++;
      dt = *ti - t;
      if (dt != prev_dt) {
        if (dt <= 0)
          throw std::runtime_error("set_time_axis called with t_axis that is not increasing");
        steps_t.push_back((int) ((t - t_first) / ::shop::data::shop_time_resolution_unit));
        steps_l.push_back((double) (dt / ::shop::data::shop_time_resolution_unit));
        prev_dt = dt;
      }
    }
    ::shop::data::shop_time start_time{t_first};
    ::shop::data::shop_time end_time{*ti_last};
    if (!ShopSetTimeResolution(
          c.get(),
          start_time,
          end_time,
          ::shop::data::shop_time_unit::from_time_t(::shop::data::shop_time_resolution_unit),
          (int) steps_t.size(),
          const_cast<int *>(steps_t.data()),
          const_cast<double *>(steps_l.data())))
      throw std::runtime_error("failed to set time resolution");
    time_axis = t_axis;
    time_axis_defined = true;
  }

  void api::execute_cmd(std::string_view cmd, std::string_view opt, std::string_view obj) {
    if (opt.size() > ::shop::data::max_object_sz)
      throw std::runtime_error(fmt::format(
        "shop-api: size of object {} ({}) is longer than allowed max limit {}",
        opt,
        opt.size(),
        ::shop::data::max_object_sz));
    if (obj.size() > ::shop::data::max_option_sz)
      throw std::runtime_error(fmt::format(
        "shop-api: size of option {} ({}) is longer than allowed max limit {}",
        obj,
        obj.size(),
        ::shop::data::max_option_sz));
    std::string opt_str = std::string{opt};
    int n_opt = !opt.empty();
    std::vector<char const *> opt_l{opt_str.c_str()};
    std::string obj_str = std::string{obj};
    int n_obj = !obj.empty();
    std::vector<char const *> obj_l{obj_str.c_str()};
    if (!ShopExecuteCommand(c.get(), std::string(cmd).c_str(), n_opt, opt_l.data(), n_obj, obj_l.data()))
      throw std::runtime_error(fmt::format(
        "failed to execute shop command: {} {} {}",
        cmd,
        (opt.empty() ? "" : fmt::format("/{}", opt)),
        (obj.empty() ? "" : fmt::format(" {}", std::string(obj)))));
  }

  void api::execute_cmd(std::string_view cmd, std::vector<std::string_view> opt, std::vector<std::string_view> obj) {
    auto n_opt = std::ranges::size(opt);
    std::vector<std::string> opts(n_opt);
    std::ranges::transform(opt, opts.data(), [](auto &o) {
      if (o.size() > ::shop::data::max_option_sz)
        throw std::runtime_error(fmt::format(
          "shop-api: size of option {} ({}) is longer than allowed max limit {}",
          o,
          o.size(),
          ::shop::data::max_option_sz));
      return std::string(o);
    });
    std::vector<char const *> v_opts(n_opt);
    std::ranges::transform(opts, v_opts.data(), [](auto const &o) {
      return o.data();
    });
    auto n_obj = std::ranges::size(obj);
    std::vector<std::string> objs(n_obj);
    std::ranges::transform(obj, objs.data(), [](auto &o) {
      if (o.size() > ::shop::data::max_object_sz)
        throw std::runtime_error(fmt::format(
          "shop-api: size of object {} ({}) is longer than allowed max limit {}",
          o,
          o.size(),
          ::shop::data::max_object_sz));
      return std::string(o);
    });
    std::vector<char const *> v_objs(n_obj);
    // FIXME: figure out how to use BOOST_HOF_LIFT to use the member ptr here
    char const *(std::string::*cstr)() const = &std::string::c_str;
    std::ranges::transform(objs, v_objs.data(), cstr);
    if (!ShopExecuteCommand(c.get(), std::string(cmd).c_str(), n_opt, v_opts.data(), n_obj, v_objs.data())) {
      throw std::runtime_error(fmt::format(
        "failed to execute shop command: {}{}",
        fmt::join(
          std::views::transform(
            opt,
            [](auto const &opt) {
              return fmt::format(" /{}", opt);
            }),
          ""),
        fmt::join(
          std::views::transform(
            obj,
            [](auto const &obj) {
              return fmt::format(" {}", obj);
            }),
          "")));
    }
  }

  void api::execute_cmd_string(std::string_view cmd) {
    if (!ShopExecuteCommand(c.get(), std::string(cmd).c_str()))
      throw std::runtime_error(fmt::format("failed to execute shop command: {}", cmd));
  }

  std::vector<std::string> api::get_executed_cmd_strings() const {
    std::vector<std::string> r;
    if (!ShopGetExecutedCommands(c.get(), r))
      throw std::runtime_error("failed to get shop commands");
    return r;
  }

  void api::start_sim(std::string_view o) {
    if (o.size() >= ::shop::data::max_object_sz)
      throw std::runtime_error(fmt::format(
        "start_sim called with too long argument: {} (), max size {}", o, o.size(), ::shop::data::max_object_sz));
    std::string o_{o};
    std::vector<char const *> o_v{o_.c_str()};
    std::string_view cmd{"start sim"};
    if (!ShopExecuteCommand(c.get(), cmd.data(), 0, nullptr, 1, o_v.data()))
      throw std::runtime_error(fmt::format("failed to execute shop command: {} {}", cmd, o));
  }

  std::string
    api::dump_yaml(bool input_only, bool output_only, bool compress_txy, bool compress_connection, bool escape_ascii)
      const {
    int n = 0;
    if (!ShopDumpYamlString(
          c.get(), nullptr, n, input_only, output_only, compress_txy, compress_connection, escape_ascii))
      throw std::runtime_error("failed to get yaml string allocation size");
    if (n < 1)
      return {};
    std::string r(n, '\0');
    if (!ShopDumpYamlString(
          c.get(), r.data(), n, input_only, output_only, compress_txy, compress_connection, escape_ascii))
      throw std::runtime_error("failed to create yaml string");
    r.resize(n - 1);
    return r;
  }

  std::string api::obj_ref_str(int oid) const {
    // SHOP API calls cause segfault if we ask for indices out of range.
    if (oid >= ShopGetObjectCount(c.get()))
      return fmt::format("'INVALID ATTRIBUTE ID[{}, NO OBJECT TYPE]'", oid);
    return fmt::format("'{}[{}, {}]'", ShopGetObjectType(c.get(), oid), oid, ShopGetObjectName(c.get(), oid));
  }

  std::string api::attr_ref_str(int oid, int aid) const {
    // SHOP API calls cause segfault if we ask for indices out of range.
    std::optional<std::string> attr_name;
    if (aid < ShopGetAttributeCount(c.get()))
      attr_name = ShopGetAttributeName(c.get(), aid);
    std::optional<std::string> obj_name;
    std::optional<std::string> obj_type;
    if (oid < ShopGetObjectCount(c.get())) {
      obj_name = ShopGetObjectName(c.get(), oid);
      obj_type = ShopGetObjectType(c.get(), oid);
    }
    return fmt::format(
      "'{}[{}, {}].({}, {})'",
      (obj_type ? *obj_type : "INVALID OBJECT TYPE"),
      oid,
      (obj_name ? *obj_name : "INVALID OBJECT ID"),
      aid,
      (attr_name ? *attr_name : "NO OBJECT TYPE"));
  }

  int api::create_object(int otype, std::string_view name) {
    auto ix = ShopAddObject(c.get(), otype, std::string(name).c_str());
    if (ix < 0)
      throw std::runtime_error(fmt::format("failed to create object of type {} (name: {})", otype, name));
    return ix;
  }

  int api::get_object(int otype, std::string_view name) const {
    auto type_name = ShopGetObjectTypeName(c.get(), otype);
    auto ix = ShopGetObjectIndex(c.get(), type_name, std::string(name).c_str());
    if (ix < 0)
      throw std::runtime_error(fmt::format("failed to get object of type {} (name: {})", otype, name));
    return ix;
  }

}