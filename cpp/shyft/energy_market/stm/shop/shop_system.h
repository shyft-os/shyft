/*
 * Integration of stm with shop api via proxy.
 */
#pragma once

#include <iosfwd>
#include <memory>
#include <vector>

#include <shyft/energy_market/stm/shop/export/shop_export.h>
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_api.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_commander.h>
#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <shyft/energy_market/stm/shop/shop_post_process.h>

namespace shyft::energy_market::stm::shop {

  using core::to_seconds64;

  /** @brief shop_system allows optimization  of a stm_system
   *  @details
   *  The class is responsible for facilitating and keeping the
   *  correlated state between the descriptive stm system model
   *  and the corresponding optimization shop-model.
   *  It delegates the work to shop_emitter, shop_adapter and shop_commander.
   *
   *
   */
  struct shop_system {
    api api_handle;       ///< Shop api proxy object, could be private, but is set public for testing.
    shop_adapter adapter; ///< Basic stm to shop object adapter, takes care of 1-1 emitting tasks helper for emitter.
    shop_emitter emitter; ///< The shop exchange interface, that is used to orchestrate shop, notice that emitter have
                          ///< refs to the above api and emitter.
    shop_commander commander; ///< Shop is a stateful live system and commander helps us interact with the instance.

    time_axis::generic_dt time_axis; ///< Optimization time-axis.

    shop_system(time_axis::generic_dt const & time_axis);
    ~shop_system() = default;

    std::string get_version_info() {
      return api_handle.get_version_info();
    }

    void set_logging_to_stdstreams(bool on = true) {
      api_handle.set_logging_to_stdstreams(on);
    }

    void set_logging_to_files(bool on = true) {
      api_handle.set_logging_to_files(on);
    }

    std::vector<log_entry> get_log_buffer(std::size_t limit = 1024) {
      return api_handle.get_log_buffer(limit);
    }

    void emit(stm_system const & stm); ///< Emit stm system to shop, typical step 1 of an optimization.
    void collect(stm_system& stm);  ///< Collect results from shop into stm system, typical step 3 of an optimization.
    void complete(stm_system& stm); ///< Update stm system, performing post-process calculations based on collected
                                    ///< results from shop, typical step 4 of an optimization.

    // Get some default/singleton shop objects not completely represented
    // in stm but may be relevant (temporary solution, to be refactored?)
    // Get known default objects.
    // There are a few objects that are always present in Shop:
    //   - Object "S1" (0) of type "scenario" (19) - default object, additional objects may
    //     be added, only relevant when running the stochastic version of SHOP (SHARM).
    //   - Object "average_objective" (1) of type "objective" (20) - default object, one
    //     additional object will automatically be created for each created scenario object
    //     when running the stochastic version of SHOP (SHARM).
    //   - Object "global_settings" (2) of type global_settings (13) - singleton object.
    //   - Object "lp_model" (3) of type "lp_model" (29) - singleton object?
    //   - Object "system" (4) of type "system" (24) - singleton object?
    // Below are dedicated getters for the relevant ones. Lookup up the object by name,
    // instead of relying on the current object index as noted above, as that is more
    // subject to change.
    shop_global_settings get_global_settings() const;
    shop_objective get_average_objective() const;

    // Export the entire system as yaml formatted string (using function from shop api)
    std::string export_yaml(
      bool input_only = false,
      bool output_only = false,
      bool compress_txy = false,
      bool compress_connection = false) {
      return api_handle.dump_yaml(input_only, output_only, compress_txy, compress_connection);
    }

    // Export topology graph as dot formatted string into stream (custom implementation)
    void export_topology(bool all, bool raw, std::ostream& destination) const;
    // Export data as json formatted string into stream (custom implementation)
    void export_data(bool all, std::ostream& destination) const;

    // Low level utility functions
    void command_raw(
      std::string command) { // Note: Send commands using the newer api function to supply entire command string
                             // directly, instead of using our own ShopCommand/ShopCommander, but note that this lacks
                             // support for quoted arguments and therefore also e.g. slash and space in object
                             // arguments, and it bypasses some features implemented in shop_system::command.
      api_handle.execute_cmd_string(command);
    }

    std::vector<std::string>
      get_executed_commands_raw() { // Note: Using a newer api feature to return raw command strings directly from the
                                    // api, which will not parse them into our own shop_command, but note that this does
                                    // not wrap quotes around oobject arguments containing spaces etc and will therefore
                                    // not necessarily show a valid command string accepted by command_raw.
      return api_handle.get_executed_cmd_strings();
    }

    static void optimize(
      stm_system& stm,
      generic_dt const & time_axis,
      std::vector<shop_command> const & commands,
      bool logging_to_stdstreams = false,
      bool logging_to_files = false);

    static void environment(std::ostream& destination);
  };

}
