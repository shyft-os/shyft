#pragma once
#include <optional>
#include <string_view>

#include <shyft/energy_market/stm/unit_group_type.h>

namespace shyft::energy_market::stm::shop {
  /**
   * Translations of stm structs to shop api strings
   */

  inline constexpr auto shop_market_type = [] {
    /**
     * list of string_views to market_type in SHOP,
     * see https://docs.shop.sintef.energy/objects/market/market.html#market-market-type
     * Unrepresentable types are emtpy string_views
     */
    std::array<std::string_view, enumerator_count<unit_group_type>> a;
    a[etoi(unit_group_type::fcr_n_up)] = "FCR_N_UP";
    a[etoi(unit_group_type::fcr_n_down)] = "FCR_N_DOWN";
    a[etoi(unit_group_type::fcr_d_up)] = "FCR_D_UP";
    a[etoi(unit_group_type::fcr_d_down)] = "FCR_D_DOWN";
    a[etoi(unit_group_type::afrr_up)] = "FRR_UP";
    a[etoi(unit_group_type::afrr_down)] = "FRR_DOWN";
    a[etoi(unit_group_type::mfrr_up)] = "RR_UP";
    a[etoi(unit_group_type::mfrr_down)] = "RR_DOWN";
    a[etoi(unit_group_type::rr_up)] = "RR_UP";
    a[etoi(unit_group_type::rr_down)] = "RR_DOWN";
    a[etoi(unit_group_type::production)] = "ENERGY";
    return a;
  }();

  static_assert(
    std::ranges::all_of(
      market_unit_group_types,
      [](auto const & t) {
        return !shop_market_type[etoi(t)].empty();
      }),
    "Every type in market_unit_group_types needs to translate into a non empty SHOP market type");

  constexpr std::string_view get_shop_market_type(unit_group_type const t) {
    return shop_market_type[etoi(t)];
  }

}