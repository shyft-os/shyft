#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>

namespace shyft::energy_market::stm::shop::topology {

  enum class direction : std::uint8_t {
    upstream,
    downstream
  };

  enum class role_filter : std::uint8_t { // See also shyft::energy_market::hydro_power::connection_role
    main = 1u << 1,                       // Output main waterway, usually a production waterway
    bypass = 1u << 2, // Output bypass, in the meaning of bypass relative a hydro-aggregate/or production unit
    flood = 1u << 3,  // Output flood/spill, water escaping a reservoir uncontrolled, at high water-levels
    input = 1u << 4,  // Input
    all = 0xFF        // All, convenience for bitwise combination of all values
  };

  bool match_role_filter(hydro_power::connection_role role, role_filter filter);
  std::vector<hydro_power::hydro_connection> const &
    connections(hydro_power::hydro_component const & from, direction direction);

  std::shared_ptr<hydro_power::hydro_component> first_connected(
    hydro_power::hydro_component const & from,
    direction direction,
    role_filter roles = role_filter::all);

  bool is_connected(
    hydro_power::hydro_component const & from,
    hydro_power::hydro_component const & to,
    direction direction,
    role_filter roles = role_filter::all,
    int levels = -1);

  std::shared_ptr<hydro_power::hydro_component> traverse(
    hydro_power::hydro_component const & from,
    direction direction,
    role_filter roles,
    int levels = -1,
    std::vector<hydro_power::hydro_connection>* result_backtrack = nullptr);

  template <class T>
  static T* traverse_find_type(
    hydro_power::hydro_component const & from,
    direction direction,
    role_filter roles = role_filter::all,
    int levels = -1) {
    // Variant of traverse with additional criteria of only stopping on a specific
    // type of component - assuming the type to search for is not a waterway.
    auto const passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
    for (int pass = 0; pass < passes; ++pass) {
      for (auto const & connection : connections(from, direction)) {
        if (match_role_filter(connection.role, roles)) {
          if (auto const & to = connection.target) {
            if (pass == 0) {
              if (auto found = dynamic_cast<T*>(to.get()))
                return found;
            } else if (dynamic_cast<waterway*>(to.get())) {
              if (auto found = traverse_find_type<T>(*to, direction, roles, levels - 1))
                return found;
            }
          }
        }
      }
    }
    return nullptr;
  }

  template <class F>
  static std::shared_ptr<hydro_power::hydro_component> traverse_find(
    F fx,
    hydro_power::hydro_component const & from,
    direction direction,
    role_filter roles = role_filter::all,
    int levels = -1) {
    // Variant of traverse with custom predicate for matching.
    auto const passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
    for (int pass = 0; pass < passes; ++pass) {
      for (auto const & connection : connections(from, direction)) {
        if (match_role_filter(connection.role, roles)) {
          if (auto const & to = connection.target) {
            if (pass == 0) {
              if (fx(*to))
                return to;
            } else if (dynamic_cast<waterway*>(to.get())) {
              if (auto const & found = traverse_find(fx, *to, direction, roles, levels - 1))
                return found;
            }
          }
        }
      }
    }
    return nullptr;
  }

  template <class F, class G>
  static void traverse_for_each(
    F f_process_component,
    G f_consider_traverse,
    hydro_power::hydro_component const & from,
    direction direction,
    role_filter roles = role_filter::all,
    int levels = -1) {
    // Variant of traverse which calls callables:
    //   - Callable f_process_component is called on connected components that are not waterways.
    //   - Callable f_consider_traverse is called on connected waterways, when considering them
    //     for traversal, must return value true if traversal should continue
    //     through it, false if not. Note that on last level of limited traversal, or when
    //     no traversal, i.e. level has value 0, then this callable will not be called on
    //     any of the current component's connected waterways.
    auto const passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
    for (int pass = 0; pass < passes; ++pass) {
      for (auto const & connection : connections(from, direction)) {
        if (match_role_filter(connection.role, roles)) {
          if (auto const & to = connection.target) {
            if (pass == 0) {
              if (!dynamic_cast<waterway*>(to.get()))
                f_process_component(to);
            } else if (auto wtr = dynamic_cast<waterway*>(to.get())) {
              if (f_consider_traverse(wtr))
                traverse_for_each(f_process_component, f_consider_traverse, *to, direction, roles, levels - 1);
            }
          }
        }
      }
    }
  }

  waterway* get_tailrace(power_plant const & pl);

  /**
   * Returns penstock for a given aggregate
   * if always_inlet, returns waterroute segment above inlet to aggregate
   * else
   * @param agg
   * @param always_inlet
   * @return waterway which is penstock
   */
  waterway* get_penstock(unit const & agg, bool always_inlet = false);

  bool is_plant_tailrace(waterway const & wtr);
  bool is_plant_outlet(waterway const & wtr);
}
