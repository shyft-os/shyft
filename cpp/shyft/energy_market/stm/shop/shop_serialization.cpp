/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
//
// 1. first include std stuff and the headers for
// files with serialization support
//

#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_problem.h>

// then include stuff you need like vector,shared, base_obj,nvp etc.
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>

//
// 2. Then implement each class serialization support
//
using namespace shyft::core;

template <class Archive>
void shyft::energy_market::stm::shop::shop_command::serialize(Archive& ar, unsigned int const /*file_version*/) {
  ar& core_nvp("keyword", keyword) & core_nvp("specifier", specifier) & core_nvp("options", options)
    & core_nvp("objects", objects);
}

template <class Archive>
void shyft::energy_market::stm::shop::problem::serialize(Archive& ar, unsigned int const /*file_version*/) {
  ar& core_nvp("time_axis", time_axis) & core_nvp("commands", commands) & core_nvp("system", system);
}

//
// 3. Then export class serialization support
//
x_serialize_instantiate_and_register(shyft::energy_market::stm::shop::shop_command);
x_serialize_instantiate_and_register(shyft::energy_market::stm::shop::problem);
