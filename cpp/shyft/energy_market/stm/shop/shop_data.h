#pragma once
#include <ctime>
#include <memory>
#include <vector>

#include <boost/hof/unpack.hpp>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/shop/api/shop_proxy.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm::shop {

  constexpr auto shop_nan = 1e40; // SHOP internally represents NaN as this number, need to convert

  template <class U>
  inline constexpr auto to_base = [](auto const v) {
    if (v == shop_nan)
      return nan;
    return U::to_base(v);
  };

  template <class Ux, class Uy>
  auto to_curve(auto const & z, auto const & x, auto const & y) {
    hydro_power::xy_point_curve_with_z r{{{}}, z};
    std::ranges::transform(std::views::zip(x, y), std::back_inserter(r.xy_curve.points), [](auto&& elements) {
      auto& [xv, yv] = elements;
      return hydro_power::point{to_base<Ux>(xv), to_base<Uy>(yv)};
    });
    return r;
  }

  template <class Ux, class Uy>
  auto from_curve(auto const & v) {
    int n = v.xy_curve.points.size();
    std::vector<double> x(n);
    std::vector<double> y(n);
    auto target = std::views::zip(x, y);
    std::ranges::transform(v.xy_curve.points, std::ranges::begin(target), [](auto const & xy) {
      return std::make_pair(Ux::from_base(xy.x), Uy::from_base(xy.y));
    });
    return std::make_tuple(std::move(x), std::move(y));
  }

  template <class U>
  auto to_ts(auto const & t_axis, auto const & t, auto const & y, std::time_t& time_resolution_unit) {
    auto n_axis{t_axis.size()};
    auto n{y.size()};
    if (n > n_axis) { // n_values should be equal to n_axis-1 for most results, or equal to n_axis for some
                      // such as reservoir level results.
      throw std::runtime_error("Expected number of values is less than or equal to number of points on the time axis");
    }
    // Average if the number of points is one less then the axis, instant if there is one value per entry:
    auto const point_fx = n == n_axis ? time_series::POINT_INSTANT_VALUE : time_series::POINT_AVERAGE_VALUE;
    std::vector<core::utctime> t_result(n);
    auto t_axis_start{t_axis.front()};
    std::ranges::transform(
      std::views::zip(t_axis, t), t_result.data(), boost::hof::unpack([&](auto&& t_axis_entry, auto&& t_v) {
        return core::utctime_from_seconds64(t_axis_start + t_v * time_resolution_unit);
      }));
    if (std::ranges::any_of(std::views::zip(t_axis, t_result), boost::hof::unpack([](auto&& t_a, auto&& t_result) {
                              return t_a != core::to_seconds64(t_result);
                            })))
      throw std::runtime_error("Time axis mismatch");
    auto end_t_axis = core::utctime_from_seconds64(t_axis.back());
    if (point_fx == time_series::POINT_INSTANT_VALUE) {
      // Instant series contain additional value at end, so we to define a new end of the time axis
      end_t_axis += core::utctimespan(1);
    }
    if (t_result.back() >= end_t_axis) {
      throw std::runtime_error("Time axis mismatch");
    }
    std::vector<double> v(n);
    std::ranges::transform(y, v.data(), to_base<U>);
    return time_series::dd::apoint_ts{
      time_axis::point_dt{std::move(t_result), std::move(end_t_axis)},
      std::move(v), std::move(point_fx)
    };
  }

  template <class U>
  auto from_ts(auto const & ts, auto const & t_axis) {
    std::vector<core::utctime> utc_axis(t_axis.size());
    std::ranges::transform(t_axis, utc_axis.data(), core::utctime_from_seconds64);
    auto t_start{utc_axis.front()};
    ::shop::data::shop_time start_time{core::to_seconds64(t_start)};
    if (!ts) {
      return std::make_tuple(start_time, std::vector<int>{}, std::vector<double>{});
    }
    auto tsa = ts.average(time_axis::generic_dt{utc_axis}).evaluate();
    int n{static_cast<int>(tsa.size())};
    std::vector<int> t(n);
    std::vector<double> y(n);
    {
      auto target = std::views::zip(t, y);
      std::ranges::transform(std::views::iota(0, n), std::ranges::begin(target), [&](auto const i) {
        return std::make_pair(
          static_cast<int>(core::to_seconds64(tsa.time(i) - t_start) / ::shop::data::shop_time_resolution_unit),
          U::from_base(tsa.value(i)));
      });
    }
    return std::make_tuple(start_time, std::move(t), std::move(y));
  }
}
