#include <algorithm>
#include <cstdint>
#include <numeric>
#include <ranges>
#include <unordered_set>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_custom_attributes.h>
#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <shyft/energy_market/stm/shop/shop_string_converter.h>
#include <shyft/energy_market/stm/shop/shop_topology.h>
#include <shyft/time_series/dd/qac_ts.h>

namespace shyft::energy_market::stm::shop {

  void shop_emitter::to_shop(stm_system const & stm) {

    if (auto cost = custom_attributes::gate_ramp_cost(stm)) {
      auto settings = api_handle.global_settings();
      settings.gate_ramp_cost = *cost;
    }

    adapter.ema_prod_area_id = 1; // shop need to start at 1,
    auto mkts = stm.market;
    std::ranges::sort(mkts, [](auto const & a, auto const & b) {
      return (!a->unit_groups.empty() && b->unit_groups.empty())
          || (a->unit_groups.empty() == b->unit_groups.empty() && a->id < b->id);
    });
    auto markets_map = emit(mkts);
    emit(stm.hps);

    auto reserve_unit_groups = std::views::filter(stm.unit_groups, [&](auto const & ug) {
      return is_operational_reserve_type(ug->group_type);
    });
    for (auto const & ug : reserve_unit_groups) {
      // Record which units are active in this group for each time step as bit coded values in a time series
      auto const group_ts = compute_group_unit_combinations_ts(ug, adapter.time_axis);
      // For each unique set of members, emit a separate group with series masked to the time steps where these
      // members are active and connect these members to it
      std::vector<shop_reserve_group> shop_groups;
      auto const group_ts_values = group_ts.values();
      std::unordered_set const unit_combinations(std::cbegin(group_ts_values), std::cend(group_ts_values));
      for (auto const & unit_combination : unit_combinations) {
        // mask ts initially all zeros, later set to 1 in each time step this combination of units are active
        apoint_ts const mask_ts = group_ts.inside(unit_combination - 0.1, unit_combination + 0.1, 0.0, 1.0, 0.0);
        // generate unique name since same stm unit group may be emitted
        // several times (with different unit combination masks)
        auto const name = fmt::format("{}#{}", ug->name, static_cast<std::size_t>(unit_combination));
        auto shop_ug = adapter.to_shop(*ug, mask_ts, name);
        for (std::size_t i = 0, n = ug->members.size(); i < n; ++i) {
          if (static_cast<std::size_t>(1) << i & static_cast<std::size_t>(unit_combination)) {
            auto const & ugm = ug->members[i];
            auto const & shop_u = objects.cget<shop_unit>(ugm->unit.get());
            api_handle.connect_objects(shop_u, shop_u.connection_standard, shop_ug.id);
          }
        }
        shop_groups.push_back(std::move(shop_ug));
      }
      // Save the list of emitted shop groups emitted for this stm unit group in object map
      objects.add(ug.get(), std::move(shop_groups));
    }

    // Unit groups: Production
    if (!stm.market.empty()) {
      std::vector<unit_group_> ema_groups; // unit group with production for each area
      // energy market area to shop market id map
      std::map<std::int64_t, std::int64_t> mid_map;
      auto add_ug_to_prod_market_map = [&](auto tup) {
        auto const& [ug, stm_market] = tup;
        auto const & mkt = objects.get<shop_market>(stm_market);
        mid_map[ug->id] = mkt.prod_area;
        ema_groups.push_back(ug);
      };
      std::ranges::for_each(
        std::views::filter(
          std::views::transform(
            stm.market,
            [](auto const & ema) {
              return std::make_tuple(ema->get_unit_group(), ema.get());
            }),
          [](auto const & tup) {
            auto const& [ug, em] = tup;
            return ug && ug->group_type == unit_group_type::production;
          }),
        add_ug_to_prod_market_map);

      apoint_ts ts{adapter.time_axis, 1.0, POINT_AVERAGE_VALUE};
      if (
        std::ranges::count_if(
          stm.market,
          [](auto const & m) {
            return !m->has_unit_group();
          })
        > 1) {
        std::runtime_error("More then one energy_market has no unit group defined, can not emit");
      }

      // BWCOMPAT: if there is a market without unit groups, create a temporary one, and add all units.
      if (ema_groups.empty()) {
        // find market without a unit group
        auto ema = std::ranges::find_if(stm.market, [](auto const & m) {
          return !m->has_unit_group();
        });
        if (ema != stm.market.end()) {
          auto ug = std::make_shared<unit_group>();
          for (auto const & hps : stm.hps)
            for (auto const & u_ : hps->units)
              ug->add_unit(std::dynamic_pointer_cast<stm::unit>(u_), ts);
          ug->group_type = unit_group_type::production;
          ug->id = ema->get()->id;
          add_ug_to_prod_market_map(std::make_tuple(ug, ema->get()));
        }
      }

      for (auto const & hps : stm.hps) {
        for (auto const & u_ : hps->units) { // for all units, attach the time-dependent market ts
          auto u = std::dynamic_pointer_cast<stm::unit>(
            u_); // because hps->units are core units, not stm units that we need
          auto mts = compute_unit_group_membership_ts(
            ema_groups, u, ts, unit_group_type::production, &mid_map); // time dep. market assoc
          if (!mts) {
            continue;
          }
          if (auto s_u = objects.get_if<shop_unit>(*u))
            adapter.set(s_u->prod_area, mts);
        }
      }

      // Shop contracts: Currently emitted as stand-alone objects in shop
      {
        auto contracts = std::views::join(std::views::transform(stm.market, [](auto const & ma) -> auto& {
          return ma->contracts;
        }));
        auto contracts_to_emit = std::views::filter(contracts, [&](auto const & ctr) {
          return should_emit(adapter.time_axis, *ctr);
        });
        std::ranges::for_each(contracts_to_emit, [&](auto const & ctr) {
          objects.add(ctr.get(), adapter.to_shop(*ctr));
        });
      }
      // Reserve market connections: Connect reserve ug to markets:
      auto shop_mugs = std::views::transform(
        markets_map, [&](auto& item) -> std::tuple<std::vector<shop_market>*, std::vector<shop_reserve_group>*> {
          auto& [m, shop_markets] = item;
          auto ug = m->get_unit_group().get();
          if (!ug)
            return {};
          auto const & shop_ugvitr = objects.find<std::vector<shop_reserve_group>>(ug);
          if (shop_ugvitr == objects.end<std::vector<shop_reserve_group>>())
            return std::make_tuple<std::vector<shop_market>*, std::vector<shop_reserve_group>*>(&shop_markets, nullptr);
          return std::make_tuple(&shop_markets, &shop_ugvitr->second);
        });
      auto to_connect = std::views::filter(shop_mugs, [](auto const & mug) {
        auto const& [shop_mkts, ugv] = mug;
        return shop_mkts != nullptr && ugv != nullptr;
      });
      std::ranges::for_each(to_connect, [&](auto const & mug) {
        auto const& [shop_mkts, ugv] = mug;
        for (auto const & shop_mkt : *shop_mkts) {
          for (auto const & shop_ug : *ugv) {
            api_handle.connect_objects(shop_mkt, shop_mkt.connection_standard, shop_ug.id);
          }
        }
      });
    }
  }

  template <typename shop_type>
  inline constexpr std::type_identity<shop_type> st{};

  void shop_emitter::from_shop(stm_system& stm) {
    auto so = api_handle.get<shop_objective>("average_objective");
    adapter.from_shop(*stm.summary, so);
    auto from_shop_if = [&]<typename shop_type>(auto* stm_component, std::type_identity<shop_type>) {
      if (!stm_component)
        return false;
      auto const shop_iterator = objects.find<shop_type>(stm_component);
      if (shop_iterator == objects.end<shop_type>())
        return false;
      adapter.from_shop(*stm_component, shop_iterator->second);
      return true;
    };

    for (auto const & m : stm.market) {
      if (auto mkt = dynamic_cast<energy_market_area*>(m.get())) {
        from_shop_if(mkt, st<shop_market>);
        for (auto const & ctr : mkt->contracts)
          from_shop_if(ctr.get(), st<shop_contract>);
      }
    }
    for (auto const & ug : stm.unit_groups) {
      if (auto const it = objects.find<std::vector<shop_reserve_group>>(ug.get());
          it != objects.end<std::vector<shop_reserve_group>>()) {
        for (auto const & shop_group : it->second) {
          adapter.from_shop(*ug, shop_group);
        }
      }
    }

    for (auto const & hps : stm.hps) {
      for (auto const & r : hps->reservoirs)
        from_shop_if(dynamic_cast<reservoir*>(r.get()), st<shop_reservoir>);
      for (auto const & p : hps->power_plants)
        from_shop_if(dynamic_cast<power_plant*>(p.get()), st<shop_power_plant>);
      for (auto const & u : hps->units) {
        if (auto uu = dynamic_cast<unit*>(u.get())) {
          uu->production.result = apoint_ts{};
          uu->discharge.result = apoint_ts{};
          from_shop_if(uu, st<shop_unit>);
          from_shop_if(uu, st<shop_pump>);
        }
      }
      for (auto const & w : hps->waterways) {
        auto wtr = dynamic_cast<waterway*>(w.get());
        if (from_shop_if(wtr, st<shop_tunnel>))
          ;
        else if (from_shop_if(wtr, st<shop_river>))
          ;
        else if (from_shop_if(wtr, st<shop_gate>))
          ;
        else
          for (auto const & g : wtr->gates)
            from_shop_if(dynamic_cast<gate*>(g.get()), st<shop_gate>);
        from_shop_if(wtr, st<shop_discharge_group>);
      }
    }
  }

  std::map<energy_market_area const *, std::vector<shop_market>>
    shop_emitter::emit(std::vector<std::shared_ptr<energy_market_area>> const & markets) {
    std::map<energy_market_area const *, std::vector<shop_market>> result;
    std::ranges::for_each(markets, [&](auto const & m) {
      auto res = adapter.to_shop(*m);
      auto&& [market, submarkets] = res;
      submarkets.push_back(market);
      objects.add(&(*m), std::move(market));
      result[m.get()] = std::move(submarkets);
    });
    return result;
  }

  void shop_emitter::emit(power_plant const & pl) {
    // Migrate power plant, with surrounding topology (aggregates and tunnels),
    // and connect it to upstream reservoirs, possibly via junctions and/or creek intakes, as well
    // as downstream reservoir (if any).
    // Current assumptions:
    // - Only a single main tunnel from intake reservoirs (not supporting parallel intakes).
    // - Each aggregate have a dedicated waterroute upstream, which is either its penstock or an inlet
    //   tunnel leading from a penstock shared with other aggregates.
    // - There can only be one waterroute segment of any inlet and penstock.
    // - Penstock loss is the headloss coefficient on individual penstocks, where aggregates can have
    //   separate or shared penstocks which must be indicated by their penstock attribute.
    // - Tailrace loss is the headloss coefficient on the shared (accross aggregates) tailrace.
    //   Assuming always one output tunnel from each aggreagate, which is always a draft tube,
    //   and then always one output tunnel from the draft tube, which is always the tailrace.
    // - Plant main loss is the headloss in the shared (accross all penstocks and aggregates)
    //   main intake tunnel, between the reservoir and the penstocks, and can be described in two ways:
    //     - Head loss coefficients: An array of double values representing loss factors. Normally there
    //       is only one coefficient, but there may be multiple values if there is a need to differentate
    //       segments of the main tunnel. Currently we only consider loss from a single segment, and then
    //       pick the most downstream main tunnel segment (immediately upstream from penstocks).
    //     - Loss function: An array of XYZ descriptions, one for each head. When this is used, one
    //       normally does not specify head loss coefficients, nor penstock loss coefficients, as the loss
    //       function describes the complete input loss in detail. We only look for loss function on the
    //       most downstream main tunnel segment (immediately upstream from penstocks).
    // Create basic Shop representation of the power station
    auto sp = objects.add(&pl, adapter.to_shop(pl));
    std::vector<waterway*> penstocks = handle_plant_units(pl, sp);
    waterway* tailrace = topology::get_tailrace(pl);
    if (tailrace && topology::traverse_find_type<reservoir>(*tailrace, topology::direction::upstream)) {
      // If tail race have an input from a reservoir, assuming bypass, then we will
      // have emitted this as a gate connection from this reservoir to the reservoir
      // downstream from plant, but must now set the option on plant that triggers Shop
      // to include this flow in the tailrace loss calculation.
      apoint_ts flag_ts{adapter.time_axis, 1.0, time_series::POINT_AVERAGE_VALUE};
      adapter.set(sp.tailrace_loss_from_bypass_flag, flag_ts);
    }
    // Set penstock and tailrace head loss attributes (penstock and tailrace segments are parts of the plant in shop)
    std::vector<double> penstock_loss;
    std::transform(
      std::cbegin(penstocks), std::cend(penstocks), std::back_inserter(penstock_loss), [this](auto penstock) {
        return get_tunnel_loss_coeff(*penstock);
      });
    adapter.set(sp.penstock_loss, penstock_loss);
    if (tailrace && adapter.exists(tailrace->head_loss_func)) { // Accept missing tailrace for plant?
      adapter.set(sp.tailrace_loss, tailrace->head_loss_func);
    }
    // Continue with topology upstream from penstocks: Connect shop plant with tunnel objects (new tunnel module) up to
    // input reservoirs.
    if (
      auto* main = penstocks.front()->upstreams.empty()
                   ? nullptr
                   : dynamic_cast<waterway*>(penstocks.front()->upstreams.front().target.get())) {
      handle_plant_input(*main, sp);
      if (adapter.exists(main->head_loss_func)) // Head loss function (XYZ array)
        adapter.set(sp.intake_loss, main->head_loss_func);
    }
    // Legacy (pre tunnel module): Array with loss in each segment of the main tunnel.
    std::vector<double> main_loss{0.0};
    adapter.set(sp.main_loss, main_loss);
    // Continue with topology downstream from trailrace: Connect plant output to downstream reservoir - if any.
    if (tailrace && !handle_plant_output(sp, *tailrace) && adapter.valid_temporal(pl.outlet_level)) {
      // If no reservoir downstream, emit tides attribute as deltas to the outlet level.
      // The initial value of outlet level attribute is already emitted as scalar value
      // outlet_level, and we will now emit the deltas compared to this for any additional
      // outlet level values within the timeaxis.
      // NOTE: This is an experimental/hacky solution, because it has not yet been
      // concluded how users want this to be handled, and also expecting improvements in
      // shop will affect this feature in coming versions.
      auto base_level = adapter.get_temporal(pl.outlet_level, 0.0);
      apoint_ts diff_ts{adapter.time_axis, 0.0, POINT_AVERAGE_VALUE};
      diff_ts = pl.outlet_level.use_time_axis_from(diff_ts) - base_level;
      adapter.set(sp.tides, diff_ts);
    }
  }

  void shop_emitter::emit(stm_hps const & hps) {
    // reservoirs must be emitted before plants.
    for (auto const & r_hydro : hps.reservoirs)
      emit(dynamic_cast< reservoir const &>(*r_hydro));
    // Gate connections of emitted
    for (auto const & r_hydro : hps.reservoirs)
      handle_reservoir_output(dynamic_cast<reservoir const &>(*r_hydro));
    for (auto const & p_hydro : hps.power_plants) {
      auto const & p = dynamic_cast<power_plant const &>(*p_hydro);
      // Note: Plants may have already been emitted
      if (!objects.exists<shop_power_plant>(p))
        emit(p);
    }
    // Emit any discharge groups, and set time delay from stm waterroutes,
    // that are not emitted directly, on shop representation of upstream
    // gate and plant objects.
    for (auto const & wv : hps.waterways) {
      auto const & w = dynamic_cast<waterway const &>(*wv);
      if (handle_inflow_river(w))
        continue;
      if (auto shop_w = objects.find<shop_river>(&w); shop_w != objects.end<shop_river>())
        continue;
      handle_discharge_group(w); //  need for weighted discharge + accumulated deviation. - jeh
      handle_time_delay(w);
    }
    for (auto const & ra : hps.reservoir_aggregates) {
      if (!ra->volume.constraint.min && !ra->volume.constraint.max)
        continue;
      auto& ra_shop = objects.add(ra.get(), adapter.to_shop(*ra));
      for (auto const & r : ra->reservoirs)
        api_handle.connect_objects(
          ra_shop, shop_volume_constraint::connection_standard, objects.get<shop_reservoir>(r).id);
    }
  }

  bool shop_emitter::is_tunnel(waterway const & wtr) const {
    return adapter.valid_temporal(wtr.head_loss_coeff);
  }

  double shop_emitter::get_tunnel_loss_coeff(waterway const & wtr) const {
    return adapter.valid_temporal(wtr.head_loss_coeff) ? adapter.get_temporal(wtr.head_loss_coeff, 0.0) : 0.0;
  }

  namespace detail {
    void foreach_upstream_shop_source(waterway const & wtr, shop_objects& objects, auto func) {
      std::vector< power_plant const *> plants;
      auto f_process_component_ = [&](auto const & c) {
        if (auto ut = dynamic_cast<unit*>(c.get())) {
          if (auto ps = dynamic_cast<power_plant*>(ut->pwr_station_().get())) {
            // In shop plants are the representation, find corresponding shop plant
            if (std::ranges::find(plants, ps) == std::ranges::end(plants)) { // only emit new plants
              if (auto const it = objects.find<shop_power_plant>(ps); it != objects.end<shop_power_plant>())
                func(it->second);
              else
                throw std::runtime_error(fmt::format("Plant not emitted to shop: {}", ps->name));
              plants.push_back(ps);
            }
          }
        }
      };
      auto f_consider_waterway = [&](auto const & w) {
        if (w.gates.empty()) {
          if (auto const it = objects.find<shop_gate>(&w); it != objects.end<shop_gate>()) {
            func(it->second);
            return true;
          }
          if (auto const it = objects.find<shop_river>(&w);
              (it != objects.end<shop_river>()) && (w.upstreams.size() == 1)
              && dynamic_cast<stm::reservoir const *>(w.upstreams[0].target.get())) {
            func(it->second);
            return true;
          }
        }
        bool is_shop_gate = false;
        std::ranges::for_each(w.gates, [&](auto const & g) {
          if (auto const it = objects.find<shop_gate>(static_cast<id_base const *>(g.get()));
              it != objects.end<shop_gate>()) {
            func(it->second);
            is_shop_gate = true;
          }
        });
        // Stop traverse by returning false if this waterway or any of its
        // gates were emitted as gates to shop.
        return !is_shop_gate;
      };
      auto f_consider_waterway_ = [&f_consider_waterway](waterway const * w) {
        return w && f_consider_waterway(*w);
      };
      if (f_consider_waterway(wtr)) {
        traverse_for_each(f_process_component_, f_consider_waterway_, wtr, topology::direction::upstream);
      }
    }
  }

  void shop_emitter::handle_discharge_group(waterway const & wtr) {
    if ( ( shop_adapter::exists(wtr.discharge.reference) &&
            (shop_adapter::exists(wtr.discharge.constraint.accumulated_max) ||
            shop_adapter::exists(wtr.discharge.constraint.accumulated_min)))
        ||
        (shop_adapter::exists(wtr.discharge.constraint.max) || shop_adapter::exists(wtr.discharge.constraint.min))
        || (shop_adapter::exists(wtr.discharge.constraint.ramping_up) ||
            shop_adapter::exists(wtr.discharge.constraint.ramping_down))
        || (shop_adapter::exists(wtr.discharge.constraint.max_average) ||
            shop_adapter::exists(wtr.discharge.constraint.min_average))) {
      auto sdg = objects.add<shop_discharge_group>(wtr, adapter.to_shop_discharge_group(wtr));
      detail::foreach_upstream_shop_source(wtr, objects, [&](auto const & shop_obj) {
        api_handle.connect_objects(sdg, sdg.connection_standard, shop_obj.id);
      });
    }
  }

  /**
   * Handle time delay for topology differences.
   *
   * When emitting waterways representing flood and bypass river output from
   * reservoirs, any time delay will be set on the gate. This function adds
   * additional handling of time delay for topology differences between stm and shop:
   * Time delay on a downstream waterway is also set on any/all gate or plant objects
   * emitted to shop that represent upstream objects, except if these already had a
   * time delay set directly.
   */
  void shop_emitter::handle_time_delay(waterway const & wtr) {
    detail::foreach_upstream_shop_source(wtr, objects, [&](auto& shop_obj) {
      adapter.set_shop_time_delay(shop_obj, wtr.delay);
    });
  }

  void shop_emitter::handle_plant_input(reservoir const & stm_obj, shop_object_id shop_downstream_obj) {
    if (auto shop_upstream = objects.get_if<shop_reservoir>(stm_obj))
      return api_handle.connect_objects(*shop_upstream, shop_upstream->connection_standard, shop_downstream_obj.id);
    throw std::runtime_error("Reached an input reservoir which has not been emitted");
  }

  void shop_emitter::handle_plant_input(power_plant const & stm_obj, shop_object_id shop_downstream_obj) {
    auto shop_plant = objects.get_if<shop_power_plant>(stm_obj);
    if (!shop_plant) {
      emit(stm_obj);
      shop_plant = objects.get_if<shop_power_plant>(stm_obj);
    }
    if (shop_plant)
      return api_handle.connect_objects(*shop_plant, shop_plant->connection_standard, shop_downstream_obj.id);
    throw std::runtime_error("Reached an input plant which could not be emitted");
  }

  void shop_emitter::handle_plant_input(waterway const & stm_obj, shop_object_id shop_downstream_obj) {
    if (auto shop_existing_obj = objects.get_if<shop_tunnel>(stm_obj))
      return api_handle.connect_objects(
        *shop_existing_obj, shop_existing_obj->connection_standard, shop_downstream_obj.id);
    // Create shop tunnel representing current stm waterway.
    auto& shop_obj = objects.add(&stm_obj, adapter.to_shop(stm_obj));
    api_handle.connect_objects(shop_obj, shop_obj.connection_standard, shop_downstream_obj.id);

    if (stm_obj.upstreams.empty())
      throw std::runtime_error(fmt::format("No upstreams from waterway {} [{}]", stm_obj.name, stm_obj.id));

    auto begin_iterator = stm_obj.upstreams.begin();
    if (auto up_reservoir = dynamic_cast<reservoir*>(begin_iterator->target.get())) {
      if (stm_obj.upstreams.size() > 1)
        throw std::runtime_error(fmt::format(
          "Multiple upstreams can only be all waterways or all units from waterway {} [{}]", stm_obj.name, stm_obj.id));
      handle_plant_input(*up_reservoir, shop_obj);
      return;
    }
    if (auto up_unit = dynamic_cast<unit*>(begin_iterator->target.get())) {
      if (std::ranges::none_of(stm_obj.upstreams, [](auto connection) {
            return dynamic_cast<unit*>(connection.target.get());
          }))
        throw std::runtime_error(
          fmt::format("Multiple upstreams are not all units from waterway {} [{}]", stm_obj.name, stm_obj.id));
      auto plant = dynamic_cast<power_plant*>(up_unit->pwr_station_().get());
      if (!plant)
        throw std::runtime_error("Reached an input unit without a plant");
      handle_plant_input(*plant, shop_obj);
      return;
    }
    if (dynamic_cast<waterway*>(begin_iterator->target.get())) {
      std::ranges::for_each(
        std::views::transform(
          stm_obj.upstreams,
          [](auto const & connection) {
            return dynamic_cast<waterway*>(connection.target.get());
          }),
        [&](auto waterway) {
          if (!waterway)
            throw std::runtime_error(
              fmt::format("Multiple upstreams are not all waterways from waterway {} [{}]", stm_obj.name, stm_obj.id));
          handle_plant_input(*waterway, shop_obj);
        });
      return;
    }

    throw std::runtime_error(fmt::format(
      "Reached an upstream from {} [{}] which is not a waterway, reservoir or unit when handling plant input",
      stm_obj.name,
      stm_obj.id));
  }

  bool shop_emitter::handle_plant_output(shop_power_plant const & shop_plant_obj, waterway const & tailrace) {
    // Handle topology downstream from plant/trailrace: If there is a downstream reservoir, then
    std::vector<hydro_power::hydro_connection> backtrack;
    auto const & down = topology::traverse(
      tailrace, topology::direction::downstream, topology::role_filter::all, -1, &backtrack);
    auto rsv = dynamic_cast<reservoir*>(down.get());
    if (!rsv)
      return false;
    auto rsv_id = objects.id_of<shop_reservoir>(rsv);
    if (!rsv_id)
      return false;
    if (backtrack.empty())
      return false;
    auto it = backtrack.crbegin();
    if (auto wtr = dynamic_cast<waterway*>(it->target.get())) {
      if (is_tunnel(*wtr)) {
        // Emitting connection to downstream reservoir as tunnel segments
        auto shop_wtr = &objects.add(wtr, adapter.to_shop(*wtr));
        api_handle.connect_objects(shop_plant_obj, shop_plant_obj.connection_standard, shop_wtr->id);
        for (++it; it != backtrack.crend(); ++it) {
          if (auto const & wtr_next = std::dynamic_pointer_cast<waterway>(it->target)) {
            if (auto shop_wtr_next = objects.get_if<shop_tunnel>(*wtr_next)) {
              api_handle.connect_objects(*shop_wtr, shop_wtr->connection_standard, shop_wtr_next->id);
              break;
            } else {
              shop_wtr_next = &objects.add(it->target.get(), adapter.to_shop(*wtr_next));
              api_handle.connect_objects(*shop_wtr, shop_wtr->connection_standard, shop_wtr_next->id);
              shop_wtr = shop_wtr_next;
            }
          } else {
            break; // Last item is the reservoir that we reached, should be the only possible case for this!
          }
        }
        api_handle.connect_objects(*shop_wtr, shop_wtr->connection_standard, rsv_id.id);
        return true; // Done, connected to downstream reservoir via plant
      }
    }
    // Emitting connection to downstream reservoir as river, connecting plant directly to the reservoir.
    api_handle.connect_objects(shop_plant_obj, shop_plant_obj.connection_standard, rsv_id.id);
    return true; // Done, connected to downstream reservoir directly (river)
  }

  void shop_emitter::handle_reservoir_output(reservoir const & rsv) {
    // Handle output connections from a reservoir, but only those ending
    // in other reservoirs. Connections from reservoirs to aggregates are handled
    // by the upstream tunnel handling for power plants.
    // NOTE: Since we currently allow bypass and main waterroutes out from reservoir
    // which are not connected to anything downstream, implicitely to the sea,
    // we must process the connections _out_ and not _in_ from the reservoirs!

    // This is a reservoir output that must be emitted: Flow from one reservoir
    // into another, or from reservoir into nothing/sea.
    auto shop_rsv = objects.get_if<shop_reservoir>(rsv);
    if (!shop_rsv)
      throw std::runtime_error(fmt::format(
        "Failed to create reservoir output: Unable to find shop object for stm reservoir {} [{}]", rsv.name, rsv.id));
    auto shop_role = [&](auto role) {
      switch (role) {
        using enum hydro_power::connection_role;
      case bypass:
        return shop_rsv->connection_bypass;
      case flood:
        return shop_rsv->connection_spill;
      default:
        return shop_rsv->connection_standard;
      }
    };

    auto emit_tunnel_output = [&](auto wtr, auto role, auto rsv_down_id, auto& backtrack) {
      // Tunnel, represented by tunnel object also in Shop.
      // Note: Requires it to end in a reservoir, a waterway string not connected
      // to anything downstream will always be emitted as a river.
      auto shop_wtr = &objects.add(wtr, adapter.to_shop(*wtr));
      api_handle.connect_objects(*shop_rsv, shop_role(role), shop_wtr->id);
      for (auto it = backtrack.crbegin(); it != backtrack.crend(); ++it) {
        if (auto back_wtr = dynamic_cast<stm::waterway*>(it->target.get())) {
          if (auto stm_tun_next = objects.get_if<shop_tunnel>(*back_wtr)) {
            api_handle.connect_objects(*shop_wtr, shop_wtr->connection_standard, stm_tun_next->id);
            break;
          } else {
            auto shop_wtr_next = &objects.add(back_wtr, adapter.to_shop(*back_wtr));
            api_handle.connect_objects(*shop_wtr, shop_wtr->connection_standard, shop_wtr_next->id);
            shop_wtr = shop_wtr_next;
          }
        } else {
          break; // Last item is the reservoir that we reached, should be the only possible case for this!
        }
      }
      api_handle.connect_objects(*shop_wtr, shop_wtr->connection_standard, rsv_down_id);
    };
    auto emit_river_or_gate_output = [&](auto wtr, auto role, auto rsv_down_id) {
      // Waterway is a river. Originally represented by gate object and now replaced by river object in Shop.
      // We start the transition by emitting waterways that have no gates in stm.
      // If the waterway has gates, we still emit as gate in Shop, one for each gate in stm.
      // Note: We only use the first (upstream) stm waterway (with its gates) to create the Shop gate,
      //       if there are multiple waterway segments then we ignore any gate on following segments.
      // TODO: We send all gates on the water route (parallel gates) to Shop, but it is not
      //       currently supporting more than one gate on the flood connection!

      auto gates = std::views::transform(wtr->gates, [&](auto&& gate) {
        return dynamic_cast<stm::gate const *>(gate.get());
      });

      auto connect = [&](auto&& obj) {
        api_handle.connect_objects(*shop_rsv, shop_role(role), obj.id);
        if (rsv_down_id)
          api_handle.connect_objects(obj, obj.connection_standard, *rsv_down_id);
      };

      if (std::ranges::empty(gates))
        connect(objects.add(wtr, adapter.to_shop_river(*wtr, nullptr)));
      else
        for (auto gate : gates)
          connect(objects.add(gate, adapter.to_shop_gate(*wtr, gate)));
    };
    for (auto const & out : rsv.downstreams) {
      if (out.role == hydro_power::connection_role::input)
        continue;
      auto wtr = dynamic_cast<stm::waterway const *>(out.target.get());
      if (!wtr)
        continue;
      // Find downstream object (if any), and then:
      // - If a downstream object was found, and it is a reservoir, create a gate
      //   object and connect the two reservoirs via it. The stm model may or may
      //   not have actual gates, it could be waterway with delta-meter function,
      //   but in any case it must be modelled as gate in shop.
      // - If the downstream object was anything else, skip - aggregates will be
      //   handled elsewhere by the upstream tunnel handling for power plants.
      // - If there was no downstream object then add a gate connected upstream only,
      //   effectively just letting it drain out into the sea. This is often used for
      //   bypass and flood, but could also be main waterway out from a reservoir
      //   at the end of the topology (ref: Trollheim). This case is only supported
      //   for rivers (not tunnels).
      std::vector<hydro_power::hydro_connection> backtrack;
      auto const & down = topology::traverse(
        *wtr, topology::direction::downstream, topology::role_filter::all, -1, &backtrack);
      auto rsv_down_id = [&] -> std::optional<std::int64_t> {
        if (auto rsv_down = dynamic_cast<stm::reservoir const *>(down.get()))
          return {objects.id_of<shop_reservoir>(*rsv_down).id};
        return std::nullopt;
      }();
      if (!rsv_down_id && down)
        continue;
      if (rsv_down_id && is_tunnel(*wtr))
        emit_tunnel_output(wtr, out.role, *rsv_down_id, backtrack);
      else
        emit_river_or_gate_output(wtr, out.role, rsv_down_id);
    }
  }

  std::vector<waterway*> shop_emitter::handle_plant_units(power_plant const & obj, shop_power_plant& shop_pp) {
    std::vector<waterway*> penstocks;
    auto handle_unit = [&](unit const & unit_obj, auto& shop_unit_obj) -> auto& {
      // Find and set penstock for the shop unit
      auto penstock = topology::get_penstock(unit_obj);
      if (!penstock)
        throw std::runtime_error(fmt::format(
          "Unable to find penstock for plant {} [{}] unit {} [{}]", obj.name, obj.id, unit_obj.name, unit_obj.id));
      int penstock_number;
      if (auto it = std::find_if(
            std::cbegin(penstocks),
            std::cend(penstocks),
            [&](auto const & v) {
              return penstock->id == v->id;
            });
          it != std::cend(penstocks)) {
        penstock_number = (int) std::distance(std::cbegin(penstocks), it) + 1;
      } else {
        penstocks.push_back(penstock);
        penstock_number = penstocks.size();
      }
      adapter.set(shop_unit_obj.penstock, penstock_number);
      // Connect plant to unit in shop
      api_handle.connect_objects(shop_pp, shop_pp.connection_standard, shop_unit_obj.id);
      return shop_unit_obj;
    };
    for (auto const & u : obj.units) {
      if (auto uu = dynamic_cast<unit*>(u.get())) {
        auto capabilities = turbine_capability(*uu);
        if (hydro_power::has_reversible_capability(capabilities)) {
          auto& sgen = handle_unit(*uu, objects.add(uu, adapter.to_shop_generator_unit(*uu)));
          auto& spump = handle_unit(*uu, objects.add(uu, adapter.to_shop_pump_unit(*uu)));
          auto sgroup = api_handle.create<shop_commit_group>(uu->name);
          api_handle.connect_objects(sgroup, sgroup.connection_standard, sgen.id);
          api_handle.connect_objects(sgroup, sgroup.connection_standard, spump.id);
        } else if (hydro_power::has_backward_capability(capabilities)) {
          handle_unit(*uu, objects.add(uu, adapter.to_shop_pump_unit(*uu)));
        } else {
          handle_unit(*uu, objects.add(uu, adapter.to_shop_generator_unit(*uu)));
        }
      }
    }
    return penstocks;
  }

  bool shop_emitter::handle_inflow_river(waterway const & wtr) {
    if (!wtr.upstreams.empty() || !shop_adapter::exists(wtr.discharge.schedule))
      return false;
    auto w = objects.add(&wtr, adapter.to_shop_river(wtr, nullptr, true));
    auto handle_wtr = [&](waterway const * ds) {
      // if emitted already, just return
      if (auto r = objects.find<shop_river>(ds); r != objects.end<shop_river>()) {
        api_handle.connect_objects(w, w.connection_standard, r->second.id);
        return false;
      }
      auto r = objects.add(ds, adapter.to_shop_river(*ds, nullptr));
      api_handle.connect_objects(w, w.connection_standard, r.id);
      w = r;
      return true;
    };
    auto handle_cmp = [&](auto const & ds) {
      if (auto res = dynamic_cast<reservoir*>(ds.get())) {
        if (auto sr = objects.find<shop_reservoir>(res); sr != objects.end<shop_reservoir>()) {
          api_handle.connect_objects(w, w.connection_standard, sr->second.id);
          return false;
        }
        throw std::runtime_error(fmt::format(
          "Failed to connect to reservoir output: Unable to find shop object for stm reservoir {} [{}]",
          res->name,
          res->id));
      }
      throw std::runtime_error(fmt::format("Object with name {} and id {} is not reservoir", ds->name, ds->id));
    };
    traverse_for_each(handle_cmp, handle_wtr, wtr, topology::direction::downstream);
    return true;
  }

}
