#pragma once

#include <algorithm>
#include <array>
#include <cstdint>
#include <functional>
#include <optional>
#include <ranges>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>

#include <fmt/compile.h>
#include <fmt/format.h>

#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/url_parse.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm::shop::custom_attributes {

#define SHYFT_STM_SHOP_CUSTOM_ATTRIBUTES (gate_ramp_cost, river_discharge_result_downstream)

  SHYFT_DEFINE_ENUM(tag, std::uint32_t, SHYFT_STM_SHOP_CUSTOM_ATTRIBUTES)

  template <typename C, typename V>
  struct spec {
    using component_type = C;
    using value_type = V;
    std::string_view name;
    bool is_input;
  };

  template <tag T>
  inline constexpr auto get_spec = [] {
    using enum tag;
    if constexpr (T == gate_ramp_cost)
      return spec<stm_system, double>{.name = "gate_ramp_cost", .is_input = true};
    else {
      static_assert(T == river_discharge_result_downstream);
      return spec<waterway, time_series::dd::apoint_ts>{.name = "discharge.result_downstream", .is_input = false};
    }
  }();

  template <tag T>
  using spec_t = decltype(auto(get_spec<T>));

  template <tag T>
  using component_t = typename spec_t<T>::component_type;
  template <tag T>
  using value_t = typename spec_t<T>::value_type;

  template <tag T>
  inline constexpr auto attribute_prefix = [] {
    return get_spec<T>.is_input ? url_planning_custom_input_prefix : url_planning_custom_output_prefix;
  };

  template <tag T>
  inline constexpr auto attribute = [] {
    return fmt::format(FMT_COMPILE("{}.{}"), attribute_prefix<T>(), get_spec<T>.name);
  };

  template <tag T>
  std::optional<time_series::dd::apoint_ts> get_ts_attribute(component_t<T> const &c) {
    static auto const a = attribute<T>();
    auto it = c.tsm.find(a);
    if (it == c.tsm.end())
      return std::nullopt;
    return it->second;
  }

  template <tag T>
  constexpr auto get_attribute(component_t<T> const &c) -> std::optional<value_t<T>> {
    using V = value_t<T>;
    static auto const a = attribute<T>();
    auto it = c.custom.find(a);
    if (it == c.custom.end()) {
      if constexpr (std::is_same_v<V, time_series::dd::apoint_ts>)
        return get_ts_attribute<T>(c);
      return std::nullopt;
    }
    if (!std::holds_alternative<V>(it->second))
      return std::nullopt;
    return std::get<V>(it->second);
  };

  template <tag T, typename C>
  inline constexpr bool is_output_for = std::is_same_v<component_t<T>, C> && !get_spec<T>.is_input;

  template <typename C>
  inline constexpr auto output_attributes = [] {
    static constexpr auto filter = [&]<std::size_t... I>(std::index_sequence<I...>) {
      return std::to_array<bool>({is_output_for<tag{I}, C>...});
    }(std::make_index_sequence<enumerator_count<tag>>{});
    static constexpr auto count = std::ranges::count(filter, true);
    std::array<std::string, count> output;

    static auto const all = [&]<std::size_t... I>(std::index_sequence<I...>) {
      return std::to_array<std::string>({attribute<tag{I}>()...});
    }(std::make_index_sequence<enumerator_count<tag>>{});

    std::ranges::copy(
      std::views::transform(
        std::views::filter(
          std::views::iota(0uz, enumerator_count<tag>),
          [](auto i) {
            return filter[i];
          }),
        [](auto i) {
          return all[i];
        }),
      output.data());
    return output;
  };

#define SHYFT_LAMBDA(r, data, name) \
  inline const auto name##_url = attribute<tag::name>(); \
  inline const auto name = [](component_t<tag::name> const &arg) { \
    return get_attribute<tag::name>(SHYFT_FWD(arg)); \
  };
  BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_SHOP_CUSTOM_ATTRIBUTES))
#undef SHYFT_LAMBDA

#undef SHYFT_DEFINE_CUSTOM_ATTRIBUTE

  auto with_output_urls(auto &&func, std::string_view model_id, stm_system const &model) {
    url_string_buffer buffer{};
    url_format_root_to(std::back_inserter(buffer), model_id);
    url_with_comps(buffer, model, [&]<typename T>(T const &subcomp) {
      auto marker = buffer.size();
      auto out = std::back_inserter(buffer);

      static constexpr auto prefixes = std::array{ts_attr_prefix, custom_attr_prefix};

      static auto const attrs = output_attributes<T>();
      for (auto [prefix, attr] : std::views::cartesian_product(prefixes, attrs)) {
        fmt::format_to(out, ".{}{}", prefix, attr);
        std::invoke(func, std::string_view{buffer.data(), buffer.size()});
        buffer.resize(marker);
      }
    });
  }

}
