#pragma once
#include <shyft/energy_market/stm/stm_system.h>

// Functions to fixup stm-system after the collection is complete
namespace shyft::energy_market::stm::shop {
  void post_process(stm_system& stm);

}