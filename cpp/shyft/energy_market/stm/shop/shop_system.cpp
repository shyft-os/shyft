#include <cmath>
#include <cstdlib>
#include <filesystem>
#include <memory>
#include <stdexcept>
#include <string>

#include <shyft/energy_market/stm/shop/shop_bundle_path.h>
#include <shyft/energy_market/stm/shop/shop_system.h>

namespace shyft::energy_market::stm::shop {

  shop_system::shop_system(time_axis::generic_dt const & ta)
    : api_handle{SHYFT_SHOP_LICENSEDIR}
    , adapter{api_handle, ta}
    , emitter{api_handle, adapter}
    , commander{api_handle}
    , time_axis{ta} {
    if (time_axis.size() == 0)
      throw std::runtime_error("time_axis.size() == 0");
    api_handle.set_time_axis(get_shop_axis(time_axis));

    if (std::getenv("SHYFT_SHOP_BUNDLE_DIR"))
      api_handle.set_library_path(std::getenv("SHYFT_SHOP_BUNDLE_DIR"));
    else if (SHYFT_SHOP_LIBDIR)
      api_handle.set_library_path(SHYFT_SHOP_LIBDIR);

    switch (to_seconds64(adapter.time_delay_unit)) {
    case 0:
      commander.set_time_delay_unit_time_step_length();
      break; // This is the default in Shop if we don't set anything
    case 60:
      commander.set_time_delay_unit_minute();
      break;
    case 3600:
      commander.set_time_delay_unit_hour();
      break;
    default:
      throw std::runtime_error("invalid time_delay_unit");
    }
  }

  void shop_system::emit(stm_system const & stm) {
    emitter.to_shop(stm);
  }

  void shop_system::collect(stm_system& stm) {
    emitter.from_shop(stm);
  }

  void shop_system::complete(stm_system& stm) {
    post_process(stm);
  }

  shop_global_settings shop_system::get_global_settings() const {
    return api_handle.global_settings();
  }

  shop_objective shop_system::get_average_objective() const {
    return api_handle.average_objective();
  }

  void shop_system::export_topology(bool all, bool raw, std::ostream& destination) const {
    shop_export::export_topology(api_handle.c.get(), all, raw, destination);
  }

  void shop_system::export_data(bool all, std::ostream& destination) const {
    shop_export::export_data(api_handle.c.get(), all, destination);
  }

  void shop_system::optimize(
    stm_system& stm,
    generic_dt const & time_axis,
    std::vector<shop_command> const & commands,
    bool logging_to_stdstreams,
    bool logging_to_files) {
    shop_system shop{time_axis};

    shop.set_logging_to_stdstreams(logging_to_stdstreams);
    shop.set_logging_to_files(logging_to_files);
    shop.emit(stm);
    for (auto const & command : commands)
      shop.commander.execute(command);
    shop.collect(stm);
    shop.complete(stm); // forced post-processing (could be optional/argument?)
  }

  void shop_system::environment(std::ostream& out) {
    for (char** the_environ = environ; *the_environ; the_environ++)
      out << *the_environ << std::endl;
  }

}
