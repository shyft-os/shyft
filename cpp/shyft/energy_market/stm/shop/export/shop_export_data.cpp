#include <cmath>
#include <cstring>
#include <memory>
#include <ostream>
#include <ranges>
#include <string>
#include <vector>

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

#include <shyft/energy_market/stm/shop/export/shop_export.h>

namespace shyft::energy_market::stm::shop {

  void shop_export::export_data(ShopSystem* api, bool all, std::ostream& out) {
    int const no = ShopGetObjectCount(api);
    auto double_or_nan = [](double const & v) {
      // Shop represents NaN values as 1e40 internally, and this will be returned when getting values, while it
      // accepts both std::nan and 1e40 as input both representing NaN.
      double const shop_nan = 1e40;
      if (!std::isfinite(v) || v == shop_nan) {
        return fmt::format("NaN");
      }
      return fmt::format("{}", v);
    };
    fmt::print(out, "{{\n");
    int const indent_size = 2;
    int n_indents = 1;
    auto ind = [&] {
      return std::string(indent_size * n_indents, ' ');
    };
    char t_start[18], t_end[18]; // YYYYMMDDhhmmssxxx0
    char t_unit[7];              // second0
    if (ShopGetTimeResolution(api, t_start, t_end, t_unit)) {
      fmt::print(out, "{}\"time_start\": \"{}\",\n", ind(), t_start);
      fmt::print(out, "{}\"time_end\": \"{}\",\n", ind(), t_end);
      fmt::print(out, "{}\"time_unit\": \"{}\",\n", ind(), t_unit);
    }
    if (int n; GetTimeResolutionDimensions(api, t_start, n)) {
      auto t = std::make_unique<int[]>(n);
      auto y = std::make_unique<double[]>(n);
      if (ShopGetTimeResolution(api, t_start, t_end, t_unit, n, t.get(), y.get())) {
        if (n > 0) {
          fmt::print(out, "{}\"time_resolution\": {{\n", ind());
          ++n_indents;
          fmt::print(out, "{}\"start\": \"{}\",\n", ind(), t_start);
          fmt::print(out, "{}\"end\": \"{}\",\n", ind(), t_end);
          fmt::print(out, "{}\"unit\": \"{}\",\n", ind(), t_unit);
          fmt::print(out, "{}\"entries\": [\n", ind());
          ++n_indents;
          auto points = std::views::transform(std::views::iota(0, n), [&](auto const i) {
            return fmt::format("{}{{ \"t\": {}, \"y\": {} }}", ind(), t[i], double_or_nan(y[i]));
          });
          fmt::print(out, "{}", fmt::join(points, ",\n"));
          --n_indents;
          fmt::print(out, "\n{}]\n", ind());
          --n_indents;
          fmt::print(out, "{}}},\n", ind());
        }
      }
    }
    fmt::print(out, "{}\"objects\": [\n", ind());
    ++n_indents;
    for (int oix = 0; oix < no; ++oix) {
      if (oix > 0)
        fmt::print(out, ",\n");
      fmt::print(out, "{}{{\n", ind());
      ++n_indents;
      auto const otype = ShopGetObjectType(api, oix);
      auto const oname = ShopGetObjectName(api, oix);
      fmt::print(out, "{}\"type\": \"{}\",\n", ind(), otype);
      fmt::print(out, "{}\"name\": \"{}\",\n", ind(), oname);
      fmt::print(out, "{}\"attributes\": [\n", ind());
      ++n_indents;
      int na = 128;   // Must be large enough to include all attributes for every type
      int alist[128]; // Array of size according to nAttributes
      size_t const max_type_len = 64;
      char otype_str[max_type_len]; // Must be large enough for longest type name
      *fmt::format_to_n(otype_str, sizeof(otype_str) - 1, "{}", otype).out = '\0';
      ShopGetObjectTypeAttributeIndices(api, otype_str, na, alist);
      int acount = 0;
      for (int ai = 0; ai < na; ++ai) {
        int aix = alist[ai];
        char aname[64];
        char afunc[64];
        char atype[64];
        char xunit[64];
        char yunit[64];
        bool aparam, ain, aout;
        ShopGetAttributeInfo(api, aix, otype_str, aparam, aname, afunc, atype, xunit, yunit, ain, aout);
        if (ShopCheckAttributeToGet(api, aix)) {
          if (
            all
            || ShopAttributeExists(
              api,
              oix,
              aix)) { // Filter on existing? Shop seem to report incorrect value in some cases (seen on load_penalty
                      // result series on market, ShopAttributeExists returns false even if it contains values)
            if (acount > 0)
              fmt::print(out, ",\n");
            fmt::print(out, "{}{{\n", ind());
            ++n_indents;
            fmt::print(out, "{}\"type\": \"{}\",\n", ind(), atype);
            fmt::print(out, "{}\"name\": \"{}\",\n", ind(), aname);
            fmt::print(out, "{}\"data_function\": \"{}\",\n", ind(), afunc);
            fmt::print(out, "{}\"unit_x\": \"{}\",\n", ind(), xunit);
            fmt::print(out, "{}\"unit_y\": \"{}\",\n", ind(), yunit);
            fmt::print(out, "{}\"is_object_parameter\": {},\n", ind(), aparam);
            fmt::print(out, "{}\"is_input\": {},\n", ind(), ain);
            fmt::print(out, "{}\"is_output\": {},\n", ind(), aout);
            if (all)
              fmt::print(out, "{}\"exists\": {},\n", ind(), ShopAttributeExists(api, oix, aix));
            fmt::print(out, "{}\"value\": ", ind());
            if (strcmp(atype, "double") == 0) {
              if (double v; ShopGetDoubleAttribute(api, oix, aix, v)) {
                fmt::print(out, "{}", double_or_nan(v));
              } else {
                fmt::print(out, "null");
              }
            } else if (strcmp(atype, "double_array") == 0) {
              fmt::print(out, "[");
              if (int n; ShopGetDoubleArrayLength(api, oix, aix, n) && n > 0)
                if (auto v = std::make_unique<double[]>(n); ShopGetDoubleArrayAttribute(api, oix, aix, n, v.get()))
                  fmt::print(
                    out,
                    "{}",
                    fmt::join(
                      std::views::transform(
                        std::views::iota(0, n),
                        [&](auto const i) {
                          return fmt::format(" {}", double_or_nan(v[i]));
                        }),
                      ","));
              fmt::print(out, "]");
            } else if (strcmp(atype, "int") == 0) {
              if (int v; ShopGetIntAttribute(api, oix, aix, v))
                fmt::print(out, "{}", v);
              else
                fmt::print(out, "null");
            } else if (strcmp(atype, "int_array") == 0) {
              fmt::print(out, "[");
              if (int n; ShopGetIntArrayLength(api, oix, aix, n) && n > 0)
                if (auto v = std::make_unique<int[]>(n); ShopGetIntArrayAttribute(api, oix, aix, n, v.get()))
                  fmt::print(
                    out,
                    "{}",
                    fmt::join(
                      std::views::transform(
                        std::views::iota(0, n),
                        [&](auto const i) {
                          return fmt::format(" {}", v[i]);
                        }),
                      ","));
              fmt::print(out, "]");
            } else if (strcmp(atype, "string") == 0) {
              if (std::string v; ShopGetStringAttribute(api, oix, aix, v))
                fmt::print(out, "\"{}\"", v);
              else
                fmt::print(out, "null");
            } else if (strcmp(atype, "txy") == 0) {
              char start_time[18];
              if (int n_scenarios, n;
                  ShopGetTxyAttributeDimensions(api, oix, aix, start_time, n, n_scenarios) && n_scenarios && n) {
                auto t = std::make_unique<int[]>(n);
                auto vs = std::make_unique<double*[]>(n_scenarios);
                std::vector<std::unique_ptr<double[]>> vsv(n_scenarios);
                for (int i = 0; i < n_scenarios; ++i) {
                  vsv[i] = std::make_unique<double[]>(n);
                  vs[i] = vsv[i].get();
                }
                if (ShopGetTxyAttribute(api, oix, aix, n, n_scenarios, t.get(), vs.get())) {
                  fmt::print(out, "{{\n");
                  ++n_indents;
                  fmt::print(out, "{}\"start\": \"{}\",\n", ind(), start_time);
                  fmt::print(out, "{}\"scenarios\": [\n", ind());
                  ++n_indents;
                  for (int i = 0; i < n_scenarios; ++i) {
                    if (i > 0)
                      fmt::print(out, ",\n");
                    fmt::print(out, "{}{{\n", ind());
                    ++n_indents;
                    fmt::print(out, "{}\"scenario\": {}, \n", ind(), i);
                    fmt::print(out, "{}\"entries\": [, \n", ind());
                    ++n_indents;
                    for (int j = 0; j < n; ++j) {
                      if (j > 0)
                        fmt::print(out, ",\n");
                      fmt::print(out, "{}{{ \"t\": {}, \"v\": {}}}", ind(), t[j], double_or_nan(vs[i][j]));
                    }
                    --n_indents;
                    fmt::print(out, "\n{}]\n", ind());
                    --n_indents;
                    fmt::print(out, "{}}}", ind());
                  }
                  --n_indents;
                  fmt::print(out, "{}]\n", ind());
                  --n_indents;
                  fmt::print(out, "{}}}", ind());
                } else {
                  fmt::print(out, "[]");
                }
              } else {
                fmt::print(out, "[]");
              }
            } else if (strcmp(atype, "xy") == 0) {
              if (int n; ShopGetXyAttributeNPoints(api, oix, aix, n) && n > 0) {
                auto x = std::make_unique<double[]>(n);
                auto y = std::make_unique<double[]>(n);
                double r;
                if (ShopGetXyAttribute(api, oix, aix, r, n, x.get(), y.get())) {
                  fmt::print(out, "{{\n");
                  ++n_indents;
                  fmt::print(out, "{}\"reference\": {},\n", ind(), r);
                  fmt::print(out, "{}\"entries\": [\n", ind());
                  ++n_indents;
                  for (int i = 0; i < n; ++i) {
                    if (i > 0)
                      fmt::print(out, ",\n");
                    fmt::print(out, "{}{{ \"x\": {}, \"y\": {} }}", ind(), x[i], double_or_nan(y[i]));
                  }
                  --n_indents;
                  fmt::print(out, "\n{}]\n", ind());
                  --n_indents;
                  fmt::print(out, "{}}}", ind());
                } else {
                  fmt::print(out, "null");
                }
              } else {
                fmt::print(out, "null");
              }
            } else if (strcmp(atype, "xy_array") == 0) {
              if (int n, np; ShopGetXyArrayDimensions(api, oix, aix, n, np) && n > 0 && np > 0) {
                auto r = std::make_unique<double[]>(n);
                auto p = std::make_unique<int[]>(n);
                auto x = std::make_unique<double*[]>(n);
                auto y = std::make_unique<double*[]>(n);
                std::vector<std::unique_ptr<double[]>> xv(n);
                std::vector<std::unique_ptr<double[]>> yv(n);
                for (int i = 0; i < n; ++i) {
                  xv[i] = std::make_unique<double[]>(np);
                  yv[i] = std::make_unique<double[]>(np);
                  x[i] = xv[i].get();
                  y[i] = yv[i].get();
                }
                if (ShopGetXyArrayAttribute(api, oix, aix, n, r.get(), p.get(), x.get(), y.get())) {
                  fmt::print(out, "[\n");
                  ++n_indents;
                  for (int i = 0; i < n; ++i) {
                    if (i > 0)
                      fmt::print(out, ",\n");
                    fmt::print(out, "{}{{\n", ind());
                    ++n_indents;
                    fmt::print(out, "{}\"reference\": {},\n", ind(), r[i]);
                    fmt::print(out, "{}\"entries\": [\n", ind());
                    ++n_indents;
                    for (int j = 0, n = p[i]; j < n; ++j) {
                      if (j > 0)
                        fmt::print(out, ",\n");
                      fmt::print(out, "{}{{ \"x\": {}, \"y\": {} }}\n", ind(), x[i][j], double_or_nan(y[i][j]));
                    }
                    --n_indents;
                    fmt::print(out, "\n{}]\n", ind());
                    --n_indents;
                    fmt::print(out, "{}}}", ind());
                  }
                  --n_indents;
                  fmt::print(out, "\n{}]", ind());
                } else {
                  fmt::print(out, "[]");
                }
              } else {
                fmt::print(out, "[]");
              }
            } else if (strcmp(atype, "xyt") == 0) {
              fmt::print(out, "[");
              if (int nt; ShopGetXytNTimes(api, oix, aix, nt) && nt > 0) {
                if (auto ta = std::make_unique<int[]>(nt); ShopGetXytTimesIntArray(api, oix, aix, nt, ta.get())) {
                  fmt::print(out, "\n");
                  ++n_indents;
                  for (int i = 0; i < nt; ++i) {
                    if (i > 0)
                      fmt::print(out, ",\n");
                    fmt::print(out, "{}{{", ind());
                    ++n_indents;
                    auto t = ta[i];
                    fmt::print(out, "{}\"t\": {},\n", ind(), t);
                    fmt::print(out, "{}\"entry\": {{", ind());
                    if (int np; ShopGetXytAttributeNPoints(api, oix, aix, np, t) && np > 0) {
                      ++n_indents;
                      double r;
                      auto x = std::make_unique<double[]>(np);
                      auto y = std::make_unique<double[]>(np);
                      if (ShopGetXytAttribute(api, oix, aix, r, np, x.get(), y.get(), t)) {
                        fmt::print(out, "{}\"reference\": {},\n", ind(), r);
                        fmt::print(out, "{}\"entries\": [\n", ind());
                        ++n_indents;
                        for (int j = 0; j < np; ++j) {
                          if (j > 0)
                            fmt::print(out, ",\n");
                          fmt::print(out, "{} \"x\": {}, \"y\": {} }}", ind(), x[j], double_or_nan(y[j]));
                        }
                        --n_indents;
                        fmt::print(out, "\n{}]\n", ind());
                      }
                      --n_indents;
                      fmt::print(out, "{}}}\n", ind());
                    }
                    fmt::print(out, "}}");
                    --n_indents;
                    fmt::print(out, "{}}}", ind());
                  }
                  --n_indents;
                  fmt::print(out, "\n{}", ind());
                }
              }
              fmt::print(out, "]");
            } else {
              fmt::print(out, "null");
            }
            fmt::print(out, "\n");
            --n_indents;
            fmt::print(out, "{}}}", ind());
            ++acount;
          }
        }
      }
      --n_indents;
      fmt::print(out, "\n{}]\n", ind());
      --n_indents;
      fmt::print(out, "{}}}", ind());
    }
    --n_indents;
    fmt::print(out, "{}]\n", ind());
    --n_indents;
    fmt::print(out, "{}}}\n", ind());
  }

}
