#include <shyft/energy_market/stm/catchment.h>

namespace shyft::energy_market::stm {
  void catchment::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels = -1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/C{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/C" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  bool catchment::operator==(catchment const & o) const {
    if (this == &o)
      return true;     // equal by addr.
    return super::operator==(o) && equal_component(*this, o);
  }
}
