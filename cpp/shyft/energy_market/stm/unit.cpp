#include <shyft/energy_market/stm/unit.h>

namespace shyft::energy_market::stm {

  namespace hana = boost::hana;
  namespace mp = shyft::mp;

  unit::unit(int id, string const & name, string const & json, stm_hps_ const & hps)
    : super(id, name, json, hps) {
    mk_url_fx(this);
  }

  unit::unit() {
    mk_url_fx(this);
  }

  void unit::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
    if (levels) {
      auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
      if (tmp)
        tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
    }
    if (!template_levels) {
      constexpr std::string_view a = "/U{o_id}";
      std::copy(std::begin(a), std::end(a), rbi);
    } else {
      auto idstr = "/U" + std::to_string(id);
      std::copy(std::begin(idstr), std::end(idstr), rbi);
    }
  }

  bool unit::operator==(unit const & other) const {
    if (this == &other)
      return true;     // equal by addr.
    return id_base::operator==(other) && equal_component(*this, other);
  }
}
