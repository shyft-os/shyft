/** This file is part of Shyft. Copyright 2015-2024 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <vector>

#include <shyft/hydrology/geo_point.h>

namespace shyft::core {

  /**
   * @brief epsg_map geo point
   * @details
   * Uses boost::geometry epsg capabilities to map between two epsg identifiers.
   *
   * @param a_ a geo_point
   * @param from_epsg  a valid boost geometry epsg id
   * @param to_epsg  a valid boost geometry epsg id
   * @return a mapped geo_point, the z values is same as source a_
   */
  [[nodiscard]] extern std::vector<geo_point> epsg_map(std::vector<geo_point> const &a_, int from_epsg, int to_epsg);

  /**
   * @brief proj4_map geo point
   * @details
   * Uses boost::geometry proj4 capabilities to map between two proj4 specification strings.
   *
   * @param a_ a geo_point
   * @param from_proj4  a valid boost geometry proj4 specification string
   * @param to_proj4  a valid boost geometry proj4 specification string
   * @return a proj4 geo_point, the z values is same as source a_
   */
  [[nodiscard]] extern geo_point proj4_map(geo_point const &a_, std::string_view from_proj4, std::string_view to_proj4);

  /**
   * @brief proj4_map geo points
   * @details
   * Uses boost::geometry proj4 capabilities to map between two proj4 specification strings.
   *
   * @param a_ a set of points
   * @param from_proj4  a valid boost geometry proj4 specification string
   * @param to_proj4  a valid boost geometry proj4 specification string
   * @return a proj4 mapped set of points, the z values is same as source a_
   */
  [[nodiscard]] extern std::vector<geo_point>
    proj4_map(std::vector<geo_point> const &a_, std::string_view from_proj4, std::string_view to_proj4);

  /**
   * @brief computes the buffer around the supplied points
   * @details
   * Uses boost::geometry::buffer algorithm to compute the cartesian buffer,
   * if it results in multiple polygons, that might happen if the distance specified is less than
   * the distance between the points, then boost::geometry::convex_hull is used to reduce the
   * polygons into a single polygon that have at least the distance specified as buffer.
   *
   * @param src a set of points
   * @param distance metric distance
   * @return a closed polygon that have at least the specified distance to the containing points
   */
  [[nodiscard]] extern std::vector<geo_point> polygon_buffer(std::vector<geo_point> const &src, double distance);

  /**
   * @brief computes the polygon horizontal area
   * @details
   * Uses boost::geometry::area, interpret passed argument as clockwise polygon,
   * @param polygon a set of points,clockwise, might be unclosed (will be fixed)
   * @return the area of the polygon
   */
  [[nodiscard]] extern double polygon_area(std::vector<geo_point> const &polygon);

  /**
   * @brief computes the polygon mid-point, centroid
   * @details
   * Uses boost::geometry::centroid, interpret passed argument as clockwise polygon,
   * @param polygon a set of points,clockwise, might be unclosed (will be fixed)
   * @return the area of the polygon
   */
  [[nodiscard]] extern geo_point polygon_mid_point(std::vector<geo_point> const &polygon);

  /**
   * @brief find the nearest neighbours
   *
   * @details
   * Given a set of points `points`, find the `n` closest neighbours to the point `p`.
   * The distance is computed as
   * distance[i]= sqrt( (p.x-points[i].x)**2 + (p.y-points[i].y)**2 + (z_scale*(p.z-points[i]))**2 )
   * Thus default value of z_scale=0.0 will disregard the vertical distance.
   *
   * @param points a set of points
   * @param p the point of interest
   * @param n number of neighbours to find
   * @param z_scale default 0.0, how to use the `z` when computing the distance
   * @return the nearest neighbour points
   */
  [[nodiscard]] extern std::vector<geo_point>
    nearest_neighbours(std::vector<geo_point> const &points, geo_point const &p, std::size_t n, double z_scale = 0.0);

  /**
   * @brief computes the polygon intersection areas
   * @details
   * Given a `polygon`, compute the intersection areas of `polygons`.
   * Use `n_neighbours_limit` to optimize the speed considering by default 8 neighbours,
   * suitable in a grid context.
   * @param polygons a set of polygons
   * @param polygon the polygon to compute intersection with
   * @return a vector with the intersection area in same order and number as the supplied polygons
   */
  [[nodiscard]] extern std::vector<double> polygon_intersection_areas(
    std::vector<std::vector<geo_point>> const &polygons,
    std::vector<geo_point> const &polygon,
    std::size_t n_neighbours_limit = 8);

  /**
   * @brief computes the polygon areas
   * @details
   * Given a list of `polygons`, compute and return the horizontal projected area of each of them.
   * @param polygons a set of polygons
   * @return the area of the polygons
   */
  [[nodiscard]] extern std::vector<double> polygons_area(std::vector<std::vector<geo_point>> const &polygons);

  /**
   * @brief computes the polygons mid_points
   * @details
   * Given a list of `polygons`, compute and return the mid_points/centroid for  each of them.
   * @param polygons a set of polygons
   * @return the mid_points,centroid, of the polygons
   */

  [[nodiscard]] extern std::vector<geo_point> polygons_mid_points(std::vector<std::vector<geo_point>> const &polygons);
}