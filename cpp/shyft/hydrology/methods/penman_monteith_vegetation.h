/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/hydro_functions.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/math_utilities.h>
#include <shyft/hydrology/vegetation_cell_data.h>
#include <shyft/hydrology/geo_cell_data.h>

namespace shyft::core::penman_monteith_vegetation {
    using namespace shyft::core::hydro_functions;

    /** @brief Evapotranspiration models
    *
     * Stewart-Penman-Mpnteith from S.L.Dingman p.280-282
    * Penman-Monteith,
    */

    struct parameter{
        double height_ws = 2.0; // [m], height of windspeed measurements
        double height_t = 2.0; // [m], height of temperature and relative humidity measurements
        parameter(double height_ws = 2.0, double height_t = 2.0) : height_ws(height_ws), height_t(height_t){}
        bool operator==(const parameter& x) const {
          return nan_equal(height_ws, x.height_ws) &&
                 nan_equal(height_t, x.height_t) ;
        }
        bool operator != (const parameter& x) const { return !operator==(x); }
        x_serialize_decl();
        
    };

    struct response {
        double et = 0.0; // reference evapotranspiration [mm/h]
        double soil_heat = 0.0;
    };


    struct calculator {
        parameter param;

        explicit calculator(const parameter &p) : param(p) {}

          /** @brief computes ASCE PENMAN-MONTEITH reference evapotranspiation, [mm/h] based on eq. B.1 from ASCE-EWRI
        * @param response -- updating response structure
        * @param sw_radiation, [MJ/m2/h] -- incoming shortwave radiation
        * @param lw_radiation, [MJ/m2/h] -- incoming longwave radiation
        * @param temperature [degC] -- air temperature
        * @param rhumidity [%] -- relative humidity
        * @param lai [-] -- leaf-area-index
        * @param albedo [-] -- albedo
        * @param hveg [m] -- vegetation height
        * @param cleaf_max [m/s] -- maximum leaf conductance
        * @param ks [-] -- shelter factor, recommended value 0.5
        * @param soil_moisture_deficit [cm] -- soil moisture deficit, default to 0 well-watered site
        * @param elevation [m]
        * @param windspeed [m/s at height_ws]
        */
        void evapotranspiration(response& response,  utctimespan dt, double swin_radiation, double lw_radiation, double tempmax,double tempmin, double rhumidity, double lai, double albedo, double hvegetation, double cleaf_max, double ks = 0.5, double soil_moisture_deficit = 0.0, double elevation=0.1, double windspeed=0.1)noexcept {

            double pressure = atm_pressure(elevation, 293.15);// recommended value for T0 during growing season is 20 gradC, see eq. B.8
            double temperature = 0.5*(tempmax+tempmin);
            double lambda_rho = vaporization_latent_heat(temperature)*rho_w;
            double delta = svp_slope(temperature);
            double avp = actual_vp(temperature, rhumidity); /// this function takes RH in %

            double net_radiation = swin_radiation*(1-albedo) + lw_radiation;
            double G = soil_heat_flux(net_radiation, lai);
            step_ = to_seconds(dt)/to_seconds(calendar::HOUR);
            G = G*(step_-24)/(-23.0);
            response.soil_heat = G;
            double Rn = 461.5; // Gas constant, [J/kg * K]
            double absolute_humidity_deficit = (svp(temperature) - avp)/Rn/(temperature+273.15);
            double sat_vp = svp_daily(tempmax,tempmin);
            double rs = 1/ canopy_conductance(swin_radiation, absolute_humidity_deficit, temperature,  soil_moisture_deficit, hvegetation, cleaf_max,lai, ks); // surface resistance
            double ra =   resistance_aerodynamic(param.height_ws,windspeed, param.height_t);
            double nominator = delta * (net_radiation - G) *step_ +
                                ktime*step_*density_air(pressure, temperature, avp)*cp/ra*(sat_vp-avp);
            double denominator = delta + gamma_pm(pressure, temperature)*(1+rs/ra);
            //std::cout<<" PM nominator_1: "<<  delta * (net_radiation - G) *step_ << std::endl;
            //std::cout<<" PM nominator_2: "<<  ktime*step_*density_air(pressure, temperature, avp)*cp/resistance_aerodynamic(param.height_ws,windspeed, param.height_t)*(sat_vp-avp) << std::endl;
            response.et= nominator/denominator/lambda_rho;
        }




    private:
        // stewart model of leaf conductance
        double step_= 1;
        double f_k_{0.1}; // factor controlling stomatal opening: Light
        double f_rho_{0.1};// vapor-pressure deficit
        double f_T_{0.1}; // leaf temperature
        double f_theta_{0.1};// leaf water content
        double c_leaf_{0.1};// leaf conductance, where Cmax_leaf -- maximum value of leaf conductunce
        double lai_{2.0};
        double sai_{2.0};
        double albedo_{0.062};
        double zd_{0.1}; // zero-plane dispacement height
        double zoh_{0.1}; // roughness height for transfer of heat and vapor, [m]
        double zom_{0.1}; // roughness length governing momentum transfer, [m]
        // constants for asce penman-monteith method
        const double rho_w = 1;//[Mg/m^3]
        const double cp = 1.013*0.001; // specific heat of moist air, [MJ/kg/gradC]
    //    const double cp = 1.013;
        const double ktime = 3600; // unit conversion for ET in mm/h
        //const double ktime = 1;//3600; // unit conversion for MJ to W/s
//        double d =  0.67 *height_veg; // zero displacement height, [m]
//        double zom = 0.123*height_veg; // roughness length governing momentum transfer, [m]
//        double zoh =  0.0123*height_veg;// roughness height for transfer of heat and vapor, [m]
        double kappa = 0.41; // von Karman's constant

        /** @brief aerodynamic resistance, [s/m], eq. B.2
        * @param hws -- wind speed measurements height, [m]
        * @param ws -- wind speed at height hws, [m/s]
        * @param ht [m] -- height of humidity and or temperature measurements
        */
        double resistance_aerodynamic(double hws, double ws, double ht=2.0) const noexcept {
            using std::log;
            return log((hws - zd_)/zom_)*log((ht-zd_)/zoh_)/(kappa*kappa*std::max(ws,0.01));
        }

        /** @brief canopy conductance, [s/m], eq. 6.55, p.281
        * @param Kin -- incoming shortwave radiation, MJ/m^2*hr
         * @param delta_rho -- vapor presssure deficit
         * @param Tzm -- leaf temparature (air temperature at zm)
         * @param delta_theta -- soil moisture deficit
         * @param cleaf_max -- maximum value of leaf conductance
         * @param lai -- leaf-area index
         * @param ks -- shelter factor [0..1], recommended 0.5
         * @return canopy conductance
        */

        double canopy_conductance(double Kin, double delta_rho, double Tzm, double delta_theta, double hvegetation,  double cleaf_max, double lai,  double ks=0.5) noexcept{
            // representative height
            zd_ = 0.7*hvegetation;
            zoh_ = 0.1*hvegetation;
            zom_ = 0.123*hvegetation;
            //Stewart's (1988) Model of Leaf Conductance as a Function of Environmental Foctors, Table 6.5 from L.Dingman
            f_k_ = 12.78*Kin*24/(11.57*Kin*24+104.4);//*step_; // incident solar radiation [MJ /m^2/day] , 0<=Kin<=86.5
            f_rho_ = 1-66.6*delta_rho; // vapor-pressure deficit delta_rho in [kg/m^3], 0<=delta_rho<=0.01152, if delta_rho>=0.01152 f_rho = 0.233
            f_T_ = Tzm*std::pow((40-Tzm),1.18)/691; // Tzm air temperature at zm [degC]
            f_theta_ = 1 - 0.00119*std::exp(0.81*delta_theta); // delta_theta -- soil-moisture deficit, [cm], 0<=delta_theta<=8.4
            c_leaf_ = cleaf_max*f_k_*f_rho_*f_T_*f_theta_;
            return c_leaf_*lai*ks;
        }


        /// TODO: move all to the hydro_functions???

        /** @brief Latent heat of vaporization, eq. B.7; ASCE-EWRI
        * @param temperature [degC]
        */
        double vaporization_latent_heat(double temperature = 20.0) const noexcept {
            return 2.501 - (2.361*0.001)*temperature;//with default value gives 2.45;
        }


        /** @brief atmospheric air density, ASCE-EWRI, eq. B.10
        * @param pressure [kPa]
        * @param temperature [degC]
        * @param actual vapor pressure, [kPa]
        */
        double density_air(double pressure, double temperature, double ea) const noexcept {
            double Tk = 273.16+temperature;
            double Tkv = Tk/(1-0.378*ea/pressure); // B.11
            return 3.486*pressure/Tkv; // B.10
        }

        /** @brief psychrometric constant, [kPa/gradC]
        * @param atmospheric pressure, [kPa]
        * @param air temperature, [degC]
        */
        double gamma_pm(double pressure, double temperature=20.0) const noexcept {

            double lambda = vaporization_latent_heat(temperature);
            const double epsilon = 0.622; // ratio of molecular weight of water vapor/dry air
            return cp*pressure/epsilon/lambda;
        }

        /** @brief soil heat flux density (G) for hourly periods, eq.B.13
        * @param net_radiation should be either MJ/m2/h or Wt/m2
        */
        double soil_heat_flux(double net_radiation, double lai) const noexcept {
            double kg = 0.4;
            if (net_radiation <= 0.0)
                kg = 2.0;
            return kg*exp(-0.5*lai)*net_radiation; // Kg exp(-0.5LAI)Rnet, 0.5LAI = lai_active
        }

        /** @brief wind speed adjustment for measurement height, eq.B.14*/
        double ws_adjustment(double height_measure, double ws_measure) const noexcept {
            using std::log;
            return ws_measure*log((2-zd_)/zom_)/log((height_measure-zd_)/zom_);
        }

    };
}
x_serialize_export_key(shyft::core::penman_monteith_vegetation::parameter);
