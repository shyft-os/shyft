/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/hydro_functions.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/math_utilities.h>
#include <shyft/hydrology/vegetation_cell_data.h>
#include <shyft/hydrology/geo_cell_data.h>

namespace shyft::core::free_water_evaporation {
    using namespace shyft::core::hydro_functions;

    /** @brief free water Evapotranspiration model
    *
     * Penman or Combination Method from S.L.Dingman p.263
    */

    struct response {
        double et = 0.0; // reference evapotranspiration [mm/h]
    };

    struct calculator {
        explicit calculator()  {}

          /** @brief computes open water  evapotranspiation, [mm/h] based on eq. 6.36a from Dingman book "Physical hydrology" (Penman or combined method)
        * @param response -- updating response structure
        * @param sw_radiation, [MJ/m2/h] -- incoming shortwave radiation
        * @param lw_radiation, [MJ/m2/h] -- incoming longwave radiation
        * @param temperature [degC] -- air temperature
        * @param rhumidity [-] -- relative humidity
        * @param hveg [m] -- vegetation height
        * @param cleaf_max [m/s] -- maximum leaf conductance
        * @param lai [-] -- leaf-area-index
        * @param ks [-] -- shelter factor, recommended value 0.5
        * @param soil_moisture_deficit [cm] -- soil moisture deficit, default to 0 well-watered site
        * @param elevation [m]
        * @param windspeed [m/s at height_ws]
        */
        void evaporation(response& response,  utctimespan dt, double swin_radiation, double lw_radiation, double tempmax,double tempmin, double rhumidity, double lake_area, double albedo,  double elevation=0.1, double windspeed=0.1)noexcept {

            double pressure = atm_pressure(elevation, 293.15);// recommended value for T0 during growing season is 20 gradC, see eq. B.8
            double temperature = 0.5*(tempmax+tempmin);
            double lambda_rho = vaporization_latent_heat(temperature)*rho_w;
            double delta = svp_slope(temperature);
            double avp = actual_vp(temperature, rhumidity*100);//this function takes RH in %

            double net_radiation = swin_radiation*(1-albedo) + lw_radiation;
            step_ = to_seconds(dt)/to_seconds(calendar::HOUR);
            double sat_vp = svp_daily(tempmax,tempmin);
            double Ke = 1.26*std::pow(lake_area, -0.05); // lake area is km^2!!
            double nominator_ow = delta * (net_radiation) *step_ +
                                ktime*step_*lambda_rho*Ke*windspeed*(sat_vp-avp);
            double denominator_ow = delta + gamma_pm(pressure, temperature);

            double open_water_et = nominator_ow/denominator_ow/lambda_rho;
            double et = open_water_et;
            response.et = et;
        }




    private:
        double step_= 1;
        const double rho_w = 1;//[Mg/m^3]
        const double cp = 1.013*0.001; // specific heat of moist air, [MJ/kg/gradC]
    //    const double cp = 1.013;
    //    const double ktime = 3600; // unit conversion for ET in mm/h
        const double ktime = 1/24;//3600; // unit conversion for MJ to W/s


        /// TODO: move all to the hydro_functions???

        /** @brief Latent heat of vaporization, [MJ/kg] eq. B.7; ASCE-EWRI
        * @param temperature [degC]
        */
        double vaporization_latent_heat(double temperature = 20.0) const noexcept {
            return 2.501 - (2.361*0.001)*temperature;//with default value gives 2.45;
        }


        /** @brief atmospheric air density, ASCE-EWRI, eq. B.10
        * @param pressure [kPa]
        * @param temperature [degC]
        * @param actual vapor pressure, [kPa]
        */
        double density_air(double pressure, double temperature, double ea) const noexcept {
            double Tk = 273.16+temperature;
            double Tkv = Tk/(1-0.378*ea/pressure); // B.11
            return 3.486*pressure/Tkv; // B.10
        }

        /** @brief psychrometric constant, [kPa/gradC]
        * @param atmospheric pressure, [kPa]
        * @param air temperature, [degC]
        */
        double gamma_pm(double pressure, double temperature=20.0) const noexcept {

            double lambda = vaporization_latent_heat(temperature);
            const double epsilon = 0.622; // ratio of molecular weight of water vapor/dry air
            return cp*pressure/epsilon/lambda;
        }



    };
}

