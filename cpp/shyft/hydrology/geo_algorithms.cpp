/** This file is part of Shyft. Copyright 2015-2024 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <algorithm>
#include <ranges>

#include <boost/geometry.hpp>
#include <boost/geometry/algorithms/buffer.hpp>
#include <boost/geometry/algorithms/distance.hpp>
#include <boost/geometry/core/cs.hpp>
#include <boost/geometry/geometries/multi_point.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/srs/transformation.hpp>
#include <boost/geometry/strategies/agnostic/buffer_distance_symmetric.hpp>

#include <shyft/core/epsg_fast.h>
#include <shyft/hydrology/geo_point.h>

namespace shyft::core {
  namespace bg = boost::geometry;
  using point_ll =
    bg::model::point<double, 2, bg::cs::geographic<bg::degree> >; ///< point (latitude,longitude),  epsg 4326 ,wgs 84
  using point_xy =
    bg::model::point<double, 2, bg::cs::cartesian>; /// < x and y of geo_point, in some metric epsg zone, utm.. etc.
  using polygon_type = bg::model::polygon<point_xy>;

  namespace detail {
    template <class pxy>
    pxy epsg_map(pxy a, int from_epsg, int to_epsg) {
      if (from_epsg == to_epsg) {
        return a;
      } else {
        bg::srs::projection<> a_prj{bg::srs::epsg_fast{from_epsg}};
        point_ll a_ll;
        a_prj.inverse(a, a_ll);
        bg::srs::projection<> r_prj{bg::srs::epsg_fast{to_epsg}};
        pxy r;
        r_prj.forward(a_ll, r);
        return r;
      }
    }

    extern template point_xy epsg_map<point_xy>(point_xy, int from_epsg, int to_epsg);

    static geo_point_ll impl_to_longitude_latitude(geo_point const &p, int geo_epsg_id) {
      point_xy p_xy(p.x, p.y);
      point_ll p_ll;
      bg::srs::epsg_fast e{geo_epsg_id};
      bg::srs::projection<> prj{e};
      prj.inverse(p_xy, p_ll);
      return geo_point_ll{bg::get<0>(p_ll), bg::get<1>(p_ll)};
    }

    template point_xy
      epsg_map<point_xy>(point_xy, int from_epsg, int to_epsg); // instantiate the extern decl in header here.

  }

  geo_point_ll convert_to_longitude_latitude(geo_point const &p, int geo_epsg_id) {
    return detail::impl_to_longitude_latitude(p, geo_epsg_id);
  }

  namespace {
    auto proj4_transform(std::string_view from, std::string_view to) {
      return bg::srs::transformation<>{bg::srs::proj4(std::string(from)), bg::srs::proj4(std::string(to))};
    }

    bool is_cartesian(std::string_view proj4) {
      // just speculating here: usually we are utm, cartesian, sometimes longlat)
      return proj4.contains("+proj=utm") || !(proj4.contains("+proj=longlat") || proj4.contains("+proj=latlong"));
    }

    template <typename S, typename D, typename T>
    geo_point proj4_project(T const &xform, geo_point const &p) {
      S s{p.x, p.y};
      D d;
      xform.forward(s, d);
      return {bg::get<0>(d), bg::get<1>(d), p.z};
    }

    template <typename S, typename D, typename T>
    std::vector<geo_point> proj4_project(T const &xform, std::vector<geo_point> const &a_) {
      auto r = a_ | std::views::transform([&](auto const &p) {
                 S src_point{p.x, p.y};
                 D dst_point{};
                 xform.forward(src_point, dst_point);
                 return geo_point{bg::get<0>(dst_point), bg::get<1>(dst_point), p.z};
               });
      return {std::begin(r), std::end(r)};
    }

    template <class T>
    T proj4_multi_map(T const &a_, std::string_view from_proj4, std::string_view to_proj4) {
      if (from_proj4 == to_proj4)
        return a_;
      auto xform = proj4_transform(from_proj4, to_proj4);
      if (is_cartesian(from_proj4)) {
        if (is_cartesian(to_proj4))
          return proj4_project<point_xy, point_xy>(xform, a_);
        else
          return proj4_project<point_xy, point_ll>(xform, a_);
      } else {
        if (is_cartesian(to_proj4))
          return proj4_project<point_ll, point_xy>(xform, a_);
        else
          return proj4_project<point_ll, point_ll>(xform, a_);
      }
    }
  }

  geo_point epsg_map(geo_point const &a_, int from_epsg, int to_epsg) {
    if (from_epsg == to_epsg) {
      return a_;
    } else {
      auto a = detail::epsg_map(point_xy(a_.x, a_.y), from_epsg, to_epsg);
      return {bg::get<0>(a), bg::get<1>(a), a_.z};
    }
  }

  std::vector<geo_point> epsg_map(std::vector<geo_point> const &a_, int from_epsg, int to_epsg) {
    if (from_epsg == to_epsg)
      return a_;
    auto r = a_ | std::views::transform([&](auto const &p) {
               auto a = detail::epsg_map(point_xy(p.x, p.y), from_epsg, to_epsg);
               return geo_point{bg::get<0>(a), bg::get<1>(a), p.z};
             });
    return {std::begin(r), std::end(r)};
  }

  geo_point proj4_map(geo_point const &a_, std::string_view from_proj4, std::string_view to_proj4) {
    return proj4_multi_map(a_, from_proj4, to_proj4);
  }

  std::vector<geo_point>
    proj4_map(std::vector<geo_point> const &a_, std::string_view from_proj4, std::string_view to_proj4) {
    return proj4_multi_map(a_, from_proj4, to_proj4);
  }

  namespace detail {

    std::vector<geo_point> polygon_buffer(std::vector<geo_point> const &src, double distance) {
      if (src.empty())
        return {};
      using multipoint_xy = bg::model::multi_point<point_xy>;
      using polygon_xy = bg::model::polygon<point_xy>;
      multipoint_xy src_xy;
      src_xy.reserve(src.size());
      for (auto &p : src)
        src_xy.emplace_back(p.x, p.y);
      bg::strategy::buffer::distance_symmetric<double> distance_strategy(distance);
      bg::strategy::buffer::join_miter join_strategy(distance); // avoid long miters for sharp corners
      bg::strategy::buffer::end_round end_strategy(6);          // a coarse circle,
      bg::strategy::buffer::point_square point_strategy;        // make squares around points, suites well
      bg::strategy::buffer::side_straight side_strategy;        // same for the sides
      bg::model::multi_polygon<polygon_xy> mp_result;
      std::vector<geo_point> rr;
      if (src.size() < 50) { // turns out buffer is incredibly slow for large distance and even few points
        // The number is selected around a range where it does not take to long time (sub second) worst case
        //  we only use it for few points, where it solves another problem of convex hull only valid for
        // some more points forming an area
        bg::buffer(src_xy, mp_result, distance_strategy, side_strategy, join_strategy, end_strategy, point_strategy);
      } else {
        polygon_xy conv_hull; // first make convex hull, that reduces number of points
        bg::convex_hull(src_xy, conv_hull);
        // then apply buffer around the conv hull
        bg::buffer(conv_hull, mp_result, distance_strategy, side_strategy, join_strategy, end_strategy, point_strategy);
      }
      // compute the average z, and use that for the buffer z
      double z = 0.0;
      for (auto const &p : src)
        z += p.z;
      z /= double(src.size());
      if (mp_result.size() > 1) { // possible, but we merge them using convex-hull
        polygon_xy polygon_result;
        bg::convex_hull(mp_result, polygon_result);
        rr.reserve(polygon_result.outer().size());
        for (auto const &p_xy : polygon_result.outer())
          rr.emplace_back(bg::get<0>(p_xy), bg::get<1>(p_xy), z);
      } else { // a single precise polygon
        rr.reserve(mp_result.front().outer().size());
        for (auto const &p_xy : mp_result.front().outer())
          rr.emplace_back(bg::get<0>(p_xy), bg::get<1>(p_xy), z);
      }
      return rr;
    }
  }

  std::vector<geo_point> polygon_buffer(std::vector<geo_point> const &src, double distance) {
    return detail::polygon_buffer(src, distance);
  }

  namespace {
    polygon_type bg_polygon(std::vector<geo_point> const &polygon) {
      namespace bg = boost::geometry;
      using shyft::core::point_xy;
      using polygon_type = bg::model::polygon<point_xy>;
      polygon_type poly;
      for (auto const &p : polygon)
        poly.outer().push_back(point_xy{p.x, p.y});
      if (polygon.front() != polygon.back())
        poly.outer().push_back(point_xy(polygon.front().x, polygon.front().y));
      return poly;
    }
  }

  double polygon_area(std::vector<geo_point> const &polygon) {
    if (polygon.size() < 3)
      return std::numeric_limits<double>::quiet_NaN();
    return bg::area(bg_polygon(polygon));
  }

  geo_point polygon_mid_point(std::vector<geo_point> const &polygon) {
    if (polygon.empty())
      return {
        std::numeric_limits<double>::quiet_NaN(),
        std::numeric_limits<double>::quiet_NaN(),
        std::numeric_limits<double>::quiet_NaN()};
    if (polygon.size() == 1)
      return polygon.front();
    point_xy mid_point{};
    bg::centroid(bg_polygon(polygon), mid_point);
    auto n = polygon.size();
    if (polygon.front() == polygon.back())
      --n;
    double z{0};
    for (auto i = 0u; i < n; ++i)
      z += polygon[i].z;

    return {bg::get<0>(mid_point), bg::get<1>(mid_point), z / double(n)};
  }

  std::vector<geo_point>
    nearest_neighbours(std::vector<geo_point> const &points, geo_point const &p, std::size_t n, double z_scale) {
    using point_index_value = std::pair<std::size_t, double>;
    if (n == 0 || points.empty())
      return {};
    std::vector<point_index_value> distances;
    distances.reserve(points.size());
    for (std::size_t i = 0; i < points.size(); ++i) {
      double distance = geo_point::zscaled_distance(p, points[i], z_scale);
      distances.emplace_back(i, distance);
    }
    auto nd = std::min(distances.size(), n);
    std::partial_sort(
      distances.begin(), distances.begin() + nd, distances.end(), [](point_index_value &a, point_index_value &b) {
        return a.second < b.second;
      });
    std::vector<geo_point> result;
    result.reserve(nd);
    for (std::size_t i = 0; i < nd; ++i) {
      result.push_back(points[distances[i].first]);
    }
    return result;
  }

  std::vector<double> polygon_intersection_areas(
    std::vector<std::vector<geo_point>> const &polygons,
    std::vector<geo_point> const &polygon,
    std::size_t n_neighbours_limit) {
    // first n nearest neighbours,
    // then intersection of those
    if (polygon.size() < 3 || polygons.empty())
      return {};
    using point_index_value = std::pair<std::size_t, double>;
    std::vector<geo_point> points;
    for (auto const &px : polygons)
      points.push_back(polygon_mid_point(px));
    auto p = polygon_mid_point(polygon);

    std::vector<point_index_value> distances;
    distances.reserve(points.size());
    for (std::size_t i = 0; i < points.size(); ++i) {
      double distance = geo_point::xy_distance(p, points[i]);
      distances.emplace_back(i, distance);
    }
    auto n = std::min(distances.size(), std::size_t(n_neighbours_limit));
    std::partial_sort(
      distances.begin(), distances.begin() + n, distances.end(), [](point_index_value &a, point_index_value &b) {
        return a.second < b.second;
      });
    // the n pairs ix,distance, use bg::intersection()..
    std::vector<double> intersection_areas(polygons.size(), 0.0);
    auto target_polygon = bg_polygon(polygon);
    for (std::size_t i = 0; i < n; ++i) {
      std::vector<polygon_type> bg_intersections;
      bg::intersection(bg_polygon(polygons[distances[i].first]), target_polygon, bg_intersections);
      double intersection_area = 0.0;
      for (auto const &intersection : bg_intersections)
        intersection_area += bg::area(intersection);
      intersection_areas[distances[i].first] = intersection_area;
    }
    return intersection_areas;
  }

  std::vector<double> polygons_area(std::vector<std::vector<geo_point>> const &polygons) {
    std::vector<double> r;
    r.reserve(polygons.size());
    std::transform(polygons.begin(), polygons.end(), std::back_inserter(r), [](std::vector<geo_point> const &polygon) {
      return bg::area(bg_polygon(polygon));
    });
    return r;
  }

  std::vector<geo_point> polygons_mid_points(std::vector<std::vector<geo_point>> const &polygons) {
    std::vector<geo_point> r;
    r.reserve(polygons.size());
    for (auto const &px : polygons)
      r.push_back(polygon_mid_point(px));
    return r;
  }
}
