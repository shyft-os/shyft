/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <boost/math/constants/constants.hpp>

namespace shyft::core::hydro_functions {
  using std::pow;
  using std::exp;
  using std::log;

  constexpr double const pi = boost::math::constants::pi<double>(); //=> shyft::core::pi defined in geo_cell_data.h
  constexpr double const sigma = 4.90 * 0.000000001;                // Stefan-Boltzmann constant  MJ/m^2 K^4
  constexpr double const gsc = 1367;                                // W/m2 -- solar constant
  constexpr double const Pa2kPa = 0.001;
  constexpr double const deg2rad = pi / 180;   // degrees to radians
  constexpr double const rad2deg = 180 / pi;   // rad to deg
  constexpr double const MJm2d2Wm2 = 0.086400; // MJ/m^2/day to W/m^2
  constexpr double const MJm2h2Wm2 = 0.0036;   // MJ/m^2/hour to W/m^2
  constexpr double const lambda = 2.45;        // latent heat of vaporization, [MJ/kg]

  // constexpr double const inverse_lambda = 0.408; // inverse lambda, [kg/MJ]
  // constexpr double const rhow = 1000;            // water density, [kg/m^3]

  /**
   * @brief computes standard atmospheric pressure
   * @param height, [m] -- elevation of the point
   * @return p, [kPa] -- atmospheric pressure
   */
  inline double atm_pressure(double height, double T0 = 288.15) noexcept { // height < 11000
    constexpr double const p0 = 101325.0;                                  //[Pa]standard sea level pressure
    constexpr double const L = 0.0065;                                     //[K/m] temperature lapse rate
    constexpr double const g = 9.80665;   //[m/s2] earth-surface gravitational acceleration
    constexpr double const R0 = 8.31447;  //[J/mol/K] Universal gas constant
    constexpr double const M = 0.0289644; //[kg/mol] molar mass of dry air
    // const double T0 = 288.15; // [K] sea level standard temperature
    return p0 * pow((1 - L * height / T0), (g * M / R0 / L)) * Pa2kPa;
  }

  /**
   * @brief computes actual vapor pressure from dewpoint temperature
   *
   * @ref  Lawrence Dingman Physical Hydrology, Third Edition, 2015, p.113
   * @param temperature, [degC]
   * @param rhumidity, [percent] -- relative humidity
   * @return e, [kPa] -- actual vapor pressure
   *
   */
  inline double actual_vp(double temperature, double rhumidity) noexcept {
    double es =
      (temperature >= 0.0)
        ? (0.6108 * exp(17.27 * temperature / (temperature + 237.3)))
        : 0.6108 * exp(21.87 * temperature / (temperature + 265.5)); // saturation vapor pressure,[kPa], eq.(3.9)
    return rhumidity / 100.0 * es;                                   //[kPa], based on eq.(3.12)
  }

  // the whole section is based on supplementary materials of
  // McMahon, T. A., Peel, M. C., Lowe, L., Srikanthan, R., and McVicar, T. R.:
  // Estimating actual, potential, reference crop and pan evaporation using standard meteorological data: a pragmatic
  // synthesis, Hydrol. Earth Syst. Sci., 17, 1331-1363, https://doi.org/10.5194/hess-17-1331-2013, 2013.
  // TODO: check units

  /**
   * @brief wet-bulb temperature  McJannet et al., 2008b, Equation 2
   */
  inline double wetbulb_temperature(double air_temp, double dew_temp, double actual_vp) noexcept {
    double tt = (dew_temp + 237.3) * (dew_temp + 237.3);
    double a = 0.00066 * 100 * air_temp + 4098 * actual_vp * dew_temp / tt;
    double b = 0.00066 * 100 + 4098 * actual_vp / tt;
    return a / b;
  }

  /** @brief dew point temperature    McJannet et al., 2008b, Equation 26*/
  inline double dew_temperature(double act_vp) noexcept {
    using std::log;
    return 116.9 + 237.3 * log(act_vp) / (16.78 - log(act_vp));
  }

  /** @brief slope of the saturation vapor pressure curve  Allen et al., 1998, Equation 13 */
  inline double svp_slope(double air_temp) noexcept {
    return 4098 * (0.6108 * exp(17.27 * air_temp / (237.3 + air_temp))) / (air_temp + 237.3) / (air_temp + 237.3);
  }

  /** @brief saturation vapor pressure at temperature  Allen et al., 1998, Equation 11 */
  inline double svp(double temperature) noexcept {
    return (temperature >= 0.0)
           ? (0.6108 * exp(17.27 * temperature / (temperature + 237.3)))
           : 0.6108 * exp(21.87 * temperature / (temperature + 265.5));
  }

  /** @brief daily saturation vapor pressure  Allen et al., 1998, Equation 12 */
  inline double svp_daily(double tmax, double tmin) noexcept {
    return 0.5 * (svp(tmax) + svp(tmin));
  }

  /** @brief mean daily actual vapor pressure  Allen et al., 1998, Equation 17 */
  inline double avp_daily_mean(double tmax, double tmin, double rhmax, double rhmin) noexcept {
    return 0.5 * (svp(tmax) * rhmax / 100 + svp(tmin) * rhmin / 100);
  }

  /** @brief mean daily actual vapor pressure using dew point temperature */
  inline double avp_daily_mean(double dew_temp) noexcept {
    return svp(dew_temp);
  }

  /** @brief psychrometric constant Allen et al., 1998, Equation 8 */
  inline double gamma(double pressure, double lambda = 2.45) noexcept {
    return 0.00163 * pressure / lambda;
  }


  /**
   * @brief smooth_0_linear
   * Utilizes log(1.0+exp(-(t-tx)/m))) to create a smooth curve that
   * for t>>tx is close to (t-tx)
   * and for t~tx is approaching 0.0 smoothly
   * suitable for day-degree pot melt, refreeze in hydrology,
   * @see Kavetski, D., and G. Kuczera (2007), Model smoothing strategies to remove microscale
   *   discontinuities and spurious secondary optima in objective functions in hydrological
   *  calibration, Water Resour. Res., 43, W03411, doi:10.1029/2006WR005195.
   *
   * @param t temperature degC (range is -50..+50 in real life)
   * @param tx threshold temperature for pot-melt,refreeze
   * @param m the range for smoothing typical 0.5 gives suitable range
   * @param tol the cut off for zero smoothing (gives exact t-tx result)
   * @return smoothed (t-tx) for t>>tx, and smoothing to zero for t<=tx
   *
   */
  [[nodiscard]] inline double smooth_0_linear(double t, double tx, double m, double tol) noexcept {
    double t_m = (t - tx) / m;
    double sm = std::log(1.0 + exp(-t_m));
    return m * (t_m + (sm > tol ? sm : 0.0));
  }

  /**
   * @brief smooth_snow_fraction
   * Computes a smoothed snow fraction,[0.0..1.0] given temperature, the threshold tx, and
   * smoothing parameters.
   * @see Kavetski, D., and G. Kuczera (2007), Model smoothing strategies to remove microscale
   *   discontinuities and spurious secondary optima in objective functions in hydrological
   *  calibration, Water Resour. Res., 43, W03411, doi:10.1029/2006WR005195.
   *
   * @param t observed temperature in degC
   * @param tx the threshold in degC, for liquid precipitation values below tx yields snow (solid phase)
   * @param m_phase 0.1..0.9, the range of the smoothing region, approx +- 2x m_scale, so a value of 0.5 gives ~ +-1.0
   * degC
   * @param tol the tolerance for clipping the fraction to 1.0 and 0.0 respectively
   * @return the computed smooth_snow_fraction in range 0.0 (all liquid) to 1.0 (all snow)
   */
  [[nodiscard]] inline double smooth_snow_fraction(double t, double tx, double m_phase, double tol = 1.0e-7) noexcept {
    double fraction = 1.0 / (1.0 + exp(-(tx - t) / m_phase));
    if (fraction < tol)
      return 0.0;
    if (fraction > (1.0 - tol))
      return 1.0;
    return fraction;
  }
}
