/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <stdexcept>

#include <boost/math/constants/constants.hpp>
#include <shyft/core/core_serialization.h>
#include <boost/serialization/version.hpp>
#include <shyft/hydrology/geo_point.h>
#include <shyft/time_series/accessor.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/dd/apoint_ts.h>


namespace shyft::core {

  using std::shared_ptr;
  using std::make_shared;
  using namespace shyft::time_series;
  using namespace shyft;
  using std::map;

  using cid_t = int64_t;
  using cids_t = std::vector<cid_t>;
  // and typedefs for commonly used types in the model
  // it is probably better to use  this one shyft::time_axis::point_dt
  // using pts_t= point_ts<time_axis::fixed_dt>;
  using shyft::time_series::dd::apoint_ts;
  using timeaxis_t = time_axis::fixed_dt;


  /**
   *  @brief vegetation_type is a type related to behavior
   *
   * @details
   * WIP:
   * During construction of geo-cells, the gis-system properties should be mapped
   * to those entities that creates the wanted behavior.
   * TBD: discuss the possibilities of implementing forest types following
   * Majasalmi, T., Eisner, S., Astrup, R., Fridman, J., and Bright, R. M.: An enhanced forest classification scheme
   * for modeling vegetation–climate interactions based on national forest inventory data, Biogeosciences, 15, 399–412,
   * https://doi.org/10.5194/bg-15-399-2018, 2018. This one is used in current HBV from NVE.
   *  forest_tne= temperate needleleaf evergreen 0
   *  forest_bne = boreal needleleaf evergreen 1
   *  fores_bnd = boreal needleleaf deciduous 2
   *  forest_trbe = tropical broadleaf evergreen 3
   *  forest_tbe = temperate broadleaf evergreen 4
   *  forest_trbd = tropical broadleaf deciduous 5
   *  forest_tbd = temperate broadleaf deciduous 6
   *  forest_bbd = boreal broadleaf deciduous 7
   *  shrub_tbe = temperate broadleaf evergreen 8
   *  shrub_tbd = temperate broadleaf deciduous 9
   *  shrub_bbd = boreal broadleaf deciduous 10
   *  grass_arctic 11
   *  grass 12
   *  cropland 13
   *  E.g: land_type  class should match the representation from the source, for example PLT (plant-functional types) in
   * MODIS, or whatever in LANDSAT
   */
  enum struct vegetation_type {
    forest_tne,
    forest_bne,
    forest_bnd,
    forest_trbe,
    forest_tbe,
    forest_trbd,
    forest_tbd,
    forest_bbd,
    shrub_tbe,
    shrub_tbd,
    shrub_bbd,
    grass_arctic,
    grass,
    cropland,
    bog,
    unspecified
  };

  // this are default values from
  //    vector<double> hveg_top_default{17.0, 17.0, 14.0, 35.0, 35.0, 20.0, 20.0, 20.0, 5.0, 4.0, 4.0, 0.6, 0.6, 0.6};
  //    vector<double> hveg_bottom_default{6.0, 6.0, 2.0,  1.0, 1.0, 11.0, 11.0, 11.0, 2.0, 2.0, 2.0, 0.0, 0.0, 0.0};
  //    vector<double> rleaf_min_default{0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.015, 0.015, 0.02,
  //    0.01}; vector<double> cleaf_ax_default{100.0, 100.0, 100.0, 100.0, 100.0, 100.0,
  //    100.0,  67.0, 67.0, 67.0, 50.0, 50.0, 100.0}; vector<double>
  //    lai_max_default{6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 4.6, 5.1, 6.0, 2.6, 2.6, 6.0}; vector<double>
  //    sai_default{2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.6, 2.4, 1.9, 3.7, 3.7, 0.7}; vector<double>
  //    albedo_default{0.062, 0.062, 0.062, 0.076, 0.076, 0.092, 0.092, 0.091, 0.099, 0.099, 0.107, 0.107, 0.101};1


  /**
   * @brief vegetation
   * @details
   * supplies the lai, albedo, vegetation_height properties in the form of
   * time-series to the cell. This is the so-called cell-personality, this all make sense for the "forest"-cell,
   * which is basically any "vegetated"-cell. We propogate vegetation properties using this structure. The properties
   * are allowed to be time-series. In the geo_cell_model we allow shared pointer to the vegetation properties. This
   * shared pointer can be exactly the same to all vegetated cells. Each property can have its own ts-type, so that we
   * can easily represent constants if needed. We use this template class as a building block of the cell-types, since
   * most cells need this information.
   * @see cell
   * @tparam timeaxis type, the ts-type should all be constructible/resetable with a timeaxis and a fill-value.
   *
   * @tparam hveg type for vegetation-height
   * @tparam lai_ts type for the leaf-area-index ts, usually a shyft::time_series::points_ts<TA> type // or maybe better
   * bulk-surface-resistance
   *
   * @tparam albedo_ts type for albedo
   */
  struct vegetation_cell_data {
    // FIXME: use time-series, or pattern for season generators.
    // apoint_ts lai_;
    // apoint_ts albedo_;

    double lai_value(utctime /*t*/) const {
      return 0.6; // just arbitrary value for now
    }

    double albedo_value(utctime /*t*/) const {
      return 0.05; // kind of average, for now
    }

    vegetation_cell_data() {
      vegetation_ = vegetation_type::grass_arctic; // default to arctic grass
    }

    vegetation_cell_data(timeaxis_t& ta) {
      init(ta);
      vegetation_ = vegetation_type::grass_arctic;
    }

    //< reset/initializes all series with nan and reserves size to ta. size.
    // fill with 0?
    // TODO: init with default single values suitable for default vegetation_type
    void init(timeaxis_t const & /*ta*/) {
      //            if (ta != lai_.ta) {
      //                lai_ = pts_t(ta, shyft::nan,ts_point_fx::POINT_AVERAGE_VALUE);
      //                albedo_ = pts_t(ta, shyft::nan,ts_point_fx::POINT_AVERAGE_VALUE);
      //            } else {
      //                lai_.fill(shyft::nan);
      //                albedo_.fill(shyft::nan);
      //            }
    }

    bool has_nan_values() const {
      // for(size_t i=0;i<lai_.size();++i) if(!std::isfinite(lai_.value(i))) return true;
      // for(size_t i=0;i<albedo_.size();++i) if(!std::isfinite(albedo_.value(i))) return true;
      return false;
    }

    void set_vegetation_type(vegetation_type veg = vegetation_type::grass_arctic) {
      vegetation_ = veg;
    }

    void set_vegetation_properties(
      apoint_ts /*lai*/,
      apoint_ts /* albedo*/,
      double hveg,
      double lai_max,
      double cleaf_max,
      double sai,
      double k_shelter,
      double soil_moisture_deficit) {
      hveg_ = hveg;
      // lai_ = lai;
      // albedo_ = albedo;
      lai_max_ = lai_max;
      cleaf_max_ = cleaf_max;
      sai_ = sai;
      k_shelter_ = k_shelter;
      soil_moisture_deficit_ = std::max(std::min(soil_moisture_deficit, 8.4), 0.0);
    }

    //        void set_defaul_vegetation_properties(vegetation_type& vt){
    //            switch(vt) {
    //                case vegetation_type::forest_tne:
    //                    hveg_top_= 17.0;
    //                    hveg_bottom_ = 6.0;
    //                    rleaf_min_ = 0.1;
    //                    cleaf_max_ = 100.0;
    //                    lai_max_ = 6.0;
    //                    sai_ = 2.0;
    //                    albedo_live_leaves_ = 0.062;
    //                    break;
    //                case vegetation_type::forest_bne:
    //                    hveg_top_=17.0;
    //                    hveg_bottom_ = 6.0;
    //                    rleaf_min_ = 0.1;
    //                    cleaf_max_ = 100.0;
    //                    lai_max_ = 6.0;
    //                    sai_ = 2.0;
    //                    albedo_live_leaves_ = 0.062;
    //                    break
    //                case vegetation_type::forest_bnd:
    //                    hveg_top_=17.0;
    //                    hveg_bottom_ = 6.0;
    //                    rleaf_min_ = 0.1;
    //                    cleaf_max_ = 100.0;
    //                    lai_max_ = 6.0;
    //                    sai_ = 2.0;
    //                    albedo_live_leaves_ = 0.062;
    //                    break
    //                case vegetation_type::unspecified:throw std::runtime_error("vegetation_type needs to be
    //                speficied");
    //            }
    //        }
    vegetation_type const & get_vegetation_type() const {
      return vegetation_;
    }

    double const & get_vegetation_property_hveg() const {
      return hveg_;
    }

    double const & get_vegetation_property_lai_max() const {
      return lai_max_;
    }

    double const & get_vegetation_property_cleaf_max() const {
      return cleaf_max_;
    }

    double const & get_vegetation_property_sai() const {
      return sai_;
    }

    double const & get_vegetation_property_ks() const {
      return k_shelter_;
    }

    double const & get_vegetation_property_soil_moisture_deficit() const {
      return soil_moisture_deficit_;
    }

    // const apoint_ts& get_vegetation_property_lai() const {return lai_;}
    // const apoint_ts& get_vegetation_property_albedo() const {return albedo_;}

    bool operator==(vegetation_cell_data const & o) const {
      return o.vegetation_ == vegetation_;
    }

    bool operator!=(vegetation_cell_data const & o) const {
      return !operator==(o);
    }

    vegetation_type vegetation_;
   private:
    double hveg_{17.0};                // vegetation height (top of canopy)
    double hveg_top_{17.0};            // vegetation height (top of canopy)
    double hveg_bottom_{6.0};          // vegetation height bottom of canopy
    double lai_max_{6.0};              // maximum value of leaf-area-index
    double cleaf_max_{100.0};          // maximum value of leaf conductance
    double sai_{2.0};                  // steam area index
    double rleaf_min_{0.1};            //
    double k_shelter_{0.5};            // shelter factor
    double albedo_live_leaves_{0.062}; // albedo of live leaves
    double soil_moisture_deficit_{0.1};
    x_serialize_decl();
  };
}

//-- serialization support shyft
// x_serialize_export_key(shyft::core::vegetation_type);
x_serialize_export_key(shyft::core::vegetation_cell_data);
BOOST_CLASS_VERSION(shyft::core::vegetation_cell_data, 1)
