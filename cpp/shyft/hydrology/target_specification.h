/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <string>
#include <vector>

#include <shyft/core/core_serialization.h>
#include <shyft/core/formatters.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::core::model_calibration {
  using shyft::time_series::dd::apoint_ts;

  /**
   * @brief target_spec_calc_type
   * @details
   * Provides enumeration of how to quantify difference between observed and simulated results
   * of some property of the model.
   *
   */
  enum target_spec_calc_type {
    NASH_SUTCLIFFE, //
    KLING_GUPTA,    ///< ref. Gupta09, Journal of Hydrology 377(2009) 80-91
    ABS_DIFF,       ///< abs-difference suitable for periodic water-balance calculations,
                    ///< cell-charge uses relative scale max
                    ///< (abs sim).
    RMSE            ///< root mean squared and scaled to average observed value
  };

  /**
   * @brief property_type for target specification
   * @details
   * Enumerate the model items that we can observe/measure
   * directly or indirectly.
   */
  enum target_property_type {
    DISCHARGE,
    SNOW_COVERED_AREA,
    SNOW_WATER_EQUIVALENT,
    ROUTED_DISCHARGE,
    CELL_CHARGE
  };

  /**
   * @brief The target_specification describes one component of the overall goal-functions
   *
   * @details The target specification contains:
   * -# a target ts (the observed quantity)
   * -# a list of catchment ids (zero-based), that denotes the catchment.discharges that should equal the target ts.
   * -# a scale_factor that is used to construct the final goal function as
   *
   * goal_function = sum of all: scale_factor*(1 - nash-sutcliff factor) or KLING-GUPTA
   *
   */

  struct target_specification {
    using target_time_series_t = apoint_ts;
    apoint_ts ts;                           ///< The target ts, - any type that is time-series compatible
    std::vector<int64_t> catchment_indexes; ///< the catchment_indexes that denotes the catchments in the model that
                                            ///< together should match the target ts
    int64_t river_id = 0;                   ///< in case of catchment_property = ROUTED_DISCHARGE
    double scale_factor{1.0}; ///<< the scale factor to be used when considering multiple target_specifications.
    target_spec_calc_type calc_mode{NASH_SUTCLIFFE};    ///< how to measure
    target_property_type catchment_property{DISCHARGE}; ///< what property to measure
    double s_r{1.0};                                    ///< KG-scalefactor for correlation
    double s_a{1.0};                                    ///< KG-scalefactor for alpha (variance)
    double s_b{1.0};                                    ///< KG-scalefactor for beta (bias)
    std::string uid; ///< external user specified id associated with this target spec.
    apoint_ts fx;    ///< use bind sim to fx.expression symbol 'simulated etc.'
    target_specification() = default;
    bool operator==(target_specification const &) const = default;

    /**
     * @brief target_specification
     * @details
     * A target specification element for calibration, specifying all needed parameters
     *
     * @param ts the target time-series that contain the target/observed discharge values
     * @param cids  a vector of the catchment ids in the model that together should add up to the target time-series
     * @param scale_factor the weight that this target_specification should have relative to the possible other
     * target_specs.
     * @param calc_mode how to calculate goal-function nash-sutcliffe or the more advanced and flexible kling-gupta
     * @param s_r scale factor r in kling-gupta
     * @param s_a scale factor a in kling-gupta
     * @param s_b scale factor b in kling-gupta
     * @param catchment_property_ determines if this target specification is for
     * DISCHARGE|SNOW_COVERED_AREA|SNOW_WATER_EQUIVALENT
     * @param uid a user supplied uid, string, to help user correlate this target to external data-sources
     */
    target_specification(
      apoint_ts const &ts,
      vector<int64_t> const &cids,
      double scale_factor,
      target_spec_calc_type calc_mode = NASH_SUTCLIFFE,
      double s_r = 1.0,
      double s_a = 1.0,
      double s_b = 1.0,
      target_property_type catchment_property_ = DISCHARGE,
      std::string uid = "")
      : ts{ts}
      , catchment_indexes{cids}
      , scale_factor{scale_factor}
      , calc_mode{calc_mode}
      , catchment_property{catchment_property_}
      , s_r{s_r}
      , s_a{s_a}
      , s_b{s_b}
      , uid{std::move(uid)} {
    }

    /**
     * @brief Constructs a target specification element for calibration, specifying all needed parameters
     *
     * @param ts the target time-series that contain the target/observed discharge values
     * @param rid  identifies the river id in the model that this target specifies
     * @param scale_factor the weight that this target_specification should have relative to the possible other
     * target_specs.
     * @param calc_mode how to calculate goal-function nash-sutcliffe or the more advanced and flexible kling-gupta
     * @param s_r scale factor r in kling-gupta
     * @param s_a scale factor a in kling-gupta
     * @param s_b scale factor b in kling-gupta
     * @param uid a user supplied uid, string, to help user correlate this target to external data-sources
     */
    target_specification(
      apoint_ts const &ts,
      int64_t river_id,
      double scale_factor,
      target_spec_calc_type calc_mode = NASH_SUTCLIFFE,
      double s_r = 1.0,
      double s_a = 1.0,
      double s_b = 1.0,
      std::string uid = "")
      : ts{ts}
      , river_id{river_id}
      , scale_factor{scale_factor}
      , calc_mode{calc_mode}
      , catchment_property{ROUTED_DISCHARGE}
      , s_r{s_r}
      , s_a{s_a}
      , s_b{s_b}
      , uid{std::move(uid)} {
    }

    x_serialize_decl();
  };

  /**
   * @brief evaluate symbolic ts with single arg
   * @param expr expression with at least one sym-ts ref
   * @param a_ts ts to be bound to all sym refs of fx expression
   * @return evaluated ts
   *
   */
  inline apoint_ts evaluate_ts_fx(apoint_ts const &expr, apoint_ts const &a_ts) {
    auto fx = expr.clone_expr();
    auto bis = fx.find_ts_bind_info();
    for (auto &bi : bis)
      bi.ts.bind(a_ts);
    fx.do_bind();
    return fx.evaluate();
  };

  /**
   * @brief goal_function_model
   * @details
   * To support persisted goal_functions_model, that can be
   * used in orchestration, and also track changes.
   * As pr usual, we utilize the model_info to store
   * other useful information about the goal_function,
   * that for now keep only the mathematical aspects
   * needed for region model calibration.
   */
  struct goal_function_model {
    std::int64_t id;                                  ///< the unique model id of the goal function description
    std::vector<target_specification> goal_functions; ///< simply the goal function it self

    goal_function_model() = default;
    bool operator==(goal_function_model const &) const = default;

    x_serialize_decl();
  };

}

x_serialize_export_key(shyft::core::model_calibration::target_specification)

  x_serialize_export_key(shyft::core::model_calibration::goal_function_model)

    BOOST_CLASS_VERSION(shyft::core::model_calibration::target_specification, 1)

  //-- formatting
  template <typename Char>
  struct fmt::formatter<shyft::core::model_calibration::target_specification, Char> {
  FMT_CONSTEXPR auto parse(auto &ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  auto format(shyft::core::model_calibration::target_specification const &o, auto &ctx) const {
    auto out = ctx.out();
    return fmt::format_to(out, "{{ .uid={}, .scale_factor={},.ts={} }}", o.uid, o.scale_factor, o.ts.stringify());
  }
};

template <typename Char>
struct fmt::formatter<shyft::core::model_calibration::goal_function_model, Char> {
  FMT_CONSTEXPR auto parse(auto &ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  auto format(shyft::core::model_calibration::goal_function_model const &o, auto &ctx) const {
    auto out = ctx.out();
    return fmt::format_to(out, "{{ .id={}, .goal_functions={} }}", o.id, o.goal_functions);
  }
};

SHYFT_DEFINE_SHARED_PTR_FORMATTER(shyft::core::model_calibration::goal_function_model)