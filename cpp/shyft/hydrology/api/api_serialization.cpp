/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif
#include <shyft/hydrology/api/api_pch.h>

/**
 serializiation implemented using boost,
  see reference: http://www.boost.org/doc/libs/1_62_0/libs/serialization/doc/
 */
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/vector.hpp>

//
// 1. first include std stuff and the headers for
//    files with serialization support
//
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/hydrology/api/api_state.h>
#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/hydrology/target_specification.h>

// then include stuff you need like vector,shared, base_obj,nvp etc.

//
// 2. Then implement each class serialization support
//

using namespace shyft::core;
using boost::serialization::base_object;

template <class Archive>
void shyft::api::cell_state_id::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("cid", cid) & core_nvp("x", x) & core_nvp("y", y) & core_nvp("a", area);
}

template <class CS>
template <class Archive>
void shyft::api::cell_state_with_id<CS>::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("id", id) & core_nvp("state", state);
}

template <class Archive>
void shyft::api::GeoPointSource::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("mid_point_", mid_point_) & core_nvp("ts", ts) & core_nvp("uid", uid);
}

template <class Archive>
void shyft::api::TemperatureSource::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("geopoint_ts", base_object<shyft::api::GeoPointSource>(*this));
}

template <class Archive>
void shyft::api::PrecipitationSource::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("geopoint_ts", base_object<shyft::api::GeoPointSource>(*this));
}

template <class Archive>
void shyft::api::WindSpeedSource::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("geopoint_ts", base_object<shyft::api::GeoPointSource>(*this));
}

template <class Archive>
void shyft::api::RelHumSource::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("geopoint_ts", base_object<shyft::api::GeoPointSource>(*this));
}

template <class Archive>
void shyft::api::RadiationSource::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("geopoint_ts", base_object<shyft::api::GeoPointSource>(*this));
}

template <class Archive>
void shyft::api::a_region_environment::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("temperature", temperature) & core_nvp("precipitation", precipitation) & core_nvp("radiation", radiation)
    & core_nvp("wind_speed", wind_speed) & core_nvp("rel_hum", rel_hum);
}

template <class Archive>
void shyft::core::model_calibration::target_specification::serialize(
  Archive &ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("ts", ts) & core_nvp("catchment_indexes", catchment_indexes) & core_nvp("river_id", river_id)
    & core_nvp("scale_factor", scale_factor) & core_nvp("calc_mode", calc_mode)
    & core_nvp("catchment_property", catchment_property) & core_nvp("s_r", s_r) & core_nvp("s_a", s_a)
    & core_nvp("s_b", s_b) & core_nvp("uid", uid);
  if (file_version > 0)
    ar &core_nvp("fx", fx);
}

/// shyft::core::optimizer
template <class Archive>
void shyft::hydrology::srv::calibration_options::serialize(
  Archive &ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("method", method) & core_nvp("max_n_iterations", max_n_iterations) & core_nvp("time_limit", time_limit)
    & core_nvp("solver_epsilon", solver_epsilon) & core_nvp("x_epsilon", x_epsilon) & core_nvp("y_epsilon", y_epsilon)
    & core_nvp("tr_start", tr_start) & core_nvp("tr_stop", tr_stop);
}

template <class Archive>
void shyft::hydrology::srv::calibration_status::serialize(
  Archive &ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("p_trace", p_trace) & core_nvp("f_trace", f_trace) & core_nvp("running", running)
    & core_nvp("p_result", p_result);
}

template <class Archive>
void shyft::hydrology::srv::state_model::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("id", id) & core_nvp("states", states);
}

template <class Archive>
void shyft::hydrology::srv::parameter_model::serialize(Archive &ar, [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("id", id) & core_nvp("parameters", parameters);
}

template <class Archive>
void shyft::core::model_calibration::goal_function_model::serialize(
  Archive &ar,
  [[maybe_unused]] unsigned int const file_version) {
  ar &core_nvp("id", id) & core_nvp("goal_functions", goal_functions);
}

//
// 3. force impl. of pointers etc.
//
// clang-format off
x_serialize_instantiate_and_register(shyft::hydrology::srv::parameter_model)
x_serialize_instantiate_and_register(shyft::hydrology::srv::state_model)
x_serialize_instantiate_and_register(shyft::hydrology::srv::calibration_status)
x_serialize_instantiate_and_register(shyft::hydrology::srv::calibration_options)
x_serialize_instantiate_and_register(shyft::core::model_calibration::target_specification)
x_serialize_instantiate_and_register(shyft::core::model_calibration::goal_function_model)
x_serialize_instantiate_and_register(shyft::api::cell_state_id)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::pt_gs_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::r_pm_gs_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::r_pm_st_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::r_pmv_st_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::r_pt_gs_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::pt_ss_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::pt_hs_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::pt_st_k::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::pt_st_hbv::state>)
x_serialize_instantiate_and_register(shyft::api::cell_state_with_id<shyft::core::pt_hps_k::state>)
x_poly_serialize_instantiate_and_register(shyft::api::GeoPointSource)
x_poly_serialize_instantiate_and_register(shyft::api::TemperatureSource)
x_poly_serialize_instantiate_and_register(shyft::api::PrecipitationSource)
x_poly_serialize_instantiate_and_register(shyft::api::WindSpeedSource)
x_poly_serialize_instantiate_and_register(shyft::api::RelHumSource)
x_poly_serialize_instantiate_and_register(shyft::api::RadiationSource)
x_serialize_instantiate_and_register(shyft::api::a_region_environment)

// clang-format on

namespace shyft { namespace api {
  //-serialization of state to byte-array in python support
  template <class CS>
  std::vector<char> serialize_to_bytes(std::shared_ptr<std::vector<CS>> const &states) {
    std::ostringstream xmls(std::ios::binary);
    core_oarchive oa(xmls, core::arch_info_flags);
    oa << core_nvp("states", states);
    xmls.flush();
    auto s = xmls.str();
    return std::vector<char>(s.begin(), s.end());
  }

  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<pt_gs_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<r_pm_gs_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<r_pm_st_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<r_pmv_st_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<r_pt_gs_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<pt_ss_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<pt_hs_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<pt_st_k::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<pt_st_hbv::state>>> const &states);
  template std::vector<char>
    serialize_to_bytes(std::shared_ptr<std::vector<cell_state_with_id<pt_hps_k::state>>> const &states);

  template <class CS>
  void deserialize_from_bytes(std::vector<char> const &bytes, std::shared_ptr<std::vector<CS>> &states) {
    std::string str_bin(bytes.begin(), bytes.end());
    std::istringstream xmli(str_bin,std::ios::binary);
    core_iarchive ia(xmli, core::arch_info_flags);
    ia >> core_nvp("states", states);
  }

  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<pt_gs_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<r_pm_gs_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<r_pm_st_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<r_pmv_st_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<r_pt_gs_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<pt_hs_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<pt_st_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<pt_st_hbv::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<pt_ss_k::state>>> &states);
  template void deserialize_from_bytes(
    std::vector<char> const &bytes,
    std::shared_ptr<std::vector<cell_state_with_id<pt_hps_k::state>>> &states);

  std::vector<char> a_region_environment::serialize_to_bytes() const {
    std::ostringstream xmls(std::ios::binary);
    core_oarchive oa(xmls, core::arch_info_flags);
    oa << core_nvp("a_region_environment", *this);
    xmls.flush();
    auto s = xmls.str();
    return std::vector<char>(s.begin(), s.end());
  }

  a_region_environment a_region_environment::deserialize_from_bytes(std::vector<char> const &ss) {
    std::string str_bin(ss.begin(), ss.end());
    std::istringstream xmli(str_bin,std::ios::binary);
    core_iarchive ia(xmli, core::arch_info_flags);
    a_region_environment r;
    ia >> core_nvp("a_region_environment", r);
    return r;
  }

}}
