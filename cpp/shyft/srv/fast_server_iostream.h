// Copyright (C) 2006  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.
#pragma once

// notice: This file is based entirely on Davis Kings server_iostream file, only diffs are namespace and disable naggle
// on socket connections
//         so that we get faster small packets over the network (vital for the drms services that when scaled up have a
//         lot of small packet exchanges)
//

#include <atomic>
#include <iosfwd>
#include <thread>
#include <tuple>

#include <dlib/logger.h>
#include <dlib/map.h>
#include <dlib/server/server_kernel.h>
#include <dlib/sockstreambuf.h>
#include <dlib/uintn.h>

#include <shyft/time/utctime_utilities.h>

namespace shyft::srv {

  using dlib::uint64;
  using dlib::auto_mutex;

  struct log_config {
    std::string file = "";
    int level = dlib::LWARN.priority;

    SHYFT_DEFINE_STRUCT(log_config, (), (file, level));
    auto operator<=>(log_config const &) const = default;
  };

  struct fast_server_iostream_config {
    core::utctimespan stale_duration = core::utctimespan(3'600'000'000l);
    core::utctimespan stale_sweep_interval = core::utctimespan(100'000l);
    log_config log = {};

    SHYFT_DEFINE_STRUCT(fast_server_iostream_config, (), (stale_duration, stale_sweep_interval, log));
    auto operator<=>(fast_server_iostream_config const &) const = default;
  };

  /**
   * @brief sensor to help detect stale connections safely
   */
  struct stale_connection_sensor {
    std::atomic<core::utctime> last_seen;           ///< dispatch should tick this on every mesg. recvd and sent
    std::atomic_flag processing = ATOMIC_FLAG_INIT; ///< dispatch should set this true while processing request.

    stale_connection_sensor()
      : last_seen{core::utctime_now()} {
    }

    void alive_tick() {
      last_seen.store(core::utctime_now());
    }

    bool is_stale(core::utctime const &t_stale_limit) const {
      return !processing.test() && last_seen.load() < t_stale_limit;
    }
  };

  /**
   * @brief scoped mech to support stale processing/tick refresh.
   */
  struct scoped_processing {
    stale_connection_sensor &s;
    scoped_processing() = delete;
    scoped_processing(scoped_processing const &) = delete;
    scoped_processing(scoped_processing &&) = delete;
    scoped_processing &operator=(scoped_processing const &) = delete;
    scoped_processing &operator=(scoped_processing &&) = delete;

    explicit scoped_processing(stale_connection_sensor &s)
      : s(s) {
      (void) s.processing.test_and_set(); // stale will skip while we process.
    }

    ~scoped_processing() {
      s.last_seen = core::utctime_now(); // if processing takes time, ensure to tick it here!
      s.processing.clear();              // prior to clearing the processing flag.
    }
  };

  class fast_server_iostream : public dlib::server {

    /*!
        INITIAL VALUE
            - next_id == 0
            - con_map.size() == 0

        CONVENTION
            - next_id == the id of the next connection
            - for all current connections
                - con_map[id] == the connection object with the given id
            - m == the mutex that protects the members of this object
    !*/

    using id_map = dlib::map<
      uint64,
      std::tuple<dlib::connection *, std::shared_ptr<stale_connection_sensor>>,
      dlib::memory_manager<char>::kernel_2a>::kernel_1b;

   public:
    fast_server_iostream(fast_server_iostream_config config = {});

    ~fast_server_iostream();

    size_t get_stale_close_count() const {
      return stale_close_count.load();
    }
   protected:
    void shutdown_connection(uint64 id);

   private:
    virtual void on_connect(
      std::istream &in,
      std::ostream &out,
      std::string const &foreign_ip,
      std::string const &local_ip,
      unsigned short foreign_port,
      unsigned short local_port,
      uint64 connection_id,
      stale_connection_sensor &) = 0;

    void on_connect(dlib::connection &con);
    void stale_sweep(std::stop_token);

    core::utctimespan stale_duration, stale_sweep_interval;
    std::atomic_size_t stale_close_count{0};
    uint64 next_id;
    id_map con_map;
    static dlib::logger const _dLog;
    dlib::mutex m;
    std::jthread stale_sweeper;
  };


}

SHYFT_DEFINE_STRUCT_FORMATTER(shyft::srv::log_config);
SHYFT_DEFINE_STRUCT_FORMATTER(shyft::srv::fast_server_iostream_config);
