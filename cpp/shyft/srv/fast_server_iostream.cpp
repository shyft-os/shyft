// Copyright (C) 2006  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.
#include <iostream>

#include <shyft/core/dlib_utils.h>
#include <shyft/srv/fast_server_iostream.h>

namespace shyft::srv {
  dlib::logger const fast_server_iostream::_dLog{"dlib.fast_server_iostream"};

  void fast_server_iostream::on_connect(dlib::connection &con) {
    bool my_fault = true;
    uint64 this_con_id = 0;
    try {
      con.disable_nagle();
      dlib::sockstreambuf buf(&con);
      std::istream in(&buf);
      std::ostream out(&buf);
      in.tie(&out);
      auto last_ptr = std::make_shared<stale_connection_sensor>();
      // add this connection to the con_map
      {
        auto_mutex M(m);
        this_con_id = next_id;
        auto this_con = std::make_tuple(&con, last_ptr);
        con_map.add(this_con_id, this_con);
        this_con_id = next_id;
        ++next_id;
      }

      my_fault = false;
      on_connect(
        in,
        out,
        con.get_foreign_ip(),
        con.get_local_ip(),
        con.get_foreign_port(),
        con.get_local_port(),
        this_con_id,
        *last_ptr);

      // remove this connection from the con_map
      {
        auto_mutex M(m);
        std::tuple<dlib::connection *, std::shared_ptr<stale_connection_sensor > > this_con{};
        uint64 junk{0};
        if (con_map.is_in_domain(this_con_id))
          con_map.remove(this_con_id, junk, this_con);
      }
    } catch (std::bad_alloc &) {
      // make sure we remove this connection from the con_map
      {
        auto_mutex M(m);
        if (con_map.is_in_domain(this_con_id)) {
          std::tuple<dlib::connection *, std::shared_ptr<stale_connection_sensor > > this_con{};
          uint64 junk{0};
          con_map.remove(this_con_id, junk, this_con);
        }
      }

      _dLog << dlib::LERROR << "We ran out of memory in server_iostream::on_connect()";
      // if this is an escaped exception from on_connect then let it fly!
      // Seriously though, this way it is obvious to the user that something bad happened
      // since they probably won't have the dlib logger enabled.
      if (!my_fault)
        throw;
    }
  }

  void fast_server_iostream::stale_sweep(std::stop_token token) {
    std::mutex cv_mutex;
    std::condition_variable_any cv;
    while (!token.stop_requested()) {
      std::vector<std::tuple<uint64, std::string> > stale;
      {
        auto_mutex M(m);
        con_map.reset();
        auto t_stale_limit = core::utctime_now() - stale_duration;
        while (con_map.move_next()) {
          auto &e = con_map.element();
          if (std::get<1>(e.value())->is_stale(t_stale_limit))
            stale.push_back(std::make_tuple(
              e.key(),
              fmt::format(
                "{}@{}:{}",
                e.key(),
                std::get<0>(e.value())->get_foreign_port(),
                std::get<0>(e.value())->get_foreign_ip())));
        }
      }
      stale_close_count += stale.size();
      for (auto const &[s, remote_id] : stale) {
        if (token.stop_requested()) // the shutdown_connection(s) could take time
          break;                    // so we check inside loop for early exit to ensure srv. closes fast if requested so
        _dLog << dlib::LDEBUG << "Closing stale connection " << remote_id;
        shutdown_connection(s);
      }
      std::unique_lock lock(cv_mutex);
      cv.wait_for(lock, stale_sweep_interval, [&token] {
        return token.stop_requested();
      });
    }
  }

  fast_server_iostream::fast_server_iostream(fast_server_iostream_config config)
    : stale_duration(config.stale_duration)
    , stale_sweep_interval(config.stale_sweep_interval)
    , next_id(0)
    , stale_sweeper([this](std::stop_token stop_token) {
      this->stale_sweep(stop_token);
    }) {
    core::logging::output_file(config.log.file);
    core::logging::level({config.log.level, "custom"});
  }

  fast_server_iostream::~fast_server_iostream() {
    stale_sweeper.request_stop();
    dlib::server::clear();
  }

  void fast_server_iostream::shutdown_connection(uint64 id) {
    auto_mutex M(m);
    if (con_map.is_in_domain(id)) {
      std::get<0>(con_map[id])->shutdown();
    }
  }
}
