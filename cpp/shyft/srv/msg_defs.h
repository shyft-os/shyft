/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cstdint>
#include <string>

namespace shyft::srv {
  using std::string;

  /** @brief  message-types
   *
   * The message types used for the wire-communication of ec::srv.
   *
   */
  struct message_type {
    using type = uint8_t;
    static constexpr type SERVER_EXCEPTION = 0;
    static constexpr type MODEL_INFO = 1;
    static constexpr type MODEL_INFO_UPDATE = 2;
    static constexpr type MODEL_STORE = 3;
    static constexpr type MODEL_READ = 4;
    static constexpr type MODEL_DELETE = 5;
    static constexpr type MODEL_READ_ARGS = 6;
    static constexpr type MODEL_INFO_FILTERED = 7;
  };
}
