// Copyright (C) 2012  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.
#pragma once

// notice: This file is based entirely on Davis Kings iosockstream file, only diffs are namespace and disable naggle on
// socket connections
//         so that we get faster small packets over the network (vital for the drms services that when scaled up have a
//         lot of small packet exchanges)
//

#include <iosfwd>
#include <memory>

#include <dlib/sockstreambuf.h>
#include <dlib/timeout.h>

#ifdef _MSC_VER
// Disable the warning about inheriting from std::iostream 'via dominance' since this warning is a warning about
// visual studio conforming to the standard and is ignorable.
// See
// http://connect.microsoft.com/VisualStudio/feedback/details/733720/inheriting-from-std-fstream-produces-c4250-warning
// for further details if interested.
#pragma warning(disable : 4250)
#endif // _MSC_VER

namespace shyft::srv {
  using dlib::network_address;
  using dlib::auto_mutex;
  using dlib::sockstreambuf;
  using dlib::connect;
  using dlib::connection;
  using dlib::rmutex;
  using dlib::timeout;

  // ----------------------------------------------------------------------------------------

  class fast_iosockstream : public std::iostream {
   public:
    fast_iosockstream();

    fast_iosockstream(network_address const & addr);

    fast_iosockstream(network_address const & addr, unsigned long timeout);

    ~fast_iosockstream();

    void open(network_address const & addr);

    void open(network_address const & addr, unsigned long timeout);

    void close(unsigned long timeout = 10000);

    void terminate_connection_after_timeout(unsigned long timeout);

    void shutdown();

   private:
    void terminate_connection(std::shared_ptr<connection> thecon);

    std::unique_ptr<timeout> con_timeout;
    rmutex class_mutex;
    std::shared_ptr<connection> con;
    std::unique_ptr<sockstreambuf> buf;
  };

  // ----------------------------------------------------------------------------------------

}
