#pragma once

#include <algorithm>
#include <cstdint>
#include <memory>
#include <ranges>
#include <stdexcept>
#include <string>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/dtss/time_series_info.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/geo_ts.h>

#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

namespace shyft::pyapi {

  template <typename Vector, typename holder_type = std::unique_ptr<Vector>, typename... Args>
  auto bind_vector(py::handle scope, std::string const &name, Args &&...args) {
    using Class_ = py::class_<Vector, holder_type>;
    Class_ cl(scope, name.c_str(), std::forward<Args>(args)...);

    py::detail::vector_buffer<Vector, Class_, Args...>(cl);

    cl.def(py::init<>());

    py::detail::vector_if_copy_constructible<Vector, Class_>(cl);

    if constexpr (fmt::is_formattable<typename Vector::value_type>::value && requires { pyapi::expose_format(cl); })
      pyapi::expose_format(cl);

    py::detail::vector_modifiers<Vector, Class_>(cl);
    py::detail::vector_accessor<Vector, Class_>(cl);

    cl.def(
      "__bool__",
      [](Vector const &v) -> bool {
        return !v.empty();
      },
      "Check whether the list is nonempty");

    cl.def("__len__", [](Vector const &vec) {
      return vec.size();
    });

    // FIXME: delete - jeh
    cl.def("size", [](Vector const &vec) {
      return vec.size();
    });

    py::implicitly_convertible<py::iterable, Vector>();

    return cl;
  }

  namespace detail {
    struct keys_view {
      virtual py::size_t len() = 0;
      virtual py::iterator iter() = 0;
      virtual bool contains(py::handle const &k) = 0;
      virtual ~keys_view() = default;
    };

    struct values_view {
      virtual py::size_t len() = 0;
      virtual py::iterator iter() = 0;
      virtual ~values_view() = default;
    };

    struct items_view {
      virtual py::size_t len() = 0;
      virtual py::iterator iter() = 0;
      virtual ~items_view() = default;
    };

    template <typename Map>
    struct KeysViewImpl : public detail::keys_view {
      explicit KeysViewImpl(Map &map)
        : map(map) {
      }

      py::size_t len() override {
        return map.size();
      }

      py::iterator iter() override {
        return py::make_key_iterator(map.begin(), map.end());
      }

      bool contains(py::handle const &k) override {
        try {
          return map.find(k.template cast<typename Map::key_type>()) != map.end();
        } catch (py::cast_error const &) {
          return false;
        }
      }

      Map &map;
    };

    template <typename Map>
    struct ValuesViewImpl : public detail::values_view {
      explicit ValuesViewImpl(Map &map)
        : map(map) {
      }

      py::size_t len() override {
        return map.size();
      }

      py::iterator iter() override {
        return py::make_value_iterator(map.begin(), map.end());
      }

      Map &map;
    };

    template <typename Map>
    struct ItemsViewImpl : public detail::items_view {
      explicit ItemsViewImpl(Map &map)
        : map(map) {
      }

      py::size_t len() override {
        return map.size();
      }

      py::iterator iter() override {
        return py::make_iterator(map.begin(), map.end());
      }

      Map &map;
    };

  }

  template <typename Map, typename holder_type = std::unique_ptr<Map>>
  auto bind_map(py::handle scope, std::string const &name, auto... args) {
    using KeyType = typename Map::key_type;
    using MappedType = typename Map::mapped_type;
    using KeysView = detail::keys_view;
    using ValuesView = detail::values_view;
    using ItemsView = detail::items_view;
    using Class_ = py::class_<Map, holder_type>;

    Class_ cl(scope, name.c_str(), args...);

    if (!py::detail::get_type_info(typeid(KeysView))) {
      py::class_<KeysView> keys_view(scope, "KeysView", args...);
      keys_view.def("__len__", &KeysView::len);
      keys_view.def("__iter__", &KeysView::iter, py::keep_alive<0, 1>());
      keys_view.def("__contains__", &KeysView::contains);
    }
    if (!py::detail::get_type_info(typeid(ValuesView))) {
      py::class_<ValuesView> values_view(scope, "ValuesView", args...);
      values_view.def("__len__", &ValuesView::len);
      values_view.def("__iter__", &ValuesView::iter, py::keep_alive<0, 1>());
    }
    if (!py::detail::get_type_info(typeid(ItemsView))) {
      py::class_<ItemsView> items_view(scope, "ItemsView", args...);
      items_view.def("__len__", &ItemsView::len);
      items_view.def("__iter__", &ItemsView::iter, py::keep_alive<0, 1>());
    }

    cl.def(py::init<>());

    if constexpr (fmt::is_formattable<typename Map::value_type>::value && requires { pyapi::expose_format(cl); })
      pyapi::expose_format(cl);

    if constexpr (std::is_copy_constructible_v<Map>)
      cl.def(py::init<Map const &>(), "CopyConstructor");

    cl.def(
      "__bool__",
      [](Map const &m) -> bool {
        return !m.empty();
      },
      "Check whether the map is nonempty");

    cl.def(
      "__iter__",
      [](Map &m) {
        return py::make_key_iterator(m.begin(), m.end());
      },
      py::keep_alive<0, 1>());

    cl.def(
      "keys",
      [](Map &m) {
        return std::unique_ptr<KeysView>(new detail::KeysViewImpl<Map>(m));
      },
      py::keep_alive<0, 1>());

    cl.def(
      "values",
      [](Map &m) {
        return std::unique_ptr<ValuesView>(new detail::ValuesViewImpl<Map>(m));
      },
      py::keep_alive<0, 1>());

    cl.def(
      "items",
      [](Map &m) {
        return std::unique_ptr<ItemsView>(new detail::ItemsViewImpl<Map>(m));
      },
      py::keep_alive<0, 1>());

    cl.def(
      "__getitem__",
      [](Map &m, KeyType const &k) -> MappedType & {
        auto it = m.find(k);
        if (it == m.end()) {
          throw py::key_error();
        }
        return it->second;
      },
      py::return_value_policy::reference_internal);

    cl.def(
      "get",
      [](Map &m, KeyType const &k, std::optional<MappedType> default_val = std::nullopt) {
        auto it = m.find(k);
        if (it != m.end()) {
          return just(it->second);
        }
        return default_val;
      },
      py::arg("key"),
      py::arg("default") = none<MappedType>,
      "Return the value for key if key is in the dictionary, else default.");

    cl.def("__contains__", [](Map &m, KeyType const &k) {
      auto it = m.find(k);
      if (it == m.end()) {
        return false;
      }
      return true;
    });
    cl.def("__contains__", [](Map &, py::object const &) {
      return false;
    });

    py::detail::map_assignment<Map, Class_>(cl);

    cl.def("__delitem__", [](Map &m, KeyType const &k) {
      auto it = m.find(k);
      if (it == m.end()) {
        throw py::key_error();
      }
      m.erase(it);
    });

    cl.def("__len__", [](Map const &m) {
      return m.size();
    });

    py::implicitly_convertible<py::iterable, Map>();

    return cl;
  }

}

PYBIND11_MAKE_OPAQUE(std::vector<char>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::time_series::dd::ats_vector>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::time_series::dd::geo_ts>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::core::geo_point>);
PYBIND11_MAKE_OPAQUE(std::vector<std::vector<shyft::core::geo_point>>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::energy_market::hydro_power::point>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::energy_market::hydro_power::turbine_operating_zone>);
PYBIND11_MAKE_OPAQUE(
  std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve>>);
PYBIND11_MAKE_OPAQUE(
  std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::xy_point_curve_with_z>>);
PYBIND11_MAKE_OPAQUE(
  std::
    map<shyft::core::utctime, std::shared_ptr<std::vector<shyft::energy_market::hydro_power::xy_point_curve_with_z>>>);
PYBIND11_MAKE_OPAQUE(
  std::map<shyft::core::utctime, std::shared_ptr<shyft::energy_market::hydro_power::turbine_description>>);
