/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/methods/hbv_physical_snow.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/model_calibration.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/stacks/pt_hps_k.h>
#include <shyft/hydrology/stacks/pt_hps_k_cell_model.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/stack.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/version.h>

namespace expose::pt_hps_k {
  using namespace shyft::core;
  using namespace shyft::core::pt_hps_k;
  constexpr auto const *stack_name = "PTHPSK";

  static void parameter_state_response(py::module_ &m) {

    auto pp = expose_stack_parameter<parameter>(
      m,
      stack_name,
      "Contains the parameters to the methods used in the PTHPSK assembly\n"
      "priestley_taylor,hbv_physical_snow,actual_evapotranspiration,precipitation_correction,kirchner\n");
    pp.def(
        py::init<
          priestley_taylor::parameter const &,
          hbv_physical_snow::parameter const &,
          actual_evapotranspiration::parameter const &,
          kirchner::parameter const &,
          precipitation_correction::parameter const &,
          glacier_melt::parameter,
          routing::uhg_parameter,
          mstack_parameter>(),
        py::arg("pt"),
        py::arg("hps"),
        py::arg("ae"),
        py::arg("k"),
        py::arg("p_corr"),
        py::arg("gm") = glacier_melt::parameter{},
        py::arg("routing") = routing::uhg_parameter{},
        py::arg("msp") = mstack_parameter{})
      .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
      .def_readwrite("ae", &parameter::ae, "ActualEvapotranspirationParameter: actual evapotranspiration parameter")
      .def_readwrite("hps", &parameter::hps, "HbvPhysicalSnowParameter: hbv-physical-snow parameter")
      .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
      .def_readwrite("kirchner", &parameter::kirchner, "KirchnerParameter: kirchner parameter")
      .def_readwrite(
        "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
      .def_readwrite(
        "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters");

    expose_stack_state<state>(m, stack_name)
      .def(py::init<hbv_physical_snow::state, kirchner::state>(), py::arg("hps"), py::arg("k"))
      .def_readwrite("hps", &state::hps, "HbvPhysicalSnowState: physical-snow state")
      .def_readwrite("kirchner", &state::kirchner, "KircherState: kirchner state");

    py::class_<response>(
      m, "PTHPSKResponse", "This struct contains the responses of the methods used in the PTHPSK assembly")
      .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
      .def_readwrite("hps", &response::hps, "HbvPhysicalSnowResponse: physical-snow response")
      .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
      .def_readwrite("ae", &response::ae, "ActualEvapotranspirationResponse: actual evapotranspiration response")
      .def_readwrite("kirchner", &response::kirchner, "KirchnerResponse: kirchner response")
      .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
  }

  static void collectors(auto &rac, auto &rd, [[maybe_unused]] auto &sn, auto &sac) {
    rac.def_readonly("destination_area", &all_response_collector::destination_area, "float: a copy of cell area [m2]")
      .def_readonly(
        "avg_discharge",
        &all_response_collector::avg_discharge,
        "TsFixed: Kirchner Discharge given in [m3/s] for the timestep")
      .def_readonly(
        "hps_sca",
        &all_response_collector::snow_sca,
        "TsFixed: hbv snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
      .def_readonly(
        "hps_swe",
        &all_response_collector::snow_swe,
        "TsFixed: hbv snow swe, [mm] over the cell sca.. area, - at the end of timestep")
      .def_readonly(
        "hps_outflow", &all_response_collector::hps_outflow, "TsFixed: hbv snow output [m^3/s] for the timestep")
      .def_readonly(
        "glacier_melt",
        &all_response_collector::glacier_melt,
        "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
      .def_readonly("ae_output", &all_response_collector::ae_output, "TsFixed: actual evap mm/h")
      .def_readonly("pe_output", &all_response_collector::pe_output, "TsFixed: pot evap mm/h")
      .def_readonly(
        "end_response", &all_response_collector::end_response, "PTHPSKResponse: end_response, at the end of collected")
      .def_readonly("avg_charge", &all_response_collector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

    rd.def_readonly("destination_area", &discharge_collector::destination_area, "float: a copy of cell area [m2]")
      .def_readonly(
        "avg_discharge",
        &discharge_collector::avg_discharge,
        "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
      .def_readonly(
        "hps_sca",
        &discharge_collector::snow_sca,
        "TsFixed: hbv snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
      .def_readonly(
        "hps_swe",
        &discharge_collector::snow_swe,
        "TsFixed: hbv snow swe, [mm] over the cell sca.. area, - at the end of timestep")
      .def_readonly(
        "end_response", &discharge_collector::end_response, "PTHPSKResponse: end_response, at the end of collected")
      .def_readwrite("collect_snow", &discharge_collector::collect_snow, "bool: controls collection of snow routine")
      .def_readonly("avg_charge", &discharge_collector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep")

      ;

    sac
      .def_readwrite(
        "collect_state",
        &state_collector::collect_state,
        "if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
      .def_readonly(
        "kirchner_discharge",
        &state_collector::kirchner_discharge,
        "TsFixed: Kirchner state instant Discharge given in m^3/s")
      .def_readonly("hps_swe", &state_collector::hps_swe, "TsFixed: swe")
      .def_readonly("hps_sca", &state_collector::hps_sca, "TsFixed: sca")
      .def_readonly("hps_surface_heat", &state_collector::hps_surface_heat, "TsFixed: surface heat")
      .def_readonly("snow_sp", &state_collector::sp, "CoreTsVector: sp")
      .def_readonly("snow_sw", &state_collector::sw, "CoreTsVector: sw")
      .def_readonly("albedo", &state_collector::albedo, "CoreTsVector: albedo")
      .def_readonly("iso_pot_energy", &state_collector::iso_pot_energy, "CoreTsVector: iso_pot_energy");
  }

  using cell_all = core::cell<parameter, state, state_collector, all_response_collector>;
  using cell_opt = core::cell<parameter, state, null_collector, discharge_collector>;

  static void cells(py::module_ &m) {
    cell<cell_all>(m, "cell_all", "tbd: cell_all doc");
    cell<cell_opt>(m, "PTHPSKCellOpt", "tbd: PTHPSKCellOpt doc");
    statistics::hbv_physical_snow<cell_all>(m, "PTHPSKCell");
    statistics::actual_evapotranspiration<cell_all>(m, "PTHPSKCell");
    statistics::priestley_taylor<cell_all>(m, "PTHPSKCell");
    statistics::kirchner<cell_all>(m, "PTHPSKCell");
  }

  static void models(py::module_ &m) {
    using opt_model = region_model<cell_discharge_response_t, a_region_environment>;
    using model = region_model<cell_complete_response_t, a_region_environment>;
    {
      auto expose_cell_stats = [&]<typename T, typename... O>(py::class_<T, O...> t) {
        t.def_property_readonly("hbv_physical_snow_state", [](T &o) {
          using S = api::hbv_physical_snow_cell_state_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("hbv_physical_snow_response", [](T &o) {
          using S = api::hbv_physical_snow_cell_response_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("priestley_taylor_response", [](T &o) {
          using S = api::priestley_taylor_cell_response_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("actual_evaptranspiration_response", [](T &o) {
          using S = api::actual_evapotranspiration_cell_response_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("kirchner_state", [](T &o) {
          using S = api::kirchner_cell_state_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
      };
      auto [t0, t1] = expose_models<model, opt_model>(m, stack_name);
      expose_cell_stats(t0);
    }
  }

  IMPL_PYEXPORT_STACK()

}
