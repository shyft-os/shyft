/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  using shyft::hydrology::srv::parameter_model;

  using parameter_map_variant_t = std::variant<
    std::map<std::int64_t, std::shared_ptr<shyft::core::pt_gs_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::pt_ss_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::pt_hs_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::pt_hps_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::r_pm_gs_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::pt_st_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::pt_st_hbv::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::r_pt_gs_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::r_pm_st_k::parameter>>,
    std::map<std::int64_t, std::shared_ptr<shyft::core::r_pmv_st_k::parameter>>>;

  void export_parameter_model(py::module_ &m) {
    {
      py::class_<parameter_model, std::shared_ptr<parameter_model>> t(
        m,
        "ParameterModel",
        "A model identifier and a strongly typed dict, cpp-map,  of stack parameters, suitable for use with a "
        "region-model matching the type.");

      t
        .def(py::init())
        .def_readwrite("id", &parameter_model::id)
        .def_property(
          "parameters",
          [](parameter_model &m) {
            return to_std_variant(m.parameters);
          },
          [](parameter_model &m,parameter_map_variant_t v){
            m.parameters = to_boost_variant(v);
          })
        .def(py::self == py::self);

      shyft::pyapi::expose_format(t);
    }
    shyft::pyapi::bind_vector<std::vector<std::shared_ptr<parameter_model>>>(m, "ParameterModelVector", "A strongly typed list of ParameterModels")
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
  }

  void parameter_client_server(py::module_ &m) {
    export_parameter_model(m);
    shyft::energy_market::export_client<
      shyft::energy_market::py_client< shyft::srv::client<parameter_model>>>(
      m, "ParameterClient", "The client api for the shyft.hydrology parameter model service.");
    shyft::energy_market::export_server<
      shyft::energy_market::py_server<shyft::srv::server<shyft::srv::db<parameter_model>>> >(
      m, "ParameterServer", "The server-side component for the shyft.hydrology parameter model repository.");
  }
}
