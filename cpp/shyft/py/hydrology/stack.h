#pragma once

#include <cstdint>
#include <map>
#include <memory>

#include <fmt/core.h>

#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/api/api_state.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/py/hydrology/expose.h>

#include <pybind11/stl_bind.h>

namespace expose {

  namespace _ {
    template <class T>
    bool parameter_ptr_eq(std::shared_ptr<T> const &a, std::shared_ptr<T> const &b) {
      if (a == nullptr && b == nullptr)
        return true;
      if (a != nullptr && b != nullptr)
        return *a == *b;
      return false;
    }

    template <class T>
    bool parameter_map_eq(T const &a, T const &b) {
      if (a.size() != b.size())
        return false;
      for (auto ai = std::begin(a), bi = std::begin(b); ai != std::end(a) && bi != std::end(b); ++ai, ++bi) {
        if (ai->first != bi->first)
          return false;
        if (!parameter_ptr_eq(ai->second, bi->second))
          return false;
      }
      return true;
    }
  }

  template <typename parameter>
  auto expose_stack_parameter(py::module_ &m, char const *name, char const *doc) {

    auto t = py::class_<parameter, std::shared_ptr<parameter>>(m, fmt::format("{}Parameter", name).c_str(), doc);
    t.def(py::init()).def(py::init<parameter const &>(), "clone a parameter", py::arg("p"));
    t.def(
       "serialize",
       &serialize_to_bytes<parameter>,
       "serializes the parameters to a blob, that later can be passed in to .deserialize()")
      .def_static("deserialize", &deserialize_from_bytes<parameter>, py::arg("blob"))
      .def(py::self == py::self)
      .def(py::self != py::self);
    shyft::pyapi::expose_format(t);

    t.def_readwrite("msp", &parameter::msp, "MethodStackParameter: contains the method stack parameters")
      .def("size", &parameter::size, "returns total number of calibration parameters")
      .def("set", &parameter::set, "set parameters from vector/list of float, ordered as by get_name(i)", py::arg("p"))
      .def("get", &parameter::get, "return the value of the i'th parameter, name given by .get_name(i)", py::arg("i"))
      .def(
        "get_name",
        &parameter::get_name,
        "returns the i'th parameter name, see also .get()/.set() and .size()",
        py::arg("i"))

      ;


    using M = std::map<std::int64_t, std::shared_ptr<parameter>>;
    t.attr("map_t") =
      shyft::pyapi::bind_map<M>(m, fmt::format("{}ParameterMap", name))
        .def(
          "__eq__",
          [](M const &m0, M const &m1) {
            return _::parameter_map_eq(m0, m1);
          })
        .def("__neq__", [](M const &m0, M const &m1) {
          return !_::parameter_map_eq(m0, m1);
        });


    return t;
  }

  template <typename state>
  auto expose_stack_state(py::module_ &m, char const *name) {
    auto t = py::class_<state>(m, fmt::format("{}State", name).c_str()).def(py::init());
    t.attr("vector_t") =
      shyft::pyapi::bind_vector<std::vector<state>, std::shared_ptr<std::vector<state>> >(
        m, fmt::format("{}StateVector", name).c_str())
        .def(py::self == py::self)
        .def(py::self != py::self);
    static auto cs_name = fmt::format("{}StateWithId", name);
    static auto cs_doc = fmt::format("{}State: Cell-state", name);
    using CellState = shyft::api::cell_state_with_id<state>;
    auto c =
      py::class_<CellState>(m, cs_name.c_str(), "Keep the cell id and cell state")
        .def(py::init())
        .def(
          py::init<shyft::api::cell_state_id, state>(),
          doc.intro("Creates a cell state with its characteristics cell-id")
            .parameters()
            .parameter("id", "CellStateId", "The cell characteristics id")
            .parameter("state", "", "The cell state (type safe)")(),
          py::arg("id"),
          py::arg("state"))
        .def_readwrite("id", &CellState::id, "int: the cell identifier for the state")
        .def_readwrite("state", &CellState::state, cs_doc.c_str())
        .def_static(
          "cell_state",
          &shyft::api::cell_state_id_of,
          "create a cell state with id for the supplied cell.geo",
          py::arg("geo_cell_data"));
    static auto cs_id_str = [&](CellState const &s) {
      return fmt::format("{}()", cs_name); // FIXME: need fmt of cell_state_id here
    };
    c.def("__str__", cs_id_str);
    c.def("__repr__", cs_id_str);

    auto cv = shyft::pyapi::bind_vector<std::vector<CellState>, std::shared_ptr<std::vector<CellState>>>(
                m, fmt::format("{}Vector", cs_name).c_str())
                .def(py::self == py::self)
                .def(py::self != py::self);
    static auto cv_str = [&](std::shared_ptr<std::vector<CellState>> const &v) {
      return fmt::format("{}Vector()", cs_name); // FIXME: provide/use common str vector instead
    };
    cv.def("__str__", cv_str);
    cv.def("__repr__", cv_str);
    c.attr("vector_t") = cv;

    m.def(
      "extract_state_vector",
      extract_cell_state_vector<state, std::shared_ptr<std::vector<CellState>>>,
      doc
        .intro(
          "Given a cell-state-with-id-vector, returns a pure state vector that can be inserted directly into "
          "region-model")
        .parameters()
        .parameter(
          "cell_state_id_vector",
          "xStateWithIdVector",
          "a complete consistent with region-model vector, all states, as in cell-order")
        .returns("cell_state_vector", "XStateVector", "a vector with cell-id removed, order preserved")(),
      py::arg("cell_state_id_vector"));
    m.def("serialize", shyft::api::serialize_to_bytes<CellState>, "make a blob out of the states", py::arg("states"));
    m.def(
      "deserialize",
      shyft::api::deserialize_from_bytes<CellState>,
      "from a blob, fill in states",
      py::arg("bytes"),
      py::arg("states"));
    m.def(
      "deserialize_from_bytes",
      [](std::vector<char> const &b) {
        auto p = std::make_shared<std::vector<CellState>>();
        shyft::api::deserialize_from_bytes<CellState>(b, p);
        return p;
      },
      py::arg("bytes"));

    cv.def("serialize_to_bytes", shyft::api::serialize_to_bytes<CellState>)
      .def(
        "serialize_to_str",
        [](std::shared_ptr<std::vector<CellState>> &s) {
          return shyft::pyapi::to_hex(shyft::api::serialize_to_bytes(s));
        })
      .def(
        "deserialize_from_str",
        [](std::string const &s) {
          auto p = std::make_shared<std::vector<CellState>>();
          shyft::api::deserialize_from_bytes(shyft::pyapi::to_byte_vector(s), p);
          return p;
        })
      .def_property_readonly("state_vector", extract_cell_state_vector<state, std::shared_ptr<std::vector<CellState>>>);

    return t;
  }

  template <typename all_response, typename discharge_response, typename null_state, typename all_state>
  auto expose_collectors_fwd(py::module_ &m, char const *stack_name) {
    auto rac = py::class_<all_response>(
      m, fmt::format("{}AllCollector", stack_name).c_str(), "collect all cell response from a run");
    auto rd = py::class_<discharge_response>(
      m, fmt::format("{}DischargeCollector", stack_name).c_str(), "collect all cell response from a run");
    auto sn = py::class_<null_state>(
      m,
      fmt::format("{}NullCollector", stack_name).c_str(),
      "A null collector, useful during calibration to minimize memory&maximize speed");
    auto sac = py::class_<all_state>(
      m, fmt::format("StateCollector", stack_name).c_str(), "collects state, if collect_state flag is set to true");
    return std::tuple{rac, rd, sn, sac};
  }

  /**
   * @brief IMPL_PYPEXPORT_STACK
   * @details
   * Used in pt_gs_k.cpp and similar to impl the pyexport(m)
   * functions within the namespace of export::stack
   * where needed functions are declared.
   * FIXME: further refactor to drive the exposure based on
   *        the types in the cell
   * @note
   * Assumes stack_name, _version_string,parameter_state_response
   * and similar symbols are available the point of use.
   */
#define IMPL_PYEXPORT_STACK() \
  void pyexport(py::module_ &m) { \
    m.attr("__doc__") = fmt::format("Shyft python api for the {} model", stack_name); \
    m.def("version", _version_string); \
    parameter_state_response(m); \
    auto [rac, rd, sn, sac] = \
      expose_collectors_fwd<all_response_collector, discharge_collector, null_collector, state_collector>( \
        m, stack_name); \
    cells(m); \
    models(m); \
    collectors(rac, rd, sn, sac); \
    model_calibrator< region_model<cell_discharge_response_t, a_region_environment>>( \
      m, fmt::format("{}Optimizer", stack_name).c_str()); \
  }


}
