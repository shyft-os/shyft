/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/free_water_evaporation.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  namespace py = shyft::py;

  void free_water_evaporation(py::module_ &m) {
    using namespace shyft::core::free_water_evaporation;

    py::class_<response>(m, "FreeWaterEvaporationResponse")
      .def(py::init())
      .def_readwrite("et", &response::et, "float: mm/h");

    typedef calculator FreeWaterEvaporationCalculator;
    py::class_<FreeWaterEvaporationCalculator>(
      m,
      "FreeWaterEvaporationCalculator",
      doc.intro(
        "Evapotranspiration model, Penman or combination method equation for open water, ponds and reservoirs, PM\n"
        "reference ::\n\n"
        "    L. Dingman Penmann evapotranspiration\n"
        "[mm/s] units.")())
      .def(
        "evaporation",
        &FreeWaterEvaporationCalculator::evaporation,
        doc.intro("calculates evapotranspiration, updating response")(),
        py::arg("response"),
        py::arg("dt"),
        py::arg("swin_radiation"),
        py::arg("lw_radiation"),
        py::arg("tempmax"),
        py::arg("tempmin"),
        py::arg("rhumidity"),
        py::arg("water_area"),
        py::arg("albedo"),
        py::arg("elevation"),
        py::arg("windspeed"));
  }
}
