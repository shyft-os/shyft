#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  void api_calibration_status(py::module_ &m) {
    using shyft::hydrology::srv::calibration_status;
    auto cs =
      py::class_<calibration_status>(
        m,
        "CalibrationStatus",
        doc.intro(
          "The CalibrationStatus is returned from drms, or local call to the region model calibration object."
          "It provides status, progress, and current trace of parameters and goal function values")())
        .def_readonly("running", &calibration_status::running, "bool: True if the calibration is still running")
        .def_readonly(
          "trace_goal_function_values",
          &calibration_status::f_trace,
          "DoubleVector: goal function values obtained so far")
        .def_property_readonly(
          "trace_size",
          +[](calibration_status &o) -> int {
            return o.f_trace.size();
          },
          doc.intro("int: returns the size of the parameter-trace")
            .see_also("trace_goal_function_value,trace_parameter")())
        .def(
          "trace_goal_function_value",
          +[](calibration_status &o, int i) {
            return i >= 0 && size_t(i) < o.f_trace.size() ? o.f_trace[i] : shyft::nan;
          },
          doc.intro("returns the i'th goal function value")(),
          py::arg("i"))
        .def(
          "trace_parameter",
          +[](calibration_status &s, int ci) {
            if (!(ci >= 0 && size_t(ci) < s.p_trace.size()))
              throw std::runtime_error(fmt::format("trace_parameter:index out of range: {}", ci));
            return boost::apply_visitor(
              [](auto const &p) -> py::object {
                return py::cast(p);
              },
              s.p_trace[ci]);
          },
          doc.intro("returns the i'th parameter tried, corresponding to the ")
            .intro("i'th trace_goal_function value")(),
          py::arg("i"))
        .def(
          "result",
          +[](calibration_status &s) {
            return boost::apply_visitor(
              [](auto const &p) -> py::object {
                return py::cast(p);
              },
              s.p_result);
          },
          doc.intro("returns resulting calibrated parameter if available")());
    auto calibration_status_str = [](calibration_status const &s) {
      return fmt::format("CalibrationStatus()"); // consider add more info later
    };
    cs.def("__str__", calibration_status_str);
    cs.def("__repr__", calibration_status_str);
  }
}
