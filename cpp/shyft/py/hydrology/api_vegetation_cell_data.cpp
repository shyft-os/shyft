/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/vegetation_cell_data.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/time_axis.h>

namespace expose {
  using namespace shyft::core;

  static shared_ptr<vegetation_cell_data> create_shared_vegetation(vegetation_cell_data const & v) {
    auto r = make_shared<vegetation_cell_data>(v);
    return r;
  }

  void api_vegetation_cell_data(py::module_& m) {

    py::enum_<vegetation_type>(m, "VegetationType")
      .value("forest_tne", vegetation_type::forest_tne)
      .value("forest_bne", vegetation_type::forest_bne)
      .value("forest_bnd", vegetation_type::forest_bnd)
      .value("forest_trbe", vegetation_type::forest_trbe)
      .value("forest_tbe", vegetation_type::forest_tbe)
      .value("forest_trbd", vegetation_type::forest_trbd)
      .value("forest_tbd", vegetation_type::forest_tbd)
      .value("forest_bbd", vegetation_type::forest_bbd)
      .value("shrub_tbe", vegetation_type::shrub_tbe)
      .value("shrub_tbd", vegetation_type::shrub_tbd)
      .value("shrub_bbd", vegetation_type::shrub_bbd)
      .value("grass_arctic", vegetation_type::grass_arctic)
      .value("grass", vegetation_type::grass)
      .value("cropland", vegetation_type::cropland)
      .value("bog", vegetation_type::bog)
      .value("unspecified", vegetation_type::unspecified)
      .export_values();

    typedef shyft::core::vegetation_cell_data VegetationCellData;

    typedef std::shared_ptr<vegetation_cell_data> VegetationShared;

    py::class_<VegetationShared>(m, "VegetationShared", "vector of cell state")
      .def(
        "create_shared_vegetation",
        create_shared_vegetation,
        doc.intro("Given a vegetation, returns shared vegetation")
          .parameters()
          .parameter(
            "VegetationCellData",
            "vegetation",
            "a complete consistent with region-model vector, all states, as in cell-order")())
      ;
    py::class_<VegetationCellData>(
      m,
      "VegetationCellData",
      doc.intro("VegetationCellData is a structure, which carries around vegetation information, \n"
                " for example, leaf-area-index (lai), albedo, vegetation height (hveg),\n"
                "It is designed as a part of GeoCellData")())
      .def(
        py::init(),
        "construct a default vegetation cell data with arctic_grass as a vegetation type and default properties")
      .def(
        py::init<timeaxis_t&>(),
        "construct VegetationCellData specifying timeaxis ta for the properties stored as time-series",
        py::arg("ta"))
      .def(
        "set_vegetation_type",
        &VegetationCellData::set_vegetation_type,
        "specify vegetation type as vegetation_type",
        py::arg("vegetation"))
      .def(
        "set_vegetation_properties",
        &VegetationCellData::set_vegetation_properties,
        "specify vegetation properties: ts lai, ts albedo, double hveg, double lai_max, double  cleaf_max, double sai, "
        "double  k_shelter, double soil_moisture_deficit",
        py::arg("lai"),
        py::arg("albedo"),
        py::arg("hveg"),
        py::arg("lai_max"),
        py::arg("cleaf_max"),
        py::arg("sai"),
        py::arg("k_shelter"),
        py::arg("soil_moisture_deficit"))
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "get_vegetation_type",
        &VegetationCellData::get_vegetation_type,
        "get vegetation type",
        py::return_value_policy::copy)
#if 0
      //FIXME: need some more basic work on vegetation/geo cell data
      .def(
        "get_vegetation_property_lai",
        &VegetationCellData::get_vegetation_property_lai,
        "get vegetation property leaf-area-index",
        py::return_value_policy::copy)
      .def(
        "get_vegetation_property_albedo",
        &VegetationCellData::get_vegetation_property_albedo,
        "get vegetation property albedo",
        py::return_value_policy::copy)
#endif
      .def(
        "get_vegetation_property_hveg",
        &VegetationCellData::get_vegetation_property_hveg,
        "get vegetation property vegetation hight",
        py::return_value_policy::copy)
      .def(
        "get_vegetation_property_lai_max",
        &VegetationCellData::get_vegetation_property_lai_max,
        "get vegetation property maximum leaf-area-index",
        py::return_value_policy::copy)
      .def(
        "get_vegetation_property_cleaf_max",
        &VegetationCellData::get_vegetation_property_cleaf_max,
        "get vegetation property maximum leaf conductance ",
        py::return_value_policy::copy)
      .def(
        "get_vegetation_property_sai",
        &VegetationCellData::get_vegetation_property_sai,
        "get vegetation property steeam index",
        py::return_value_policy::copy)
      .def(
        "get_vegetation_property_ks",
        &VegetationCellData::get_vegetation_property_ks,
        "get vegetation property leaf shelter factor",
        py::return_value_policy::copy)
      .def(
        "get_vegetation_property_soil_moisture_deficit",
        &VegetationCellData::get_vegetation_property_soil_moisture_deficit,
        "get vegetation property soil moisture deficit",
        py::return_value_policy::copy);
  }

}
