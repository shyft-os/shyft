/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/cell_model.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/time_axis.h>

namespace expose {
  void cell_environment(py::module_ &m) {
    namespace py = shyft::py;
    py::class_<shyft::core::environment_t>(
      m, "CellEnvironment", "Contains all ts projected to a certain cell-model using interpolation step (if needed)")
      .def(py::init())
      .def_readwrite("temperature", &shyft::core::environment_t::temperature, "TsFixed: temperature")
      .def_readwrite("precipitation", &shyft::core::environment_t::precipitation, "TsFixed: precipitation")
      .def_readwrite("radiation", &shyft::core::environment_t::radiation, "TsFixed: radiation")
      .def_readwrite("wind_speed", &shyft::core::environment_t::wind_speed, "TsFixed: wind speed")
      .def_readwrite("rel_hum", &shyft::core::environment_t::rel_hum, "TsFixed: relhum")
      .def("init", &shyft::core::environment_t::init, "zero all series, set time-axis ta", py::arg("ta"))
      .def(
        "has_nan_values",
        &shyft::core::environment_t::has_nan_values,
        doc.intro("scans all time-series for nan-values")
          .returns("has_nan", "bool", "true if any nan is encounted, otherwise false")());

    py::enum_<shyft::core::stat_scope>(
      m,
      "stat_scope",
      doc.intro("Defines the scope of the indexes passed to statistics functions")
        .intro(".cell      : the indexes are considered as the i'th cell")
        .intro(".catchment : the indexes are considered catchment identifiers")
        .intro("")
        .intro("The statistics is then covering the cells that matches the selected criteria")())
      .value("cell", shyft::core::stat_scope::cell_ix)
      .value("catchment", shyft::core::stat_scope::catchment_ix)
      .export_values();
  }
}
