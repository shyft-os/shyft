/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/q_adjust_result.h>
#include <shyft/hydrology/target_specification.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/accessor.h>
#include <shyft/time_series/goal_function.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::core {
  using pts_t = time_series::point_ts<time_axis::fixed_dt>;
}

namespace expose {
  using namespace shyft;
  using namespace shyft::core;
  using std::shared_ptr;
  using std::vector;
  using std::int64_t;
  using std::string;

  namespace xf {
    /**
     * @brief transform the supplied time-series
     * @details
     * f(t) interpreted according to its point_interpretation() policy
     * into a new time-series,
     * that represents the true average for each of the n intervals of length dt, starting at start.
     * the result ts will have the policy is set to POINT_AVERAGE_VALUE
     * @note that the resulting ts is a fresh new ts, not connected to the source
     */
    template < class TS, class TSS>
    std::shared_ptr<TS> to_average(utctime start, utctimespan dt, size_t n, const TSS& src) {
      shyft::time_axis::fixed_dt time_axis(start, dt, n);
      shyft::time_series::average_accessor< TSS, shyft::time_axis::fixed_dt> avg(src, time_axis);
      auto r = std::make_shared< TS>(time_axis, 0.0);
      r->set_point_interpretation(shyft::time_series::POINT_AVERAGE_VALUE);
      for (size_t i = 0; i < avg.size(); ++i)
        r->set(i, avg.value(i));
      return r;
    }

    template < class TS, class TSS>
    std::shared_ptr<TS> to_average(utctime start, utctimespan dt, size_t n, shared_ptr<TSS> src) {
      return to_average<TS, TSS>(start, dt, n, *src);
    }
  }

  struct TsTransform {
    shared_ptr<shyft::core::pts_t>
      to_average(utctime start, utctimespan dt, size_t n, shyft::time_series::dd::apoint_ts const & src) {
      return xf::to_average<shyft::core::pts_t, shyft::time_series::dd::apoint_ts>(start, dt, n, src);
    }

    shared_ptr<shyft::core::pts_t>
      to_average(utctime start, utctimespan dt, size_t n, shared_ptr<shyft::time_series::dd::apoint_ts> src) {
      return xf::to_average<shyft::core::pts_t, shyft::time_series::dd::apoint_ts>(start, dt, n, src);
    }

    shared_ptr<shyft::core::pts_t> to_average(utctime start, utctimespan dt, size_t n, shyft::core::pts_t const & src) {
      return xf::to_average<shyft::core::pts_t, shyft::core::pts_t>(start, dt, n, src);
    }

    shared_ptr<shyft::core::pts_t>
      to_average(utctime start, utctimespan dt, size_t n, shared_ptr<shyft::core::pts_t> src) {
      return xf::to_average<shyft::core::pts_t, shyft::core::pts_t>(start, dt, n, src);
    }

    shared_ptr<shyft::core::pts_t>
      to_average(int64_t start, int64_t dt, size_t n, shyft::time_series::dd::apoint_ts const & src) {
      return xf::to_average<shyft::core::pts_t, shyft::time_series::dd::apoint_ts>(seconds(start), seconds(dt), n, src);
    }

    shared_ptr<shyft::core::pts_t>
      to_average(int64_t start, int64_t dt, size_t n, shared_ptr<shyft::time_series::dd::apoint_ts> src) {
      return xf::to_average<shyft::core::pts_t, shyft::time_series::dd::apoint_ts>(seconds(start), seconds(dt), n, src);
    }

    shared_ptr<shyft::core::pts_t> to_average(int64_t start, int64_t dt, size_t n, shyft::core::pts_t const & src) {
      return xf::to_average<shyft::core::pts_t, shyft::core::pts_t>(seconds(start), seconds(dt), n, src);
    }

    shared_ptr<shyft::core::pts_t> to_average(int64_t start, int64_t dt, size_t n, shared_ptr<shyft::core::pts_t> src) {
      return xf::to_average<shyft::core::pts_t, shyft::core::pts_t>(seconds(start), seconds(dt), n, src);
    }
  };

  using shyft::time_series::dd::apoint_ts;
  using target_ts_t = apoint_ts;
  using core_ts_t = shyft::time_series::pts_t;
  using TargetSpecificationPts = model_calibration::target_specification;

  /** custom constructors needed for target-spec, to accept any type of ts
   * and at the same time represent the same efficient core-type at the
   * c++ impl. level
   * TODO: could we simply change the type of target_ts to apoint_ts and accept it as ok performance?
   * \ref boost::python make_constructor
   */
  struct target_specification_ext {

    static TargetSpecificationPts* create_default() {
      return new model_calibration::target_specification();
    }

    static TargetSpecificationPts* create_cids(
      core_ts_t const & ts,
      vector<int64_t> cids,
      double scale_factor,
      model_calibration::target_spec_calc_type calc_mode = model_calibration::NASH_SUTCLIFFE,
      double s_r = 1.0,
      double s_a = 1.0,
      double s_b = 1.0,
      model_calibration::target_property_type catchment_property_ = model_calibration::DISCHARGE,
      std::string uid = "") {
      return acreate_cids(target_ts_t(ts), cids, scale_factor, calc_mode, s_r, s_a, s_b, catchment_property_, uid);
    }

    static TargetSpecificationPts* acreate_cids(
      shyft::time_series::dd::apoint_ts const & ats,
      vector<int64_t> const & cids,
      double scale_factor,
      model_calibration::target_spec_calc_type calc_mode = model_calibration::NASH_SUTCLIFFE,
      double s_r = 1.0,
      double s_a = 1.0,
      double s_b = 1.0,
      model_calibration::target_property_type catchment_property_ = model_calibration::DISCHARGE,
      std::string uid = "") {
      return new model_calibration::target_specification(
        target_ts_t(ats.time_axis(), ats.values(), ats.point_interpretation()),
        cids,
        scale_factor,
        calc_mode,
        s_r,
        s_a,
        s_b,
        catchment_property_,
        uid);
    }

    static TargetSpecificationPts* create_cids2(
      core_ts_t const & ts,
      vector<int64_t> const & cids,
      double scale_factor,
      model_calibration::target_spec_calc_type calc_mode) {
      return create_cids(ts, cids, scale_factor, calc_mode);
    }

    static TargetSpecificationPts* acreate_cids2(
      shyft::time_series::dd::apoint_ts const & ats,
      vector<int64_t> const & cids,
      double scale_factor,
      model_calibration::target_spec_calc_type calc_mode) {
      return acreate_cids(ats, cids, scale_factor, calc_mode);
    }

    static TargetSpecificationPts* create_rid(
      core_ts_t const & ts,
      int64_t river_id,
      double scale_factor,
      model_calibration::target_spec_calc_type calc_mode = model_calibration::NASH_SUTCLIFFE,
      double s_r = 1.0,
      double s_a = 1.0,
      double s_b = 1.0,
      std::string uid = "") {
      return acreate_rid(target_ts_t(ts), river_id, scale_factor, calc_mode, s_r, s_a, s_b, uid);
    }

    static TargetSpecificationPts* acreate_rid(
      shyft::time_series::dd::apoint_ts const & ats,
      int64_t river_id,
      double scale_factor,
      model_calibration::target_spec_calc_type calc_mode = model_calibration::NASH_SUTCLIFFE,
      double s_r = 1.0,
      double s_a = 1.0,
      double s_b = 1.0,
      std::string uid = "") {
      return new model_calibration::target_specification(
        target_ts_t(ats.time_axis(), ats.values(), ats.point_interpretation()),
        river_id,
        scale_factor,
        calc_mode,
        s_r,
        s_a,
        s_b,
        uid);
    }
  };

  void target_specification(py::module_& m) {
    py::enum_<model_calibration::target_spec_calc_type>(m, "TargetSpecCalcType")
      .value("NASH_SUTCLIFFE", model_calibration::NASH_SUTCLIFFE)
      .value("KLING_GUPTA", model_calibration::KLING_GUPTA)
      .value("ABS_DIFF", model_calibration::ABS_DIFF)
      .value("RMSE", model_calibration::RMSE)
      .export_values();
    py::enum_<model_calibration::target_property_type>(m, "CatchmentPropertyType")
      .value("DISCHARGE", model_calibration::DISCHARGE)
      .value("SNOW_COVERED_AREA", model_calibration::SNOW_COVERED_AREA)
      .value("SNOW_WATER_EQUIVALENT", model_calibration::SNOW_WATER_EQUIVALENT)
      .value("ROUTED_DISCHARGE", model_calibration::ROUTED_DISCHARGE)
      .value("CELL_CHARGE", model_calibration::CELL_CHARGE)
      .export_values();

    py::class_<TargetSpecificationPts> t(
      m,
      "TargetSpecificationPts",
      "To guide the model calibration, we have a goal-function that we try to minimize\n"
      "This class contains the needed specification of this goal-function so that we can:\n\n"
      " 1. from simulations, collect time-series at catchment level for "
      "(DISCHARGE|SNOW_COVERED_AREA|SNOW_WATER_EQUIVALENT)\n"
      " 2. from observations, have a user-specified series the expression above should be equal to\n"
      " 3. use user specified kling-gupta factors to evaluate kind-of-difference between target and simulated\n"
      " 4. scale-factor to put a weight on this specific target-specification compared to other(we can have multiple)\n"
      " 5. a user specified id, uid, a string to identify the external source for calibration\n"
      " 6. a user specified time-series expression with symbolic ref to `sim`, like `TimeSeries('sim').log()`\n"
      "\n");

    t.def(py::init(&target_specification_ext::create_default))
      .def(
        py::init(&target_specification_ext::create_cids),
        doc.intro("construct a target specification filled in with supplied parameters")
          .parameters()
          .parameter("ts", "TsFixed", "time-series containing the target time-series")
          .parameter(
            "cids", "IntVector", "A list of catchment id's(cids) that together adds up into same as the target-ts")
          .parameter("scale_factor", "float", "the weight of this target-specification")
          .parameter(
            "calc_mode", "TargetSpecCalcType", "specifies how to calculate the goal function, NS, KG, Abs,RMSE method")
          .parameter("s_r", "float", "KG scalefactor for correlation")
          .parameter("s_a", "float", "KG scalefactor for alpha(variance)")
          .parameter("s_b", "float", "KG scalefactor for beta(bias)")
          .parameter(
            "catchment_property",
            "CatchmentPropertyType",
            "what to extract from "
            "catchment(DISCHARGE|SNOW_COVERED_AREA|SNOW_WATER_EQUIVALENT|ROUTED_DISCHARGE|CELL_CHARGE)")
          .parameter("uid", "str", "user specified string/id to help integration efforts")(),
        py::arg("ts"),
        py::arg("cids"),
        py::arg("scale_factor"),
        py::arg("calc_mode"),
        py::arg("s_r"),
        py::arg("s_a"),
        py::arg("s_b"),
        py::arg("catchment_property"),
        py::arg("uid"))
      .def(
        py::init(&target_specification_ext::acreate_cids),
        doc.intro("construct a target specification filled in with supplied parameters")
          .parameters()
          .parameter(
            "ts",
            "TimeSeries",
            "time-series containing the target time-series, note that the time-axis of this ts must be a "
            "fixed-interval type")
          .parameter(
            "cids", "IntVector", "A list of catchment id's(cids) that together adds up into same as the target-ts")
          .parameter("scale_factor", "float", "the weight of this target-specification")
          .parameter(
            "calc_mode", "TargetSpecCalcType", "specifies how to calculate the goal function, NS, KG, Abs method")
          .parameter("s_r", "float", "KG scalefactor for correlation")
          .parameter("s_a", "float", "KG scalefactor for alpha(variance)")
          .parameter("s_b", "float", "KG scalefactor for beta(bias)")
          .parameter(
            "catchment_property",
            "CatchmentPropertyType",
            "what to extract from "
            "catchment(DISCHARGE|SNOW_COVERED_AREA|SNOW_WATER_EQUIVALENT|ROUTED_DISCHARGE|CELL_CHARGE)")
          .parameter("uid", "str", "user specified string/id to help integration efforts")(),
        py::arg("ts"),
        py::arg("cids"),
        py::arg("scale_factor"),
        py::arg("calc_mode"),
        py::arg("s_r"),
        py::arg("s_a"),
        py::arg("s_b"),
        py::arg("catchment_property"),
        py::arg("uid"))
      .def(
        py::init(&target_specification_ext::create_cids2),
        doc.intro("construct a target specification filled in with supplied parameters")
          .parameters()
          .parameter("ts", "TsFixed", "time-series containing the target time-series")
          .parameter(
            "cids", "IntVector", "A list of catchment id's(cids) that together adds up into same as the target-ts")
          .parameter("scale_factor", "float", "the weight of this target-specification")
          .parameter(
            "calc_mode", "TargetSpecCalcType", "specifies how to calculate the goal function, NS, KG, Abs method")(),
        py::arg("ts"),
        py::arg("cids"),
        py::arg("scale_factor"),
        py::arg("calc_mode"))
      .def(
        py::init(&target_specification_ext::acreate_cids2),
        doc.intro("construct a target specification filled in with supplied parameters")
          .parameters()
          .parameter(
            "ts",
            "TimeSeries",
            "time-series containing the target time-series, note the time-axis needs to be fixed_dt!")
          .parameter(
            "cids", "IntVector", "A list of catchment id's(cids) that together adds up into same as the target-ts")
          .parameter("scale_factor", "float", "the weight of this target-specification")
          .parameter(
            "calc_mode", "TargetSpecCalcType", "specifies how to calculate the goal function, NS, KG, Abs method")(),
        py::arg("ts"),
        py::arg("cids"),
        py::arg("scale_factor"),
        py::arg("calc_mode"))
      .def(
        py::init(&target_specification_ext::create_rid),
        doc.intro("construct a target specification filled in with supplied parameters")
          .parameters()
          .parameter("ts", "TsFixed", "time-series containing the target time-series")
          .parameter("rid", "int", "A river-id identifying the point of flow in the river-network")
          .parameter("scale_factor", "float", "the weight of this target-specification")
          .parameter(
            "calc_mode", "TargetSpecCalcType", "specifies how to calculate the goal function, NS, KG, Abs method")
          .parameter("s_r", "float", "KG scalefactor for correlation")
          .parameter("s_a", "float", "KG scalefactor for alpha(variance)")
          .parameter("s_b", "float", "KG scalefactor for beta(bias)")
          .parameter("uid", "str", "user specified string/id to help integration efforts")(),
        py::arg("ts"),
        py::arg("rid"),
        py::arg("scale_factor"),
        py::arg("calc_mode"),
        py::arg("s_r"),
        py::arg("s_a"),
        py::arg("s_b"),
        py::arg("uid"))
      .def(
        py::init(&target_specification_ext::acreate_rid),
        doc.intro("construct a target specification filled in with supplied parameters")
          .parameters()
          .parameter(
            "ts",
            "TimeSeries",
            "time-series containing the target time-series, note time-axis required to be fixed-dt type")
          .parameter("rid", "int", "A river-id identifying the point of flow in the river-network")
          .parameter("scale_factor", "float", "the weight of this target-specification")
          .parameter(
            "calc_mode", "TargetSpecCalcType", "specifies how to calculate the goal function, NS, KG, Abs method")
          .parameter("s_r", "float", "KG scalefactor for correlation")
          .parameter("s_a", "float", "KG scalefactor for alpha(variance)")
          .parameter("s_b", "float", "KG scalefactor for beta(bias)")
          .parameter("uid", "str", "user specified string/id to help integration efforts")(),
        py::arg("ts"),
        py::arg("rid"),
        py::arg("scale_factor"),
        py::arg("calc_mode"),
        py::arg("s_r"),
        py::arg("s_a"),
        py::arg("s_b"),
        py::arg("uid"))
      .def_readwrite(
        "scale_factor",
        &TargetSpecificationPts::scale_factor,
        "float: the scale factor to be used when considering multiple target_specifications")
      .def_readwrite(
        "calc_mode",
        &TargetSpecificationPts::calc_mode,
        "TargetSpecCalcType: NASH_SUTCLIFFE, KLING_GUPTA,ABS_DIFF,RMSE")
      .def_readwrite(
        "catchment_property",
        &TargetSpecificationPts::catchment_property,
        "CatchmentPropertyType: DISCHARGE,SNOW_COVERED_AREA, SNOW_WATER_EQUIVALENT")
      .def_readwrite("s_r", &TargetSpecificationPts::s_r, "float: KG-scalefactor for correlation")
      .def_readwrite("s_a", &TargetSpecificationPts::s_a, "float: KG-scalefactor for alpha (variance)")
      .def_readwrite("s_b", &TargetSpecificationPts::s_b, "float: KG-scalefactor for beta (bias)")
      .def_readwrite("ts", &TargetSpecificationPts::ts, "TimeSeries: target ts")
      .def_readwrite(
        "river_id", &TargetSpecificationPts::river_id, "int: river identifier for routed discharge calibration")
      .def_readwrite(
        "catchment_indexes", &TargetSpecificationPts::catchment_indexes, "IntVector: catchment indexes, 'cids'")
      .def_readwrite("uid", &TargetSpecificationPts::uid, "str: user specified identifier:string")
      .def_property(
        "fx",
        +[](TargetSpecificationPts const & me) {
          return me.fx;
        },
        +[](TargetSpecificationPts& me, apoint_ts const & x) {
          if (x.ts) {
            if (x.find_ts_bind_info().empty())
              throw std::runtime_error(
                "The fx expression should have a symbolic ts reference, like TimeSeries(\"sim\")");
          }
          me.fx = x;
        },
        "TimeSeries: an expression to be applied to the simulated ts, e.g.:`TimeSeries('sim').log()`")
      .def(
        "evaluate_fx",
        +[](TargetSpecificationPts const & me, apoint_ts const & x) -> apoint_ts {
          return me.fx.ts ? shyft::core::model_calibration::evaluate_ts_fx(me.fx, x) : x;
        },
        doc.intro("Compute the evaluated result using the `fx` expression.")
          .parameters()
          .parameter("ts", "TimeSeries", "")
          .returns(
            "result",
            "TimeSeries",
            "The evaluated result using the passed time-series as substitute to symbolic reference(s).")(),
        py::arg("ts"));
    shyft::pyapi::expose_format(t);

    pyapi::bind_vector<std::vector<TargetSpecificationPts>>(
      m,
      "TargetSpecificationVector",
      "A list of (weighted) target specifications to be used for model calibration")
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;

    shared_ptr<shyft::core::pts_t> (TsTransform::*m1)(
      utctime, utctimespan, size_t, shyft::time_series::dd::apoint_ts const &) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m2)(
      utctime, utctimespan, size_t, shared_ptr<shyft::time_series::dd::apoint_ts>) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m3)(
      utctime, utctimespan, size_t, shyft::core::pts_t const &) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m4)(
      utctime, utctimespan, size_t, shared_ptr<shyft::core::pts_t>) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m1i)(
      int64_t, int64_t, size_t, shyft::time_series::dd::apoint_ts const &) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m2i)(
      int64_t, int64_t, size_t, shared_ptr<shyft::time_series::dd::apoint_ts>) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m3i)(
      int64_t, int64_t, size_t, shyft::core::pts_t const &) = &TsTransform::to_average;
    shared_ptr<shyft::core::pts_t> (TsTransform::*m4i)(
      int64_t, int64_t, size_t, shared_ptr<shyft::core::pts_t>) = &TsTransform::to_average;

    py::class_<TsTransform>(
      m,
      "TsTransform",
      "transform the supplied time-series, f(t) interpreted according to its point_interpretation() policy\n"
      "into a new shyft core TsFixed time-series,\n"
      "that represents the true average for each of the n intervals of length dt, starting at start.\n"
      "the result ts will have the policy is set to POINT_AVERAGE_VALUE\n"
      "note:  that the resulting ts is a fresh new ts, not connected to the source ts\n"
      "\n")
      .def(py::init())
      .def("to_average", m1, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m2, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m3, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m4, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m1i, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m2i, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m3i, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "")
      .def("to_average", m4i, py::arg("start"), py::arg("dt"), py::arg("n"), py::arg("src").noconvert(), "");

    py::class_<q_adjust_result>(
      m, "FlowAdjustResult", doc.intro("The result type of region-model .adjust_state_to_target_flow(..) method")())
      .def(py::init())
      .def_readwrite(
        "q_0", &q_adjust_result::q_0, doc.intro("float: The flow m3/s from selected catchments before tuning")())
      .def_readwrite("q_r", &q_adjust_result::q_r, doc.intro("float: The obtaioned flow m3/s after tuning")())
      .def_readwrite(
        "diagnostics",
        &q_adjust_result::diagnostics,
        doc.intro("str: If tuning failed, the diagnostics of failure, zero length/empty if success")());
  }
}
