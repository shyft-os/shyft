/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <fstream>

#include <boost/dll.hpp>

#include <shyft/core/fs_compat.h>
#include <shyft/py/hydrology/api.h>

char const *version() {
  return "v5.x";
}

namespace expose {
  extern void api_geo_cell_data(py::module_ &);
  extern void hydrology_vectors(py::module_ &);
  extern void target_specification(py::module_ &);
  extern void region_environment(py::module_ &);
  extern void priestley_taylor(py::module_ &);
  extern void actual_evapotranspiration(py::module_ &);
  extern void gamma_snow(py::module_ &);
  extern void kirchner(py::module_ &);
  extern void precipitation_correction(py::module_ &);
  extern void hbv_snow(py::module_ &);
  extern void snow_tiles(py::module_ &);
  extern void hbv_physical_snow(py::module_ &);
  extern void cell_environment(py::module_ &);
  extern void interpolation(py::module_ &);
  extern void skaugen_snow(py::module_ &);
  extern void kalman(py::module_ &);
  extern void hbv_soil(py::module_ &);
  extern void hbv_tank(py::module_ &);
  extern void hbv_actual_evapotranspiration(py::module_ &);
  extern void glacier_melt(py::module_ &);
  extern void routing(py::module_ &);
  extern void api_cell_state_id(py::module_ &);
  extern void radiation(py::module_ &);
  extern void penman_monteith(py::module_ &);
  extern void penman_monteith_vegetation(py::module_ &);
  extern void drms(py::module_ &);
  extern void api_calibration_status(py::module_ &);
  extern void api_calibration_options(py::module_ &);
  extern void api_parameter_optimizer(py::module_ &);
  extern void cf_time(py::module_ &);
  extern void gcd_client_server(py::module_ &);
  extern void state_client_server(py::module_ &);
  extern void parameter_client_server(py::module_ &);
  extern void goal_function_model_client_server(py::module_ &);
  extern void free_water_evaporation(py::module_ &);

  void hydrology_basics(py::module_ &m) {
    routing(m);
    api_cell_state_id(m);
    api_geo_cell_data(m);
    cf_time(m);

    target_specification(m);
    region_environment(m);
    hydrology_vectors(m);

    precipitation_correction(m);
    priestley_taylor(m);
    actual_evapotranspiration(m);
    gamma_snow(m);
    skaugen_snow(m);
    hbv_snow(m);
    snow_tiles(m);
    hbv_physical_snow(m);
    kirchner(m);
    cell_environment(m);
    interpolation(m);
    kalman(m);
    hbv_soil(m);
    hbv_tank(m);
    hbv_actual_evapotranspiration(m);
    glacier_melt(m);
    radiation(m);
    penman_monteith(m);
    penman_monteith_vegetation(m);
    free_water_evaporation(m);
    api_calibration_status(m);
    api_calibration_options(m);
    api_parameter_optimizer(m);
  }

  namespace r_pt_gs_k {
    extern void pyexport(py::module_ &);
  }

  namespace r_pm_gs_k {
    extern void pyexport(py::module_ &);
  }

  namespace r_pm_st_k {
    extern void pyexport(py::module_ &);
  }

  namespace r_pmv_st_k {
    extern void pyexport(py::module_ &);
  }

  namespace pt_gs_k {
    extern void pyexport(py::module_ &);
  }

  namespace pt_ss_k {
    extern void pyexport(py::module_ &);
  }

  namespace pt_hs_k {
    extern void pyexport(py::module_ &);
  }

  namespace pt_st_k {
    extern void pyexport(py::module_ &);
  }

  namespace pt_st_hbv {
    extern void pyexport(py::module_ &);
  }

  namespace pt_hps_k {
    extern void pyexport(py::module_ &);
  }

  void hydrology_stacks(py::module_ &m) {
#define stack_sub_module(x) \
  { \
    auto sm = m.def_submodule(#x); \
    x::pyexport(sm); \
  }
    stack_sub_module(r_pt_gs_k);
    stack_sub_module(r_pm_gs_k);
    stack_sub_module(r_pm_st_k);
    stack_sub_module(r_pmv_st_k);
    stack_sub_module(pt_gs_k);
    stack_sub_module(pt_ss_k);
    stack_sub_module(pt_hs_k);
    stack_sub_module(pt_st_k);
    stack_sub_module(pt_st_hbv);
    stack_sub_module(pt_hps_k);
#undef stack_sub_module
  }

  void hydrology_servers(py::module_ &m) {
    drms(m);
    gcd_client_server(m);
    state_client_server(m);
    parameter_client_server(m);
    goal_function_model_client_server(m);
  }

  void hydrology(py::module_ &m) {

    // note bw-compat only: re-export from time-series module
    auto const time_series = py::module_::import("shyft.time_series");
    for (auto const name : {"GeoPoint", "TimeAxis", "TsVector", "GeoPointVector", "DoubleVector"})
      m.attr(name) = time_series.attr(name);

    m.attr("__doc__") = "Shyft hydrology python api providing basic types";
    m.def("version", version);
    hydrology_basics(m);
    hydrology_stacks(m);
    hydrology_servers(m);
  }
}

SHYFT_PYTHON_MODULE(hydrology, m) {
  using namespace shyft;
  expose::hydrology(m);
  try {
    pyapi::make_python_namespace(m, boost::dll::this_line_location().parent_path() / "hydrology");
    auto shyft_data_dir = (boost::dll::this_line_location().parent_path().parent_path() / "shyft-data").string();
    if (std::getenv("SHYFT_DATA") != nullptr)
      shyft_data_dir = std::getenv("SHYFT_DATA");
    m.attr("shyftdata_dir") = shyft_data_dir;
  } catch (std::exception const &e) {
    throw std::runtime_error(
      fmt::format("Failed to set __path__  or the shyftdata_dir property for shyft.hydrology: {}", e.what()));
  }
}
