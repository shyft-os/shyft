/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/skaugen.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>

namespace expose {

  void skaugen_snow(py::module_ &m) {
    using namespace shyft::core::skaugen;

    py::class_<parameter>(m, "SkaugenParameter")
      .def(
        py::init<double, double, double, double, double, double, double, double>(),
        py::arg("alpha_0") = 40.77,
        py::arg("d_range") = 113.0,
        py::arg("unit_size") = 0.1,
        py::arg("max_water_fraction") = 0.1,
        py::arg("tx") = 0.16,
        py::arg("cx") = 2.5,
        py::arg("ts") = 0.14,
        py::arg("cfr") = 0.01)
      .def(py::init< parameter const &>(), "create a clone of p", py::arg("p"))
      .def_readwrite("alpha_0", &parameter::alpha_0, "float: default = 40.77")
      .def_readwrite("d_range", &parameter::d_range, "float: default = 113.0")
      .def_readwrite("unit_size", &parameter::unit_size, "float: default = 0.1")
      .def_readwrite("max_water_fraction", &parameter::max_water_fraction, "float: default = 0.1")
      .def_readwrite("tx", &parameter::tx, "float: default = 0.16")
      .def_readwrite("cx", &parameter::cx, "float: default = 2.5")
      .def_readwrite("ts", &parameter::ts, "float: default = 0.14")
      .def_readwrite("cfr", &parameter::cfr, "float: default = 0.01");

    py::class_<state>(m, "SkaugenState")
      .def(
        py::init<double, double, double, double, double, double, double>(),
        py::arg("nu") = 4.077,
        py::arg("alpha") = 40.77,
        py::arg("sca") = 0.0,
        py::arg("swe") = 0.0,
        py::arg("free_water") = 0.0,
        py::arg("residual") = 0.0,
        py::arg("num_units") = 0)
      .def(py::init< state const &>(), "create a clone of s", py::arg("s"))
      .def_readwrite("nu", &state::nu, "float: ..")
      .def_readwrite("alpha", &state::alpha, "float: ..")
      .def_readwrite("sca", &state::sca, "float: ..")
      .def_readwrite("swe", &state::swe, "float: ..")
      .def_readwrite("free_water", &state::free_water, "float: ..")
      .def_readwrite("residual", &state::residual, "float: ..")
      .def_readwrite("num_units", &state::num_units, "int: ..");

    py::class_<response>(m, "SkaugenResponse")
      .def_readwrite("sca", &response::sca, "float: snow-covered area in fraction")
      .def_readwrite("swe", &response::swe, "float: snow water equivalient [mm] over cell-area")
      .def_readwrite("outflow", &response::outflow, "float: from snow-routine in [mm/h] over cell-area")
      .def_readwrite("total_stored_water", &response::swe, "float: same as swe(deprecated)");

    py::class_<calculator>(
      m,
      "SkaugenCalculator",
      "Skaugen snow model method\n"
      "\n"
      "This algorithm uses theory from Skaugen \n"
      "\n")
      .def(
        "step",
        &calculator::step,
        "steps the model forward delta_t seconds, using specified input, updating state and response",
        py::arg("delta_t"),
        py::arg("parameter"),
        py::arg("temperature"),
        py::arg("precipitation"),
        py::arg("radiation"),
        py::arg("wind_speed"),
        py::arg("state"),
        py::arg("response"));
  }
}
