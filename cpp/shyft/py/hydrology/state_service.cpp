/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <memory>
#include <vector>

#include <boost/variant.hpp>

#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  using shyft::hydrology::srv::state_model;

  using state_variant_t = std::variant<
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_gs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_ss_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_hs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_hps_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pm_gs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_st_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_st_hbv::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pt_gs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pm_st_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pmv_st_k::state_t>>>>;

  void export_state_model(py::module_ &m) {
    {
      py::class_<state_model, std::shared_ptr<state_model>> t(
        m,
        "StateModel",
        "A model identifier and a StateVector, where the vector can keep an uniform set of StateId of specified type");

      t
        .def(py::init())
        .def_readwrite("id", &state_model::id, "int: unique model id")
        .def_property(
          "states",
          [](state_model &s) {
            return to_std_variant(s.states);
          },
          [](state_model &m,state_variant_t v){
            m.states = to_boost_variant(std::move(v));
          })
        .def(py::self == py::self);
      shyft::pyapi::expose_format(t);
    }
    shyft::pyapi::bind_vector<std::vector<std::shared_ptr<state_model>>>(
      m, "StateModelVector", "A strongly typed list of StateModel's")
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
  }

  void state_client_server(py::module_ &m) {
    export_state_model(m);
    shyft::energy_market::export_client< shyft::energy_market::py_client<shyft::srv::client<state_model>>>(
      m, "StateClient", "The client api for the shyft.hydrology state model service.");
    shyft::energy_market::export_server<
      shyft::energy_market::py_server<shyft::srv::server<shyft::srv::db<state_model>>>>(
      m, "StateServer", "The server-side component for the shyft.hydrology state model repository.");
  }

}
