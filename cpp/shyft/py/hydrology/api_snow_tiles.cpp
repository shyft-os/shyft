/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {

  void snow_tiles(py::module_ &m) {
    using namespace shyft::core::snow_tiles;
    using std::vector;

    py::class_<parameter>(m, "SnowTilesParameter")
      .def(
        py::init<double, double, double, double, double, double, vector<double>>(),
        py::arg("shape") = 2.0,
        py::arg("tx") = 0.0,
        py::arg("cx") = 1.,
        py::arg("ts") = 0.0,
        py::arg("lwmax") = 0.1,
        py::arg("cfr") = 0.5,
        py::arg("area_fractions") = std::vector{0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1})
      .def_readwrite(
        "tx", &parameter::tx, "float: threshold temperature determining precipitation phase, unit (C), range [-4, 4]")
      .def_readwrite("cx", &parameter::cx, "float: degree-day melt factor, unit (mm/C/day), range [0, 30]")
      .def_readwrite("ts", &parameter::ts, "float: threshold temperature for melt onset, unit (C), range [-4, 4]")
      .def_readwrite(
        "lwmax",
        &parameter::lwmax,
        "float: max liquid water content given as a fraction of ice in the snowpack, unit (-), range [0, 1]")
      .def_readwrite("cfr", &parameter::cfr, "float: refreeze coefficient, unit (-), range [0, 1]")
      .def_property(
        "area_fractions",
        py::cpp_function(&parameter::get_area_fractions, py::return_value_policy::copy),
        &parameter::set_area_fractions,
        "DoubleVector: area fractions of the individual tiles, unit (-), range per tile [0.01, 1], the sum of the "
        "elements in the vector must equal 1")
      .def_property(
        "shape",
        &parameter::get_shape,
        &parameter::set_shape,
        "float: shape parameter of the Gamma distribution defining the multiplication factors for snowfall, unit (-), "
        "range [0.1, inf]")
      .def_property_readonly(
        "_multiply",
        [](const parameter &p){
          return shyft::pyapi::make_array_ref(p.get_multiply());
        },
        "DoubleVector: multiplication factors for distributing solid precipitation between the tiles (mean of elements "
        "= 1)")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::class_<state>(m, "SnowTilesState")
      .def(py::init())
      .def(py::init<vector<double>, vector<double>>(), py::arg("fw"), py::arg("lw"))
      .def_readwrite("fw", &state::fw, "DoubleVector: frozen water in the snowpack, unit (mm)")
      .def_readwrite("lw", &state::lw, "DoubleVector: liquid water in the snowpack, unit (mm)")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::class_<response>(m, "SnowTilesResponse")
      .def(py::init())
      .def_readwrite("outflow", &response::outflow, "average outflow, rain and snowmelt, for the cell, unit (mm/hour)")
      .def_readwrite("swe", &response::swe, "float: average swe for the cell, unit (mm)")
      .def_readwrite("sca", &response::sca, "float: average sca as a fraction of total cell area, unit (-)");

    py::class_<calculator>(
      m,
      "SnowTilesCalculator",
      "Tile based snow model method\n"
      "\n"
      "This algorithm uses ...\n"
      "\n")
      .def(py::init<parameter const &>(), py::arg("parameter"))
      .def(
        "step",
        &calculator::step,
        doc.intro("steps the model forward from t0 to t1, updating state and response")(),
        py::arg("state"),
        py::arg("response"),
        py::arg("t0"),
        py::arg("t1"),
        py::arg("precipitation"),
        py::arg("temperature"));
  }
}
