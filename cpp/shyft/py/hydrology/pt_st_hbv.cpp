/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/methods/hbv_soil.h>
#include <shyft/hydrology/methods/hbv_tank.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/hydrology/model_calibration.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/stacks/pt_st_hbv.h>
#include <shyft/hydrology/stacks/pt_st_hbv_cell_model.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/stack.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/version.h>

namespace expose::pt_st_hbv {
  using namespace shyft::core;
  using namespace shyft::core::pt_st_hbv;
  constexpr auto stack_name = "PTSTHBV";

  static void parameter_state_response(py::module_ &m) {
    auto pp = expose_stack_parameter<parameter>(
      m,
      stack_name,
      "Contains the parameters to the methods used in the PTSTHBV assembly\n"
      "priestley_taylor, snow_tiles, precipitation_correction, hbv_soil, hbv_tank\n");
    pp.def(
        py::init<
          priestley_taylor::parameter const &,
          snow_tiles::parameter const &,
          hbv_soil::parameter const &,
          hbv_tank::parameter const &,
          precipitation_correction::parameter const &,
          glacier_melt::parameter,
          routing::uhg_parameter,
          mstack_parameter >(),
        py::arg("pt"),
        py::arg("snow"),
        py::arg("hbv_soil"),
        py::arg("hbv_tank"),
        py::arg("p_corr"),
        py::arg("gm") = glacier_melt::parameter{},
        py::arg("routing") = routing::uhg_parameter{},
        py::arg("msp") = mstack_parameter{})
      .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
      .def_readwrite("st", &parameter::st, "SnowTilesParameter: snow_tiles parameter")
      .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
      .def_readwrite("soil", &parameter::soil, "HbvSoilParameter: hbv soil parameter")
      .def_readwrite("tank", &parameter::tank, "HbvTankParameter: hbv tank parameter")
      .def_readwrite(
        "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
      .def_readwrite(
        "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters");

    expose_stack_state<state>(m, stack_name)
      .def(
        py::init<snow_tiles::state, hbv_soil::state, hbv_tank::state>(),
        py::arg("snow"),
        py::arg("hbv_soil"),
        py::arg("hbv_tank"))
      .def_readwrite("snow", &state::snow, "SnowTilesState: snow_tiles state")
      .def_readwrite("soil", &state::soil, "HbvSoilState: hbv_soil state")
      .def_readwrite("tank", &state::tank, "HbvTankState: hbv_tank state");

    py::class_<response>(
      m, "PTSTHBVResponse", "This struct contains the responses of the methods used in the PTSTHBV assembly")
      .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: priestley_taylor response")
      .def_readwrite("snow", &response::snow, "SnowTilesResponse: snow-tiles method response")
      .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "float: glacier melt response[m3s]")
      .def_readwrite("soil", &response::soil, "ActualEvapotranspirationResponse: hbv_soil response")
      .def_readwrite("tank", &response::tank, "HbvTankResponse: hbv_tank response")
      .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
  }

  static void collectors(auto &rac, auto &rd, [[maybe_unused]] auto &sn, auto &sac) {
    rac.def_readonly("destination_area", &all_response_collector::destination_area, "float: a copy of cell area [m2]")
      .def_readonly(
        "avg_discharge",
        &all_response_collector::avg_discharge,
        "TsFixed: HBV Discharge given in [m3/s] for the timestep")

      .def_readonly(
        "soil_ae",
        &all_response_collector::soil_ae,
        "TsFixed: HBV soil actual evaporation given in [mm/h] for the timestep")
      .def_readonly(
        "inuz",
        &all_response_collector::inuz,
        "TsFixed: HBV soil perculation to upper zone given in [mm/h] for the timestep")
      .def_readonly(
        "elake", &all_response_collector::elake, "TsFixed: HBV tank lake evaporation given in [mm/h] for the timestep")
      .def_readonly("qlz", &all_response_collector::qlz, "TsFixed: HBV tank lower zone in [m^3/s] for the timestep")
      .def_readonly(
        "quz0", &all_response_collector::quz0, "TsFixed: HBV tank upper zone slow response in [m^3/s] for the timestep")
      .def_readonly(
        "quz1", &all_response_collector::quz1, "TsFixed: HBV tank upper zone mid response in [m^3/s] for the timestep")
      .def_readonly(
        "quz2", &all_response_collector::quz2, "TsFixed: HBV tank upper zone fast response in [m^3/s] for the timestep")

      .def_readonly(
        "snow_sca",
        &all_response_collector::snow_sca,
        "TsFixed: snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
      .def_readonly(
        "snow_swe",
        &all_response_collector::snow_swe,
        "TsFixed: snow swe, [mm] over the cell sca.. area, - at the end of timestep")
      .def_readonly(
        "snow_outflow", &all_response_collector::snow_outflow, "TsFixed: snow output [m^3/s] for the timestep")
      .def_readonly(
        "glacier_melt",
        &all_response_collector::glacier_melt,
        "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
      .def_readonly("pe_output", &all_response_collector::pe_output, "TsFixed: pot evap mm/h")
      .def_readonly(
        "end_response", &all_response_collector::end_response, "PTSTHBVResponse: end_response, at the end of collected")
      .def_readonly("avg_charge", &all_response_collector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

    rd.def_readonly("destination_area", &discharge_collector::destination_area, "float: a copy of cell area [m2]")
      .def_readonly("cell_area", &discharge_collector::destination_area, "float: a copy of cell area [m2]")
      .def_readonly(
        "avg_discharge",
        &discharge_collector::avg_discharge,
        "TsFixed: HBV Discharge given in [m^3/s] for the timestep")
      .def_readonly(
        "snow_sca",
        &discharge_collector::snow_sca,
        "TsFixed: snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
      .def_readonly(
        "snow_swe",
        &discharge_collector::snow_swe,
        "TsFixed: snow swe, [mm] over the cell area, - at the end of timestep")
      .def_readonly(
        "end_response", &discharge_collector::end_response, "PTSTHBVResponse: end_response, at the end of collected")
      .def_readwrite("collect_snow", &discharge_collector::collect_snow, "bool: controls collection of snow routine")
      .def_readonly("avg_charge", &discharge_collector::charge_m3s, "TsFixed: cell charge [m^3/s] for the timestep");

    sac
      .def_readwrite(
        "collect_state",
        &state_collector::collect_state,
        "bool: if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
      .def_readonly("soil_sm", &state_collector::soil_sm, "TsFixed: HBV soil moisture state [mm]")
      .def_readonly("tank_uz", &state_collector::tank_uz, "TsFixed: HBV tank upper zone state [mm]")
      .def_readonly("tank_lz", &state_collector::tank_lz, "TsFixed: HBV tank lower zone state [mm]")
      .def_readonly("snow_sp", &state_collector::fw, "CoreTsVector: raw snow-tiles state-data for 'fw'")
      .def_readonly("snow_sw", &state_collector::lw, "CoreTsVector: raw snow-tiles state-data for 'lw'")
      .def_property_readonly(
        "snow_sca",
        +[](state_collector const &a) {
          return apoint_ts(a.sca_());
        },
        "TimeSeries: Snow covered area, derived from snow_sp, snow_sw, snow-tiles parameter and snow_fraction")
      .def_property_readonly(
        "snow_swe",
        +[](state_collector const &a) {
          return apoint_ts(a.swe_());
        },
        "TimeSeries: Snow water-equivalent, derived from snow_sp, snow_sw, snow-tiles parameter and snow_fraction");
  }

  using cell_all = core::cell<parameter, state, state_collector, all_response_collector>;
  using cell_opt = core::cell<parameter, state, null_collector, discharge_collector>;

  static void cells(py::module_ &m) {
    cell<cell_all>(m, "cell_all", "tbd: cell_all doc");
    cell<cell_opt>(m, "PTSTHBVCellOpt", "tbd: PTSTHBVCellOpt doc");
    statistics::priestley_taylor<cell_all>(m, "PTSTHBVCell");
    statistics::hbv_soil<cell_all>(m, "PTSTHBVCell");
    statistics::hbv_tank<cell_all>(m, "PTSTHBVCell");
    statistics::snow_tiles<cell_all>(m, "PTSTHBVCell");
  }

  static void models(py::module_ &m) {
    using opt_model = region_model<cell_discharge_response_t, a_region_environment>;
    using model = region_model<cell_complete_response_t, a_region_environment>;

    auto expose_cell_stats = [&]<typename T, typename... O>(py::class_<T, O...> t) {
      t.def_property_readonly("snow_tiles_state", [](T &o) {
        using S = api::snow_tiles_cell_state_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
      t.def_property_readonly("snow_tiles_response", [](T &o) {
        using S = api::snow_tiles_cell_response_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
      t.def_property_readonly("priestley_taylor_response", [](T &o) {
        using S = api::priestley_taylor_cell_response_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
      t.def_property_readonly("soil_state", [](T &o) {
        using S = api::hbv_soil_cell_state_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
      t.def_property_readonly("soil_response", [](T &o) {
        using S = api::hbv_soil_cell_response_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
      t.def_property_readonly("tank_state", [](T &o) {
        using S = api::hbv_tank_cell_state_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
      t.def_property_readonly("tank_response", [](T &o) {
        using S = api::hbv_tank_cell_response_statistics<typename T::cell_t>;
        return S(o.get_cells());
      });
    };
    auto [t0, t1] = expose::expose_models<model, opt_model>(m, stack_name);
    expose_cell_stats(t0);
  }

  IMPL_PYEXPORT_STACK()

}
