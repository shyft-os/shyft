/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/mstack_param.h>
#include <shyft/hydrology/routing.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  using namespace shyft::core;

  void routing_path_info(py::module_ &m) {
    auto c=py::class_<routing_info>(
      m,
      "RoutingInfo",
      doc.intro("Describe the hydrological distance and the id of the target routing element (river)")())
      .def(py::init<int64_t, double>(), py::arg("id") = 0, py::arg("distance") = 0.0)
      .def_readwrite("id", &routing_info::id, "int: id of the target,down-stream river")
      .def_readwrite("distance", &routing_info::distance, "float: the hydrological distance, in unit of [m]")
      .def(py::self == py::self)
      .def(py::self != py::self);
    auto c_str=[](routing_info const & p) {
      return fmt::format("RoutingInfo(id={}, distance={})", p.id, p.distance);
    };
    c.def("__str__", c_str);
    c.def("__repr__", c_str);
  }

  void routing_ugh_parameter(py::module_ &m) {

    auto c=py::class_<routing::uhg_parameter>(
      m,
      "UHGParameter",
      doc.intro("The Unit Hydro Graph Parameter contains sufficient\n"
                "description to create a unit hydro graph, that have a shape\n"
                "and a discretized 'time-length' according to the model time-step resolution.\n"
                "Currently we use a gamma function for the shape:\n"
                "<a ref='https://en.wikipedia.org/wiki/Gamma_distribution'>gamma distribution</a>\n")())
      .def(py::init<double, double, double>(), py::arg("velocity") = 1.0, py::arg("alpha") = 7.0, py::arg("beta") = 0.0)
      .def_readwrite("velocity", &routing::uhg_parameter::velocity, "float: default 1.0, unit [m/s]")
      .def_readwrite(
        "alpha", &routing::uhg_parameter::alpha, "float: default 3.0,unit-less, ref. shape of gamma-function")
      .def_readwrite(
        "beta", &routing::uhg_parameter::beta, "float: default 0.0, unit-less, added to pdf of gamma(alpha,1.0)");
    auto c_str=[](routing::uhg_parameter const & p) {
      return fmt::format("UHGParameter(velocity={}, alpha={}, beta={})", p.velocity, p.alpha, p.beta);
    };
    c.def("__str__", c_str);
    c.def("__repr__", c_str);

    m.def(
      "make_uhg_from_gamma",
      &routing::make_uhg_from_gamma,
      doc
        .intro("make_uhg_from_gamma a simple function to create a uhg (unit hydro graph) weight vector\n"
               "containing n_steps, given the gamma shape factor alpha and beta.\n"
               "ensuring the sum of the weight vector is 1.0\n"
               "and that it has a min-size of one element (1.0)\n")
        .parameters()
        .parameter("n_steps", "int", "number of time-steps, elements, in the vectorn")
        .parameter("alpha", "float", "the gamma_distribution gamma-factor")
        .parameter("beta", "float", "the base-line, added to pdf(gamma(alpha,1))")
        .returns("unit hydro graph factors", "", " - normalized to sum 1.0")(),
      py::arg("n_steps"),
      py::arg("alpha"),
      py::arg("beta"));
  }

  void routing_river(py::module_ &m) {
    py::class_<routing::river>(
      m,
      "River",
      doc.intro(
        "A river that we use for routing, its a single piece of a RiverNetwork\n"
        "\n"
        "The routing river have flow from:\n\n"
        "  a) zero or more 'cell_nodes',  typically a cell_model type, lateral flow,like cell.rc.average_discharge "
        "[m3/s]\n"
        "  b) zero or more upstream connected rivers, taking their .output_m3s()\n"
        "\n"
        "then a routing river can *optionally* be connected to a down-stream river\n"
        "providing a routing function (currently just a convolution of a uhg).\n"
        "\n"
        "This definition is recursive, and we use RiverNetwork to ensure the routing graph\n"
        "is directed and with no cycles.")())
      .def(py::init())
      .def(
        py::init<int64_t, routing_info, routing::uhg_parameter>(),
        "a new object with specified parameters, notice that a valid river-id|routing-id must be >0",
        py::arg("id"),
        py::arg("downstream") = routing_info{},
        py::arg("parameter") = routing::uhg_parameter())
      .def_readonly("id", &routing::river::id, "int: a valid identifier >0 for the river|routing element")
      .def_readwrite(
        "downstream",
        &routing::river::downstream,
        "RoutingInfo: routing information for downstream, target-id, and hydrological distance")
      .def_readwrite("parameter", &routing::river::parameter, "UHGParameter: describing the downstream propagation ")
      .def(
        "uhg",
        &routing::river::uhg,
        "create the hydro-graph for this river, taking specified delta-t, dt,\n"
        "static hydrological distance as well as the shape parameters\n"
        "alpha and beta used to form the gamma-function.\n"
        "The length of the uhg (delay) is determined by the downstream-distance,\n"
        "and the velocity parameter. \n"
        "The shape of the uhg is determined by alpha&beta parameters.\n",
        py::arg("dt"));
  }

  void routing_river_network(py::module_ &m) {
    routing::river &(routing::river_network::*griver)(int64_t) = &routing::river_network::river_by_id;
    py::class_<routing::river_network>(
      m,
      "RiverNetwork",
      doc.intro(
        "A RiverNetwork takes care of the routing\n"
        "see also description of River\n"
        "The RiverNetwork provides all needed functions to build\n"
        "routing into the region model\n"
        "It ensures safe manipulation of rivers:\n\n"
        " * no cycles,\n"
        " * no duplicate object-id's etc.\n"
        "\n")())
      .def(py::init())
      .def(py::init< routing::river_network const &>(), "make a clone of river-network", py::arg("clone"))
      .def(
        "add",
        &routing::river_network::add,
        "add a river to the network, verifies river id, no cycles etc.\n"
        "\nraises exception on error\ntip: build your river-network from downstream to upstream order\n",
        py::arg("river"),
        py::return_value_policy::reference_internal)
      .def(
        "remove_by_id", &routing::river_network::remove_by_id, "disconnect and remove river for network", py::arg("id"))
      .def("river_by_id", griver, "get river by id", py::arg("id"), py::return_value_policy::reference_internal)
      .def(
        "upstreams_by_id",
        &routing::river_network::upstreams_by_id,
        "returns a list(vector) of id of upstream rivers from the specified one\n",
        py::arg("id"))
      .def(
        "downstream_by_id",
        &routing::river_network::downstream_by_id,
        "return id of downstream river, 0 if none",
        py::arg("id"))
      .def(
        "set_downstream_by_id",
        &routing::river_network::set_downstream_by_id,
        "set downstream target for specified river id",
        py::arg("id"),
        py::arg("downstream_id"))
      .def(
        "network_contains_directed_cycle",
        &routing::river_network::network_contains_directed_cycle,
        "True if network have cycles detected");
  }

  void mstack_parameter_x(py::module_ &m) {
    auto c=py::class_<mstack_parameter>(
      m,
      "MethodStackParameter",
      "Contains the parameters for the method-stack,\n"
      "related to inter-method and routing behaviour\n")
      .def(py::init())
      .def(py::init< mstack_parameter const &>(), "make a clone", py::arg("clone"))
      .def_readwrite(
        "reservoir_direct_response_fraction",
        &mstack_parameter::reservoir_direct_response_fraction,
        doc.intro("float: range 0..1, default 1.0, e.g. all precipitation on a reservoir goes to direct response\n"
                  " - set to 0.0, then all precipitation is routed as pr. standard for the stack\n")());
    auto c_str=[](mstack_parameter const & p) {
      return fmt::format("MethodStackParameter(reservoir_direct_response_fraction={})", p.reservoir_direct_response_fraction);
    };
    c.def("__str__", c_str);
    c.def("__repr__", c_str);
  }

  void routing(py::module_ &m) {
    routing_path_info(m);
    routing_ugh_parameter(m);
    routing_river(m);
    routing_river_network(m);
    mstack_parameter_x(m);
  }
}
