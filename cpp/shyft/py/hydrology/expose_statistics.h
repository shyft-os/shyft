/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/hydrology/api/api_statistics.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose::statistics {

  typedef shyft::time_series::dd::apoint_ts rts_;
  typedef std::vector<double> vd_;
  typedef std::vector<int64_t> const &cids_;
  typedef size_t ix_;
  using shyft::core::stat_scope;

  template <class cell>
  static void kirchner(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}KirchnerStateStatistics", cell_name);
    typedef typename shyft::api::kirchner_cell_state_statistics<cell> sc_stat;

    rts_ (sc_stat::*discharge_ts)(cids_, stat_scope) const = &sc_stat::discharge;
    vd_ (sc_stat::*discharge_vd)(cids_, ix_, stat_scope) const = &sc_stat::discharge;
    py::class_<sc_stat>(m, state_name.c_str(), "Kirchner response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "discharge",
        discharge_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "discharge",
        discharge_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "discharge_value",
        &sc_stat::discharge_value,
        "returns sum discharge[m3/s]  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void hbv_soil(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}HBVSoilStateStatistics", cell_name);
    auto response_name = fmt::format("{}HBVSoilResponseStatistics", cell_name);
    typedef typename shyft::api::hbv_soil_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_soil_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*sm_ts)(cids_, stat_scope) const = &sc_stat::sm;
    vd_ (sc_stat::*sm_vd)(cids_, ix_, stat_scope) const = &sc_stat::sm;

    py::class_<sc_stat>(m, state_name.c_str(), "HBV Soil state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "sm", sm_ts, "returns sum for catcment_ids", py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sm",
        sm_vd,
        "returns value for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sm_value",
        &sc_stat::sm_value,
        "returns value for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);

    rts_ (rc_stat::*soil_ae_ts)(cids_, stat_scope) const = &rc_stat::soil_ae;
    vd_ (rc_stat::*soil_ae_vd)(cids_, ix_, stat_scope) const = &rc_stat::soil_ae;

    rts_ (rc_stat::*inuz_ts)(cids_, stat_scope) const = &rc_stat::inuz;
    vd_ (rc_stat::*inuz_vd)(cids_, ix_, stat_scope) const = &rc_stat::inuz;

    py::class_<rc_stat>(m, response_name.c_str(), "HBV soil response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "soil_ae",
        soil_ae_ts,
        "returns sum for catcment_ids [mm/h]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "soil_ae",
        soil_ae_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "soil_ae_value",
        &rc_stat::soil_ae_value,
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "inuz",
        inuz_ts,
        "returns sum for catcment_ids[mm/h]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "inuz",
        inuz_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "inuz_value",
        &rc_stat::inuz_value,
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void hbv_tank(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}HBVTankStateStatistics", cell_name);
    auto response_name = fmt::format("{}HBVTankResponseStatistics", cell_name);
    typedef typename shyft::api::hbv_tank_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_tank_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*uz_ts)(cids_, stat_scope) const = &sc_stat::uz;
    vd_ (sc_stat::*uz_vd)(cids_, ix_, stat_scope) const = &sc_stat::uz;

    rts_ (sc_stat::*lz_ts)(cids_, stat_scope) const = &sc_stat::lz;
    vd_ (sc_stat::*lz_vd)(cids_, ix_, stat_scope) const = &sc_stat::lz;


    py::class_<sc_stat>(m, state_name.c_str(), "hbv tank state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "uz", uz_ts, "returns sum for catcment_ids", py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "uz",
        uz_vd,
        "returns value for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "uz_value",
        &sc_stat::uz_value,
        "returns value for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lz", lz_ts, "returns sum for catcment_ids", py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lz",
        lz_vd,
        "returns value for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lz_value",
        &sc_stat::lz_value,
        "returns value for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);

    rts_ (rc_stat::*elake_ts)(cids_, stat_scope) const = &rc_stat::elake;
    vd_ (rc_stat::*elake_vd)(cids_, ix_, stat_scope) const = &rc_stat::elake;

    rts_ (rc_stat::*qlz_ts)(cids_, stat_scope) const = &rc_stat::qlz;
    vd_ (rc_stat::*qlz_vd)(cids_, ix_, stat_scope) const = &rc_stat::qlz;

    rts_ (rc_stat::*quz0_ts)(cids_, stat_scope) const = &rc_stat::quz0;
    vd_ (rc_stat::*quz0_vd)(cids_, ix_, stat_scope) const = &rc_stat::quz0;

    rts_ (rc_stat::*quz1_ts)(cids_, stat_scope) const = &rc_stat::quz1;
    vd_ (rc_stat::*quz1_vd)(cids_, ix_, stat_scope) const = &rc_stat::quz1;

    rts_ (rc_stat::*quz2_ts)(cids_, stat_scope) const = &rc_stat::quz2;
    vd_ (rc_stat::*quz2_vd)(cids_, ix_, stat_scope) const = &rc_stat::quz2;

    py::class_<rc_stat>(m, response_name.c_str(), "hbv tank response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "elake",
        elake_ts,
        "returns sum for catcment_ids [mm/h]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "elake",
        elake_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "elake_value",
        &rc_stat::elake_value,
        "returns value for cells matching catchments_ids at the i'th timestep [mm/h]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "qlz",
        qlz_ts,
        "returns sum for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "qlz",
        qlz_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "qlz_value",
        &rc_stat::qlz_value,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz0",
        quz0_ts,
        "returns sum for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz0",
        quz0_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz0_value",
        &rc_stat::quz0_value,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz1",
        quz1_ts,
        "returns sum for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz1",
        quz1_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz1_value",
        &rc_stat::quz1_value,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz2",
        quz2_ts,
        "returns sum for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz2",
        quz2_vd,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "quz2_value",
        &rc_stat::quz2_value,
        "returns value for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void priestley_taylor(py::module_ &m, char const *cell_name) {
    auto response_name = fmt::format("{}PriestleyTaylorResponseStatistics", cell_name);
    typedef typename shyft::api::priestley_taylor_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(m, response_name.c_str(), "PriestleyTaylor response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "output",
        output_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output",
        output_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output_value",
        &rc_stat::output_value,
        "returns for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void radiation(py::module_ &m, char const *cell_name) {
    auto response_name = fmt::format("{}RadiationResponseStatistics", cell_name);
    typedef typename shyft::api::radiation_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(m, response_name.c_str(), "Radiation response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "output",
        output_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output",
        output_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output_value",
        &rc_stat::output_value,
        "returns for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void penman_monteith(py::module_ &m, char const *cell_name) {
    auto response_name = fmt::format("{}PenmanMonteithResponseStatistics", cell_name);
    typedef typename shyft::api::penman_monteith_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(m, response_name.c_str(), "PenmanMonteith response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "output",
        output_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output",
        output_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output_value",
        &rc_stat::output_value,
        "returns for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void actual_evapotranspiration(py::module_ &m, char const *cell_name) {
    auto response_name = fmt::format("{}ActualEvapotranspirationResponseStatistics", cell_name);
    typedef typename shyft::api::actual_evapotranspiration_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    rts_ (rc_stat::*pot_ratio_ts)(cids_, stat_scope) const = &rc_stat::pot_ratio;
    vd_ (rc_stat::*pot_ratio_vd)(cids_, ix_, stat_scope) const = &rc_stat::pot_ratio;
    py::class_<rc_stat>(m, response_name.c_str(), "ActualEvapotranspiration response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "output",
        output_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output",
        output_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output_value",
        &rc_stat::output_value,
        "returns for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "pot_ratio",
        pot_ratio_ts,
        "returns the avg ratio (1-exp(-water_level*3/scale_factor)) for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "pot_ratio",
        pot_ratio_vd,
        "returns the ratio the ratio (1-exp(-water_level*3/scale_factor)) for cells matching catchments_ids at the "
        "i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "pot_ratio_value",
        &rc_stat::pot_ratio_value,
        "returns the ratio avg (1-exp(-water_level*3/scale_factor)) value for cells matching catchments_ids at the "
        "i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void hbv_actual_evapotranspiration(py::module_ &m, char const *cell_name) {
    auto response_name = fmt::format("{}HbvActualEvapotranspirationResponseStatistics", cell_name);
    typedef typename shyft::api::hbv_actual_evapotranspiration_cell_response_statistics<cell> rc_stat;

    rts_ (rc_stat::*output_ts)(cids_, stat_scope) const = &rc_stat::output;
    vd_ (rc_stat::*output_vd)(cids_, ix_, stat_scope) const = &rc_stat::output;
    py::class_<rc_stat>(m, response_name.c_str(), "HbvActualEvapotranspiration response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "output",
        output_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output",
        output_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "output_value",
        &rc_stat::output_value,
        "returns for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void gamma_snow(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}GammaSnowStateStatistics", cell_name);
    auto response_name = fmt::format("{}GammaSnowResponseStatistics", cell_name);
    typedef typename shyft::api::gamma_snow_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::gamma_snow_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*albedo_ts)(cids_, stat_scope) const = &sc_stat::albedo;
    vd_ (sc_stat::*albedo_vd)(cids_, ix_, stat_scope) const = &sc_stat::albedo;

    rts_ (sc_stat::*lwc_ts)(cids_, stat_scope) const = &sc_stat::lwc;
    vd_ (sc_stat::*lwc_vd)(cids_, ix_, stat_scope) const = &sc_stat::lwc;

    rts_ (sc_stat::*surface_heat_ts)(cids_, stat_scope) const = &sc_stat::surface_heat;
    vd_ (sc_stat::*surface_heat_vd)(cids_, ix_, stat_scope) const = &sc_stat::surface_heat;

    rts_ (sc_stat::*alpha_ts)(cids_, stat_scope) const = &sc_stat::alpha;
    vd_ (sc_stat::*alpha_vd)(cids_, ix_, stat_scope) const = &sc_stat::alpha;

    rts_ (sc_stat::*sdc_melt_mean_ts)(cids_, stat_scope) const = &sc_stat::sdc_melt_mean;
    vd_ (sc_stat::*sdc_melt_mean_vd)(cids_, ix_, stat_scope) const = &sc_stat::sdc_melt_mean;

    rts_ (sc_stat::*acc_melt_ts)(cids_, stat_scope) const = &sc_stat::acc_melt;
    vd_ (sc_stat::*acc_melt_vd)(cids_, ix_, stat_scope) const = &sc_stat::acc_melt;

    rts_ (sc_stat::*iso_pot_energy_ts)(cids_, stat_scope) const = &sc_stat::iso_pot_energy;
    vd_ (sc_stat::*iso_pot_energy_vd)(cids_, ix_, stat_scope) const = &sc_stat::iso_pot_energy;

    rts_ (sc_stat::*temp_swe_ts)(cids_, stat_scope) const = &sc_stat::temp_swe;
    vd_ (sc_stat::*temp_swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::temp_swe;

    py::class_<sc_stat>(m, state_name.c_str(), "GammaSnow state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "albedo",
        albedo_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "albedo",
        albedo_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "albedo_value",
        &sc_stat::albedo_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lwc",
        lwc_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lwc",
        lwc_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lwc_value",
        &sc_stat::lwc_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "surface_heat",
        surface_heat_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "surface_heat",
        surface_heat_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "surface_heat_value",
        &sc_stat::surface_heat_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "alpha",
        alpha_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "alpha",
        alpha_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "alpha_value",
        &sc_stat::alpha_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sdc_melt_mean",
        sdc_melt_mean_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sdc_melt_mean",
        sdc_melt_mean_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sdc_melt_mean_value",
        &sc_stat::sdc_melt_mean_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "acc_melt",
        acc_melt_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "acc_melt",
        acc_melt_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "acc_melt_value",
        &sc_stat::acc_melt_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "iso_pot_energy",
        iso_pot_energy_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "iso_pot_energy",
        iso_pot_energy_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "iso_pot_energy_value",
        &sc_stat::iso_pot_energy_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "temp_swe",
        temp_swe_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "temp_swe",
        temp_swe_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "temp_swe_value",
        &sc_stat::temp_swe_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);


    rts_ (rc_stat::*sca_ts)(cids_, stat_scope) const = &rc_stat::sca;
    vd_ (rc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &rc_stat::sca;

    rts_ (rc_stat::*swe_ts)(cids_, stat_scope) const = &rc_stat::swe;
    vd_ (rc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &rc_stat::swe;

    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;

    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(m, response_name.c_str(), "GammaSnow response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "outflow",
        outflow_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow",
        outflow_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &rc_stat::swe_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &rc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_ts,
        "returns sum  for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_vd,
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void hbv_snow(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}HBVSnowStateStatistics", cell_name);
    auto response_name = fmt::format("{}HBVSnowResponseStatistics", cell_name);
    typedef typename shyft::api::hbv_snow_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_snow_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;

    py::class_<sc_stat>(m, state_name.c_str(), "HBVSnow state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "swe",
        swe_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &sc_stat::swe_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &sc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);


    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(m, response_name.c_str(), "HBVSnow response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "outflow",
        outflow_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow",
        outflow_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_ts,
        "returns sum  for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_vd,
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void snow_tiles(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}SnowTilesStateStatistics", cell_name);
    auto response_name = fmt::format("{}SnowTilesResponseStatistics", cell_name);
    using sc_stat = shyft::api::snow_tiles_cell_state_statistics<cell>;
    using rc_stat = shyft::api::snow_tiles_cell_response_statistics<cell>;

    rts_ (sc_stat::*s_swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*s_swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*s_sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*s_sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;

    py::class_<sc_stat>(m, state_name.c_str(), "Snow tiles state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "swe",
        s_swe_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        s_swe_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &sc_stat::swe_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        s_sca_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        s_sca_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &sc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);

    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*swe_ts)(cids_, stat_scope) const = &rc_stat::swe;
    vd_ (rc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &rc_stat::swe;
    rts_ (rc_stat::*sca_ts)(cids_, stat_scope) const = &rc_stat::sca;
    vd_ (rc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &rc_stat::sca;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(m, response_name.c_str(), "SnowTiles response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "outflow",
        outflow_ts,
        "returns sum of outflow for catcment_ids [m3 s-1]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow",
        outflow_vd,
        "returns outflow for cells matching catchments_ids at the i'th timestep [m3 s-1]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        "returns sum of outfow for cells matching catchments_ids at the i'th timestep [m3 s-1]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_ts,
        "returns average snow-water equivalent for catcment_ids [mm]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_vd,
        "returns snow-water equivalent for cells matching catchments_ids at the i'th timestep [mm]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &rc_stat::swe_value,
        "returns average snow-water equivalent for cells matching catchments_ids at the i'th timestep [mm]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_ts,
        "returns average snow cover fraction for catcment_ids [0...1]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_vd,
        "returns snow cover fraction for cells matching catchments_ids at the i'th timestep [0...1]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &rc_stat::sca_value,
        "returns average snow cover fraction for cells matching catchments_ids at the i'th timestep [0...1]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_ts,
        "returns sum of glacier melt for catcment_ids [m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_vd,
        "returns glacier melt for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        "returns sum of glacier melt for cells matching catchments_ids at the i'th timestep[m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void hbv_physical_snow(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}HBVPhysicalSnowStateStatistics", cell_name);
    auto response_name = fmt::format("{}HBVPhysicalSnowResponseStatistics", cell_name);
    typedef typename shyft::api::hbv_physical_snow_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::hbv_physical_snow_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;
    rts_ (sc_stat::*surface_heat_ts)(cids_, stat_scope) const = &sc_stat::surface_heat;
    vd_ (sc_stat::*surface_heat_vd)(cids_, ix_, stat_scope) const = &sc_stat::surface_heat;

    py::class_<sc_stat>(m, state_name.c_str(), "HBVPhysicalSnow state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "swe",
        swe_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &sc_stat::swe_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &sc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "surface_heat",
        surface_heat_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "surface_heat",
        surface_heat_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "surface_heat_value",
        &sc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);


    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;

    py::class_<rc_stat>(m, response_name.c_str(), "HBVSnow response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "outflow",
        outflow_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow",
        outflow_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_ts,
        "returns sum  for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_vd,
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void skaugen(py::module_ &m, char const *cell_name) {
    auto state_name = fmt::format("{}SkaugenStateStatistics", cell_name);
    auto response_name = fmt::format("{}SkaugenResponseStatistics", cell_name);
    typedef typename shyft::api::skaugen_cell_state_statistics<cell> sc_stat;
    typedef typename shyft::api::skaugen_cell_response_statistics<cell> rc_stat;

    rts_ (sc_stat::*alpha_ts)(cids_, stat_scope) const = &sc_stat::alpha;
    vd_ (sc_stat::*alpha_vd)(cids_, ix_, stat_scope) const = &sc_stat::alpha;
    rts_ (sc_stat::*nu_ts)(cids_, stat_scope) const = &sc_stat::nu;
    vd_ (sc_stat::*nu_vd)(cids_, ix_, stat_scope) const = &sc_stat::nu;
    rts_ (sc_stat::*lwc_ts)(cids_, stat_scope) const = &sc_stat::lwc;
    vd_ (sc_stat::*lwc_vd)(cids_, ix_, stat_scope) const = &sc_stat::lwc;
    rts_ (sc_stat::*residual_ts)(cids_, stat_scope) const = &sc_stat::residual;
    vd_ (sc_stat::*residual_vd)(cids_, ix_, stat_scope) const = &sc_stat::residual;
    rts_ (sc_stat::*swe_ts)(cids_, stat_scope) const = &sc_stat::swe;
    vd_ (sc_stat::*swe_vd)(cids_, ix_, stat_scope) const = &sc_stat::swe;
    rts_ (sc_stat::*sca_ts)(cids_, stat_scope) const = &sc_stat::sca;
    vd_ (sc_stat::*sca_vd)(cids_, ix_, stat_scope) const = &sc_stat::sca;

    py::class_<sc_stat>(m, state_name.c_str(), "Skaugen snow state statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "alpha",
        alpha_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "alpha",
        alpha_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "alpha_value",
        &sc_stat::alpha_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "nu", nu_ts, "returns sum  for catcment_ids", py::arg("indexes"), py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "nu",
        nu_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "nu_value",
        &sc_stat::nu_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lwc",
        lwc_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lwc",
        lwc_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lwc_value",
        &sc_stat::lwc_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "residual",
        residual_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "residual",
        residual_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "residual_value",
        &sc_stat::residual_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        swe_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &sc_stat::swe_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        sca_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &sc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);

    rts_ (rc_stat::*outflow_ts)(cids_, stat_scope) const = &rc_stat::outflow;
    vd_ (rc_stat::*outflow_vd)(cids_, ix_, stat_scope) const = &rc_stat::outflow;
    rts_ (rc_stat::*total_stored_water_ts)(cids_, stat_scope) const = &rc_stat::total_stored_water;
    vd_ (rc_stat::*total_stored_water_vd)(cids_, ix_, stat_scope) const = &rc_stat::total_stored_water;
    rts_ (rc_stat::*glacier_melt_ts)(cids_, stat_scope) const = &rc_stat::glacier_melt;
    vd_ (rc_stat::*glacier_melt_vd)(cids_, ix_, stat_scope) const = &rc_stat::glacier_melt;
    rts_ (rc_stat::*r_sca_ts)(cids_, stat_scope) const = &rc_stat::sca;
    vd_ (rc_stat::*r_sca_vd)(cids_, ix_, stat_scope) const = &rc_stat::sca;

    py::class_<rc_stat>(m, response_name.c_str(), "Skaugen snow response statistics")
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "outflow",
        outflow_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow",
        outflow_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "outflow_value",
        &rc_stat::outflow_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "total_stored_water",
        total_stored_water_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "total_stored_water",
        total_stored_water_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "total_stored_water_value",
        &rc_stat::total_stored_water_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        total_stored_water_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe",
        total_stored_water_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "swe_value",
        &rc_stat::total_stored_water_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_ts,
        "returns sum  for catcment_ids[m3/s]",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt",
        glacier_melt_vd,
        "returns  for cells matching catchments_ids at the i'th timestep [m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_melt_value",
        &rc_stat::glacier_melt_value,
        "returns  for cells matching catchments_ids at the i'th timestep[m3/s]",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        r_sca_ts,
        "returns avg for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca",
        r_sca_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "sca_value",
        &rc_stat::sca_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

  template <class cell>
  static void basic_cell(py::module_ &m, char const *cell_name) {
    auto base_name = fmt::format("{}Statistics", cell_name);
    typedef typename shyft::api::basic_cell_statistics<cell> bc_stat;

    rts_ (bc_stat::*discharge_ts)(cids_, stat_scope) const = &bc_stat::discharge;
    vd_ (bc_stat::*discharge_vd)(cids_, ix_, stat_scope) const = &bc_stat::discharge;

    rts_ (bc_stat::*charge_ts)(cids_, stat_scope) const = &bc_stat::charge;
    vd_ (bc_stat::*charge_vd)(cids_, ix_, stat_scope) const = &bc_stat::charge;

    rts_ (bc_stat::*temperature_ts)(cids_, stat_scope) const = &bc_stat::temperature;
    vd_ (bc_stat::*temperature_vd)(cids_, ix_, stat_scope) const = &bc_stat::temperature;

    rts_ (bc_stat::*radiation_ts)(cids_, stat_scope) const = &bc_stat::radiation;
    vd_ (bc_stat::*radiation_vd)(cids_, ix_, stat_scope) const = &bc_stat::radiation;

    rts_ (bc_stat::*wind_speed_ts)(cids_, stat_scope) const = &bc_stat::wind_speed;
    vd_ (bc_stat::*wind_speed_vd)(cids_, ix_, stat_scope) const = &bc_stat::wind_speed;

    rts_ (bc_stat::*rel_hum_ts)(cids_, stat_scope) const = &bc_stat::rel_hum;
    vd_ (bc_stat::*rel_hum_vd)(cids_, ix_, stat_scope) const = &bc_stat::rel_hum;

    rts_ (bc_stat::*precipitation_ts)(cids_, stat_scope) const = &bc_stat::precipitation;
    vd_ (bc_stat::*precipitation_vd)(cids_, ix_, stat_scope) const = &bc_stat::precipitation;

    rts_ (bc_stat::*snow_swe_ts)(cids_, stat_scope) const = &bc_stat::snow_swe;
    vd_ (bc_stat::*snow_swe_vd)(cids_, ix_, stat_scope) const = &bc_stat::snow_swe;

    rts_ (bc_stat::*snow_sca_ts)(cids_, stat_scope) const = &bc_stat::snow_sca;
    vd_ (bc_stat::*snow_sca_vd)(cids_, ix_, stat_scope) const = &bc_stat::snow_sca;

    py::class_<bc_stat>(
      m,
      base_name.c_str(),
      doc.intro("This class provides statistics for group of cells, as specified")
        .intro("by the list of catchment identifiers, or list of cell-indexes passed to the methods.")
        .intro("It is provided both as a separate class, but is also provided")
        .intro("automagically through the region_model.statistics property.")())
      .def(py::init<std::shared_ptr<std::vector<cell>>>(), py::arg("cells"))
      .def(
        "discharge",
        discharge_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "discharge",
        discharge_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "discharge_value",
        &bc_stat::discharge_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "charge",
        charge_ts,
        "returns sum charge[m^3/s] for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "charge",
        charge_vd,
        "returns charge[m^3/s]  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "charge_value",
        &bc_stat::charge_value,
        "returns charge[m^3/s] for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_swe",
        snow_swe_ts,
        "returns snow_swe [mm] for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_swe",
        snow_swe_vd,
        "returns snow_swe [mm]  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_swe_value",
        &bc_stat::snow_swe_value,
        "returns snow_swe [mm] for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_sca",
        snow_sca_ts,
        "returns snow_sca [] for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_sca",
        snow_sca_vd,
        "returns snow_sca []  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_sca_value",
        &bc_stat::snow_sca_value,
        "returns snow_sca [] for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "temperature",
        temperature_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "temperature",
        temperature_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "temperature_value",
        &bc_stat::temperature_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "precipitation",
        precipitation_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "precipitation",
        precipitation_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "precipitation_value",
        &bc_stat::precipitation_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "radiation",
        radiation_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "radiation",
        radiation_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "radiation_value",
        &bc_stat::radiation_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "wind_speed",
        wind_speed_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "wind_speed",
        wind_speed_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "wind_speed_value",
        &bc_stat::wind_speed_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "rel_hum",
        rel_hum_ts,
        "returns sum  for catcment_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "rel_hum",
        rel_hum_vd,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "rel_hum_value",
        &bc_stat::rel_hum_value,
        "returns  for cells matching catchments_ids at the i'th timestep",
        py::arg("indexes"),
        py::arg("i"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "total_area",
        &bc_stat::total_area,
        "returns total area[m2] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "forest_area",
        &bc_stat::forest_area,
        "returns forest area[m2] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "glacier_area",
        &bc_stat::glacier_area,
        "returns glacier area[m2] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "lake_area",
        &bc_stat::lake_area,
        "returns lake area[m2] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "reservoir_area",
        &bc_stat::reservoir_area,
        "returns reservoir area[m2] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "unspecified_area",
        &bc_stat::unspecified_area,
        "returns unspecified area[m2] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "snow_storage_area",
        &bc_stat::snow_storage_area,
        "returns snow_storage area where snow can build up[m2], eg total_area - lake and reservoir",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix)
      .def(
        "elevation",
        &bc_stat::elevation,
        "returns area-average elevation[m.a.s.l] for cells matching catchments_ids",
        py::arg("indexes"),
        py::arg("ix_type") = stat_scope::catchment_ix);
  }

}
