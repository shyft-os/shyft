/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/hydrology/methods/hbv_tank.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  namespace py = shyft::py;

  void hbv_tank(py::module_ &m) {
    using namespace shyft::core::hbv_tank;

    py::class_<parameter>(m, "HbvTankParameter")
      .def(
        py::init<
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
        double>(),
        py::arg("uz1") = 10.0,
        py::arg("uz2") = 50.0,
        py::arg("kuz0") = 0.05,
        py::arg("kuz1") = 0.1,
        py::arg("kuz2") = 0.0,
        py::arg("perc") = 0.6,
        py::arg("klz") = 0.05,
        py::arg("ce") = 0.17 / 24.0,
        py::arg("cevpl") = 1.1)
      .def_readwrite("uz1", &parameter::uz1, "float: [ mm ] Mid-threshold for upper ground water zone. default=10")
      .def_readwrite("uz2", &parameter::uz2, "float: [ mm ] High-threshold for upper ground water zone. default=50")
      .def_readwrite(
        "kuz0", &parameter::kuz0, "float: [ 1/step ] Slow response coefficient upper ground water zone. default=0.05")
      .def_readwrite(
        "kuz1", &parameter::kuz1, "float: [ 1/step ] Mid response coefficient upper ground water zone. default=0.1")
      .def_readwrite(
        "kuz2",
        &parameter::kuz2,
        "float: [ 1/step ] Fast response coefficient upper ground water zone. default=0 (not active)")
      .def_readwrite("perc", &parameter::perc, "float: [ mm/step ] Perculation to lower ground water zone. default=0.6")
      .def_readwrite(
        "klz", &parameter::klz, "float: [ 1/step ] Slow response coefficient lower ground water zone. default=0.05")
      .def_readwrite(
        "ce", &parameter::ce, "float: [ mm/deg/day ] Evapotranspiration constant (degree-day-factor). default=0.17/24")
      .def_readwrite(
        "cevpl",
        &parameter::cevpl,
        "float: [ unitless ], adjustment factor for potential evapotranspiration on lakes. default=1.1");

    py::class_<state>(m, "HbvTankState")
      .def(py::init())
      .def(py::init<double, double>(), "create a state with specified values", py::arg("uz") = 10, py::arg("lz") = 10)
      .def_readwrite("uz", &state::uz, "float: Upper ground water zone content [mm]")
      .def_readwrite("lz", &state::lz, "float: Lower ground water zone content [mm]");

    py::class_<response>(m, "HbvTankResponse")
      .def(
        py::init<double, double, double, double, double, double>(),
        py::arg("quz0") = 0.0,
        py::arg("quz1") = 0.0,
        py::arg("quz2") = 0.0,
        py::arg("qlz") = 0.0,
        py::arg("elake") = 0.0,
        py::arg("perculation") = 0.0)
      .def_property_readonly("quz", &response::quz, "float: [mm/step] Total for upper zone")
      .def_readwrite("quz0", &response::quz0, "float: [mm/step] Upper zone slow response")
      .def_readwrite("quz1", &response::quz1, "float: [mm/step] Upper zone mid response")
      .def_readwrite("quz2", &response::quz2, "float: [mm/step] Upper zone fast response")
      .def_readwrite("qlz", &response::qlz, "float: [mm/step] Total for lower zone")
      .def_readwrite("elake", &response::elake, "float: [ mm/step ] Evaporation from lake")
      .def_readwrite(
        "perculation", &response::perc_effective, "float: [ mm/step ] Effective perculation from upper to lower")
      .def_property_readonly("q", &response::q, "float: [mm/step] Total discharge from tank");

    typedef calculator HbvTankCalculator;
    py::class_<HbvTankCalculator>(
      m,
      "HbvTankCalculator",
      doc.intro("Computing water through the HBV ground water zone of the HBV model.")
        .intro("\n")
        .intro("Reference:\n")
        .intro(" * Nils Roar Sæhlthun: The Nordic HBV model 1996 "
               "https://publikasjoner.nve.no/publication/1996/publication1996_07.pdf\n")
        .intro("\n")
        .notes()
        .note("Lake and glacier are treated in the stack")())
      .def(
        py::init< parameter const &, double>(),
        "creates a calculator with given parameters",
        py::arg("parameter"),
        py::arg("lake_fraction"))
      .def(
        "step",
        &HbvTankCalculator::step,
        doc.intro("One step of the model, given state, parameters and input.\n")
          .intro("Updates the state and response.\n")
          .parameters()
          .parameter("state", "HbvSoilState", " param of type S, in/out, ref template parameters")
          .parameter("response", "HbvSoilResponse", " param of type R, in/out, ref template parameters")
          .parameter("from_soil", "float", "[mm/step] Inflow to upper zone (from soil moisture)")
          .parameter(
            "precip",
            "float",
            "[mm/step] Routed precipitation. Assumed that it is corrected for lake fraction already.")
          .parameter("t2m", "float", "[deg C] Air-temperature at 2m (proxy for water temperature)")
          .parameter(
            "lake_fraction",
            "float",
            "[unitless] Lake-fraction of the cell. Not directly used/desribed in ref.'Nils Roar', but introduced here "
            "to adopt to the stack.")(),
        py::arg("state"),
        py::arg("response"),
        py::arg("from_soil"),
        py::arg("precip"),
        py::arg("t2m"));
  }
}
