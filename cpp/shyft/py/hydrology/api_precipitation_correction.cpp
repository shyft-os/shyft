/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>

namespace expose {

  void precipitation_correction(py::module_ &m) {
    using namespace shyft::core::precipitation_correction;
    py::class_<parameter>(m, "PrecipitationCorrectionParameter")
      .def(py::init<double>(), py::arg("scale_factor") = 1.0)
      .def_readwrite("scale_factor", &parameter::scale_factor, "float: default =1.0");
    py::class_<calculator>(m, "PrecipitationCorrectionCalculator", "Scales precipitation with the specified scale factor")
      .def(py::init<double>(), py::arg("scale_factor"))
      .def("calc", &calculator::calc,
           "returns scale_factor*precipitation\n",
           py::arg("precipitation"));
  }

}
