/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/py/hydrology/api.h>

namespace shyft::hydrology::grammar {
  extern shyft::core::utcperiod parse_cf_time(char const *);
}

namespace expose {
  namespace py = shyft::py;
  void cf_time(py::module_ &m) {
    using shyft::hydrology::grammar::parse_cf_time;
    m.def(
      "parse_cf_time",
      &parse_cf_time,
      doc.intro("parses cf time unit string like hours since 1970-01-01 00:00:00")
        .intro("and returs an UtcPeriod, where the .start is the reference time")
        .intro("and the .timespan() is the delta time, e.g. 3600.0")
        .intro("If the reference is illformed, then the returned period is not .valid()")
        .intro("The intended usage is to get the reference time and time-delta of netcdf files,")
        .intro("replacing the need of cftime python library.")
        .parameters()
        .parameter("reference", "str", "netcdf time-reference")
        .returns(
          "p",
          "UtcPeriod",
          "p.valid()==True if well formed,p.start equal to the reference time, and p.timespan() equal to the "
          "time-delta specified")(),
      py::arg("reference"));
  }
}
