/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

#include <fmt/core.h>

template <typename Char>
struct fmt::formatter<shyft::core::land_type_fractions, Char> {
  FMT_CONSTEXPR auto parse(auto& ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  auto format(shyft::core::land_type_fractions const & o, auto& ctx) const {
    auto out = ctx.out();
    return fmt::format_to(out, "LandTypeFractions(glacier={}, lake={}, forest={}, unspecified={})", o.glacier(),
                          o.lake(), o.forest(), o.unspecified());
  }
};
template <typename Char>
struct fmt::formatter<shyft::core::geo_cell_data, Char> {
  FMT_CONSTEXPR auto parse(auto& ctx) {
    auto it = ctx.begin(), end = ctx.end();
    if (it != end && *it != '}')
      FMT_ON_ERROR(ctx, "invalid format");
    return it;
  }

  auto format(shyft::core::geo_cell_data const & o, auto& ctx) const {
    auto out = ctx.out();
    return fmt::format_to(out, "GeoCellData( mid_point={}, catchment_id={}, radiation_slope_factor={}, area={}, ltf={})", o.mid_point(),
                          o.catchment_id(), o.radiation_slope_factor(), o.area(), fmt::format("{}",o.land_type_fractions_info()));
  }
};


namespace expose {
  namespace py = shyft::py;

  using namespace shyft::core;

  void api_geo_cell_data(py::module_ &m) {
    {
      auto t = py::class_<land_type_fractions>(
      m,
      "LandTypeFractions",
      doc.intro("LandTypeFractions are used to describe type of land,\n"
                "like glacier, lake, reservoir and forest.\n"
                "It is designed as a part of GeoCellData")())
      .def(py::init())
      .def(py::init<double, double, double, double, double>(),
           "construct LandTypeFraction specifying the area of each type",
           py::arg("glacier"), py::arg("lake"), py::arg("reservoir"), py::arg("forest"), py::arg("unspecified"))
      .def("glacier", &land_type_fractions::glacier, "returns the glacier part")
      .def("lake", &land_type_fractions::lake, "returns the lake part")
      .def("reservoir", &land_type_fractions::reservoir, "returns the reservoir part")
      .def("forest", &land_type_fractions::forest, "returns the forest part")
      .def("unspecified", &land_type_fractions::unspecified, "returns the unspecified part")
      .def(
        "set_fractions",
        &land_type_fractions::set_fractions,
        "set the fractions explicit, each a value in range 0..1, sum should be 1.0",
        py::arg("glacier"), py::arg("lake"), py::arg("reservoir"), py::arg("forest"))
      .def(
        "snow_storage",
        &land_type_fractions::snow_storage,
        "returns the area where snow can build up, 1.0-lake-reservoir")
      .def(py::self == py::self)
      .def(py::self != py::self);
      shyft::pyapi::expose_format(t);
    }
    {

    auto t = py::class_<geo_cell_data>(
      m,
      "GeoCellData",
      doc.intro(
        "Represents common constant geo_cell properties across several possible models and cell assemblies.\n"
        "The idea is that most of our algorithms uses one or more of these properties,\n"
        "so we provide a common aspect that keeps this together.\n"
        "Currently it keep these items:\n"
        "- mid-point geo_point, (x,y,z) (default 0)\n"
        "- TIN data \n"
        "- the area in m^2, (default 1000 x 1000 m^2)\n"
        "- land_type_fractions (unspecified=1)\n"
        "- catchment_id   def (-1)\n"
        "- radiation_slope_factor def 0.9\n"
        "- routing_info def(0,0.0), i.e. not routed and hydrological distance=0.0m")())
      .def(py::init())
      .def(
        py::init<geo_point, double, int64_t, double, land_type_fractions const &, routing_info>(),
        doc.intro("Constructs a GeoCellData with all parameters specified")
          .parameters()
          .parameter("mid_point", "GeoPoint", "specifies the x,y,z mid-point of the cell-area")
          .parameter("area", "float", "area in unit [m^2]")
          .parameter("catchment_id", "int", "catchment-id that this cell is a part of")
          .parameter(
            "radiation_slope_factor",
            "float",
            "aspect dependent factor used to calculate the effective radiation for this cell,range 0.0..1.0")
          .parameter(
            "land_type_fractions",
            "LandTypeFractions",
            "specifies the fractions of glacier, forrest, lake and reservoirs for this cell")
          .parameter(
            "routing_info",
            "RoutingInfo",
            "Specifies the destination routing network-node and velocity for this cell")(),
        py::arg("mid_point"),
        py::arg("area"),
        py::arg("catchment_id"),
        py::arg("radiation_slope_factor") = default_radiation_slope_factor,
        py::arg("land_type_fractions") = land_type_fractions(),
        py::arg("routing_info") = routing_info())
      .def(
        py::init<geo_point, geo_point, geo_point, int64_t, int64_t, land_type_fractions const &, routing_info>(),
        doc
          .intro("Constructs a TIN-based GeoCellData with all parameters specified, slope/aspect etc. is based on "
                 "TIN")
          .parameters()
          .parameter("p1", "GeoPoint", "specifies the first vertex in TIN")
          .parameter("p2", "GeoPoint", "specifies the second vertex in TIN")
          .parameter("p3", "GeoPoint", "specifies the third vertex in TIN")
          .parameter(
            "epsg_id",
            "int",
            "specifies the geo projection as epsg-id, used for projection from cartesian coord to long-lat  "
            "algorithms that needs this(e.g. radiation, TIN-models)")
          .parameter("catchment_id", "int", "catchment-id that this cell is a part of")
          .parameter(
            "land_type_fractions",
            "LandTypeFractions",
            "specifies the fractions of glacier, forrest, lake and reservoirs for this cell")
          .parameter(
            "routing_info",
            "RoutingInfo",
            "Specifies the destination routing network-node and velocity for this cell")(),
        py::arg("p1"),
        py::arg("p2"),
        py::arg("p3"),
        py::arg("epsg_id"),
        py::arg("catchment_id"),
        py::arg("land_type_fractions") = land_type_fractions(),
        py::arg("routing_info") = routing_info())
      .def(
        "mid_point",
        &geo_cell_data::mid_point,
        "returns the mid_point",
        py::return_value_policy::copy)
      .def("catchment_id", &geo_cell_data::catchment_id, "returns the current catchment_id")
      .def(
        "set_catchment_id",
        &geo_cell_data::set_catchment_id,
        doc.intro("Set the catchment_id to specified value")
          .intro("Note: Only use this method *before* a region-model is created")
          .parameters()
          .parameter("catchment_id", "int", "Catchment-id for this cell")(),
        py::arg("catchment_id"))
      .def("radiation_slope_factor", &geo_cell_data::radiation_slope_factor)
      .def(
        "land_type_fractions_info",
        &geo_cell_data::land_type_fractions_info,
        "land_type_fractions",
        py::return_value_policy::copy)
      .def(
        "set_land_type_fractions", &geo_cell_data::set_land_type_fractions, py::arg("ltf"), "set new LandTypeFractions")
      .def_readwrite(
        "routing_info",
        &geo_cell_data::routing,
        "RoutingInfo: routing information for the cell keep destination id and hydrological distance to destination")
      .def_readwrite(
        "epsg_id",
        &geo_cell_data::epsg_id,
        "int: the epsg-id that should consistently identify the geo projection for all cells in a region model")
      .def("surface_area", &geo_cell_data::rarea, "returns the real surface area in m^2")
      .def(
        "area",
        &geo_cell_data::area,
        "returns the effective area, as projected to the horisontal plane, in m^2")
      .def("set_tin_data", &geo_cell_data::set_tin_data, "set TIN data from vector of vertexes", py::arg("vertexes"))
      .def("is_tin", &geo_cell_data::is_tin, "returns true if the geo-cell is fully specified as a TIN model")
      .def("slope", &geo_cell_data::slope, "returns slope, deg")
      .def("aspect", &geo_cell_data::aspect, "returns aspect, deg")
      .def("vertexes", &geo_cell_data::vertexes, "returns vector of tin vertexes")
      .def(py::self == py::self)
      .def(py::self != py::self);

      shyft::pyapi::expose_format(t);
    }
  }
}
