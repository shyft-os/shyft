/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>

namespace expose {
  namespace py = shyft::py;
  using namespace shyft::core::actual_evapotranspiration;

  void actual_evapotranspiration(py::module_ &m) {
    py::class_<parameter>(m, "ActualEvapotranspirationParameter")
      .def(py::init<double>(), py::arg("ae_scale_factor") = 1.5)
      .def_readwrite("ae_scale_factor", &parameter::ae_scale_factor);
    py::class_<response>(m, "ActualEvapotranspirationResponse")
      .def_readwrite("ae", &response::ae, "float: actual evapotranspiration");
    m.def(
      "ActualEvapotranspirationCalculate_step",
      calculate_step,
      doc.intro("actual_evapotranspiration calculates actual evapotranspiration, returning same unit as input pot.evap")
        .intro("based on supplied parameters")
        .parameters()
        .parameter("water_level", "", "[mm] water level eqvivalent in ground, ae goes to zero if ground is drying out")
        .parameter("potential_evapotranspiration", "", "[mm/x], x time-unit")
        .parameter("scale_factor", "", "typically 1.5")
        .parameter("snow_fraction", "", " 0..1 there is to ae over snow surface, so snow_fraction 1.0, yields 0.0")
        .parameter("dt", "", "[s] timestep length, currently not part of the formula")
        .returns("actual evapotranspiration", "", "")(),
      py::arg("water_level"),
      py::arg("potential_evapotranspiration"),
      py::arg("scale_factor"),
      py::arg("snow_fraction"),
      py::arg("dt"));
  }
}
