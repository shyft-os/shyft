#pragma once

#include <map>
#include <memory>
#include <vector>

#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/api/api_state.h>
#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/model_calibration.h>
#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>

#include <pybind11/pybind11.h>

namespace expose {

  namespace py = shyft::py;

  template<typename... T>
  auto to_boost_variant(std::variant<T...> v){
    return std::visit( 
      [](auto t){ return boost::variant<T...>(std::move(t)); },
      std::move(v));
  }
  template<typename... T>
  auto to_std_variant(boost::variant<T...> v){
    return boost::apply_visitor( 
      [](auto t){ return std::variant<T...>(std::move(t)); },
      std::move(v));
  }

}

PYBIND11_MAKE_OPAQUE(std::vector<shyft::core::geo_cell_data>);
PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::core::gcd_model>>);


PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::GeoPointSource>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::TemperatureSource>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::PrecipitationSource>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::WindSpeedSource>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::RelHumSource>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::RadiationSource>);

PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::core::model_calibration::goal_function_model>>);
PYBIND11_MAKE_OPAQUE(std::vector<shyft::core::model_calibration::target_specification>);

#define SHYFT_HYDROLOGY_STACKS (pt_gs_k, pt_hps_k, pt_hs_k, pt_ss_k, pt_st_hbv, pt_st_k, r_pm_gs_k, r_pm_st_k, r_pmv_st_k, r_pt_gs_k)
   
#define SHYFT_LAMBDA(r, data, stack)\
  PYBIND11_MAKE_OPAQUE(std::vector<shyft::core::stack::state>);       \
  PYBIND11_MAKE_OPAQUE(std::vector<shyft::api::cell_state_with_id<shyft::core::stack::state>>); \
  PYBIND11_MAKE_OPAQUE(std::map<std::int64_t, std::shared_ptr<shyft::core::stack::parameter>>);
  ;
BOOST_PP_LIST_FOR_EACH(
  SHYFT_LAMBDA,
  _,
  BOOST_PP_TUPLE_TO_LIST(SHYFT_HYDROLOGY_STACKS))
#undef SHYFT_LAMBDA

PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::hydrology::srv::state_model>>);
PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::hydrology::srv::parameter_model>>);


PYBIND11_NAMESPACE_BEGIN(PYBIND11_NAMESPACE)
namespace detail {
  template <class P, class S, class SC, class RC>
  class type_caster<std::vector<shyft::core::cell<P, S, SC, RC>>> : public type_caster_base<
    std::vector<shyft::core::cell<P, S, SC, RC>>>{};
}
PYBIND11_NAMESPACE_END(PYBIND11_NAMESPACE)
