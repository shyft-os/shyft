/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/hbv_snow.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  namespace py = shyft::py;

  void hbv_snow(py::module_ &m) {
    using namespace shyft::core::hbv_snow;
    using std::vector;

    py::class_<parameter>(m, "HbvSnowParameter")
      .def(
        py::init<double, double, double, double, double>(),
        py::arg("tx") = 0.0,
        py::arg("cx") = 1.0,
        py::arg("ts") = 0.0,
        py::arg("lw") = 0.1,
        py::arg("cfr") = 0.5)
      .def(
        py::init<vector<double> const &, vector<double> const &, double, double, double, double, double>(),
        "create a parameter with snow re-distribution factors, quartiles and optionally the other parameters",
        py::arg("snow_redist_factors"),
        py::arg("quantiles"),
        py::arg("tx") = 0.0,
        py::arg("cx") = 1.0,
        py::arg("ts") = 0.0,
        py::arg("lw") = 0.1,
        py::arg("cfr") = 0.5)
      .def(
        "set_snow_redistribution_factors", &parameter::set_snow_redistribution_factors, py::arg("snow_redist_factors"))
      .def("set_snow_quantiles", &parameter::set_snow_quantiles, py::arg("quantiles"))
      .def_readwrite("tx", &parameter::tx, "float: threshold temperature determining if precipitation is rain or snow")
      .def_readwrite("cx", &parameter::cx, "float: temperature index, i.e., melt = cx(t - ts) in mm per degree C")
      .def_readwrite("ts", &parameter::ts, "float: threshold temperature for melt onset")
      .def_readwrite("lw", &parameter::lw, "float: max liquid water content of the snow")
      .def_readwrite("cfr", &parameter::cfr, "float: cfr")
      .def_readwrite("s", &parameter::s, "DoubleVector: snow redistribution factors,default =1.0..")
      .def_readwrite("intervals", &parameter::intervals, "DoubleVector: snow quantiles list default 0, 0.25 0.5 1.0");

    py::class_<state>(m, "HbvSnowState")
      .def(py::init<double, double>(), py::arg("swe") = 0.0, py::arg("sca") = 0.0)
      .def_readwrite("swe", &state::swe, "float: snow water equivalent[mm]")
      .def_readwrite("sca", &state::sca, "float: snow covered area [0..1]")
      .def(
        "distribute",
        &state::distribute,
        doc.intro("Distribute state according to parameter settings.")
          .parameters()
          .parameter("p", "HbvSnowParameter", "descr")
          .parameter(
            "force",
            "bool",
            "default true, if false then only distribute if state vectors are of different size than parameters passed")
          .returns("", "None", "")(),
        py::arg("p"),
        py::arg("force") = true)
      .def_readwrite("sw", &state::sw, "DoubleVector: snow water[mm]")
      .def_readwrite("sp", &state::sp, "DoubleVector: snow dry[mm]");

    py::class_<response>(m, "HbvSnowResponse")
      .def(py::init())
      .def_readwrite("outflow", &response::outflow, "float: from snow-routine in [mm]")
      .def_readwrite("snow_state", &response::snow_state, "HbvSnowState: swe and snow covered area");

    typedef calculator HbvSnowCalculator;
    py::class_<HbvSnowCalculator>(
      m,
      "HbvSnowCalculator",
      "Generalized quantile based HBV Snow model method\n"
      "\n"
      "This algorithm uses arbitrary quartiles to model snow. No checks are performed to assert valid input.\n"
      "The starting points of the quantiles have to partition the unity, \n"
      "include the end points 0 and 1 and must be given in ascending order.\n"
      "\n")
      .def(py::init<parameter const &>(), py::arg("parameter"))
      .def(
        "step",
        &HbvSnowCalculator::step,
        doc.intro("steps the model forward from t0 to t1, updating state and response")(),
        py::arg("state"),
        py::arg("response"),
        py::arg("t0"),
        py::arg("t1"),
        py::arg("precipitation"),
        py::arg("temperature"))

      ;
  }
}
