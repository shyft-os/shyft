/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/spatial/kalman.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  namespace sa = shyft::api;

  static void kalman_parameter(py::module_ &m) {
    using shyft::core::kalman::parameter;
    auto c =
      py::class_<parameter>(
        m,
        "KalmanParameter",
        "Defines the parameters that is used to tune the kalman-filter algorithm for temperature type of signals")
        .def(
          py::init<int, double, double, double, double>(),
          py::arg("n_daily_observations") = 8,
          py::arg("hourly_correlation") = 0.93,
          py::arg("covariance_init") = 0.5,
          py::arg("std_error_bias_measurements") = 2.0,
          py::arg("ratio_std_w_over_v") = 0.15,
          "Constructs KalmanParameter with default or supplied values")
        .def(py::init< parameter const &>(), "clone the supplied KalmanParameter", py::arg("const_ref"))
        .def_readwrite(
          "n_daily_observations", &parameter::n_daily_observations, "int:  default = 8 each 24hour, every 3 hour")
        .def_readwrite(
          "hourly_correlation",
          &parameter::hourly_correlation,
          "float: default=0.93, correlation from one-hour to the next")
        .def_readwrite(
          "covariance_init",
          &parameter::covariance_init,
          "float:  default=0.5,  for the error covariance P matrix start-values")
        .def_readwrite(
          "std_error_bias_measurements",
          &parameter::std_error_bias_measurements,
          "float: default=2.0, st.dev for the bias measurements")
        .def_readwrite(
          "ratio_std_w_over_v", &parameter::ratio_std_w_over_v, "float: default=0.06, st.dev W /st.dev V ratio");
    auto c_str = [](parameter const &o) {
      return fmt::format(
        "KalmanParameter(n_daily_observations={}, hourly_correlation={}, covariance_init={}, "
        "std_error_bias_measurements={}, ratio_std_w_over_v={})",
        o.n_daily_observations,
        o.hourly_correlation,
        o.covariance_init,
        o.std_error_bias_measurements,
        o.ratio_std_w_over_v);
    };
    c.def("__str__", c_str);
    c.def("__repr__", c_str);
  }

  static std::vector<double> kalman_x(shyft::core::kalman::state const &s) {
    return arma::conv_to<std::vector<double>>::from(s.x);
  }

  static std::vector<double> kalman_k(shyft::core::kalman::state const &s) {
    return arma::conv_to<std::vector<double>>::from(s.k);
  }

  /** flattens supplied matrix into a vector row by col */
  static std::vector<double> arma_flatten(arma::mat const &m) {
    std::vector<double> r;
    for (arma::uword i = 0; i < m.n_rows; ++i) {
      auto row = arma::conv_to<std::vector<double>>::from(m.row(i));
      for (auto v : row)
        r.push_back(v);
    }
    return r; // return flatten vector rows repeated n_cols
  }

  static std::vector<double> kalman_P(shyft::core::kalman::state const &s) {
    return arma_flatten(s.P);
  }

  static std::vector<double> kalman_W(shyft::core::kalman::state const &s) {
    return arma_flatten(s.W);
  }

  template <typename T>
  auto make_square_matrix(std::vector<T> x) {
    auto v = std::make_unique<std::vector<T>>(std::move(x));
    py::capsule buffer_handle(v.get(), [](void *p) {
      std::default_delete<std::vector<T>>{}(reinterpret_cast<std::vector<T> *>(p));
    });
    auto p = v.release();
    auto N = p->size();
    py::ssize_t M = std::sqrt(N);
    py::ssize_t B = sizeof(T);

    return py::array(
      py::buffer_info(
        p->data(),
        py::detail::any_container<py::ssize_t>{M, M},
        py::detail::any_container<py::ssize_t>{M * B, B},
        false),
      std::move(buffer_handle));
  }

  static void kalman_state(py::module_ &m) {
    using shyft::core::kalman::state;

    py::class_<state>(
      m,
      "KalmanState",
      "keeps the state of the specialized kalman-filter:\n"
      "  * x : vector[n=n_daily_observations] best estimate\n"
      "  * k : vector[n], gain factors\n"
      "  * P : matrix[nxn], covariance\n"
      "  * W : noise[nxn]\n"
      "\n")
      .def(py::init())
      .def(
        py::init<int, double, double, double>(),
        py::arg("n_daily_observations"),
        py::arg("covariance_init"),
        py::arg("hourly_correlation"),
        py::arg("process_noise_init"))
      .def(py::init<state const &>(), "clone the supplied state", py::arg("clone"))
      .def("size", &state::size, "returns the size of the state, corresponding to n_daily_observations")
      .def_property_readonly(
        "x",
        [](state const &s) {
          return shyft::pyapi::make_array(kalman_x(s));
        })
      .def_property_readonly(
        "k",
        [](state const &s) {
          return shyft::pyapi::make_array(kalman_k(s));
        })
      .def_property_readonly(
        "P",
        [](state const &s) {
          return make_square_matrix(kalman_P(s));
        }) // FIXME: just convert straight from arma - jeh
      .def_property_readonly(
        "W",
        [](state const &s) {
          return make_square_matrix(kalman_W(s));
        })
      // FIXME: delete - jeh
      .def_static("get_x", kalman_x)
      .def_static("get_k", kalman_k)
      .def_static("get_P", kalman_P)
      .def_static("get_W", kalman_W);
  }

  static void kalman_filter(py::module_ &m) {
    using shyft::core::kalman::filter;
    using shyft::core::kalman::parameter;
    py::class_<filter>(
      m,
      "KalmanFilter",
      doc
        .intro(
          "Specialized kalman filter for temperature (e.g.:solar-driven bias patterns)\n"
          "The observation point(t, v) is folded on to corresponding period of the\n"
          "day(number of periods in a day is parameterized, typically 8).\n"
          "A simplified kalman filter algorithm using the forecast bias as\n"
          "the state - variable.\n"
          "Observed bias(fc - obs) is feed into the filter and establishes the\n"
          "kalman best predicted estimates(x) for the bias.\n"
          "This bias can then be used as a correction to forecast in the future\n"
          "to compensate for systematic forecast errors.\n"
          "Credits: Thanks to met.no for providing the original source for this algorithm.\n")
        .see_also("https://en.wikipedia.org/wiki/Kalman_filter")())
      .def(py::init<parameter>(), py::arg("p") = parameter{})
      .def("create_initial_state", &filter::create_initial_state)
      .def(
        "update",
        &filter::update,
        doc.intro("update the with the observed_bias for a specific period starting at utctime t")
          .parameters()
          .parameter("observed_bias", "float", "nan if no observation is available otherwise obs - fc")
          .parameter(
            "t",
            "time",
            "utctime of the start of the observation period, this filter utilizes daily solar patterns, so time in day "
            "- cycle is the only important aspect.")
          .parameter("state", "KalmanState", "contains the kalman state,that is updated by the function upon return")(),
        py::arg("observed_bias"),
        py::arg("t"),
        py::arg("state"))
      .def_readwrite("parameter", &filter::p);
  }

  /// thin wrappers to fwd the call from py to c++
  static void update_with_forecast_geo_ts_and_obs(
    shyft::core::kalman::bias_predictor &bp,
    std::shared_ptr<std::vector<sa::TemperatureSource>> const &fc,
    shyft::time_series::dd::apoint_ts const &obs,
    shyft::time_axis::generic_dt const &ta) {
    std::vector<shyft::time_series::dd::apoint_ts> fc_ts_set;
    for (auto &geo_ts : *fc)
      fc_ts_set.push_back(geo_ts.ts);
    bp.update_with_forecast(fc_ts_set, obs, ta);
  }

  static shyft::time_series::dd::apoint_ts compute_running_bias(
    shyft::core::kalman::bias_predictor &bp,
    shyft::time_series::dd::apoint_ts const &fc_ts,
    shyft::time_series::dd::apoint_ts const &obs,
    shyft::time_axis::generic_dt const &ta) {
    if (ta.gt() != shyft::time_axis::generic_dt::FIXED)
      throw std::runtime_error("The supplied time-axis must be of type FIXED for the compute_running_bias function");
    return bp.compute_running_bias<shyft::time_series::dd::apoint_ts>(fc_ts, obs, ta.f());
  }

  static void kalman_bias_predictor(py::module_ &m) {
    using shyft::core::kalman::bias_predictor;
    using shyft::core::kalman::filter;
    using shyft::core::kalman::state;

    py::class_<bias_predictor>(
      m,
      "KalmanBiasPredictor",
      "A bias predictor using a daily pattern KalmanFilter for temperature (etc.)\n"
      "(tbd)")
      .def(py::init())
      .def(py::init<filter const &>(), py::arg("filter"))
      .def(py::init<filter const &, state const &>(), py::arg("filter"), py::arg("state"))
      .def(
        "update_with_geo_forecast",
        &update_with_forecast_geo_ts_and_obs,
        doc
          .intro(
            "update the bias-predictor with forecasts and observation\n"
            "After the update, the state is updated with new kalman estimates for the bias, .state.x\n")
          .parameters()
          .parameter(
            "temperature_sources",
            "TemperatureSourceVector",
            "ta set of forecasts, in the order oldest to the newest."
            "Note that the geo part of source is not used in this context, only the ts"
            "with periods covering parts of the observation_ts and time_axis supplied")
          .parameter("observation_ts", "Timeseries", "the observation time-series")
          .parameter(
            "time_axis",
            "TimeAxis",
            "covering the period/timesteps to be updated,"
            "e.g. yesterday, 3h resolution steps, according to the points in the filter")(),
        py::arg("temperature_sources"),
        py::arg("observation_ts"),
        py::arg("time_axis"))
      .def("update_with_forecast", &update_with_forecast_geo_ts_and_obs)
      .def(
        "update_with_forecast_vector",
        &bias_predictor::update_with_forecast<
          shyft::time_series::dd::apoint_ts,
          shyft::time_series::dd::apoint_ts,
          shyft::time_axis::generic_dt>,
        doc
          .intro(
            "update the bias-predictor with forecasts and observation\n"
            "After the update, the state is updated with new kalman estimates for the bias, .state.x\n")
          .parameters()
          .parameter(
            "temperature_sources",
            "TsVector",
            "a set of forecasts, in the order oldest to the newest."
            "With the periods covering parts of the observation_ts and time_axis supplied")
          .parameter("observation_ts", "TimeSeries", "the observation time-series")
          .parameter(
            "time_axis",
            "TimeAxis",
            "covering the period/timesteps to be updated, "
            "e.g. yesterday, 3h resolution steps, according to the points in the filter")(),
        py::arg("temperature_sources"),
        py::arg("observation_ts"),
        py::arg("time_axis"))
      .def(
        "update_with_forecast",
        &bias_predictor::update_with_forecast<
          shyft::time_series::dd::apoint_ts,
          shyft::time_series::dd::apoint_ts,
          shyft::time_axis::generic_dt>)
      .def_static(
        "compute_running_bias_ts",
        compute_running_bias,
        doc
          .intro(
            "compute the running bias timeseries,\n"
            "using one 'merged' - forecasts and one observation time - series.\n"
            "\n"
            "Before each day - period, the bias - values are copied out to form\n"
            "a continuous bias prediction time-series.\n")
          .parameters()
          .parameter("bias_predictor", "KalmanBiasPredictor", "The bias predictor object it self")
          .parameter(
            "forecast_ts",
            "TimeSeries",
            "a merged forecast ts, with period covering the observation_ts and time_axis supplied")
          .parameter("observation_ts", "TimeSeries", "the observation time-series")
          .parameter(
            "time_axis",
            "TimeAxis",
            "covering the period/timesteps to be updated, e.g. yesterday, 3h resolution steps, according to the points "
            "in the filter")
          .returns(
            "bias_ts", "TimeSeries", "With ts( time_axis,bias_vector,POINT_AVERAGE),  computed running bias-ts")(),
        py::arg("bias_predictor"),
        py::arg("forecast_ts"),
        py::arg("observation_ts"),
        py::arg("time_axis"))
      .def("compute_running_bias", &compute_running_bias)
      .def_readonly("filter", &bias_predictor::f)
      .def_readwrite("state", &bias_predictor::s);
  }

  void kalman(py::module_ &m) {
    kalman_parameter(m);
    kalman_state(m);
    kalman_filter(m);
    kalman_bias_predictor(m);
  }
}
