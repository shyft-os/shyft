/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/dtss/geo.h> // looking for geo_ts that we need to support native
#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/point_ts.h>
#include <shyft/time_series/time_axis.h>

namespace expose {
  namespace sc = shyft::core;
  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::geo_ts;
  namespace sa = shyft::api;

  template <class S>
  static auto geo_tsv_values(std::shared_ptr<std::vector<S>> const &geo_tsv, sc::utctime t) {
    std::vector<double> r;
    if (geo_tsv) {
      r.reserve(geo_tsv->size());
      for (auto const &gts : *geo_tsv)
        r.push_back(gts.ts(t));
    }
    return r;
  }

  template <class T>
  static void export_geo_point_source_type(py::module_ &m, char const *py_name, char const *py_vector, char const *py_doc) {
    auto e = py::class_<T, sa::GeoPointSource>(m, py_name, py_doc)
      .def(py::init< sc::geo_point const &, sa::apoint_ts const &>(), py::arg("midpoint"), py::arg("ts"))
      .def(py::init< geo_ts const &>(), py::arg("geo_ts"));

    py::implicitly_convertible<geo_ts, T>();

    typedef std::vector<T> TSourceVector;
    auto v = shyft::pyapi::bind_vector<TSourceVector, std::shared_ptr<TSourceVector>>(m, py_vector, py::dynamic_attr())
      .def(
        py::init(
        +[](std::vector<geo_ts> const &gtsv){
          auto r = std::make_shared<std::vector<T>>();
          r->reserve(gtsv.size());
          for (auto const &gts : gtsv)
            r->emplace_back(T{gts});
          return r;
        }),
        doc.intro("Construct from a GeoTimeSeriesVector,\n"
                  "Reference to the shyft.time_series.GeoTsMatrix.extract_geo_ts_vector() to see how to create one "
                  "from the dtss/geo extensions")(),
        py::arg("geo_ts_vector"))
      .def_static(
        "from_geo_and_ts_vector",
        +[](std::vector<sc::geo_point> const &gpv, ats_vector const &tsv) {
          if (gpv.size() != tsv.size())
            throw std::runtime_error("list of geo-points and time-series must have equal length");
          std::vector<T> r;
          r.reserve(tsv.size());
          for (size_t i = 0; i < tsv.size(); ++i)
            r.emplace_back(gpv[i], tsv[i]);
          return r;
        },
        doc.intro("Create from a geo_points and corresponding ts-vectors")
          .parameters()
          .parameter("geo_points", "GeoPointVector", "the geo-points")
          .returns("src_vector", "SourceVector", "a newly created geo-located vector of specified type")
          .parameter("tsv", "TsVector", "the corresponding time-series located at corresponding geo-point")(),
        py::arg("geo_points"),
        py::arg("tsv"))
      .def_property_readonly(
        "geo_tsvector",
        +[](const std::vector<T> &tsv){
          std::vector<geo_ts> r;
          r.reserve(tsv.size());
          for (auto const &ts : tsv)
            r.emplace_back(ts.get_geo_ts());
          return r;
        },
        doc.intro("GeoTimeSeriesVector: returns a GeoTimeSeriesVector containing GeoTimeSeries where the geo-points "
                  "are copied, but time-series are by-reference")())
      .def("values_at_time", &geo_tsv_values<T>)
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
    e.attr("vector_t") = v;

    m.def(
      "compute_geo_ts_values_at_time",
      &geo_tsv_values<T>,
      doc
        .intro("compute the ts-values of the GeoPointSourceVector type for the specified time t and return "
               "DoubleVector")
        .parameters()
        .parameter("geo_ts_vector", "GeoPointSourceVector", "Any kind of GeoPointSource vector")
        .returns("values", "DoubleValue", "List of extracted values at same size/position as the geo_ts_vector")
        .parameter("t", "int", "timestamp in utc seconds since epoch")(),
      py::arg("geo_ts_vector"),
      py::arg("t"));
  }

  static void export_geo_point_source(py::module_ &m) {
    typedef std::vector<sa::GeoPointSource> GeoPointSourceVector;

    py::class_<sa::GeoPointSource>(
      m,
      "GeoPointSource",
      "GeoPointSource contains common properties, functions\n"
      "for the point sources in Shyft.\n"
      "Typically it contains a GeoPoint (3d position), plus a time-series\n")
      .def(py::init())
      .def(py::init< sc::geo_point const &, sa::apoint_ts const &>(), py::arg("midpoint"), py::arg("ts"))
      .def(py::init< geo_ts const &>(), py::arg("geo_ts"))
      .def_readwrite("mid_point_", &sa::GeoPointSource::mid_point_, "reference to internal mid_point")
      .def("mid_point", &sa::GeoPointSource::mid_point, "returns a copy of mid_point")
      .def_property("ts", &sa::GeoPointSource::get_ts, &sa::GeoPointSource::set_ts, "TimeSeries: time-series")
      .def_readwrite("uid", &sa::GeoPointSource::uid, "str: user specified identifier, string")
      .def_property_readonly(
        "geo_ts",
        &sa::GeoPointSource::get_geo_ts,
        doc.intro("GeoTimeSeries: returns a GeoTimeSeries  where the geo-point are copied, but time-series are "
                  "by-reference")())
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
    py::implicitly_convertible<geo_ts, sa::GeoPointSource>();


    shyft::pyapi::bind_vector<GeoPointSourceVector, std::shared_ptr<GeoPointSourceVector>>(m, "GeoPointSourceVector", py::dynamic_attr())
      .def("values_at_time", &geo_tsv_values<sa::GeoPointSource>)
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
    m.def(
      "compute_geo_ts_values_at_time",
      &geo_tsv_values<sa::GeoPointSource>,
      doc.intro("compute the ts-values of the GeoPointSourceVector for the specified time t and return DoubleVector")
        .parameters()
        .parameter("geo_ts_vector", "GeoPointSourceVector", "Any kind of GeoPointSource vector")
        .returns("values", "DoubleValue", "List of extracted values at same size/position as the geo_ts_vector")
        .parameter("t", "int", "timestamp in utc seconds since epoch")(),
      py::arg("geo_ts_vector"),
      py::arg("t"));

    export_geo_point_source_type<sa::TemperatureSource>(
      m, "TemperatureSource", "TemperatureSourceVector", "geo located temperatures[deg Celcius]");
    export_geo_point_source_type<sa::PrecipitationSource>(
      m, "PrecipitationSource", "PrecipitationSourceVector", "geo located precipitation[mm/h]");
    export_geo_point_source_type<sa::WindSpeedSource>(m, "WindSpeedSource", "WindSpeedSourceVector", "geo located wind speeds[m/s]");
    export_geo_point_source_type<sa::RelHumSource>(
      m, "RelHumSource", "RelHumSourceVector", "geo located relative humidity[%rh], range 0..1");
    export_geo_point_source_type<sa::RadiationSource>(m, "RadiationSource", "RadiationSourceVector", "geo located radiation[W/m2]");
  }

  static void export_a_region_environment(py::module_ &m) {
    py::class_<sa::a_region_environment>(
      m, "ARegionEnvironment", "Contains all geo-located sources to be used by a Shyft core model")
      .def(py::init())
      .def_property(
        "temperature",
        &sa::a_region_environment::get_temperature,
        &sa::a_region_environment::set_temperature,
        "TemperatureSourceVector: temperature sources")
      .def_property(
        "precipitation",
        &sa::a_region_environment::get_precipitation,
        &sa::a_region_environment::set_precipitation,
        "PrecipitationSourceVector: precipitation sources")
      .def_property(
        "wind_speed",
        &sa::a_region_environment::get_wind_speed,
        &sa::a_region_environment::set_wind_speed,
        "WindSpeedSourceVector: wind-speed sources")
      .def_property(
        "rel_hum",
        &sa::a_region_environment::get_rel_hum,
        &sa::a_region_environment::set_rel_hum,
        "RelHumSourceVector: rel-hum sources")
      .def_property(
        "radiation",
        &sa::a_region_environment::get_radiation,
        &sa::a_region_environment::set_radiation,
        "RadiationSourceVector: radiation sources")
      .def_static(
        "create_from_geo_ts_matrix",
        +[](
           shyft::dtss::geo::geo_ts_matrix const *m,
           int t,
           int e,
           std::vector<std::int64_t> v) {
          if (m == nullptr)
            throw std::runtime_error("geo_ts_matrix must be  non-null");
          sa::a_region_environment r;
          if (v.size() == 0)
            v = std::vector<std::int64_t>{0, 1, 2, 3, 4};
          if (v.size() != 5)
            throw std::runtime_error(
              "The number of variables specified in the v-list must be 5, or 0 (defaults v-indicies)");
          if (m->shape.n_v < 5)
            throw std::runtime_error("The supplied GeoTsMatrix does not have the required 5 variables defined");

          auto fill_geo_source = [&](auto &gpsv, size_t vix) {
            gpsv.resize(m->shape.n_g);
            for (auto g = 0u; g < m->shape.n_g; ++g) {
              gpsv[g].ts = m->_ts(t, vix, e, g);
              gpsv[g].mid_point_ = m->_get_geo_point(t, vix, e, g);
              // consider to set gpsv[g].uid to t,vix,e,g to indicate where it was fetched from for presentation purposes
            }
          };
          for (auto vix : v) {
            m->shape.validate(t, vix, e, 0);
            switch (vix) {
            case 0:
              fill_geo_source(*r.temperature, vix);
              break;
            case 1:
              fill_geo_source(*r.precipitation, vix);
              break;
            case 2:
              fill_geo_source(*r.radiation, vix);
              break;
            case 3:
              fill_geo_source(*r.wind_speed, vix);
              break;
            case 4:
              fill_geo_source(*r.rel_hum, vix);
              break;
            }
          }
          return r;
        },
        doc.intro("Construct ARegionEnvironment from a specified slice of a GeoTsMatrix")
          .parameters()
          .parameter(
            "m",
            "GeoTsMatrix",
            "The matrix from which to extract temperature,precipitation,radiation,wind-speed,rel-hum data")
          .parameter("t", "int", "the forecast index to extract")
          .parameter("e", "int", "the ensemble index to extract")
          .returns("ARegionEnvironment", "ARegionEnvironment", "A ready to use region environment for interpolation")
          .parameter(
            "v",
            "IntVector",
            "variable indicies that in order of appearance selects "
            "temperature,precipitation,radiation,wind-speed,rel-hum.If empty/default, same as [0,1,2,3,4]")(),
        py::arg("m"),
        py::arg("t") = 0,
        py::arg("e") = 0,
        py::arg("v") = std::vector<std::int64_t>{})
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "serialize",
        &sa::a_region_environment::serialize_to_bytes,
        "convert ARegionEnvironment into a binary blob that later can be restored with the .deserialize(blob) method\n")
      .def_static(
        "deserialize",
        &sa::a_region_environment::deserialize_from_bytes,
        py::arg("blob"),
        "convert a blob, as returned by .serialize() into a ARegionEnvironment")
      .def_property_readonly(
        "variables",
        +[](const sa::a_region_environment &e){
          return std::make_tuple(
            std::make_tuple("temperature", e.temperature),
            std::make_tuple("precipitation", e.precipitation),
            std::make_tuple("radiation", e.radiation),
            std::make_tuple("rel_hum", e.rel_hum),
            std::make_tuple("wind_speed", e.wind_speed));
        });
      ;
  }

  void region_environment(py::module_ &m) {
    export_geo_point_source(m);
    export_a_region_environment(m);
  }
}
