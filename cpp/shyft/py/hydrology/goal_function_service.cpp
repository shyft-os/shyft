/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <memory>
#include <vector>

#include <shyft/hydrology/target_specification.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  using namespace shyft::core::model_calibration;

  void pyexport_goal_function_model(py::module_ &m) {
    {
      py::class_<goal_function_model, std::shared_ptr<goal_function_model>> t(
        m,
        "GoalFunctionModel",
        "A model identifier and goal-funcions, aka `TargetSpecificationVector`  used in Shyft region model "
        "calibrations. "
        "This plays the role of the goal-function specification for shyft region-models, so that they\n"
        "can be calibrated, and measured against observed quantities.");

      t.def(py::init())
        .def_readwrite("id", &goal_function_model::id, "int: unique model id")
        .def_readwrite(
          "goal_functions", &goal_function_model::goal_functions, "TargetSpecificationVector: The goal functions")
        .def(py::self == py::self);

      shyft::pyapi::expose_format(t);
    }
    shyft::pyapi::bind_vector<std::vector<std::shared_ptr<goal_function_model>>>(m, "GoalFunctionModelVector")
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
  }

  void goal_function_model_client_server(py::module_ &m) {
    pyexport_goal_function_model(m);
    {
      shyft::energy_market::export_client<
        shyft::energy_market::py_client<shyft::srv::client<goal_function_model>>>(
        m, "GoalFunctionClient", "The client api for the shyft.hydrology goal-function model service.");
      shyft::energy_market::export_server<
        shyft::energy_market::py_server<shyft::srv::server<shyft::srv::db<goal_function_model>>>>(
        m, "GoalFunctionServer", "The server-side component for the goal-function model repository.");
    }
  }
}
