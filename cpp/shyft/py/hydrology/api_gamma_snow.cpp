/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/gamma_snow.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  using namespace shyft::core::gamma_snow;

  void gamma_snow(py::module_ &m) {
    py::class_<parameter>(m, "GammaSnowParameter")
      .def(py::init())
      .def(
        py::init<
          int,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double>(),
        py::arg("winter_end_day_of_year"),
        py::arg("initial_bare_ground_fraction") = 0.04,
        py::arg("snow_cv") = 0.4,
        py::arg("tx") = -0.5,
        py::arg("wind_scale") = 2.0,
        py::arg("wind_const") = 1.0,
        py::arg("max_water") = 0.1,
        py::arg("surface_magnitude") = 30.0,
        py::arg("max_albedo") = 0.9,
        py::arg("min_albedo") = 0.6,
        py::arg("fast_albedo_decay_rate") = 5.0,
        py::arg("slow_albedo_decay_rate") = 5.0,
        py::arg("snowfall_reset_depth") = 0.4,
        py::arg("glacier_albedo") = false)
      // Note: due to max arity of 15, the init function does not provide all the params, TODO: consider kwargs etc.
      // instead
      .def_readwrite(
        "winter_end_day_of_year",
        &parameter::winter_end_day_of_year,
        "int: Last day of accumulation season,default= 100")
      .def_readwrite("n_winter_days", &parameter::n_winter_days, "int: number of winter-days, default 221")
      .def_readwrite(
        "initial_bare_ground_fraction",
        &parameter::initial_bare_ground_fraction,
        "float: Bare ground fraction at melt onset,default= 0.04")
      .def_readwrite(
        "snow_cv", &parameter::snow_cv, "float: Spatial coefficient variation of fresh snowfall, default= 0.4")
      .def_readwrite("tx", &parameter::tx, "float: default= -0.5 [degC]")
      .def_readwrite("wind_scale", &parameter::wind_scale, "float: Slope in turbulent wind function default=2.0 [m/s]")
      .def_readwrite("wind_const", &parameter::wind_const, "float: Intercept in turbulent wind function,default=1.0")
      .def_readwrite("max_water", &parameter::max_water, "float: Maximum liquid water content,default=0.1")
      .def_readwrite("surface_magnitude", &parameter::surface_magnitude, "float: Surface layer magnitude,default=30.0")
      .def_readwrite("max_albedo", &parameter::max_albedo, "float: Maximum albedo value,default=0.9")
      .def_readwrite("min_albedo", &parameter::min_albedo, "float: Minimum albedo value,default=0.6")
      .def_readwrite(
        "fast_albedo_decay_rate",
        &parameter::fast_albedo_decay_rate,
        "float: Albedo decay rate during melt [days],default=5.0")
      .def_readwrite(
        "slow_albedo_decay_rate",
        &parameter::slow_albedo_decay_rate,
        "float: Albedo decay rate in cold conditions [days],default=5.0")
      .def_readwrite(
        "snowfall_reset_depth",
        &parameter::snowfall_reset_depth,
        "float: Snowfall required to reset albedo [mm],default=5.0")
      .def_readwrite("glacier_albedo", &parameter::glacier_albedo, "float: Glacier ice fixed albedo, default=0.4")
      .def_readwrite(
        "calculate_iso_pot_energy",
        &parameter::calculate_iso_pot_energy,
        "float: Whether or not to calculate the potential energy flux,default=false")
      .def_readwrite(
        "snow_cv_forest_factor",
        &parameter::snow_cv_forest_factor,
        "float: default=0.0, [ratio]\n\tthe effective snow_cv gets an additional value of "
        "geo.forest_fraction()*snow_cv_forest_factor")
      .def_readwrite(
        "snow_cv_altitude_factor",
        &parameter::snow_cv_altitude_factor,
        "float: default=0.0, [1/m]\n\t the effective snow_cv gets an additional value of altitude[m]* "
        "snow_cv_altitude_factor")
      .def(
        "effective_snow_cv",
        &parameter::effective_snow_cv,
        "returns the effective snow cv, taking the forest_fraction and altitude into the equations using corresponding "
        "factors",
        py::arg("forest_fraction"),
        py::arg("altitude"))
      .def(
        "is_snow_season",
        &parameter::is_snow_season,
        "returns true if specified t is within the snow season, e.g. sept.. winder_end_day_of_year",
        py::arg("t"))
      .def(
        "is_start_melt_season",
        &parameter::is_start_melt_season,
        "true if specified interval t day of year is wind_end_day_of_year",
        py::arg("t"),
        py::arg("dt") = 0);

    py::class_<state>(m, "GammaSnowState", "The state description of the GammaSnow routine")
      .def(py::init())
      .def(
        py::init<double, double, double, double, double, double, double, double>(),
        py::arg("albedo") = 0.4,
        py::arg("lwc") = 0.1,
        py::arg("surface_heat") = 30000.0,
        py::arg("alpha") = 1.26,
        py::arg("sdc_melt_mean") = 0.0,
        py::arg("acc_melt") = 0.0,
        py::arg("iso_pot_energy") = 0.0,
        py::arg("temp_swe") = 0.0)
      .def_readwrite("albedo", &state::albedo, "float:albedo (Broadband snow reflectivity fraction),default = 0.4")
      .def_readwrite("lwc", &state::lwc, "float:lwc (liquid water content) [mm],default = 0.1")
      .def_readwrite(
        "surface_heat", &state::surface_heat, "float:surface_heat (Snow surface cold content) [J/m2],default = 30000.0")
      .def_readwrite("alpha", &state::alpha, "float:alpha (Dynamic shape state in the SDC),default = 1.26")
      .def_readwrite(
        "sdc_melt_mean",
        &state::sdc_melt_mean,
        "float:sdc_melt_mean  (Mean snow storage at melt onset) [mm],default = 0.0")
      .def_readwrite("acc_melt", &state::acc_melt, "float:acc_melt (Accumulated melt depth) [mm],default = 0.0")
      .def_readwrite(
        "iso_pot_energy",
        &state::iso_pot_energy,
        "float:iso_pot_energy (Accumulated energy assuming isothermal snow surface) [J/m2],default = 0.0")
      .def_readwrite(
        "temp_swe",
        &state::temp_swe,
        "float:temp_swe (Depth of temporary new snow layer during spring) [mm],default = 0.0");

    py::class_<response>(m, "GammaSnowResponse", "The response(output) from gamma-snow for one time-step")
      .def(py::init())
      .def_readwrite("sca", &response::sca, "float:Snow covered area [0..1]")
      .def_readwrite("storage", &response::storage, "float:Snow storage in [mm] over the area")
      .def_readwrite("outflow", &response::outflow, "float:Water out from the snow pack [mm/h]");
    typedef calculator GammaSnowCalculator;
    py::class_<GammaSnowCalculator>(m, "GammaSnowCalculator")
      .def(py::init())
      .def(
        "step",
        &GammaSnowCalculator::step,
        doc.intro("Step the snow model forward from time t to t+dt, given state, parameters and input.\n")
          .intro("Updates the state and response upon return.\n")
          .parameters()
          .parameter("state", "GammaSnowState", " param state state of type S,in/out, ref template parameters")
          .parameter("response", "GammaSnowResponse", "the response , out parameter.")
          .parameter("t", "float", "ttemperature degC, considered constant over timestep dt")
          .parameter("dt", "time", "length of time-step, seconds")
          .parameter(
            "param", "GammaSnowParameter", " response result of type R, output only, ref. template parameters\n")
          .parameter("radiation", "float", " in W/m2, considered constant over timestep")
          .parameter("precipitation", "float", " in mm/h")
          .parameter("wind_speed", "float", " in m/s")
          .parameter("rel_hum", "float", " range  0..1")
          .parameter("forest_fraction", "float", "range 0..1, influences calculation of effective snow_cv")
          .parameter("altitude", "float", " 0..x [m], influences calculation of effective_snow_cv")(),
        py::arg("state"),
        py::arg("response"),
        py::arg("t"),
        py::arg("dt"),
        py::arg("parameter"),
        py::arg("temperature"),
        py::arg("radiation"),
        py::arg("precipitation"),
        py::arg("wind_speed"),
        py::arg("rel_hum"),
        py::arg("forest_fraction"),
        py::arg("altitude"));
  }
}
