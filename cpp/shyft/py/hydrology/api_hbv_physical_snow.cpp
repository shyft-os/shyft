/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/methods/hbv_physical_snow.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {
  namespace py = shyft::py;

  void hbv_physical_snow(py::module_ &m) {
    using namespace shyft::core::hbv_physical_snow;

    py::class_<parameter>(m, "HbvPhysicalSnowParameter")
      .def(
        py::init< double, double, double, double, double, double, double, double, double, double, double, bool>(),
        py::arg("tx") = 0.0,
        py::arg("lw") = 0.1,
        py::arg("cfr") = 0.5,
        py::arg("wind_scale") = 2.0,
        py::arg("wind_const") = 1.0,
        py::arg("surface_magnitude") = 30.0,
        py::arg("max_albedo") = 0.9,
        py::arg("min_albedo") = 0.6,
        py::arg("fast_albedo_decay_rate") = 5.0,
        py::arg("slow_albedo_decay_rate") = 5.0,
        py::arg("snowfall_reset_depth") = 5.0,
        py::arg("calculate_iso_pot_energy") = false)
      .def(
        py::init<
          vector<double> const &,
          vector<double> const &,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          double,
          bool>(),
        "create a parameter with snow re-distribution factors, quartiles and optionally the other parameters",
        py::arg("snow_redist_factors"),
        py::arg("quantiles"),
        py::arg("tx") = 0.0,
        py::arg("lw") = 0.1,
        py::arg("cfr") = 0.5,
        py::arg("wind_scale") = 2.0,
        py::arg("wind_const") = 1.0,
        py::arg("surface_magnitude") = 30.0,
        py::arg("max_albedo") = 0.9,
        py::arg("min_albedo") = 0.6,
        py::arg("fast_albedo_decay_rate") = 5.0,
        py::arg("slow_albedo_decay_rate") = 5.0,
        py::arg("snowfall_reset_depth") = 5.0,
        py::arg("calculate_iso_pot_energy") = false)
      .def(
        "set_snow_redistribution_factors",
        &parameter::set_snow_redistribution_factors,
        py::arg("snow_redist_factors"))
      .def("set_snow_quantiles", &parameter::set_snow_quantiles, py::arg("quantiles"))
      .def_readwrite("tx", &parameter::tx, "float: threshold temperature determining if precipitation is rain or snow")
      .def_readwrite("lw", &parameter::lw, "float: max liquid water content of the snow")
      .def_readwrite("cfr", &parameter::cfr, "float: cfr")
      .def_readwrite("wind_scale", &parameter::wind_scale, "float: slope in turbulent wind function [m/s]")
      .def_readwrite("wind_const", &parameter::wind_const, "float: intercept in turbulent wind function")
      .def_readwrite("surface_magnitude", &parameter::surface_magnitude, "float: surface layer magnitude")
      .def_readwrite("max_albedo", &parameter::max_albedo, "float: maximum albedo value")
      .def_readwrite("min_albedo", &parameter::min_albedo, "float: minimum albedo value")
      .def_readwrite(
        "fast_albedo_decay_rate", &parameter::fast_albedo_decay_rate, "float: albedo decay rate during melt [days]")
      .def_readwrite(
        "slow_albedo_decay_rate",
        &parameter::slow_albedo_decay_rate,
        "float: albedo decay rate in cold conditions [days]")
      .def_readwrite(
        "snowfall_reset_depth", &parameter::snowfall_reset_depth, "float: snowfall required to reset albedo [mm]")
      .def_readwrite(
        "calculate_iso_pot_energy",
        &parameter::calculate_iso_pot_energy,
        "float: whether or not to calculate the potential energy flux")
      .def_readwrite("s", &parameter::s, "DoubleVector: snow redistribution factors,default =1.0..")
      .def_readwrite("intervals", &parameter::intervals, "DoubleVector: snow quantiles list default 0, 0.25 0.5 1.0");

    py::class_<state>(m, "HbvPhysicalSnowState")
      .def(py::init())
      .def(py::init< vector<double> const &, vector<double> const &, double, double, double>(),
           py::arg("albedo"), py::arg("iso_pot_energy"), py::arg("surface_heat") = 30000.0, py::arg("swe") = 0.0, py::arg("sca") = 0.0)
      .def_readwrite("albedo", &state::albedo, "float: albedo (Broadband snow reflectivity fraction)")
      .def_readwrite(
        "iso_pot_energy",
        &state::iso_pot_energy,
        "float: iso_pot_energy (Accumulated energy assuming isothermal snow surface) [J/m2]")
      .def_readwrite("surface_heat", &state::surface_heat, "float: surface_heat (Snow surface cold content) [J/m2]")
      .def_readwrite("sw", &state::sw, "DoubleVector: snow water[mm]")
      .def_readwrite("sp", &state::sp, "DoubleVector: snow dry[mm]")
      .def_readwrite("swe", &state::swe, "float: snow water equivalent[mm]")
      .def_readwrite("sca", &state::sca, "float: snow covered area [0..1]")
      .def(
        "distribute",
        &state::distribute,
        doc.intro("Distribute state according to parameter settings.")
          .parameters()
          .parameter("p", "HbvPhysicalSnowParameter", "descr")
          .parameter(
            "force",
            "bool",
            "default true, if false then only distribute if state vectors are of different size than parameters passed")
        .returns("", "None", "")(),
        py::arg("p"), py::arg("force") = true)
      ;

    py::class_<response>(m, "HbvPhysicalSnowResponse")
      .def(py::init())
      .def_readwrite("outflow", &response::outflow, "float: from snow-routine in [mm]")
      .def_readwrite("hps_state", &response::hps_state, "HbvPhysicalSnowState: current state instance")
      .def_readwrite("sca", &response::sca, "float: snow-covered area")
      .def_readwrite("storage", &response::storage, "float: snow storage [mm]");

    typedef calculator HbvPhysicalSnowCalculator;
    py::class_<HbvPhysicalSnowCalculator>(
      m,
      "HbvPhysicalSnowCalculator",
      "Generalized quantile based HBV Physical Snow model method\n"
      "\n"
      "This algorithm uses arbitrary quartiles to model snow. No checks are performed to assert valid input.\n"
      "The starting points of the quantiles have to partition the unity, \n"
      "include the end points 0 and 1 and must be given in ascending order.\n"
      "\n")
      .def(py::init< parameter const &>(),
           "creates a calculator with given parameter and initial state, notice that state is updated in this call(hmm)",
           py::arg("parameter"))
      .def(
        "step",
        &HbvPhysicalSnowCalculator::step,
        "steps the model forward from t to t+dt, updating state and response",
        py::arg("state"),
        py::arg("response"),
        py::arg("t"),
        py::arg("dt"),
        py::arg("temperature"),
        py::arg("rad"),
        py::arg("prec_mm_h"),
        py::arg("wind_speed"),
        py::arg("rel_hum"))
      ;
  }
}
