/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/methods/penman_monteith_vegetation.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>

namespace expose {

  void penman_monteith_vegetation(py::module_ &m) {
    using namespace shyft::core::penman_monteith_vegetation;

    py::class_<parameter>(m, "PenmanMonteithVegetationParameter")
      .def(py::init())
      .def(py::init<double, double>(), py::arg("height_ws"), py::arg("height_t"))
      .def_readwrite("height_t", &parameter::height_t, "float: typical value 2.0")
      .def_readwrite("height_ws", &parameter::height_ws, "float: typical value 2.0");

    py::class_<response>(m, "PenmanMonteithVegetationResponse")
      .def_readwrite("et", &response::et)
      .def_readwrite("soil_heat", &response::soil_heat);

    typedef calculator PenmanMonteithVegetationCalculator;
    py::class_<PenmanMonteithVegetationCalculator>(
      m,
      "PenmanMonteithVegetationCalculator",
      doc.intro("Evapotranspiration model, Penman-Monteith equation with vegetation properties, PM\n"
                "reference ::\n\n"
                "    L. Dingman Stewart-Penmann-Monteith evapotranspiration\n"
                "[mm/s] units.")())
      .def(py::init<parameter const &>(), py::arg("param"))
      .def(
        "evapotranspiration",
        &PenmanMonteithVegetationCalculator::evapotranspiration,
        doc.intro("calculates evapotranspiration, updating response")(),
        py::arg("response"),
        py::arg("dt"),
        py::arg("swin_radiation"),
        py::arg("lw_radiation"),
        py::arg("tempmax"),
        py::arg("tempmin"),
        py::arg("rhumidity"),
        py::arg("lai"),
        py::arg("albedo"),
        py::arg("hvegetation"),
        py::arg("cleaf_max"),
        py::arg("ks"),
        py::arg("soil_moisture_deficit"),
        py::arg("elevation"),
        py::arg("windspeed"));
  }
}
