/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/gamma_snow.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/model_calibration.h>
#include <shyft/hydrology/stacks/pt_gs_k.h>
#include <shyft/py/bindings.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/py/hydrology/expose.h>
#include <shyft/py/hydrology/expose_statistics.h>
#include <shyft/py/hydrology/stack.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/version.h>

namespace expose::pt_gs_k {
  using namespace shyft::core;
  using namespace shyft::core::pt_gs_k;
  constexpr auto stack_name = "PTGSK";

  static void parameter_state_response(py::module_ &m) {

    auto pp = expose_stack_parameter<parameter>(
      m,
      stack_name,
      "Contains the parameters to the methods used in the method stack assembly\n"
      "priestley_taylor,gamma_snow,actual_evapotranspiration,precipitation_correction,kirchner\n");
    pp.def(
        py::init<
          priestley_taylor::parameter,
          gamma_snow::parameter,
          actual_evapotranspiration::parameter,
          kirchner::parameter,
          precipitation_correction::parameter,
          glacier_melt::parameter,
          routing::uhg_parameter,
          mstack_parameter>(),
        py::arg("pt"),
        py::arg("gs"),
        py::arg("ae"),
        py::arg("k"),
        py::arg("p_corr"),
        py::arg("gm") = glacier_melt::parameter{},
        py::arg("routing") = routing::uhg_parameter{},
        py::arg("msp") = mstack_parameter{})
      .def_readwrite("pt", &parameter::pt, "PriestleyTaylorParameter: priestley_taylor parameter")
      .def_readwrite("gs", &parameter::gs, "GammaSnowParameter: gamma-snow parameter")
      .def_readwrite("gm", &parameter::gm, "GlacierMeltParameter: glacier melt parameter")
      .def_readwrite("ae", &parameter::ae, "ActualEvapotranspirationParameter: actual evapotranspiration parameter")
      .def_readwrite("kirchner", &parameter::kirchner, "KirchnerParameter: kirchner parameter")
      .def_readwrite(
        "p_corr", &parameter::p_corr, "PrecipitationCorrectionParameter: precipitation correction parameter")
      .def_readwrite(
        "routing", &parameter::routing, "UHGParameter: routing cell-to-river catchment specific parameters");

    expose_stack_state<state>(m, stack_name)
      .def(py::init<gamma_snow::state, kirchner::state>(), py::arg("gs"), py::arg("k"))
      .def_readwrite("gs", &state::gs, "GammaSnowState: gamma-snow state")
      .def_readwrite("kirchner", &state::kirchner, "KirchnerState: kirchner state");

    py::class_<response>(
      m,
      fmt::format("{}Response", stack_name).c_str(),
      "This struct contains the responses of the methods used in the method stack assembly")
      .def_readwrite("pt", &response::pt, "PriestleyTaylorResponse: response")
      .def_readwrite("gs", &response::gs, "GammaSnowResponse: gamma-snow response")
      .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "GlacierMeltResponse: glacier melt response[m3s]")
      .def_readwrite("ae", &response::ae, "ActualEvapotranspirationResponse: evapotranspiration response")
      .def_readwrite("kirchner", &response::kirchner, "KirchnerResponse: kirchner response")
      .def_readwrite("total_discharge", &response::total_discharge, "float: total stack response");
  }

  static void collectors(auto &rac, auto &rd, [[maybe_unused]] auto &sn, auto &sac) {
    rac.def_readonly("destination_area", &all_response_collector::destination_area, "float: a copy of cell area [m2]")
      .def_readonly(
        "avg_discharge",
        &all_response_collector::avg_discharge,
        "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
      .def_readonly(
        "snow_sca",
        &all_response_collector::snow_sca,
        "TsFixed: gamma snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
      .def_readonly(
        "snow_swe",
        &all_response_collector::snow_swe,
        "TsFixed: gamma snow swe, [mm] over the cell sca.. area, - at the end of timestep")
      .def_readonly(
        "snow_outflow", &all_response_collector::snow_outflow, "TsFixed: gamma snow output [m^3/s] for the timestep")
      .def_readonly(
        "glacier_melt",
        &all_response_collector::glacier_melt,
        "TsFixed: glacier melt (outflow) [m3/s] for the timestep")
      .def_readonly("ae_output", &all_response_collector::ae_output, "TsFixed: actual evap mm/h")
      .def_readonly("pe_output", &all_response_collector::pe_output, "TsFixed: pot evap mm/h")
      .def_readonly(
        "end_response",
        &all_response_collector::end_response,
        fmt::format("{}Response: at the end of collected", stack_name).c_str())
      .def_readonly("avg_charge", &all_response_collector::charge_m3s, "TsFixed: average charge in [m^3/s]");

    rd.def_readonly("destination_area", &discharge_collector::cell_area, "float: a copy of cell area [m2]")
      .def_readonly("cell_area", &discharge_collector::cell_area, "float: a copy of cell area [m2]")
      .def_readonly(
        "avg_discharge",
        &discharge_collector::avg_discharge,
        "TsFixed: Kirchner Discharge given in [m^3/s] for the timestep")
      .def_readonly(
        "snow_sca",
        &discharge_collector::snow_sca,
        "TsFixed: gamma snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
      .def_readonly(
        "snow_swe",
        &discharge_collector::snow_swe,
        "TsFixed: gamma snow swe, [mm] over the cell sca.. area, - at the end of timestep")
      .def_readonly(
        "end_response",
        &discharge_collector::end_response,
        fmt::format("{}Response: end_response, at the end of collected", stack_name).c_str())
      .def_readwrite("collect_snow", &discharge_collector::collect_snow, "bool: controls collection of snow routine")
      .def_readonly("avg_charge", &discharge_collector::charge_m3s, "TsFixed:  charge in [m^3/s]");

    // sn

    sac
      .def_readwrite(
        "collect_state",
        &state_collector::collect_state,
        "bool: if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
      .def_readonly(
        "kirchner_discharge",
        &state_collector::kirchner_discharge,
        "TsFixed: Kirchner state instant Discharge given in m^3/s")
      .def_readonly("gs_albedo", &state_collector::gs_albedo, "TsFixed: albedo")
      .def_readonly("gs_lwc", &state_collector::gs_lwc, "TsFixed: lwc")
      .def_readonly("gs_surface_heat", &state_collector::gs_surface_heat, "TsFixed: surface heat")
      .def_readonly("gs_alpha", &state_collector::gs_alpha, "TsFixed: alpha")
      .def_readonly("gs_sdc_melt_mean", &state_collector::gs_sdc_melt_mean, "TsFixed: sdc melt mean")
      .def_readonly("gs_acc_melt", &state_collector::gs_acc_melt, "TsFixed: accumulated melt")
      .def_readonly("gs_iso_pot_energy", &state_collector::gs_iso_pot_energy, "TsFixed: iso pot energy")
      .def_readonly("gs_temp_swe", &state_collector::gs_temp_swe, "TsFixed: temp swe");
  }

  using cell_all = core::cell<parameter, state, state_collector, all_response_collector>;
  using cell_opt = core::cell<parameter, state, null_collector, discharge_collector>;

  static void cells(py::module_ &m) {
    cell<cell_all>(
      m,
      fmt::format("{}CellAll", stack_name).c_str(),
      fmt::format("Refer to the {} method stack documentation", stack_name).c_str());
    cell<cell_opt>(
      m,
      fmt::format("{}CellOpt", stack_name).c_str(),
      fmt::format("Refer to the {} method stack documentation", stack_name).c_str());
    auto const cell_name = fmt::format("{}Cell", stack_name);
    statistics::gamma_snow<cell_all>(m, cell_name.c_str());
    statistics::actual_evapotranspiration<cell_all>(m, cell_name.c_str());
    statistics::priestley_taylor<cell_all>(m, cell_name.c_str());
    statistics::kirchner<cell_all>(m, cell_name.c_str());
  }

  static void models(py::module_ &m) {
    using opt_model = region_model<cell_discharge_response_t, a_region_environment>;
    using model = region_model<cell_complete_response_t, a_region_environment>;
    {
      auto expose_cell_stats = [&]<typename T, typename... O>(py::class_<T, O...> t) {
        t.def_property_readonly("gamma_snow_state", [](T &o) {
          using S = api::gamma_snow_cell_state_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("gamma_snow_response", [](T &o) {
          using S = api::gamma_snow_cell_response_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("priestley_taylor_response", [](T &o) {
          using S = api::priestley_taylor_cell_response_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("actual_evaptranspiration_response", [](T &o) {
          using S = api::actual_evapotranspiration_cell_response_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
        t.def_property_readonly("kirchner_state", [](T &o) {
          using S = api::kirchner_cell_state_statistics<typename T::cell_t>;
          return S(o.get_cells());
        });
      };
      auto [t0, t1] = expose_models<model, opt_model>(m, stack_name);
      expose_cell_stats(t0);
    }
  }

  IMPL_PYEXPORT_STACK()

}
