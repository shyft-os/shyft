/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <fmt/core.h>

#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/srv/db.h>
#include <shyft/time/utctime_utilities.h>

namespace expose {
  using namespace shyft::core;

  void pyexport_gcd_model(py::module_ &m) {
    {
      py::class_<gcd_model, std::shared_ptr<gcd_model>> t(
        m,
        "GeoCellDataModel",
        doc.intro("A model, with mandatory identifier and geo-cell-data vector.")
          .intro("The optional filled in epsg, polygon and json members can provide")
          .intro("useful information on how it was created, or can be presented.")());
      t
        .def(py::init())
        .def_readwrite("id", &gcd_model::id, "int: unique model identifier")
        .def_readwrite(
          "geo_cell_data",
          &gcd_model::gcd,
          doc.intro("GeoCellDataVector: a strongly typed list of geo-cell-data, the geo-personality of Shyft cells")())
        .def_readwrite("json", &gcd_model::json, doc.intro("str: extra useful json formatted information")())
        .def_readwrite(
          "epsg_id", &gcd_model::epsg_id, doc.intro("int: EPSG code for the polygon coordinate reference system")())
        .def_readwrite("polygon", &gcd_model::polygon, doc.intro("GeoPointVector: convex hull for the geo_cell_data")())
        .def(py::self == py::self)
        .def(py::self != py::self);
      shyft::pyapi::expose_format(t);
    }
    pyapi::bind_vector<std::vector<std::shared_ptr<gcd_model>>>(
      m, "GeoCellDataModelVector", "A strongly typed list of GeoCellDataModel's")
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;
  }

  void gcd_client_server(py::module_ &m) {
    pyexport_gcd_model(m);
    shyft::energy_market::export_client< shyft::energy_market::py_client<shyft::srv::client<gcd_model>>>(
      m, "GeoCellDataClient", "The client api for the shyft.hydrology geo_cell_data model service.");
    shyft::energy_market::export_server<
      shyft::energy_market::py_server<shyft::srv::server<shyft::srv::db<gcd_model>>>>(
      m, "GeoCellDataServer", "The server-side component for the shyft.hydrology geo_cell_data model repository.");
  }
}
