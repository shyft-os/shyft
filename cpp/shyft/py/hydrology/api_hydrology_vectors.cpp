/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <set>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/hydrology/api/a_region_environment.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/geo_algorithms.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/hydrology/api.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace expose {
  using namespace shyft::core;

  namespace sa = shyft::api;
  namespace ts = shyft::time_series;

  template <class S>
  auto create_from_geo_tsv_from_np(
    ts::dd::gta_t const &ta,
    std::vector<geo_point> const &gpv,
    py::array_t<double> const &a,
    ts::ts_point_fx point_fx) {

    if (a.ndim() != 2)
      throw std::runtime_error("Array must be two-dimensional.");

    std::vector<S> r;
    std::size_t n_ts = a.shape()[0];
    std::size_t n_pts = a.shape()[1];
    if (ta.size() != n_pts)
      throw std::runtime_error("time-axis should have same length as second dim in numpy array");
    if (n_ts != gpv.size())
      throw std::runtime_error("geo-point vector should have same size as first dim (n_ts) in numpy array");
    r.reserve(n_ts);
    for (std::size_t i = 0; i < n_ts; ++i) {
      std::vector<double> v;
      v.reserve(n_pts);
      for (std::size_t j = 0; j < n_pts; ++j)
        v.emplace_back(a.at(i, j));
      r.emplace_back(gpv[i], sa::apoint_ts(ta, v, point_fx));
    }
    return r;
  }

  static void expose_geo_cell_data_vector(py::module_ &m) {
    pyapi::bind_vector<std::vector<geo_cell_data>>(m, "GeoCellDataVector", "A vector, list, of GeoCellData")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "polygon_buffer",
        +[](std::vector<geo_cell_data> const &me, double distance) {
          std::vector<geo_point> r;
          for (auto const &g : me) {
            if (g.is_tin()) {
              auto pts = g.vertexes();
              r.insert(r.end(), pts.begin(), pts.end());
            } else
              r.emplace_back(g.mid_point());
          }
          struct geo_point_compare {
            bool operator()(geo_point const &a, geo_point const &b) const {
              return a.x < b.x || (a.x == b.x && (a.y < b.y || (a.y == b.y && a.z < b.z)));
            }
          };
          std::set<geo_point, geo_point_compare> u{r.begin(), r.end()};
          r.assign(u.begin(), u.end());
          return polygon_buffer(r, distance);
        },
        doc.intro("Computes the polygon_buffer of tin points, or mid-points, using boost::geometry::buffer algorithm")
          .intro("If number of points is large, currently > 50,  a reduction algorithm using convex_hull is first "
                 "applied")
          .parameters()
          .parameter("distance", "float", "metric[m] buffer distance")
          .returns("polygon", "GeoPointVector", "A closed polygon envelope around the cells in the model")(),
        py::arg("distance"))
      .def(
        "serialize",
        +[](const std::vector<shyft::core::geo_cell_data> &gcd){
          std::ostringstream bs(std::ios::binary);
          core_oarchive oa(bs, core::arch_info_flags);
          oa << core_nvp("geo_cell_data_vector", gcd);
          bs.flush();
          auto bss = bs.str();
          return py::cast(std::vector<char>(std::begin(bss), std::end(bss)));
        },
        doc.intro("serialize to a binary byte vector")())
      .def_static(
        "deserialize",
        +[](const std::vector<char> &bss){
          std::istringstream xmli(string(bss.data(), bss.size()),std::ios::binary);
          core_iarchive ia(xmli, core::arch_info_flags);
          std::vector<geo_cell_data> gcd;
          ia >> core_nvp("geo_cell_data_vector", gcd);
          return py::cast(gcd);
        },
        doc.intro("deserialize from a binary to GeoCellDataVector")())
      ;
  }

  static void expose_geo_ts_vector_create(py::module_ &m) {
    m.def(
      "create_temperature_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::TemperatureSource>,
      doc.intro("Create a TemperatureSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "TemperatureSourceVector",
          "a TemperatureSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")(),
      py::arg("time_axis"),
      py::arg("geo_points"),
      py::arg("np_array"),
      py::arg("point_fx"));

    m.def(
      "create_precipitation_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::PrecipitationSource>,
      doc.intro("Create a PrecipitationSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "PrecipitationSourceVector",
          "a PrecipitationSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")(),
      py::arg("time_axis"),
      py::arg("geo_points"),
      py::arg("np_array"),
      py::arg("point_fx"));

    m.def(
      "create_wind_speed_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::WindSpeedSource>,
      doc.intro("Create a WindSpeedSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "WindSpeedSourceVector",
          "a WindSpeedSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")(),
      py::arg("time_axis"),
      py::arg("geo_points"),
      py::arg("np_array"),
      py::arg("point_fx"));

    m.def(
      "create_rel_hum_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::RelHumSource>,
      doc.intro("Create a RelHumSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "RelHumSourceVector",
          "a RelHumSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")(),
      py::arg("time_axis"),
      py::arg("geo_points"),
      py::arg("np_array"),
      py::arg("point_fx"));

    m.def(
      "create_radiation_source_vector_from_np_array",
      &create_from_geo_tsv_from_np<sa::RadiationSource>,
      doc.intro("Create a RadiationSourceVector from specified time_axis,geo_points, 2-d np_array and point_fx.")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis that matches in length to 2nd dim of np_array")
        .parameter("geo_points", "GeoPointVector", "the geo-positions for the time-series, should be of length n_ts")
        .parameter("np_array", "np.ndarray", "numpy array of dtype=np.float64, and shape(n_ts,n_points)")
        .parameter("point_fx", "point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
        .returns(
          "tsv",
          "RadiationSourceVector",
          "a RadiationSourceVector of length first np_array dim, n_ts, each with geo-point and time-series with "
          "time-axis, values and point_fx")(),
      py::arg("time_axis"),
      py::arg("geo_points"),
      py::arg("np_array"),
      py::arg("point_fx"));
  }

  void hydrology_vectors(py::module_ &m) {
    expose_geo_cell_data_vector(m);
    expose_geo_ts_vector_create(m);
  }
}
