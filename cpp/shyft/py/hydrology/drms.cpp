/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <atomic>
#include <future>
#include <mutex>

#include <shyft/hydrology/srv/client.h>
#include <shyft/hydrology/srv/server.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/hydrology/api.h>

namespace expose {

  using std::string;
  using std::vector;
  using std::shared_ptr;

  using shyft::hydrology::srv::client;
  using shyft::hydrology::srv::server;
  using shyft::hydrology::srv::rmodel_type;

  using parameter_variant_t = std::variant<
    std::shared_ptr<shyft::core::pt_gs_k::parameter>,
    std::shared_ptr<shyft::core::pt_ss_k::parameter>,
    std::shared_ptr<shyft::core::pt_hs_k::parameter>,
    std::shared_ptr<shyft::core::pt_hps_k::parameter>,
    std::shared_ptr<shyft::core::r_pm_gs_k::parameter>,
    std::shared_ptr<shyft::core::pt_st_k::parameter>,
    std::shared_ptr<shyft::core::pt_st_hbv::parameter>,
    std::shared_ptr<shyft::core::r_pt_gs_k::parameter>,
    std::shared_ptr<shyft::core::r_pm_st_k::parameter>,
    std::shared_ptr<shyft::core::r_pmv_st_k::parameter>>;

  using state_variant_t = std::variant<
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_gs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_ss_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_hs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_hps_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pm_gs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_st_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::pt_st_hbv::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pt_gs_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pm_st_k::state_t>>>,
    std::shared_ptr<std::vector<shyft::api::cell_state_with_id<shyft::core::r_pmv_st_k::state_t>>>>;

  using shyft::hydrology::srv::gta_t;
  using shyft::hydrology::srv::calibration_options;
  using shyft::hydrology::srv::calibration_status;
  using shyft::core::model_calibration::target_specification;

  using namespace shyft::pyapi;
  using shyft::time_series::dd::apoint_ts;
  using shyft::core::q_adjust_result;

  namespace {
    // just to help python get an indicator of number of connections pr. process
    static std::atomic_size_t py_client_count{0};

    /**
     * @brief a util class that combines gil release with client mutex unique_lock
     */
    struct safe {
      safe(std::mutex &mx)
        : lck{mx} {
      }

      std::unique_lock<std::mutex> lck;
      scoped_gil_release gil;
    };

    /** @brief A  client
     *
     * @details
     * This class takes care of  python gil and mutex(ref safe) , ensuring that any attempt using
     * multiple python threads will be serialized.
     * gil is released while the call is in progress.
     *
     */
    struct py_client {
      std::mutex mx;
      client impl;

      py_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms)
        : impl{host_port, timeout_ms, operation_timeout_ms} {
        ++py_client_count;
      }

      ~py_client() {
        --py_client_count;
      }

      static size_t get_client_count() {
        return py_client_count.load();
      }

      string get_host_port() {
        return impl.c.host_port;
      }

      int get_timeout_ms() {
        return impl.c.timeout_ms;
      }

      bool is_open() const {
        return impl.c.is_open;
      }

      size_t get_reconnect_count() const {
        return impl.c.reconnect_count;
      }

      int get_operation_timeout_ms() const {
        return impl.c.operation_timeout_ms;
      }

      void set_operation_timeout_ms(int operation_timeout_ms) {
        impl.c.operation_timeout_ms = operation_timeout_ms;
      }

      void close(int /*timeout_ms*/ = 1000) {
        safe _{mx};
        impl.close();
      }

      string get_server_version() {
        safe _{mx};
        return impl.get_version_info();
      }

      vector<string> get_model_ids() {
        safe _{mx};
        return impl.get_model_ids();
      }

      bool remove_model(string const &mid) {
        safe _{mx};
        return impl.remove_model(mid);
      }

      bool rename_model(string const &mid, string const &new_mid) {
        safe _{mx};
        return impl.rename_model(mid, new_mid);
      }

      bool create_model(string const &mid, rmodel_type mtype, vector<shyft::core::geo_cell_data> const &gcd) {
        safe _{mx};
        return impl.create_model(mid, mtype, gcd);
      }

      auto get_state(string const &mid, shyft::api::cids_t const &cids) {
        return boost::apply_visitor(
          [](auto const &s) -> py::object {
            return py::cast(s);
          },
          [&] {
            safe _{mx};
            return impl.get_state(mid, cids);
          }());
      }

      bool set_initial_state(string const &mid) {
        safe _{mx};
        return impl.set_initial_state(mid);
      }

      [[nodiscard]] bool set_state_variant(string const &mid, state_variant_t sv) {
        safe _{mx};
        return impl.set_state(mid, to_boost_variant(sv));
      }

      template <class S>
      bool set_state(string const &mid, S s) {
        return set_state_variant(mid, state_variant_t{s});
      }

      [[nodiscard]] bool set_region_parameter_variant(string const &mid, parameter_variant_t pv) {
        safe _{mx};
        return impl.set_region_parameter(mid, to_boost_variant(std::move(pv)));
      }

      [[nodiscard]] bool set_catchment_parameter_variant(string const &mid, parameter_variant_t pv, size_t cid) {
        safe _{mx};
        return impl.set_catchment_parameter(mid, to_boost_variant(std::move(pv)), cid);
      }

      auto get_catchment_parameter(string const &mid, size_t cid) {
        auto p = [&] {
          safe _{mx};
          return impl.get_catchment_parameter(mid, cid);
        }();
        return boost::apply_visitor(
          [](auto const &x) -> py::object {
            return py::cast(x);
          },
          p);
      }

      bool has_catchment_parameter(string const &mid, size_t cid) {
        safe _{mx};
        return impl.has_catchment_parameter(mid, cid);
      }

      void remove_catchment_parameter(string const &mid, size_t cid) {
        safe _{mx};
        impl.remove_catchment_parameter(mid, cid);
      }

      auto get_region_parameter(string const &mid) {
        auto p = [&] {
          safe _{mx};
          return impl.get_region_parameter(mid);
        }();
        return boost::apply_visitor(
          [](auto const &x) {
            return py::cast(x);
          },
          p);
      }

      gta_t get_time_axis(string const &mid) {
        safe _{mx};
        return impl.get_time_axis(mid);
      }

      template <class P>
      bool set_region_parameter(string const &mid, P p) {
        return set_region_parameter_variant(mid, {std::move(p)});
      }

      template <class P>
      bool set_catchment_parameter(string const &mid, P p, size_t cid) {
        return set_catchment_parameter_variant(mid, {std::move(p)}, cid);
      }

      bool run_interpolation(
        string const &mid,
        shyft::core::interpolation_parameter const &ip_parameter,
        shyft::time_axis::generic_dt const &ta,
        shyft::api::a_region_environment const &r_env,
        bool best_effort) {
        safe _{mx};
        return impl.run_interpolation(mid, ip_parameter, ta, r_env, best_effort);
      }

      bool set_cell_environment(
        string const &mid,
        shyft::time_axis::generic_dt const &ta,
        shyft::api::a_region_environment const &r_env) {
        safe _{mx};
        return impl.set_cell_environment(mid, ta, r_env);
      }

      bool run_cells(string const &mid, size_t use_ncore, int start_step, int n_steps) {
        safe _{mx};
        return impl.run_cells(mid, use_ncore, start_step, n_steps);
      }

      q_adjust_result adjust_q(
        string const &mid,
        shyft::api::cids_t const &indexes,
        double wanted_q,
        size_t start_step = 0,
        double scale_range = 3.0,
        double scale_eps = 1e-3,
        size_t max_iter = 300,
        size_t n_steps = 1) {
        safe _{mx};
        return impl.adjust_q(mid, indexes, wanted_q, start_step, scale_range, scale_eps, max_iter, n_steps);
      }

      [[nodiscard]] apoint_ts
        get_discharge(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_discharge(mid, indexes, ix_type);
      }

      apoint_ts get_temperature(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_temperature(mid, indexes, ix_type);
      }

      apoint_ts
        get_precipitation(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_precipitation(mid, indexes, ix_type);
      }

      [[nodiscard]] apoint_ts
        get_snow_swe(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_snow_swe(mid, indexes, ix_type);
      }

      [[nodiscard]] apoint_ts
        get_charge(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_charge(mid, indexes, ix_type);
      }

      [[nodiscard]] apoint_ts
        get_snow_sca(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_snow_sca(mid, indexes, ix_type);
      }

      [[nodiscard]] apoint_ts
        get_radiation(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_radiation(mid, indexes, ix_type);
      }

      apoint_ts get_wind_speed(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_wind_speed(mid, indexes, ix_type);
      }

      [[nodiscard]] apoint_ts
        get_rel_hum(string const &mid, shyft::api::cids_t const &indexes, shyft::core::stat_scope ix_type) {
        safe _{mx};
        return impl.get_rel_hum(mid, indexes, ix_type);
      }

      [[nodiscard]] bool
        set_catchment_calculation_filter(string const &mid, std::vector<int64_t> const &catchment_id_list) {
        safe _{mx};
        return impl.set_catchment_calculation_filter(mid, catchment_id_list);
      }

      bool revert_to_initial_state(string const &mid) {
        safe _{mx};
        return impl.revert_to_initial_state(mid);
      }

      bool clone_model(string const &old_mid, string const &new_mid) {
        safe _{mx};
        return impl.clone_model(old_mid, new_mid);
      }

      bool copy_model(string const &old_mid, string const &new_mid) {
        safe _{mx};
        return impl.copy_model(old_mid, new_mid);
      }

      bool set_state_collection(string const &mid, int64_t catchment_id, bool on_or_off) {
        safe _{mx};
        return impl.set_state_collection(mid, catchment_id, on_or_off);
      }

      bool set_snow_sca_swe_collection(string const &mid, int64_t catchment_id, bool on_or_off) {
        safe _{mx};
        return impl.set_snow_sca_swe_collection(mid, catchment_id, on_or_off);
      }

      bool is_cell_env_ts_ok(string const &mid) {
        safe _{mx};
        return impl.is_cell_env_ts_ok(mid);
      }

      bool is_calculated(string const &mid, size_t cid) {
        safe _{mx};
        return impl.is_calculated(mid, cid);
      }

      bool start_calibration(
        string const &mid,
        parameter_variant_t const &p_start,
        parameter_variant_t const &p_min,
        parameter_variant_t const &p_max,
        vector<target_specification> const &spec,
        calibration_options opt) {
        if (p_start.index() != p_min.index() || p_start.index() != p_max.index())
          throw std::runtime_error("All parameters passed to calibration must be for the same model type");
        safe _{mx};
        return impl.start_calibration(
          mid, to_boost_variant(p_start), to_boost_variant(p_min), to_boost_variant(p_max), spec, opt);
      }

      calibration_status check_calibration(string const &mid) {
        safe _{mx};
        return impl.check_calibration(mid);
      }

      bool cancel_calibration(string const &mid) {
        safe _{mx};
        return impl.cancel_calibration(mid);
      }

      std::vector<shyft::core::geo_cell_data> get_geo_cell_data(string const &mid) {
        safe _{mx};
        return impl.get_geo_cell_data(mid);
      }

      shyft::api::a_region_environment get_region_env(string const &mid) {
        safe _{mx};
        return impl.get_region_env(mid);
      }

      shyft::core::interpolation_parameter get_interpolation_parameter(string const &mid) {
        safe _{mx};
        return impl.get_interpolation_parameter(mid);
      }

      string get_description(string const &mid) {
        safe _{mx};
        return impl.get_description(mid);
      }

      bool set_description(string const &mid, string const &description) {
        safe _{mx};
        return impl.set_description(mid, description);
      }

      bool do_callback(string const &mid, string const &fx_args) {
        safe _{mx};
        return impl.fx(mid, fx_args);
      }

      void close_conn() { // weird, close is not a name we can use here..
        safe _{mx};
        impl.close();
      }
    };

    /** @brief The server side component for a model repository
     *
     *
     * This class wraps/provides the server-side
     * suitable for exposure to python.
     *
     */

    struct py_server {
      server impl;
      std::function<bool(std::string, std::string)> py_callback;

      py_server(shyft::srv::fast_server_iostream_config cfg)
        : impl{cfg} {
        impl.callback = [&](std::string const &mid, std::string const &fx_args) -> bool {
          return this->py_do_callback(mid, fx_args);
        };
      }

      size_t get_alive_connections() {
        return impl.alive_connections.load();
      }

      void set_listening_port(int port) {
        impl.set_listening_port(port);
      }

      int get_listening_port() {
        return impl.get_listening_port();
      }

      void set_max_connections(int n) {
        impl.set_max_connections(size_t(n));
      }

      int get_max_connections() {
        return int(impl.get_max_connections());
      }

      void set_listening_ip(string const &ip) {
        impl.set_listening_ip(ip);
      }

      string get_listening_ip() {
        return impl.get_listening_ip();
      }

      int start_server() {
        return impl.start_server();
      }

      void stop_server(int timeout_ms) {
        impl.set_graceful_close_timeout(timeout_ms);
        impl.clear();
      }

      string get_version_info() {
        return impl.do_get_version_info();
      }

      bool is_running() {
        return impl.is_running();
      }

      ~py_server() {
      }

      //-- py exposed functions, to be used by the server-side python:
      vector<string> get_model_ids() {
        scoped_gil_release gil;
        return impl.do_get_model_ids();
      }

      auto get_model(string mid) {
        return boost::apply_visitor(
          [&](auto const &x) {
            return py::cast(x);
          },
          [&] {
            scoped_gil_release gil;
            return impl.get_model(mid);
          }());
      }

      /** this is where we attempt to fire user specified callback, */
      bool py_do_callback(string const &mid, string const &fx_arg) {
        bool r{false};
        if (py_callback) {
          try {
            r = py_callback(mid, fx_arg);
          } catch (py::error_already_set const &e) {
            shyft::pyapi::throw_formatted_exception(e);
          }
        }
        return r;
      }
    };

    void expose_common(py::module_ &m) {
      py::enum_<rmodel_type>(
        m, "RegionModelType", "Ref to DrmClient, used do specify what remote region-model type to create")
        .value("PT_GS_K", rmodel_type::pt_gs_k)
        .value("PT_GS_K_OPT", rmodel_type::pt_gs_k_opt)
        .value("PT_SS_K", rmodel_type::pt_ss_k)
        .value("PT_SS_K_OPT", rmodel_type::pt_ss_k_opt)
        .value("PT_HS_K", rmodel_type::pt_hs_k)
        .value("PT_HS_K_OPT", rmodel_type::pt_hs_k_opt)
        .value("PT_HPS_K", rmodel_type::pt_hps_k)
        .value("PT_HPS_K_OPT", rmodel_type::pt_hps_k_opt)
        .value("R_PM_GS_K", rmodel_type::r_pm_gs_k)
        .value("R_PM_GS_K_OPT", rmodel_type::r_pm_gs_k_opt)
        .value("PT_ST_K", rmodel_type::pt_st_k)
        .value("PT_ST_K_OPT", rmodel_type::pt_st_k_opt)
        .value("PT_ST_HBV", rmodel_type::pt_st_hbv)
        .value("PT_ST_K_HBV", rmodel_type::pt_st_hbv_opt)
        .value("R_PT_GS_K", rmodel_type::r_pt_gs_k)
        .value("R_PT_GS_K_OPT", rmodel_type::r_pt_gs_k_opt)
        .value("R_PM_ST_K", rmodel_type::r_pm_st_k)
        .value("R_PM_ST_K_OPT", rmodel_type::r_pm_st_k_opt)
        .value("R_PMV_ST_K", rmodel_type::r_pmv_st_k)
        .value("R_PMV_ST_K_OPT", rmodel_type::r_pmv_st_k_opt)
        .export_values();
    }
  }

  void expose_client(py::module_ &m) {
    using cm = py_client;
    using shyft::api::cell_state_with_id;
    using namespace shyft::core;
    py::class_<cm>(
      m,
      "DrmClient",
      doc.intro(
        "Distributed region model client provides all needed functionality to transfer\n"
        "Shyft region-models, and then run simulations/optimizations")(),
      py::dynamic_attr())
      .def(
        py::init<string const &, int, int>(),
        py::arg("host_port"),
        py::arg("timeout_ms"),
        py::arg("operation_timeout_ms") = 0)
      .def_property_readonly_static(
        "total_clients",
        [](py::object const &) {
          return cm::get_client_count();
        },
        "int: total clients connected")
      .def_property_readonly("host_port", &cm::get_host_port, "str: Endpoint network address of the remote server.")
      .def_property_readonly(
        "timeout_ms", &cm::get_timeout_ms, "int: Timout for remote server operations, in number milliseconds.")
      .def_property(
        "operation_timeout_ms",
        &cm::get_operation_timeout_ms,
        &cm::set_operation_timeout_ms,
        "int: Operation timeout for remote server operations, in number milliseconds.")
      .def_property_readonly("is_open", &cm::is_open, "bool: If the connection to the remote server is (still) open.")
      .def_property_readonly(
        "reconnect_count",
        &cm::get_reconnect_count,
        "int: Number of reconnects to the remote server that have been performed.")
      .def("close", &cm::close_conn, doc.intro("Close the connection. It will automatically reopen if needed.")())
      .def(
        "get_server_version",
        &cm::get_server_version,
        doc.intro("returns the server version").returns("version", "str", "Server version string")())
      .def(
        "create_model",
        &cm::create_model,
        doc.intro("create the model")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("mtype", "enum rmodel_type", "rmodel_type.pt_gs_k rmodel_type.pt_gs_k_opt or similar model type")
          .parameter("gcd", "GeoCellDataVector", "strongly typed list of GeoCellData")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("mtype"),
        py::arg("gcd"))
      .def(
        "remove_model",
        &cm::remove_model,
        doc.intro("remove the specified model")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"))
      .def(
        "rename_model",
        &cm::rename_model,
        doc.intro("rename the specified model")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("new_mid", "str", "the new wanted model-id for the model")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("new_mid"))
      .def(
        "clone_model",
        &cm::clone_model,
        doc.intro("clone the specified model")
          .parameters()
          .parameter("mid", "str", "original model")
          .parameter("new_mid", "str", "the model-id for the cloned model")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("new_mid"))
      .def(
        "copy_model",
        &cm::copy_model,
        doc.intro("full copy of the specified model")
          .parameters()
          .parameter("mid", "str", "original model")
          .parameter("new_mid", "str", "the model-id for the copied model")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("new_mid"))
      .def(
        "get_model_ids",
        &cm::get_model_ids,
        doc.intro("returns a list of model ids (mids) that is alive and known at the remote server")
          .returns("", "StringList", "Strongly typed list of strings naming the available models on the drms")())
      .def(
        "set_state",
        &cm::set_state_variant,
        doc.intro("set the cell state")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("state"))
      .def(
        "get_state",
        &py_client::get_state,
        doc.intro("Extract cell state for the optionaly specified catchment ids, cids")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("indexes", "IntVector", "list of catchment-id's, if empty, extract all")
          .returns("", "xStateWithIdVector", "strongly typed list of stack state with identifier for the cells")(),
        py::arg("mid"),
        py::arg("indexes") = shyft::api::cids_t{})
      .def(
        "get_catchment_parameter",
        &py_client::get_catchment_parameter,
        doc.intro("Get the effective catchment parameter for the specified catchment")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("cid", "int", "catchment identifier")
          .returns("", "XXXParameter", "The catchment parameter for specified catchment")(),
        py::arg("mid"),
        py::arg("cid"))
      .def(
        "get_region_parameter",
        &py_client::get_region_parameter,
        doc.intro("Get the region model parameter settings")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "XXXParameter", "The region parameter for the model")(),
        py::arg("mid"))
      .def(
        "has_catchment_parameter",
        &cm::has_catchment_parameter,
        doc.intro("Get the current time-axis of the model")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("cid", "int", "catchment identifier")
          .returns("", "bool", "True if catchment has its own parameter setting")(),
        py::arg("mid"),
        py::arg("cid"))
      .def(
        "remove_catchment_parameter",
        &cm::remove_catchment_parameter,
        doc.intro("Remove the specified catchments local parameter, and set it to the region parameter")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("cid", "int", "catchment identifier")(),
        py::arg("mid"),
        py::arg("cid"))
      .def(
        "get_time_axis",
        &py_client::get_time_axis,
        doc.intro("Get the current time-axis of the model")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "TimeAxis", "The region model time-axis")(),
        py::arg("mid"))
      .def(
        "cancel_calibration",
        &cm::cancel_calibration,
        doc.intro("Cancels any ongoing not finished calibration for the specified model")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "bool", "indicating success")(),
        py::arg("mid"))
      .def(
        "start_calibration",
        &cm::start_calibration,
        doc.intro("Start calibration with specified parameters for the model given by 'mid'")
          .parameters()
          .parameter("mid", "", "model identifier")
          .parameter("p_start", "", "starting parameters")
          .parameter("p_min", "", "min-range for parameters")
          .parameter("p_max", "", "max-range for parameters")
          .parameter("spec", "", "goal function specification, including reference series and weights")
          .parameter("opt", "", "optimizer options, like BOBYQA, GLOBAL,SCEUA or DREAM etc.")
          .returns("", "bool", "indicating success")(),
        py::arg("mid"),
        py::arg("p_start"),
        py::arg("p_min"),
        py::arg("p_max"),
        py::arg("spec"),
        py::arg("opt"))
      .def(
        "check_calibration",
        &cm::check_calibration,
        doc
          .intro(
            "Check ongoing calibration, return intermediate status with trace, or final status with trace and "
            "resulting parameters")
          .parameters()
          .parameter("mid", "", "model identifier")
          .returns("", "", "Calibration Status class with information and results")(),
        py::arg("mid"))
      .def(
        "set_region_parameter",
        &cm::set_region_parameter_variant,
        doc.intro("set region parameter")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("parameter"))
      .def(
        "set_catchment_parameter",
        &cm::set_catchment_parameter_variant,
        doc.intro("set catchment parameter")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
          .parameter("cid", "int", "catchment id that should get the supplied parameter")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("parameter"),
        py::arg("cid"))
      .def(
        "run_interpolation",
        &cm::run_interpolation,
        doc.intro("initializes the cell environment and project region environment time_series to cells.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("ip_parameter", "InterpolationParameter", "interpolation parameter")
          .parameter("ta", "TimeAxis", "time axis")
          .parameter("r_env", "ARegionEnvironment", "region environment containing all geo-located sources")
          .parameter("best_effort", "bool", "best effort type interpolation")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("ip_parameter"),
        py::arg("ta"),
        py::arg("r_env"),
        py::arg("best_effort"))
      .def(
        "set_cell_environment",
        &cm::set_cell_environment,
        doc.intro("Set the forcing data cell enviroment (cell.env_ts.* )")
          .intro("")
          .intro("The method initializes the cell environment, that keeps temperature, precipitation etc")
          .intro("for all the cells.")
          .intro("The region-model time-axis is set to the supplied time-axis, so that")
          .intro("the the region model is ready to run cells, using this time-axis.")
          .intro("")
          .intro("There are strict requirements to the content of the `region_env` parameter:")
          .intro("")
          .intro(" - rm.cells[i].mid_point()== region_env.temperature[i].mid_point() for all i")
          .intro(" - similar for precipitation,rel_hum,radiation,wind_speed")
          .intro("")
          .intro("So same number of forcing data, in the same order and geo position as the cells.")
          .intro("Tip: If time_axis is equal to the forcing time-axis, it is twice as fast.")
          .intro("")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("ta", "TimeAxis", "specifies the time-axisfor the region-model, and thus the cells")
          .parameter(
            "r_env", "ARegionEnvironment", "A region environment with ready to use forcing data for all the cells.")
          .returns("success", "bool", "true if successfull, raises exception otherwise")(),
        py::arg("mid"),
        py::arg("ta"),
        py::arg("r_env"))
      .def(
        "run_cells",
        &cm::run_cells,
        doc
          .intro(
            "Run calculation over the specified time axis. Requires that cells and environment have been "
            "initialized and interpolated.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("use_ncore", "int", "default 0, means autodetect core, otherwise you can specify 1..n")
          .parameter("start_step", "int", "default 0, from start step of time-axis")
          .parameter(
            "n_steps", "int", "default 0, means run entire time-axis, 1..n means number of steps from start_step")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("use_ncore") = 0,
        py::arg("start_step") = 0,
        py::arg("n_steps") = 0)
      .def(
        "adjust_q",
        &cm::adjust_q,
        doc.intro("State adjustment to achieve wanted/observed flow")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("indexes", "List[int]", "cell ids that should be adjusted")
          .parameter("wanted_q", "float", "the average flow first time-step we want to achieve, in m**3/s")
          .parameter("start_step", "int", "time-axis start step index, default=0")
          .parameter("scale_range", "float", "default=3, min,max= scale0/scale_range.. scale0*scale_range")
          .parameter("scale_eps", "float", "default 1e-3, stop iteration when scale-change is less than this")
          .parameter("max_iter", "int", "default=300, stop searching for solution after this limit is reached")
          .parameter("n_steps", "int", "default=1, number of steps on time-axis to average to match wanted flow")
          .returns(
            "",
            "q_adjust_result",
            "obtained flow in m**3/s. This can deviate from wanted flow due to model and state constraints. q_r is "
            "optimized q and q_0 is unadjusted.")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("wanted_q"),
        py::arg("start_step") = 0,
        py::arg("scale_range") = 3.0,
        py::arg("scale_eps") = 1e-3,
        py::arg("max_iter") = 300,
        py::arg("n_steps") = 1)
      .def(
        "get_discharge",
        &cm::get_discharge,
        doc.intro("get the discharge")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_temperature",
        &cm::get_temperature,
        doc.intro("get the temperature")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_precipitation",
        &cm::get_precipitation,
        doc.intro("get the precipitation")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_snow_swe",
        &cm::get_snow_swe,
        doc.intro("get the snow_swe")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_charge",
        &cm::get_charge,
        doc.intro("get the charge")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_snow_sca",
        &cm::get_snow_sca,
        doc.intro("get the snow_sca")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_radiation",
        &cm::get_radiation,
        doc.intro("get the radiation")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_wind_speed",
        &cm::get_wind_speed,
        doc.intro("get the wind speed")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "get_rel_hum",
        &cm::get_rel_hum,
        doc.intro("get the rel_hum")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter(
            "indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
          .parameter(
            "index_type",
            "stat_scope",
            "stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, "
            "catchment returns sum of catchments")
          .returns("", "TimeSeries", "returns sum  for catcment_ids")(),
        py::arg("mid"),
        py::arg("indexes"),
        py::arg("index_type") = shyft::core::stat_scope::catchment_ix)
      .def(
        "set_catchment_calculation_filter",
        &cm::set_catchment_calculation_filter,
        doc.intro("set catchment calculation filter")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("catchment_id_list", "List[int]", "list of catchment indexes")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("catchment_id_list"))
      .def(
        "set_current_state_as_initial_state",
        &cm::set_initial_state,
        doc
          .intro(
            "set current state as initial state for the region model.\n"
            "the method .revert_to_initial_state will restore this state.\n")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"))
      .def(
        "revert_to_initial_state",
        &cm::revert_to_initial_state,
        doc
          .intro(
            "revert model to initial state, notice that this \n"
            "require that the set_current_state_as_initial_state() or run_cells() is called.\n"
            "The latter will set initial state if not already done.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"))
      .def(
        "set_state_collection",
        &cm::set_state_collection,
        doc.intro("enable state collection for specified or all cells")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("catchment_id", "int", "to enable state collection for, -1 means turn on/off for all")
          .parameter("on_or_off", "bool", "on_or_off true|or false")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("catchment_id"),
        py::arg("on_or_off"))
      .def(
        "set_snow_sca_swe_collection",
        &cm::set_snow_sca_swe_collection,
        doc.intro("enable/disable collection of snow sca|sca for calibration purposes")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("catchment_id", "int", "to enable snow calibration for, -1 means turn on/off for all")
          .parameter("on_or_off", "bool", "on_or_off true|or false")
          .returns("", "bool", "true if succeeded")(),
        py::arg("mid"),
        py::arg("catchment_id"),
        py::arg("on_or_off"))
      .def(
        "is_cell_env_ts_ok",
        &cm::is_cell_env_ts_ok,
        doc.intro("check if all values in cell.env_ts for selected calculation-filter is non-nan, i.e. valid numbers")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "bool", "true if ok, false if not ok")(),
        py::arg("mid"))
      .def(
        "is_calculated",
        &cm::is_calculated,
        doc.intro("using the catchment_calculation_filter to decide if discharge etc. are calculated.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("cid", "int", "catchment id")
          .returns("", "bool", "true if catchment id is calculated during runs")(),
        py::arg("mid"),
        py::arg("catchment_id"))
      .def(
        "get_geo_cell_data",
        &cm::get_geo_cell_data,
        doc.intro("returns the geo cell data vector for the specified model.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "GeoCellDataVector", "The GeoCellDataVector for the specified model")(),
        py::arg("mid"))
      .def(
        "get_region_env",
        &cm::get_region_env,
        doc.intro("returns the current region-env for the specified model.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "ARegionEnvironment", "The region envirionment for the specified model")(),
        py::arg("mid"))
      .def(
        "get_interpolation_parameter",
        &cm::get_interpolation_parameter,
        doc.intro("returns the current interpolation parameters  used to interpolate region-env to the cells.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "InterpolationParameter", "The interpolation parameter for the specified model")(),
        py::arg("mid"))
      .def(
        "get_description",
        &cm::get_description,
        doc.intro("returns the user specified description string, typically a json formatted string.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "str", "The  description(json string) for the specified model")(),
        py::arg("mid"))
      .def(
        "set_description",
        &cm::set_description,
        doc.intro("Set the user specified description string, typically a json formatted string.")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .returns("", "bool", "True if successful")(),
        py::arg("mid"),
        py::arg("description"))
      .def(
        "fx",
        &cm::do_callback,
        doc.intro("Invoke the serverside fx callback, on the exlusively locked model given by mid")
          .parameters()
          .parameter("mid", "str", "model identifier")
          .parameter("fx_args", "str", "args to the callback, typically a json string")
          .returns("", "bool", "the server side evaluation result, as pr. callback return value")(),
        py::arg("mid"),
        py::arg("fx_args"));
  }

  void expose_server(py::module_ &m) {

    using srv = py_server;
    py::class_<srv>(
      m,
      "DrmServer",
      doc.intro(
        "The DRMS, distributed region model server provides the server part that implements\n"
        "the needed functionality to create, run and get results from hydrology region-models\n."
        "Together with the DrmClient, it allow the Shyft hydrology region-model operations to be\n"
        "scaled out on several servers, giving providing support for system and analysis of any size\n")())
      .def(
        py::init<shyft::srv::fast_server_iostream_config const &>(),
        doc.intro("Creates a server object with stale connection detect parameters")(),
        py::arg("config") = shyft::srv::fast_server_iostream_config{})
      .def(
        "set_listening_port",
        &srv::set_listening_port,
        doc.intro("set the listening port for the service")
          .parameters()
          .parameter("port_no", "int", "a valid and available tcp-ip port number to listen on.")
          .paramcont("typically it could be 20000 (avoid using official reserved numbers)")
          .returns("nothing", "None", "")(),
        py::arg("port_no"))
      .def(
        "set_listening_ip",
        &srv::set_listening_ip,
        doc.intro("set the listening port for the service")
          .parameters()
          .parameter("ip", "str", "ip or host-name to start listening on")
          .returns("nothing", "None", "")(),
        py::arg("ip"))
      .def(
        "start_server",
        &srv::start_server,
        doc.intro("start server listening in background, and processing messages")
          .see_also("set_listening_port(port_no),is_running")
          .returns(
            "port_no",
            "in",
            "the port used for listening operations, either the value as by set_listening_port, or if it was "
            "unspecified, a new available port")())
      .def(
        "set_max_connections",
        &srv::set_max_connections,
        doc.intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
          .parameters()
          .parameter("max_connect", "int", "maximum number of connections before denying more connections")
          .see_also("get_max_connections()")(),
        py::arg("max_connect"))
      .def(
        "get_max_connections",
        &srv::get_max_connections,
        doc.intro("returns the maximum number of connections to be served concurrently")())
      .def(
        "stop_server",
        &srv::stop_server,
        doc.intro("stop serving connections, gracefully.").see_also("start_server()")(),
        py::arg("ms") = 2000)
      .def(
        "is_running",
        &srv::is_running,
        doc.intro("true if server is listening and running").see_also("start_server()")())
      .def(
        "get_listening_port",
        &srv::get_listening_port,
        "returns the port number it's listening at for serving incoming request")
      .def_property_readonly(
        "alive_connections",
        &srv::get_alive_connections,
        doc.intro("int: returns currently alive connections to the server")())
      .def(
        "get_model_ids",
        &srv::get_model_ids,
        doc.intro("returns a list of model ids (mids) that is alive and known in the server")
          .returns("", "StringList", "Strongly typed list of strings naming the available models on the drms")())
      .def(
        "get_model",
        [](srv &s, std::string mid) {
          return s.get_model(mid);
        },
        doc
          .intro(
            "returns the named model, so it can be inspected/modified\n"
            "WIP: needs to be sure that there is no activity on the model from the remote side\n")
          .returns("", "Model", "Of the type as set by the client")())
      .def_property(
        "fx",
        [](srv &s) {
          return s.py_callback;
        },
        [](srv &s, std::function<bool(std::string, std::string)> c) {
          s.py_callback = shyft::pyapi::wrap_callback(std::move(c));
        },
        doc.intro("Callable[[str,str],bool]: server-side callable function(lambda) that takes two parameters:")
          .intro("mid :  the model id")
          .intro("fx_arg: arbitrary string to pass to the server-side function")
          .intro("The server-side fx is called when the client (or web-api) invoke the c.fx(mid,fx_arg).")
          .intro("The signature of the callback function should be callback(mid:str, fx_arg:str)->bool")
          .intro("This feature is simply enabling the users to tailor server-side functionality in python!")
          .intro("NOTE: Currently we do take exclusive lock on the model id, so that we avoid race conditions")
          .intro("Examples:\n")
          .intro(
            ">>> from shyft.hydrology import DrmServer\n"
            ">>> s=DrmServer()\n"
            ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
            ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
            ">>>   # note we can use captured Server s here!"
            ">>>     return True\n"
            ">>> # and then bind the function to the callback\n"
            ">>> s.fx=my_fx\n"
            ">>> s.start_server()\n"
            ">>> : # later using client from anywhere to invoke the call\n"
            ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n")());
  }

  void drms(py::module_ &m) {
    expose_common(m);
    expose_client(m);
    expose_server(m);
  }
}
