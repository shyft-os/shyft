#pragma once

#include <algorithm>
#include <cstdint>
#include <memory>
#include <ranges>
#include <stdexcept>
#include <string>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/core/fs_compat.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/geo_ts.h>

#include <pybind11/cast.h>
#include <pybind11/functional.h>
#include <pybind11/gil.h>
#include <pybind11/numpy.h>
#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

namespace shyft {

  // NOTE:
  //   temporary alias to set up migration path from boost::python
  //   preferrably it was called api, but already taken. not in
  //   py namespace since this is defined multiple places and could
  //   collide.
  //    - jeh
  namespace py = pybind11;

  namespace pyapi {

    template <typename T>
    std::optional<T> try_cast(auto &&h) {
      try {
        return h.template cast<T>();
      } catch (py::cast_error const &) {
        return std::nullopt;
      }
    }

    /*
     * @brief wrapped invocation of python callback
     * @details
     * invokes callback and translates any python errors to std::runtime_error
     * to preserve same behaviour as when using boost::python
     */
    auto invoke_callback(auto &&f, auto &&...x) {
      try {
        return std::invoke(SHYFT_FWD(f), SHYFT_FWD(x)...);
      } catch (py::error_already_set const &e) {
        throw std::runtime_error(e.what());
      }
    }

    template <typename T, typename... R>
    std::function<T(R...)> wrap_callback(std::function<T(R...)> f) {
      return {[f_ = std::move(f)](R... r) {
        return invoke_callback(f_, r...);
      }};
    }

    inline auto to_hex(std::vector<char> const &b) {
      static char const _h[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
      std::string s;
      s.reserve(2 * b.size());
      for (std::size_t i = 0; i < b.size(); ++i) {
        std::uint8_t x = b[i];
        s.push_back(_h[x >> 4]);
        s.push_back(_h[x & 0xf]);
      }
      return s;
    }

    constexpr char hex_to_num(char x) { // assume x in _h
      return x - (x < 'a' ? '0' : 'a' - 10);
    }

    inline auto to_byte_vector(std::string const &s) {
      if (s.size() == 0)
        return std::vector<char>{};
      if (s.size() % 2)
        throw std::runtime_error("hex_str should be even-sized");

      std::vector<char> r;
      r.reserve(s.size() / 2);
      char const *p = s.data();
      for (std::size_t i = 0; i < s.size() / 2; ++i) {
        r.push_back((hex_to_num(p[2 * i]) << 4) | hex_to_num(p[2 * i + 1]));
      }
      return r;
    }

    template <typename T>
    struct extract {
      py::handle const &obj;

      bool check() const {
        return py::isinstance<T>(obj);
      }

      operator bool() const {
        return check();
      }

      T operator()() const {
        return obj.cast<T>();
      }
    };

    using scoped_gil_aquire = py::gil_scoped_acquire;
    using scoped_gil_release = py::gil_scoped_release;

    template <typename T, typename... O>
    auto expose_comparison(py::class_<T, O...> c) {
      c.def(py::self == py::self).def(py::self != py::self);
      if constexpr (requires(T const &t) { t < t; })
        c.def(py::self < py::self).def(py::self <= py::self).def(py::self > py::self).def(py::self >= py::self);
      return c;
    }

    inline void throw_formatted_exception(py::error_already_set const &e) {
      auto traceback = py::module_::import("traceback");
      py::list info = traceback.attr("format_exception")(e.type(), e.value(), e.trace());
      std::vector<std::string> msg;
      for (auto const &i : info)
        msg.push_back(i.cast<std::string>());
      throw std::runtime_error(fmt::format("{}", fmt::join(msg, "\n")));
    }

#define SHYFT_PYTHON_MODULE(name, module_object) PYBIND11_MODULE(name, module_object)

    /**
     * note: the make_array functions can only be called if keeping GIL
     */
    template <typename T>
    auto make_array(std::vector<T> x) {
      auto v = std::make_unique<std::vector<T>>(std::move(x));
      // next statements assumes GIL
      py::capsule buffer_handle(v.get(), [](void *p) {
        std::default_delete<std::vector<T>>{}(reinterpret_cast<std::vector<T> *>(p));
      });
      auto p = v.release();
      return py::array(
        py::buffer_info(
          p->data(),
          py::detail::any_container<py::ssize_t>{(py::ssize_t) p->size()},
          py::detail::any_container<py::ssize_t>{(py::ssize_t) sizeof(T)},
          false),
        std::move(buffer_handle));
    }

    template <typename T>
    auto make_array_2d(std::vector<std::vector<T>> x) {
      if (x.empty())
        return py::array();
      std::vector<T> flat_array;
      std::size_t column_size = x[0].size();
      flat_array.reserve(x.size() * column_size);
      for (auto const &inner : x) {
        flat_array.insert(flat_array.end(), inner.begin(), inner.end());
        if (column_size != inner.size())
          throw std::runtime_error("make_array_2d: All columns must have same size");
      }
      auto v = std::make_unique<std::vector<T>>(std::move(flat_array));
      // next statements assumes GIL
      py::capsule buffer_handle(v.get(), [](void *p) {
        std::default_delete<std::vector<T>>{}(reinterpret_cast<std::vector<T> *>(p));
      });

      std::vector<py::ssize_t> shape = {
        static_cast<py::ssize_t>(x.size()), static_cast<py::ssize_t>(column_size)}; // {rows,columns}
      std::vector<py::ssize_t> strides = {
        static_cast<py::ssize_t>(sizeof(T) * column_size), static_cast<py::ssize_t>(sizeof(T))}; // c-style strides
      auto p = v.release();
      return py::array(
        py::buffer_info(
          p->data(), // Pointer to the flattened data.
          sizeof(T), // Size of each element (e.g., sizeof(double)).
          py::format_descriptor<T>::format(),
          shape.size(), // Number of dimensions.
          shape,        // Shape of the array, rows,cols in 2D
          strides,      // Strides.
          false),       // allow user to own/modify the returned ccopy
        std::move(buffer_handle));
    }

    template <typename V>
    auto make_array_ref(V &&x) {
      auto d = x.data();
      using U = std::remove_pointer_t<decltype(d)>;
      using T = std::remove_const_t<U>;

      py::capsule buffer_handle([]() { });

      return py::array(
        py::buffer_info(
          const_cast<T *>(d),
          py::detail::any_container<py::ssize_t>{(py::ssize_t) x.size()},
          py::detail::any_container<py::ssize_t>{(py::ssize_t) sizeof(T)},
          std::is_const_v<U>),
        buffer_handle);
    }

    /**
     * @brief make a python namespace link
     * @details
     * When using pybind11, and creating a top-level module 'x',
     * then we must ensure that module.__path__ contains [x-subdir],
     * so that python name-space resolution works.
     * E.g.: `from shyft.hydrology import pt_gs_k`.
     * This only works if namespace resolution is in effect.
     * The existence of shyft/hydrology...so extension hides this,
     * unless the `module.__path__` list have entries that
     * points to the namespace modules directories.
     * This mechanism allow us to build a pure pybind11 module hierarchy with a mix of pybind11 modules
     * and sometimes also mixed with python files.
     * @param m the module with name 'x', that is to about to be created, e.g. ref `SHYFT_PYTHON_MODULE(x,m)`
     * @param path_to_namespace_dir the full path to the 'x' directory.
     */
    inline void make_python_namespace(py::module_ const &m, fs::path const &path_to_namespace_dir) {
      py::list path_list;
      path_list.append(path_to_namespace_dir.string());
      m.attr("__path__") = path_list;
    }
  }

}
