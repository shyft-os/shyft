#pragma once

#include <algorithm>
#include <cctype>
#include <cstddef>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

#include <boost/describe/enumerators.hpp>
#include <boost/describe/members.hpp>
#include <boost/describe/modifiers.hpp>
#include <boost/mp11/algorithm.hpp>
#include <fmt/core.h>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>
#include <shyft/core/utility.h>
#include <shyft/py/bindings.h>

#include <pybind11/cast.h>
#include <pybind11/pybind11.h>

namespace shyft::pyapi {

  template <reflected_enum T>
  void expose_enumerators(py::enum_<T> &py_type) {
    for_each_enum<T>([&](auto tag) {
      std::string name = tag.name;
      std::ranges::transform(name, name.begin(), [](unsigned char c) {
        return std::toupper(c);
      });
      py_type.value(name.c_str(), tag.value);
    });
  }

  template <reflected_struct T, typename... O>
  void expose_members(py::class_<T, O...> &py_type) {
    for_each_member<T>([&](auto tag) {
      py_type.def_readwrite(tag.name, tag.pointer);
    });
  }
#ifndef _MSC_VER
  // this uses bases_of, that does/did not compile on ms c++
  template <reflected_struct T, typename... O>
  void expose_constructor(py::class_<T, O...> &py_type) {
    [&]<typename... B, std::size_t... I, typename... M>(
      std::type_identity<std::tuple<B...>>, std::index_sequence<I...>, std::type_identity<std::tuple<M...>>) {
      py_type.def(
        py::init([](typename B::type... bases, member_type_t<M::pointer>... args) {
          return new T{bases..., args...};
        }),
        py::arg(fmt::format("base{}", I).c_str())...,
        py::pos_only(),
        py::arg(M::name)...);
    }(std::type_identity<bases_of<T>>{},
      std::make_index_sequence<std::tuple_size_v<bases_of<T>>>(),
      std::type_identity<members_of<T, boost::describe::mod_any_access>>{});
  }
#endif
}
