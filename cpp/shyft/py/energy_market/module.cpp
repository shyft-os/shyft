#include <map>
#include <memory>
#include <set>
#include <string>
#include <string_view>
#include <vector>

#include <boost/dll.hpp>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/module.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/version.h>

namespace shyft::energy_market {

  void pyexport_attributes(py::module_ &m) {
    auto expose_utc_attr_map_type = [&]<typename T>(std::type_identity<T>, auto name) {
      using U = std::map<shyft::core::utctime, std::shared_ptr<T>>;
      auto t =
        pyapi::bind_map<U, std::shared_ptr<U>>(m, name)
          .def(
            "__call__",
            +[](U const &m, core::utctime const &t) -> std::shared_ptr<T> {
              auto it = std::find_if(m.rbegin(), m.rend(), [&t](auto const &v) -> bool {
                return v.first <= t;
              });
              return it != m.rend() ? it->second : nullptr;
            },
            "Find value for a given time.\n",
            py::arg("time"))
          .def(py::self == py::self)
          .def(py::self != py::self);
      return t;
    };
    expose_utc_attr_map_type(std::type_identity<hydro_power::xy_point_curve>{}, "t_xy");
    expose_utc_attr_map_type(std::type_identity<hydro_power::xy_point_curve_with_z>{}, "t_xyz");
    expose_utc_attr_map_type(std::type_identity<std::vector<hydro_power::xy_point_curve_with_z>>{}, "t_xyz_list");
    expose_utc_attr_map_type(std::type_identity<hydro_power::turbine_description>{}, "t_turbine_description");

    {
      auto t_ugt = py::enum_<stm::unit_group_type>(
        m,
        "UnitGroupType",
        doc.intro(
          "The unit-group type specifies the purpose of the group, and thus also how\n"
          "it is mapped to the optimization as constraint. E.g. operational reserve fcr_n.up.\n"
          "Current mapping to optimizer/shop:\n\n"
          "    *        FCR* :  primary reserve, instant response, note, sensitivity set by droop settings on the unit "
          "\n"
          "    *       AFRR* : automatic frequency restoration reserve, ~ minute response\n"
          "    *       MFRR* : it is the manual frequency restoration reserve, ~ 15 minute response\n"
          "    *         FFR : NOT MAPPED, fast frequency restoration reserve, 49.5..49.7 Hz, ~ 1..2 sec response\n"
          "    *        RR*  : NOT MAPPED, replacement reserve, 40..60 min response\n"
          "    *      COMMIT : currently not mapped\n"
          "    *  PRODUCTION : used for energy market area unit groups\n\n")());
      pyapi::expose_enumerators(t_ugt);
      t_ugt.export_values();
    }
    {
      pyapi::def_a_wrap<std::int8_t>(m, "i8_attr");
      pyapi::def_a_wrap<std::int16_t>(m, "i16_attr");
      pyapi::def_a_wrap<std::uint16_t>(m, "u16_attr");
      pyapi::def_a_wrap<std::int32_t>(m, "i32_attr");
      pyapi::def_a_wrap<std::int64_t>(m, "i64_attr");
      pyapi::def_a_wrap<double>(m, "double_attr");
      pyapi::def_a_wrap<bool>(m, "bool_attr");
      pyapi::def_a_wrap<std::string>(m, "string_attr");
      pyapi::def_a_wrap<core::geo_point>(m, "geo_point_attr");

      pyapi::def_a_wrap<time_series::dd::apoint_ts>(m, "ts_attr");
      pyapi::def_a_wrap<time_axis::generic_dt>(m, "time_axis_attr");

      pyapi::def_a_wrap<stm::t_turbine_description_>(m, "turbine_description_attr");
      pyapi::def_a_wrap<stm::t_xy_>(m, "t_xy_attr");
      pyapi::def_a_wrap<stm::t_xyz_>(m, "t_xyz_attr");
      pyapi::def_a_wrap<stm::t_xyz_list_>(m, "t_xy_z_list_attr");
      pyapi::def_a_wrap<stm::unit_group_type>(m, "unit_group_type_attr");
      pyapi::def_a_wrap<std::vector<std::pair<core::utctime, std::string>>>(m, "message_list_attr");
    }
    pyapi::bind_map<decltype(id_base::tsm)>(m, "TsAttrDict");
    pyapi::bind_map<decltype(id_base::custom)>(m, "AnyAttrDict");
  }

  struct mod_ext {
    static auto to_blob(std::shared_ptr<market::model> const &m) {
      auto s = m->to_blob();
      return std::vector<char>(s.begin(), s.end());
    }

    static auto from_blob(std::vector<char> &blob) {
      std::string s(blob.begin(), blob.end());
      return market::model::from_blob(s);
    }
  };

  auto pyexport_market_model_fwd(py::module_ &m) {
    return expose_system_type<market::model>(
      m,
      "Model",
      doc.intro("The Model class describes the  LTM (persisted) model")
        .intro("A model consists of model_areas and power-lines interconnecting them.")
        .intro("To buid a model use the .add_area() and .add_power_line() methods")
        .see_also("ModelArea,PowerLine,PowerModule")());
  }

  void pyexport_market_model(py::module_ &m, auto &mm) {

    py::class_<market::model_builder>(
      m, "ModelBuilder", doc.intro("This class helps building an EMPS model, step by step")())
      .def(
        py::init<std::shared_ptr<market::model> &>(),
        doc.intro("Make a model-builder for the model")
          .intro("The model can be modified/built using the methods")
          .intro("available in this class")
          .parameters()
          .parameter("model", "Model", "the model to be built/modified")(),
        py::arg("model"))
      .def(
        "create_power_line",
        &market::model_builder::create_power_line,
        doc.intro("create and add a power line with capacity_MW between area a and b to the model")
          .parameters()
          .parameter("id", "int", "unique ID of the power-line")
          .parameter("name", "string", "unique name of the power-line")
          .parameter("json", "string", "json for the power-line")
          .parameter("a", "ModelArea", "from existing model-area, that is part of the current model")
          .parameter("b", "ModelArea", "to existing model-area, that is part of the current model")
          .returns("pl", "PowerLine", "the newly created power-line, that is now a part of the model")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json"),
        py::arg("a"),
        py::arg("b"))
      .def(
        "create_model_area",
        &market::model_builder::create_model_area,
        doc.intro("create and add an area to the model.")
          .intro("ensures that area_name, and that area_id is unique.")
          .parameters()
          .parameter("id", "int", "unique identifier for the area, must be unique within model")
          .parameter("name", "string", "any valid area-name, must be unique within model")
          .parameter("json", "string", "json for the area")
          .returns("area", "ModelArea", "a reference to the newly added area")
          .see_also("add_area")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_power_module",
        &market::model_builder::create_power_module,
        doc.intro("create and add power-module to the area, doing validity checks")
          .parameters()
          .parameter("id", "int", "encoded power_type/load/wind module id")
          .parameter("name", "string", "unique module-name for each area")
          .parameter("json", "string", "json for the pm")
          .parameter("model_area", "ModelArea", "the model-area for which we create a power-module")
          .returns("pm", "PowerModule", "a reference to the created and added power-module")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json"),
        py::arg("model_area"));


    mm.def(
        py::init<int, std::string const &, std::string const &>(),
        doc.intro("constructs a Model object with the specified parameters")
          .parameters()
          .parameter("id", "int", "a global unique identifier of the mode")
          .parameter("name", "string", "the name of the model")
          .parameter("json", "string", "extra info as json for the model")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def_readwrite(
        "created",
        &market::model::created,
        doc.intro("time: The timestamp when the model was created, utc seconds 1970")())
      .def_readonly(
        "area", &market::model::area, doc.intro("ModelAreaDict: a dict(area-name,area) for the model-areas")())
      .def_readonly(
        "power_lines",
        &market::model::power_lines,
        doc.intro("PowerLineList: list of power-lines,each with connection to the areas they interconnect")())
      .def(
        "equal_structure",
        &market::model::equal_structure,
        doc.intro("Compare this model with other_model for equality in topology and interconnections.")
          .intro("The comparison is using each object`.id` member to identify the same objects.")
          .intro("Notice that the attributes of the objects are not considered, only the topology.")
          .parameters()
          .parameter("other", "Model", "The model to compare with")
          .returns("equal", "bool", "true if other_model has structure and objects as self")(),
        py::arg("other"))
      .def(
        "create_model_area",
        [](std::shared_ptr<market::model> &m, int id, std::string const &name, std::string const &json) {
          return market::model_builder{m}.create_model_area(id, name, json);
        },
        doc.intro("create and add an area to the model.")
          .intro("ensures that area_name, and that area_id is unique.")
          .parameters()
          .parameter("uid", "int", "unique identifier for the area, must be unique within model")
          .parameter("name", "string", "any valid area-name, must be unique within model")
          .parameter("json", "string", "json for the area")
          .returns("area", "ModelArea", "a reference to the newly added area")
          .see_also("add_area")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_power_module",
        [](
          std::shared_ptr<market::model> &m,
          std::shared_ptr<market::model_area> &a,
          int id,
          std::string const &name,
          std::string const &json) {
          return market::model_builder{m}.create_power_module(id, name, json, a);
        },
        doc.intro("create and add power-module to the area, doing validity checks")
          .parameters()
          .parameter("model_area", "ModelArea", "the model-area for which we create a power-module")
          .parameter("uid", "string", "encoded power_type/load/wind module id")
          .parameter("module_name", "string", "unique module-name for each area")
          .parameter("json", "string", "json for the pm")
          .returns("pm", "PowerModule", "a reference to the created and added power-module")(),
        py::arg("model_area"),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_power_line",
        [](
          std::shared_ptr<market::model> &m,
          std::shared_ptr<market::model_area> &a,
          std::shared_ptr<market::model_area> &b,
          int id,
          std::string const &name,
          std::string const &json) {
          return market::model_builder{m}.create_power_line(id, name, json, a, b);
        },
        doc.intro("create and add a power line with capacity_MW between area a and b to the model")
          .parameters()
          .parameter("a", "ModelArea", "from existing model-area, that is part of the current model")
          .parameter("b", "ModelArea", "to existing model-area, that is part of the current model")
          .parameter("uid", "int", "unique ID of the power-line")
          .parameter("name", "string", "unique name of the power-line")
          .parameter("json", "string", "json for the power-line")
          .returns("pl", "PowerLine", "the newly created power-line, that is now a part of the model")(),
        py::arg("a"),
        py::arg("b"),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "equal_content",
        &market::model::equal_content,
        doc
          .intro(
            "Compare this model with other_model for equality, except for the `.id`, `.name`,`.created`, "
            "attributes of the model it self.")
          .intro(
            "This is the same as the equal,==, operation, except that the self model local attributes are not "
            "compared.")
          .intro(
            "This method can be used to determine that two models have the same content, even if they model.id "
            "etc. are different.")
          .parameters()
          .parameter("other", "Model", "The model to compare with")
          .returns(
            "equal",
            "bool",
            "true if other have exactly the same content as self(disregarding the model .id,.name,.created,.json "
            "attributes)")(),
        py::arg("other"))
      .def(
        "to_blob",
        &mod_ext::to_blob,
        doc.intro("serialize the model into a blob")
          .returns("blob", "ByteVector", "serialized version of the model")
          .see_also("from_blob")())
      .def_static(
        "from_blob",
        &mod_ext::from_blob,
        doc.intro("constructs a model from a blob previously created by the to_blob method")
          .parameters()
          .parameter("blob", "ByteVector", "blob representation of the model, as create by the to_blob method")(),
        py::arg("blob"));
  }

  auto pyexport_market_model_area_fwd(py::module_ &m) {
    auto ma = expose_system_type<market::model_area>(
      m,
      "ModelArea",
      doc.intro("The ModelArea class describes the EMPS LTM (persisted) model-area")
        .intro("A model-area consists of power modules and hydro-power-system.")
        .intro("To buid a model-are use the .add_power_module() and the hydro-power-system builder")
        .see_also("Model,PowerLine,PowerModule,HydroPowerSystem")());
    pyapi::bind_map<std::map<int, std::shared_ptr<market::model_area>>>(m, "AreaDict")
      .def(py::self == py::self)
      .def(py::self != py::self);
    return ma;
  }

  void pyexport_market_model_area(py::module_ &m, auto &ma) {
    ma.def(
        py::init<std::shared_ptr<market::model> const &, int, std::string const &, std::string const &>(),
        doc.intro("constructs a ModelArea object with the specified parameters")
          .parameters()
          .parameter("model", "Model", "the model owning the created model-area")
          .parameter("id", "int", "a global unique identifier of the model-area")
          .parameter("name", "string", "the name of the model-area")
          .parameter("json", "string", "extra info as json")(),
        py::arg("model"),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def_readonly(
        "power_modules",
        &market::model_area::power_modules,
        doc.intro("PowerModuleDict: power-modules in this area, a dictionary using power-module unique id")())
      .def_property(
        "detailed_hydro",
        &market::model_area::get_detailed_hydro,
        &market::model_area::set_detailed_hydro,
        doc.intro("HydroPowerSystem:  detailed hydro description.").see_also("HydroPowerSystem")())
      .def_property_readonly("model", &market::model_area::get_model, "Model: the model for the area")
      .def(
        "equal_structure",
        &market::model_area::equal_structure,
        doc.intro("Compare this model-area with other_model-area for equality in topology and interconnections.")
          .intro("The comparison is using each object`.id` member to identify the same objects.")
          .intro("Notice that the attributes of the objects are not considered, only the topology.")
          .parameters()
          .parameter("other", "ModelArea", "The model-area to compare with")
          .returns("equal", "bool", "true if other_model has structure and objects as self")(),
        py::arg("other"))
      .def(
        "create_power_module",
        [](std::shared_ptr<market::model_area> &a, int id, std::string const &name, std::string const &json) {
          return market::model_builder{a->mdl}.create_power_module(id, name, json, a);
        },
        doc.intro("create and add power-module to the area, doing validity checks")
          .parameters()
          .parameter("id", "string", "encoded power_type/load/wind module id")
          .parameter("module_name", "string", "unique module-name for each area")
          .parameter("json", "string", "json for the pm")
          .returns("pm", "PowerModule", "a reference to the created and added power-module")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = "");
  }

  void pyexport_power_line(py::module_ &m) {
    expose_system_type<market::power_line>(
      m,
      "PowerLine",
      doc.intro("The PowerLine class describes the LTM (persisted) power-line")
        .intro("A power-line represents the transmission capacity between two model-areas.")
        .intro("Use the ModelArea.create_power_line(a1,a2,id) to build a power line")
        .see_also("Model,ModelArea,PowerModule,HydroPowerSystem")())
      .def(
        py::init<
          std::shared_ptr<market::model> const &,
          std::shared_ptr<market::model_area> &,
          std::shared_ptr<market::model_area> &,
          int,
          std::string const &,
          std::string const &>(),
        doc.intro("constructs a PowerLine object between area 1 and 2 with the specified id")
          .parameters()
          .parameter("model", "Model", "the model for the power-line")
          .parameter("area_1", "ModelArea", "a reference to an existing area in the model")
          .parameter("area_2", "ModelArea", "a reference to an existing area in the model")
          .parameter("id", "int", "a global unique identifier for the power-line")
          .parameter("name", "string", "a global unique name for the power-line")
          .parameter("json", "string", "extra json for the power-line")(),
        py::arg("model"),
        py::arg("area_1"),
        py::arg("area_2"),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def_property_readonly(
        "model", &market::power_line::get_model, doc.intro("Model: the model for this power-line")())
      .def_property(
        "area_1",
        &market::power_line::get_area_1,
        &market::power_line::set_area_1,
        doc.intro("ModelArea: reference to area-from")())
      .def_property(
        "area_2",
        &market::power_line::get_area_2,
        &market::power_line::set_area_2,
        doc.intro("ModelArea: reference to area-to")())
      .def(
        "equal_structure",
        &market::power_line::equal_structure,
        doc.intro("Compare this power-line with the other for equality in topology and interconnections.")
          .intro("The comparison is using each object`.id` member to identify the same objects.")
          .intro("Notice that the attributes of the objects are not considered, only the topology.")
          .parameters()
          .parameter("other", "", "The model-area to compare with")
          .returns("equal", "bool", "true if other has structure equal to self")(),
        py::arg("other"));
    pyapi::bind_vector<std::vector<std::shared_ptr<market::power_line>>>(m, "PowerLineList")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  void pyexport_power_module(py::module_ &m) {
    expose_system_type<market::power_module>(
      m,
      "PowerModule",
      doc.intro("The PowerModule class describes the LTM (persisted) power-module")
        .intro("A power-module represents an actor that consume/produces power for given price/volume")
        .intro("characteristics. The user can influence this characteristics giving")
        .intro("specific semantic load_type/power_type and extra data and/or relations to")
        .intro("other power-modules within the same area.")
        .see_also("Model,ModelArea,PowerLine,HydroPowerSystem")())
      .def(
        py::init<std::shared_ptr<market::model_area> const &, int, std::string, std::string>(),
        doc.intro("constructs a PowerModule with specified mandatory name and module-id")
          .parameters()
          .parameter("area", "ModelArea", "the area for this power-module")
          .parameter("id", "int", "unique pm-id for area")
          .parameter("name", "string", "the name of the power-module")
          .parameter("json", "string", "optional json ")(),
        py::arg("area"),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def_property_readonly(
        "area", &market::power_module::get_area, "ModelArea: the model-area for this power-module");
    pyapi::bind_map<std::map<int, std::shared_ptr<market::power_module>>>(m, "PowerModuleDict")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  void py_destroy(void *o) {
    if (auto pyo = static_cast<py::object *>(o))
      delete pyo;
  }

  void pyexport(py::module_ &m) {
    m.attr("__doc__") = "Shyft Open Source Energy Market model core";
    m.attr("__version__") = _version_string();
    pyapi::make_python_namespace(m, boost::dll::this_line_location().parent_path() / "energy_market");
    py::module_ time_series = py::module_::import("shyft.time_series");
    pyexport_time_series_support(m);
    hydro_power::pyexport_xy_point_curves(m);

    pyexport_attributes(m);
    auto mm = pyexport_market_model_fwd(m);
    auto ma = pyexport_market_model_area_fwd(m);
    hydro_power::pyexport(m);
    pyexport_power_line(m);
    pyexport_power_module(m);
    pyexport_market_model_area(m, ma);
    pyexport_market_model(m, mm);
    pyexport_client_server(m);
    em_handle::destroy = py_destroy;
  }
}

SHYFT_PYTHON_MODULE(energy_market, m) {
  shyft::energy_market::pyexport(m);
}
