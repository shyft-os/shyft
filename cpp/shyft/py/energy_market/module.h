#pragma once
#include <iterator>
#include <string>
#include <stdexcept>
#include <string>
#include <string_view>

#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market {

  /** py_object_ext is for extending c++ classes the model with a python object
   *
   * @details
   * The base object of the energy-market model contains an optional 'em-handle'
   * ref to the 'id_base' object.
   * This is the interface that lets the user assign/read this
   * property.
   *
   * @tparam T like reservoir, unit etc.
   *
   */
  template <class T>
  struct py_object_ext {
    static py::object get_obj(T const & o) {
      if (auto pyo = static_cast<py::object*>(o.h.obj))
        return *pyo;
      return py::none();
    }

    static void set_obj(T& o, py::object& h) {
      auto pyo = static_cast<py::object*>(o.h.obj);
      if (pyo) {
        *pyo = h;
      } else {
        pyo = new py::object(h);
        o.h.obj = static_cast<void*>(pyo);
      }
    }
  };

  template <class T, class... O>
  auto expose_attribute_maps(py::class_<T, O...> c) {
    c.def_readwrite("ts", &T::tsm);
    c.def_readwrite("custom", &T::custom);
    c.def(
      "get_tsm_object",
      +[](T &self, std::string key) {
        auto it = self.tsm.find(key);
        if (it == self.tsm.end())
          throw std::runtime_error("Key does not exist");
        return a_wrap<apoint_ts>(
          [&self](sbi_t &so, int levels, int template_levels, std::string_view) {
            if (levels)
              self.generate_url(so, levels - 1, template_levels > 0 ? template_levels - 1 : template_levels);
          },
          std::string("ts.") + key,
          it->second);
      },
      doc.intro("Get a specific extra time series for this object.")
        .details("The returned time series is wrapped in an object which exposes method for retrieving url etc.")
        .parameters()
        .parameter("key", "str", "The key in the tsm of the time series to get.")
        .raises()
        .raise("runtime_error", "If specified key does not exist.")(),
      py::arg("key"));
    return c;
  }

  template <typename T, typename... O>
  auto expose_type(auto &scope, auto name, auto doc, auto... args) {
    auto c = py::class_<T, std::shared_ptr<T>, O...>(scope, name, doc, py::dynamic_attr(), args...);
    c.def(py::self == py::self).def(py::self != py::self);
    if constexpr(requires { pyapi::expose_format(c); })
      pyapi::expose_format(c); 
    return c;
  }

  template <typename T, typename... O>
  auto expose_system_type(auto &scope, auto name, auto tdoc, auto... args) {
    auto c = expose_type<T, O...>(scope, name, tdoc, args...);
    c
      .def_property(
        "id",
        [](T &t) -> decltype(auto) { return (t.id); },
        [](T &t, int i){ t.id = i; })
      .def_property(
        "name",
        [](T &t) -> decltype(auto) { return (t.name); },
        [](T &t, const std::string &i){ t.name = i; })
      .def_property(
        "json",
        [](T &t) -> decltype(auto) { return (t.json); },
        [](T &t, const std::string &i){ t.json = i; })
      .def_property(
        "obj",
        &py_object_ext<T>::get_obj,
        &py_object_ext<T>::set_obj,
        doc.intro("object: a python object")())
      .def("__hash__", [](const T &t){
        return std::hash<std::int64_t>{}(t.id);
      })
    ;
    expose_attribute_maps(c);
    return c;
  }
  template <typename T, typename... O>
  auto expose_system_init(py::class_<T, O...> c) {
    c.def(
      py::init<int, std::string const &, std::string const &>(),
      py::arg("id"),
      py::arg("name"),
      py::arg("json") = "");
  }


  template <typename T, typename... O>
  auto expose_system_component_type(auto &scope, auto name, auto tdoc, auto... args) {
    auto c = expose_system_type<T, O...>(scope, name, tdoc, args...);
    c.def_property_readonly(
      "hps",&T::hps_, doc.intro("HydroPowerSystem: returns the hydro power system this component is a part of")());
    return c;
  }

  template <typename T, typename... O>
  auto expose_system_component_init(py::class_<T, O...> c) {
    c.def(
      py::
        init<int, std::string const &, std::string const &, std::shared_ptr<hydro_power::hydro_power_system> const &>(),
      py::arg("id"),
      py::arg("name"),
      py::arg("json"),
      py::arg("hps"));
  }

  template <typename T, typename... O>
  auto expose_hydro_component_type(auto &scope, auto name, auto doc, auto... args) {
    auto t = expose_type<T, hydro_power::hydro_component, O...>(scope, name, doc, args...);
    expose_system_component_init(t);
    t.def("__hash__", [](const T &t){
      return std::hash<std::int64_t>{}(t.id);
    });
    return t;
  }

  namespace hydro_power {
    void pyexport_xy_point_curves(py::module_ &);
    void pyexport(py::module_ &);
  }

  void pyexport_time_series_support(py::module_ &);
  void pyexport_client_server(py::module_ &);

}

PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::id_base::tsm));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::id_base::custom));
PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::energy_market::hydro_power::hydro_component>>);
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::hydro_power::waterway::gates));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::hydro_power::hydro_power_system::reservoirs));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::hydro_power::hydro_power_system::units));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::hydro_power::hydro_power_system::waterways));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::hydro_power::hydro_power_system::catchments));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::hydro_power::hydro_power_system::power_plants));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::market::model_area::power_modules));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::market::model::area));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::market::model::power_lines));
PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::energy_market::hydro_power::hydro_power_system>>);
