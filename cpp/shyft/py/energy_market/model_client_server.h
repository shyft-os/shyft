#pragma once

#include <csignal>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>

#include <shyft/py/doc_builder.h>
#include <shyft/srv/client.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/srv/model_info.h>
#include <shyft/srv/server.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/energy_market/srv/request_handler.h>

namespace shyft::energy_market {

  using shyft::core::utcperiod;
  using shyft::srv::model_info;
  using shyft::srv::client;
  using shyft::srv::server;
  using shyft::web_api::energy_market::srv::request_handler;
  using namespace shyft::pyapi;

  /**
   * @brief A  client for model type M suitable for python exposure
   *
   * This class takes care of  python gil and mutex, ensuring that any attempt using
   * multiple python threads will be serialized.
   * gil is released while the call is in progress.
   *
   * Using this template saves us the repeating work for similar model-repositories
   *
   * @tparam Client a client type, should be shyft::srv::client<M> or derived from it.
   *
   */
  template <class Client>
  struct py_client {
    using M_t = typename Client::M_t;
    std::mutex mx; ///< to enforce just one thread active on this client object at a time
    Client impl;

    py_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms)
      : impl{host_port, timeout_ms, operation_timeout_ms} {
    }

    ~py_client() = default;

    py_client() = delete;
    py_client(py_client const &) = delete;
    py_client(py_client &&) = delete;
    py_client &operator=(py_client const &o) = delete;

    std::string get_host_port() {
      return impl.c.host_port;
    }

    int get_timeout_ms() {
      return impl.c.timeout_ms;
    }

    int get_operation_timeout_ms() {
      return impl.c.operation_timeout_ms;
    }

    void set_operation_timeout_ms(int operation_timeout_ms) {
      impl.c.operation_timeout_ms = operation_timeout_ms;
    }

    [[nodiscard]] bool is_open() const {
      return impl.c.is_open;
    }

    [[nodiscard]] size_t get_reconnect_count() const {
      return impl.c.reconnect_count;
    }

    void close(int timeout_ms = 1000) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.close(timeout_ms);
    }

    void reopen(int timeout_ms = 1000) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.reopen(timeout_ms);
    }

    [[nodiscard]] std::vector<model_info> get_model_infos(std::vector<int64_t> const &mids, utcperiod per) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_model_infos(mids, per);
    }

    [[nodiscard]] int64_t store_model(std::shared_ptr<M_t> const &m, model_info const &mi) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.store_model(m, mi);
    }

    [[nodiscard]] std::shared_ptr<M_t> read_model(int64_t mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.read_model(mid);
    }

    [[nodiscard]] std::vector<std::shared_ptr<M_t>> read_models(std::vector<int64_t> mids) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.read_models(mids);
    }

    [[nodiscard]] int64_t remove_model(int64_t mid) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.remove_model(mid);
    }

    [[nodiscard]] bool update_model_info(int64_t mid, model_info const &mi) {
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.update_model_info(mid, mi);
    }

    void close_conn() { // weird, close is not a name we can use here
      scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.close();
    }
  };

  /**
   * @brief The server side component for a model repository
   *
   * @details
   * This class wraps/provides the server-side for the model-repository of type DB<M>
   * suitable for exposure to python.
   *
   * @tparam Server, the server type. Should be a specialization of shyft::srv::server<DB>,
   *   or derived from it.
   *
   */
  template <class Server>
  struct py_server {
    using M_t = typename Server::M;
    Server impl;

    py_server(std::string const &root_dir, shyft::srv::fast_server_iostream_config cfg)
      : impl{root_dir, std::move(cfg)} {
    }

    void set_listening_port(int port) {
      impl.set_listening_port(port);
    }

    [[nodiscard]] int get_listening_port() {
      return impl.get_listening_port();
    }

    void set_max_connections(int n) {
      impl.set_max_connections(static_cast<size_t>(n));
    }

    [[nodiscard]] int get_max_connections() {
      return static_cast<int>(impl.get_max_connections());
    }

    void set_listening_ip(std::string const &ip) {
      impl.set_listening_ip(ip);
    }

    [[nodiscard]] std::string get_listening_ip() {
      return impl.get_listening_ip();
    }

    [[nodiscard]] std::size_t get_stale_close_count() const {
      return impl.get_stale_close_count();
    }

    [[nodiscard]] int start_server() {
      return impl.start_server();
    }

    void stop_server(int timeout_ms) {
      impl.set_graceful_close_timeout(timeout_ms);
      impl.clear();
    }

    [[nodiscard]] bool is_running() {
      return impl.is_running();
    }

    ~py_server() {
      stop_server(1000);
    }

    //-- direct db model operations makes more usage scenarios possible, prior to starting server.
    //
    [[nodiscard]] auto get_model_infos(std::vector<int64_t> const &mids, utcperiod per) {
      scoped_gil_release gil;
      if (per.valid())
        return impl.db.get_model_infos(mids, per);
      return impl.db.get_model_infos(mids);
    }

    [[nodiscard]] auto read_model(std::int64_t mid) {
      scoped_gil_release gil;
      return impl.db.read_model(mid);
    }

    [[nodiscard]] std::vector<std::shared_ptr<M_t>> read_models(std::vector<int64_t> const& mids) {
      scoped_gil_release gil;
      if (mids.empty())
        return std::vector<std::shared_ptr<M_t>>{};
      std::vector<std::shared_ptr<M_t>> result;
      for (auto &mid : mids) {
        auto m = impl.db.read_model(mid);
        if (m)
          result.push_back(std::move(m));
        else
          throw std::runtime_error(fmt::format("Failed to read model with id {}.", mid));
      }
      return result;
    }

    [[nodiscard]] int64_t store_model(std::shared_ptr<M_t> const &m, model_info const &mi) {
      scoped_gil_release gil;
      return impl.db.store_model(m, mi);
    }

    [[nodiscard]] bool update_model_info(int64_t mid, model_info const &mi) {
      scoped_gil_release gil;
      return impl.db.update_model_info(mid, mi);
    }

    [[nodiscard]] auto read_model_blob(std::int64_t mid) {
      scoped_gil_release gil;
      auto blob = impl.db.read_model_blob(mid);
      return std::vector<char>{blob.begin(), blob.end()};
    }

    [[nodiscard]] int64_t remove_model(int64_t mid) {
      scoped_gil_release gil;
      return impl.db.remove_model(mid);
    }

    //-- get rid of stuff that would not work
    py_server() = delete;
    py_server(py_server const &) = delete;
    py_server(py_server &&) = delete;
    py_server &operator=(py_server const &) = delete;
    py_server &operator=(py_server &&) = delete;
  };

  template <class Server, class RequestHandler = request_handler<Server>>
  struct py_server_with_web_api : public py_server<Server> {
    using super = py_server<Server>;
    using DB_t = typename Server::DB_t;
    RequestHandler bg_server;
    std::future<int> web_srv;

    py_server_with_web_api(std::string const &root_dir, shyft::srv::fast_server_iostream_config cfg)
      : super(root_dir, std::move(cfg)) {
      bg_server.srv = &(this->impl);
    }

    ~py_server_with_web_api() {
      stop_web_api();
    }

    [[nodiscard]] int
      start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads, bool tls_only) {
      scoped_gil_release gil;
      if (!web_srv.valid()) {
        web_srv = std::async(
          std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads, tls_only]() -> int {
            return shyft::web_api::run_web_server(
              bg_server,
              host_ip,
              static_cast<unsigned short>(port),
              std::make_shared<std::string>(doc_root),
              fg_threads,
              bg_threads,
              tls_only);
          });
      }
      return static_cast<int>(bg_server.wait_for_real_port());
    }

    void stop_web_api() {
      scoped_gil_release gil;
      bg_server.running = false;
      if (web_srv.valid()) {
        (void) web_srv.get();
      }
    }

    [[nodiscard]] bool auth_needed() const {
      return bg_server.auth.needed();
    }

    [[nodiscard]] std::vector<std::string> auth_tokens() const {
      return bg_server.auth.tokens();
    }

    void add_auth_tokens(std::vector<std::string> const &tokens) {
      bg_server.auth.add(tokens);
    }

    void remove_auth_tokens(std::vector<std::string> const &tokens) {
      bg_server.auth.remove(tokens);
    }
  };

  /**
   * @brief Expose to python the client side api for a model-repository of type M
   *
   * @tparam PyClient the client type. Should be an instantiation of shyft::py::energy_market::py_client<Client>,
   *  or derived from it.
   *
   *  @return an instance of boost::python::class_ that exposes PyClient to Python with interface for base class
   *      functionality.
   */
  template <class PyClient>
  auto export_client(auto &scope, char const *name, char const *doc_str) {
    using cm = PyClient;

    py::class_<cm> py_wrapper(scope, name, doc_str, py::dynamic_attr());
    py_wrapper
      .def(
        py::init<std::string const &, int, int>(),
        "Creates a python client that can communicate with the corresponding server",
        py::arg("host_port"),
        py::arg("timeout_ms"),
        py::arg("operation_timeout_ms") = 0)
      .def_property_readonly("host_port", &cm::get_host_port, "str: Endpoint network address of the remote server.")
      .def_property_readonly(
        "timeout_ms", &cm::get_timeout_ms, "int: Timout for remote server operations, in number milliseconds.")
      .def_property_readonly("is_open", &cm::is_open, "bool: If the connection to the remote server is (still) open.")
      .def_property_readonly(
        "reconnect_count",
        &cm::get_reconnect_count,
        "int: Number of reconnects to the remote server that have been performed.")
      .def_property(
        "operation_timeout_ms",
        &cm::get_operation_timeout_ms,
        &cm::set_operation_timeout_ms,
        "int: Operation timeout for remote server operations, in number milliseconds.")
      .def("close", &cm::close_conn, doc.intro("Close the connection. It will automatically reopen if needed.")())
      .def(
        "get_model_infos",
        &cm::get_model_infos,
        doc.intro("returns all or selected model-info objects based on model-identifiers(mids)")
          .parameters()
          .parameter("mids", "IntVector", "empty = all, or a list of known exisiting model-identifiers")
          .parameter("created_in", "UtcPeriod", "For which period you are interested in model-infos.")
          .returns("model_infos", "ModelInfoVector", "Strongly typed list of ModelInfo")(),
        py::arg("mids"),
        py::arg("created_in") = utcperiod())
      .def(
        "store_model",
        &cm::store_model,
        doc.intro("Store the model to backend, if m.id==0 then a new unique model-info is created and used")
          .parameters()
          .parameter("m", "Model", "The model to store")
          .parameter("mi", "ModelInfo", "The model-info to store for the model")
          .returns("mid", "int", "model-identifier for the stored model and model-info")(),
        py::arg("m"),
        py::arg("mi"))
      .def(
        "read_model",
        &cm::read_model,
        doc.intro("Read and return the model for specified model-identifier (mid)")
          .parameters()
          .parameter("mid", "int", "the model-identifer for the wanted model")
          .returns("m", "Model", "The resulting model from the server")(),
        py::arg("mid"))
      .def(
        "read_models",
        &cm::read_models,
        doc.intro("Read and return the model for specified model-identifier (mid)")
          .parameters()
          .parameter("mids", "list[int]", "A strongly typed list of ints, the model-identifers for the wanted models")
          .returns("m", "Model", "The resulting model from the server")(),
        py::arg("mids"))
      .def(
        "remove_model",
        &cm::remove_model,
        doc.intro("Remove the specified model bymodel-identifier (mid)")
          .parameters()
          .parameter("mid", "int", "the model-identifer for the wanted model")
          .returns("ec", "int", "0 or error-code?")(),
        py::arg("mid"))
      .def(
        "update_model_info",
        &cm::update_model_info,
        doc.intro("Update the model-info for specified model-identifier(mid)")
          .parameters()
          .parameter("mid", "int", "model-identifer")
          .parameter("mi", "ModelInfo", "The new updated model-info")
          .returns("ok", "bool", "true if success")(),
        py::arg("mid"),
        py::arg("mi"));
    return py_wrapper;
  }

  /**
   * @brief a function to expose a server using a DB for model DB::model_t
   *
   * @details
   * This class simply expose the methods of the above py_server that takes
   * a backend storage service 'DB', for  model type M=DB::model_t
   * @tparam DB template for backend-store, same requirements as for py_server<M>
   * @tparam PyServer python server wrapper type. Should be specialization of py_server<Server>,
   *  or derived from it.
   */
  template <class PyServer>
  auto export_server(auto &scope, char const *name, char const *doc_str) {
    using srv = PyServer;
    py::class_<srv> srv_wrapper(scope, name, doc_str, py::dynamic_attr());
    srv_wrapper
      .def(
        py::init<std::string const &, shyft::srv::fast_server_iostream_config const &>(),
        doc.intro("Creates a server object that serves models from root_dir.")
          .intro("The root_dir will be create if it does not exists.")
          .parameters()
          .parameter("root_dir", "str", "Path to the root-directory that keeps/will keep the model-files")
          .parameter("config", "ServerConfig", "Configuration of the server")(),
        py::arg("root_dir"),
        py::arg("config") = shyft::srv::fast_server_iostream_config{})
      .def(
        "set_listening_port",
        &srv::set_listening_port,
        doc.intro("set the listening port for the service")
          .parameters()
          .parameter("port_no", "int", "a valid and available tcp-ip port number to listen on.")
          .paramcont("typically it could be 20000 (avoid using official reserved numbers)")
          .returns("nothing", "None", "")(),
        py::arg("port_no"))
      .def(
        "set_listening_ip",
        &srv::set_listening_ip,
        doc.intro("set the listening port for the service")
          .parameters()
          .parameter("ip", "str", "ip or host-name to start listening on")
          .returns("nothing", "None", "")(),
        py::arg("ip"))
      .def(
        "start_server",
        &srv::start_server,
        doc.intro("start server listening in background, and processing messages")
          .see_also("set_listening_port(port_no),is_running")
          .returns(
            "port_no",
            "in",
            "the port used for listening operations, either the value as by set_listening_port, or if it was "
            "unspecified, a new available port")())
      .def(
        "set_max_connections",
        &srv::set_max_connections,
        doc.intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
          .parameters()
          .parameter("max_connect", "int", "maximum number of connections before denying more connections")
          .see_also("get_max_connections()")(),
        py::arg("max_connect"))
      .def(
        "get_max_connections",
        &srv::get_max_connections,
        doc.intro("returns the maximum number of connections to be served concurrently")())
      .def(
        "stop_server",
        &srv::stop_server,
        doc.intro("stop serving connections, gracefully.").see_also("start_server()")(),
        py::arg("timeout") = 1000)
      .def(
        "is_running",
        &srv::is_running,
        doc.intro("true if server is listening and running").see_also("start_server()")())
      .def(
        "get_listening_port",
        &srv::get_listening_port,
        "returns the port number it's listening at for serving incoming request")
      .def_property_readonly(
        "stale_connection_close_count",
        &srv::get_stale_close_count,
        doc.intro("int: returns count of connection closed due to stale/no communication activity")())
      .def(
        "get_model_infos",
        &srv::get_model_infos,
        doc.intro("returns all or selected model-info objects based on model-identifiers(mids)")
          .parameters()
          .parameter("mids", "IntVector", "empty = all, or a list of known exisiting model-identifiers")
          .parameter("created_in", "UtcPeriod", "For which period you are interested in model-infos.")
          .returns("model_infos", "ModelInfoVector", "Strongly typed list of ModelInfo")(),
        py::arg("mids"),
        py::arg("created_in") = utcperiod())
      .def(
        "read_model",
        &srv::read_model,
        doc.intro("Read and return the model for specified model-identifier (mid)")
          .parameters()
          .parameter("mid", "int", "the model-identifer for the wanted model")
          .returns("m", "Model", "The resulting model from the server")(),
        py::arg("mid"))
      .def(
        "read_model_blob",
        &srv::read_model_blob,
        doc.intro("Read and return the model blob for specified model-identifier (mid)")
          .parameters()
          .parameter("mid", "int", "the model-identifer for the wanted model")
          .returns("m", "ByteVector", "The resulting blob model from the server")(),
        py::arg("mid"))
      .def(
        "read_models",
        &srv::read_models,
        doc.intro("Read and return the model for specified model-identifier (mid)")
          .parameters()
          .parameter("mids", "list[int]", "A strongly typed list of ints, the model-identifers for the wanted models")
          .returns("m", "Model", "The resulting model from the server")(),
        py::arg("mids"))
      .def(
        "store_model",
        &srv::store_model,
        doc.intro("Store the model to backend, if m.id==0 then a new unique model-info is created and used")
          .parameters()
          .parameter("m", "Model", "The model to store")
          .parameter("mi", "ModelInfo", "The model-info to store for the model")
          .returns("mid", "int", "model-identifier for the stored model and model-info")(),
        py::arg("m"),
        py::arg("mi"))
      .def(
        "remove_model",
        &srv::remove_model,
        doc.intro("Remove the specified model bymodel-identifier (mid)")
          .parameters()
          .parameter("mid", "int", "the model-identifer for the wanted model")
          .returns("ec", "int", "0 or error-code?")(),
        py::arg("mid"))
      .def(
        "update_model_info",
        &srv::update_model_info,
        doc.intro("Update the model-info for specified model-identifier(mid)")
          .parameters()
          .parameter("mid", "int", "model-identifer")
          .parameter("mi", "ModelInfo", "The new updated model-info")
          .returns("ok", "bool", "true if success")(),
        py::arg("mid"),
        py::arg("mi"));
    ;
    return srv_wrapper;
  }

  template <class PyServer>
  auto export_server_with_web_api(auto &scope, char const *name, char const *doc_str) {
    using srv = PyServer;
    // The base needs to be exposed as well:
    auto srv_wrapper = export_server<PyServer>(scope, name, doc_str);

    srv_wrapper
      .def(
        "start_web_api",
        &srv::start_web_api,
        doc.intro("Start a web API for communicating with server")
          .parameters()
          .parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only, &c.")
          .parameter(
            "port", "int", "port number to serve the web API on. 0 for auto-port, otherwise ensure it's available")
          .parameter("doc_root", "str", "directory form which we will serve http/https documents.")
          .parameter("fg_threads", "int", "number of web API foreground threads, typically 1-4 depending on load.")
          .parameter("bg_threads", "int", "number of long running background thread workers to serve requests &c.")
          .parameter("tls_only", "bool", "default false, set to true to enforce tls sessions only.")
          .returns(
            "port", "int", "real port number, if `port` arg is 0, then auto-allocated, otherwise the passed arg")(),
        py::arg("host_ip"),
        py::arg("port"),
        py::arg("doc_root"),
        py::arg("fg_threads") = 2,
        py::arg("bg_threads") = 4,
        py::arg("tls_only") = false)
      .def("stop_web_api", &srv::stop_web_api, doc.intro("Stops any ongoing web API service.")())
      .def_property_readonly(
        "auth_needed",
        &srv::auth_needed,
        doc.intro(
          "bool: returns true if the server is setup with auth-tokens, requires web-api clients to pass a "
          "valid token")())
      .def("auth_tokens", &srv::auth_tokens, doc.intro("returns the registered authentication tokens.")())
      .def(
        "add_auth_tokens",
        &srv::add_auth_tokens,
        doc.intro("Adds auth tokens, and activate authentication.")
          .intro("The tokens is compared exactly to the autorization token passed in the request.")
          .intro(
            "Authorization should onlye be used for the https/wss, unless other measures(vpn/ssh tunnels etc.) "
            "are used to protect auth tokens on the wire")
          .parameters()
          .parameter(
            "tokens", "", "list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")(),
        py::arg("tokens"))
      .def(
        "remove_auth_tokens",
        &srv::remove_auth_tokens,
        doc
          .intro(
            "removes auth tokens, if it matches all available tokens, then deactivate auth requirement for "
            "clients")
          .parameters()
          .parameter(
            "tokens", "", "list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")(),
        py::arg("tokens"));
    return srv_wrapper;
  }

}
