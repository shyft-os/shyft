#include <cstdint>
#include <iterator>
#include <string>

#include <boost/describe/enumerators.hpp>
#include <boost/mp11.hpp>

#include <shyft/config.h>
#include <shyft/core/reflection.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/version.h>

#if defined(SHYFT_WITH_STM)

#include <shyft/py/energy_market/ui/ui_generators.h>

namespace shyft::energy_market::ui { // Expose Python api for generating ui json configuration from Qt Widgets

  using std::size_t;
  using std::intptr_t;
  using std::string;
  using std::to_string;
  using namespace std::string_literals;
  using std::runtime_error;

  extern void pyexport_client_server(py::module_ &);

  void pyexport(py::module_ &m) {
    m.attr("qt_version") = string{QT_VERSION_STR};

    py::enum_<web_api::generator::ItemDataProperty>(m, "ItemDataProperty")
      .value("DataX", web_api::generator::ItemDataProperty::DataX)
      .value("DataY", web_api::generator::ItemDataProperty::DataY)
      .value("Decimals", web_api::generator::ItemDataProperty::Decimals)
      .value("ScaleX", web_api::generator::ItemDataProperty::ScaleX)
      .value("ScaleY", web_api::generator::ItemDataProperty::ScaleY)
      .value("ValidationX", web_api::generator::ItemDataProperty::ValidationX)
      .value("ValidationY", web_api::generator::ItemDataProperty::ValidationY)
      .value("Tags", web_api::generator::ItemDataProperty::Tags)
      .export_values();
    py::enum_<web_api::generator::LineStyle>(m, "LineStyle")
      .value("DashLine", web_api::generator::LineStyle::DashLine)
      .value("DotLine", web_api::generator::LineStyle::DotLine)
      .value("NoPen", web_api::generator::LineStyle::NoPen)
      .value("SolidLine", web_api::generator::LineStyle::SolidLine)
      .value("CustomDashLine", web_api::generator::LineStyle::CustomDashLine)
      .value("DashDotLine", web_api::generator::LineStyle::DashDotLine)
      .value("DashDotDotLine", web_api::generator::LineStyle::DashDotDotLine)
      .export_values();
    py::enum_<web_api::generator::Alignment>(m, "Aligment")
      .value("AlignLeft", web_api::generator::Alignment::AlignLeft)
      .value("AlignBottom", web_api::generator::Alignment::AlignBottom)
      .value("AlignCenter", web_api::generator::Alignment::AlignCenter)
      .value("AlignJustify", web_api::generator::Alignment::AlignJustify)
      .value("AlignRight", web_api::generator::Alignment::AlignRight)
      .value("AlignTop", web_api::generator::Alignment::AlignTop)
      .value("AlignHCenter", web_api::generator::Alignment::AlignHCenter)
      .value("AlignVCenter", web_api::generator::Alignment::AlignVCenter)
      .export_values();
    py::enum_<web_api::generator::ItemFlags>(m, "ItemFlags")
      .value("NoItemFlags", web_api::generator::ItemFlags::NoItemFlags)
      .value("ItemIsSelectable", web_api::generator::ItemFlags::ItemIsSelectable)
      .value("ItemIsEditable", web_api::generator::ItemFlags::ItemIsEditable)
      .value("ItemIsDragEnabled", web_api::generator::ItemFlags::ItemIsDragEnabled)
      .value("ItemIsDropEnabled", web_api::generator::ItemFlags::ItemIsDropEnabled)
      .value("ItemIsUserCheckable", web_api::generator::ItemFlags::ItemIsUserCheckable)
      .value("ItemIsEnabled", web_api::generator::ItemFlags::ItemIsEnabled)
      .value("ItemIsAutoTristate", web_api::generator::ItemFlags::ItemIsAutoTristate)
      .value("ItemNeverHasChildren", web_api::generator::ItemFlags::ItemNeverHasChildren)
      .value("ItemIsUserTristate", web_api::generator::ItemFlags::ItemIsUserTristate)
      .export_values();
    py::enum_<web_api::generator::Orientation>(m, "Orientation")
      .value("Horizontal", web_api::generator::Orientation::Horizontal)
      .value("Vertical", web_api::generator::Orientation::Vertical)
      .export_values();
    py::enum_<web_api::generator::ScrollBarPolicy>(m, "ScrollBarPolicy")
      .value("ScrollBarAsNeeded", web_api::generator::ScrollBarPolicy::ScrollBarAsNeeded)
      .value("ScrollBarAlwaysOff", web_api::generator::ScrollBarPolicy::ScrollBarAlwaysOff)
      .value("ScrollBarAlwaysOn", web_api::generator::ScrollBarPolicy::ScrollBarAlwaysOn)
      .export_values();
    py::enum_<web_api::generator::standard_buttons>(m, "StandardButtons")
      .value("Ok", web_api::generator::standard_buttons::Ok)
      .value("Open", web_api::generator::standard_buttons::Open)
      .value("Save", web_api::generator::standard_buttons::Save)
      .value("Cancel", web_api::generator::standard_buttons::Cancel)
      .value("No", web_api::generator::standard_buttons::No)
      .value("NoButton", web_api::generator::standard_buttons::NoButton)
      .value("Close", web_api::generator::standard_buttons::Close)
      .value("SaveAll", web_api::generator::standard_buttons::SaveAll)
      .value("Yes", web_api::generator::standard_buttons::Yes)
      .value("YesToAll", web_api::generator::standard_buttons::YesToAll)
      .value("NoToAll", web_api::generator::standard_buttons::NoToAll)
      .value("Abort", web_api::generator::standard_buttons::Abort)
      .value("Ignore", web_api::generator::standard_buttons::Ignore)
      .value("Retry", web_api::generator::standard_buttons::Retry)
      .value("Discard", web_api::generator::standard_buttons::Discard)
      .value("Help", web_api::generator::standard_buttons::Help)
      .value("Apply", web_api::generator::standard_buttons::Apply)
      .value("Reset", web_api::generator::standard_buttons::Reset)
      .value("RestoreDefaults", web_api::generator::standard_buttons::RestoreDefaults)
      .export_values();

    py::class_<web_api::generator::color>(m, "Color")
      .def(py::init())
      .def(py::init(static_cast<web_api::generator::color (*)(std::string const &)>(&web_api::generator::make_color)))
      .def(py::init(static_cast<web_api::generator::color (*)(int, int, int, int)>(&web_api::generator::make_color)))
      .def(py::self == py::self);
    py::class_<web_api::generator::pen>(m, "Pen")
      .def(py::init())
      .def("setWidth", &web_api::generator::set_width)
      .def("setStyle", &web_api::generator::set_style)
      .def("setColor", &web_api::generator::set_color_pen)
      .def("style", &web_api::generator::get_style)
      .def("width", &web_api::generator::get_width);
    py::class_<web_api::generator::point_f>(m, "PointF")
      .def(py::init<double, double>())
      .def_readonly("y", &web_api::generator::point_f::y);

    py::class_<web_api::generator::qt_objects_interface> app_obj(m, "Application");

    app_obj.def(py::init());
    // wait with abb_obj until classes are generated:
    auto base_proxy_types = enums<web_api::generator::ui_tag>(
      [&](auto... e){
        return std::make_tuple(
          py::class_<web_api::generator::base_proxy<e.value>>(
            m, pyapi::pep8_typename(fmt::format("{}", e.name)).c_str())...);
      });

    auto id_types = enums<web_api::generator::ui_tag>(
      [&](auto... e){
        return std::make_tuple(
          py::class_<web_api::generator::id<e.value>>(
            m, pyapi::pep8_typename(fmt::format("{}Id", e.name)).c_str())...);
      });

    for_each_enum<web_api::generator::ui_tag>([&](auto e) {
      auto &obj = std::get<etoi(e.value)>(base_proxy_types);
      auto &id_class = std::get<etoi(e.value)>(id_types);

      auto obj_str = [=](web_api::generator::base_proxy<e.value> const &o) {
        return fmt::format("{}()", pyapi::pep8_typename(fmt::format("{}", e.name)));
      };
      obj.def("__str__", obj_str);
      obj.def("__repr__", obj_str);
      pyapi::expose_members(id_class);

      if constexpr (
        requires(web_api::generator::qt_objects_interface &i) { web_api::generator::make_base<e.value>(i); }) {
        auto dummy = pyapi::pep8_typename(fmt::format("{}", e.name), true);
        app_obj.def(dummy.c_str(), &web_api::generator::make_base<e.value>);
      }
      obj.def_readonly("id", &web_api::generator::base_proxy<e.value>::i);
      if constexpr (std::ranges::contains(web_api::generator::qt_objects_array, e.value)) {
        obj.def("property", &web_api::generator::get_property<e.value>)
          .def("setProperty", &web_api::generator::set_property<e.value>)
          .def("getChildren", &web_api::generator::get_children<e.value>)
          .def("setObjectName", &web_api::generator::set_object_name<e.value>)
          .def("objectName", &web_api::generator::object_name<e.value>);
      }
      if constexpr (std::ranges::contains(web_api::generator::qt_abstract_series_array, e.value)) {
        obj.def("setVisible", &web_api::generator::set_visible<e.value>)
          .def("name", &web_api::generator::get_name<e.value>)
          .def("setName", &web_api::generator::set_name<e.value>)
          .def("attachAxis", &web_api::generator::attach_axis<e.value>)
          .def("attachedAxes", &web_api::generator::get_attached_axes<e.value>);
        if constexpr (std::ranges::contains(web_api::generator::qt_xy_series_array, e.value)) {
          obj.def("setPen", &web_api::generator::set_pen<e.value>)
            .def("replace", &web_api::generator::replace<e.value>)
            .def("pen", &web_api::generator::get_pen<e.value>)
            .def("color", &web_api::generator::get_color<e.value>)
            .def(
              "append",
              static_cast<void (*)(web_api::generator::base_proxy<e.value> &, web_api::generator::point_f)>(
                &web_api::generator::append_x_y_series<e.value>))
            .def(
              "append",
              static_cast<void (*)(web_api::generator::base_proxy<e.value> &, double, double)>(
                &web_api::generator::append_x_y_series<e.value>))
            .def("points", &web_api::generator::get_points<e.value>);
        }
        if constexpr (std::ranges::contains(web_api::generator::qt_bar_series_array, e.value)) {
          obj.def("setBarWidth", &web_api::generator::set_bar_width<e.value>)
            .def("append", &web_api::generator::append_bar_set_series<e.value>)
            .def("barSets", &web_api::generator::get_bar_set<e.value>);
        }
      }

      if constexpr (std::ranges::contains(web_api::generator::qt_abstract_axes_array, e.value)) {
        obj.def("setGridLineColor", &web_api::generator::set_grid_line_color<e.value>)
          .def("setVisible", &web_api::generator::set_visible<e.value>)
          .def("setTitleText", &web_api::generator::set_title_text<e.value>);
        if constexpr (e.value == web_api::generator::ui_tag::value_axis) {
          obj.def("setRange", &web_api::generator::set_range)
            .def("setMin", &web_api::generator::set_min_v)
            .def("setMax", &web_api::generator::set_max_v)
            .def("setTickCount", &web_api::generator::set_tick_count<e.value>)
            .def("setLabelFormat", &web_api::generator::set_label_format)
            .def("setFormat", &web_api::generator::set_label_format);
        }
        if constexpr (e.value == web_api::generator::ui_tag::bar_category_axis) {
          obj.def("setCategories", &web_api::generator::set_categories).def("append", &web_api::generator::append_b);
        }
        if constexpr (e.value == web_api::generator::ui_tag::date_time_axis) {
          obj.def("setTickCount", &web_api::generator::set_tick_count<e.value>)
            .def("setFormat", &web_api::generator::set_format)
            .def("setMin", &web_api::generator::set_min_dt)
            .def("setMax", &web_api::generator::set_max_dt);
        }
      }

      if constexpr (std::ranges::contains(web_api::generator::qt_widget_array, e.value)) {
        obj.def("setLayout", &web_api::generator::set_layout<e.value>)
          .def("setAccessibleName", &web_api::generator::set_accessible_name<e.value>)
          .def("setToolTip", &web_api::generator::set_tool_tip<e.value>)
          .def("setStatusTip", &web_api::generator::set_status_tip<e.value>)
          .def("setWhatsThis", &web_api::generator::set_whats_this<e.value>)
          .def("setWindowRole", &web_api::generator::set_window_role<e.value>)
          .def("setWindowTitle", &web_api::generator::set_window_title<e.value>)
          .def("setAccessibleDescription", &web_api::generator::set_accessible_description<e.value>)
          .def("setStyleSheet", &web_api::generator::set_style_sheet<e.value>)
          .def("setMinimumHeight", &web_api::generator::set_minimum_height<e.value>)
          .def("toolTip", &web_api::generator::get_tool_tip<e.value>);
        if constexpr (e.value == web_api::generator::ui_tag::table_widget) {
          obj.def("setColumnCount", &web_api::generator::set_column_count)
            .def("setRowCount", &web_api::generator::set_row_count)
            .def("setVerticalHeaderItem", &web_api::generator::set_vertical_header_item)
            .def("setHorizontalHeaderLabels", &web_api::generator::set_horizontal_labels)
            .def("rowCount", &web_api::generator::row_count)
            .def("verticalHeaderItem", &web_api::generator::vertical_header_item)
            .def("columnCount", &web_api::generator::get_column_count)
            .def("setItem", &web_api::generator::set_item)
            .def("setVerticalHeaderVisible", &web_api::generator::set_vertical_header_visible)
            .def("setShowGrid", &web_api::generator::set_show_grid);
        }
        if constexpr (e.value == web_api::generator::ui_tag::scroll_area) {
          obj.def("setWidgetResizable", &web_api::generator::set_widget_resizeble)
            .def("setWidget", &web_api::generator::set_widget)
            .def("setVerticalScrollBarPolicy", &web_api::generator::set_vertical_scroll_bar_policy)
            .def("widget", &web_api::generator::get_widget<e.value>);
        }
        if constexpr (e.value == web_api::generator::ui_tag::push_button) {
          obj.def("addAction", &web_api::generator::add_action<e.value>)
            .def("setText", &web_api::generator::set_text<e.value>)
            .def("isVisible", &web_api::generator::is_visible<e.value>)
            .def("setMenu", &web_api::generator::set_menu<e.value>)
            .def("actions", &web_api::generator::get_actions)
            .def("setFlat", &web_api::generator::set_flat);
        }
        if constexpr (e.value == web_api::generator::ui_tag::menu) {
          obj.def("addAction", &web_api::generator::add_action<e.value>);
        }
        if constexpr (e.value == web_api::generator::ui_tag::dialog_button_box) {
          obj.def("button", &web_api::generator::button).def("addButton", &web_api::generator::add_button);
        }
        if constexpr (e.value == web_api::generator::ui_tag::line_edit) {
          obj.def("setText", &web_api::generator::set_text<e.value>)
            .def("setPlaceholderText", &web_api::generator::set_placeholder_text);
        }
      }
      if constexpr (std::ranges::contains(web_api::generator::qt_layout_array, e.value)) {
        obj.def("addWidget", &web_api::generator::add_widget<e.value>);
        if (e.value == web_api::generator::ui_tag::grid_layout) {
          obj.def("setHorizontalSpacing", &web_api::generator::set_horizontal_spacing)
            .def("setVerticalSpacing", &web_api::generator::set_vertical_spacing)
            .def("addWidget", &web_api::generator::add_widget_grid);
        }
        if (e.value == web_api::generator::ui_tag::v_box_layout) {
          obj.def("count", &web_api::generator::count).def("itemAt", &web_api::generator::item_at);
        }
      }
      if constexpr (e.value == web_api::generator::ui_tag::table_widget_item) {
        obj.def("setData", &web_api::generator::set_data)
          .def("setFlagsEditable", &web_api::generator::set_flags_editable)
          .def("data", &web_api::generator::get_data)
          .def("text", &web_api::generator::text)
          .def("isFlagsEditable", &web_api::generator::is_flags_editable);
      }
      if constexpr (e.value == web_api::generator::ui_tag::chart) {
        obj.def("setTitle", &web_api::generator::set_title)
          .def("setTitleFont", &web_api::generator::set_title_font)
          .def("setBackgroundBrush", &web_api::generator::set_background_brush)
          .def("setLegendVisible", &web_api::generator::set_legend_visible)
          .def("addAxis", &web_api::generator::add_axis)
          .def("addSeries", &web_api::generator::add_series)
          .def("isLegendVisible", &web_api::generator::is_legend_visible)
          .def("series", &web_api::generator::get_series)
          .def("axes", &web_api::generator::get_axes);
      }
      if constexpr (e.value == web_api::generator::ui_tag::bar_set) {
        obj.def("setBorderColor", &web_api::generator::set_border_color)
          .def("setLabel", &web_api::generator::set_label)
          .def("setColor", &web_api::generator::set_color)
          .def("at", &web_api::generator::get_bar)
          .def("count", &web_api::generator::get_count);
      }
      if constexpr (e.value == web_api::generator::ui_tag::action) {
        obj.def("setText", &web_api::generator::set_text<e.value>)
          .def("isVisible", &web_api::generator::is_visible<e.value>)
          .def("setVisible", &web_api::generator::set_visible<e.value>)
          .def("setMenu", &web_api::generator::set_menu<e.value>);
      }
    });
    app_obj.def("vBoxLayout", &web_api::generator::make_v_box_layout, py::arg("layout") = py::none())
      .def(
        "action",
        static_cast<web_api::generator::qt_action (*)(web_api::generator::qt_objects_interface &, py::object const &)>(
          &web_api::generator::make_action),
        py::arg("qt_widget") = py::none())
      .def(
        "action",
        static_cast<web_api::generator::qt_action (*)(
          web_api::generator::qt_objects_interface &, std::string const &, py::object const &)>(
          &web_api::generator::make_action))
      .def("lineEdit", &web_api::generator::make_line_edit)
      .def("tableWidget", &web_api::generator::make_table_widget, py::arg("object") = py::none())
      .def("pushButton", &web_api::generator::make_push_button, py::arg("qt_widget") = py::none())
      .def("menu", &web_api::generator::make_menu, py::arg("qt_widget") = py::none())
      .def("barSet", &web_api::generator::make_bar_set, py::arg("name"))
      .def("tableWidgetItem", &web_api::generator::make_table_widget_item, py::arg("name"))
      .def("chartView", &web_api::generator::make_chart_view, py::arg("chart"), py::arg("widget") = py::none());
    m.def("export_qt", &web_api::generator::export_qt, py::arg("widget"));
    m.def("fromSecsSinceEpoch", &web_api::generator::from_secs_since_epoch, py::arg("secs"));
    m.def("addSecs", &web_api::generator::add_secs);
    m.def("toMSecsSinceEpoch", &web_api::generator::to_m_sec_since_epoch);
    m.def("color", static_cast<web_api::generator::color (*)(std::string const &)>(&web_api::generator::make_color));
    m.def("color", static_cast<web_api::generator::color (*)(int, int, int, int)>(&web_api::generator::make_color));
    pyexport_client_server(m);
  }
}

#endif

SHYFT_PYTHON_MODULE(ui, m) {
  using namespace shyft;
  m.attr("__doc__") = "Shyft Energy Market user interface configuration api";
  m.attr("__version__") = _version_string();
#if defined(SHYFT_WITH_STM)
  m.attr("shyft_with_stm") = true;
  py::module_ time_series = py::module_::import("shyft.time_series");
  energy_market::ui::pyexport(m);
  // make alias for shyft.time_series.ModelInfo
  auto model_info = time_series.attr("ModelInfo");
  m.attr("ModelInfo") = model_info;
#else
  m.attr("shyft_with_stm") = false;
#endif
}
