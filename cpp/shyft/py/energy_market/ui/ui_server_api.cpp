
#include <csignal>
#include <memory>
#include <mutex>
#include <string>

#include <shyft/energy_market/ui/client.h>
#include <shyft/energy_market/ui/server.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/py/formatters.h>
#include <shyft/web_api/ui/request_handler.h>

namespace shyft::energy_market::ui {

  /** @brief A  client for model type M suitable for python exposure
   *
   * This class takes care of  python gil and mutex, ensuring that any attempt using
   * multiple python threads will be serialized.
   * gil is released while the call is in progress.
   *
   * Using this template saves us the repeating work for similar model-repositories
   *
   * @tparam M a model, same requirements as for srv::client<M>
   *
   */
  struct py_config_client : py_client<config_client> {
    using client_t = config_client;
    using super = py_client;

    py_config_client(std::string const &host_port, int const timeout_ms, int const operation_timeout_ms)
      : super{host_port, timeout_ms, operation_timeout_ms} {
    }

    ~py_config_client() = default;

    py_config_client() = delete;
    py_config_client(py_config_client const &) = delete;
    py_config_client(py_config_client &&) = delete;
    py_config_client &operator=(py_config_client const &o) = delete;

    auto read_model_with_args(
      int64_t mid,
      std::string const &layout_name,
      std::string const &args,
      bool store_layout = false) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.read_model_with_args(mid, layout_name, args, store_layout);
    }
  };

  using py_config_server = py_server_with_web_api<config_server, web_api::ui::request_handler>;

  void pyexport_layout_info(py::module_ &m) {
    // used by stm.Application, thus it must be defined in stm, and alias here.
    auto const stm_module = py::module::import("shyft.energy_market.stm");
    auto const layout_info = stm_module.attr("LayoutInfo");
    m.attr("LayoutInfo") = layout_info;
  }

  void pyexport_client(py::module_ &m) {
    using cm = py_config_client;

    auto wrapper_client = energy_market::export_client<cm>(
      m, "LayoutClient", doc.intro("The client-side components for layouts")());
    wrapper_client.def(
      "read_model_with_args",
      &cm::read_model_with_args,
      doc.intro("Read a layout from storage.")
        .intro("If it is not found, it will try to generate a new layout "
               "based")
        .intro("on provided arguments using the callback "
               "function on the server.")
        .parameters()
        .parameter("mid", "int", "Model ID of layout to read")
        .parameter("name", "str", "Name of layout, if it needs to be generated")
        .parameter("args", "str", "Json format of arguments to generate layout, if needed.")
        .parameter(
          "store_layout", "bool", "If a new layout is generated, whether to store it on server. Defaults to False.")
        .returns("li", "LayoutInfo", "Read or generated LayoutInfo")(),
      py::arg("mid"),
      py::arg("name"),
      py::arg("args"),
      py::arg("store_layout") = false);
  }

  void pyexport_server(py::module_ &m) {
    auto srv_wrapper = energy_market::export_server_with_web_api<py_config_server>(
      m, "LayoutServer", doc.intro("The server-side components for layouts")());

    srv_wrapper.def_property(
      "fx",
      +[](py_config_server &srv) {
        return srv.impl.db.read_cb;
      },
      +[](py_config_server &srv, fx_read_cb_t fx) {
        srv.impl.db.read_cb = pyapi::wrap_callback(std::move(fx));
      },
      doc.intro("server-side callable function(lambda) that takes two parameters:")
        .intro("name :  the name of the layout")
        .intro("fx_args: arbitrary string to pass to the server-side function")
        .intro("The server-side fx is called when the client (or web-api) invoke the "
               "c.read_model_with_args(name,fx_args).")
        .intro("The signature of the callback function should be (name:str, fx_args:str): -> str")
        .intro("This feature is simply enabling the users to tailor server-side functionality in python!")
        .intro("\nExample\n\n")
        .pure(">>> from shyft.energy_market.ui import LayoutServer\n\n"
              ">>> s=LayoutServer()\n"
              ">>> def my_fx(name:str, fx_args:str)->bool:\n"
              ">>>     print(f'invoked with name={name} fx_args={fx_args}')\n"
              ">>>   # note we can use captured Server s here!"
              ">>>     return True\n"
              ">>> # and then bind the function to the callback\n"
              ">>> s.fx=my_fx\n"
              ">>> s.start_server()\n"
              ">>> : # later using client from anywhere to invoke the call\n"
              ">>> fx_result=c.fx('my_layout_id', 'my_args')\n")());
  }

  void pyexport_client_server(py::module_ &m) {
    pyexport_layout_info(m);
    pyexport_server(m);
    pyexport_client(m);
  }
}
