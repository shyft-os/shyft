#include <shyft/py/bindings.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/ts_compress.h>

namespace shyft::energy_market {

  static auto compressed_size_float(std::vector<float> const & v, float accuracy) {
    return time_series::ts_compress_size(v, accuracy);
  }

  static auto compressed_size_double(std::vector<double> const & v, double accuracy) {
    return time_series::ts_compress_size(v, accuracy);
  }

  void pyexport_time_series_support(py::module_ &m) {
    m.def("compressed_size", compressed_size_double, py::arg("double_vector"), py::arg("accuracy"));
    m.def("compressed_size", compressed_size_float, py::arg("float_vector"), py::arg("accuracy"));
  }

}
