#include <algorithm>
#include <cstdint>
#include <iterator>
#include <memory>
#include <ranges>
#include <string>
#include <unordered_map>
#include <vector>

#include <fmt/format.h>

#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/graph/graph_utilities.h>
#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/hydro_operations.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/module.h>
#include <shyft/py/formatters.h>

namespace shyft::energy_market::hydro_power {

  template <auto key, typename T>
  auto make_component_items(std::vector<std::shared_ptr<T>> const &C) {
    std::vector<std::pair<std::string, std::shared_ptr<T>>> M;
    std::ranges::copy(
      std::views::transform(
        C,
        [](auto const &c) {
          return std::make_pair(fmt::format("{}", std::invoke(key, *c)), c);
        }),
      std::inserter(M, M.end()));
    return M;
  }

  template <auto key, typename T>
  auto make_component_keys(std::vector<std::shared_ptr<T>> const &C) {
    std::vector<std::string> M(C.size());
    std::ranges::copy(
      std::views::transform(
        C,
        [](auto const &c) {
          return fmt::format("{}", std::invoke(key, *c));
        }),
      M.data());
    return M;
  }

  template <typename T, auto map_key>
  auto make_component_list(py::handle m, char const *name) {
    using U = std::vector<std::shared_ptr<T>>;
    return pyapi::bind_vector<U>(m, fmt::format("{}List", name).c_str())
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def(
        "items",
        [](U const &l) {
          return make_component_items<map_key>(l);
        })
      .def(
        "__getitem__",
        [](U const &l, std::string const &n) {
          auto it = std::ranges::find_if(l, [&](std::shared_ptr<T> const &t) {
            return fmt::format("{}", std::invoke(map_key, *t)) == n;
          });
          if (it == std::ranges::end(l))
            throw std::runtime_error(fmt::format("Key '{}' not found", n));
          return *it;
        })
      .def(
        "keys",
        [](U const &l) {
          return make_component_keys<map_key>(l);
        })
      .def("values", [](U const &l) {
        return l;
      });
  }

  void pyexport_xy_point_curves(py::module_ &m) {

    pyapi::expose_format(
      py::class_<point>(m, "Point", doc.intro("Simply a point (x,y)")())
        .def(py::init())
        .def(py::init<double, double>(), doc.intro("construct a point with x and y")(), py::arg("x"), py::arg("y"))
        .def(py::init<point const &>(), doc.intro("Create a clone.")(), py::arg("clone"))
        .def_readwrite("x", &point::x, "float: ")
        .def_readwrite("y", &point::y, "float: ")
        .def(py::self == py::self)
        .def(py::self != py::self));

    pyapi::bind_vector<std::vector<point>>(m, "XyPointList").def(py::self == py::self).def(py::self != py::self);

    pyapi::expose_format(
      py::class_<xy_point_curve, std::shared_ptr<xy_point_curve>>(
        m, "XyPointCurve", doc.intro("A curve described using points, piecewise linear.")())
        .def(py::init())
        .def(py::init<std::vector<point> const &>(), py::arg("points"))
        .def(
          py::init(+[](std::vector<double> const &x, std::vector<double> const &y) {
            return xy_point_curve::make(x, y);
          }),
          py::arg("x_vector"),
          py::arg("y_vector"))
        .def(py::init<xy_point_curve const &>(), doc.intro("Create a clone.")(), py::arg("clone"))
        .def_readwrite("points", &xy_point_curve::points, "PointList: describing the curve")
        .def(
          "calculate_y",
          static_cast<double (xy_point_curve::*)(double) const>(&xy_point_curve::calculate_y),
          doc.intro("interpolating and extending")(),
          py::arg("x"))
        .def(
          "calculate_y",
          static_cast<apoint_ts (xy_point_curve::*)(apoint_ts const &, interpolation_scheme) const>(
            &xy_point_curve::calculate_y),
          doc.intro("interpolating and extending")(),
          py::arg("x"),
          py::arg("method") = "linear")
        .def(
          "calculate_x",
          static_cast<double (xy_point_curve::*)(double) const>(&xy_point_curve::calculate_x),
          doc.intro("interpolating and extending")(),
          py::arg("x"))
        .def(
          "calculate_x",
          static_cast<apoint_ts (xy_point_curve::*)(apoint_ts const &, interpolation_scheme) const>(
            &xy_point_curve::calculate_x),
          doc.intro("interpolating and extending")(),
          py::arg("x"),
          py::arg("method") = "linear")
        .def(
          "is_mono_increasing",
          &xy_point_curve::is_xy_mono_increasing,
          doc.intro("true if y=f(x) is monotone and increasing")())
        .def("is_convex", &xy_point_curve::is_xy_convex, doc.intro("true if y=f(x) is convex")())
        .def(
          "x_min",
          +[](xy_point_curve const &c) {
            return x_min(c);
          },
          doc.intro("returns smallest value of x")())
        .def(
          "x_max",
          +[](xy_point_curve const &c) {
            return x_max(c);
          },
          doc.intro("returns largest value of x")())
        .def(
          "y_min",
          +[](xy_point_curve const &c) {
            return y_min(c);
          },
          doc.intro("returns smallest value of y")())
        .def(
          "y_max",
          +[](xy_point_curve const &c) {
            return y_max(c);
          },
          doc.intro("returns largest value of y")())
        .def(py::self == py::self)
        .def(py::self != py::self));

    py::implicitly_convertible<std::vector<point>, xy_point_curve>();

    pyapi::expose_format(
      py::class_<xy_point_curve_with_z, std::shared_ptr<xy_point_curve_with_z>>(
        m, "XyPointCurveWithZ", doc.intro("A XyPointCurve with a reference value z.")())
        .def(py::init())
        .def(py::init<xy_point_curve_with_z const &>(), py::arg("other"))
        .def(py::init<xy_point_curve const &, double>(), py::arg("xy_point_curve"), py::arg("z"))
        .def_readwrite("xy_point_curve", &xy_point_curve_with_z::xy_curve, "XyPointCurve: describes the function at z")
        .def_readwrite("z", &xy_point_curve_with_z::z, "float: z value")
        .def(py::self == py::self)
        .def(py::self != py::self));

    auto t_xy_point_curve_with_z =
      pyapi::bind_vector<std::vector<xy_point_curve_with_z>, std::shared_ptr<std::vector<xy_point_curve_with_z>>>(
        m, "XyPointCurveWithZList", doc.intro("A strongly typed list of XyPointCurveWithZ.")())
        .def(
          "x_min",
          +[](std::vector<xy_point_curve_with_z> const &c) {
            return x_min(c);
          },
          doc.intro("returns smallest value of x")())
        .def(
          "x_max",
          +[](std::vector<xy_point_curve_with_z> const &c) {
            return x_max(c);
          },
          doc.intro("returns largest value of x")())
        .def(
          "y_min",
          +[](std::vector<xy_point_curve_with_z> const &c) {
            return y_min(c);
          },
          doc.intro("returns smallest value of y")())
        .def(
          "y_max",
          +[](std::vector<xy_point_curve_with_z> const &c) {
            return y_max(c);
          },
          doc.intro("returns largest value of y")())
        .def(
          "z_min",
          +[](std::vector<xy_point_curve_with_z> const &c) {
            return z_min(c);
          },
          doc.intro("returns smallest value of z")())
        .def(
          "z_max",
          +[](std::vector<xy_point_curve_with_z> const &c) {
            return z_max(c);
          },
          doc.intro("returns largest value of z")())
        .def( // FIXME: remove this, copy on each eval... - jeh
          "evaluate",
          +[](std::vector<xy_point_curve_with_z> const &self, double x, double z) {
            return xyz_point_curve(self).evaluate(x, z);
          },
          doc.intro("Evaluate the curve at the point (x, z)")(),
          py::arg("x"),
          py::arg("z"))
        .def(py::self == py::self)
        .def(py::self != py::self);

    pyapi::bind_map<xyz_point_curve::container>(m, "XyzPointCurveDict")
      .def(py::self == py::self)
      .def(py::self != py::self);

    auto t_xyz_point_curve =
      py::class_<xyz_point_curve, std::shared_ptr<xyz_point_curve>>(
        m,
        "XyzPointCurve",
        doc.intro("A 3D curve consisting of one or more 2D curves parametrised over a third variable.")())
        .def(py::init())
        .def(
          py::init<std::vector<xy_point_curve_with_z> const &>(), doc.intro("Create from a list.")(), py::arg("curves"))
        .def(
          py::init([](xyz_point_curve::container const &c) {
            xyz_point_curve x{};
            x.curves = c;
            return x;
          }),
          py::arg("curves"))
        .def(
          "set_curve",
          static_cast<void (xyz_point_curve::*)(double, xy_point_curve const &)>(&xyz_point_curve::set_curve),
          doc.intro("Assign an XyzPointCurve to a z-value")(),
          py::arg("z"),
          py::arg("xy"))
        .def("get_curve", &xyz_point_curve::get_curve, doc.intro("get the curve assigned to the value")(), py::arg("z"))
        .def(
          "evaluate",
          &xyz_point_curve::evaluate,
          doc.intro("Evaluate the curve at the point (x, z)")(),
          py::arg("x"),
          py::arg("z"))
        .def("gradient", &xyz_point_curve::gradient)
        .def_readwrite("curves", &xyz_point_curve::curves)
        .def(py::self == py::self)
        .def(py::self != py::self);
    pyapi::expose_format(t_xyz_point_curve);

    auto t_turbine_operating_zone =
      py::class_<turbine_operating_zone, std::shared_ptr<turbine_operating_zone>>(
        m,
        "TurbineOperatingZone",
        doc.intro("A turbine efficiency.")
          .details(
            "Defined by a set of efficiency curves, one for each net head, with optional production limits.\n"
            "Part of the turbine description, to describe the efficiency of an entire turbine, or an "
            "isolated\n"
            "operating zone or a Pelton needle combination. Production limits are only relevant when "
            "representing\n"
            "an isolated operating zone or a Pelton needle combination.")())
        .def(py::init())
        .def(py::init<std::vector<xy_point_curve_with_z> const &>(), py::arg("efficiency_curves"))
        .def(
          py::init<std::vector<xy_point_curve_with_z> const &, double, double>(),
          py::arg("efficiency_curves"),
          py::arg("production_min"),
          py::arg("production_max"))
        .def(
          py::init<std::vector<xy_point_curve_with_z> const &, double, double, double, double, double>(),
          py::arg("efficiency_curves"),
          py::arg("production_min"),
          py::arg("production_max"),
          py::arg("production_nominal"),
          py::arg("fcr_min"),
          py::arg("fcr_max"))
        .def(py::init<turbine_operating_zone const &>(), doc.intro("Create a clone.")(), py::arg("clone"))
        .def_readwrite(
          "efficiency_curves",
          &turbine_operating_zone::efficiency_curves,
          doc.intro(
            "XyPointCurveWithZList: A list of XyPointCurveWithZ efficiency curves for the net head range of "
            "the entire turbine, or an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "production_min",
          &turbine_operating_zone::production_min,
          doc.intro("float: The minimum production for which the efficiency curves are valid.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "production_max",
          &turbine_operating_zone::production_max,
          doc.intro("float: The maximum production for which the efficiency curves are valid.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "production_nominal",
          &turbine_operating_zone::production_nominal,
          doc
            .intro(
              "float: The nominal production, or installed/rated/nameplate capacity, for which the efficiency "
              "curves are valid.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "fcr_min",
          &turbine_operating_zone::fcr_min,
          doc
            .intro(
              "float: The temporary minimum production allowed for this set of efficiency curves when "
              "delivering "
              "FCR.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def_readwrite(
          "fcr_max",
          &turbine_operating_zone::fcr_max,
          doc
            .intro(
              "float: The temporary maximum production allowed for this set of efficiency curves when "
              "delivering "
              "FCR.")
            .notes()
            .note("Only relevant when representing an isolated operating zone or a Pelton needle combination.")())
        .def(
          "evaluate",
          &turbine_operating_zone::evaluate,
          doc.intro("Evaluate the efficiency curves at a point (x, z)")(),
          py::arg("x"),
          py::arg("z"))
        .def(py::self == py::self)
        .def(py::self != py::self);

    pyapi::expose_format(t_turbine_operating_zone);

    pyapi::bind_vector<std::vector<turbine_operating_zone>>(m, "TurbineOperatingZoneList")
      .def(py::self == py::self)
      .def(py::self != py::self);

    py::enum_<turbine_capability>(m, "TurbineCapability", doc.intro("Describes the capabilities of a turbine.")())
      .value("turbine_none", turbine_capability::turbine_none)
      .value("turbine_forward", turbine_capability::turbine_forward)
      .value("turbine_backward", turbine_capability::turbine_backward)
      .value("turbine_reversible", turbine_capability::turbine_reversible)
      .export_values();

    m.def(
      "has_forward_capability",
      +[](turbine_capability const &t) {
        return has_forward_capability(t);
      },
      doc.intro("Checks if a turbine can support generating")());
    m.def(
      "has_backward_capability",
      +[](turbine_capability const &t) {
        return has_backward_capability(t);
      },
      doc.intro("Checks if a turbine can support pumping")());
    m.def(
      "has_reversible_capability",
      +[](turbine_capability const &t) {
        return has_reversible_capability(t);
      },
      doc.intro("Checks if a turbine can support both generating and pumping")());

    {
      auto t_turbine_description =
        py::class_<turbine_description, std::shared_ptr<turbine_description>>(
          m,
          "TurbineDescription",
          doc.intro("Complete description of efficiencies a turbine for all operating zones.")
            .details(
              "Pelton turbines typically have multiple operating zones; one for each needle combination.\n"
              "Other turbines normally have only a single operating zone describing the entire turbine,\n"
              "but may have more than one to model different isolated operating zones.\n"
              "Each operating zone is described with a turbine efficiency object, which in turn may\n"
              "contain multiple efficiency curves; one for each net head.")())
          .def(py::init())
          .def(py::init<std::vector<turbine_operating_zone> const &>(), py::arg("operating_zones"))
          .def(py::init<turbine_description const &>(), py::arg("clone"), doc.intro("Create a clone.")())
          .def(
            "get_operating_zone",
            &turbine_description::get_operating_zone,
            py::arg("p"),
            doc.intro("Find operating zone for given production value p")
              .notes()
              .note(
                "If operating zones are overlapping then the zone with lowest value of production_min will be "
                "selected.")())
          .def_readwrite("operating_zones", &turbine_description::operating_zones)
          .def(py::self == py::self)
          .def(py::self != py::self)
          .def(
            "capability",
            +[](turbine_description const &p) {
              return capability(p);
            },
            doc.intro("Return the capability of the turbine")());

      pyapi::expose_format(t_turbine_description);
    }

    m.def(
      "points_from_x_y",
      [](std::vector<double> const &x, std::vector<double> const &y) {
        if (x.size() != y.size())
          throw std::runtime_error("diff size");
        std::vector<point> r;
        for (std::size_t i = 0; i < x.size(); ++i) {
          r.push_back(point(x[i], y[i]));
        }
        return r;
      },
      py::arg("x"),
      py::arg("y"));
  }

  void pyexport_hydro_component(py::module_ &m) {
    py::enum_<connection_role>(m, "ConnectionRole")
      .value("main", connection_role::main)
      .value("bypass", connection_role::bypass)
      .value("flood", connection_role::flood)
      .value("input", connection_role::input)
      .export_values();
    auto hc = py::class_<hydro_connection, std::shared_ptr<hydro_connection>>(
      m,
      "HydroConnection",
      doc.intro(
        "A hydro connection is the connection object that relate one hydro component to another.\n"
        "A hydro component have zero or more hydro connections, contained in upstream and downstream lists.\n"
        "If you are using the hydro system builder, there will always be a mutual/two way connection.\n"
        "That is, if a reservoir connects downstream to a tunell (in role main), then the tunell will have\n"
        "a upstream connection pointing to the reservoir (as in role input)\n")(),
      py::dynamic_attr());

    auto hyc = expose_system_component_type<hydro_component>(
      m,
      "HydroComponent",
      doc.intro(
        "A hydro component keeps the common attributes and relational properties common for all components "
        "that can contain water")());

    make_component_list<hydro_component, &hydro_component::id>(m, "HydroComponent");

    hyc
      .def_readonly(
        "upstreams",
        &hydro_component::upstreams,
        doc.intro("HydroComponentList: list of hydro-components that are conceptually upstreams")())
      .def_readonly(
        "downstreams",
        &hydro_component::downstreams,
        doc.intro("HydroComponentList: list of hydro-components that are conceptually downstreams")())
      .def(
        "disconnect_from",
        &hydro_component::disconnect_from,
        doc.intro("disconnect from another component")(),
        py::arg("other"))
      .def(
        "equal_structure",
        &hydro_component::equal_structure,
        doc
          .intro(
            "Returns true if the `other` object have the same interconnections to the close neighbors as "
            "self.")
          .intro(
            "The neighbors are identified by their `.id` attribute, and they must appear in the same role to "
            "be considered equal.")
          .intro(
            "E.g. if for a reservoir, a waterway is in role flood for "
            "self, and in role bypass for other, they are different "
            "connections.")
          .parameters()
          .parameter("other", "", "the other object, of same type, hydro component, to compare.")
          .returns("bool", "", "True if the other have same interconnections as self")(),
        py::arg("other"));

    hc.def_readwrite("role", &hydro_connection::role, doc.intro("ConnectionRole: role like main,bypass,flood,input")())
      .def_property_readonly(
        "target",
        +[](hydro_connection const &h) {
          return h.target;
        },
        doc.intro("HydroComponent: target of the hydro-connection, Reservoir|Unit|Waterway")())
      .def_property_readonly(
        "has_target", &hydro_connection::has_target, doc.intro("bool: true if valid/available target")());
    pyapi::expose_format(hc);
  }

  void pyexport_catchment(py::module_ &m) {
    auto t = expose_system_component_type<catchment>(
      m,
      "Catchment",
      doc.intro(
        "Catchment descriptive component, suitable for energy market long-term and/or short term "
        "managment.\n"
        "This component usually would contain usable view of the much more details shyft.hydrology region "
        "model\n")());
    expose_system_component_init(t);

    make_component_list<catchment, &catchment::id>(m, "Catchment");
  }

  auto pyexport_waterway_fwd(py::module_ &m) {
    auto ww_cls = expose_hydro_component_type<waterway>(
      m,
      "Waterway",
      doc.intro("The waterway can be a river or a tunnel, and connects the reservoirs, units(turbine).")());
    make_component_list<waterway, &waterway::id>(m, "Waterway");
    return ww_cls;
  }

  void pyexport_waterway(py::module_ &m, auto &ww_cls) {
    auto g = expose_system_component_type<gate>(
      m,
      "Gate",
      doc.intro(
        "A gate controls the amount of flow into the waterway by the gate-opening.\n"
        "In the case of tunnels, it's usually either closed or open.\n"
        "For reservoir flood-routes, the gate should be used to model the volume-flood characteristics.\n"
        "The resulting flow through a waterway is a function of many factors, most imporant:\n    \n"
        "    * gate opening and gate-characteristics\n"
        "    * upstream water-level\n"
        "    * downstrem water-level(in some-cases)\n"
        "    * waterway properties(might be state dependent)\n\n")());


    make_component_list<gate, &gate::id>(m, "Gate");

    ww_cls
      .def_property_readonly(
        "downstream", &waterway::downstream, doc.intro("HydroComponent: returns downstream object(if any)")())
      .def_property_readonly(
        "upstream", &waterway::upstream, doc.intro("HydroComponent: returns upstream object(if any)")())
      .def_property_readonly(
        "upstream_role",
        &waterway::upstream_role,
        doc.intro("the role the water way has relative to the component above")())
      .def(
        "add_gate",
        +[](std::shared_ptr<waterway> &ww, std::shared_ptr<gate> &g) {
          waterway::add_gate(ww, g);
        },
        doc.intro("add a gate to the waterway")(),
        py::arg("gate"))
      .def("remove_gate", &waterway::remove_gate, doc.intro("remove a gate from the waterway")(), py::arg("gate"))
      .def_readonly("gates", &waterway::gates, doc.intro("GateList: the gates attached to the inlet of the waterway")())
      .def(
        "input_from",
        +[](std::shared_ptr<waterway> s, std::shared_ptr<waterway> o) {
          return waterway::input_from(s, o);
        },
        doc.intro("Connect the input of this waterway to the output of the other waterway.")(),
        py::arg("other"))
      .def(
        "input_from",
        +[](std::shared_ptr<waterway> s, std::shared_ptr<unit> u) {
          return waterway::input_from(s, u);
        },
        doc.intro("Connect the input of this waterway to the output of the unit.")(),
        py::arg("other"))
      .def(
        "input_from",
        +[](std::shared_ptr<waterway> s, std::shared_ptr<reservoir> rsv, connection_role r) {
          return waterway::input_from(s, rsv, r);
        },
        doc.intro(
          "Connect the input of this waterway to the output of the reservoir, and assign the connection "
          "role.")(),
        py::arg("reservoir"),
        py::arg("role") = connection_role::main)
      .def(
        "output_to",
        +[](std::shared_ptr<waterway> s, std::shared_ptr<waterway> w) {
          return waterway::output_to(s, w);
        },
        doc.intro("Connect the output of this waterway to the input of the other waterway.")(),
        py::arg("other"))
      .def(
        "output_to",
        +[](std::shared_ptr<waterway> s, std::shared_ptr<reservoir> r) {
          return waterway::output_to(s, r);
        },
        doc.intro("Connect the output of this waterway to the input of the reservoir.")(),
        py::arg("other"))
      .def(
        "output_to",
        +[](std::shared_ptr<waterway> s, std::shared_ptr<unit> u) {
          return waterway::output_to(s, u);
        },
        doc.intro("Connect the output of this waterway to the input of the unit.")(),
        py::arg("other"));


    g.def_property_readonly(
       "waterway", &gate::wtr_, doc.intro("Waterway: ref. to the waterway where this gate controls the flow")())
      .def_property_readonly( // FIXME: delete this - jeh
        "water_route",
        &gate::wtr_,
        doc.intro("Waterway: ref. to the waterway where this gate controls the flow")());
    expose_system_init(g);
  }

  void pyexport_reservoir(py::module_ &m) {
    expose_hydro_component_type<reservoir>(m, "Reservoir", doc.intro("")())
      .def(
        "input_from",
        +[](std::shared_ptr<reservoir> s, std::shared_ptr<waterway> w) {
          return reservoir::input_from(s, w);
        },
        doc.intro("Connect the input of the reservoir to the output of the waterway.")(),
        py::arg("other"))
      .def(
        "output_to",
        +[](std::shared_ptr<reservoir> s, std::shared_ptr<waterway> w, connection_role r) {
          return reservoir::output_to(s, w, r);
        },
        doc.intro(
          "Connect the output of this reservoir to the input of the waterway, and assign the connection "
          "role")(),
        py::arg("other"),
        py::arg("role") = connection_role::main);
    make_component_list<reservoir, &reservoir::id>(m, "Reservoir");
  }

  void pyexport_power_plant(py::module_ &m) {
    auto pp_cls = expose_system_component_type<power_plant>(
      m,
      "PowerPlant",
      doc.intro(
        "A hydro power plant is the site/building that contains a number of units.\n"
        "The attributes of the power plant, are typically sum-requirement and/or operations that applies\n"
        "all of the units."
        "\n")());
    {
      expose_hydro_component_type<unit>(
        m,
        "Unit",
        doc.intro(
          "An Unit consist of a turbine and a connected generator.\n"
          "The turbine is hydrologically connected to upstream tunnel and downstream tunell/river.\n"
          "The generator part is connected to the electrical grid through a busbar.\n"
          ""
          "In the long term models, the entire power plant is represented by a virtual unit that represents\n"
          "the total capability of the power-plant.\n"
          "\n"
          "The short-term detailed models, usually describes every aggratate up to a granularity that is\n"
          "relevant for the short-term optimization/simulation horizont.\n"
          "\n"
          "A power plant is a collection of one or more units that are natural to group into one power plant.")())
        .def_property_readonly(
          "downstream",
          &unit::downstream,
          doc.intro("Waterway: returns downstream waterway(river/tunnel) object(if any)")())
        .def_property_readonly(
          "upstream", &unit::upstream, doc.intro("Waterway: returns upstream tunnel(water-route) object(if any)")())
        .def_property_readonly(
          "power_plant",
          &unit::pwr_station_,
          doc.intro("PowerPlant: return the hydro power plant associated with this unit")())
        .def_property_readonly( // FIXME: delete this - jeh
          "power_station",
          &unit::pwr_station_,
          doc.intro("PowerPlant: return the hydro power plant associated with this unit")())
        .def_property_readonly(
          "is_pump", &unit::is_pump, doc.intro("bool: Returns true if the unit is a pump, otherwise, returns false")())
        .def(
          "input_from",
          +[](std::shared_ptr<unit> s, std::shared_ptr<waterway> w) {
            return unit::input_from(s, w);
          },
          doc.intro("Connect the input of this unit to the output of the waterway.")(),
          py::arg("other"))
        .def(
          "output_to",
          +[](std::shared_ptr<unit> s, std::shared_ptr<waterway> w) {
            return unit::output_to(s, w);
          },
          doc.intro("Connect the output of this unit to the input of the waterway.")(),
          py::arg("other"));
    }
    make_component_list<unit, &unit::id>(m, "Unit");
    {
      expose_system_component_init(pp_cls);
      pp_cls.def_readwrite("units", &power_plant::units, doc.intro("UnitList: associated units")())
        .def_readwrite("aggregates", &power_plant::units, doc.intro("UnitList: associated units")())
        .def(
          "add_unit",
          +[](std::shared_ptr<power_plant> pp, std::shared_ptr<unit> u) {
            power_plant::add_unit(pp, u);
          },
          doc.intro("add unit to plant")(),
          py::arg("unit"))
        .def("remove_unit", &power_plant::remove_unit, doc.intro("remove unit from plant")(), py::arg("unit"))
        // FIXME: delete - jeh
        .def(
          "add_aggregate",
          +[](std::shared_ptr<power_plant> pp, std::shared_ptr<unit> u) {
            power_plant::add_unit(pp, u);
          },
          doc.intro("add unit to plant")(),
          py::arg("unit"))
        .def("remove_aggregate", &power_plant::remove_unit, doc.intro("remove unit from plant")(), py::arg("unit"));
    }
    make_component_list<power_plant, &power_plant::id>(m, "PowerPlant");
  }

  struct hps_ext {
    static std::vector<char> to_blob(std::shared_ptr<hydro_power_system> const &hps) {
      auto s = hydro_power_system::to_blob(hps);
      return std::vector<char>(s.begin(), s.end());
    }

    static std::shared_ptr<hydro_power_system> from_blob(std::vector<char> &blob) {
      std::string s(blob.begin(), blob.end());
      return hydro_power_system::from_blob(s);
    }
  };

  auto pyexport_hydro_power_system_fwd(py::module_ &m) {
    return expose_system_type<hydro_power_system>(m, "HydroPowerSystem", doc.intro("")());
  }

  void pyexport_hydro_power_system(py::module_ &m, auto &hps) {
    hps
      .def(
        py::init<std::string>(),
        doc.intro("creates an empty hydro power system with the specified name")(),
        py::arg("name"))
      .def(
        py::init([](int i, std::string const &a, std::string const &b) {
          return std::make_shared<hydro_power_system>(i, a, b);
        }),
        doc.intro(
          "creates a an empty new hydro power system with "
          "specified id and name and json str info")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = std::string{})
      .def_readwrite(
        "created",
        &hydro_power_system::created,
        doc.intro("time: The time when this system was created(you should specify it when you create it)")())
      .def_property_readonly(
        "gates",
        +[](hydro_power_system const &h) {
          std::vector<std::shared_ptr<gate>> g;
          std::ranges::copy(h.gates(), std::back_inserter(g));
          return g;
        },
        "GateList: all the gates of the system")
      .def_property(
        "model_area",
        &hydro_power_system::mdl_area_,
        +[](hydro_power_system &hps, std::shared_ptr<model_area> &ma) {
          hps.set_mdl_area(ma);
        },
        doc.intro("ModelArea: returns the model area this hydro-power-system is a part of").see_also("ModelArea")())
      .def(
        "find_unit_by_name",
        &hydro_power_system::find_unit_by_name,
        "returns object that exactly  matches name",
        py::arg("name"))
      .def(
        "find_reservoir_by_name",
        &hydro_power_system::find_reservoir_by_name,
        "returns object that exactly  matches name",
        py::arg("name"))
      .def(
        "find_waterway_by_name",
        &hydro_power_system::find_waterway_by_name,
        "returns object that exactly  matches name",
        py::arg("name"))
      .def(
        "find_gate_by_name",
        &hydro_power_system::find_gate_by_name,
        "returns object that exactly  matches name",
        py::arg("name"))
      .def(
        "find_power_plant_by_name",
        &hydro_power_system::find_power_plant_by_name,
        "returns object that exactly  matches name",
        py::arg("name"))
      .def("find_unit_by_id", &hydro_power_system::find_unit_by_id, "returns object with specified id", py::arg("id"))
      .def(
        "find_reservoir_by_id",
        &hydro_power_system::find_reservoir_by_id,
        "returns object with specified id",
        py::arg("id"))
      .def(
        "find_waterway_by_id",
        &hydro_power_system::find_waterway_by_id,
        "returns object with specified id",
        py::arg("id"))
      .def("find_gate_by_id", &hydro_power_system::find_gate_by_id, "returns object with specified id", py::arg("id"))
      .def(
        "find_power_plant_by_id",
        &hydro_power_system::find_power_plant_by_id,
        "returns object with specified id",
        py::arg("id"))
      .def(
        "equal_structure",
        &hydro_power_system::equal_structure,
        doc.intro(
          "returns true if equal structure of identified objects, using the .id, but not comparing .name, "
          ".attributes etc., to the other")(),
        py::arg("other_hps"))
      .def(
        "equal_content",
        &hydro_power_system::equal_content,
        doc.intro(
          "returns true if alle the content of the hps are equal, same as the equal == operator, except "
          "that "
          ".id, .name .created at the top level is not compared")(),
        py::arg("other_hps"))
      .def(
        "to_blob",
        &hps_ext::to_blob,
        doc.intro("serialize the model into an blob")
          .returns("blob", "string", "blob-serialized version of the model")
          .see_also("from_blob")())
      .def_static(
        "from_blob",
        &hps_ext::from_blob,
        doc.intro("constructs a model from a blob_string previously created by the to_blob method")
          .parameters()
          .parameter(
            "blob_string", "string", "blob-formatted representation of the model, as create by the to_blob method")(),
        py::arg("blob_string"))
      .def(
        "create_river",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_river(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_tunnel",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_tunnel(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_unit",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_unit(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_gate",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_gate(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_power_plant",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_power_plant(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_reservoir",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_reservoir(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_catchment",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_catchment(id, name, json);
        },
        doc.intro("create and add catchment to the system")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "");

    hps
      .def_static( // FIXME: delete - jeh
        "to_blob_ref",
        &hps_ext::to_blob,
        doc.intro("serialize the model into an blob")
          .returns("blob", "string", "blob-serialized version of the model")
          .see_also("from_blob")())
      .def(
        "create_aggregate", // FIXME: delete - jeh
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_unit(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def( // FIXME: delete - jeh
        "create_power_station",
        [](std::shared_ptr<hydro_power_system> &h, int id, std::string const &name, std::string const &json) {
          return hydro_power_system_builder{h}.create_power_plant(id, name, json);
        },
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "");


    hps.def_readwrite("waterways", &hydro_power_system::waterways);
    hps.def_readwrite("units", &hydro_power_system::units);
    hps.def_readwrite("power_plants", &hydro_power_system::power_plants);
    hps.def_readwrite("reservoirs", &hydro_power_system::reservoirs);
    hps.def_readwrite("catchments", &hydro_power_system::catchments);

    // FIXME: remove - jeh
    hps.def_readwrite("aggregates", &hydro_power_system::units);
    hps.def_readwrite("water_routes", &hydro_power_system::waterways);
    hps.def_readwrite("power_stations", &hydro_power_system::power_plants);

    auto hpsb =
      py::class_<hydro_power_system_builder>(
        m, "HydroPowerSystemBuilder", doc.intro("class to support building hydro-power-systems save and easy")())
        .def(py::init<std::shared_ptr<hydro_power_system> &>(), py::arg("hydro_power_system"))
        .def(
          "create_river",
          &hydro_power_system_builder::create_river,
          doc.intro("create and add river to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_tunnel",
          &hydro_power_system_builder::create_tunnel,
          doc.intro("create and add river to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_waterway",
          &hydro_power_system_builder::create_waterway,
          doc.intro("create and add river to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_gate",
          &hydro_power_system_builder::create_gate,
          doc.intro("create and add a gate to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_unit",
          &hydro_power_system_builder::create_unit,
          doc.intro("creates a new unit with the specified parameters")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_reservoir",
          &hydro_power_system_builder::create_reservoir,
          doc.intro("creates and adds a reservoir to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_power_plant",
          &hydro_power_system_builder::create_power_plant,
          doc.intro("creates and adds a power plant to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "")
        .def(
          "create_catchment",
          &hydro_power_system_builder::create_catchment,
          doc.intro("create and add catchmment to the system")(),
          py::arg("id"),
          py::arg("name"),
          py::arg("json") = "");

    // FIXME: delete - jeh
    hpsb
      .def(
        "create_power_station",
        &hydro_power_system_builder::create_power_plant,
        doc.intro("creates and adds a power plant to the system")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_aggregate",
        &hydro_power_system_builder::create_unit,
        doc.intro("creates a new unit with the specified parameters")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "")
      .def(
        "create_water_route",
        &hydro_power_system_builder::create_waterway,
        doc.intro("create and add river to the system")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "");
  }

  void pyexport_hydro_graph(py::module_ &m) {
    m.def(
      "upstream_reservoirs",
      graph::upstream_reservoirs,
      doc.intro("Find all reservoirs upstream from component, stopping at `max_dist` traversals.")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("reservoirs", "ReservoirList", "The reservoirs within the specified distance")(),
      py::arg("component"),
      py::arg("max_dist") = 0);
    m.def(
      "downstream_reservoirs",
      graph::downstream_reservoirs,
      doc.intro("Find all reservoirs upstream from component, stopping at `max_dist` traversals")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("reservoirs", "ReservoirList", "The reservoirs within the specified distance")(),
      py::arg("component"),
      py::arg("max_dist") = 0);
    m.def(
      "upstream_units",
      graph::upstream_units,
      doc.intro("Find units upstream from component, stopping at `max_dist` traversals")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("units", "UnitList", "The units within the specified distance")(),
      py::arg("component"),
      py::arg("max_dist") = 0);
    m.def(
      "downstream_units",
      graph::downstream_units,
      doc.intro("Find all units downstream from component, stopping at `max_dist` traversals")
        .parameters()
        .parameter("max_dist", "int", "max traversals")
        .returns("units", "UnitList", "The units within the specified distance")(),
      py::arg("component"),
      py::arg("max_dist") = 0);
  }

  void pyexport_hydro_operations(py::module_ &m) {
    using hgt = hydro_operations;
    using chc = std::shared_ptr<hydro_component> const;
    using hc_vec = std::vector<std::shared_ptr<hydro_component>>;

    py::class_<hgt, std::shared_ptr<hgt>>(m, "HydroGraphTraversal", doc.intro("A collection of hydro operations")())
      .def_static<void (*)(hc_vec &, connection_role)>(
        "path_to_ocean", &hgt::path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .def_static<void (*)(hc_vec &)>(
        "path_to_ocean", &hgt::path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .def_static<hc_vec (*)(chc &, connection_role)>(
        "get_path_to_ocean", &hgt::get_path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .def_static<hc_vec (*)(chc &)>(
        "get_path_to_ocean", &hgt::get_path_to_ocean, doc.intro("finds path to ocean for a given hydro component")())
      .def_static<hc_vec (*)(chc &, chc &, connection_role)>(
        "get_path_between", &hgt::get_path_between, doc.intro("finds path between two hydro components")())
      .def_static<hc_vec (*)(chc &, chc &)>(
        "get_path_between", &hgt::get_path_between, doc.intro("finds path between two hydro components")())
      .def_static<bool (*)(chc &, chc &, connection_role)>(
        "is_connected", &hgt::is_connected, doc.intro("finds whether two hydro components are connected")())
      .def_static<bool (*)(chc &, chc &)>(
        "is_connected", &hgt::is_connected, doc.intro("finds whether two hydro components are connected")())
      .def_static(
        "extract_water_courses",
        &hgt::extract_water_courses,
        doc.intro("extracts the sub-hydro system from a given hydro system")(),
        py::arg("hps"));
  }

  void pyexport(py::module_ &m) {

    py::module_ base = py::module_::import("shyft.time_series");
    auto hps = pyexport_hydro_power_system_fwd(m);
    make_component_list<hydro_power_system, &hydro_power_system::name>(m, "HydroPowerSystem");

    pyexport_catchment(m);
    pyexport_hydro_component(m);
    auto ww_cls = pyexport_waterway_fwd(m);
    pyexport_reservoir(m);
    pyexport_power_plant(m);
    pyexport_waterway(m, ww_cls);
    pyexport_hydro_graph(m);
    pyexport_hydro_operations(m);
    pyexport_hydro_power_system(m, hps);
  }

}
