/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>

namespace shyft::energy_market::stm {

  std::tuple<
    py::class_<contract, std::shared_ptr<contract>>,
    py::class_<contract_portfolio, std::shared_ptr<contract_portfolio>>>
    pyexport_model_contract_fwd(py::module_ &m) {
    auto c = expose_component_type<contract>(
      m,
      "Contract",
      doc.intro("A contract between two parties, seller and buyer, for sale of a product at a given price.")
        .intro("The contract can refer to specific power-plant, unit group, or even other contracts.")());
    auto cp = expose_component_type<contract_portfolio>(
      m,
      "ContractPortfolio",
      doc.intro(
        "Stm contract portfolio represents a set of contracts, so that the sum of interesting contract "
        "properties can be evaluated and compared.")());
    return {c, cp};
  }

  void pyexport_model_contract(
    py::module_ &m,
    py::class_<contract, std::shared_ptr<contract>> &c,
    py::class_<contract_portfolio, std::shared_ptr<contract_portfolio>> &cp) {

    expose_subcomponent_init<stm_system, &contract::sys_>(c, "sys", "system");
    {
      auto cr = expose_type<contract_relation>(
        c,
        "ContractRelation",
        doc.intro("A relation to another contract, where the relation-type is a user specified integer.")
          .intro("This allows building and maintaining contract system that have internal rules/constraints")
          .intro("There is a minimal set of rules, like avoiding circularities that are enforced")());
      expose_component_list_type<contract_relation>(c, "ContractRelation");
      cr.def_readonly("related", &contract_relation::related, doc.intro("Contract: The related contract")());
      cr.def_readonly("id", &contract_relation::id, doc.intro("int: The id of the relation")());
      add_proxy_property(
        cr,
        "relation_type",
        contract_relation,
        relation_type,
        doc.intro("_u16: Free to use integer to describe relation type")());
      cr.def_property_readonly(
        "owner", &contract_relation::owner_, doc.intro("Contract: ref. to the contract that owns this relation")());
    }


    c.def(
       "get_portfolios",
       &contract::get_portfolios,
       "Get any portfolios this contract is associated with. Convenience for search in ContractPortfolio.contracts.")
      .def(
        "add_to_portfolio",
        &contract::add_to_portfolio,
        "Add this contract to specified portfolio. Convenience for appending to ContractPortfolio.contracts.")
      .def(
        "get_market_areas",
        &contract::get_energy_market_areas,
        "Get any energy market areas this contract is associated with. Convenience for search in MarketArea.contracts.")
      .def(
        "add_to_market_area",
        &contract::add_to_energy_market_area,
        "Add this contract to specified energy market area. Convenience for appending to MarketArea.contracts.")
      .def(
        "add_relation",
        &contract::add_relation,
        doc.intro("Add a contract as a relation from this contract")
          .parameters()
          .parameter("id", "Id", "The relation id (must be unique for this contract)")
          .parameter("contract", "Contract", "The contract to be added as a relation")
          .parameter("relation_type", "RelationType", "A free to use integer to describe relation type")(),
        py::arg("id"),
        py::arg("contract"),
        py::arg("relation_type"))
      .def(
        "remove_relation",
        &contract::remove_relation,
        doc.intro("Remove relation to contract.")
          .parameters()
          .parameter("contract_relation", "ContractRelation", "The relation to be removed")(),
        py::arg("contract_relation"))
      .def(
        "find_related_to_this",
        &contract::find_related_to_this,
        doc.intro("Find contracts that have a relation to self.")
          .returns("contracts", "ContractVector", "The contracts that have a relation to this contract")());

    add_proxy_property(
      c,
      "quantity",
      contract,
      quantity,
      "_ts: [J/s] Contract quantity, in rate units, so that integrated over contract period gives total volume.");
    add_proxy_property(c, "price", contract, price, "_ts: [money/J] Contract price.");
    add_proxy_property(
      c,
      "fee",
      contract,
      fee,
      "_ts: [money/s] Any contract fees, in rate units so that fee over the contract period gives total fee.");
    add_proxy_property(
      c,
      "revenue",
      contract,
      revenue,
      "_ts: [money/s] Calculated revenue, in rate units, so that integrated of the contract period gives total revenue "
      "volume");
    add_proxy_property(
      c,
      "parent_id",
      contract,
      parent_id,
      "_string: Optional reference to parent (contract). Typically for forwardes/futures, that is splitted into "
      "shorter terms as time for the delivery is approaching.");
    add_proxy_property(c, "active", contract, active, "_ts: Contract status (dead/alive).");
    add_proxy_property(
      c,
      "validation",
      contract,
      validation,
      "_ts: Validation status (and whether the contract has been validated or not).");
    add_proxy_property(c, "buyer", contract, buyer, "_string: The name of the buyer party of the contract.");
    add_proxy_property(c, "seller", contract, seller, "_string: The name of the seller party of the contract.");
    add_proxy_property(
      c, "options", contract, options, "_t_xy_: Defines optional price/volume curves for future trading.");

    {
      auto cc = py::class_<contract::constraint_>(c, "Constraint", doc.intro("Contract.Constraint attributes")());
      _add_proxy_property(
        cc,
        "min_trade",
        contract::constraint_,
        min_trade,
        "_ts: Minimum quantity (volume) that must be traded for this contract.");
      _add_proxy_property(
        cc,
        "max_trade",
        contract::constraint_,
        max_trade,
        "_ts: Maximum quantity (volume) that must be traded for this contract.");
      _add_proxy_property(
        cc,
        "ramping_up",
        contract::constraint_,
        ramping_up,
        "_ts: Max quantity (volume) to ramp up between timesteps for this contract.");
      _add_proxy_property(
        cc,
        "ramping_down",
        contract::constraint_,
        ramping_down,
        "_ts: Max quantity (volume) to ramp down between timesteps for this contract.");
      _add_proxy_property(
        cc,
        "ramping_up_penalty_cost",
        contract::constraint_,
        ramping_up_penalty_cost,
        "_ts: Penalty for violating ramping up limit.");
      _add_proxy_property(
        cc,
        "ramping_down_penalty_cost",
        contract::constraint_,
        ramping_down_penalty_cost,
        "_ts: Penalty for violating ramping down limit.");
    }

    expose_subcomponent_init<stm_system, &contract_portfolio::sys_>(cp, "sys", "system");

    cp.def_readwrite("contracts", &contract_portfolio::contracts);

    add_proxy_property(
      cp,
      "quantity",
      contract_portfolio,
      quantity,
      "_ts: [J/s] normally sum of contracts, unit depends on the contracts.");
    add_proxy_property(
      cp, "fee", contract_portfolio, fee, "_ts: [money/s] Fees of the portfolio, normally sum of contracts.");
    add_proxy_property(cp, "revenue", contract_portfolio, revenue, "_ts: [money/s] Calculated revenue.");

    c.def_readwrite("power_plants", &contract::power_plants, "PowerPlantList: List of associated power plants.")
      .def_readwrite("wind_farms", &contract::wind_farms, "WindFarmList: List of associated wind farms.")
      .def_readwrite("relations", &contract::relations, "ContractRelationList: List of related contracts.")
      .def_readwrite("constraint", &contract::constraint, "Constraint: Constrant of this contract.");
  }
}
