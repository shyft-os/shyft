#include <algorithm>
#include <mutex>
#include <stdexcept>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/srv/compute/client.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/srv/compute/server.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/srv/fast_server_iostream.h>
#include <shyft/version.h>

namespace shyft::energy_market::stm::srv::compute {

  struct py_client {
    std::mutex mutex;
    client impl;

    py_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms)
      : impl{{.connection = srv_connection{host_port, timeout_ms, operation_timeout_ms}}} {
    }

    ~py_client() = default;

    any_reply send(any_request const &request) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lock(mutex);
      return std::visit(
        [&](auto const &req) {
          return any_reply{impl.send(req)};
        },
        request);
    }

    int get_operation_timeout_ms() {
      return impl.connection.operation_timeout_ms;
    }

    void set_operation_timeout_ms(int operation_timeout_ms) {
      impl.connection.operation_timeout_ms = operation_timeout_ms;
    }
  };

  void pyexport(py::module_ &m) {
    {
      auto t_state = py::enum_<compute::state>(m, "State", "Describes the possible states of a compute server");
      pyapi::expose_enumerators(t_state);
    }
    // import from stm the  ComputeServerStatus,ComputeManagedServerState from stm, alias here as compute.ServerStatus
    auto const stm_module = py::module::import("shyft.energy_market.stm");
    auto ServerStatusAttr = stm_module.attr("ServerStatus");
    auto ManagedServerStateAttr = stm_module.attr("ManagedServerState");
    m.attr("ServerStatus") = ServerStatusAttr;
    m.attr("ManagedServerState") = ManagedServerStateAttr;
    {
      for_each_enum<compute::message_tag>([&](auto e) {
        {
          using request = compute_request<e.value>;
          auto t_name = pyapi::pep8_typename(fmt::format("{}_request", e.name));
          auto t_request = py::class_<request>(m, t_name.c_str(), "Compute request").def(py::init());
          pyapi::expose_members(t_request);
          pyapi::expose_format_str(t_request);
          pyapi::expose_format_repr(t_request);
        }
        {
          using reply = compute_reply<e.value>;
          auto t_name = pyapi::pep8_typename(fmt::format("{}_reply", e.name));
          auto t_reply = py::class_<reply>(m, t_name.c_str(), "Compute reply").def(py::init());
          pyapi::expose_members(t_reply);
          pyapi::expose_format_str(t_reply);
          pyapi::expose_format_repr(t_reply);
        }
      });
    }
    {
      auto t_client =
        py::class_<py_client>(m, "Client", "A client to the Server.")
          .def(
            py::init<std::string, int, int>(),
            "Create a client with the host port `host_port` ",
            py::arg("host_port"),
            py::arg("timeout_ms"),
            py::arg("operation_timeout_ms") = 0)
          .def_property(
            "operation_timeout_ms",
            &py_client::get_operation_timeout_ms,
            &py_client::set_operation_timeout_ms,
            "int: Operation timeout for remote server operations, in number milliseconds.");

      t_client.def("send", &py_client::send, py::arg("request"));

      auto config = py::class_<server_config, std::unique_ptr<server_config>, shyft::srv::fast_server_iostream_config>(
                      m, "ServerConfig", "Compute server config")
                      .def(py::init<>());

      pyapi::expose_members(config);
      pyapi::expose_constructor(config);

      py::class_<server>(m, "Server", "A server for running heavy STM computations.")
        .def(
          py::init(+[](server_config const &config) {
            return new server{server_dispatch{config}, config}; // slice/copy, then move: Ok?
          }),
          py::arg("config") = server_config{})
        .def("start_server", &compute::server::start_server)
        .def(
          "stop_server",
          +[](compute::server &server, int ms) {
            pyapi::scoped_gil_release gil;
            if (ms > 0)
              server.set_graceful_close_timeout(ms);
            server.clear();
          },
          py::arg("timeout") = 1000)
        .def("is_running", &compute::server::is_running)
        .def("set_listening_port", &compute::server::set_listening_port, py::arg("port_no"))
        .def("get_listening_port", &compute::server::get_listening_port)
        .def("get_listening_ip", &compute::server::get_listening_ip)
        .def("set_listening_ip", &compute::server::set_listening_ip, py::arg("ip"))
        .def("close", &compute::server::clear);
    }
  }
}

SHYFT_PYTHON_MODULE(compute, m) {
  m.attr("__doc__") = "Shyft Energy Market compute server";
  m.attr("__version__") = shyft::_version_string();
  shyft::energy_market::stm::srv::compute::pyexport(m);
}
