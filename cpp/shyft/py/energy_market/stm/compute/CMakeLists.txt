shyft_add_python_module(TARGET shyft-stm-compute-python NAME compute
  DEPENDS shyft-time_series-python shyft-energy_market-python shyft-stm-python
  PATH energy_market/stm)
shyft_glob_sources(TARGET shyft-stm-compute-python)
target_link_libraries(shyft-stm-compute-python PRIVATE shyft-core shyft-stm)
shyft_install_python_module(TARGET shyft-stm-compute-python COMPONENT python PATH energy_market/stm)
