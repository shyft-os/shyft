shyft_add_python_module(TARGET shyft-stm-python NAME stm
  DEPENDS shyft-time_series-python shyft-energy_market-python
  PATH energy_market
  IS_PARENT TRUE
)

if(SHYFT_WITH_STM)
    file(GLOB sources CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
    target_sources(shyft-stm-python PRIVATE ${sources})
    file(GLOB sources CONFIGURE_DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/srv/*.cpp)
    target_sources(shyft-stm-python PRIVATE ${sources})
else()
    target_sources(shyft-stm-python PRIVATE submodule.cpp)
endif()

target_link_libraries(shyft-stm-python PRIVATE shyft-core)
if(SHYFT_WITH_STM)
    target_link_libraries(shyft-stm-python PRIVATE shyft-stm)
endif()

shyft_install_python_module(TARGET shyft-stm-python COMPONENT python PATH energy_market)
