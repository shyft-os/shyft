/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/stm/urls.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>

#include <pybind11/cast.h>

namespace shyft::energy_market::stm {
  void pyexport_url(py::module_ &m) {
    {
      auto e = py::class_<url_resolve_error>(m, "UrlResolveError", "Url resolve error");
      pyapi::expose_members(e);
      pyapi::expose_format(e);
    }
    m.attr("url_planning_custom_input_prefix") = url_planning_custom_input_prefix;
    m.attr("url_planning_custom_output_prefix") = url_planning_custom_output_prefix;

    m.def(
      "url_planning_inputs",
      url_planning_inputs,
      doc.intro("Get all the time-series input for running an algorithmic process on a stm_sytem")(),
      py::arg("model_id"),
      py::arg("model"));
    m.def(
      "url_planning_outputs",
      url_planning_outputs,
      doc.intro("Get all the time-series urls that are considered result, or might contain result, after running an "
                "algorithmic process on a stm-system")(),
      py::arg("model_id"),
      py::arg("model"));
  }
}
