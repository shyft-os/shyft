/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <fmt/core.h>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>

namespace shyft::energy_market::stm {

  /** extensions to ease py expose */
  struct net_ext {
    // wrap all create calls via the network_builder to enforce build-rules
    static auto create_transmission_line(
      std::shared_ptr<network>& n,
      int id,
      std::string const & name,
      std::string const & json) {
      return network_builder(n).create_transmission_line(id, name, json);
    }

    static auto create_busbar(std::shared_ptr<network>& n, int id, std::string const & name, std::string const & json) {
      return network_builder(n).create_busbar(id, name, json);
    }
  };

  py::class_<network, std::shared_ptr<network>> pyexport_model_network_fwd(py::module_& m) {
    auto c = expose_component_type<network>(
      m, "Network", doc.intro("A network consisting of busbars and transmission lines.")());
    return c;
  }

  void pyexport_model_network(py::module_& m, py::class_<network, std::shared_ptr<network>>& c) {
    expose_subcomponent_init<stm_system, &network::sys_>(c, "sys", "system");
    c.def(
       "create_transmission_line",
       &net_ext::create_transmission_line,
       doc.intro("Create stm transmission line with unique uid.")
         .returns("transmission_line", "TransmissionLine", "The new transmission line.")(),
       py::arg("uid"),
       py::arg("name"),
       py::arg("json") = std::string(""))
      .def(
        "create_busbar",
        &net_ext::create_busbar,
        doc.intro("Create stm busbar with unique uid.").returns("busbar", "Busbar", "The new busbar.")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = std::string(""))
      .def_readonly(
        "transmission_lines", &network::transmission_lines, "TransmissionLineList: List of transmission lines.")
      .def_readonly("busbars", &network::busbars, "BusbarList: List of busbars.");
  }
}
