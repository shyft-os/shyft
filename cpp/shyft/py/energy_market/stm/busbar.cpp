/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/py/bindings.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>

namespace shyft::energy_market::stm {
  py::class_<busbar, std::shared_ptr<busbar>> pyexport_model_busbar_fwd(py::module_ &m) {
    auto c = expose_component_type<busbar>(m, "Busbar", doc.intro("A hub connected by transmission lines")());
    return c;
  }

  void pyexport_model_busbar(py::module_ &m, py::class_<busbar, std::shared_ptr<busbar>> &c) {

    expose_subcomponent_init<network, &busbar::net_>(c, "net", "network");

    {
      auto pmm = expose_type<power_module_member>(
        m,
        "PowerModuleMember",
        "A power_mobule busbar member, refers to a specific power_module along with the time-series that takes care of "
        "the temporal membership/contribution to the busbar sum.");
      pmm.def_readonly(
        "power_module",
        &power_module_member::power_module,
        doc.intro("PowerModule: The power_module this member represents.")());
      add_proxy_property(
        pmm,
        "active",
        power_module_member,
        active,
        doc.intro(
          "_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
          "power_module.")());
      c.def_readonly(
        "power_modules", &busbar::power_modules, "PowerModuleMemberList: modules associated with this busbar");
    }

    {
      auto um = expose_type<unit_member>(
        m,
        "UnitMember",
        "A unit busbar member, refers to a specific unit along with the time-series that takes care of the temporal "
        "membership/contribution to the busbar sum.");

      um.def_readonly("unit", &unit_member::unit, doc.intro("Unit: The unit this member represents.")());
      add_proxy_property(
        um,
        "active",
        unit_member,
        active,
        doc.intro(
          "_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
          "unit.")());
      c.def_readonly("units", &busbar::units, "UnitMemberList: Units associated with this busbar");
    }
    {
      auto wtm = expose_type<wind_farm_member>(
        m,
        "WindFarmMember",
        "A wind farm busbar member, refers to a specific wind farm along with the time-series that takes care of the "
        "temporal "
        "membership/contribution to the busbar sum.");

      wtm.def_readonly("farm", &wind_farm_member::farm, doc.intro("Farm: The wind farm this member represents.")());
      add_proxy_property(
        wtm,
        "active",
        wind_farm_member,
        active,
        doc.intro(
          "_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
          "wind farm.")());
      c.def_readonly("wind_farms", &busbar::wind_farms, "WindFarmMemberList: Wind Farms associated with this busbar");
    }
    {
      auto p = expose_attributes_type<busbar::ts_triplet>(
        c, "TsTriplet", doc.intro("Describes .realised, .schedule and .result time-series")());
      _add_proxy_property(
        p, "realised", busbar::ts_triplet, realised, "_ts: SI-units, realised time-series, as in historical fact");
      _add_proxy_property(
        p, "schedule", busbar::ts_triplet, schedule, "_ts: SI-units, schedule, as in current schedule");
      _add_proxy_property(
        p, "result", busbar::ts_triplet, result, "_ts: SI-units, result, as provided by optimisation");
      c.def_readonly("flow", &busbar::flow, "TsTriplet: Flow attributes.")
        .def_readonly("price", &busbar::price, "TsTriplet: Price attributes.");
    }
    c.def(
       "get_transmission_lines_from_busbar",
       &busbar::get_transmission_lines_from_busbar,
       "Get any transmission lines connected from this busbar")
      .def(
        "get_transmission_lines_to_busbar",
        &busbar::get_transmission_lines_to_busbar,
        "Get any transmission lines connected to this busbar")
      .def("get_market_areas", &busbar::get_market_areas, "Get any market areas associated with this busbar")
      .def(
        "add_to_start_of_transmission_line",
        &busbar::add_to_start_of_transmission_line,
        "Add this busbar to the start of a transmission line")
      .def(
        "add_to_end_of_transmission_line",
        &busbar::add_to_end_of_transmission_line,
        "Add this busbar to the end of a transmission line")
      .def("add_power_module", &busbar::add_power_module, "Associate a (time-dependent) power module to this busbar")
      .def(
        "remove_power_module", &busbar::remove_power_module, "Remove a (time-dependent) power module from this busbar")
      .def("add_unit", &busbar::add_unit, "Associate a (time-dependent) unit to this busbar")
      .def("remove_unit", &busbar::remove_unit, "Remove a (time-dependent) unit from this busbar")
      .def("add_wind_farm", &busbar::add_wind_farm, "Associate a (time-dependent) wind farm to this busbar")
      .def("remove_wind_farm", &busbar::remove_wind_farm, "Remove a (time-dependent) wind farm from this busbar")
      .def("add_to_market_area", &busbar::add_to_market_area, "Associate a market area to this busbar");
  }
}
