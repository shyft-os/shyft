/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/wind_turbine.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {
  py::class_<wind_farm, std::shared_ptr<wind_farm>> pyexport_model_wind_farm_fwd(py::module_ &m) {
    auto w = expose_component_type<wind_farm>(
      m,
      "WindFarm",
      doc.intro(
        "A wind farm, models a `wiki-wind-farm`_.\n"
        "In Shyft we provide the descriptive model suitable for covering\n"
        "multiple processes and purposes, ranging from high level estimates of wind farm,\n"
        "suitable for region-level power production analysis, to detailed turbine-level models,\n"
        "allowing detailed modelling and operations for day-ahead/intraday\n\n"
        ".. _wiki-wind-farm: https://en.wikipedia.org/wiki/Wind_farm\n")());
    return w;
  }

  void pyexport_model_wind_farm(py::module_ &m, py::class_<wind_farm, std::shared_ptr<wind_farm>> &w) {

    expose_subcomponent_init<stm_system, &wind_farm::sys_>(w, "sys", "system");

    w.def(
       "create_wind_turbine",
       &wind_farm::create_wind_turbine,
       doc
         .intro(
           "Creates and adds a wind-turbine to the wind-farm already a part of StmSystem(requirement).\n"
           "Valid id and non-empty name and uniqueness of those are enforced.\n")
         .parameters()
         .parameter("id", "int", "Unique id of the wind-turbine.")
         .parameter("name", "str", "Unique name of the wind-turbine.")
         .parameter("json", "str", "Json payload for py customization.")
         .parameter("location", "GeoPoint", "Location of the wind-turbine.")
         .parameter("height", "TimeSeries", "Height of the wind-turbine.")
         .parameter("power_curve", "t_xy_", "Power-curve for the wind-turbine.")
         .returns("wind_turbine", "WindTurbine", "The newly created and added wind-turbine.")(),
       py::arg("id"),
       py::arg("name"),
       py::arg("json"),
       py::arg("location"),
       py::arg("height"),
       py::arg("power_curve"))
      .def(
        "add_wind_turbines",
        &wind_farm::add_wind_turbines,
        doc
          .intro(
            "Adds a prepared list of  wind-turbines to the wind-farm already a part of a StmSystem(requirement).\n"
            "The items are validated prior any modifications to the wind-farm.\n"
            "The wind-farm is assigned the owner of the turbines added.")
          .parameters()
          .parameter("wind_turbines", "WindTurbineList", "Wind-turbines to be added.")(),
        py::arg("wind_turbines"));

    w.def_readonly(
      "wind_turbines", &wind_farm::wind_turbines, "WindTurbineList: list of wind turbines for this WindFarm");
    {
      auto prod = expose_attributes_type<wind_farm::production_>(
        w, "Production", doc.intro("WindFarm.production attributes, amount power produced[W].")());
      _add_proxy_property(
        prod,
        "forecast",
        wind_farm::production_,
        forecast,
        "_ts: [W] The forecast amount of power produced, time series.");
      _add_proxy_property(
        prod, "schedule", wind_farm::production_, schedule, "_ts: [W] The current schedule, time series.");
      _add_proxy_property(
        prod, "realised", wind_farm::production_, realised, "_ts: [W] Production realised, time series.");
      _add_proxy_property(
        prod,
        "result",
        wind_farm::production_,
        result,
        "_ts: [W] Power produced. As computed by optimization/simulation process.");
      {
        auto upc = expose_attributes_type<wind_farm::production_::constraint_>(
          prod, "Constraint", doc.intro("Contains the effect constraints to the wind-turbine.")());
        _add_proxy_property(
          upc, "min", wind_farm::production_, constraint_::min, "_ts: [W] Production constraint minimum.");
        _add_proxy_property(
          upc, "max", wind_farm::production_, constraint_::max, "_ts: [W] Production constraint maximum.");
      }
      prod.def_readonly("constraint", &wind_farm::production_::constraint, "Constraint: relevant for production");
    }
    w.def_readonly("production", &wind_farm::production, "Production: Production attributes.");
    {
      auto r = expose_attributes_type<wind_farm::reserve_>(
        w, "Reserve", doc.intro("WindFarm.Reserve contains all operational reserve related attributes.")());
      {
        auto rs = expose_attributes_type<wind_farm::reserve_::spec_>(
          r, "Spec", doc.intro("Describes reserve specification, (.schedule, or min..result..max) SI-units is W.")());
        _add_proxy_property(rs, "schedule", wind_farm::reserve_::spec_, schedule, "_ts: Reserve schedule.");
        _add_proxy_property(
          rs, "min", wind_farm::reserve_::spec_, min, "_ts: Reserve minimum of range if no schedule.");
        _add_proxy_property(
          rs, "max", wind_farm::reserve_::spec_, max, "_ts: Reserve minimum of range if no schedule.");
        _add_proxy_property(rs, "cost", wind_farm::reserve_::spec_, cost, "_ts: Reserve cost.");
        _add_proxy_property(rs, "result", wind_farm::reserve_::spec_, result, "_ts: Reserve result.");
        _add_proxy_property(rs, "penalty", wind_farm::reserve_::spec_, penalty, "_ts: Reserve penalty.");
        _add_proxy_property(rs, "realised", wind_farm::reserve_::spec_, realised, "_ts: Reserve realised.");
      }
      {
        auto rp = expose_attributes_type<wind_farm::reserve_::pair_>(
          r, "Pair", doc.intro("Describes the up and down pair of reserve specification.")());
        rp.def_readonly("up", &wind_farm::reserve_::pair_::up, "Spec: Up reserve specification.");
        rp.def_readonly("down", &wind_farm::reserve_::pair_::down, "Spec: Down reserve specification.");
      }
      r.def_readonly("fcr_n", &wind_farm::reserve_::fcr_n, "Pair: FCR_n up, down attributes.")
        .def_readonly("fcr_d", &wind_farm::reserve_::fcr_d, "Pair: FCR_d up, down attributes.")
        .def_readonly("mfrr", &wind_farm::reserve_::mfrr, "Pair: mFRR up, down attributes.");
    }
    w.def_readonly("reserve", &wind_farm::reserve, "Reserve: Reserve attributes.");

    add_proxy_property(
      w, "height", wind_farm, height, doc.intro("_ts: [m] average turbine height for the Wind Farm.")());
    add_proxy_property(
      w, "unavailability", wind_farm, unavailability, doc.intro("_ts: [] 0..1, unavailability for the Wind Farm.")());
    add_proxy_property(
      w,
      "power_correction",
      wind_farm,
      power_correction,
      doc.intro("_ts: [] 0.7..1.3 air-density power correction factor for the Wind Farm.")());
    add_proxy_property(
      w, "wake_loss", wind_farm, wake_loss, doc.intro("_ts: [] 0.87..0.93 wake-loss factor for the Wind Farm.")());
    add_proxy_property(
      w, "icing", wind_farm, icing, doc.intro("_ts: [] 0.6..1.0 icing production loss factor for the Wind Farm.")());

    add_proxy_property(
      w,
      "location",
      wind_farm,
      location,
      doc.intro("_geo_point: [m,m,m] in specified epsg, the geo mid-point of the Wind Farm.")());
    add_proxy_property(
      w, "epsg", wind_farm, epsg, doc.intro("_int: [] epsg spec for the location attribute of the Wind Farm,.")());
    add_proxy_property(
      w,
      "power_curve",
      wind_farm,
      power_curve,
      doc.intro("_t_xy_: [x=m/s,y=W] the time-dependent power-curve of the Wind Farm.")());
    {
      auto cost = expose_attributes_type<wind_farm::cost_>(
        w, "Cost", doc.intro("WindFarm.cost related attributes, tactical and factual.")());
      _add_proxy_property(
        cost,
        "break_even",
        wind_farm::cost_,
        break_even,
        doc.intro("_ts: [money/J] minimum price to break-even, also known as cutoff-price time series.")());
    }
    w.def_readonly("cost", &wind_farm::cost, "Cost: Cost attributes.");
  }
}
