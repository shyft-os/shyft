#include <array>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

#include <boost/describe/enumerators.hpp>
#include <fmt/core.h>
#include <fmt/format.h>

#include <shyft/core/reflection.h>
#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/srv/application/client.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>
#include <shyft/energy_market/stm/srv/application/server.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/srv/application.h>

namespace shyft::energy_market::stm::srv::experimental::application {

  void pyexport(py::module_ &m) {
    auto client = py::class_<py_client>(m, "ApplicationClient")
      .def(py::init<std::string, int, int>(),py::arg("host_port"), py::arg("timeout_ms"),py::arg("operation_timeout_ms")=0);

    for_each_enum<message_tag>([&](auto e) {
      using request_type = protocols::request<application_message<e.value>>;
      [&]<typename... M>(std::type_identity<std::tuple<M...>>) {
        auto py_send = +[](py_client &client, member_type_t<M::pointer>... args) {
          const auto rep = client.send(request_type{std::move(args)...});
          return std::apply(
            [](auto &&...args) {
              return std::make_tuple(SHYFT_FWD(args)...);
            },
            members(rep));
        };
        client.def(e.name, py_send, py::arg(M::name)...);
      }(std::type_identity<members_of<request_type>>{});
    });

    auto const callbacks_arg_names = []<typename... M>(std::type_identity<std::tuple<M...>>) {
      return std::array{fmt::format("{}_callback", M::name)...};
    }(std::type_identity<enums_of<model_tag>>{});

    auto _server =
      py::class_<server>(m, "ApplicationServer")
        .def(
          py::init(
            +[](std::string const &ip, int port, py::tuple callbacks) {
              auto out = new server{};
              out->set_listening_ip(ip);
              out->set_listening_port(port);
              // [&]<typename Tags, auto... I>(std::type_identity<Tags>, std::index_sequence<I...>) {
              //   (pyapi::assign_function(
              //      out->manager.callback<std::tuple_element_t<I, Tags>::value>(), boost::python::object{callbacks[I]}),
              //    ...);
              // }(std::type_identity<enums_of<model_tag>>{}, std::make_index_sequence<enumerator_count<model_tag>>());
              return out;
            }),
          doc.parameters()
            .parameter("ip", "str", "Listening IP")
            .parameter("port", "int", "Listening port")
            .parameter("callbacks", fmt::format("{}", fmt::join(callbacks_arg_names, ", ")), "Model callbacks")(),
          py::arg("ip"), py::arg("port"), py::arg("callbacks"))
        .def("start_server", &server::start_server)
        .def(
          "stop_server",
          +[](server &server, int ms) {
            pyapi::scoped_gil_release gil;
            if (ms > 0)
              server.set_graceful_close_timeout(ms);
            server.clear();
          },
          py::arg("timeout") = 1000)
        .def("close", &server::clear);
  };
}
