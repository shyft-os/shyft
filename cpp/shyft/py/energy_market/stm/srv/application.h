#pragma once

#include <mutex>
#include <string>

#include <shyft/py/bindings.h>
#include <shyft/energy_market/stm/srv/application/client.h>
#include <shyft/energy_market/stm/srv/application/protocol.h>

namespace shyft::energy_market::stm::srv::experimental::application {

  struct py_client {

    py_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms)
      : impl{{.connection=core::srv_connection{host_port, timeout_ms, operation_timeout_ms}}} {
    }

    template <auto message>
    protocols::reply<message> send(protocols::request<message> const &req) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lock(mutex);
      return impl.send(std::move(req));
    }

   private:
    std::mutex mutex;
    client impl;
  };
}
