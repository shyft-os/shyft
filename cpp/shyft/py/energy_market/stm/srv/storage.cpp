/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/py/bindings.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/srv/db.h>

namespace shyft::srv {

  /**
   * @brief receive_patch
   * @details
   * The client provides customisation for receiving models.
   * Fx to be applied after client receives a stm_system,
   * in this case fix_uplinks, so they are pointing to stm_system.
   * This ensure that the model do have uplinks for all child objects.
   *
   */
  template <>
  struct receive_patch<energy_market::stm::stm_system> {
    static void apply(std::shared_ptr<energy_market::stm::stm_system> const &m) {
      energy_market::stm::stm_system::fix_uplinks(m);
    }
  };
}

namespace shyft::energy_market::stm::srv::storage {
  void hps_client_server(py::module_ &m) {
    using model = stm::stm_hps;
    using client_t = energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;

    energy_market::export_client<client_t>(
      m, "HpsClient", "The client api for the hydro-power-system repostory server.");
    energy_market::export_server<srv_t>(
      m, "HpsServer", "The server-side component for the hydro-power-system model repository.");
  }

  void stm_client_server(py::module_ &m) {
    using model = stm::stm_system;
    using client_t = energy_market::py_client<shyft::srv::client<model>>;
    using srv_t = energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;

    energy_market::export_client<client_t>(
      m, "StmClient", "The client api for the stm repository server.");
    energy_market::export_server<srv_t>(
      m, "StmServer", "The server-side component for the stm energy_market model repository.");
  }

  void pyexport(py::module_ &m) {
    hps_client_server(m);
    stm_client_server(m);
  }
}
