#pragma once

#include <cstdint>
#include <memory>
#include <mutex>
#include <string>

#include <shyft/energy_market/stm/srv/task/client.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/py/energy_market/model_client_server.h>
#include <shyft/web_api/energy_market/stm/task/request_handler.h>

namespace shyft::energy_market::stm::srv {

  struct py_task_client : py_client<task::client> {
    py_task_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms)
      : py_client{host_port, timeout_ms,operation_timeout_ms} {
    }

    auto add_case(std::int64_t mid, std::shared_ptr<stm_case> const &run) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      impl.add_case(mid, run);
    }

    auto remove_case(std::int64_t mid, std::int64_t rid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.remove_case(mid, rid);
    }

    auto remove_case(std::int64_t mid, std::string const &rname) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.remove_case(mid, rname);
    }

    auto get_case(std::int64_t mid, std::int64_t rid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.get_case(mid, rid);
    }

    std::shared_ptr<stm_case> get_case(std::int64_t mid, string const &rname) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.get_case(mid, rname);
    }

    auto update_case(std::int64_t mid, stm_case const &ce) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.update_case(mid, ce);
    }

    auto add_model_ref(std::int64_t mid, std::int64_t rid, model_ref_ const &mr) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.add_model_ref(mid, rid, mr);
    }

    auto update_model_ref(std::int64_t mid, std::int64_t rid, model_ref_ const &mr) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.update_model_ref(mid, rid, mr);
    }

    auto remove_model_ref(std::int64_t mid, std::int64_t rid, string const &mkey) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.remove_model_ref(mid, rid, mkey);
    }

    auto get_model_ref(std::int64_t mid, std::int64_t rid, string const &mkey) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.get_model_ref(mid, rid, mkey);
    }

    auto fx(std::int64_t mid, string fx_args) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck{mx};
      return impl.fx(mid, fx_args);
    }
  };

  using py_task_server =
    py_server_with_web_api<task::server, web_api::energy_market::stm::task::request_handler>;

}
