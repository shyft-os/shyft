#include <cstdint>
#include <memory>
#include <vector>

#include <shyft/energy_market/stm/srv/task/client.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/srv/task.h>
#include <shyft/web_api/energy_market/stm/task/request_handler.h>

PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::energy_market::stm::srv::model_ref>>);

namespace shyft::energy_market::stm::srv::task {

  void pyexport(py::module_ &m) {
    // Run info
    auto mr =
      py::class_<model_ref, std::shared_ptr<model_ref>>(
        m, "StmModelRef", doc.intro("Reference to a model, and where to find it.")())
        .def(py::init())
        .def(
          py::init<string, int, int, string, std::int64_t>(),
          doc.intro("Create a run info.")
            .parameters()
            .parameter("host", "str", "Where the referenced model is stored.")
            .parameter("port_num", "int", "At what port number to interface with the server.")
            .parameter("api_port_num", "int", "At what port number to interface with the server using the web API.")
            .parameter("model_key", "str", "The model key the referenced model is stored under.")
            .parameter("stm_key", "int", "If >0, then the stm model id of the persisted StmSystem")(),
          py::arg("host"),
          py::arg("port_num"),
          py::arg("api_port_num"),
          py::arg("model_key"),
          py::arg("stm_key") = 0)
        .def(py::init(), doc.intro("Default constructor for StmRunInfo.")())
        .def_readwrite("host", &model_ref::host, "str: Where model is stored.")
        .def_readwrite("port_num", &model_ref::port_num, "int: Port number to interface with server.")
        .def_readwrite(
          "api_port_num", &model_ref::api_port_num, "int: Port number to interface with server through web API.")
        .def_readwrite("model_key", &model_ref::model_key, "str: The model key the referenced model is stored under.")
        .def_readwrite("stm_id", &model_ref::stm_id, "int: If >0 The id of the persisted StmSystem")
        .def_readwrite("labels", &model_ref::labels, "StringVector: A set of labels for the model.")
        .def(py::self == py::self)
        .def(py::self != py::self);
    auto model_ref_str = [](model_ref const &o) {
      return fmt::format(
        "StmModelRef(host='{}', port_num={}, api_port_num={}, model_key='{}', stm_key={})",
        o.host,
        o.port_num,
        o.api_port_num,
        o.model_key,
        o.stm_id);
    };
    mr.def("__str__", model_ref_str);
    mr.def("__repr__", model_ref_str);

    auto mrv = pyapi::bind_vector<std::vector<std::shared_ptr<model_ref>>>(m, "ModelRefList")
                 .def(py::self == py::self)
                 .def(py::self != py::self);
    auto model_ref_list_str = [&](std::vector<std::shared_ptr<model_ref>> const &o) {
      auto r = fmt::format("ModelRefList([");
      bool add_comma = false;
      for (auto &m : o) {
        if (add_comma)
          r += ", ";
        r += (m != nullptr ? model_ref_str(*m) : std::string("null"));
        add_comma = true;
      }
      r += "])";
      return r;
    };
    mrv.def("__str__", model_ref_list_str);
    mrv.def("__repr__", model_ref_list_str);

    auto sc =
      py::class_<stm_case, std::shared_ptr<stm_case>>(
        m,
        "StmCase",
        doc.intro("Provided a case concept for Stm. A case, can hold a number of references to stm model runs. A "
                  "StmTask contains many cases, each with a set of stm model runs.")())
        .def(py::init())
        .def(
          py::init<int64_t, string, utctime, string, std::vector<string>, std::vector<std::shared_ptr<model_ref>>>(),
          py::arg("id"),
          py::arg("name"),
          py::arg("created"),
          py::arg("json") = string{""},
          py::arg("labels") = std::vector<string>(),
          py::arg("model_refs") = std::vector<std::shared_ptr<model_ref>>())
        .def_readwrite("id", &stm_case::id, "int: The unique case ID.")
        .def_readwrite("name", &stm_case::name, "str: Any useful name of description.")
        .def_readwrite("created", &stm_case::created, "time: The time of creation, or last modification.")
        .def_readwrite("json", &stm_case::json, "str: A json formatted string with miscellaneous data.")
        .def_readonly(
          "model_refs",
          &stm_case::model_refs,
          "ModelRefList: Set of stm run model references that are related to the case.")
        .def(py::self == py::self)
        .def(py::self != py::self)
        .def(
          "add_model_ref",
          &stm_case::add_model_ref,
          doc.intro("Add a model reference to the case.")
            .parameters()
            .parameter("mr", "StmModelRef", "The model reference to add.")(),
          py::arg("mr"))
        .def(
          "update_model_ref",
          &stm_case::update_model_ref,
          doc.intro("Update model reference, using the model_key as reference.")
            .parameters()
            .parameter("mr", "StmModelRef", "The model reference to update.")(),
          py::arg("mr"))
        .def(
          "remove_model_ref",
          &stm_case::remove_model_ref,
          doc.intro("Remove a stm run model reference from the case.")
            .parameters()
            .parameter("mkey", "str", "The stm run model key the reference has stored. Cf. StmModelRef.model_key.")
            .returns(
              "success",
              "Boolean",
              "True if model reference was successfully removed. False if case did not contain model reference with "
              "given model key.")(),
          py::arg("mkey"))
        .def(
          "get_model_ref",
          &stm_case::get_model_ref,
          doc.intro("Get model reference in the case based on model key.")
            .parameters()
            .parameter("mkey", "str", "The model key the reference has stored. Cf. StmModelRef.model_key.")
            .returns("mr", "StmModelRef", "Requested model reference. None if not found in the case.")(),
          py::arg("mkey"));

    sc.def_readwrite("labels", &stm_case::labels);
    sc.def_readwrite("model_refs", &stm_case::model_refs);

    // Expose StmTask:
    auto st =
      py::class_<stm_task, std::shared_ptr<stm_task>>(
        m,
        "StmTask",
        doc
          .intro("Task concept for Stm. Can contain a number of cases, each of them can contain one or more stm "
                 "model-run references.")
          .intro("Defined through its set of labels, a base model, which should relate to child runs and model_refs,")
          .intro("and a task name.")())
        .def(py::init())
        .def(
          py::init<
            int64_t,
            string,
            utctime,
            string,
            std::vector<string>,
            std::vector<std::shared_ptr<stm_case>>,
            model_ref,
            string>(),
          py::arg("id"),
          py::arg("name"),
          py::arg("created"),
          py::arg("json") = string{""},
          py::arg("labels") = std::vector<string>(),
          py::arg("cases") = std::vector<std::shared_ptr<stm_case>>(),
          py::arg("base_model") = model_ref(),
          py::arg("task_name") = string{""})
        .def_readwrite("id", &stm_task::id, "int: The unique task ID.")
        .def_readwrite("name", &stm_task::name, "str: Any useful name or description.")
        .def_readwrite("created", &stm_task::created, "time: The time of creation, or last modification.")
        .def_readwrite("json", &stm_task::json, "str: A json formatted string with miscellaneous data.")
        .def_readonly("cases", &stm_task::cases, "StmCaseVector: Set of runs connected to task.")
        .def_readwrite(
          "base_model", &stm_task::base_mdl, "StmModelRef: Base model, which should correspond to models used in runs.")
        .def_readwrite("task_name", &stm_task::task_name, "str: Name of task connected to task.")
        .def(py::self == py::self)
        .def(py::self != py::self)
        .def(
          "add_case",
          &stm_task::add_case,
          doc.intro("Add a case (set of stm model runs) to the task.")
            .parameters()
            .parameter("case", "StmCase", "The case instance to add to the task.")
            .returns("None", "None", "Returns nothing.")(),
          py::arg("case"))
        .def(
          "remove_case",
          static_cast<bool (stm_task::*)(int64_t)>(&stm_task::remove_case),
          doc.intro("Remove a case from task based on id.")(),
          py::arg("id"))
        .def(
          "remove_case",
          static_cast<bool (stm_task::*)(string const &)>(&stm_task::remove_case),
          doc.intro("Remove a case from task based on name.")(),
          py::arg("name"))
        .def(
          "get_case",
          static_cast<std::shared_ptr<stm_case> (stm_task::*)(int64_t)>(&stm_task::get_case),
          doc.intro("Get run from task based on ID.")
            .parameters()
            .parameter("cid", "int", "Id of case you want to get.")
            .returns("case", "StmCase", "Request case, None if not found.")(),
          py::arg("cid"))
        .def(
          "get_case",
          static_cast<std::shared_ptr<stm_case> (stm_task::*)(string const &)>(&stm_task::get_case),
          doc.intro("Get case based on its name.")
            .parameters()
            .parameter("rname", "str", "Name of case you want to get.")
            .returns("case", "StmCase", "Request case, None if not found.")(),
          py::arg("rname"))
        .def(
          "update_case",
          static_cast<bool (stm_task::*)(stm_case const &)>(&stm_task::update_case),
          doc.intro("Update case inplace").parameters().parameter("case", "StmCase", "The case to update inplace.")(),
          py::arg("case"));

    st.def_readwrite("labels", &stm_task::labels);

    auto session_client = export_client<py_task_client>(
      m, "StmTaskClient", "The client api for the Stm Task repository.");
    session_client
      .def(
        "add_case",
        &py_task_client::add_case,
        doc.intro("Add a case to a task on server.")
          .parameters()
          .parameter("mid", "int", "Task id to add a run to.")
          .parameter("case", "StmCase", "The case to add to the task.")
          .raises()
          .raise("RuntimeError", "If provided mid is an invalid task id.")
          .raise("RuntimeError", "If provided case has the same id or name as a case already in task.")(),
        py::arg("mid"),
        py::arg("case"))
      .def(
        "remove_case",
        static_cast< bool (py_task_client::*)(int64_t, int64_t) >(&py_task_client::remove_case),
        doc.intro("Remove a case from a task on server.")
          .parameters()
          .parameter("mid", "int", "Task id to remove run from.")
          .parameter("cid", "int", "Id of case to remove.")
          .returns("success", "Boolean", "True if the case was successfully removed from task.")(),
        py::arg("mid"),
        py::arg("cid"))
      .def(
        "remove_case",
        static_cast< bool (py_task_client::*)(int64_t, string const &) >(&py_task_client::remove_case),
        doc.intro("Remove case from a task on server based on it's case name.")
          .parameters()
          .parameter("mid", "int", "Task id to remove run from.")
          .parameter("cname", "str", "Name of the case to remove.")
          .returns("success", "Boolean", "True if case was successfully removed from task.")(),
        py::arg("mid"),
        py::arg("rname"))
      .def(
        "get_case",
        static_cast< std::shared_ptr<stm_case> (py_task_client::*)(int64_t, int64_t)>(&py_task_client::get_case),
        doc.intro("Get case from task based on case's id.")
          .parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("cid", "int", "Case ID.")
          .returns("case", "StmCase", "Requested case, or None if not found.")(),
        py::arg("mid"),
        py::arg("cid"))
      .def(
        "get_case",
        static_cast< std::shared_ptr<stm_case> (py_task_client::*)(int64_t, string const &)>(&py_task_client::get_case),
        doc.intro("Get from task based on case's name.")
          .parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("cname", "str", "Case name.")
          .parameter("case", "StmCase", "Requested run, or None if not found.")(),
        py::arg("mid"),
        py::arg("cname"))
      .def(
        "update_case",
        static_cast< void (py_task_client::*)(int64_t, stm_case const &)>(&py_task_client::update_case),
        doc.parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("case", "StmCase", "Case to be updated inplace within task.")(),
        py::arg("mid"),
        py::arg("case"))
      .def(
        "add_model_ref",
        &py_task_client::add_model_ref,
        doc.intro("Add a model reference to a case on server.")
          .parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("cid", "int", "Case ID.")
          .parameter(
            "mr", "StmModelRef", "The model reference to add to case with ID=cid, contained in task with ID=mid.")(),
        py::arg("mid"),
        py::arg("cid"),
        py::arg("mr"))
      .def(
        "update_model_ref",
        &py_task_client::update_model_ref,
        doc.intro("Update a model reference in a case on server.")
          .parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("cid", "int", "Case ID.")
          .parameter(
            "mr",
            "StmModelRef",
            "The model reference to update to case with model_key, ID=cid, contained in task with ID=mid.")(),
        py::arg("mid"),
        py::arg("cid"),
        py::arg("mr"))
      .def(
        "remove_model_ref",
        &py_task_client::remove_model_ref,
        doc.intro("Remove a model reference from a case contained in a task.")
          .parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("cid", "int", "Case ID.")
          .parameter("mkey", "str", "Model key of model reference. What is stored in StmModelRef.model_key.")
          .returns("success", "Boolean", "True if model reference was successfully removed, False otherwise.")(),
        py::arg("mid"),
        py::arg("cid"),
        py::arg("mkey"))
      .def(
        "get_model_ref",
        &py_task_client::get_model_ref,
        doc.intro("Get a model reference stored in a case contained in a task.")
          .parameters()
          .parameter("mid", "int", "Task ID.")
          .parameter("cid", "int", "Case ID.")
          .parameter("mkey", "str", "Model key of model reference. What is stored in StmModelRef.model_key.")
          .returns(
            "mr",
            "StmModelRef",
            "Requested model reference. None if run wasn't found or didn't contain requested model reference.")(),
        py::arg("mid"),
        py::arg("cid"),
        py::arg("mkey"))
      .def(
        "fx",
        &py_task_client::fx,
        doc.intro("Execute the serverside fx, passing supplied arguments.")
          .parameters()
          .parameter("mid", "str", "ID of task for the server-side fx.")
          .parameter("fx_arg", "str", "Any argument passed to the server-side fx.")
          .returns("success", "bool", "true if call successfully done.")(),
        py::arg("mid"),
        py::arg("fx_arg"));

    auto py_srv = export_server_with_web_api<py_task_server>(
      m, "StmTaskServer", "The server-side component for the STM Task repository.");
    py_srv.def_property(
      "fx",
      [](py_task_server const &s) {
        return s.impl.callback;
      },
      [](py_task_server &s, std::function<bool(std::int64_t, std::string)> f) {
        s.impl.callback = pyapi::wrap_callback(std::move(f));
      },
      doc.intro("Callable[[str,str],bool]: server-side callable function(lambda) that takes two parameters:")
        .intro("mid :  the model id")
        .intro("fx_arg: arbitrary string to pass to the server-side function")
        .intro("The server-side fx is called when the client (or web-api) invoke the c.fx(mid,fx_arg).")
        .intro("The signature of the callback function should be (mid:str, fx_arg:str) -> bool")
        .intro("This feature is simply enabling the users to tailor server-side functionality in python!")
        .intro("\nExamples:\n")
        .intro(">>> from shyft.energy_market.stm import StmTaskServer\n"
               ">>> s=StmTaskServer()\n"
               ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
               ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
               ">>>   # note we can use captured Server s here!"
               ">>>     return True\n"
               ">>> # and then bind the function to the callback\n"
               ">>> s.fx=my_fx\n"
               ">>> s.start_server()\n"
               ">>> : # later using client from anywhere to invoke the call\n"
               ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n")());
  }
}
