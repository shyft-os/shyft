#include <mutex>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <boost/thread/locks.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/reflection.h>
#include <shyft/srv/db.h>
#include <shyft/web_api/energy_market/request_handler.h>

namespace shyft::energy_market::stm::srv::dstm {

  struct py_client {
    std::mutex mx; ///< to enforce just one thread active on this client object at a time
    client impl;

    py_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms = 0)
      : impl{{core::srv_connection{host_port, timeout_ms, operation_timeout_ms}}} {
    }

    ~py_client() = default;

    std::string get_host_port() {
      return impl.connection.host_port;
    }

    [[nodiscard]] int get_timeout_ms() const {
      return impl.connection.timeout_ms;
    }

    int get_operation_timeout_ms() {
      return impl.connection.operation_timeout_ms;
    }

    void set_operation_timeout_ms(int operation_timeout_ms) {
      impl.connection.operation_timeout_ms = operation_timeout_ms;
    }

    [[nodiscard]] bool is_open() const {
      return impl.connection.is_open;
    }

    [[nodiscard]] size_t get_reconnect_count() const {
      return impl.connection.reconnect_count;
    }

    void close() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.close();
    }

    [[nodiscard]] std::string get_server_version() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_version_info().version;
    }

    [[nodiscard]] bool create_model(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.create_model(mid);
    }

    [[nodiscard]] bool add_model(std::string const &mid, stm_system_ const &mdl) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.add_model({mid, mdl}).status;
    }

    [[nodiscard]] bool set_model(std::string const &mid, stm_system_ const &mdl) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.set_model({mid, mdl}).status;
    }

    [[nodiscard]] bool remove_model(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.remove_model({mid}).status;
    }

    [[nodiscard]] bool clone_model(std::string const &old_mid, std::string const &new_mid, bool sparse) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.clone_model({old_mid, new_mid, sparse}).status;
    }

    [[nodiscard]] bool rename_model(std::string const &old_mid, std::string const &new_mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.rename_model(old_mid, new_mid);
    }

    [[nodiscard]] std::vector<std::string> get_model_ids() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_model_ids();
    }

    [[nodiscard]] auto get_model_infos() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_model_infos().infos;
    }

    [[nodiscard]] stm_system_ get_model(std::string const &mid, bool const stripped) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return stripped ? impl.get_model_stripped({mid}).model : impl.get_model({mid}).model;
    }

    [[nodiscard]] bool optimize(
      std::string const &mid,
      generic_dt const &ta,
      std::vector<shop::shop_command> const &cmd,
      bool opt_only) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.optimize({mid, ta, cmd, opt_only}).status;
    }

    [[nodiscard]] bool start_tune(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock<std::mutex> lck(mx);
      return impl.start_tune({mid}).status;
    }

    [[nodiscard]] bool tune(std::string const &mid, generic_dt const &ta, std::vector<shop::shop_command> const &cmd) {
      pyapi::scoped_gil_release gil;
      std::unique_lock<std::mutex> lck(mx);
      return impl.tune({mid, ta, cmd}).status;
    }

    [[nodiscard]] bool stop_tune(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock<std::mutex> lck(mx);
      return impl.stop_tune({mid}).status;
    }

    [[nodiscard]] std::vector<log_entry> get_log(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_log({mid}).log;
    }

    [[nodiscard]] model_state get_state(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_state({mid}).state;
    }

    [[nodiscard]] auto evaluate_ts(
      ats_vector const &tsv,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.evaluate_ts({tsv, bind_period, use_ts_cached_read, update_ts_cache, clip_period}).time_series;
    }

    [[nodiscard]] bool evaluate_model(
      std::string const &mid,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.evaluate_model({mid, bind_period, use_ts_cached_read, update_ts_cache, clip_period}).status;
    }

    [[nodiscard]] bool fx(std::string const &mid, std::string const &fx_arg) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.fx({mid, fx_arg}).status;
    }

    [[nodiscard]] ats_vector get_ts(std::string const &mid, std::vector<std::string> const &ts_urls) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_ts({mid, ts_urls}).time_series;
    }

    void set_ts(std::string const &mid, ats_vector const &tsv) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.set_ts({mid, tsv});
    }

    [[nodiscard]] auto add_compute_server(std::string const &host_port) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.add_compute_server({host_port}).status;
    }

    [[nodiscard]] auto set_attrs(std::vector<std::pair<std::string, any_attr>> const &attrs) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.set_attrs({attrs}).attrs;
    }

    [[nodiscard]] auto get_attrs(std::vector<std::string> const &attrs) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_attrs({attrs}).attrs;
    }

    [[nodiscard]] bool reset_model(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.reset_model({mid}).status;
    }

    [[nodiscard]] bool patch(std::string const &mid, stm::stm_patch_op op, stm::stm_system_ const &p) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.patch_model({mid, op, p}).status;
    }

    [[nodiscard]] auto compute_server_status() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.compute_server_status().status;
    }

    void cache_flush() {
      pyapi::scoped_gil_release gil;
      impl.cache_flush({});
    }

    [[nodiscard]] dtss::cache_stats cache_stats() {
      pyapi::scoped_gil_release gil;
      return impl.cache_stats({}).cache_stats;
    }
  };

  /** @brief  Server implementation
   *
   *  TODO: consider use server as impl instead of inheritance, and use pyapi::scoped_gil for all python exposed calls
   */
  struct py_server final : server {
    using request_handler = web_api::energy_market::request_handler;

    request_handler bg_server; ///< this object handle the requests from the web-api
    std::future<int> web_srv;  ///< mutex

    std::string web_api_ip; ///< Listening IP for web_api

    void add_container(std::string const &container_name, std::string const &root_dir) const {
      dtss::add_container(*dispatch.state->dtss, container_name, root_dir);
    }

    [[nodiscard]] auto get_version_info() {
      return dstm::get_version_info(*this).version;
    }

    void set_master(
      std::string const &ip,
      int const port,
      double const master_poll_time,
      std::size_t const unsubscribe_min_threshold,
      double const unsubscribe_max_delay) const {
      dtss::set_master(
        *dispatch.state->dtss, ip, port, master_poll_time, unsubscribe_min_threshold, unsubscribe_max_delay);
    }

    void clear_master() const {
      dtss::clear_master(*dispatch.state->dtss);
    }

    void py_notify_change(std::vector<std::string> const &urls) const {
      pyapi::scoped_gil_release gil;
      if (urls.empty())
        return;
      dispatch.state->sm->notify_change(urls);
    }

    explicit py_server(compute::janitor_config j_config = {}, shyft::srv::fast_server_iostream_config cfg = {})
      : dstm::server{server_dispatch{std::make_unique<server_state>(std::move(j_config))}, std::move(cfg)}
      , bg_server{dispatch.state->models, dispatch.state->sm, dispatch.state->callback, dispatch.state->dtss.get()} {
    }

    ~py_server() final {
      if (web_srv.valid())
        web_srv.wait();
    }

    [[nodiscard]] int start_web_api(
      std::string const &host_ip,
      int const port,
      std::string const &doc_root,
      int const fg_threads,
      int const bg_threads,
      bool const tls_only) {
      pyapi::scoped_gil_release gil;
      if (!web_srv.valid()) {
        web_api_ip = host_ip;
        web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads, tls_only]() {
          return shyft::web_api::run_web_server(
            bg_server,
            host_ip,
            static_cast<unsigned short>(port),
            std::make_shared<std::string>(doc_root),
            fg_threads,
            bg_threads,
            tls_only // accept_plain == !tls_only
          );
        });
      }
      return static_cast<int>(bg_server.wait_for_real_port());
    }

    void stop_web_api() {
      pyapi::scoped_gil_release gil;
      bg_server.running = false;
      if (web_srv.valid()) {
        (void) web_srv.get();
      }
    }

    [[nodiscard]] int get_web_api_port() const {
      if (web_srv.valid()) {
        return static_cast<int>(bg_server.real_port);
      }
      return -1;
    }

    [[nodiscard]] std::string get_web_api_ip() const {
      if (web_srv.valid()) {
        return web_api_ip;
      } else {
        return "";
      }
    }

    [[nodiscard]] auto py_optimize(
      std::string const &mid,
      generic_dt const &ta,
      std::vector<shop::shop_command> const &cmd,
      bool const opt_only) {
      pyapi::scoped_gil_release gil;
      return optimize(*this, mid, ta, cmd, opt_only).status;
    }

    [[nodiscard]] auto py_evaluate_model(
      std::string const &mid,
      utcperiod const bind_period,
      bool const use_ts_cached_read,
      bool const update_ts_cache,
      utcperiod const clip_period) {
      pyapi::scoped_gil_release gil;
      return evaluate_model(*this, mid, bind_period, use_ts_cached_read, update_ts_cache, clip_period).status;
    }

    [[nodiscard]] auto py_clone_model(std::string const &old_mid, std::string const &new_mid, bool const sparse) {
      pyapi::scoped_gil_release gil;
      return clone_model(*this, old_mid, new_mid, sparse).status;
    }

    [[nodiscard]] auto py_remove_model(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      return remove_model(*this, mid).status;
    }

    [[nodiscard]] auto py_get_model(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      return get_model(*this, mid).model;
    }

    [[nodiscard]] auto rename_model(std::string const &old_model_id, std::string const &new_model_id) {
      pyapi::scoped_gil_release gil;
      return clone_model(*this, old_model_id, new_model_id).status && remove_model(*this, old_model_id).status;
    }

    [[nodiscard]] auto py_get_state(std::string const &mid) {
      pyapi::scoped_gil_release gil;
      return get_state(*this, mid).state;
    }

    [[nodiscard]] auto get_model_ids() {
      pyapi::scoped_gil_release gil;
      std::vector<std::string> model_ids;
      std::ranges::copy(std::views::keys(dstm::get_model_infos(*this).infos), std::back_inserter(model_ids));
      return model_ids;
    }

    [[nodiscard]] auto py_get_model_infos() {
      pyapi::scoped_gil_release gil;
      return get_model_infos(*this).infos;
    }

    [[nodiscard]] auto py_add_model(std::string const &mid, stm_system_ const &mdl) {
      pyapi::scoped_gil_release gil;
      return add_model(*this, mid, mdl).status;
    }

    [[nodiscard]] auto py_create_model(std::string const &mid) {
      return py_add_model(mid, nullptr);
    }

    [[nodiscard]] auto add_compute_server(std::string const &addr) {
      pyapi::scoped_gil_release gil;
      return dstm::add_compute_server(*this, addr).status;
    }

    void stop_server(int const ms) {
      pyapi::scoped_gil_release gil;
      if (ms > 0)
        set_graceful_close_timeout(ms);
      clear();
    }

    [[nodiscard]] auto
      py_apply(std::string const &mid, std::function<py::object(std::shared_ptr<stm_system>)> const &action) {
      pyapi::scoped_gil_release gil;
      return dispatch.state->models
        .mutate_or_throw(
          mid,
          [&](auto &&view) {
            return action(view.model);
          })
        .get();
    }

    // provide fx to dtss internals to inspect/control cache size
    [[nodiscard]] auto get_cache_stats() const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.get_cache_stats();
    }

    void clear_cache_stats() const {
      pyapi::scoped_gil_release gil;
      dispatch.state->dtss->ts_cache.clear_cache_stats();
    }

    [[nodiscard]] auto get_cache_size() const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.get_capacity();
    }

    void set_cache_size(std::int64_t const v) const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.set_capacity(v);
    }

    [[nodiscard]] auto get_ts_size() const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.get_ts_size();
    }

    void set_ts_size(std::int64_t v) const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.set_ts_size(v);
    }

    [[nodiscard]] auto get_cache_memory_target_size() const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.get_mem_max();
    }

    void set_cache_memory_target_size(std::int64_t const v) const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.set_mem_max(v);
    }

    void remove_from_cache(std::vector<std::string> const &v) const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.remove(v);
    }

    void flush_cache() const {
      pyapi::scoped_gil_release gil;
      return dispatch.state->dtss->ts_cache.flush();
    }
  };

  void pyexport(py::module_ &m) {
    auto py_janitor_config =
      py::class_<compute::janitor_config>(
        m,
        "JanitorConfig",
        doc.intro("A janitor config")
          .intro("The `duration` member determines how often to check for stale connection.")
          .intro("The `stale_threshold` is the threshold where no activity leads to server-side close of socket.")())
        .def(py::init());
    pyapi::expose_members(py_janitor_config);

    py_janitor_config.def(
      py::init([](utctime const duration, utctime const stale_threshold) {
        return compute::janitor_config{.duration = duration, .stale_threshold = stale_threshold};
      }),
      py::arg("duration"),
      py::arg("stale_threshold"));

    auto janitor_str = [](compute::janitor_config const &o) {
      return fmt::format("JanitorConfig(duration={}, stale_threshold={})", o.duration, o.stale_threshold);
    };
    py_janitor_config.def("__str__", janitor_str);
    py_janitor_config.def("__repr__", janitor_str);

    auto py_dstm =
      py::class_<py_server>(
        m,
        "DStmServer",
        doc.intro(
          "A server object serving distributed, 'live' STM systems.\n"
          "The server contains an DTSS that handles time series for the models stored in the server.")())
        .def(
          py::init<compute::janitor_config, shyft::srv::fast_server_iostream_config>(),
          py::arg("j_config") = compute::janitor_config{},
          py::arg("config") = shyft::srv::fast_server_iostream_config{})
        .def("start_server", &py_server::start_server)
        .def(
          "set_listening_port",
          &py_server::set_listening_port,
          doc.intro("set the listening port for the service")
            .parameters()
            .parameter("port_no", "int", "a valid and available tcp-ip port number to listen on.")
            .paramcont("typically it could be 20000 (avoid using official reserved numbers)")
            .returns("nothing", "None", "")(),
          py::arg("port_no"))
        .def(
          "add_container",
          &py_server::add_container,
          doc.intro("Add a container to the server's DTSS.")
            .parameters()
            .parameter("container_name", "str", "name of container to create.")
            .parameter("root_dir", "str", "Directory where container's time series are stored.")
            .returns("nothing", "None", "")(),
          py::arg("container_name"),
          py::arg("root_dir"))
        .def(
          "get_listening_port",
          &py_server::get_listening_port,
          "returns the port number it's listening at for serving incoming request")
        .def(
          "get_listening_ip",
          &py_server::get_listening_ip,
          doc.intro("return the ip adress the server is listening on for serving incoming requests")())
        .def(
          "set_listening_ip",
          &py_server::set_listening_ip,
          doc.intro("set the listening port for the service")
            .parameters()
            .parameter("ip", "str", "ip or host-name to start listening on")
            .returns("nothing", "None", "")(),
          py::arg("ip"))
        .def("get_version_info", &py_server::get_version_info, "returns the version number of the DStmServer")
        .def(
          "set_master_slave_mode",
          &py_server::set_master,
          doc
            .intro(
              "Set master-slave mode, redirecting all IO calls on this dtss to the master ip:port dtss.\n"
              "This instance of the dtss is kept in sync with changes done on the master using subscription to "
              "changes on the master\n"
              "Calculations, and caches are still done locally unloading the computational efforts from the "
              "master.")
            .parameters()
            .parameter("ip", "str", "The ip address where the master dtss is running")
            .parameter("port", "int", "The port number for the master dtss")
            .parameter("master_poll_time", "time", "[s] max time between each update from master, typicall 0.1 s is ok")
            .parameter(
              "unsubscribe_threshold",
              "int",
              "minimum number of unsubscribed time-series before also unsubscribing from the master")
            .parameter("unsubscribe_max_delay", "time", "maximum time to delay unsubscriptions, regardless number")(),
          py::arg("ip"),
          py::arg("port"),
          py::arg("master_poll_time"),
          py::arg("unsubscribe_threshold"),
          py::arg("unsubscribe_max_delay"))
        .def(
          "clear_master_slave",
          &py_server::clear_master,
          doc.intro("Clear master-slave mode if set, cleanly shut down connection to master dtss.")())
        .def(
          "create_model",
          &py_server::py_create_model,
          doc.intro("Create a new model")
            .parameters()
            .parameter("mid", "str", "ID of new model")
            .returns("mdl", "StmSystem", "Empty model with ID set to 'mid'")
            .raises()
            .raise("RuntimeError", "If 'mid' already is ID of a model")(),
          py::arg("mid"))
        .def(
          "add_model",
          &py_server::py_add_model,
          doc.intro("Add model to server")
            .parameters()
            .parameter("mid", "str", "ID/key to store model as.")
            .parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
            .returns("success", "bool", "Returns True on success")
            .raises()
            .raise("RuntimeError", "If 'mid' is already an ID of a model.")(),
          py::arg("mid"),
          py::arg("mdl"))
        .def(
          "get_model",
          &py_server::py_get_model,
          (py::arg("self"), py::arg("mid")),
          doc.intro("Get model from server")
            .parameters()
            .parameter("mid", "str", "ID/key to store model as.")
            .returns("mdl", "StmSystem", "Found system")())
        .def(
          "remove_model",
          &py_server::py_remove_model,
          doc.intro("Remove model by ID.")
            .parameters()
            .parameter("mid", "str", "ID of model to remove.")
            .returns("success", "bool", "Returns True on success.")
            .raises()
            .raise("RuntimeError", "If model with ID 'mid' does not exist.")(),
          py::arg("mid"))
        .def(
          "rename_model",
          &py_server::rename_model,
          doc.intro("Rename a model")
            .parameters()
            .parameter("old_mid", "str", "ID of model to rename")
            .parameter("new_mid", "str", "New ID of model")
            .returns("success", "bool", "Returns True on success.")
            .raises()
            .raise("RuntimeError", "'new_mid' already stores a model")
            .raise("RuntimeError", "'old_mid' doesn't store a model to rename")(),
          py::arg("old_mid"),
          py::arg("new_mid"))
        .def(
          "clone_model",
          &py_server::py_clone_model,
          doc.intro("Clone existing model with ID.")
            .parameters()
            .parameter("old_mid", "str", "ID of model to clone")
            .parameter("new_mid", "str", "ID to store cloned model against")
            .parameter("sparse", "bool", "Stips model of expressions if true, increases speed.")
            .returns("success", "bool", "Returns True on success")
            .raises()
            .raise("RuntimeError", "'new_mid' already stores a model")
            .raise("RuntimeError", "'old_mid' doesn't store a model to clone")(),
          py::arg("old_mid"),
          py::arg("new_mid"),
          py::arg("sparse") = false)
        .def(
          "get_model_ids",
          &py_server::get_model_ids,
          doc.intro("Get IDs of all models stored").returns("id_list", "List[str]", "List of model IDs")())
        .def(
          "get_model_infos",
          &py_server::py_get_model_infos,
          doc.intro("Get model infos of all models stored")
            .returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
            .see_also("shyft.energy_market.core.ModelInfo")())
        .def(
          "get_state",
          &py_server::py_get_state,
          doc.intro("Get state of stored model by ID")
            .parameters()
            .parameter("mid", "str", "ID of model to get state of")
            .returns("state", "ModelState", "State of requested model")
            .raises()
            .raise("RuntimeError", "Unable to find model with ID 'mid'")(),
          py::arg("mid"))
        .def_property(
          "fx",
          [](py_server const &s) {
            return s.dispatch.state->callback;
          },
          [](py_server const &s, std::function<bool(std::string, std::string)> callback) {
            s.dispatch.state->callback = pyapi::wrap_callback(std::move(callback));
          },
          doc.intro("Callable[[str,str],bool]: server-side callable function(lambda) that takes two parameters:")
            .intro("mid :  the model id")
            .intro("fx_arg: arbitrary string to pass to the server-side function")
            .intro("The server-side fx is called when the client (or web-api) invokea the c.fx(mid,fx_arg).")
            .intro("The signature of the callback function should be (mid:str, fx_arg:str) -> bool")
            .intro("This feature is simply enabling the users to tailor server-side functionality in python!")
            .intro("Examples:\n")
            .intro(
              ">>> from shyft.energy_market.stm import DStmServer\n"
              ">>> s=DStmServer()\n"
              ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
              ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
              ">>>   # note we can use captured Server s here!"
              ">>>     return True\n"
              ">>> # and then bind the function to the callback\n"
              ">>> s.fx=my_fx\n"
              ">>> s.start_server()\n"
              ">>> : # later using client from anywhere to invoke the call\n"
              ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n")())
        .def(
          "optimize",
          &py_server::py_optimize,
          doc.intro("Run SHOP optimization on a model")
            .parameters()
            .parameter("mid", "str", "ID of model to run optimization on")
            .parameter("ta", "TimeAxis", "Time span to run optimization over")
            .parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
            .parameter(
              "compute_node_mode",
              "bool",
              "default false, for compute nodes set to true to minimize work done after optimize")
            .see_also("ShopCommand")
            .returns("success", "bool", "Stating whether optimization was started successfully or not.")(),
          py::arg("mid"),
          py::arg("ta"),
          py::arg("cmd"),
          py::arg("compute_node_mode") = false)
        .def(
          "evaluate_model",
          &py_server::py_evaluate_model,
          doc.intro("Evaluate any unbound time series attributes of a model")
            .parameters()
            .parameter("mid", "str", "ID of model to evaluate")
            .parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
            .parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
            .parameter(
              "update_ts_cache",
              "bool",
              "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
            .parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
            .see_also("UtcPeriod")
            .returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")(),
          py::arg("mid"),
          py::arg("bind_period"),
          py::arg("use_ts_cached_read") = true,
          py::arg("update_ts_cache") = true,
          py::arg("clip_period") = utcperiod{})
        .def(
          "start_web_api",
          &py_server::start_web_api,
          doc.intro("Start a web API for communicating with server")
            .parameters()
            .parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only, etc.")
            .parameter("port", "int", "port number to serve the web API on. Ensure it's available!")
            .parameter("doc_root", "str", "directory from which we will serve http/https documents.")
            .parameter("fg_threads", "int", "number of web API foreground threads, typical 1-4 depending on load.")
            .parameter("bg_threads", "int", "number of long running background thread workers to serve requests etc.")
            .parameter("tls_only", "bool", "default false, set to true to enforce tls sessions only.")
            .returns("port", "int", "the real port number used, if `port` arg is 0, then the auto-allocated value.")(),
          py::arg("host_ip"),
          py::arg("port"),
          py::arg("doc_root"),
          py::arg("fg_threads") = 2,
          py::arg("bg_threads") = 4,
          py::arg("tls_only") = false)
        .def("stop_web_api", &py_server::stop_web_api, doc.intro("Stops any ongoing web API service")())
        .def(
          "get_web_api_port",
          &py_server::get_web_api_port,
          doc.intro("Get listening port for web API. Returns -1 if not running")())
        .def(
          "get_web_api_ip",
          &py_server::get_web_api_ip,
          doc.intro("Get listening IP for web API. Returns empty string if not running.")())
        .def(
          "notify_change",
          &py_server::py_notify_change,
          doc.intro("Notify change on model urls, dstm://M..., so that changes are pushed to subscribers.")
            .intro("In case the urls are wrong, misformed etc, there is no exception raised for that.")
            .intro("It's the callers responsibility entirely to provide valid URLs")
            .parameters()
            .parameter("urls", "StringVector", "List of valid model-urls as kept by this server")(),
          py::arg("urls"))
        .def(
          "close", &py_server::clear, doc.intro("close and stop serving requests on the hpc binary socket interface")())
        .def(
          "add_compute_server",
          &py_server::add_compute_server,
          doc.intro("add a compute node specified by it's address string of form host:port")
            .parameters()
            .parameter("host_port", "str", "the address of the dstm compute node service in the form of host:port")(),
          py::arg("host_port"))
        .def(
          "stop_server",
          &py_server::stop_server,
          doc.intro("stop serving connections, gracefully.").see_also("start_server()")(),
          py::arg("timeout") = 1000)
        .def(
          "is_running",
          &py_server::is_running,
          doc.intro("true if server is listening and running").see_also("start_server()")())
        .def(
          "apply",
          &py_server::py_apply,
          doc.intro("Apply an action to a model.")
            .intro("")
            .intro(
              "The action gets exclusive access to the "
              "model when applied.")
            .intro("Take care not to return something owned by the model itself,")
            .intro("as exclusive access will be lost on return")
            .parameters()
            .parameter("mid", "str", "ID of model")
            .parameter("action", "Callable[[StmSystem],object]", "function to apply to model")
            .returns("obj", "object", "Object returned by the action")(),
          py::arg("mid"),
          py::arg("action"))
        .def_property_readonly(
          "cache_stats",
          &py_server::get_cache_stats,
          doc.intro("CacheStats: the internal dtss current cache statistics")())
        .def(
          "clear_cache_stats",
          &py_server::clear_cache_stats,
          doc.intro("clear accumulated cache_stats of the internal dtss server")())
        .def_property(
          "cache_max_items",
          &py_server::get_cache_size,
          &py_server::set_cache_size,
          doc.intro("int: internal dtss cache_max_items is the maximum number of time-series identities that are")
            .intro("kept in memory. Elements exceeding this capacity is elided using the least-recently-used")
            .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
            .intro("time-series from cache in the least recently used order.")())
        .def_property(
          "cache_ts_initial_size_estimate",
          &py_server::get_ts_size,
          &py_server::set_ts_size,
          doc.intro("int: The internal dtss initial time-series size estimate in bytes for the cache mechanism.")
            .intro("memory-target = cache_ts_initial_size_estimate * cache_max_items")
            .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
            .intro("time-series from cache in the least recently used order.")())
        .def_property(
          "cache_memory_target",
          &py_server::get_cache_memory_target_size,
          &py_server::set_cache_memory_target_size,
          doc.intro("int: The internal dtss servere memory max target in number of bytes.")
            .intro("If not set directly the following equation is  use:")
            .intro("cache_memory_target = cache_ts_initial_size_estimate * cache_max_items")
            .intro("When setting the target directly, number of items in the chache is ")
            .intro("set so that real memory usage is less than the specified target.")
            .intro("The setter could cause elements to be flushed out of cache.")())
        .def(
          "remove_from_cache",
          &py_server::remove_from_cache,
          doc.intro("flushes the *specified* ts_ids from cache")
            .intro("Has only effect for ts-ids that are in cache, non-existing items are ignored")
            .parameters()
            .parameter("ts_ids", "StringVector", "a list of time-series ids to flush out")(),
          py::arg("ts_ids"))
        .def(
          "flush_cache",
          &py_server::flush_cache,
          doc.intro("flushes all items out of internal dtss cache (cache_stats remain un-touched)")());

    py::class_<py_client>(
      m,
      "DStmClient",
      doc.intro("Client side for the DStmServer")
        .intro("")
        .intro("Takes care of message exchange to the remote server, using the supplied parameters.")
        .intro("It implements the message protocol of the server, sending message-prefix, ")
        .intro("arguments, waiting for the response, deserialize the response ")
        .intro("and handle it back to the user.")
        .see_also("DStmServer")())
      .def(
        py::init<string, int, int>(), py::arg("host_port"), py::arg("timeout_ms"), py::arg("operation_timeout_ms") = 0)
      .def_property_readonly(
        "host_port", &py_client::get_host_port, "str: Endpoint network address of the remote server.")
      .def_property_readonly(
        "timeout_ms", &py_client::get_timeout_ms, "int: Timout for remote server operations, in number milliseconds.")
      .def_property(
        "operation_timeout_ms",
        &py_client::get_operation_timeout_ms,
        &py_client::set_operation_timeout_ms,
        "int: Operation timeout for remote server operations, in number milliseconds.")
      .def_property_readonly(
        "is_open", &py_client::is_open, "bool: If the connection to the remote server is (still) open.")
      .def_property_readonly(
        "reconnect_count",
        &py_client::get_reconnect_count,
        "int: Number of reconnects to the remote server that have been performed.")
      .def("close", &py_client::close, doc.intro("Close the connection. It will automatically reopen if needed.")())
      .def(
        "get_server_version",
        &py_client::get_server_version,
        doc.intro("Get version of remote server.").returns("version", "str", "Server version string")())
      .def(
        "create_model",
        &py_client::create_model,
        doc.intro("Create an empty model and store it server side.")
          .parameters()
          .parameter("mid", "str", "ID of new model")
          .returns("success", "bool", "Returns true on success")(),
        py::arg("mid"))
      .def(
        "add_model",
        &py_client::add_model,
        doc.intro("Add model to server")
          .parameters()
          .parameter("mid", "str", "ID/key to store model as.")
          .parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
          .returns("success", "bool", "Returns True on success")(),
        py::arg("mid"),
        py::arg("mdl"))
      .def(
        "remove_model",
        &py_client::remove_model,
        doc.intro("Remove model by ID.")
          .parameters()
          .parameter("mid", "str", "ID of model to remove.")
          .returns("success", "bool", "Returns True on success.")(),
        py::arg("mid"))
      .def(
        "rename_model",
        &py_client::rename_model,
        doc.intro("Rename a model")
          .parameters()
          .parameter("old_mid", "str", "ID of model to rename")
          .parameter("new_mid", "str", "New ID of model")
          .returns("success", "bool", "Returns True on success.")(),
        py::arg("old_mid"),
        py::arg("new_mid"))
      .def(
        "clone_model",
        &py_client::clone_model,
        doc.intro("Clone existing model with ID.")
          .parameters()
          .parameter("old_mid", "str", "ID of model to clone")
          .parameter("new_mid", "str", "ID to store cloned model against")
          .parameter("sparse", "bool", "Stips model of expressions if true, increases speed.")
          .returns("success", "bool", "Returns True on success")(),
        py::arg("old_mid"),
        py::arg("new_mid"),
        py::arg("sparse") = false)
      .def(
        "get_model_ids",
        &py_client::get_model_ids,
        doc.intro("Get IDs of all models stored").returns("id_list", "List[str]", "List of model IDs")())
      .def(
        "get_model_infos",
        &py_client::get_model_infos,
        doc.intro("Get model infos of all models stored")
          .returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
          .see_also("shyft.energy_market.core.ModelInfo")())
      .def(
        "get_model",
        &py_client::get_model,
        doc.intro("Get a stored model by ID")
          .parameters()
          .parameter("mid", "str", "ID of model to get")
          .parameter("stripped", "bool", "default false, full model, if true return stripped skeleton model")
          .returns("mdl", "StmSystem", "Requested model")(),
        py::arg("mid"),
        py::arg("stripped") = false)
      .def(
        "get_state",
        &py_client::get_state,
        doc.intro("Get the state of a model by ID")
          .parameters()
          .parameter("mid", "str", "ID of model to get state of")
          .returns("state", "ModelState", "State of requested model")(),
        py::arg("mid"))
      .def(
        "optimize",
        &py_client::optimize,
        doc.intro("Run optimization on a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .parameter("ta", "TimeAxis", "Time span to run optimization over")
          .parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
          .parameter(
            "compute_node_mode",
            "bool",
            "default false, for compute nodes set to true to minimize work done after optimize")
          .see_also("ShopCommand")
          .returns("success", "bool", "Stating whether optimization was started successfully or not.")(),
        py::arg("mid"),
        py::arg("ta"),
        py::arg("cmd"),
        py::arg("compute_node_mode") = false)
      .def(
        "start_tune",
        &py_client::start_tune,
        doc.intro("Start tuning a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .returns("success", "bool", "Stating tuning was started successfully or not.")(),
        py::arg("mid"))
      .def(
        "tune",
        &py_client::tune,
        doc.intro("Run tuning optimization on a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .parameter("ta", "TimeAxis", "Time span to run optimization over")
          .parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
          .see_also("ShopCommand")
          .returns("success", "bool", "Stating whether tuning optimization was started successfully or not.")(),
        py::arg("mid"),
        py::arg("ta"),
        py::arg("cmd"))
      .def(
        "stop_tune",
        &py_client::stop_tune,
        doc.intro("Stop tuning a model")
          .parameters()
          .parameter("mid", "str", "ID of model to run optimization on")
          .returns("success", "bool", "Stating tuning was stopped successfully or not.")(),
        py::arg("mid"))
      .def(
        "get_log",
        &py_client::get_log,
        doc.intro("Get log for a model")
          .parameters()
          .parameter("mid", "str", "ID of model to get log for")
          .returns("entries", "ShopLogEntryList", "List of log entries")(),
        py::arg("mid"))
      .def(
        "evaluate_ts",
        &py_client::evaluate_ts,
        doc.intro("Evaluate any time series expressions")
          .intro("If an expression can not be evaluated, TsEvaluationError is returned in place of the ts.")
          .parameters()
          .parameter("tsv", "TsVector", "Timeseries to evaluate")
          .parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
          .parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
          .parameter(
            "update_ts_cache",
            "bool",
            "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
          .parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
          .see_also("UtcPeriod")
          .returns("result", "list", "Returns the evaluated timeseries or an evaluation error.")(),
        py::arg("tsv"),
        py::arg("bind_period"),
        py::arg("use_ts_cached_read") = true,
        py::arg("update_ts_cache") = true,
        py::arg("clip_period") = utcperiod{})
      .def(
        "evaluate_model",
        &py_client::evaluate_model,
        doc.intro("Evaluate any unbound time series attributes of a model")
          .parameters()
          .parameter("mid", "str", "ID of model to evaluate")
          .parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
          .parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
          .parameter(
            "update_ts_cache",
            "bool",
            "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
          .parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
          .see_also("UtcPeriod")
          .returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")(),
        py::arg("mid"),
        py::arg("bind_period"),
        py::arg("use_ts_cached_read") = true,
        py::arg("update_ts_cache") = true,
        py::arg("clip_period") = utcperiod{})
      .def(
        "fx",
        &py_client::fx,
        doc.intro("Execute the serverside fx, passing supplied arguments")
          .parameters()
          .parameter("mid", "str", "ID of model for the server-side fx")
          .parameter("fx_arg", "str", "any argument passed to the server-side fx")
          .returns("success", "bool", "true if call successfully done")(),
        py::arg("mid"),
        py::arg("fx_arg"))
      .def(
        "get_ts",
        &py_client::get_ts,
        doc.intro("Get the time-series from the model mid, as specified by ts_urls")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("ts_urls", "StringVector", "Strongly typed list of strings, urls, like dstm://Mmid/..")
          .returns("time-series", "TsVector", "list of time-series as specifed by the list of ts_urls, same order")(),
        py::arg("mid"),
        py::arg("ts_urls"))
      .def(
        "set_ts",
        &py_client::set_ts,
        doc.intro("Set the time-series in the model mid, as specified by ts_urls in the tsv.")
          .intro("If anyone is subscribers on time-series, or expressions affected, they are notified")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("tsv", "TsVector", "list of TimeSeries(ts_url,ts_with_values) ,ts_url, like dstm://Mmid/..")(),
        py::arg("mid"),
        py::arg("tsv"))
      .def(
        "add_compute_server",
        &py_client::add_compute_server,
        doc.intro("add a compute node specified by it's address string of form host:port")
          .parameters()
          .parameter("host_port", "str", "the address of the dstm compute node service in the form of host:port")(),
        py::arg("host_port"))
      .def(
        "set_attrs",
        &py_client::set_attrs,
        doc.intro("Set a list of attributes specified by url.")
          .intro("If anyone is subscribed on the attribute they are notified")
          .parameters()
          .parameter("attrs", "list", "list of pairs of dstm url and attribute.")
          .returns(
            "attrs", "list", "list of None if successful or UrlResolveError if the attribute could not be set.")(),
        py::arg("attrs"))
      .def(
        "get_attrs",
        &py_client::get_attrs,
        doc.intro("Get a list of attributes specified by url.")
          .intro("If an url is unable to be resolved UrlResolveError is returned in place of the attribute.")
          .parameters()
          .parameter("urls", "list", "list of dstm attribute urls.")
          .returns("attrs", "list", "list of dstm attributes.")(),
        py::arg("urls"))
      .def(
        "reset_model",
        &py_client::reset_model,
        doc.intro("Reset the model specified by mid.")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .returns("ok", "bool", "whether or not the model was reset.")(),
        py::arg("mid"))
      .def(
        "set_model",
        &py_client::set_model,
        doc.intro("Set the model specified by mid.")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("model", "StmSystem", "Model to store")
          .returns("ok", "bool", "whether or not the model was set.")(),
        py::arg("mid"),
        py::arg("model"))
      .def(
        "patch_model",
        &py_client::patch,
        doc.intro("Patch the model `mid` with patch `p` using operation `op`.")
          .parameters()
          .parameter("mid", "str", "ID of model")
          .parameter("op", "", "Operation is one of ADD,REMOVE_RELATIONS,REMOVE_OBJECTS")
          .parameter("p", "", "The StmSystem describing the patch")
          .returns("ok", "bool", "whether or not the model was reset.")(),
        py::arg("mid"),
        py::arg("op"),
        py::arg("p"))
      .def(
        "compute_server_status",
        &py_client::compute_server_status,
        doc.intro("Get status of managed compute servers")
          .returns("status", "List[ComputeServerStatus]", "status of managed compute servers.")())
      .def(
        "cache_flush", &py_client::cache_flush, doc.intro("flushes all items out of internal dtss cache and stats")())
      .def(
        "cache_stats", &py_client::cache_stats, doc.intro("CacheStats: the internal dtss current cache statistics")());
  }

}
