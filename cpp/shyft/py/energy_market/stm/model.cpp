/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <algorithm>
#include <string>
#include <type_traits>
#include <utility>

#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/core/fmt_std_opt.h>
#include <shyft/energy_market/stm/attributes.h>
#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>
#include <shyft/energy_market/stm/unit_group_type.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/py/bindings.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/reflection.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_attribute_types(py::module_ &m) {
    {
      auto t_model_state = py::enum_<model_state>(
        m, "ModelState", doc.intro("Describes the possible state of the STM model")());
      pyapi::expose_enumerators(t_model_state);
    }
    {
      auto t_patch_op = py::enum_<stm_patch_op>(
        m, "StmPatchOperation", doc.intro("Describes how a patch should be applied to the STM model")());
      pyapi::expose_enumerators(t_patch_op);
    }
    {
      auto ac = py::class_<absolute_constraint>(
        m, "AbsoluteConstraint", "A grouping of time series related to an absolute constraint (i.e. infinite cost)\n");

      _add_proxy_property(
        ac, "limit", absolute_constraint, limit, "TimeSeries: The threshold related to the constraint");
      _add_proxy_property(
        ac, "flag", absolute_constraint, flag, "TimeSeries: Flag indicating whether the constraint is active or not");
      pyapi::expose_format(ac);
    }
    {
      auto pc = py::class_<penalty_constraint>(
        m, "PenaltyConstraint", "A grouping of time-series related to a constraint with a penalty cost\n");

      _add_proxy_property(
        pc, "limit", penalty_constraint, limit, "TimeSeries: The threshold related to the constraint");
      _add_proxy_property(
        pc, "flag", penalty_constraint, flag, "TimeSeries: Flag indicating whether the constraint is active or not");
      _add_proxy_property(pc, "cost", penalty_constraint, cost, "TimeSeries: The cost of violating the constraint");
      _add_proxy_property(
        pc, "penalty", penalty_constraint, penalty, "TimeSeries: Incurred cost of violating the constraint");
      pyapi::expose_format(pc);
    }


    m.def(
      "compute_effective_price",
      +[](time_series::dd::apoint_ts usage, t_xy_ bids, bool cheapest) {
        return effective_price(usage, bids, cheapest);
      },
      "Given usage, and bids, compute the effective price achieved consuming bids in the order as speficied with "
      "`use_cheapest`.\n"
      "If usage is 0, then first available price is computed.\n"
      "If bids are None, usage None, or empty, then empty is returned.\n"
      "If usage is more than available in the bids, the effective price for all the bids are computed.\n"
      "\nArgs:"
      "\n    usage: The usage in W\n"
      "\n    bids: The available bids, time-dependent xy, where x= price [Money/J], y= energy [W]\n"
      "\n    use_cheapest: Take cheapest bids first, act as buyer, if false, act as seller, and take highest bids "
      "first\n"
      "\nReturns:"
      "\n    effective price: The computed effective price result\n",
      py::arg("usage"),
      py::arg("bids"),
      py::arg("use_cheapest"));
    {
      auto t_log_severity = py::enum_<log_severity>(m, "LogSeverity", "Log severity\n");
      pyapi::expose_enumerators(t_log_severity);
    }
    {
      auto t_log_entry = py::class_<log_entry>(m, "LogEntry", "A log entry.\n").def(py::init());
      pyapi::expose_members(t_log_entry);
      pyapi::expose_format_str(t_log_entry);
      pyapi::expose_format_repr(t_log_entry);
      t_log_entry.def(py::self == py::self).def(py::self != py::self);
    }
  }

  extern void pyexport_model_busbar(py::module_ &, py::class_<busbar, std::shared_ptr<busbar>> &bb);
  py::class_<busbar, std::shared_ptr<busbar>> pyexport_model_busbar_fwd(py::module_ &m);
  extern void pyexport_model_contract(
    py::module_ &m,
    py::class_<contract, std::shared_ptr<contract>> &c,
    py::class_<contract_portfolio, std::shared_ptr<contract_portfolio>> &cp);
  std::tuple<
    py::class_<contract, std::shared_ptr<contract>>,
    py::class_<contract_portfolio, std::shared_ptr<contract_portfolio>>>
    pyexport_model_contract_fwd(py::module_ &m);
  extern void pyexport_model_gate(py::module_ &);
  extern py::class_<stm_hps, std::shared_ptr<stm_hps>> pyexport_model_hps_fwd(py::module_ &);
  extern void pyexport_model_hps(py::module_ &, py::class_<stm_hps, std::shared_ptr<stm_hps>> &);
  extern void pyexport_model_network(py::module_ &, py::class_<network, std::shared_ptr<network>> &);
  extern py::class_<network, std::shared_ptr<network>> pyexport_model_network_fwd(py::module_ &);
  extern void pyexport_model_optimization_summary(py::module_ &);
  extern void pyexport_model_power_module(py::module_ &);
  extern void pyexport_model_power_plant(py::module_ &);
  extern void pyexport_model_reservoir_aggregate(py::module_ &);
  extern void pyexport_model_reservoir(py::module_ &);
  extern void pyexport_model_run_parameters(py::module_ &);
  extern void pyexport_model_energy_market_area(py::module_ &);
  extern void pyexport_model_stm_system(py::module_ &, py::class_<stm_system, std::shared_ptr<stm_system>> &);
  extern py::class_<stm_system, std::shared_ptr<stm_system>> pyexport_model_stm_system_fwd(py::module_ &);
  extern void pyexport_model_transmission_line(py::module_ &);
  extern void pyexport_model_unit(py::module_ &);
  extern void pyexport_model_unit_group(py::module_ &, py::class_<unit_group, std::shared_ptr<unit_group>> &);
  extern py::class_<unit_group, std::shared_ptr<unit_group>> pyexport_model_unit_group_fwd(py::module_ &);

  extern void pyexport_model_waterway(py::module_ &);
  extern void pyexport_model_wind_turbine(py::module_ &);
  extern py::class_<wind_farm, std::shared_ptr<wind_farm>> pyexport_model_wind_farm_fwd(py::module_ &);
  extern void pyexport_model_wind_farm(py::module_ &, py::class_<wind_farm, std::shared_ptr<wind_farm>> &);
  extern void pyexport_url(py::module_ &);
  extern void pyexport_evaluate_ts_error(py::module_ &);
  extern void pyexport_model_catchment(py::module_ &);

  void pyexport(py::module_ &m) {
    pyexport_attribute_types(m);
    pyexport_evaluate_ts_error(m);

    auto hps = pyexport_model_hps_fwd(m);
    auto bb = pyexport_model_busbar_fwd(m);
    auto net = pyexport_model_network_fwd(m);
    auto sys = pyexport_model_stm_system_fwd(m);
    auto [contract, portfolio] = pyexport_model_contract_fwd(m);
    auto ug = pyexport_model_unit_group_fwd(m);
    auto wf = pyexport_model_wind_farm_fwd(m);
    pyexport_url(m);
    pyexport_model_unit(m);
    pyexport_model_gate(m);
    pyexport_model_power_plant(m);
    pyexport_model_reservoir(m);
    pyexport_model_reservoir_aggregate(m);
    pyexport_model_catchment(m);
    pyexport_model_transmission_line(m);
    pyexport_model_waterway(m);
    pyexport_model_energy_market_area(m);
    pyexport_model_power_module(m);
    pyexport_model_run_parameters(m);
    pyexport_model_optimization_summary(m);
    pyexport_model_wind_turbine(m);

    pyexport_model_unit_group(m, ug);

    pyexport_model_wind_farm(m, wf);
    pyexport_model_contract(m, contract, portfolio);
    pyexport_model_stm_system(m, sys);
    pyexport_model_busbar(m, bb);
    pyexport_model_hps(m, hps);
    pyexport_model_network(m, net);
  }

}
