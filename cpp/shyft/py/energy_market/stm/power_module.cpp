/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>

namespace shyft::energy_market::stm {

  void pyexport_model_power_module(py::module_ &m) {
    auto c = expose_component_type<power_module>(
      m,
      "PowerModule", doc.intro("A power module representing consumption.")());

    expose_subcomponent_init<stm_system, &power_module::sys_>(c, "sys", "system");
    {
      auto p = expose_attributes_type<power_module::power_>(
        c,
        "Power", doc.intro("Unit.Power attributes, consumption[W, J/s].")());
      _add_proxy_property(
        p,
        "realised",
        power_module::power_,
        realised,
        "_ts: Historical fact, time series. W, J/s, if positive then production, if negative then consumption.");
      _add_proxy_property(
        p,
        "schedule",
        power_module::power_,
        schedule,
        "_ts: The current schedule, time series. W, J/s, if positive then production, if negative then consumption");
      _add_proxy_property(
        p,
        "result",
        power_module::power_,
        result,
        "_ts: The optimal/simulated/estimated result, time series. W, J/s, if positive then production, if negative "
        "then consumption");
    }
    c
      .def_readonly("power", &power_module::power, "Power: Power attributes.")
      ;

  }
}
