
#include <memory>
#include <string>

#include <fmt/core.h>

#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>

namespace shyft::energy_market::stm {

  void pyexport_model_optimization_summary(py::module_ &m) {

    auto os =
      expose_component_type<optimization_summary>(
        m,
        "_OptimizationSummary", "Summary of optimzation results.");
    {
      auto opr = expose_attributes_type<optimization_summary::reservoir_>(
        os,
        "_Reservoir");
      _add_proxy_property(
        opr,
        "end_value",
        optimization_summary::reservoir_,
        end_value,
        "float: [NOK] Value of remaining water after sim-end.");
      _add_proxy_property(
        opr,
        "sum_ramping_penalty",
        optimization_summary::reservoir_,
        sum_ramping_penalty,
        "float: [NOK] Sum penalty for all reservoirs for violating ramping constraints.");
      _add_proxy_property(
        opr,
        "sum_limit_penalty",
        optimization_summary::reservoir_,
        sum_limit_penalty,
        "float: [NOK] Sum penalty for all reservoirs for violating volume limits during simulation horizon.");
      _add_proxy_property(
        opr,
        "end_limit_penalty",
        optimization_summary::reservoir_,
        end_limit_penalty,
        "float: [NOK] Sum penalty for all reservoirs for violating volume limits at the end of simulation horizon.");
      _add_proxy_property(
        opr,
        "hard_limit_penalty",
        optimization_summary::reservoir_,
        hard_limit_penalty,
        "float: [NOK] Sum penalty for all reservoirs for violating the hard volume or level limits.");

      auto opw = expose_attributes_type<optimization_summary::waterway_>(os, "_Waterway");
      _add_proxy_property(
        opw,
        "vow_in_transit",
        optimization_summary::waterway_,
        vow_in_transit,
        "float: [NOK] Value of water in transit.");
      _add_proxy_property(
        opw,
        "sum_discharge_fee",
        optimization_summary::waterway_,
        sum_discharge_fee,
        "float: [NOK] Sum of discharge fee.");
      _add_proxy_property(
        opw,
        "discharge_group_penalty",
        optimization_summary::waterway_,
        discharge_group_penalty,
        "float: [NOK] Discharge group penalty.");
      _add_proxy_property(
        opw,
        "discharge_group_ramping_penalty",
        optimization_summary::waterway_,
        discharge_group_penalty,
        "float: [NOK] Discharge group ramping penalty.");

      auto opg = expose_attributes_type<optimization_summary::gate_>(os, "_Gate");
      _add_proxy_property(
        opg,
        "ramping_penalty",
        optimization_summary::gate_,
        ramping_penalty,
        "float: [NOK] Penalty for violating ramping constraints, for all gates and timesteps.");
      _add_proxy_property(
        opg,
        "discharge_cost",
        optimization_summary::gate_,
        discharge_cost,
        "float: [NOK] Sum discharge cost for all gates and timesteps.");
      _add_proxy_property(
        opg,
        "discharge_constraint_penalty",
        optimization_summary::gate_,
        discharge_constraint_penalty,
        "float: [NOK] Sum penalty for violating max/min constraints, for all gates and timesteps.");


      auto ops = expose_attributes_type<optimization_summary::spill_>(os, "_Spill");
      _add_proxy_property(
        ops,
        "cost",
        optimization_summary::spill_,
        cost,
        "float: [NOK] Sum cost of spill for all spill gates gates and timesteps.");
      _add_proxy_property(
        ops,
        "physical_cost",
        optimization_summary::spill_,
        physical_cost,
        "float: [NOK] Sum cost of physical spill for all reservoirs and timesteps.");
      _add_proxy_property(
        ops,
        "nonphysical_cost",
        optimization_summary::spill_,
        nonphysical_cost,
        "float: [NOK] Sum cost of non-physical spill for all reservoirs and timesteps.");
      _add_proxy_property(
        ops,
        "physical_volume",
        optimization_summary::spill_,
        physical_volume,
        "float: [NOK] Sum of physical spill volume for all reservoirs and timesteps.");
      _add_proxy_property(
        ops,
        "nonphysical_volume",
        optimization_summary::spill_,
        nonphysical_volume,
        "float: [NOK] Sum of non-physical spill volume for all reservoirs and timesteps.");


      auto opb = expose_attributes_type<optimization_summary::bypass_>(os, "_Bypass");
      _add_proxy_property(
        opb,
        "cost",
        optimization_summary::bypass_,
        cost,
        "float: [NOK] Sum cost of bypass for all bypass gates and timesteps.");

      auto opra = expose_attributes_type<optimization_summary::ramping_>(os, "_Ramping");
      _add_proxy_property(
        opra,
        "ramping_penalty",
        optimization_summary::ramping_,
        ramping_penalty,
        "float: [NOK] Sum penalty for violating ramping constraints, for all constraints and timesteps.");

      auto opre = expose_attributes_type<optimization_summary::reserve_>(os, "_Reserve");
      _add_proxy_property(
        opre,
        "violation_penalty",
        optimization_summary::reserve_,
        violation_penalty,
        "float: [NOK] Total penalty cost when reserve deviate below obligations.");
      _add_proxy_property(
        opre, "sale_buy", optimization_summary::reserve_, sale_buy, "float: [NOK] Reserve trade value.");
      _add_proxy_property(
        opre, "obligation_value", optimization_summary::reserve_, obligation_value, "float: [NOK] Obligation value.");

      auto opu = expose_attributes_type<optimization_summary::unit_>(os, "_Unit");
      _add_proxy_property(
        opu,
        "startup_cost",
        optimization_summary::unit_,
        startup_cost,
        "float: [NOK] Total startup and shutdown cost.");
      _add_proxy_property(
        opu,
        "schedule_penalty",
        optimization_summary::unit_,
        schedule_penalty,
        "float: [NOK] Total penalty cost when generator schedule is violated.");

      auto opp = expose_attributes_type<optimization_summary::plant_>(os, "_Plant");
      _add_proxy_property(
        opp,
        "production_constraint_penalty",
        optimization_summary::plant_,
        production_constraint_penalty,
        "float: [NOK] Total production cost when time-dependent max/min production is violated.");
      _add_proxy_property(
        opp,
        "discharge_constraint_penalty",
        optimization_summary::plant_,
        discharge_constraint_penalty,
        "float: [NOK] Total discharge cost when time-dependent max/min discharge is violated.");
      _add_proxy_property(
        opp,
        "schedule_penalty",
        optimization_summary::plant_,
        schedule_penalty,
        "float: [NOK] Total penalty cost for violating plant schedule in simulation horizon.");
      _add_proxy_property(
        opp,
        "ramping_penalty",
        optimization_summary::plant_,
        ramping_penalty,
        "float: [NOK] Total penalty cost for violating plant ramping constraints in simulation horizon.");

      auto opm = expose_attributes_type<optimization_summary::market_>(os, "_Market");
      _add_proxy_property(
        opm,
        "sum_sale_buy",
        optimization_summary::market_,
        sum_sale_buy,
        "float: [NOK] Sum energy bought minus energy sold in the market for all timesteps.");
      _add_proxy_property(
        opm,
        "load_penalty",
        optimization_summary::market_,
        load_penalty,
        "float: [NOK] Sum penalty for violating load obligation for all markets and timesteps.");
      _add_proxy_property(
        opm,
        "load_value",
        optimization_summary::market_,
        load_value,
        "float: [NOK] Total load multiplied with marked sale price.");
    }
    os.def_readonly("reservoir", &optimization_summary::reservoir, "_Reservoir: Reservoir related summary.")
      .def_readonly("waterway", &optimization_summary::waterway, "_Waterway: Waterway related summary.")
      .def_readonly("gate", &optimization_summary::gate, "_Gate: Gate related summary.")
      .def_readonly("spill", &optimization_summary::spill, "_Spill: Spill related summary.")
      .def_readonly("bypass", &optimization_summary::bypass, "_Bypass: Bypass related summary.")
      .def_readonly("ramping", &optimization_summary::ramping, "_Ramping: Ramping related summary.")
      .def_readonly("reserve", &optimization_summary::reserve, "_Reserve: Reserve related summary.")
      .def_readonly("unit", &optimization_summary::unit, "_Unit: Unit related summary.")
      .def_readonly("plant", &optimization_summary::plant, "_Plant: Plant related summary.")
      .def_readonly("market", &optimization_summary::market, "_Market: Market related summary.")
      ;
    add_proxy_property(os, "total", optimization_summary, total, "float: Total");
    add_proxy_property(os, "sum_penalties", optimization_summary, sum_penalties, "float: Total penalty cost.");
    add_proxy_property(
      os, "minor_penalties", optimization_summary, minor_penalties, "float: Total minor penalty cost.");
    add_proxy_property(
      os, "major_penalties", optimization_summary, major_penalties, "float: Total major penalty cost.");
    add_proxy_property(os, "grand_total", optimization_summary, grand_total, "float: Grand total.");
  }

}
