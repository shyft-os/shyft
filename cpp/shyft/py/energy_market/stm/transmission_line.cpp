/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <memory>
#include <string>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>

namespace shyft::energy_market::stm {

  void pyexport_model_transmission_line(py::module_ &m) {
    auto c = expose_component_type<transmission_line>(
      m, "TransmissionLine", doc.intro("A transmission line connecting two busbars.")());

    expose_subcomponent_init<network, &transmission_line::net_>(c, "net", "network");

    c
      .def_readonly("from_bb", &transmission_line::from_bb, "Busbar: connected from this transmission line")
      .def_readonly("to_bb", &transmission_line::to_bb, "Busbar: connected to this transmission line")
      ;
    add_proxy_property(c, "capacity", transmission_line, capacity, "_ts: Transmission line capacity");
  }
}
