/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/py/bindings.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_reservoir(py::module_ &m) {
    auto r = expose_component_type<reservoir, hydro_power::reservoir>(m, "Reservoir", "Stm reservoir.");
    expose_subcomponent_init<stm_hps>(r, "hps");
    add_proxy_property(
      r,
      "volume_level_mapping",
      reservoir,
      volume_level_mapping,
      "_t_xy_: Volume capacity description, time-dependent x=[masl], y=[m3].");
    {
      auto rl = py::class_<reservoir::level_>(r, "Level");

      {
        auto rlc = py::class_<reservoir::level_::constraint_>(rl, "Constraints");

        _add_proxy_property(
          rlc, "min", reservoir::level_::constraint_, min, "_ts: [masl] Level constraint minimum, time series.");
        _add_proxy_property(
          rlc, "max", reservoir::level_::constraint_, max, "_ts: [masl] Level constraint maximum, time series.");
      }
      rl.def_readonly("constraint", &reservoir::level_::constraint, "Constraints: Level constraint attributes.");

      _add_proxy_property(
        rl,
        "regulation_min",
        reservoir::level_,
        regulation_min,
        "_ts: [masl] Lowest regulated water level, time-dependent attribute.");
      _add_proxy_property(
        rl,
        "regulation_max",
        reservoir::level_,
        regulation_max,
        "_ts: [masl] Highest regulated water level, time-dependent attribute.");
      _add_proxy_property(
        rl, "realised", reservoir::level_, realised, "_ts: [masl] Historical water level, time series.");
      _add_proxy_property(rl, "schedule", reservoir::level_, schedule, "_ts: [masl] Level schedule, time series.");
      _add_proxy_property(rl, "result", reservoir::level_, result, "_ts: [masl] Level results, time series.");

      auto rv = py::class_<reservoir::volume_>(r, "Volume");
      {
        auto rvc = py::class_<reservoir::volume_::constraint_>(rv, "Constraints");

        _add_proxy_property(
          rvc, "min", reservoir::volume_::constraint_, min, "_ts: [m3] Volume constraint minimum, time series.");
        _add_proxy_property(
          rvc, "max", reservoir::volume_::constraint_, max, "_ts: [m3] Volume constraint maximum, time series.");

        {
          auto rvct = py::class_<reservoir::volume_::constraint_::tactical_>(rvc, "Tactical");
          rvct
            .def_readonly(
              "min",
              &reservoir::volume_::constraint_::tactical_::min,
              "PenaltyConstraint: [masl],[money] Tactical minimum volume constraint.")
            .def_readonly(
              "max",
              &reservoir::volume_::constraint_::tactical_::max,
              "PenaltyConstraint: [masl],[money] Tactical maximum volume constraint.");
        }

        auto rvs = py::class_<reservoir::volume_::slack_>(rv, "Slack");
        _add_proxy_property(rvs, "lower", reservoir::volume_::slack_, lower, "_ts: [m3] Lower volume slack");
        _add_proxy_property(rvs, "upper", reservoir::volume_::slack_, upper, "_ts: [m3] Upper volume slack");

        auto rvco = py::class_<reservoir::volume_::cost_>(rv, "Cost");
        {
          auto rvcc = py::class_<reservoir::volume_::cost_::cost_curve_>(rvco, "CostCurve");

          _add_proxy_property(
            rvcc, "curve", reservoir::volume_::cost_::cost_curve_, curve, "_t_xy_: [m3],[money/m3] Volume cost curve.");
          _add_proxy_property(
            rvcc,
            "penalty",
            reservoir::volume_::cost_::cost_curve_,
            penalty,
            "_ts: [money] Volume penalty, time series.");
        }
        rvc.def_readonly(
          "tactical", &reservoir::volume_::constraint_::tactical, "Tactical: Tactical volume constraint attributes.");

        rvco.def_readonly("flood", &reservoir::volume_::cost_::flood, "_CostCurve: Cost applied to volume.")
          .def_readonly(
            "peak", &reservoir::volume_::cost_::peak, "CostCurve: Cost applied to the highest volume reached.");
      }

      rv.def_readonly("constraint", &reservoir::volume_::constraint, "Constraints: Volume constraint attributes.")
        .def_readonly("slack", &reservoir::volume_::slack, "Slack: Slack attributes.");

      _add_proxy_property(
        rv,
        "static_max",
        reservoir::volume_,
        static_max,
        "_ts: [m3] Maximum regulated volume, time-dependent attribute.");
      _add_proxy_property(rv, "realised", reservoir::volume_, realised, "_ts: [m3] Historical volume, time series.");
      _add_proxy_property(rv, "schedule", reservoir::volume_, schedule, "_ts: [m3] Volume schedule, time series.");
      _add_proxy_property(rv, "result", reservoir::volume_, result, "_ts: [m3] Volume results, time series.");
      _add_proxy_property(rv, "penalty", reservoir::volume_, penalty, "_ts: [m3] Volume penalty, time series.");

      auto rwv = py::class_<reservoir::water_value_>(r, "WaterValue");

      _add_proxy_property(
        rwv, "endpoint_desc", reservoir::water_value_, endpoint_desc, "_ts: [money/joule] Fixed water-value endpoint.");
      {
        auto rwvr = py::class_<reservoir::water_value_::result_>(rwv, "Result");
        _add_proxy_property(
          rwvr,
          "local_volume",
          reservoir::water_value_::result_,
          local_volume,
          "_ts: [money/m3] Resulting water-value.");
        _add_proxy_property(
          rwvr,
          "global_volume",
          reservoir::water_value_::result_,
          global_volume,
          "_ts: [money/m3] Resulting water-value.");
        _add_proxy_property(
          rwvr,
          "local_energy",
          reservoir::water_value_::result_,
          local_energy,
          "_ts: [money/joule] Resulting water-value.");
        _add_proxy_property(
          rwvr,
          "end_value",
          reservoir::water_value_::result_,
          end_value,
          "_ts: [money] Resulting water-value at the endpoint.");
      }
      rwv.def_readonly("result", &reservoir::water_value_::result, "Result: Optimizer detailed watervalue results.");

      auto ri = py::class_<reservoir::inflow_>(r, "Inflow");

      _add_proxy_property(ri, "schedule", reservoir::inflow_, schedule, "_ts: [m3/s] Inflow input, time series.");
      _add_proxy_property(ri, "realised", reservoir::inflow_, realised, "_ts: [m3/s] Historical inflow, time series.");
      _add_proxy_property(ri, "result", reservoir::inflow_, result, "_ts: [m3/s]  Inflow result, time series.");

      auto rr = py::class_<reservoir::ramping_>(r, "Ramping");

      _add_proxy_property(
        rr, "level_down", reservoir::ramping_, level_down, "_ts: [m/s] Max level change down, time series.");
      _add_proxy_property(
        rr, "level_up", reservoir::ramping_, level_up, "_ts: [m/s] Max level change up, time series.");
      _add_proxy_property(
        rr,
        "amplitude_down",
        reservoir::ramping_,
        amplitude_down,
        "_t_xy: [s][m] Max level change down for a time period, duration vs m change.");
      _add_proxy_property(
        rr,
        "amplitude_up",
        reservoir::ramping_,
        amplitude_up,
        "_t_xy: [s][m] Max level change up for a time period, duration vs m change.");
    }
    r.def_readonly("level", &reservoir::level, "_Level: Level attributes.")
      .def_readonly("volume", &reservoir::volume, "Volume: Volume attributes.")
      .def_readonly("inflow", &reservoir::inflow, "Inflow: Inflow attributes.")
      .def_readonly("ramping", &reservoir::ramping, "Ramping: Ramping attributes.")
      .def_readonly("water_value", &reservoir::water_value, "WaterValue: water-value attributes.");
  }
}
