#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <boost/preprocessor/stringize.hpp>

#include <shyft/config.h>
#if defined(SHYFT_WITH_SHOP)
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_custom_attributes.h>
#include <shyft/energy_market/stm/stm_system.h>
#endif
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/version.h>

#if defined(SHYFT_WITH_SHOP)
#include <shyft/energy_market/stm/shop/shop_bundle_path.h>
#include <shyft/energy_market/stm/shop/shop_system.h>
#endif

namespace shyft::energy_market::stm::shop {
#if defined(SHYFT_WITH_SHOP)

  void expose_shop_commander(py::module_ &m) {

    py::class_<shop_commander, std::shared_ptr<shop_commander>>(
      m, "ShopCommander", "A shop commander, utility for sending individual commands to the shop core.")
      .def("execute", &shop_commander::execute, "Execute command object.", py::arg("command"))
      .def("execute_string", &shop_commander::execute_string, "Execute command string.", py::arg("command_string"))
      .def("executed", &shop_commander::executed, "Get executed commands as objects.")
      .def("executed_strings", &shop_commander::executed_strings, "Get executed commands as strings.")
      .def("set_method_primal", &shop_commander::set_method_primal, "Shop command string 'set method /primal'.")
      .def("set_method_dual", &shop_commander::set_method_dual, "Shop command string 'set method /dual'.")
      .def("set_method_baropt", &shop_commander::set_method_baropt, "Shop command string 'set method /baropt'.")
      .def(
        "set_method_hydbaropt", &shop_commander::set_method_hydbaropt, "Shop command string 'set method /hydbaropt'.")
      .def(
        "set_method_netprimal", &shop_commander::set_method_netprimal, "Shop command string 'set method /netprimal'.")
      .def("set_method_netdual", &shop_commander::set_method_netdual, "Shop command string 'set method /netdual'.")
      .def("set_code_full", &shop_commander::set_code_full, "Shop command string 'set code /full'.")
      .def(
        "set_code_incremental", &shop_commander::set_code_incremental, "Shop command string 'set code /incremental'.")
      .def("set_code_head", &shop_commander::set_code_head, "Shop command string 'set code /head'.")
      .def(
        "set_password",
        &shop_commander::set_password,
        "Shop command string 'set password <value>'.",
        py::arg("key"),
        py::arg("value"))
      .def(
        "start_sim", &shop_commander::start_sim, "Shop command string 'start sim <iterations>'.", py::arg("iterations"))
      .def("start_shopsim", &shop_commander::start_shopsim, "Shop command string 'start shopsim'.")
      .def("log_file", (void(shop_commander::*)()) & shop_commander::log_file, "Shop command string 'log file'.")
      .def(
        "log_file",
        (void(shop_commander::*)(std::string const &)) & shop_commander::log_file,
        "Shop command string 'log file <filename>'.",
        py::arg("filename"))
      .def(
        "log_file_lp",
        &shop_commander::log_file_lp,
        "Shop command string 'log file /lp <filename>'.",
        py::arg("filename"))
      .def("set_xmllog", &shop_commander::set_xmllog, "Shop command string 'set xmllog /on|/off'.", py::arg("on"))
      .def(
        "return_simres",
        &shop_commander::return_simres,
        "Shop command string 'return simres <filename>'.",
        py::arg("filename"))
      .def(
        "return_simres_gen",
        &shop_commander::return_simres_gen,
        "Shop command string 'return simres /gen <filename>'.",
        py::arg("filename"))
      .def(
        "save_series",
        &shop_commander::save_series,
        "Shop command string 'save series <filename>'.",
        py::arg("filename"))
      .def(
        "save_xmlseries",
        &shop_commander::save_xmlseries,
        "Shop command string 'save xmlseries <filename>'.",
        py::arg("filename"))
      .def(
        "print_model",
        &shop_commander::print_model,
        "Shop command string 'print model <filename>'.",
        py::arg("filename"))
      .def(
        "return_scenario_result_table",
        &shop_commander::return_scenario_result_table,
        "Shop command string 'return scenario_result_table <filename>'.",
        py::arg("filename"))
      .def("save_tunnelloss", &shop_commander::save_tunnelloss, "Shop command string 'save tunnelloss'.")
      .def(
        "save_shopsimseries",
        &shop_commander::save_shopsimseries,
        "Shop command string 'save shopsimseries <filename>'.",
        py::arg("filename"))
      .def(
        "return_shopsimres",
        &shop_commander::return_shopsimres,
        "Shop command string 'return shopsimres <filename>'.",
        py::arg("filename"))
      .def(
        "return_shopsimres_gen",
        &shop_commander::return_shopsimres_gen,
        "Shop command string 'return shopsimres /gen <filename>'.",
        py::arg("filename"))
      .def(
        "save_xmlshopsimseries",
        &shop_commander::save_xmlshopsimseries,
        "Shop command string 'save xmlshopsimseries <filename>'.",
        py::arg("filename"))
      .def(
        "save_pq_curves",
        &shop_commander::save_pq_curves,
        py::arg("on"),
        "Shop command string 'save pq_curves /on|/off'.")
      .def<void (shop_commander::*)()>(
        "print_pqcurves_all",
        &shop_commander::print_pqcurves_all,

        "Shop command string 'print pqcurves /all'.")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_all",
        &shop_commander::print_pqcurves_all,
        "Shop command string 'print pqcurves /all <filename>'.",
        py::arg("filename"))
      .def<void (shop_commander::*)()>(
        "print_pqcurves_original",
        &shop_commander::print_pqcurves_original,

        "Shop command string 'print pqcurves /original'.")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_original",
        &shop_commander::print_pqcurves_original,
        "Shop command string 'print pqcurves /original <filename>'.",
        py::arg("filename"))
      .def<void (shop_commander::*)()>(
        "print_pqcurves_convex",
        &shop_commander::print_pqcurves_convex,

        "Shop command string 'print pqcurves /convex'.")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_convex",
        &shop_commander::print_pqcurves_convex,
        "Shop command string 'print pqcurves /convex <filename>'.",
        py::arg("filename"))
      .def<void (shop_commander::*)()>(
        "print_pqcurves_final",
        &shop_commander::print_pqcurves_final,

        "Shop command string 'print pqcurves /final'.")
      .def<void (shop_commander::*)(std::string const &)>(
        "print_pqcurves_final",
        &shop_commander::print_pqcurves_final,
        "Shop command string 'print pqcurves /final <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves",
        &shop_commander::print_mc_curves,
        "Shop command string 'print mc_curves <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up",
        &shop_commander::print_mc_curves_up,
        "Shop command string 'print mc_curves /up <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_down",
        &shop_commander::print_mc_curves_down,
        "Shop command string 'print mc_curves /down <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_pq",
        &shop_commander::print_mc_curves_pq,
        "Shop command string 'print mc_curves /pq <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_mod",
        &shop_commander::print_mc_curves_mod,
        "Shop command string 'print mc_curves /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_pq",
        &shop_commander::print_mc_curves_up_pq,
        "Shop command string 'print mc_curves /up /pq <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_mod",
        &shop_commander::print_mc_curves_up_mod,
        "Shop command string 'print mc_curves /up /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_pq_mod",
        &shop_commander::print_mc_curves_up_pq_mod,
        "Shop command string 'print mc_curves /up /pq /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_down_pq",
        &shop_commander::print_mc_curves_down_pq,
        "Shop command string 'print mc_curves /down /pq <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_down_mod",
        &shop_commander::print_mc_curves_down_mod,
        "Shop command string 'print mc_curves /down /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_down_pq_mod",
        &shop_commander::print_mc_curves_down_pq_mod,
        "Shop command string 'print mc_curves /down /pq /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_down",
        &shop_commander::print_mc_curves_up_down,
        "Shop command string 'print mc_curves /up /down <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_down_pq",
        &shop_commander::print_mc_curves_up_down_pq,
        "Shop command string 'print mc_curves /up /down /pq <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_down_mod",
        &shop_commander::print_mc_curves_up_down_mod,
        "Shop command string 'print mc_curves /up /down /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_up_down_pq_mod",
        &shop_commander::print_mc_curves_up_down_pq_mod,
        "Shop command string 'print mc_curves /up /down /pq /mod <filename>'.",
        py::arg("filename"))
      .def(
        "print_mc_curves_pq_mod",
        &shop_commander::print_mc_curves_pq_mod,
        "Shop command string 'print mc_curves /pq /mod <filename>'.",
        py::arg("filename"))
      .def("print_bp_curves", &shop_commander::print_bp_curves, "Shop command string 'print bp_curves'.")
      .def(
        "print_bp_curves_all_combinations",
        &shop_commander::print_bp_curves_all_combinations,
        "Shop command string 'print bp_curves /all_combinations'.")
      .def(
        "print_bp_curves_current_combination",
        &shop_commander::print_bp_curves_current_combination,
        "Shop command string 'print bp_curves /current_combination'.")
      .def(
        "print_bp_curves_from_zero",
        &shop_commander::print_bp_curves_from_zero,
        "Shop command string 'print bp_curves /from_zero'.")
      .def(
        "print_bp_curves_mc_format",
        &shop_commander::print_bp_curves_mc_format,
        "Shop command string 'print bp_curves /mc_format'.")
      .def(
        "print_bp_curves_operation",
        &shop_commander::print_bp_curves_operation,
        "Shop command string 'print bp_curves /operation'.")
      .def(
        "print_bp_curves_discharge",
        &shop_commander::print_bp_curves_discharge,
        "Shop command string 'print bp_curves /discharge'.")
      .def(
        "print_bp_curves_production",
        &shop_commander::print_bp_curves_production,

        "Shop command string 'print bp_curves /production'.")
      .def(
        "print_bp_curves_dyn_points",
        &shop_commander::print_bp_curves_dyn_points,

        "Shop command string 'print bp_curves /dyn_points'.")
      .def(
        "print_bp_curves_old_points",
        &shop_commander::print_bp_curves_old_points,

        "Shop command string 'print bp_curves /old_points'.")
      .def(
        "print_bp_curves_market_ref_mc",
        &shop_commander::print_bp_curves_market_ref_mc,

        "Shop command string 'print bp_curves /market_ref_mc'.")
      .def(
        "print_bp_curves_no_vertical_step",
        &shop_commander::print_bp_curves_no_vertical_step,

        "Shop command string 'print bp_curves /no_vertical_step'.")
      .def(
        "penalty_flag_all",
        &shop_commander::penalty_flag_all,
        "Shop command string 'penalty flag /all /on|/off'.",
        py::arg("on"))
      .def(
        "penalty_flag_reservoir_ramping",
        &shop_commander::penalty_flag_reservoir_ramping,
        "Shop command string 'penalty flag /on|/off /reservoir /ramping'.",
        py::arg("on"))
      .def(
        "penalty_flag_reservoir_endpoint",
        &shop_commander::penalty_flag_reservoir_endpoint,
        "Shop command string 'penalty flag /on|/off /reservoir /endpoint'.",
        py::arg("on"))
      .def(
        "penalty_flag_load",
        &shop_commander::penalty_flag_load,
        "Shop command string 'penalty flag /on|/off /load'.",
        py::arg("on"))
      .def(
        "penalty_flag_powerlimit",
        &shop_commander::penalty_flag_powerlimit,
        "Shop command string 'penalty flag /on|/off /powerlimit'.",
        py::arg("on"))
      .def(
        "penalty_flag_discharge",
        &shop_commander::penalty_flag_discharge,
        "Shop command string 'penalty flag /on|/off /discharge'.",
        py::arg("on"))
      .def(
        "penalty_flag_plant_min_p_con",
        &shop_commander::penalty_flag_plant_min_p_con,
        "Shop command string 'penalty flag /on|/off /plant /min_p_con'.",
        py::arg("on"))
      .def(
        "penalty_flag_plant_max_p_con",
        &shop_commander::penalty_flag_plant_max_p_con,
        "Shop command string 'penalty flag /on|/off /plant /max_p_con'.",
        py::arg("on"))
      .def(
        "penalty_flag_plant_min_q_con",
        &shop_commander::penalty_flag_plant_min_q_con,
        "Shop command string 'penalty flag /on|/off /plant /min_q_con'.",
        py::arg("on"))
      .def(
        "penalty_flag_plant_max_q_con",
        &shop_commander::penalty_flag_plant_max_q_con,
        "Shop command string 'penalty flag /on|/off /plant /max_q_con'.",
        py::arg("on"))
      .def(
        "penalty_flag_plant_schedule",
        &shop_commander::penalty_flag_plant_schedule,
        "Shop command string 'penalty flag /on|/off /plant /schedule'.",
        py::arg("on"))
      .def(
        "penalty_flag_gate_min_q_con",
        &shop_commander::penalty_flag_gate_min_q_con,
        "Shop command string 'penalty flag /on|/off /gate /min_q_con'.",
        py::arg("on"))
      .def(
        "penalty_flag_gate_max_q_con",
        &shop_commander::penalty_flag_gate_max_q_con,
        "Shop command string 'penalty flag /on|/off /gate /max_q_con'.",
        py::arg("on"))
      .def(
        "penalty_flag_gate_ramping",
        &shop_commander::penalty_flag_gate_ramping,
        "Shop command string 'penalty flag /on|/off /gate /ramping'.",
        py::arg("on"))
      .def(
        "penalty_cost_all",
        &shop_commander::penalty_cost_all,
        py::arg("value"),
        "Shop command string 'penalty cost /all <value>'.")
      .def(
        "penalty_cost_reservoir_ramping",
        &shop_commander::penalty_cost_reservoir_ramping,
        "Shop command string 'penalty cost /reservoir /ramping <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_reservoir_endpoint",
        &shop_commander::penalty_cost_reservoir_endpoint,
        "Shop command string 'penalty cost /reservoir /endpoint <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_load",
        &shop_commander::penalty_cost_load,
        "Shop command string 'penalty cost /load <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_powerlimit",
        &shop_commander::penalty_cost_powerlimit,
        "Shop command string 'penalty cost /powerlimit <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_discharge",
        &shop_commander::penalty_cost_discharge,
        "Shop command string 'penalty cost /discharge <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_gate_ramping",
        &shop_commander::penalty_cost_gate_ramping,
        "Shop command string 'penalty cost /gate /ramping <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_overflow",
        &shop_commander::penalty_cost_overflow,
        "Shop command string 'penalty cost /overflow <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_overflow_time_adjust",
        &shop_commander::penalty_cost_overflow_time_adjust,
        "Shop command string 'penalty cost /overflow_time_adjust <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_reserve",
        &shop_commander::penalty_cost_reserve,
        "Shop command string 'penalty cost /reserve <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_soft_p_penalty",
        &shop_commander::penalty_cost_soft_p_penalty,
        "Shop command string 'penalty cost /soft_p_penalty <value>'.",
        py::arg("value"))
      .def(
        "penalty_cost_soft_q_penalty",
        &shop_commander::penalty_cost_soft_q_penalty,
        "Shop command string 'penalty cost /soft_q_penalty <value>'.",
        py::arg("value"))
      .def("set_newgate", &shop_commander::set_newgate, "Shop command string 'set newgate /on|/off'.", py::arg("on"))
      .def("set_ramping", &shop_commander::set_ramping, py::arg("mode"), "Shop command string 'set ramping <value>'.")
      .def(
        "set_mipgap",
        &shop_commander::set_mipgap,
        "Shop command string 'set mipgap /absolute|/relative <value>'.",
        py::arg("absolute"),
        py::arg("value"))
      .def(
        "set_nseg_all", &shop_commander::set_nseg_all, "Shop command string 'set nseg /all <value>'.", py::arg("value"))
      .def("set_nseg_up", &shop_commander::set_nseg_up, "Shop command string 'set nseg /up <value>'.", py::arg("value"))
      .def(
        "set_nseg_down",
        &shop_commander::set_nseg_down,
        "Shop command string 'set nseg /down <value>'.",
        py::arg("value"))
      .def(
        "set_dyn_seg_on",
        &shop_commander::set_dyn_seg_on,

        "Shop command string 'set dyn_seg /on'.")
      .def(
        "set_dyn_seg_incr",
        &shop_commander::set_dyn_seg_incr,

        "Shop command string 'set dyn_seg /incr'.")
      .def(
        "set_dyn_seg_mip",
        &shop_commander::set_dyn_seg_mip,

        "Shop command string 'set dyn_seg /mip'.")
      .def(
        "set_max_num_threads",
        &shop_commander::set_max_num_threads,
        "Shop command string 'set max_num_threads <value>'.",
        py::arg("value"))
      .def(
        "set_parallel_mode_auto",
        &shop_commander::set_parallel_mode_auto,

        "Shop command string 'set parallel_mode /auto'.")
      .def(
        "set_parallel_mode_deterministic",
        &shop_commander::set_parallel_mode_deterministic,

        "Shop command string 'set parallel_mode /deterministic'.")
      .def(
        "set_parallel_mode_opportunistic",
        &shop_commander::set_parallel_mode_opportunistic,

        "Shop command string 'set parallel_mode /opportunistic'.")
      .def(
        "set_capacity_all",
        &shop_commander::set_capacity_all,
        "Shop command string 'set capacity /all <value>'.",
        py::arg("value"))
      .def(
        "set_capacity_gate",
        &shop_commander::set_capacity_gate,
        "Shop command string 'set capacity /gate <value>'.",
        py::arg("value"))
      .def(
        "set_capacity_bypass",
        &shop_commander::set_capacity_bypass,
        "Shop command string 'set capacity /bypass <value>'.",
        py::arg("value"))
      .def(
        "set_capacity_spill",
        &shop_commander::set_capacity_spill,
        "Shop command string 'set capacity /spill <value>'.",
        py::arg("value"))
      .def(
        "set_headopt_feedback",
        &shop_commander::set_headopt_feedback,
        "Shop command string 'set headopt_feedback <value>'.",
        py::arg("value"))
      .def(
        "set_timelimit",
        &shop_commander::set_timelimit,
        "Shop command string 'set timelimit <value>'.",
        py::arg("value"))
      .def(
        "set_dyn_flex_mip",
        &shop_commander::set_dyn_flex_mip,
        "Shop command string 'set dyn_flex_mip <value>'.",
        py::arg("value"))
      .def(
        "set_universal_mip_on",
        &shop_commander::set_universal_mip_on,

        "Shop command string 'set universal_mip /on'.")
      .def(
        "set_universal_mip_off",
        &shop_commander::set_universal_mip_off,

        "Shop command string 'set universal_mip /off'.")
      .def(
        "set_universal_mip_not_set",
        &shop_commander::set_universal_mip_not_set,

        "Shop command string 'set universal_mip /not_set'.")
      .def("set_merge_on", &shop_commander::set_merge_on, "Shop command string 'set merge /on'.")
      .def("set_merge_off", &shop_commander::set_merge_off, "Shop command string 'set merge /off'.")
      .def(
        "set_merge_stop",
        &shop_commander::set_merge_stop,

        "Shop command string 'set merge /stop'.")
      .def(
        "set_com_dec_period",
        &shop_commander::set_com_dec_period,
        "Shop command string 'set com_dec_period <value>'.",
        py::arg("value"))
      .def(
        "set_time_delay_unit_hour",
        &shop_commander::set_time_delay_unit_hour,

        "Shop command string 'set time_delay_unit hour'.")
      .def(
        "set_time_delay_unit_minute",
        &shop_commander::set_time_delay_unit_minute,

        "Shop command string 'set time_delay_unit minute'.")
      .def(
        "set_time_delay_unit_time_step_length",
        &shop_commander::set_time_delay_unit_time_step_length,

        "Shop command string 'set time_delay_unit time_step_length'.")
      .def(
        "set_power_head_optimization",
        &shop_commander::set_power_head_optimization,
        "Shop command string 'set power_head_optimization /on|/off'.",
        py::arg("on"))
      .def(
        "set_bypass_loss",
        &shop_commander::set_bypass_loss,
        "Shop command string 'set bypass_loss /on|/off'.",
        py::arg("on"))
      .def(
        "set_reserve_slack_cost",
        &shop_commander::set_reserve_slack_cost,
        "Shop command string 'set reserve_slack_cost <value>'.",
        py::arg("value"))
      .def(
        "set_fcr_n_band",
        &shop_commander::set_fcr_n_band,
        "Shop command string 'set fcr_n_band <value>'.",
        py::arg("value"))
      .def(
        "set_fcr_d_band",
        &shop_commander::set_fcr_d_band,
        "Shop command string 'set fcr_d_band <value>'.",
        py::arg("value"))
      .def(
        "set_fcr_n_equality",
        &shop_commander::set_fcr_n_equality,
        "Shop command string 'set fcr_n_equality <value>'.",
        py::arg("value"))
      .def(
        "set_droop_discretization_limit",
        &shop_commander::set_droop_discretization_limit,
        "Shop command string 'set droop_discretization_limit <value>'.",
        py::arg("value"))
      .def(
        "set_reserve_ramping_cost",
        &shop_commander::set_reserve_ramping_cost,
        "Shop command string 'set reserve_ramping_cost <value>'.",
        py::arg("value"))
      .def(
        "set_gen_turn_off_limit",
        &shop_commander::set_gen_turn_off_limit,
        "Shop command string 'set gen_turn_off_limit <value>'.",
        py::arg("value"));
  }

  // Utility functions for Python wrapping of generic shop_system functions that streams
  // data into an std::ostream reference (by default std::cout) given as last argument,
  // to collect the data and return as a string, which is easier to work with from Python.
  template <class T, class Func, class... Args>
  auto string_wrap_streaming_function(T const &obj, Func func, Args... args) {
    std::ostringstream stream;
    (obj.*func)(args..., stream);
    return stream.str();
  }

  template <class Func, class... Args>
  auto string_wrap_streaming_function(Func func, Args... args) {
    std::ostringstream stream;
    func(args..., stream);
    return stream.str();
  }

  void expose_shop_optimize(
    stm_system &stm,
    shyft::core::utctime t0,
    shyft::core::utctime t_end,
    shyft::core::utctimespan dt,
    std::vector<shop_command> const &cmds,
    bool log_to_stdstream,
    bool log_to_files) {
    calendar utc;
    std::size_t n_steps = shyft::core::utcperiod(t0, t_end).diff_units(utc, dt);
    time_axis::generic_dt ta{t0, dt, n_steps};
    return shop_system::optimize(stm, ta, cmds, log_to_stdstream, log_to_files);
  }

  void expose_shop_optimize_ta(
    stm_system &stm,
    shyft::time_axis::generic_dt ta,
    std::vector<shop_command> const &cmds,
    bool log_stream,
    bool log_file) {
    return shop_system::optimize(stm, ta, cmds, log_stream, log_file);
  }

  struct shop_system_ext {
    static shop_system *create_from_period_and_dt(shyft::core::utcperiod p, shyft::core::utctimespan dt) {
      calendar utc;
      std::size_t n_steps = p.diff_units(utc, dt);
      time_axis::generic_dt ta{p.start, dt, n_steps};
      return new shop_system(ta);
    }
  };

  static void expose_shop_system(py::module_ &m) {

    py::class_<shop_system, std::shared_ptr<shop_system>>(
      m, "ShopSystem", "A shop system, managing a session to the shop core.")
      .def(
        py::init(&shop_system_ext::create_from_period_and_dt),
        "Create shop system, initialize with fixed time resolution. note that you should provide valid period and "
        "reasonable dt\n")
      .def(py::init< generic_dt const &>())
      .def_readonly("commander", &shop_system::commander, "Get shop commander object.\n")
      .def("get_version_info", &shop_system::get_version_info, "Get version information from the shop core.")
      .def("set_logging_to_stdstreams", &shop_system::set_logging_to_stdstreams, py::arg("on") = true)
      .def("set_logging_to_files", &shop_system::set_logging_to_files, py::arg("on") = true)
      .def("get_log_buffer", &shop_system::get_log_buffer, py::arg("limit"))
      .def(
        "get_log_buffer",
        +[](shop_system &o) {
          return o.get_log_buffer();
        })
      .def("emit", &shop_system::emit, py::arg("stm_system"), "Emit a stm system into the shop core.")
      .def("collect", &shop_system::collect, py::arg("stm_system"))
      .def("complete", &shop_system::complete, py::arg("stm_system"))
      .def_static(
        "optimize2",
        &expose_shop_optimize,
        py::arg("stm_system"),
        py::arg("time_begin"),
        py::arg("time_end"),
        py::arg("time_step"),
        py::arg("commands"),
        py::arg("logging_to_stdstreams"),
        py::arg("logging_to_files"))
      .def_static(
        "optimize",
        &expose_shop_optimize_ta,
        py::arg("stm_system"),
        py::arg("time_axis"),
        py::arg("commands"),
        py::arg("logging_to_stdstreams"),
        py::arg("logging_to_files"))
      .def(
        "export_yaml_string",
        &shop_system::export_yaml,
        py::arg("input_only") = false,
        py::arg("output_only") = false,
        py::arg("compress_txy") = false,
        py::arg("compress_connection") = false)
      .def(
        "export_topology_string",
        +[](shop_system const &o, bool all, bool raw) -> string {
          return string_wrap_streaming_function(o, &shop_system::export_topology, all, raw);
        },
        py::arg("all") = false,
        py::arg("raw") = false)
      .def(
        "export_topology",
        +[](shop_system const &o, bool all, bool raw) {
          o.export_topology(all, raw, std::cout);
        },
        py::arg("all") = false,
        py::arg("raw") = false)
      .def(
        "export_data_string",
        +[](shop_system const &o, bool all) -> string {
          return string_wrap_streaming_function(o, &shop_system::export_data, all);
        },
        py::arg("all") = false)
      .def(
        "export_data",
        +[](shop_system const &o, bool all) {
          o.export_data(all, std::cout);
        },
        py::arg("all") = false)
      .def("command_raw", &shop_system::command_raw, py::arg("command"))
      .def("get_executed_commands_raw", &shop_system::get_executed_commands_raw)
      .def(
        "environment_string",
        +[] {
          return string_wrap_streaming_function(&shop_system::environment);
        },
        "Get all environment variables as a newline delimited string value.")
      .def(
        "environment",
        +[] {
          shop_system::environment(std::cout);
        },
        "Print all environment variables to standard output.");
  }

#endif

  void pyexport(py::module_ &m) {
    m.attr("__doc__") = "Statkraft Energy Market short term model Shop adapter";
    m.attr("__version__") = shyft::_version_string();
#if defined(SHYFT_WITH_STM)
    auto const stm_module = py::module_::import("shyft.energy_market.stm");
    auto ShopCommandAttr = stm_module.attr("ShopCommand");
    m.attr("ShopCommand") = ShopCommandAttr; // ensure alias here
    auto builtins = py::module_::import("builtins");
    auto list_class = builtins.attr("list");
    m.attr("ShopCommandList") = list_class;
#endif
#if defined(SHYFT_WITH_SHOP)
    m.attr("shyft_with_shop") = true;
    m.attr("shop_api_version") = SHYFT_SHOP_VERSION;
    expose_shop_commander(m);
    expose_shop_system(m);
    {
// TODO: move to submodule in pybind11 - jeh
#define SHYFT_LAMBDA(r, data, name) m.attr(BOOST_PP_STRINGIZE(name##_url)) = custom_attributes::name##_url;
      BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_SHOP_CUSTOM_ATTRIBUTES))
#undef SHYFT_LAMBDA
    }

#else
    m.attr("shyft_with_shop") = false;
    m.attr("shop_api_version") = "";
#endif
  }

}

SHYFT_PYTHON_MODULE(shop, m) {
  shyft::energy_market::stm::shop::pyexport(m);
}
