/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <cstring>
#include <memory>
#include <ranges>
#include <vector>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  py::class_<unit_group, std::shared_ptr<unit_group>> pyexport_model_unit_group_fwd(py::module_ &m) {
    auto ug = expose_component_type<unit_group>(
      m,
      "UnitGroup",
      "A a group of Units, with constraints applicable to the sum of the unit-features (production, flow...), that the "
      "optimization can take into account.");
    return ug;
  }

  void pyexport_model_unit_group(py::module_ &m, py::class_<unit_group, std::shared_ptr<unit_group>> &ug) {

    auto ugo = expose_attributes_type<unit_group::obligation_>(
      ug,
      "Obligation",
      doc.intro("This describes the obligation aspects of the unit group that the members should satisfy.")());
    _add_proxy_property(
      ugo, "schedule", unit_group::obligation_, schedule, "_ts: [W] schedule/target that should be met.");
    _add_proxy_property(
      ugo, "cost", unit_group::obligation_, cost, "_ts: [money/W] the cost of not satisfying the schedule.");
    _add_proxy_property(
      ugo, "result", unit_group::obligation_, result, "_ts: [W] the resulting target/slack met after optimization.");
    _add_proxy_property(
      ugo, "penalty", unit_group::obligation_, penalty, "_ts: [money] If target violated, the cost of the violation.");

    auto ugd = expose_attributes_type<unit_group::delivery_>(
      ug, "Delivery", doc.intro("This describes the sum of product delivery aspects for the units.")());
    _add_proxy_property(ugd, "schedule", unit_group::delivery_, schedule, "_ts: [product-unit] sum schedule");
    _add_proxy_property(ugd, "result", unit_group::delivery_, result, "_ts: [product-unit] sum result");
    _add_proxy_property(
      ugd, "realised", unit_group::delivery_, realised, "_ts: [product-unit] sum realised, as historical fact");

    auto ugm = expose_type<unit_group_member>(
      ug,
      "Member",
      "A unit group member, refers to a specific unit along with the time-series that takes care of the temporal "
      "membership/contribution to the unit_group sum.");
    expose_component_list_type<unit_group_member>(ug, "Member");
    ugm.def_readonly("unit", &unit_group_member::unit, doc.intro("Unit: The unit this member represents.")());
    add_proxy_property(
      ugm,
      "active",
      unit_group_member,
      active,
      doc.intro(
        "_ts: [unit-less] if available, this time-series is multiplied with the contribution from the "
        "unit.")());

    ug.def_readwrite(
        "group_type",
        &unit_group::group_type,
        "unit_group_type: Unit group-type, one of GroupType enum, fcr_n_up, fcr_n_down etc.")
      .def_readonly("members", &unit_group::members, "MemberList: unit group members")
      .def_property_readonly("market_area", &unit_group::get_energy_market_area, "MarketArea: Getter market area.")
      .def(
        "add_unit",
        &unit_group::add_unit,
        doc.intro("Adds a unit to the group, maintaining any needed expressions.")
          .parameters()
          .parameter("unit", "Unit", "The unit to be added to the group (sum expressions automagically updated).")
          .parameter(
            "active", "TimeSeries", "Determine the temporal group-member-ship, if empty Ts, then always member.")(),
        py::arg("unit"),
        py::arg("active"))
      .def(
        "remove_unit",
        &unit_group::remove_unit,
        doc.intro("Remove a nunit from the group maintaining any needed expressions.")
          .parameters()
          .parameter(
            "unit", "Unit", "The unit to be removed from the group (sum expressions automagically updated).")(),
        py::arg("unit"))
      .def(
        "_update_sum_expressions",
        &unit_group::update_sum_expressions,
        doc.intro("update the sum-expressions, in the case this has not been done, or is out of sync.")())
      .def_readonly(
        "obligation",
        &unit_group::obligation,
        doc.intro("Obligation: Obligation schedule, cost, results and penalty.")())
      .def_readonly(
        "delivery",
        &unit_group::delivery,
        doc.intro("Delivery: Sum of product/obligation delivery schedule,result and realised for unit-members.")());
    add_proxy_property(
      ug,
      "production",
      unit_group,
      production,
      doc.intro("_ts: [W] the sum resulting production for this unit-group.")());
    add_proxy_property(
      ug, "flow", unit_group, flow, doc.intro("_ts: [m3/s] the sum resulting water flow for this unit-group.")());
  }
}
