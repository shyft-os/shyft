/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <memory>
#include <string>
#include <vector>

#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_power_plant(py::module_& m) {
    auto p = expose_component_type<power_plant, hydro_power::power_plant>(
      m,
      "PowerPlant",
      doc.intro("A power plant keeping stm units.")
        .details("After creating the units, create the power plant,\n"
                 "and add the units to the power plant.")());
    p.def(
      "add_unit",
      +[](std::shared_ptr<power_plant>& pp, std::shared_ptr<unit>& u) {
        hydro_power::power_plant::add_unit(pp, u);
      },
      doc.intro("Add unit to plant.")(),
      py::arg("unit"));
    p.def(
      "add_aggregate", //FIXME: delete
      +[](std::shared_ptr<power_plant>& pp, std::shared_ptr<unit>& u) {
        hydro_power::power_plant::add_unit(pp, u);
      },
      doc.intro("Add unit to plant.")(),
      py::arg("unit"));
    expose_subcomponent_init<stm_hps>(p, "hps");

    add_proxy_property(
      p, "outlet_level", power_plant, outlet_level, "_ts: [masl] Outlet level, time-dependent attribute.");
    add_proxy_property(p, "gross_head", power_plant, gross_head, "_ts: [masl]Gross head, time-dependent attribute.");
    add_proxy_property(p, "mip", power_plant, mip, "_ts: Mip flag.");
    add_proxy_property(p, "unavailability", power_plant, unavailability, "_ts: Unavailability, time series.");

    {
      auto pd = expose_attributes_type<power_plant::discharge_>(p, "Discharge");
      _add_proxy_property(
        pd,
        "constraint_min",
        power_plant::discharge_,
        constraint_min,
        "_ts: [m3/s] Discharge minimum restriction, time series.");
      _add_proxy_property(
        pd,
        "constraint_max",
        power_plant::discharge_,
        constraint_max,
        "_ts: [m3/s] Discharge maximum restriction, time series.");
      _add_proxy_property(
        pd, "schedule", power_plant::discharge_, schedule, "_ts: [m3/s] Discharge schedule, time series.");
      _add_proxy_property(pd, "result", power_plant::discharge_, result, "_ts: [m3/s] Discharge result, time series.");
      _add_proxy_property(
        pd,
        "realised",
        power_plant::discharge_,
        realised,
        "_ts: [m3/s] Discharge realised, usually related/or defined as unit.discharge.realised.");
      _add_proxy_property(
        pd,
        "upstream_level_constraint",
        power_plant::discharge_,
        upstream_level_constraint,
        "_t_xy_: Max discharge limited by upstream water level.");
      _add_proxy_property(
        pd,
        "downstream_level_constraint",
        power_plant::discharge_,
        downstream_level_constraint,
        "_t_xy_: Max discharge limited by downstream water level.");
      _add_proxy_property(
        pd,
        "intake_loss_from_bypass_flag",
        power_plant::discharge_,
        intake_loss_from_bypass_flag,
        "_ts: Headloss of intake affected by bypass discharge.");
      _add_proxy_property(
        pd,
        "ramping_up",
        power_plant::discharge_,
        ramping_up,
        "_ts: [m3/s] Constraint on increasing discharge, time-dependent attribute.");
      _add_proxy_property(
        pd,
        "ramping_down",
        power_plant::discharge_,
        ramping_down,
        "_ts: [m3/s] Constraint on decreasing discharge, time-dependent attribute.");
    }
    {
      auto pp = expose_attributes_type<power_plant::production_>(p, "Production");

      _add_proxy_property(
        pp,
        "constraint_min",
        power_plant::production_,
        constraint_min,
        "_ts: [W] Production minimum restriction, time series.");
      _add_proxy_property(
        pp,
        "constraint_max",
        power_plant::production_,
        constraint_max,
        "_ts: [W] Production maximum restriction, time series.");
      _add_proxy_property(
        pp, "schedule", power_plant::production_, schedule, "_ts: [W] Production schedule, time series.");
      _add_proxy_property(
        pp,
        "realised",
        power_plant::production_,
        realised,
        "_ts: [W] Production realised, usually related to sum of unit.production.realised.");
      _add_proxy_property(
        pp,
        "merge_tolerance",
        power_plant::production_,
        merge_tolerance,
        "_ts: [W] Max deviation in production, time-dependent attribute.");
      _add_proxy_property(
        pp,
        "ramping_up",
        power_plant::production_,
        ramping_up,
        "_ts: [W] Constraint on increasing production, time-dependent attribute.");
      _add_proxy_property(
        pp,
        "ramping_down",
        power_plant::production_,
        ramping_down,
        "_ts: [W] Constraint on decreasing production, time-dependent attribute.");
      _add_proxy_property(pp, "result", power_plant::production_, result, "_ts: [W] Production result, time series.");
      _add_proxy_property(
        pp,
        "instant_max",
        power_plant::production_,
        instant_max,
        "_ts: [W] Computed instant max production for the rotating units");
    }
    {
      auto pbp = expose_attributes_type<power_plant::best_profit_>(
        p, "BestProfit", doc.intro("Describes the best profit (BP) curves of the power plant.")());

      _add_proxy_property(
        pbp,
        "discharge",
        power_plant,
        best_profit_::discharge,
        "_t_xy_: Plant discharge after changing the production of the plant from the current operating point to a new"
        "point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "cost_average",
        power_plant,
        best_profit_::cost_average,
        "_t_xy_: Average production cost after changing the production of the plant from the current operating point"
        "to a new point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "cost_marginal",
        power_plant,
        best_profit_::cost_marginal,
        "_t_xy_: Marginal production cost after changing the production of the plant from the current operating "
        "point to a new point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "cost_commitment",
        power_plant,
        best_profit_::cost_commitment,
        "_t_xy_: Sum of startup costs or shutdown costs after changing the production of the plant from the "
        "current operating point to a new point in the best profit curve of the plant.");
      _add_proxy_property(
        pbp,
        "dynamic_water_value",
        power_plant,
        best_profit_::dynamic_water_value,
        "_ts: Flag determining whether water values in the best profit calculation should be based on dynamic "
        "results per time step from the optimization or the static end value description.");
      auto fed = expose_attributes_type<power_plant::fees_>(
        p, "Fees", doc.intro("Describes other costs when producing.")());
      _add_proxy_property(
        fed,
        "feeding_fee",
        power_plant,
        fees_::feeding_fee,
        "_ts: Extra cost for feeding electricity into the grid. The feeding fee is added as an extra production cost "
        "in the objective function. Tip: Use nan for periods that should have no fee.");
    }
    {
      auto r = expose_attributes_type<power_plant::reserve_>(
        p, "_Reserve", doc.intro("Describes reserve constraints for the units in the power plant.")());
      {
        auto rs = expose_attributes_type<power_plant::reserve_::spec_>(
          r, "_Spec", doc.intro("Describes reserve constraints, SI-units is W.")());
        _add_proxy_property(rs, "min", power_plant::reserve_::spec_, min, "_ts: Minimum reserve for sum of units.");
        _add_proxy_property(rs, "max", power_plant::reserve_::spec_, max, "_ts: Minimum reserve for sum of units.");


        auto rp = expose_attributes_type<power_plant::reserve_::pair_>(
          r, "_Pair", doc.intro("Describes the up and down pair of reserve specification.")());
        rp.def_readonly("up", &power_plant::reserve_::pair_::up, "_Spec: Up reserve specification.");
        rp.def_readonly("down", &power_plant::reserve_::pair_::down, "_Spec: Down reserve specification.");
      }
      r.def_readonly("afrr", &power_plant::reserve_::afrr, "_Pair: aFRR up & down attributes")
        .def_readonly("mfrr", &power_plant::reserve_::mfrr, "_Pair: mFRR up & down attributes");
    }
    p.def_readonly("discharge", &power_plant::discharge, "Discharge: Discharge attributes.")
      .def_readonly("production", &power_plant::production, "Production: Production attributes.")
      .def_readonly("best_profit", &power_plant::best_profit, "BestProfit: BP curves.")
      .def_readonly("fees", &power_plant::fees, "Fees: other costs of production.")
      .def_readonly("reserve", &power_plant::reserve, "_Reserve: Reserve attributes.");
  }
}
