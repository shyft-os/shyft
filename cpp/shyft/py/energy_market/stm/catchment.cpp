/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/catchment.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_catchment(py::module_& m) {
    auto c = expose_component_type<catchment, hydro_power::catchment>(m, "Catchment", "Stm catchment.");
    //expose_system_init(c);
    expose_subcomponent_init<stm_hps>(c, "hps");
    add_proxy_property(c, "inflow_m3s", catchment,inflow_m3s, doc.intro("_ts: discharge m3/s, time series.")());
  }
}
