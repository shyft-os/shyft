/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_reservoir_aggregate(py::module_ &m) {
    auto ra =
      expose_component_type<reservoir_aggregate>(
        m,
        "ReservoirAggregate",
        doc.intro("A reservoir_aggregate keeping stm reservoirs.")
          .details("After creating the reservoirs, create the reservoir aggregate,\n"
                   "and add the reservoirs to it.")());

    expose_subcomponent_init<stm_hps>(ra, "hps");

    ra
      .def_readonly("reservoirs", &reservoir_aggregate::reservoirs)
      .def(
        "add_reservoir",
        &reservoir_aggregate::add_reservoir,
        doc.intro("Adds a reservoir to the reservoir aggregate.")
          .parameters()
        .parameter("reservoir", "Reservoir", "The reservoir to be added to the reservoir aggregate")(),
        py::arg("reservoir"))
      .def(
        "remove_reservoir",
        &reservoir_aggregate::remove_reservoir,
        doc.intro("Remove a reservoir from the reservoir aggregate")
          .parameters()
        .parameter("reservoir", "Reservoir", "The reservoir to be removed from the reservoir aggregate")(),
        py::arg("reservoir"))
      ;
        
    {
      auto rai = expose_attributes_type<reservoir_aggregate::inflow_>(ra, "Inflow");

      _add_proxy_property(
        rai, "schedule", reservoir_aggregate::inflow_, schedule, "_ts: [m3/s] Inflow schedule, time series.");
      _add_proxy_property(
        rai, "realised", reservoir_aggregate::inflow_, realised, "_ts: [m3/s] Inflow realised, time series.");
      _add_proxy_property(
        rai, "result", reservoir_aggregate::inflow_, result, "_ts: [m3/s] Inflow result, time series.");
      ra.def_readonly("inflow", &reservoir_aggregate::inflow, "Inflow: Inflow attributes.");
      auto rav = expose_attributes_type<reservoir_aggregate::volume_>(ra, "Volume");
      {
        auto ravc = expose_attributes_type<reservoir_aggregate::volume_::constraint_>(rav, "Constraints");
        _add_proxy_property(
          ravc,
          "min",
          reservoir_aggregate::volume_::constraint_,
          min,
          "_ts: [m3] Volume constraint minimum, time series.");
        _add_proxy_property(
          ravc,
          "max",
          reservoir_aggregate::volume_::constraint_,
          max,
          "_ts: [m3] Volume constraint maximum, time series.");
        _add_proxy_property(
          ravc,
          "min_penalty",
          reservoir_aggregate::volume_::constraint_,
          min_penalty,
          "_ts: [money/m3] Penalty cost for violating the minimum constraint.");
        _add_proxy_property(
          ravc,
          "max_penalty",
          reservoir_aggregate::volume_::constraint_,
          max_penalty,
          "_ts: [money/m3] Penalty cost for violating the minimum constraint.");
      }
      rav.def_readonly(
        "constraint", &reservoir_aggregate::volume_::constraint, "Constraints: Volume constraint attributes.");
      _add_proxy_property(
        rav, "static_max", reservoir_aggregate::volume_, static_max, "_ts: [m3] Production static max, time series.");
      _add_proxy_property(
        rav, "schedule", reservoir_aggregate::volume_, schedule, "_ts: [m3] Production schedule, time series.");
      _add_proxy_property(
        rav, "realised", reservoir_aggregate::volume_, realised, "_ts: [m3] Production realised, time series.");
      _add_proxy_property(
        rav, "result", reservoir_aggregate::volume_, result, "_ts: [m3] Production result, time series.");
      ra.def_readonly("volume", &reservoir_aggregate::volume, "Volume: Volume attributes.");
    }

  }

}
