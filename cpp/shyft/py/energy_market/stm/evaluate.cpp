/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/stm/evaluate.h>
#include <shyft/py/bindings.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>

namespace shyft::energy_market::stm {
  void pyexport_evaluate_ts_error(py::module_ &m) {
    auto e = py::class_<evaluate_ts_error>(m, "TsEvaluationError", "Ts evaluation error")
      .def(py::init());
    pyapi::expose_members(e);
    pyapi::expose_format(e);
  }
}
