/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <algorithm>
#include <iterator>
#include <memory>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <boost/hof/unpack.hpp>

#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  using shyft::time_series::dd::apoint_ts;

  struct stm_sys_ext {
    static auto to_blob(std::shared_ptr<stm_system> const & m) {
      auto s = stm_system::to_blob(m);
      return std::vector<char>(s.begin(), s.end());
    }

    static auto to_stripped_blob(std::shared_ptr<stm_system> const & m) {
      auto s = stm_system::to_blob_strip(m);
      return std::vector<char>(s.begin(), s.end());
    }

    static auto from_blob(std::vector<char>& blob) {
      std::string s(blob.begin(), blob.end());
      return stm_system::from_blob(s);
    }

    static auto from_stripped_blob(std::vector<char>& blob) {
      std::string s(blob.begin(), blob.end());
      return stm_system::from_blob_strip(s);
    }

    static auto create_power_module(
      std::shared_ptr<stm_system>& sys,
      int id,
      std::string const & name,
      std::string const & json) {
      return stm_builder(sys).create_power_module(id, name, json);
    }

    static auto
      create_network(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_network(id, name, json);
    }

    static auto
      create_wind_farm(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_wind_farm(id, name, json);
    }

    static auto
      create_market_area(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_market_area(id, name, json);
    }

    static auto
      create_hps(std::shared_ptr<stm_system>& sys, int id, std::string const & name, std::string const & json) {
      return stm_builder(sys).create_hydro_power_system(id, name, json);
    }

    static bool patch(std::shared_ptr<stm_system>& sys, stm_patch_op op, std::shared_ptr<stm_system> const & p) {
      return p ? stm::patch(sys, op, *p) : false;
    }

    static bool fix_uplinks(std::shared_ptr<stm_system>& sys) {
      return stm_system::fix_uplinks(sys);
    }
  };

  void pyexport_model_energy_market_area(py::module_& m) {
    auto ma = expose_component_type<energy_market_area>(
      m,
      "MarketArea",
      doc.intro("A market area, with load/price and other properties.")
        .details("Within the market area, there are zero or more energy-\n"
                 "producing/consuming units.")());

    expose_subcomponent_init<stm_system, &energy_market_area::sys_>(ma, "stm_sys", "system");

    ma.def_property(
      "unit_group",
      &energy_market_area::get_unit_group,
      &energy_market_area::set_unit_group,
      "UnitGroup: Getter and setter for unit group.");
    ma.def(
        "remove_unit_group",
        &energy_market_area::remove_unit_group,
        doc.intro("Remove the unit group associated with this market if available.")
          .parameters()
          .returns("unit_group", "UnitGroup", "The newly removed unit group.")())
      .def(
        "transmission_lines_to",
        &energy_market_area::transmission_lines_to,
        doc.intro("Get transmission lines (from this) to MarketArea")
          .parameters()
          .parameter("to", "MarketArea", "MarketArea to get transmission lines to")
          .returns("transmission_line_list", "TransmissionLineList", "List of transmission lines to MarketArea")(),
        py::arg("to_market"))
      .def(
        "transmission_lines_from",
        &energy_market_area::transmission_lines_from,
        doc.intro("Remove the unit group associated with this market if available.")
          .parameters()
          .parameter("from", "MarketArea", "MarketArea to get transmission lines from")
          .returns("transmission_line_list", "TransmissionLineList", "List of transmission lines from MarketArea")(),
        py::arg("from_market"))
      .def(
        "create_busbar_derived_unit_group",
        &energy_market_area::create_busbar_derived_unit_group,
        doc.intro("Add a unit-group of type 'production' to the system and to the market area.")
          .intro("The unit group will derive its units from the associated network busbars.")
          .intro("Units are automatically added/removed if busbars are added/removed on the network.")
          .parameters()
          .parameter("id", "int", "Unique id of the group.")
          .parameter("name", "str", "Unique name of the group.")
          .parameter("json", "str", "Json payload for py customization.")
          .returns("unit_group", "UnitGroup", "The newly created unit-group.")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "");


    add_proxy_property(ma, "load", energy_market_area, load, "_ts: [W] Load.");
    add_proxy_property(ma, "price", energy_market_area, price, "_ts: [Money/J] Price.");
    add_proxy_property(ma, "max_buy", energy_market_area, max_buy, "_ts: [W] Maximum buy.");
    add_proxy_property(ma, "max_sale", energy_market_area, max_sale, "_ts: [W] Maximum sale.");
    add_proxy_property(ma, "buy", energy_market_area, buy, "_ts: [W] Buy result.");
    add_proxy_property(ma, "sale", energy_market_area, sale, "_ts: [W] Sale result.");
    add_proxy_property(ma, "production", energy_market_area, production, "_ts: [W] Production result.");
    add_proxy_property(
      ma,
      "reserve_obligation_penalty",
      energy_market_area,
      reserve_obligation_penalty,
      "_ts: [Money/x] Obligation penalty.");
    {
      auto ts3 = expose_attributes_type<energy_market_area::ts_triplet_>(
        ma, "TsTriplet", doc.intro("describes .realised, .schedule and .result time-series")());
      _add_proxy_property(
        ts3,
        "realised",
        energy_market_area::ts_triplet_,
        realised,
        "_ts: SI-units, realised time-series, as in historical fact");
      _add_proxy_property(
        ts3, "schedule", energy_market_area::ts_triplet_, schedule, "_ts: SI-units, schedule, as in current schedule");
      _add_proxy_property(
        ts3, "result", energy_market_area::ts_triplet_, result, "_ts: SI-units, result, as provided by optimisation");
      shyft::pyapi::expose_format(ts3);

      auto offr = py::class_<energy_market_area::offering_>(
        ma, "Offering", doc.intro("describes actors (supply/demand) offering into the market")());
      offr
        .def_readonly(
          "usage", &energy_market_area::offering_::usage, "TsTriplet: The amount consumed/used of the offering")
        .def_readonly(
          "price",
          &energy_market_area::offering_::price,
          "TsTriplet: Given the usage, the effective price based on bids");
      _add_proxy_property(
        offr, "bids", energy_market_area::offering_, bids, "_t_xy_: time dep x,y, x=price[Money/J],y=amount[W]");

      expose_attributes_type<energy_market_area::ts_triplet_result>(
        ma, "TsTripletResult", doc.intro("describes .realised, .schedule and .result time-series")())
        .def_readonly(
          "realised",
          &energy_market_area::ts_triplet_result::realised,
          "_ts: SI-units, realised time-series, as in historical fact")
        .def_readonly(
          "schedule",
          &energy_market_area::ts_triplet_result::schedule,
          "_ts: SI-units, schedule, as in current schedule")
        .def_readonly(
          "result",
          &energy_market_area::ts_triplet_result::result,
          "_ts: SI-units, result, as provided by optimisation");
    }
    ma.def(
        "get_production",
        &energy_market_area::get_production,
        doc.intro("Get the sum of production contribution for all busbars")
          .parameters()
          .returns(
            "production", "TsTripletResult", "Production contribution sum, as triplet: result,realised,schedule")())
      .def(
        "get_consumption",
        &energy_market_area::get_consumption,
        doc.intro("Get the sum of consumption contribution for all busbars")
          .parameters()
          .returns(
            "consumption", "TsTripletResult", "Consumption contribution sum, as triplet: result,realised,schedule")())
      .def(
        "get_import",
        &energy_market_area::get_import,
        doc.intro("Get the total import of the market area.")
          .intro("If the actual sum of busbar flow is negative, import is 0.")
          .parameters()
          .returns("import", "TsTripletResult", "Market area import, as triplet: result,realised,schedule")())
      .def(
        "get_export",
        &energy_market_area::get_export,
        doc.intro("Get the total export of the market area. (converted to positive value)")
          .intro("If the actual sum of busbar flow is positive, export is 0.")
          .parameters()
          .returns("export", "TsTripletResult", "Market area export, as triplet: result,realised,schedule")());

    ma.def_readwrite("contracts", &energy_market_area::contracts);
    ma.def_readwrite("busbars", &energy_market_area::busbars);

    ma.def_readonly("demand", &energy_market_area::demand, "Offering: The demand side describing the buyers of energy")
      .def_readonly(
        "supply", &energy_market_area::supply, "Offering: The supply side describing the producers of energy");
  }

  py::class_<stm_system, std::shared_ptr<stm_system>> pyexport_model_stm_system_fwd(py::module_& m) {
    auto ss = expose_system_type<stm_system>(
      m, "StmSystem", "A complete stm system, with market areas, and hydro power systems.");
    return ss;
  }

  void pyexport_model_stm_system(py::module_& m, py::class_<stm_system, std::shared_ptr<stm_system>>& ss) {

    ss.def(
      py::init<int, std::string const &, std::string const &>(),
      "Create stm system.",
      py::arg("uid"),
      py::arg("name"),
      py::arg("json") = "");

    expose_url_tag(ss);

    ss.def_readwrite("market_areas", &stm_system::market);
    ss.def_readwrite("hydro_power_systems", &stm_system::hps);
    ss.def_readwrite("networks", &stm_system::networks);
    ss.def_readwrite("wind_farms", &stm_system::wind_farms);
    ss.def_readwrite("contracts", &stm_system::contracts);
    ss.def_readwrite("contract_portfolios", &stm_system::contract_portfolios);
    ss.def_readwrite("power_modules", &stm_system::power_modules);
    ss.def_readwrite("unit_groups", &stm_system::unit_groups);

    ss.def_readonly("summary", &stm_system::summary).def_readonly("run_parameters", &stm_system::run_params);

    ss.def(
        "create_power_module",
        &stm_sys_ext::create_power_module,
        doc.intro("Create stm power module with unique uid.")
          .returns("power_module", "PowerModule", "The new power module.")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = std::string(""))
      .def(
        "create_network",
        &stm_sys_ext::create_network,
        doc.intro("Create stm network with unique uid.").returns("network", "Network", "The new network.")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = std::string(""))
      .def(
        "create_wind_farm",
        &stm_sys_ext::create_wind_farm,
        doc.intro("Create stm network with unique uid.").returns("wind_farm", "WindFarm", "The new wind farm.")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = std::string(""))
      .def(
        "create_market_area",
        &stm_sys_ext::create_market_area,
        doc.intro("Create stm energy market area with unique uid.")
          .returns("market_area", "MarketArea", "The new market area.")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = std::string(""))
      .def(
        "create_hydro_power_system",
        &stm_sys_ext::create_hps,
        doc.intro("Create hydro power system with unique uid.")
          .returns("hps", "HydroPowerSystem", "The new hydro power system.")(),
        py::arg("uid"),
        py::arg("name"),
        py::arg("json") = std::string(""))
      .def(
        "add_unit_group",
        &stm_system::add_unit_group,
        doc.intro("Add an empty named unit-group to the system.")
          .parameters()
          .parameter("id", "int", "Unique id of the group.")
          .parameter("name", "str", "Unique name of the group.")
          .parameter("json", "str", "Json payload for py customization.")
          .parameter("group_type", "int", "One of UnitGroupType values, default 0.")
          .returns("unit_group", "UnitGroup", "The newly created unit-group.")(),
        py::arg("id"),
        py::arg("name"),
        py::arg("json") = "",
        py::arg("group_type") = unit_group_type{});

    ss.def(
      "result_ts_urls",
      +[](stm_system const * model, std::string prefix) {
        auto model_id = url_peek_model_id(prefix);
        std::vector<std::string> urls;
        url_with_planning_outputs(model_id, *model, [&]<typename T>(ignore_t, T const &, std::string_view url) {
          if constexpr (std::is_same_v<T, time_series::dd::apoint_ts>)
            urls.push_back(std::string(url));
        });
        return urls;
      },
      doc.intro("Generate and return all result time series urls for the specified model.")
        .intro("Useful in the context of a dstm client, that have a get_ts method that accepts this list.")
        .parameters()
        .parameter("prefix", "", "Url prefix, like dstm://Mmodel_id.")
        .returns("ts_urls", "StringVector", "A strongly typed list of strings with ready to use ts-urls.")(),
      py::arg("prefix"));

    ss.def(
        "to_blob",
        &stm_sys_ext::to_blob,
        doc.intro("Serialize the model to a blob.").returns("blob", "ByteVector", "Blob form of the model.")())
      .def_static(
        "from_blob",
        &stm_sys_ext::from_blob,
        doc.intro("Re-create a stm system from a previously create blob.")
          .returns("stm_sys", "StmSystem", "A stm system including hydro-power-systems and markets.")(),
        py::arg("blob"))
      .def(
        "to_stripped_blob",
        &stm_sys_ext::to_stripped_blob,
        doc.intro("Serialize the model to a blob, stripping away expressions, json and custom attributes.")
          .returns("blob", "ByteVector", "Blob form of the stripped model.")())
      .def_static(
        "from_stripped_blob",
        &stm_sys_ext::from_stripped_blob,
        doc.intro("Re-create a stm system from a previously created stripped blob.")
          .returns("stm_sys", "StmSystem", "A stripped stm system including hydro-power-systems and markets.")(),
        py::arg("blob"));

    ss.def(
      "patch",
      &stm_sys_ext::patch,
      doc.intro("Patch self with patch `p` using operation `op`.")
        .intro("If operation is ADD, the new objects in p that have id==0, will have auto assigned unique `id`s")
        .intro("For the value of the auto assigned `id` is max(obj.id)+1, in the order of appearance. ")
        .intro("side-effects: The patch `p` is updated with these new object id values.")
        .parameters()
        .parameter("op", "", "Operation is one of ADD,REMOVE_RELATIONS,REMOVE_OBJECTS")
        .parameter("p", "", "The StmSystem describing the patch")
        .returns("result", "", "True if patch operation and type was applied, false if not supported operation")(),
      py::arg("op"),
      py::arg("p"));

    ss.def(
      "fix_uplinks",
      &stm_sys_ext::fix_uplinks,
      doc.intro("Ensure all members(hps,contracts etc.) refers to this system to enable easy py navigation.")
        .intro("Usually only needed in cases where the stm-system is built without proper system refrences for child "
               "objects.")
        .intro("side-effects: Missing uplinks are fixed.")
        .returns("result", "", "True if anything was changed in uplinks")());

    ss.def("set_attr", set_attr, doc.intro("Set an attribute specified by url.")(), py::arg("url"), py::arg("attr"));
    ss.def("get_attr", get_attr, doc.intro("Get an attribute specified by url.")(), py::arg("url"));

    ss.def(
      "set_attrs",
      +[](stm_system& model, std::vector<std::pair<std::string, any_attr>> attrs) {
        std::vector<std::optional<stm::url_resolve_error>> out{};
        out.reserve(attrs.size());
        std::ranges::copy(
          std::views::transform(attrs, boost::hof::unpack([&](auto&&... args) {
                                  return set_attr(model, SHYFT_FWD(args)...);
                                })),
          std::back_inserter(out));
        return out;
      },
      doc.intro("Set attributes specified by url")(),
      py::arg("attrs"));

    ss.def(
      "get_attrs",
      +[](stm_system const & model, std::vector<std::string> urls) {
        std::vector<std::variant<any_attr, url_resolve_error>> out{};
        out.reserve(urls.size());
        std::ranges::copy(
          std::views::transform(
            urls,
            [&](auto&& url) {
              return get_attr(model, SHYFT_FWD(url));
            }),
          std::back_inserter(out));
        return out;
      },
      doc.intro("Get attributes specified by urls.")(),
      py::arg("urls"));
  }
}
