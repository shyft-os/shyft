#include <boost/dll.hpp>

#include <shyft/config.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/version.h>

#if defined(SHYFT_WITH_STM)
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/ui/ui_core.h>
#if defined(SHYFT_WITH_SHOP)
#include <shyft/energy_market/stm/shop/shop_custom_attributes.h>
#endif
namespace shyft::energy_market::ui {
  void pyexport_layout(py::module_ &m) {
    auto o = py::class_<layout_info, std::shared_ptr<layout_info>>(
      m, "LayoutInfo", doc.intro("Provides layout information that can be leveraged by a renderer.")());
    o.def(
       py::init<int, std::string const &, std::string const &>(),
       doc.intro("Construct a LayoutInfo from id, name, and json")(),
       py::arg("id"),
       py::arg("name"),
       py::arg("json") = "")
      .def_readwrite("id", &layout_info::id, "int: identifying number")
      .def_readwrite("name", &layout_info::name, "str: A descriptive name")
      .def_readwrite("json", &layout_info::json, "str: Json-string containing layout information")
      .def(py::self == py::self)
      .def(py::self != py::self);
    pyapi::expose_format(o);
  }
}

namespace shyft::energy_market::stm {
  extern void pyexport(py::module_ &m);

  namespace srv {
    namespace dstm {
      extern void pyexport(py::module_ &m);
    }

    namespace task {
      extern void pyexport(py::module_ &m);
    }

    namespace storage {
      extern void pyexport(py::module_ &m);
    }

    namespace experimental::application {
      extern void pyexport(py::module_ &m);
    }
  }

  void pyexport_shop_command(py::module_ &m);

  void pyexport_backward_compatibility(py::module_ &m) {
    m.attr("_bool") = py::type::of<a_wrap<bool>>();
    m.attr("_i8") = py::type::of<a_wrap<std::int8_t>>();
    m.attr("_i16") = py::type::of<a_wrap<std::int16_t>>();
    m.attr("_u16") = py::type::of<a_wrap<std::uint16_t>>();
    m.attr("_i32") = py::type::of<a_wrap<std::int32_t>>();
    m.attr("_i64") = py::type::of<a_wrap<std::int64_t>>();
    m.attr("_double") = py::type::of<a_wrap<double>>();
    m.attr("_string") = py::type::of<a_wrap<std::string>>();
    m.attr("_unit_group_type") = py::type::of<a_wrap<unit_group_type>>();
    m.attr("_geo_point") = py::type::of<a_wrap<geo_point>>();
    m.attr("_message_list") = py::type::of<a_wrap<std::vector<std::pair<utctime, std::string>>>>();
    m.attr("_t_xy_") = py::type::of<a_wrap<t_xy_>>();
    m.attr("_t_xy_z_list") = py::type::of<a_wrap<t_xyz_list_>>();
    m.attr("_txyz") = py::type::of<a_wrap<t_xyz_>>();
    m.attr("_time_axis") = py::type::of<a_wrap<generic_dt>>();
    m.attr("_ts") = py::type::of<a_wrap<apoint_ts>>();
    m.attr("_turbine_description") = py::type::of<a_wrap<t_turbine_description_>>();
    // re-exported from shyft.energy_market base, using test-results as condition for re-export(its deprecated)
    auto const energy_market = py::module_::import("shyft.energy_market");
    for (
      auto const &name :
      {"t_xy",
       "t_xyz",
       "t_xyz_list",
       "t_turbine_description",
       "XyPointCurve",
       "UnitGroupType",
       "i8_attr",
       "i16_attr",
       "i32_attr",
       "i64_attr",
       "double_attr",
       "bool_attr",
       "string_attr",
       "geo_point_attr",
       "ts_attr",
       "time_axis_attr",
       "turbine_description_attr",
       "t_xy_attr",
       "t_xyz_attr",
       "t_xy_z_list_attr",
       "unit_group_type_attr",
       "message_list_attr"

      }) {
      m.attr(name) = energy_market.attr(name);
    }
  }

  namespace srv::compute {
    void pyexport_state(py::module_ &m) {
      {
        auto c = py::enum_<managed_server_state>(
          m, "ManagedServerState", "Describes the possible states of a managed compute server");
        pyapi::expose_enumerators(c);
      }
      {
        auto c = py::class_<managed_server_status>(m, "ServerStatus", "Status of a managed compute server");
        // FIXME: add ct, then repr (and str) should be executable to create the state.
        auto server_status_str = [](managed_server_status const &s) {
          return fmt::format("ServerStatus()" /*,s.address,s.last_send,s.model_id,s.state*/);
        };
        c.def("__str__", server_status_str);
        c.def("__repr__", server_status_str);
        pyapi::expose_members(c);
      }
    }
  }
}
#endif

SHYFT_PYTHON_MODULE(stm, m) {
  using namespace shyft;
  m.attr("__doc__") = "Shyft Energy Market detailed model";
  m.attr("__version__") = _version_string();
  pyapi::make_python_namespace(m, boost::dll::this_line_location().parent_path() / "stm");
#if defined(SHYFT_WITH_STM)
  m.attr("shyft_with_stm") = true;
  auto const time_series = py::module_::import("shyft.time_series");
  auto const energy_market = py::module_::import("shyft.energy_market");
  // auto const compute = py::module_::import("shyft.energy_market.stm.compute");//need types of this
  energy_market::stm::srv::compute::pyexport_state(m); // used by clients in stm space
  energy_market::stm::pyexport_shop_command(m);        // needed here for intellisense/pybind
  energy_market::stm::pyexport(m);
  energy_market::stm::srv::storage::pyexport(m);
  energy_market::stm::srv::dstm::pyexport(m);
  energy_market::stm::srv::task::pyexport(m);
  energy_market::ui::pyexport_layout(m); // layout types used by application
  energy_market::stm::srv::experimental::application::pyexport(m);
  energy_market::stm::pyexport_backward_compatibility(m);
#else
  m.attr("shyft_with_stm") = false;
#endif
}
