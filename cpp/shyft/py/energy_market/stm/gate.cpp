/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/waterway.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_gate(py::module_& m) {
    auto g = expose_component_type<gate, hydro_power::gate>(
      m,
      "Gate", "Stm gate.");
    expose_system_init(g);
    add_proxy_property(
      g,
      "flow_description",
      gate,
      flow_description,
      doc.intro("_t_xy_z_list: Gate flow description. Flow [m^3/s] as a function of water level [m] for relative gate "
                "opening [%].")());

    add_proxy_property(
      g,
      "flow_description_delta_h",
      gate,
      flow_description_delta_h,
      doc.intro("_t_xy_z_list: Gate flow description. Flow [m^3/s] as a function of water level difference [m] for "
                "relative gate opening [%].")());

    add_proxy_property(g, "cost", gate, cost, doc.intro("_ts: Gate adjustment cost, time series.")());

    {
      auto go = expose_attributes_type<gate::opening_>(g, "Opening");

      {
        auto goc = expose_attributes_type<gate::opening_::constraint_>(go, "Constraints", "Constraint attributes");
        _add_proxy_property(
          goc,
          "positions",
          gate::opening_::constraint_,
          positions,
          "_t_xy_: Predefined gate positions mapped to gate opening.");
        _add_proxy_property(
          goc,
          "continuous",
          gate::opening_::constraint_,
          continuous,
          "_ts: Flag determining whether the gate can be set to anything between predefined positions, time-dependent "
          "attribute.");
        go.def_readonly("constraint", &gate::opening_::constraint, "Constraints: Opening constraint attributes.");
      }

      _add_proxy_property(
        go, "schedule", gate::opening_, schedule, "_ts: Planned opening schedule, value between 0.0 and 1.0.");
      _add_proxy_property(
        go, "realised", gate::opening_, realised, "_ts:Historical opening schedule, value between 0.0 and 1.0.");
      _add_proxy_property(
        go, "result", gate::opening_, result, "_ts:Result opening schedule, value between 0.0 and 1.0.");

      g.def_readonly("opening", &gate::opening);
    }
    {
      auto gd = expose_attributes_type<gate::discharge_>(g, "Discharge");
      {
        auto gdc = expose_attributes_type<gate::discharge_::constraint_>(
          gd,
          "Constraints");
        _add_proxy_property(
          gdc, "min", gate::discharge_::constraint_, min, "_ts: [masl] Discharge constraint minimum, time series.");
        _add_proxy_property(
          gdc, "max", gate::discharge_::constraint_, max, "_ts: [masl] Discharge constraint maximum, time series.");
        gd.def_readonly("constraint", &gate::discharge_::constraint, "Constraints: Discharge constraint attributes.");
      }
      _add_proxy_property(gd, "schedule", gate::discharge_, schedule, "_ts: [m3/s] Discharge schedule restriction.");
      _add_proxy_property(gd, "realised", gate::discharge_, realised, "_ts: [m3/s] Historical discharge restriction.");
      _add_proxy_property(gd, "result", gate::discharge_, result, "_ts: [m3/s] Discharge result.");
      _add_proxy_property(gd, "static_max", gate::discharge_, static_max, "_ts: [m3/s] Maximum discharge.");
      _add_proxy_property(
        gd,
        "merge_tolerance",
        gate::discharge_,
        merge_tolerance,
        "_ts: [m3/s] Maximum deviation in discharge between two timesteps.");

      g.def_readonly("discharge", &gate::discharge);
    }
  }
}
