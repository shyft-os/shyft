#pragma once
#include <stdexcept>
#include <string>
#include <string_view>

#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/formatters.h>
#include <shyft/py/energy_market/module.h>

PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::hps));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::market));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::contracts));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::contract_portfolios));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::networks));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::power_modules));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::unit_groups));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::stm_system::wind_farms));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::network::transmission_lines));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::network::busbars));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::contract::relations));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::contract::power_plants));
PYBIND11_MAKE_OPAQUE(decltype(shyft::energy_market::stm::unit_group::members));

PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::energy_market::stm::unit>>);
PYBIND11_MAKE_OPAQUE(std::vector<std::shared_ptr<shyft::energy_market::stm::reservoir>>);

namespace shyft::energy_market::stm {

  template <typename T, typename... O>
  auto expose_url_tag(py::class_<T,O...> c) {

    auto func = +[](T const & o){
      const int levels = -1;
      const int template_levels = -1;
      std::string res;
      res.reserve(100);
      auto rbi = std::back_inserter(res);
      o.generate_url(rbi, levels, template_levels);
      return res;
    };
    
    c.def_property_readonly(
        "tag",
        func,
        "str: Url tag.")
      ;
  }
  template <typename T>
  auto expose_component_list_type(auto &scope, auto name,auto... args) {
    using V = std::vector<std::shared_ptr<T>>;
    auto v = pyapi::bind_vector<V>(
      scope, fmt::format("{}List", name).c_str(), args...)
      .def("__eq__",
           [](const V &l,const V &r){
             return stm::equal_component_ptrs(l, r);
           })
      .def("__neq__",
           [](const V &l,const V &r){
             return !stm::equal_component_ptrs(l, r);
           })
      ;
    return v;
  }
  template <typename T, typename... O>
  auto expose_component_type(auto &scope, auto name, auto tdoc, auto... args) {
    auto c = expose_system_type<T, O...>(scope, name, tdoc, args...);
    expose_url_tag(c);
    c.def(
      "flattened_attributes",
      +[](T& self) {
        return pyapi::make_flat_attribute_dict(self);
      },
      "Flat dict containing all component attributes.")
      ;
    expose_component_list_type<T>(scope, name);
    return c;
  }

  template <typename U,typename T, typename... O>
  auto expose_subcomponent_init(py::class_<T, O...> &c,const char *name){
    c.def(py::init<int, std::string const &, std::string const &, std::shared_ptr<U> &>(),
          py::arg("uid"), py::arg("name"), py::arg("json"), py::arg(name));
  }
  template <auto U_weak,typename T, typename... O>
  auto expose_subcomponent_owner(py::class_<T, O...> &c,const char *fn){
    c
      .def_property_readonly(
        fn,
        +[](T const &self){
          return std::invoke(U_weak, self);
        },
        "The owner of this object.")
      ;
  }
  template <typename U,auto U_weak,typename T, typename... O>
  auto expose_subcomponent_init(py::class_<T, O...> &c,const char *kwarg,const char *fn){
    expose_subcomponent_init<U>(c, kwarg);
    expose_subcomponent_owner<U_weak>(c, fn);
  }
  template <typename U,auto U_weak,typename T, typename... O>
  auto expose_subcomponent_init(py::class_<T, O...> &c,const char *name){
    expose_subcomponent_init(c, name, name);
  }


  template <typename T, typename... O>
  auto expose_attributes_type(auto &scope, auto name, auto... args) {
    auto c = py::class_<T, O...>(scope, name, "Attribute collection", args...);
    if constexpr(requires { pyapi::expose_format(c); })
      pyapi::expose_format(c); 
    return c;
  }

}

