/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <fmt/core.h>

#include <shyft/energy_market/stm/wind_farm.h>
#include <shyft/energy_market/stm/wind_turbine.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_wind_turbine(py::module_ &m) {

    auto w = expose_component_type<wind_turbine>(
      m,
      "WindTurbine",
      doc.intro(
        "A Wind turbine, `wiki-wind-turbine`_. is a part of wind park.\n\n"
        ".. _wiki-wind-turbine: https://en.wikipedia.org/wiki/Wind_turbine\n")());

    w.def(
      py::init<int, std::string const &, std::string const &>(),
      "Create wind turbine with unique id and name, can later be added to a wind farm.",
      py::arg("uid"),
      py::arg("name"),
      py::arg("json") = "");

    w.def_property_readonly(
      "wind_farm",
      +[](wind_turbine const &self) {
        return self.farm_();
      },
      "StmSystem: The owning/parent system that keeps this WindTurbine.");


    add_proxy_property(
      w,
      "height",
      wind_turbine,
      height,
      doc.intro("_ts: [m] average turbine height for the Wind Farm, time series.")());
    add_proxy_property(
      w,
      "location",
      wind_turbine,
      location,
      doc.intro("_geo_point: [m,m,m] in specified `epsg` of its `WindFarm`, the geo mid-point of the Wind turbine.")());
    add_proxy_property(
      w, "power_curve", wind_turbine, power_curve, doc.intro("_t_xy_: [x=m/s,y=W] the time-dependent power-curve.")());
    {
      auto cost = expose_attributes_type<wind_turbine::cost_>(
        w, "Cost", doc.intro("WindTurbine.cost related attributes, tactical and factual.")());
      pyapi::expose_format(cost);
      _add_proxy_property(cost, "start", wind_turbine::cost_, start, "_ts: [money/#] start cost, time series.");
      _add_proxy_property(cost, "stop", wind_turbine::cost_, stop, "_ts: [money/#] stop cost, time series.");
    }
    {
      auto prod = expose_attributes_type<wind_turbine::production_>(
        w, "Production", doc.intro("WindTurbine.production attributes, amount power produced[W].")());
      pyapi::expose_format(prod);
      _add_proxy_property(
        prod, "schedule", wind_turbine::production_, schedule, "_ts: [W, J/s] The current schedule, time series.");
      _add_proxy_property(
        prod, "realised", wind_turbine::production_, realised, "_ts: [W, J/s] Production realised, time series.");
      _add_proxy_property(
        prod,
        "result",
        wind_turbine::production_,
        result,
        "_ts: [W, J/s] Power produced. As computed by optimization/simulation process.");
      {
        auto upc = expose_attributes_type<wind_turbine::production_::constraint_>(
          prod, "Constraint", doc.intro("Contains the effect constraints to the wind-turbine.")());
        shyft::pyapi::expose_format(upc);
        _add_proxy_property(
          upc, "min", wind_turbine::production_, constraint_::min, "_ts: [W] Production constraint minimum.");
        _add_proxy_property(
          upc, "max", wind_turbine::production_, constraint_::max, "_ts: [W] Production constraint maximum.");
      }
      auto r = expose_attributes_type<wind_turbine::reserve_>(
        prod, "Reserve", doc.intro("WindTurbine.Reserve contains all operational reserve related attributes.")());

      {
        auto rs = expose_attributes_type<wind_turbine::reserve_::spec_>(
          r, "Spec", doc.intro("Describes reserve specification, (.schedule, or min..result..max) SI-units is W.")());
        _add_proxy_property(rs, "schedule", wind_turbine::reserve_::spec_, schedule, "_ts: Reserve schedule.");
        _add_proxy_property(
          rs, "min", wind_turbine::reserve_::spec_, min, "_ts: Reserve minimum of range if no schedule.");
        _add_proxy_property(
          rs, "max", wind_turbine::reserve_::spec_, max, "_ts: Reserve minimum of range if no schedule.");
        _add_proxy_property(rs, "cost", wind_turbine::reserve_::spec_, cost, "_ts: Reserve cost.");
        _add_proxy_property(rs, "result", wind_turbine::reserve_::spec_, result, "_ts: Reserve result.");
        _add_proxy_property(rs, "penalty", wind_turbine::reserve_::spec_, penalty, "_ts: Reserve penalty.");
        _add_proxy_property(rs, "realised", wind_turbine::reserve_::spec_, realised, "_ts: Reserve realised.");
      }
      {
        auto rp = expose_attributes_type<wind_turbine::reserve_::pair_>(
          r, "Pair", doc.intro("Describes the up and down pair of reserve specification.")());
        rp.def_readonly("up", &wind_turbine::reserve_::pair_::up, "Spec: Up reserve specification.");
        rp.def_readonly("down", &wind_turbine::reserve_::pair_::down, "Spec: Down reserve specification.");
      }
      r.def_readonly("fcr_n", &wind_turbine::reserve_::fcr_n, "Pair: FCR_n up, down attributes.")
        .def_readonly("fcr_d", &wind_turbine::reserve_::fcr_d, "Pair: FCR_d up, down attributes.")
        .def_readonly("mfrr", &wind_turbine::reserve_::mfrr, "Pair: mFRR up, down attributes.");

      prod.def_readonly("constraint", &wind_turbine::production_::constraint, "Constraint: relevant for production");
    }
    w.def_readonly("production", &wind_turbine::production, "Production: Production attributes.");
    w.def_readonly("reserve", &wind_turbine::reserve, "Reserve: Operational reserve attributes.");
    w.def_readonly("cost", &wind_turbine::cost, "Cost: cost related tactical and actual attributes.");
  }
}
