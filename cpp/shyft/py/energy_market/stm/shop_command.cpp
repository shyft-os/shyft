
#include <shyft/config.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/version.h>

#if defined(SHYFT_WITH_STM)
#include <shyft/energy_market/stm/shop/shop_command.h>

namespace shyft::energy_market::stm {

  void pyexport_shop_command(py::module_ &m) {
    using shop::shop_command;
    auto t_shop_command =
      py::class_<shop_command>(
        m,
        "ShopCommand",
        "A shop command, which can later be sent to the shop core.\n"
        "\n"
        "In the shop core a command can be described as a string with syntax:\n"
        "'<keyword> <specifier> [/<opt> [/<opt>...]] [[<obj>] [<obj>...]]'.\n"
        "The keyword is a general word that specifies the kind of action, for\n"
        "example 'set', 'save' or 'return'.\n"
        "The specifier identifies what data will be affected by the command,\n"
        "and it is unique for every command, for example 'method', or 'ramping'.\n"
        "Commands can accept one or several pre-set options to further specify\n"
        "the command. These always start with a forward slash, consist of one\n"
        "word only, and if more than one the order is important.\n"
        "Some commands also needs input objects, usually an integer, floating\n"
        "point, or string value."
        "\n")
        .def(
          py::init< std::string const &>(),
          "Create from a single string in shop command language syntax.",
          py::arg("command"))
        .def(
          py::init<
            std::string const &,
            std::string const &,
            std::vector<std::string> const &,
            std::vector<std::string> const &>(),
          "Create from individual components.",
          py::arg("keyword"),
          py::arg("specifier"),
          py::arg("options"),
          py::arg("objects"))
        .def(py::self == py::self)
        .def(py::self != py::self)
        .def_readwrite("keyword", &shop_command::keyword, "keyword of the command")
        .def_readwrite("specifier", &shop_command::specifier, "specifier of the command")
        .def_readwrite("options", &shop_command::options, "list of options")
        .def_readwrite("objects", &shop_command::objects, "list of objects")
        .def_static("set_method_primal", &shop_command::set_method_primal, "Shop command string 'set method /primal'.")
        .def_static("set_method_dual", &shop_command::set_method_dual, "Shop command string 'set method /dual'.")
        .def_static("set_method_baropt", &shop_command::set_method_baropt, "Shop command string 'set method /baropt'.")
        .def_static(
          "set_method_hydbaropt", &shop_command::set_method_hydbaropt, "Shop command string 'set method /hydbaropt'.")
        .def_static(
          "set_method_netprimal", &shop_command::set_method_netprimal, "Shop command string 'set method /netprimal'.")
        .def_static(
          "set_method_netdual", &shop_command::set_method_netdual, "Shop command string 'set method /netdual'.")
        .def_static("set_code_full", &shop_command::set_code_full, "Shop command string 'set code /full'.")
        .def_static(
          "set_code_incremental", &shop_command::set_code_incremental, "Shop command string 'set code /incremental'.")
        .def_static("set_code_head", &shop_command::set_code_head, "Shop command string 'set code /head'.")
        .def_static(
          "set_password",
          &shop_command::set_password,
          "Shop command string 'set password <value>'.",
          py::arg("key"),
          py::arg("value"))
        .def_static(
          "start_sim", &shop_command::start_sim, "Shop command string 'start sim <iterations>'.", py::arg("iterations"))
        .def_static("start_shopsim", &shop_command::start_shopsim, "Shop command string 'start shopsim'.")
        .def_static<shop_command (*)()>("log_file", &shop_command::log_file, "Shop command string 'log file'.")
        .def_static<shop_command (*)(std::string)>(
          "log_file", &shop_command::log_file, "Shop command string 'log file <filename>'.", py::arg("filename"))
        .def_static(
          "log_file_lp",
          &shop_command::log_file_lp,
          "Shop command string 'log file /lp <filename>'.",
          py::arg("filename"))
        .def_static(
          "set_xmllog", &shop_command::set_xmllog, "Shop command string 'set xmllog /on|/off'.", py::arg("on"))
        .def_static(
          "return_simres",
          &shop_command::return_simres,
          "Shop command string 'return simres <filename>'.",
          py::arg("filename"))
        .def_static(
          "return_simres_gen",
          &shop_command::return_simres_gen,
          "Shop command string 'return simres /gen <filename>'.",
          py::arg("filename"))
        .def_static(
          "save_series",
          &shop_command::save_series,
          "Shop command string 'save series <filename>'.",
          py::arg("filename"))
        .def_static(
          "save_xmlseries",
          &shop_command::save_xmlseries,
          "Shop command string 'save xmlseries <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_model",
          &shop_command::print_model,
          "Shop command string 'print model <filename>'.",
          py::arg("filename"))
        .def_static(
          "return_scenario_result_table",
          &shop_command::return_scenario_result_table,
          "Shop command string 'return scenario_result_table <filename>'.",
          py::arg("filename"))
        .def_static("save_tunnelloss", &shop_command::save_tunnelloss, "Shop command string 'save tunnelloss'.")
        .def_static(
          "save_shopsimseries",
          &shop_command::save_shopsimseries,
          "Shop command string 'save shopsimseries <filename>'.",
          py::arg("filename"))
        .def_static(
          "return_shopsimres",
          &shop_command::return_shopsimres,
          "Shop command string 'return shopsimres <filename>'.",
          py::arg("filename"))
        .def_static(
          "return_shopsimres_gen",
          &shop_command::return_shopsimres_gen,
          "Shop command string 'return shopsimres /gen <filename>'.",
          py::arg("filename"))
        .def_static(
          "save_xmlshopsimseries",
          &shop_command::save_xmlshopsimseries,
          "Shop command string 'save xmlshopsimseries <filename>'.",
          py::arg("filename"))
        .def_static(
          "save_pq_curves",
          &shop_command::save_pq_curves,
          (py::arg("on")),
          "Shop command string 'save pq_curves /on|/off'.")
        .def_static<shop_command (*)()>(
          "print_pqcurves_all", &shop_command::print_pqcurves_all, "Shop command string 'print pqcurves /all'.")
        .def_static<shop_command (*)(std::string)>(
          "print_pqcurves_all",
          &shop_command::print_pqcurves_all,
          "Shop command string 'print pqcurves /all <filename>'.",
          py::arg("filename"))
        .def_static<shop_command (*)()>(
          "print_pqcurves_original",
          &shop_command::print_pqcurves_original,
          "Shop command string 'print pqcurves /original'.")
        .def_static<shop_command (*)(std::string)>(
          "print_pqcurves_original",
          &shop_command::print_pqcurves_original,
          "Shop command string 'print pqcurves /original <filename>'.",
          py::arg("filename"))
        .def_static<shop_command (*)()>(
          "print_pqcurves_convex",
          &shop_command::print_pqcurves_convex,
          "Shop command string 'print pqcurves /convex'.")
        .def_static<shop_command (*)(std::string)>(
          "print_pqcurves_convex",
          &shop_command::print_pqcurves_convex,
          "Shop command string 'print pqcurves /convex <filename>'.",
          py::arg("filename"))
        .def_static<shop_command (*)()>(
          "print_pqcurves_final", &shop_command::print_pqcurves_final, "Shop command string 'print pqcurves /final'.")
        .def_static<shop_command (*)(std::string)>(
          "print_pqcurves_final",
          &shop_command::print_pqcurves_final,
          "Shop command string 'print pqcurves /final <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves",
          &shop_command::print_mc_curves,
          "Shop command string 'print mc_curves <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_up",
          &shop_command::print_mc_curves_up,
          "Shop command string 'print mc_curves /up <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_down",
          &shop_command::print_mc_curves_down,
          "Shop command string 'print mc_curves /down <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_pq",
          &shop_command::print_mc_curves_pq,
          "Shop command string 'print mc_curves /pq <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_mod",
          &shop_command::print_mc_curves_mod,
          "Shop command string 'print mc_curves /mod <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_up_pq",
          &shop_command::print_mc_curves_up_pq,
          "Shop command string 'print mc_curves /up /pq <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_up_mod",
          &shop_command::print_mc_curves_up_mod,
          "Shop command string 'print mc_curves /up /mod <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_up_pq_mod",
          &shop_command::print_mc_curves_up_pq_mod,
          "Shop command string 'print mc_curves /up /pq /mod <filename>'.",
          py::arg("filename"))
        .def_static(
          "print_mc_curves_down_pq",
          &shop_command::print_mc_curves_down_pq,
          "Shop command string 'print mc_curves /down /pq <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_down_mod",
          &shop_command::print_mc_curves_down_mod,
          "Shop command string 'print mc_curves /down /mod <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_down_pq_mod",
          &shop_command::print_mc_curves_down_pq_mod,
          "Shop command string 'print mc_curves /down /pq /mod <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_up_down",
          &shop_command::print_mc_curves_up_down,
          "Shop command string 'print mc_curves /up /down <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_up_down_pq",
          &shop_command::print_mc_curves_up_down_pq,
          "Shop command string 'print mc_curves /up /down /pq <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_up_down_mod",
          &shop_command::print_mc_curves_up_down_mod,
          "Shop command string 'print mc_curves /up /down /mod <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_up_down_pq_mod",
          &shop_command::print_mc_curves_up_down_pq_mod,
          "Shop command string 'print mc_curves /up /down /pq /mod <filename>'.",
          py::arg("filename"))

        .def_static(
          "print_mc_curves_pq_mod",
          &shop_command::print_mc_curves_pq_mod,
          "Shop command string 'print mc_curves /pq /mod <filename>'.",
          py::arg("filename"))

        .def_static("print_bp_curves", &shop_command::print_bp_curves, "Shop command string 'print bp_curves'.")

        .def_static(
          "print_bp_curves_all_combinations",
          &shop_command::print_bp_curves_all_combinations,
          "Shop command string 'print bp_curves /all_combinations'.")

        .def_static(
          "print_bp_curves_current_combination",
          &shop_command::print_bp_curves_current_combination,
          "Shop command string 'print bp_curves /current_combination'.")

        .def_static(
          "print_bp_curves_from_zero",
          &shop_command::print_bp_curves_from_zero,
          "Shop command string 'print bp_curves /from_zero'.")

        .def_static(
          "print_bp_curves_mc_format",
          &shop_command::print_bp_curves_mc_format,
          "Shop command string 'print bp_curves /mc_format'.")

        .def_static(
          "print_bp_curves_operation",
          &shop_command::print_bp_curves_operation,
          "Shop command string 'print bp_curves /operation'.")

        .def_static(
          "print_bp_curves_discharge",
          &shop_command::print_bp_curves_discharge,
          "Shop command string 'print bp_curves /discharge'.")

        .def_static(
          "print_bp_curves_production",
          &shop_command::print_bp_curves_production,
          "Shop command string 'print bp_curves /production'.")

        .def_static(
          "print_bp_curves_dyn_points",
          &shop_command::print_bp_curves_dyn_points,
          "Shop command string 'print bp_curves /dyn_points'.")

        .def_static(
          "print_bp_curves_old_points",
          &shop_command::print_bp_curves_old_points,
          "Shop command string 'print bp_curves /old_points'.")

        .def_static(
          "print_bp_curves_market_ref_mc",
          &shop_command::print_bp_curves_market_ref_mc,
          "Shop command string 'print bp_curves /market_ref_mc'.")

        .def_static(
          "print_bp_curves_no_vertical_step",
          &shop_command::print_bp_curves_no_vertical_step,
          "Shop command string 'print bp_curves /no_vertical_step'.")

        .def_static(
          "penalty_flag_all",
          &shop_command::penalty_flag_all,
          "Shop command string 'penalty flag /all /on|/off'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_reservoir_ramping",
          &shop_command::penalty_flag_reservoir_ramping,
          "Shop command string 'penalty flag /on|/off /reservoir /ramping'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_reservoir_endpoint",
          &shop_command::penalty_flag_reservoir_endpoint,
          "Shop command string 'penalty flag /on|/off /reservoir /endpoint'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_load",
          &shop_command::penalty_flag_load,
          "Shop command string 'penalty flag /on|/off /load'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_powerlimit",
          &shop_command::penalty_flag_powerlimit,
          "Shop command string 'penalty flag /on|/off /powerlimit'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_discharge",
          &shop_command::penalty_flag_discharge,
          "Shop command string 'penalty flag /on|/off /discharge'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_plant_min_p_con",
          &shop_command::penalty_flag_plant_min_p_con,
          "Shop command string 'penalty flag /on|/off /plant /min_p_con'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_plant_max_p_con",
          &shop_command::penalty_flag_plant_max_p_con,
          "Shop command string 'penalty flag /on|/off /plant /max_p_con'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_plant_min_q_con",
          &shop_command::penalty_flag_plant_min_q_con,
          "Shop command string 'penalty flag /on|/off /plant /min_q_con'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_plant_max_q_con",
          &shop_command::penalty_flag_plant_max_q_con,
          "Shop command string 'penalty flag /on|/off /plant /max_q_con'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_plant_schedule",
          &shop_command::penalty_flag_plant_schedule,
          "Shop command string 'penalty flag /on|/off /plant /schedule'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_gate_min_q_con",
          &shop_command::penalty_flag_gate_min_q_con,
          "Shop command string 'penalty flag /on|/off /gate /min_q_con'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_gate_max_q_con",
          &shop_command::penalty_flag_gate_max_q_con,
          "Shop command string 'penalty flag /on|/off /gate /max_q_con'.",
          py::arg("on"))

        .def_static(
          "penalty_flag_gate_ramping",
          &shop_command::penalty_flag_gate_ramping,
          "Shop command string 'penalty flag /on|/off /gate /ramping'.",
          py::arg("on"))

        .def_static(
          "penalty_cost_all",
          &shop_command::penalty_cost_all,
          "Shop command string 'penalty cost /all <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_reservoir_ramping",
          &shop_command::penalty_cost_reservoir_ramping,
          "Shop command string 'penalty cost /reservoir /ramping <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_reservoir_endpoint",
          &shop_command::penalty_cost_reservoir_endpoint,
          "Shop command string 'penalty cost /reservoir /endpoint <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_load",
          &shop_command::penalty_cost_load,
          "Shop command string 'penalty cost /load <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_powerlimit",
          &shop_command::penalty_cost_powerlimit,
          "Shop command string 'penalty cost /powerlimit <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_discharge",
          &shop_command::penalty_cost_discharge,
          "Shop command string 'penalty cost /discharge <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_gate_ramping",
          &shop_command::penalty_cost_gate_ramping,
          "Shop command string 'penalty cost /gate /ramping <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_overflow",
          &shop_command::penalty_cost_overflow,
          "Shop command string 'penalty cost /overflow <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_overflow_time_adjust",
          &shop_command::penalty_cost_overflow_time_adjust,
          "Shop command string 'penalty cost /overflow_time_adjust <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_reserve",
          &shop_command::penalty_cost_reserve,
          "Shop command string 'penalty cost /reserve <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_soft_p_penalty",
          &shop_command::penalty_cost_soft_p_penalty,
          "Shop command string 'penalty cost /soft_p_penalty <value>'.",
          py::arg("value"))

        .def_static(
          "penalty_cost_soft_q_penalty",
          &shop_command::penalty_cost_soft_q_penalty,
          "Shop command string 'penalty cost /soft_q_penalty <value>'.",
          py::arg("value"))

        .def_static(
          "set_newgate", &shop_command::set_newgate, (py::arg("on")), "Shop command string 'set newgate /on|/off'.")

        .def_static(
          "set_ramping", &shop_command::set_ramping, (py::arg("mode")), "Shop command string 'set ramping <value>'.")

        .def_static(
          "set_mipgap",
          &shop_command::set_mipgap,
          "Shop command string 'set mipgap /absolute|/relative <value>'.",
          py::arg("absolute"),
          py::arg("value"))

        .def_static(
          "set_nseg_all", &shop_command::set_nseg_all, "Shop command string 'set nseg /all <value>'.", py::arg("value"))

        .def_static(
          "set_nseg_up", &shop_command::set_nseg_up, "Shop command string 'set nseg /up <value>'.", py::arg("value"))

        .def_static(
          "set_nseg_down",
          &shop_command::set_nseg_down,
          "Shop command string 'set nseg /down <value>'.",
          py::arg("value"))

        .def_static("set_dyn_seg_on", &shop_command::set_dyn_seg_on, "Shop command string 'set dyn_seg /on'.")

        .def_static("set_dyn_seg_incr", &shop_command::set_dyn_seg_incr, "Shop command string 'set dyn_seg /incr'.")

        .def_static("set_dyn_seg_mip", &shop_command::set_dyn_seg_mip, "Shop command string 'set dyn_seg /mip'.")

        .def_static(
          "set_max_num_threads",
          &shop_command::set_max_num_threads,
          "Shop command string 'set max_num_threads <value>'.",
          py::arg("value"))
        .def_static(
          "set_parallel_mode_auto",
          &shop_command::set_parallel_mode_auto,
          "Shop command string 'set parallel_mode /auto'.")

        .def_static(
          "set_parallel_mode_deterministic",
          &shop_command::set_parallel_mode_deterministic,
          "Shop command string 'set parallel_mode /deterministic'.")

        .def_static(
          "set_parallel_mode_opportunistic",
          &shop_command::set_parallel_mode_opportunistic,
          "Shop command string 'set parallel_mode /opportunistic'.")

        .def_static(
          "set_capacity_all",
          &shop_command::set_capacity_all,
          "Shop command string 'set capacity /all <value>'.",
          py::arg("value"))

        .def_static(
          "set_capacity_gate",
          &shop_command::set_capacity_gate,
          "Shop command string 'set capacity /gate <value>'.",
          py::arg("value"))

        .def_static(
          "set_capacity_bypass",
          &shop_command::set_capacity_bypass,
          "Shop command string 'set capacity /bypass <value>'.",
          py::arg("value"))

        .def_static(
          "set_capacity_spill",
          &shop_command::set_capacity_spill,
          "Shop command string 'set capacity /spill <value>'.",
          py::arg("value"))

        .def_static(
          "set_headopt_feedback",
          &shop_command::set_headopt_feedback,
          "Shop command string 'set headopt_feedback <value>'.",
          py::arg("value"))

        .def_static(
          "set_timelimit",
          &shop_command::set_timelimit,
          "Shop command string 'set timelimit <value>'.",
          py::arg("value"))

        .def_static(
          "set_dyn_flex_mip",
          &shop_command::set_dyn_flex_mip,
          "Shop command string 'set dyn_flex_mip <value>'.",
          py::arg("value"))

        .def_static(
          "set_universal_mip_on", &shop_command::set_universal_mip_on, "Shop command string 'set universal_mip /on'.")

        .def_static(
          "set_universal_mip_off",
          &shop_command::set_universal_mip_off,
          "Shop command string 'set universal_mip /off'.")

        .def_static(
          "set_universal_mip_not_set",
          &shop_command::set_universal_mip_not_set,
          "Shop command string 'set universal_mip /not_set'.")

        .def_static("set_merge_on", &shop_command::set_merge_on, "Shop command string 'set merge /on'.")

        .def_static("set_merge_off", &shop_command::set_merge_off, "Shop command string 'set merge /off'.")

        .def_static("set_merge_stop", &shop_command::set_merge_stop, "Shop command string 'set merge /stop'.")

        .def_static(
          "set_com_dec_period",
          &shop_command::set_com_dec_period,
          "Shop command string 'set com_dec_period <value>'.",
          py::arg("value"))

        .def_static(
          "set_time_delay_unit_hour",
          &shop_command::set_time_delay_unit_hour,
          "Shop command string 'set time_delay_unit hour'.")

        .def_static(
          "set_time_delay_unit_minute",
          &shop_command::set_time_delay_unit_minute,
          "Shop command string 'set time_delay_unit minute'.")

        .def_static(
          "set_time_delay_unit_time_step_length",
          &shop_command::set_time_delay_unit_time_step_length,
          "Shop command string 'set time_delay_unit time_step_length'.")

        .def_static(
          "set_power_head_optimization",
          &shop_command::set_power_head_optimization,
          "Shop command string 'set power_head_optimization /on|/off'.",
          py::arg("on"))

        .def_static(
          "set_bypass_loss",
          &shop_command::set_bypass_loss,
          "Shop command string 'set bypass_loss /on|/off'.",
          py::arg("on"))

        .def_static(
          "set_reserve_slack_cost",
          &shop_command::set_reserve_slack_cost,
          "Shop command string 'set reserve_slack_cost <value>'.",
          py::arg("value"))

        .def_static(
          "set_fcr_n_band",
          &shop_command::set_fcr_n_band,
          "Shop command string 'set fcr_n_band <value>'.",
          py::arg("value"))

        .def_static(
          "set_fcr_d_band",
          &shop_command::set_fcr_d_band,
          "Shop command string 'set fcr_d_band <value>'.",
          py::arg("value"))

        .def_static(
          "set_fcr_n_equality",
          &shop_command::set_fcr_n_equality,
          "Shop command string 'set fcr_n_equality <value>'.",
          py::arg("value"))

        .def_static(
          "set_droop_discretization_limit",
          &shop_command::set_droop_discretization_limit,
          "Shop command string 'set droop_discretization_limit <value>'.",
          py::arg("value"))

        .def_static(
          "set_reserve_ramping_cost",
          &shop_command::set_reserve_ramping_cost,
          "Shop command string 'set reserve_ramping_cost <value>'.",
          py::arg("value"))

        .def_static(
          "set_gen_turn_off_limit",
          &shop_command::set_gen_turn_off_limit,
          "Shop command string 'set gen_turn_off_limit <value>'.",
          py::arg("value"));

    t_shop_command.def(
      "__str__", +[](shop_command const &c) {
        return c.operator std::string();
      });
    t_shop_command.def(
      "__repr__", +[](shop_command const &c) {
        return fmt::format("ShopCommand({})", c.operator std::string());
      });
  }

}
#endif
