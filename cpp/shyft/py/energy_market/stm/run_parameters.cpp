/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>
#include <fmt/std.h>

#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/py/bindings.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_run_parameters(py::module_ &m) {
    auto rp = py::class_<run_parameters>(
      m, "RunParameters", "A set of parameters from a simulation- or optimization run. It is a part of the StmSystem class.");
    rp.def_readwrite("n_inc_runs", &run_parameters::n_inc_runs, "int: Number of incremental runs.")
      .def_readwrite("n_full_runs", &run_parameters::n_full_runs, "int: Number of full runs.")
      .def_readwrite("head_opt", &run_parameters::head_opt, "bool: Whether head optimization is turned on.");

    add_proxy_property(rp, "fx_log", run_parameters, fx_log, "MessageList: from the fx-callback.");
    add_proxy_property(rp, "run_time_axis", run_parameters, run_time_axis, "TimeAxis: The time axis for the SHOP run.");
  }
}
