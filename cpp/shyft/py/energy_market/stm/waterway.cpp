/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <fmt/core.h>

#include <shyft/energy_market/stm/waterway.h>
#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/energy_market/stm/attr_wrap.h>
#include <shyft/py/energy_market/stm/submodule.h>
#include <shyft/py/formatters.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

  void pyexport_model_waterway(py::module_ &m) {
    auto w = expose_component_type<waterway, hydro_power::waterway>(m, "Waterway", "Stm waterway.");

    expose_subcomponent_init<stm_hps>(w, "hps");

    {
      auto wd = expose_attributes_type<waterway::discharge_>(w, "Discharge");

      _add_proxy_property(
        wd,
        "static_max",
        waterway::discharge_,
        static_max,
        "_ts: [m3/s] Discharge maximum, time-dependent attribute, bi-directional value.");
      _add_proxy_property(
        wd,
        "result",
        waterway::discharge_,
        result,
        "_ts: [m3/s] Discharge result. As computed by optimization/simulation process.");
      _add_proxy_property(
        wd, "schedule", waterway::discharge_, schedule, "_ts: [m3/s] Discharge schedule. As in wanted scheduled flow.");
      _add_proxy_property(
        wd,
        "reference",
        waterway::discharge_,
        reference,
        "_ts: [m3/s] Discharge reference. Used as reference for the constraint criteria (might be different from "
        ".schedule).");
      _add_proxy_property(
        wd,
        "realised",
        waterway::discharge_,
        realised,
        "_ts: [m3/s] Discharge realised. - as in historical fact. For the case of constraint, used to "
        "establish initial accumulated deviation at the start of the optimisation period.");

      {
        auto wdc = expose_attributes_type<waterway::discharge_::constraint_>(
          wd,
          "Constraints",
          doc.intro(
            "The constraints for a waterway, provide means of controlling the flow, change of flow, or even "
            "accumulated volume of flow.")());
        _add_proxy_property(
          wdc, "min", waterway::discharge_::constraint_, min, "_ts: [m3/s] Discharge constraint min flow.");
        _add_proxy_property(
          wdc, "max", waterway::discharge_::constraint_, max, "_ts: [m3/s] Discharge constraint max flow.");
        _add_proxy_property(
          wdc,
          "ramping_up",
          waterway::discharge_::constraint_,
          ramping_up,
          "_ts: [m3/s] Discharge constraint ramping_up.");
        _add_proxy_property(
          wdc,
          "ramping_down",
          waterway::discharge_::constraint_,
          ramping_down,
          "_ts: [m3/s] Discharge constraint ramping_down.");
        _add_proxy_property(
          wdc,
          "accumulated_min",
          waterway::discharge_::constraint_,
          accumulated_min,
          "_ts: [m3] allowed accumulated negative deviation volume, actual vs .reference. If set to nan at a timestep, "
          "the accumulator is reset to 0 starting from that timestep.");
        _add_proxy_property(
          wdc,
          "accumulated_max",
          waterway::discharge_::constraint_,
          accumulated_max,
          "_ts: [m3] allowed accumulated positive deviation volume, actual vs .reference. If set to nan at a timestep, "
          "the accumulator is reset to 0 starting from that timestep.");
        _add_proxy_property(
          wdc,
          "min_average",
          waterway::discharge_::constraint_,
          min_average,
          "_t_xy: [s][m3/s] minimum average discharge on waterway as a function of duration in seconds.");
        _add_proxy_property(
          wdc,
          "max_average",
          waterway::discharge_::constraint_,
          max_average,
          "_t_xy: [s][m3/s] maximum average discharge on waterway as a function of duration in seconds.");


        auto wdp = expose_attributes_type<waterway::discharge_::penalty_>(wd, "Penalties");

        {
          auto wdpc = expose_attributes_type<waterway::discharge_::penalty_::cost_>(wdp, "Costs");
          _add_proxy_property(
            wdpc,
            "rate",
            waterway::discharge_::penalty_::cost_,
            rate,
            "_ts: [money/(m3/s)] Penalty cost for rate of discharge.");
          _add_proxy_property(
            wdpc,
            "constraint_min",
            waterway::discharge_::penalty_::cost_,
            constraint_min,
            "_ts: [m3/s] Penalty cost constraint_min.");
          _add_proxy_property(
            wdpc,
            "constraint_max",
            waterway::discharge_::penalty_::cost_,
            constraint_max,
            "_ts: [m3/s] Penalty cost constraint_max.");
          _add_proxy_property(
            wdpc,
            "ramping_up",
            waterway::discharge_::penalty_::cost_,
            ramping_up,
            "_ts: [m3/s] Penalty cost ramping_up.");
          _add_proxy_property(
            wdpc,
            "ramping_down",
            waterway::discharge_::penalty_::cost_,
            ramping_down,
            "_ts: [m3/s] Penalty cost ramping_down.");
          _add_proxy_property(
            wdpc,
            "accumulated_min",
            waterway::discharge_::penalty_::cost_,
            accumulated_min,
            "_ts: [money/m3] Penalty cost accumulated_min.");
          _add_proxy_property(
            wdpc,
            "accumulated_max",
            waterway::discharge_::penalty_::cost_,
            accumulated_max,
            "_ts: [money/m3] Penalty cost constraint accumulated_max.");
          _add_proxy_property(
            wdpc,
            "peak_curve",
            waterway::discharge_::penalty_::cost_,
            peak_curve,
            "_t_xy: [m3/s][nok/m3/s] Cost of peak flow curve.");
          _add_proxy_property(
            wdpc,
            "min_average",
            waterway::discharge_::penalty_::cost_,
            min_average,
            "_ts: [(money/h)/(m3/s)] Penalty cost for constraint min_average_discharge.");
          _add_proxy_property(
            wdpc,
            "max_average",
            waterway::discharge_::penalty_::cost_,
            max_average,
            "_ts: [(money/h)/(m3/s)] Penalty cost for constraint max_average_discharge.");


          auto wdpr = expose_attributes_type<waterway::discharge_::penalty_::result_>(wdp, "Results");
          _add_proxy_property(
            wdpr,
            "rate",
            waterway::discharge_::penalty_::result_,
            rate,
            "_ts: [money] Resulting penalty for discharge.");
          _add_proxy_property(
            wdpr,
            "constraint_min",
            waterway::discharge_::penalty_::result_,
            constraint_min,
            "_ts: [money] Resulting penalty for violating min constraint.");
          _add_proxy_property(
            wdpr,
            "constraint_max",
            waterway::discharge_::penalty_::result_,
            constraint_max,
            "_ts: [money] Resulting penalty for violating max constraint.");
          _add_proxy_property(
            wdpr,
            "ramping_up",
            waterway::discharge_::penalty_::result_,
            ramping_up,
            "_ts: [money] Resulting penalty for violating ramping up constraint.");
          _add_proxy_property(
            wdpr,
            "ramping_down",
            waterway::discharge_::penalty_::result_,
            ramping_down,
            "[money] Resulting penalty for violating ramping down constraint.");
          _add_proxy_property(
            wdpr,
            "accumulated_min",
            waterway::discharge_::penalty_::result_,
            accumulated_min,
            "_ts: [money] Resulting penalty for violating accumulated_min,");
          _add_proxy_property(
            wdpr,
            "accumulated_max",
            waterway::discharge_::penalty_::result_,
            accumulated_max,
            "_ts: [money] Resulting penalty for violating accumulated_max.");
        }
        wdp.def_readonly("cost", &waterway::discharge_::penalty_::cost, "Costs: Penalty cost attributes.")
          .def_readonly("result", &waterway::discharge_::penalty_::result, "Results: Penalty result attributes.");


        wd.def_readonly("constraint", &waterway::discharge_::constraint, "Constraints: Discharge constraint attributes")
          .def_readonly("penalty", &waterway::discharge_::penalty, "Penalties: Discharge penalty attributes.");
      }

      auto wg = expose_attributes_type<waterway::geometry_>(w, "Geometry");

      _add_proxy_property(
        wg, "length", waterway::geometry_, length, "_ts: [m] Tunnel length, time-dependent attribute.");
      _add_proxy_property(
        wg, "diameter", waterway::geometry_, diameter, "_ts: [m] Tunnel diameter, time-dependent attribute.");
      _add_proxy_property(
        wg, "z0", waterway::geometry_, z0, "_ts: [masl] Tunnel inlet level, time-dependent attribute.");
      _add_proxy_property(
        wg, "z1", waterway::geometry_, z1, "_ts: [masl] Tunnel outlet level, time-dependent attribute.");

      auto wfdev = expose_attributes_type<waterway::deviation_>(w, "_Deviation");

      _add_proxy_property(
        wfdev, "realised", waterway::deviation_, realised, "_ts: [m3] Waterway deviation realised attribute");
      _add_proxy_property(
        wfdev, "result", waterway::deviation_, result, "_ts: [m3] Waterway deviation result attribute");
      auto wfd = expose_attributes_type<waterway::flow_description_>(w, "FlowDescription");

      _add_proxy_property(
        wfd, "upstream_ref", waterway::flow_description_, upstream_ref, "_t_xy: [m3/s] Discharge constraint min flow.");

      {
        auto wfddm = expose_attributes_type<waterway::flow_description_::delta_meter_>(
          wfd, "DeltaMeter", doc.intro("Delta meter flow description of the waterway.")());
        _add_proxy_property(
          wfddm,
          "upstream_ref",
          waterway::flow_description_::delta_meter_,
          upstream_ref,
          "_t_xy_z_list: x=difference between up- and downstream level [m], y=flow [m3/s]. z=upstream level [masl]");
        _add_proxy_property(
          wfddm,
          "downstream_ref",
          waterway::flow_description_::delta_meter_,
          downstream_ref,
          "_t_xy_z_list: x=difference between up- and downstream level [m], y=flow [m3/s]. z=downstream level [masl]");
      }
      wfd.def_readonly(
        "delta_meter", &waterway::flow_description_::delta_meter, "DeltaMeter: Delta meter description.");

      w.def_readonly("geometry", &waterway::geometry, "Geometry: Geometry attributes.")
        .def_readonly("discharge", &waterway::discharge, "Discharge: Discharge attributes.")
        .def_readonly("flow_description", &waterway::flow_description, "FlowDescription: Flow describing attributes.")
        .def_readonly("deviation", &waterway::deviation, "_Deviation: Deviation attributes. ")
        .def(
          "add_gate",
          +[](std::shared_ptr<waterway> &self, int uid, std::string const &name, std::string const &json) {
            auto hps = std::static_pointer_cast<stm_hps>(self->hps.lock());
            gate_ gate = stm_hps_builder(hps).create_gate(uid, name, json);
            waterway::add_gate(self, gate);
            return gate;
          },
          "Create and add a new gate to the waterway.",
          py::arg("uid"),
          py::arg("name"),
          py::arg("json") = std::string(""))
        .def(
          "add_gate",
          +[](std::shared_ptr<waterway> &self, std::shared_ptr<gate> &gate) {
            waterway::add_gate(self, gate);
            return gate;
          },
          "Add an existing gate to the waterway.",
          py::arg("gate"));
      add_proxy_property(
        w, "head_loss_coeff", waterway, head_loss_coeff, "_ts: Loss factor, time-dependent attribute.");
      add_proxy_property(
        w, "head_loss_func", waterway, head_loss_func, "_t_xy_z_list: Loss function, time-dependent attribute.");
      add_proxy_property(w, "delay", waterway, delay, "_t_xy_: Time delay of flow.");
    }
  }
}
