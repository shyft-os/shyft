/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdint>
#include <future>
#include <iterator>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <shyft/core/reflection.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_binary.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/dtss_url.h>
#include <shyft/dtss/queue_msg.h>
#include <shyft/dtss/store_policy.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/py/reflection.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/web_api/dtss_web_api.h>

// also consider policy: from
// https://www.codevate.com/blog/7-concurrency-with-embedded-python-in-a-multi-threaded-c-application

namespace shyft::dtss {

  struct py_server final
    : server_state
    , binary_server {
    web_api::request_handler bg_server; ///< handle web-api requests
    std::future<int> web_srv;           ///<

    explicit py_server(srv::fast_server_iostream_config cfg)
      : server_state()
      , binary_server{*static_cast<server_state *>(this), server_config{.config = std::move(cfg)}} {
      bg_server.state = this;

      // FIXME: just don't expect this - jeh
      auto set_throw_if_empty = []<typename R, typename... A>(std::function<R(A...)> &f) {
        f = [](A...) -> R {
          throw std::runtime_error("missing callback");
        };
      };
      std::apply(
        [&](auto &...cb) {
          (set_throw_if_empty(cb), ...);
        },
        std::tie(
          callbacks.bind_ts,
          callbacks.find_ts,
          callbacks.store_ts,
          callbacks.remove_external,
          callbacks.geo_read,
          callbacks.geo_store));
    }

    ~py_server() final {
      stop_server(1000);
      stop_web_api();
    }

    [[nodiscard]] auto get_alive_connections() const {
      return alive_connections.load();
    }

    [[nodiscard]] auto get_failed_connections() const {
      return failed_connections.load();
    }

    [[nodiscard]] bool auth_needed() const {
      return bg_server.auth.needed();
    }

    [[nodiscard]] auto auth_tokens() const {
      return bg_server.auth.tokens();
    }

    void add_auth_tokens(std::vector<std::string> const &tokens) {
      bg_server.auth.add(tokens);
    }

    void remove_auth_tokens(std::vector<std::string> const &tokens) {
      bg_server.auth.remove(tokens);
    }

    void no_callback_error(std::string const &cb_message) {
      throw runtime_error("No python callback provided for : " + cb_message);
    }

    void process_messages(int const msec) {
      pyapi::scoped_gil_release gil;
      if (!is_running())
        start_async();
      std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<long>(msec)));
    }

    int start_web_api(std::string host_ip, int port, string doc_root, int fg_threads, int bg_threads, bool tls_only) {
      if (!web_srv.valid()) {
        bg_server.running = false;
        web_srv = std::async(
          std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads, tls_only]() -> int {
            return web_api::start_web_server(
              bg_server, host_ip, port, make_shared<string>(doc_root), fg_threads, bg_threads, tls_only);
          });
        auto const real_port = static_cast<int>(bg_server.wait_for_real_port(5000));
        if (real_port == 0) {
          throw std::runtime_error("Failed web-api thread did not signal ready to take sockets");
        }
        return real_port;
      }
      return static_cast<int>(bg_server.real_port);
    }

    void stop_web_api() {
      bg_server.running = false; // triggers shutdown
      if (web_srv.valid()) {
        (void) web_srv.get(); // wait for shutdown to complete
      }
    }

    void stop_server(int timeout_ms) {
      set_graceful_close_timeout(timeout_ms);
      clear();
    }

    std::vector<geo::ts_db_config_> get_geo_ts_db_info() {
      return do_get_geo_info(*this);
    }

    void add_to_cache(id_vector_t const &ids, ts_vector_t const &tss) {
      pyapi::scoped_gil_release gil;
      if (ids.size() != tss.size())
        throw runtime_error("attempt to add mismatched size for ts-ids and ts to cache");
      if (ids.empty())
        return;
      std::vector<utcperiod> tss_tp{tss.size()};
      std::vector<ts_frag> tsf;
      tsf.reserve(tss.size());
      for (auto const &ats : tss)
        tsf.emplace_back(std::make_shared<gpoint_ts const>(ats.time_axis(), ats.values(), ats.point_interpretation()));
      ts_cache.add(std::views::zip(ids, tsf, tss_tp));
    }

    //-support direct transfer from server to/from remote
    auto find(std::string const &search_expression) {
      pyapi::scoped_gil_release gil;
      return do_find_ts(*this, search_expression);
    }

    void store(ts_vector_t const &tsv, store_policy p) {
      pyapi::scoped_gil_release gil;
      do_store_ts(*this, tsv, p);
    }

    ts_vector_t read(id_vector_t const &ts_ids, utcperiod p, bool use_ts_cached_read, bool update_ts_cache) {
      pyapi::scoped_gil_release gil;
      auto [ts_frags, periods, errors] = try_read(*this, ts_ids, p, use_ts_cached_read, update_ts_cache);
      ts_vector_t r;
      r.reserve(ts_frags.size());
      for (auto &f : ts_frags)
        r.emplace_back(std::move(f));
      return r;
    }

    void wrap_maintain_backend_container(std::string const &a, bool const info_db, bool const data_db) {
      pyapi::scoped_gil_release gil;
      maintain_backend_container(*this, a, info_db, data_db);
    }
  };

  struct py_srv_connection { // simple struct for exposing information from shyft::core::srv_connection
    string host_port;
    int timeout_ms;
    bool is_open;
    std::size_t reconnect_count;

    // to be able to expose a vector of py_srv_connection to python with indexing capabilities (making it
    // appear as a list) comparison operator is required
    bool operator==(py_srv_connection const &other) const {
      return host_port == other.host_port && timeout_ms == other.timeout_ms && is_open == other.is_open
          && reconnect_count == other.reconnect_count;
    }

    bool operator!=(py_srv_connection const &other) const {
      return !operator==(other);
    }
  };

  // just to help python get an indicator of number of connections pr. process
  static std::atomic<std::size_t> py_client_count{0};

  // need to wrap core client to unlock gil during processing
  struct py_client {
    mutex mx; ///< to enforce just one thread active on this client object at a time
    client impl;

    // bw. compat deprecated with auto connect flag
    py_client(std::string const &host_port, bool /*deprecated*/, int timeout_ms, int version)
      : impl(host_port, timeout_ms, static_cast<version_type>(version), 0) {
      ++py_client_count; // add active clients
    }

    py_client(std::vector<std::string> const &host_ports, bool /*deprecated*/, int timeout_ms, int version)
      : impl(host_ports, timeout_ms, static_cast<version_type>(version), 0) {
      ++py_client_count; // add active clients
    }

    // new ct.
    py_client(std::string const &host_port, int timeout_ms, int operation_timeout_ms, int version)
      : impl(host_port, timeout_ms, static_cast<version_type>(version), operation_timeout_ms) {
      ++py_client_count; // add active clients
    }

    py_client(std::vector<std::string> const &host_ports, int timeout_ms, int operation_timeout_ms, int version)
      : impl(host_ports, timeout_ms, static_cast<version_type>(version), operation_timeout_ms) {
      ++py_client_count; // add active clients
    }

    ~py_client() {
      --py_client_count;
    }

    py_client(py_client const &) = delete;
    py_client(py_client &&) = delete;
    py_client &operator=(py_client const &o) = delete;

    static std::size_t get_client_count() {
      return py_client_count.load();
    }

    [[nodiscard]] bool get_auto_connect() const {
      return true; // TODO: remove now always true
    }

    int get_operation_timeout_ms() const {
      return impl.get_operation_timeout();
    }

    void set_operation_timeout_ms(int operation_timeout_ms) {
      impl.set_operation_timeout(operation_timeout_ms);
    }

    [[nodiscard]] bool get_compress_expressions() const {
      return impl.compress_expressions;
    }

    void set_compress_expressions(bool v) {
      impl.compress_expressions = v;
    }

    [[nodiscard]] std::vector<py_srv_connection> get_connections() const {
      std::vector<py_srv_connection> result;
      std::transform(impl.srv_con.cbegin(), impl.srv_con.cend(), std::back_inserter(result), [](auto const &bc) {
        auto &c = bc.connection;
        return py_srv_connection{c.host_port, c.timeout_ms, c.is_open, c.reconnect_count};
      });
      return result;
    }

    void close(int timeout_ms = 1000) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.close(timeout_ms);
    }

    void reopen(int timeout_ms = 1000) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.reopen(timeout_ms);
    }

    [[nodiscard]] auto get_container_names() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_container_names();
    }

    [[nodiscard]] auto get_server_version() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_server_version();
    }

    [[nodiscard]] auto get_server_supported_protocols() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_supported_protocols();
    }

    [[nodiscard]] auto get_client_supported_protocols() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return std::array<version_type, 2>{min_version, latest_version};
    }

    [[nodiscard]] auto percentiles(
      ts_vector_t tsv,
      utcperiod p,
      gta_t ta,
      std::vector<int64_t> percentile_spec,
      bool use_ts_cached_read,
      bool update_ts_cache) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return ts_vector_t(impl.percentiles(
        std::move(tsv), std::move(p), std::move(ta), std::move(percentile_spec), use_ts_cached_read, update_ts_cache));
    }

    [[nodiscard]] auto
      evaluate(ts_vector_t tsv, utcperiod p, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_result) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return ts_vector_t(
        impl.evaluate(std::move(tsv), std::move(p), use_ts_cached_read, update_ts_cache, std::move(clip_result)));
    }

    [[nodiscard]] auto find(std::string search_expression) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.find(std::move(search_expression));
    }

    void set_container(std::string name, std::string rel_path, std::string type = "ts_db", db_cfg dc = {}) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.set_container(std::move(name), std::move(rel_path), std::move(type), std::move(dc));
    }

    void remove_container(std::string container_url, bool remove_from_disk) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.remove_container(std::move(container_url), remove_from_disk);
    }

    void swap_container(std::string a, std::string b) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.swap_container(std::move(a), std::move(b));
    }

    [[nodiscard]] ts_info get_ts_info(std::string ts_url) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_ts_info(std::move(ts_url));
    }

    void store_ts(ts_vector_t tsv, bool overwrite_on_write, bool cache_on_write) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      (void) impl.store_ts(std::move(tsv), {.recreate = overwrite_on_write, .cache = cache_on_write});
    }

    [[nodiscard]] diags_t store_ts_w_policy(ts_vector_t tsv, store_policy p) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.store_ts(std::move(tsv), p);
    }

    void merge_store_ts(ts_vector_t tsv, bool cache_on_write) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.merge_store_ts(std::move(tsv), cache_on_write);
    }

    void remove(std::string ts_url) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.remove(std::move(ts_url));
    }

    void cache_flush() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.cache_flush();
    }

    [[nodiscard]] cache_stats get_cache_stats() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_cache_stats();
    }

    [[nodiscard]] geo::geo_ts_matrix geo_evaluate(
      string geo_ts_db_id,
      std::vector<std::string> variables,
      std::vector<int64_t> ens,
      gta_t ta,
      utctime ts_dt,
      geo::query geo_range,
      bool concat,
      utctimespan cc_dt0,
      bool use_cache,
      bool update_cache) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.geo_evaluate(
        geo::eval_args{
          std::move(geo_ts_db_id),
          std::move(variables),
          std::move(ens),
          std::move(ta),
          std::move(ts_dt),
          std::move(geo_range),
          concat,
          std::move(cc_dt0)},
        use_cache,
        update_cache);
    }

    void update_geo_ts_db(string geo_ts_db_id, string description, string json, string origin_proj4) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.update_geo_ts_db(std::move(geo_ts_db_id), std::move(description), std::move(json), std::move(origin_proj4));
    }

    [[nodiscard]] geo::geo_ts_matrix geo_evaluate2(geo::eval_args ea, bool use_cache, bool update_cache) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.geo_evaluate(std::move(ea), use_cache, update_cache);
    }

    [[nodiscard]] std::vector<geo::ts_db_config_> get_geo_ts_db_info() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_geo_ts_db_info();
    }

    void geo_store(std::string geo_db_name, geo::ts_matrix tsm, bool replace, bool cache) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.geo_store(std::move(geo_db_name), std::move(tsm), replace, cache);
    }

    void add_geo_ts_db(geo::ts_db_config_ gdb) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.add_geo_ts_db(std::move(gdb));
    }

    void remove_geo_ts_db(std::string geo_db_name) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.remove_geo_ts_db(std::move(geo_db_name));
    }

    [[nodiscard]] std::vector<std::string> q_list() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_list();
    }

    [[nodiscard]] queue::msg_info q_msg_info(std::string q_name, string msg_id) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_msg_info(std::move(q_name), std::move(msg_id));
    }

    [[nodiscard]] std::vector<queue::msg_info> q_msg_infos(std::string q_name) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_msg_infos(std::move(q_name));
    }

    void q_put(std::string q_name, string msg_id, string descript, utctime ttl, ts_vector_t tsv) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_put(std::move(q_name), std::move(msg_id), std::move(descript), std::move(ttl), std::move(tsv));
    }

    [[nodiscard]] auto q_get(std::string q_name, utctime max_wait) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_get(std::move(q_name), std::move(max_wait));
    }

    [[nodiscard]] auto q_find(std::string q_name, std::string msg_id) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_find(q_name, msg_id);
    }

    void q_ack(std::string q_name, string msg_id, string diag) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_ack(std::move(q_name), std::move(msg_id), std::move(diag));
    }

    [[nodiscard]] std::size_t q_size(std::string q_name) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_size(std::move(q_name));
    }

    void q_add(std::string q_name) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_add(std::move(q_name));
    }

    void q_remove(std::string q_name) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_remove(std::move(q_name));
    }

    void q_maintain(std::string q_name, bool keep_ttl_items, bool flush_all) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.q_maintain(std::move(q_name), keep_ttl_items, flush_all);
    }

    void start_transfer(transfer::configuration cfg) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.start_transfer(std::move(cfg));
    }

    [[nodiscard]] std::vector<transfer::configuration> get_transfers() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_transfers();
    }

    [[nodiscard]] transfer::status get_transfer_status(std::string name, bool clear_status) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_transfer_status(std::move(name), clear_status);
    }

    void stop_transfer(std::string name, utctime max_wait) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.stop_transfer(std::move(name), std::move(max_wait));
    }

    void start_q_bridge(q_bridge::configuration cfg) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      impl.start_q_bridge(std::move(cfg));
    }

    [[nodiscard]] std::vector<q_bridge::configuration> get_q_bridges() {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_q_bridges();
    }

    [[nodiscard]] q_bridge::status get_q_bridge_status(std::string name, bool clear_status) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.get_q_bridge_status(std::move(name), clear_status);
    }

    void stop_q_bridge(std::string name, utctime max_wait) {
      pyapi::scoped_gil_release gil;
      std::unique_lock lck(mx);
      return impl.stop_q_bridge(std::move(name), std::move(max_wait));
    }
  };

  using core::utcperiod;
  using core::utctimespan;
  using core::utctime;
  using core::utctime_now;

  static void dtss_messages(py::module_ &m) {

    typedef ts_info TsInfo;
    py::class_<TsInfo> c(
      m,
      "TsInfo",
      doc.intro("Gives some information from the backend ts data-store")
        .intro("about the stored time-series, that could be useful in some contexts")());
    c.def(py::init())
      .def(
        py::init<std::string, time_series::ts_point_fx, utctimespan, std::string, utcperiod, utctime, utctime>(),
        doc.intro("construct a TsInfo with all values specified")(),
        py::arg("name"),
        py::arg("point_fx"),
        py::arg("delta_t"),
        py::arg("olson_tz_id"),
        py::arg("data_period"),
        py::arg("created"),
        py::arg("modified"))
      .def_readwrite("name", &TsInfo::name, doc.intro("the unique name")())
      .def_readwrite(
        "point_fx",
        &TsInfo::point_fx,
        doc.intro("how to interpret the points, instant value, or average over period")())
      .def_readwrite("delta_t", &TsInfo::delta_t, doc.intro("time-axis steps, in seconds, 0 if irregular time-steps")())
      .def_readwrite(
        "olson_tz_id",
        &TsInfo::olson_tz_id,
        doc.intro("empty or time-axis calendar for calendar,t0,delta_t type time-axis")())
      .def_readwrite("data_period", &TsInfo::data_period, doc.intro("the period for data-stored, if applicable")())
      .def_readwrite("created", &TsInfo::created, doc.intro("when time-series was created, seconds 1970s utc")())
      .def_readwrite("modified", &TsInfo::modified, doc.intro("when time-series was last modified, seconds 1970 utc")())
      .def(py::self == py::self)
      .def(py::self != py::self);
    pyapi::expose_format(c);
  }

  static void dtss_transfer_configuration(py::module_ &m) {
    using C = transfer::configuration;
    py::class_<transfer::configuration> c(
      m,
      "TransferConfiguration",
      doc.intro(
        "The transfer configuration describes what time-series to transfer, when, how and where to transfer.")());
    {
      auto w = py::class_<C::time_series_spec>(c, "TimeSeriesSpec");
      pyapi::expose_format(w);
      w.def_readwrite("search_pattern", &C::time_series_spec::search_pattern, "str: reg-ex search pattern")
        .def_readwrite("replace_pattern", &C::time_series_spec::replace_pattern, "str: reg-ex replace pattern");

      auto wh = py::class_<C::remote_spec>(c, "RemoteSpec");
      pyapi::expose_format(wh);
      wh.def_readwrite("host", &C::remote_spec::host, "str: host ip or name")
        .def_readwrite("port", &C::remote_spec::port, "str: host port number");

      auto ps = py::class_<C::period_spec>(c, "PeriodSpec");
      pyapi::expose_format(ps);
      ps.def_readwrite(
          "now_relative", &C::period_spec::now_relative, "time: if >0, then time.now() trimmed to now_relative")
        .def_readwrite(
          "spec", &C::period_spec::spec, "UtcPeriod: period range to transfer,either now_releative, or absolute");

      auto ws = py::class_<C::when_spec>(c, "WhenSpec");
      pyapi::expose_format(ws);
      ws.def_readwrite("ta", &C::when_spec::ta, "TimeAxis: if non-empty, transfer according to time-axis")
        .def_readwrite("changed", &C::when_spec::changed, "bool: if true, use subscription to transfer when changed")
        .def_readwrite("poll_interval", &C::when_spec::poll_interval, "time: how often to check for changes")
        .def_readwrite(
          "change_linger_time",
          &C::when_spec::change_linger_time,
          "time: max time to wait before transfer detected changes");

      auto rs = py::class_<C::xfer_spec>(c, "XferSpec");
      pyapi::expose_format(rs);
      rs.def_readwrite("retries", &C::xfer_spec::retries, "int: retries on connect/re-send for remote")
        .def_readwrite(
          "sleep_before_retry",
          &C::xfer_spec::sleep_before_retry,
          "time: when stuff fail, how long to sleep before retry connect/send message to remote")
        .def_readwrite(
          "partition_size", &C::xfer_spec::partition_size, "int: maximum number of ts to transfer in one batch");
    }
    // ensure subtypes are known before exposing them
    c.def(py::init())
      .def_readwrite("name", &C::name, "str: name of the transfer, used for reference to active transfers")
      .def_readwrite("json", &C::json, "str: a user specified json info that could be useful for extensions")
      .def_readwrite("what", &C::what, "TimeSeriesSpec: timeseries to transfer,reg-expr")
      .def_readwrite("where", &C::where, "RemoteSpec: specification for the remote, host, port")
      .def_readwrite("xfer_period", &C::xfer_period, "PeriodSpec: specification of the time-period to transfer")
      .def_readwrite(
        "read_remote", &C::read_remote, "bool: transfer-direction,if true, pull from remote,otherwise push")
      .def_readwrite("read_updates_cache", &C::read_updates_cache, "bool: if true, also update cache while reading")
      .def_readwrite("store_policy", &C::write_policy, "StorePolicy: used for writing time-series")
      .def_readwrite("when", &C::when, "WhenSpec: oneshot, subscription,continuous, scheduled")
      .def_readwrite("how", &C::how, "XferSpec: retries and delays for remote connection")
      .def_readwrite("protocol_version", &C::protocol_version, "int: protocol version to run the transfer over");
    pyapi::expose_format(c);
  }

  static void dtss_transfer_status(py::module_ &m) {
    using C = transfer::status;
    py::class_<C> c(
      m, "TransferStatus", doc.intro("Transfer status gives insight into the current state of a transfer.")());
    {
      auto re = py::class_<C::read_error>(c, "ReadError");
      pyapi::expose_format(re);
      re.def_readonly("ts_url", &C::read_error::ts_url, "str: ts-url")
        .def_readonly("code", &C::read_error::code, "LookupError: read failure code");
      // expose::expose_vector<C::read_error>("ReadErrorList", "A strongly typed list of ReadError");

      auto we = py::class_<C::write_error>(c, "WriteError");
      pyapi::expose_format(we);
      we.def_readonly("ts_url", &C::write_error::ts_url, "str: ts-url")
        .def_readonly("code", &C::write_error::code, "TsDiagnostics: write failure code");
      // expose::expose_vector<C::write_error>("WriteErrorList", "A strongly typed list of WriteError");
    }
    c.def_readonly("read_errors", &C::read_errors, "ReadErrorList: read errors")
      .def_readonly("write_errors", &C::write_errors, "WriteErrorList: write errors")
      .def_readonly("remote_errors", &C::remote_errors, "StringVector: remote connection errors")
      .def_readonly("reader_alive", &C::reader_alive, "bool: true if reader is still working/monitoring")
      .def_readonly("writer_alive", &C::writer_alive, "bool: true if writer is still working/monitoring")
      .def_readonly("read_speed", &C::read_speed, "float: points/seconds ")
      .def_readonly("write_speed", &C::write_speed, "float: points/seconds ")
      .def_readonly("total_transferred", &C::total_transferred, "int: number of points transferred")
      .def_readonly("last_activity", &C::last_activity, "time: number of points transferred")
      .def_readonly(
        "n_ts_found",
        &C::n_ts_found,
        "int: number of ts found for transfer, if it is zero an readers alive the reader part will continue search for "
        "time-series until they appear");
    pyapi::expose_format(c);
  }

  static void dtss_q_bridge_configuration(py::module_ &m) {

    using C = q_bridge::configuration;
    py::class_<C> c(
      m,
      "QueueBridgeConfiguration",
      doc.intro(
        "The bridge configuration describes what queues to bridge between remote and local, and how tight the timing "
        "should be.")());
    {
      auto w = py::class_<C::queue_spec>(c, "QueueSpec");
      pyapi::expose_format(w);
      w.def_readwrite("queue_name", &C::queue_spec::queue_name, "str: the name of the local queue")
        .def_readwrite(
          "ack_poll_interval",
          &C::queue_spec::ack_poll_interval,
          "time: how often to check for ack messages in the local queue");

      auto wh = py::class_<C::remote_spec>(c, "RemoteSpec");
      pyapi::expose_format(wh);
      wh.def_readwrite("host", &C::remote_spec::host, "str: host ip or name")
        .def_readwrite("port", &C::remote_spec::port, "str: host port number")
        .def_readwrite("queue_name", &C::remote_spec::queue_name, "str: name of the remote queue");
      ;

      auto rs = py::class_<C::xfer_spec>(c, "XferSpec", doc.intro(R"_(
This class specifies how to deal with remote communication errors, and
how long to wait in io-operation on the remote queue.
The latter influences how fast the dtss server that
hosts the exchange/q_bridge will react to a shut-down/restart.


The `io_operation_timeout` parameter controls for how
long an io operation is allowed to take before
it is timed out, the connection closed and
the event reported, then reconnection cycle started.


When waiting for queue messages, the `queue_pick_up_max_wait`
timeouts are added to compensate for known server time.

The minimum value would be the time it takes to:

   1. send the message
   2. server to compute the answer
   3. send the answer back

Due to the nature of network, a large slack should be added.
Typical values could be 1..10 seconds.
)_")());
      pyapi::expose_format(rs);
      rs.def_readwrite("retries", &C::xfer_spec::retries, "int: retries on connect for remote")
        .def_readwrite(
          "sleep_before_retry",
          &C::xfer_spec::sleep_before_retry,
          "time: when stuff fail, how long to sleep before retry.")
        .def_readwrite(
          "queue_pick_up_max_wait",
          &C::xfer_spec::queue_pick_up_max_wait,
          "time: time to try read message or ack from queues")
        .def_readwrite(
          "io_operation_timeout",
          &C::xfer_spec::io_operation_timeout,
          "time: timeout remote operations that are stuck in network/no reply");
    }
    c.def(py::init())
      .def_readwrite("name", &C::name, "str: name of the queue bridge, used for reference to active bridges")
      .def_readwrite("json", &C::json, "str: a user specified json info that could be useful for extensions")
      .def_readwrite("where", &C::where, "RemoteSpec: specification for the remote, host, port")
      .def_readwrite("what", &C::what, "QueueSpec: queue to bridge,local and remote")
      .def_readwrite("how", &C::how, "XferSpec: time-out and retry parameters");
    pyapi::expose_format(c);
  }

  static void dtss_q_bridge_status(py::module_ &m) {
    /** consider nest it into QueueBridgeStatus */ {
      using E = q_bridge::error_entry;
      py::class_<E> c(m, "QueueBridgeErrorEntry", doc.intro("A single error entry from the queue bridge status")());
      pyapi::expose_format(c);
      c.def_readwrite("time", &E::time, "time: time when error occurred")
        .def_readwrite("code", &E::code, "str: error failure code")
        .def(py::self == py::self)
        .def(py::self != py::self);
      pyapi::expose_format(c);
    }
    using C = q_bridge::status;
    py::class_<C> c(m, "QueueBridgeStatus", doc.intro("Status of the queue-bridge operations.")());
    c.def_readwrite("name", &C::name, "str: the name of the queue bridge")
      .def_readwrite("ack_errors", &C::ack_errors, "int: number of ack related errors")
      .def_readwrite("last_ack_error", &C::last_ack_error, "QueueBridgeErrorEntry: last ack related error")
      .def_readwrite("fetch_errors", &C::fetch_errors, "int: number of fetch errors to remote queue")
      .def_readwrite("last_fetch_error", &C::last_fetch_error, "QueueBridgeErrorEntry: last fetch error")
      .def_readwrite("last_activity", &C::last_activity, "time: when any msg fetch or ack workers had activity")
      .def_readwrite("completed", &C::completed, "int: number of completed message/ack exchanges over the bridge")
      .def_readwrite("fetched", &C::fetched, "int: number of message fetched from remote and written to local queue")
      .def(py::self == py::self)
      .def(py::self != py::self);

    pyapi::expose_format(c);
  }

  static void dtss_store_policy_object(py::module_ &m) {
    py::class_<store_policy> c(
      m, "StorePolicy", doc.intro("Determine how DTSS stores time-series, used in context of the DtsClient.")());
    c.def(
       py::init<bool, bool, bool, bool>(),
       doc.intro("construct object with specified parameters")(),
       py::arg("recreate"),
       py::arg("strict"),
       py::arg("cache"),
       py::arg("best_effort") = false)
      .def_readwrite(
        "recreate",
        &store_policy::recreate,
        doc.intro("bool: recreate time-series if it existed, with entirely new definition")())
      .def_readwrite(
        "strict",
        &store_policy::strict,
        doc.intro("bool: use strict requirement for aligment of timepoints.")
          .intro("If True(default), then require perfect matching time-axis on the ")
          .intro("incoming ts fragment, and transfer those points to the target.")
          .intro("Notice that if the target is a break-point/flexible interval time-series,")
          .intro("then all time-axis passed are a perfect match.")
          .intro("If False, use a functional mapping approach, resampling or averaging")
          .intro("the passed time-series fragment to align with the target time-series.")
          .intro("If target is linear time-series, the new ts fragment is evaluated f(t) for the")
          .intro("covered time-points in the target time-series.")
          .intro("If target is stair-case time-series, then the true average of the new ts fragment,")
          .intro("evaluated over the covering/touched period of the target time-series.")())
      .def_readwrite("cache", &store_policy::cache, doc.intro("bool: update cache with new values")())
      .def_readwrite(
        "best_effort",
        &store_policy::best_effort,
        doc.intro("bool: try to store all, return diagnostics for logical errors instead of raise exception")())
      .def(py::self == py::self)
      .def(py::self != py::self);
    pyapi::expose_format(c);
  }

  static void dtss_q_messages(py::module_ &m) {
    using queue::msg_info;
    py::class_<msg_info>(
      m,
      "QueueMessageInfo",
      doc.intro("Information about the queue item,")
        .intro("such as the state of the item, in-queue,fetched, done.")
        .intro("This element is never to be created by the python user, but is a return type")
        .intro("from the dtss queue message info related calls.")())
      .def_readonly("msg_id", &msg_info::msg_id, doc.intro("str: The unique id for this message in the live-queue")())
      .def_readonly(
        "description",
        &msg_info::description,
        doc.intro("str: A user specified description, we recommend json format")())
      .def_readonly(
        "ttl", &msg_info::ttl, doc.intro("time: Time to live set for this message, used to prune out old messages")())
      .def_readonly("created", &msg_info::created, doc.intro("time: Time when the message was put into the queue")())
      .def_readonly(
        "fetched", &msg_info::fetched, doc.intro("time: Time when the message was fetched from the queue")())
      .def_readonly(
        "done",
        &msg_info::done,
        doc.intro("time: Time when the message acknowledged done from the receiver(end-to-end ack)")())
      .def_readonly(
        "diagnostics",
        &msg_info::diagnostics,
        doc.intro("str: Time when the message acknowledged done from the receiver(end-to-end ack)")())
      .def(py::self == py::self)
      .def(py::self != py::self);

    using queue::tsv_msg;
    py::class_<tsv_msg, std::shared_ptr<tsv_msg>>(
      m,
      "QueueMessage",
      doc.intro(
        "A QueueMessage as returned from the DtsClient.q_get(..) consist of the .info part and the payload "
        "time-series vector .tsv")())
      .def(
        py::init(
          [](std::string const &msg_id, std::string const &desc, utctime ttl, time_series::dd::ats_vector const &tsv) {
            return new tsv_msg{
              msg_info{msg_id, desc, ttl, utctime_now()},
              tsv
            };
          }),
        doc.intro("constructs a QueueMessage")
          .parameters()
          .parameter("msg_id", "str", "unique identifier for the message")
          .parameter("desc", "str", "custom description")
          .parameter("ttl", "time", "time the message should live on the queue")
          .parameter("tsv", "TsVector", "timeseries payload")(),
        py::arg("msg_id"),
        py::arg("desc"),
        py::arg("ttl"),
        py::arg("tsv"))
      .def_readonly("info", &tsv_msg::info, "The information about the message")
      .def_readonly("tsv", &tsv_msg::tsv, "The time-series vector payload part of the message")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static void dtss_server(py::module_ &m) {
    using DtsServer = py_server;
    py::class_<DtsServer>(
      m,
      "DtsServer",
      doc
        .pure("A distributed time-series server.\n\n"
              "The server part of the Shyft Distributed TimeSeries System(DTSS).\n"
              "Capable of processing time-series messages and responding accordingly.\n\n"
              "It has dual service interfaces:\n"
              "    \n"
              "    1. raw-socket boost serialized binary, use ")
        .ref_class("DtsClient")
        .intro(
          "\n"
          "    2. web-api, web-socket(https/wss w. auth supported) using boost.beast, boost.spirit to process/emit "
          "messages. This also supports ts-change subscriptions.\n"
          "\n"
          "**python customization and extension capability**\n"
          "\n"
          "The user can setup callback to python to handle unbound symbolic time-series references, `ts-urls`.\n"
          "This means that you can use your own ts database backend if you have one that can beat the shyft-internal "
          "ts-db.\n"
          "\n"
          "The DtsServer then resolves symbolic references reading time-series from a service or storage for the "
          "specified period.\n"
          "The server object will then compute the resulting time-series vector,\n"
          "and respond back to clients with the results\n"
          "multi-node considerations:\n"
          "    \n"
          "    1. firewall/routing: ensure that the port you are using are open for ip-traffic(use ssh-tunnel if you "
          "need ssl/tls)\n"
          "    2. we strongly recommend using linux for performance and long term stability\n"
          "\n"
          "The Dts also support master-slave mode, that allows scaling out computations to several Dtss instances, see "
          "set_master_slave_mode\n"
          "\n"
          "**backend storage**\n"
          "\n"
          "There are several internal backends, and customization for external storage as well.\n"
          "Internal storage containers:\n"
          "    \n"
          "    1. **rocksdb** - by facebook, configurable specifying `ts_rdb` in the `set_container` method\n"
          "    2. **filedb** - fast zero overhead, and simple internal binary formats, configurable specifying `ts_db` "
          "in the `set_container` method\n"
          "\n\n"
          "The kind of backend storage for the backing store ts-containers is specified in the `set_container` method, "
          "for explicit creation of ts-containers.\n"
          "Notice that for remotely client created containers for geo time-series storage, the `default_geo_db_type` "
          "applies, set to `ts_rdb`.\n\n"
          "**External storage** can be setup by suppling python callbacks for the `find`,`read`,`store` and "
          "`remove_container` hooks.\n\n"
          "Then specifying something else than the `shyft://` pre-fix for the ts-urls, allows any external storage to "
          "be used.\n\n"
          "To ensure that containers are (remotely)  found and configured after reboot/restart, ensure to provide\n"
          "a dtss configuration file where this information is stored.\n"
          "\n\n"
          "**HPC setup, configure linux os user limits**\n\n"
          "For high performance environments, the `ulimit`, especially memory, number of files open, needs to be set "
          "to\n"
          "higher values than the defaults, usually `nofiles` is 1024, which is to low for HPC apps.\n"
          "We recommend 4096, or 8192 or even higher\n"
          "for demanding databases.\n"
          "For tuning rocksdb, read tuning guides for those libraries, -we provide some basic parameters "
          "for tuning,\n"
          "but more can be added if needed.\n")
        .see_also("DtsClient")())
      .def(py::init<srv::fast_server_iostream_config>(), py::arg("config") = srv::fast_server_iostream_config{})
      .def_property(
        "configuration_file",
        [](DtsServer const &c) {
          return c.cfg_file;
        },
        [](DtsServer &c, std::string const &s) {
          set_container_cfg(c, s);
        },
        doc.intro("str: configuration file to enable persistent container configurations over coldstarts")())
      .def(
        "set_can_remove",
        +[](py_server &server, bool can_remove) {
          server.can_remove = can_remove;
        },
        doc.intro("Set whether the DtsServer support removing time-series")
          .intro("The default setting is false, su unless this method is called with true")
          .intro("as argument the server will not allow removing data using `DtsClient.remove`.")
          .parameters()
          .parameter("can_remove", "bool", "``true`` if the server should allow removing data. ``false`` otherwise")(),
        py::arg("can_remove"))
      .def(
        "set_listening_port",
        &DtsServer::set_listening_port,
        doc.intro("set the listening port for the service")
          .parameters()
          .parameter("port_no", "int", "a valid and available tcp-ip port number to listen on.")
          .paramcont("typically it could be 20000 (avoid using official reserved numbers)")
          .returns("nothing", "None", "")(),
        py::arg("port_no"))
      .def(
        "start_async",
        &DtsServer::start_server,
        doc.intro("(deprecated, use start_server) start server listening in background, and processing messages")
          .see_also("set_listening_port(port_no),set_listening_ip,is_running,cb,process_messages(msec)")
          .returns(
            "port_no",
            "in",
            "the port used for listening operations, either the value as by set_listening_port, or if it was "
            "unspecified, a new available port")
          .notes()
          .note("you should have setup up the callback, cb before calling start_async")
          .note("Also notice that processing will acquire the GIL\n -so you need to release the GIL to allow for "
                "processing messages")
          .see_also("process_messages(msec)")())
      .def(
        "start_server",
        &DtsServer::start_server,
        doc.intro("start server listening in background, and processing messages")
          .see_also("set_listening_port(port_no),set_listening_ip,is_running,cb,process_messages(msec)")
          .returns(
            "port_no",
            "in",
            "the port used for listening operations, either the value as by set_listening_port, or if it was "
            "unspecified, a new available port")
          .notes()
          .note("you should have setup up the callback, cb before calling start_server")
          .note("Also notice that processing will acquire the GIL\n -so you need to release the GIL to allow for "
                "processing messages")
          .see_also("process_messages(msec)")())
      .def(
        "stop_server",
        &DtsServer::stop_server,
        doc.intro("stop serving connections, gracefully.").see_also("start_server()")(),
        py::arg("timeout") = 1000)
      .def(
        "set_master_slave_mode",
        +[](
           py_server &server,
           string ip,
           int port,
           double master_poll_time,
           size_t unsubscribe_min_threshold,
           double unsubscribe_max_delay,
           version_type protocol_version = internal_version) {
          set_master(
            server, ip, port, master_poll_time, unsubscribe_min_threshold, unsubscribe_max_delay, protocol_version);
        },
        doc
          .intro("Set master-slave mode, redirecting all IO calls on this dtss to the master ip:port dtss.\n"
                 "This instance of the dtss is kept in sync with changes done on the master using subscription to "
                 "changes on the master\n"
                 "Calculations, and caches are still done locally unloading the computational efforts from the master.")
          .parameters()
          .parameter("ip", "str", "The ip address where the master dtss is running")
          .parameter("port", "int", "The port number for the master dtss")
          .parameter("master_poll_time", "time", "[s] max time between each update from master, typicall 0.1 s is ok")
          .parameter(
            "unsubscribe_threshold",
            "int",
            "minimum number of unsubscribed time-series before also unsubscribing from the master")
          .parameter("unsubscribe_max_delay", "int", "maximum time to delay unsubscriptions, regardless number")
          .parameter("protocol_version", "int", "version to run the sync over, defaults to internal")(),
        py::arg("ip"),
        py::arg("port"),
        py::arg("master_poll_time"),
        py::arg("unsubscribe_threshold"),
        py::arg("unsubscribe_max_delay"),
        py::arg("protocol_version") = internal_version)
      .def(
        "set_max_connections",
        &DtsServer::set_max_connections,
        doc.intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
          .parameters()
          .parameter("max_connect", "int", "maximum number of connections before denying more connections")
          .see_also("get_max_connections()")(),
        py::arg("max_connect"))
      .def(
        "get_max_connections",
        &DtsServer::get_max_connections,
        doc.intro("returns the maximum number of connections to be served concurrently")())
      .def(
        "clear",
        &DtsServer::clear,
        doc.intro("stop serving connections, gracefully.").see_also("cb, process_messages(msec),start_server()")())
      .def( // FIXME: delete
        "close",
        &DtsServer::clear,
        doc.intro("stop serving connections, gracefully.").see_also("cb, process_messages(msec),start_server()")())
      .def(
        "is_running",
        &DtsServer::is_running,
        doc.intro("true if server is listening and running").see_also("start_server(),process_messages(msec)")())
      .def(
        "get_listening_port",
        &DtsServer::get_listening_port,

        "returns the port number it's listening at for serving incoming request")
      .def(
        "set_listening_ip",
        &DtsServer::set_listening_ip,
        doc.intro("Set the ip address to specific interface ip. Must be called prior to the start server method")
          .parameters()
          .parameter("ip", "", "ip address, like 127.0.0.1 for local host only interface")(),
        py::arg("ip"))
      .def(
        "get_listening_ip",
        &DtsServer::get_listening_ip,
        doc.intro("Get the current ip listen address")
          .returns("listening ip", "", "note that 0.0.0.0 means listening for all interfaces")())
      .def_property(
        "cb",
        [](py_server &s) {
          return s.callbacks.bind_ts;
        },
        [](py_server &s, std::function<ts_vector_t(id_vector_t const &ts_ids, utcperiod p)> c) {
          s.callbacks.bind_ts = pyapi::wrap_callback(std::move(c));
        },
        doc
          .intro("Callable[[StringVector,UtcPeriod],TsVector]: callback for binding unresolved time-series references "
                 "to concrete time-series.")
          .intro("Called *if* the incoming messages contains unbound time-series.")
          .intro("The signature of the callback function should be TsVector cb(Vector,utcperiod)")
          .intro("\nExamples:\n")
          .intro(
            ">>> from shyft import time_series as sa\n\n"
            ">>> def resolve_and_read_ts(ts_ids,read_period):\n"
            ">>>     print('ts_ids:', len(ts_ids), ', read period=', str(read_period))\n"
            ">>>     ta = sa.TimeAxis(read_period.start, sa.deltahours(1), read_period.timespan()//sa.deltahours(1))\n"
            ">>>     x_value = 1.0\n"
            ">>>     r = sa.TsVector()\n"
            ">>>     for ts_id in ts_ids :\n"
            ">>>         r.append(sa.TimeSeries(ta, fill_value = x_value))\n"
            ">>>         x_value = x_value + 1\n"
            ">>>     return r\n"
            ">>> # and then bind the function to the callback\n"
            ">>> dtss=sa.DtsServer()\n"
            ">>> dtss.cb=resolve_and_read_ts\n"
            ">>> dtss.set_listening_port(20000)\n"
            ">>> dtss.process_messages(60000)\n")())
      .def_property(
        "find_cb",
        [](py_server &s) {
          return s.callbacks.find_ts;
        },
        [](py_server &s, std::function<ts_info_vector_t(std::string)> c) {
          s.callbacks.find_ts = pyapi::wrap_callback(std::move(c));
        },
        doc.intro("Callable[[str],TsInfoVector]: callback for finding time-series using a search-expression.")
          .intro("Called everytime the .find() method is called.")
          .intro("The signature of the callback function should be fcb(search_expr: str)->TsInfoVector")
          .intro("\nExamples:\n")
          .intro(
            ">>> from shyft import time_series as sa\n\n"
            ">>> def find_ts(search_expr: str)->sa.TsInfoVector:\n"
            ">>>     print('find:',search_expr)\n"
            ">>>     r = sa.TsInfoVector()\n"
            ">>>     tsi = sa.TsInfo()\n"
            ">>>     tsi.name = 'some_test'\n"
            ">>>     r.append(tsi)\n"
            ">>>     return r\n"
            ">>> # and then bind the function to the callback\n"
            ">>> dtss=sa.DtsServer()\n"
            ">>> dtss.find_cb=find_ts\n"
            ">>> dtss.set_listening_port(20000)\n"
            ">>> # more code to invoce .find etc.\n")())
      .def_property(
        "store_ts_cb",
        [](py_server &s) {
          return s.callbacks.store_ts;
        },
        [](py_server &s, std::function<void(ts_vector_t const &)> c) {
          s.callbacks.store_ts = pyapi::wrap_callback(std::move(c));
        },
        doc.intro("Callable[[TsVector],None]: callback for storing time-series.")
          .intro("Called everytime the .store_ts() method is called and non-shyft urls are passed.")
          .intro("The signature of the callback function should be scb(tsv: TsVector)->None")
          .intro("\nExamples:\n")
          .intro(
            ">>> from shyft import time_series as sa\n\n"
            ">>> def store_ts(tsv:sa.TsVector)->None:\n"
            ">>>     print('store:',len(tsv))\n"
            ">>>     # each member is a bound ref_ts with an url\n"
            ">>>     # extract the url, decode and store\n"
            ">>>     #\n"
            ">>>     #\n"
            ">>>     return\n"
            ">>> # and then bind the function to the callback\n"
            ">>> dtss=sa.DtsServer()\n"
            ">>> dtss.store_ts_cb=store_ts\n"
            ">>> dtss.set_listening_port(20000)\n"
            ">>> # more code to invoce .store_ts etc.\n")())
      .def_property(
        "remove_container_cb",
        [](py_server &s) {
          return s.callbacks.remove_external;
        },
        [](py_server &s, std::function<void(std::string const &, bool)> c) {
          s.callbacks.remove_external = pyapi::wrap_callback(std::move(c));
        },
        doc.intro("Callable[[str, bool],None]: callback for removing external containers.")
          .intro("Called when the .remove_container() method is called with a non-shyft container url.")
          .intro("The signature of the callback function should be rcb(container_url: string, remove_from_disk: "
                 "bool)->None")())
      .def_property(
        "geo_ts_read_cb",
        [](py_server &s) {
          return s.callbacks.geo_read;
        },
        [](
          py_server &s,
          std::function<geo::ts_matrix(std::shared_ptr<geo::ts_db_config> const &, geo::slice const &)> c) {
          s.callbacks.geo_read = pyapi::wrap_callback(std::move(c));
        },
        doc.intro("Callable[[GeoTimeSeriesConfiguration,GeoSlice],GeoMatrix]: Callback for reading  geo_ts db.")
          .intro("Called everytime there is a need for geo_ts not stored in cached.")
          .intro("The signature of the callback function should be grcb(cfg:GeoTimeSeriesConfiguration, "
                 "slice:GeoSlice)->GeoMatrix")())
      .def_property(
        "geo_ts_store_cb",
        [](py_server &s) {
          return s.callbacks.geo_store;
        },
        [](py_server &s, std::function<void(std::shared_ptr<geo::ts_db_config>, geo::ts_matrix const &, bool)> c) {
          s.callbacks.geo_store = pyapi::wrap_callback(std::move(c));
        },
        doc.intro("Callable[[GeoTimeSeriesConfiguration,GeoMatrix,bool],None]: callback for storing to geo_ts db.")
          .intro("Called everytime the client.store_geo_ts() method is called.")
          .intro("The signature of the callback function should be gscb(cfg:GeoTimeSeriesConfiguration, tsm:GeoMatrix, "
                 "replace:bool)->None")())
      .def(
        "process_messages",
        &DtsServer::process_messages,
        doc.intro("wait and process messages for specified number of msec before returning")
          .intro("the dtss-server is started if not already running")
          .parameters()
          .parameter("msec", "int", "number of millisecond to process messages")
          .notes()
          .note("this method releases GIL so that callbacks are not blocked when the\n"
                "dtss-threads perform the callback ")
          .see_also("cb,start_server(),is_running,clear()")(),
        py::arg("msec"))
      .def(
        "set_container",
        +[](
           py_server &server,
           std::string const &container_name,
           std::string const &root_dir,
           std::string container_type = std::string{},
           db_cfg cfg = db_cfg{}) {
          add_container(server, container_name, root_dir, container_type, cfg);
        },
        doc.intro("set ( or replaces) an internal shyft store container to the dtss-server.")
          .intro("All ts-urls with shyft://<container>/ will resolve")
          .intro("to this internal time-series storage for find/read/store operations")
          .parameters()
          .parameter("name", "str", "Name of the container as pr. url definition above")
          .parameter("root_dir", "str", "A valid directory root for the container")
          .parameter(
            "container_type",
            "str",
            "one of ('ts_rdb', 'ts_db'), container type to add.") // TODO: document properly
          .notes()
          .note("currently this call should only be used when the server is not processing messages, "
                "- before starting, or after stopping listening operations\n")(),
        py::arg("name"),
        py::arg("root_dir"),
        py::arg("container_type") = std::string{},
        py::arg("cfg") = db_cfg{})
      .def(
        "remove_container",
        +[](py_server &server, std::string const &container_url, bool remove_from_disk) {
          remove_container(server, container_url, remove_from_disk);
        },
        doc.intro("remove an internal shyft store container or an external container from the dtss-server.")
          .intro("container_url on the form shyft://<container>/ will remove internal containers")
          .intro("all other urls with be forwarded to the remove_external_cb callback on the server")
          .intro("removal of containers can take a long time to finish")
          .parameters()
          .parameter("container_url", "str", "url of the container as pr. url definition above")
          .parameter("delete_from_disk", "bool", "Flag to indicate if the container should be deleted from disk")(),
        py::arg("container_url"),
        py::arg("delete_from_disk") = false)
      .def(
        "maintain_backend_container",
        &DtsServer::wrap_maintain_backend_container,
        py::arg("name"),
        py::arg("info_db"),
        py::arg("data_db"),
        doc.intro("maintain backend container store, e.g. rocksdb.")
          .intro("The `info_db` and `data_db` controls which part of backend are maintained.")
          .intro("The `info` part is the key-value store for ts-name to ts-info")
          .intro("The `data` part is the key-value store for the data values of the time-series.")
          .intro("The rocksdb is to a large degree self-maintained and auto-tuned, but introduction of certain")
          .intro("filters (bloom filter for the `info` db, and prefix-transform filter for `data` db),")
          .intro("will require maintenance of existing databases to take full performance effect of those filter.")
          .intro("Notice that the maintenance is not needed unless there is performance issues that can")
          .intro("be addressed by the introduced filter-configuration.")
          .intro("The `info` part are usually a small data-base, so the operation goes in few seconds")
          .intro("The `data` part can however span several terra-bytes, and would involve reading and writing")
          .intro("the entire amount stored.")
          .intro("We recommend running this on realistic copy of prod on test/preprod system,")
          .intro("and verify time-usage and performance improvements.")
          .intro("For detailed technical description refer to rocksdb CompactRange function.")
          .parameters()
          .parameter("name", "str", "Name of the container as")
          .parameter("info_db", "bool", "If true, to force maintenance for the info-part of db")
          .parameter("data_db", "bool", "If true, to force maintenance for the data-part of db")
          .notes()
          .note("This functions can be called on a running db, but it will impact performance for the length of the "
                "call, "
                "- so ensure to compute size and time estimates before starting\n")())
      .def(
        "swap_container",
        +[](py_server &server, std::string const &container_name_a, std::string const &container_name_b) {
          swap_container(server, container_name_a, container_name_b);
        },
        doc.intro("Swap the backend storage for container `a` and `b`.")
          .intro("The content of a and b should be equal prior to the call to ensure wanted semantics,")
          .intro("as well as cache correctness.")
          .intro("This is the case if `a` is immutable, and copied to `b` prior to the operation.")
          .intro("If `a` is not permanently immutable, it has to be ensured at least for")
          .intro("the time where the copy/swap operation is done.")
          .intro("The intended purpose is to support migration and moving ts-db backends.")
          .intro("When swap is done, the remove_container can be used for the container that is redunant.")
          .intro("A typical operation is copy `a`->`a_tmp`, then swap(`a`,`a_tmp`), then remove(`shyft://a_tmp`,True)")
          .parameters()
          .parameter("container_name_a", "str", "Name of container a")
          .parameter("container_name_b", "str", "Name of container b")(),
        py::arg("container_name_a"),
        py::arg("container_name_b") = false)
      .def(
        "set_geo_ts_db",
        +[](py_server &server, geo::ts_db_config_ const &cfg) {
          add_geo_ts_db(server, cfg);
        },
        doc.intro("This add/replace a geo-ts database to the server, so that geo-related requests")
          .intro("can be resolved by means of this configuation and the geo-related callbacks.")
          .parameters()
          .parameter("geo_ts_cfg", "GeoTimeseriesConfiguration", "The configuration for the new geo-ts data-base")(),
        py::arg("geo_ts_cfg"))
      .def(
        "set_auto_cache",
        +[](py_server &server, bool active) {
          server.cache_all_reads = active;
        },
        doc.intro("set auto caching all reads active or passive.")
          .intro("Default is off, and caching must be done through")
          .intro("explicit calls to .cache(ts_ids,ts_vector)")
          .parameters()
          .parameter("active", "bool", "if set True, all reads will be put into cache")(),
        py::arg("active"))
      .def(
        "cache",
        &DtsServer::add_to_cache,
        doc.intro("add/update specified ts_ids with corresponding ts to cache")
          .intro("please notice that there is no validation of the tds_ids, they")
          .intro("are threated identifiers,not verified against any existing containers etc.")
          .intro("Requests that follows, will use the cached item as long as it satisfies")
          .intro("the identifier and the coverage period requested")
          .parameters()
          .parameter("ts_ids", "StringVector", "a list of time-series ids")
          .parameter("ts_vector", "TsVector", "a list of corresponding time-series")(),
        py::arg("ts_ids"),
        py::arg("ts_vector"))
      .def(
        "flush_cache",
        +[](py_server &server, id_vector_t &ids) {
          server.ts_cache.remove(ids);
        },
        doc.intro("flushes the *specified* ts_ids from cache")
          .intro("Has only effect for ts-ids that are in cache, non-existing items are ignored")
          .parameters()
          .parameter("ts_ids", "StringVector", "a list of time-series ids to flush out")(),
        py::arg("ts_ids"))
      .def(
        "flush_cache_all",
        +[](py_server &server) {
          return server.ts_cache.flush();
        },
        doc.intro("flushes all items out of cache (cache_stats remain un-touched)")())
      .def_property_readonly(
        "cache_stats",
        +[](py_server &server) {
          return server.ts_cache.get_cache_stats();
        },
        doc.intro("CacheStats: the current cache statistics")())
      .def(
        "clear_cache_stats",
        +[](py_server &server) {
          server.ts_cache.clear_cache_stats();
        },
        doc.intro("clear accumulated cache_stats")())
      .def_property(
        "cache_max_items",
        +[](py_server &server) {
          return server.ts_cache.get_capacity();
        },
        +[](py_server &server, std::size_t max_size) {
          server.ts_cache.set_capacity(max_size);
        },
        doc.intro("int: cache_max_items is the maximum number of time-series identities that are")
          .intro("kept in memory. Elements exceeding this capacity is elided using the least-recently-used")
          .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
          .intro("time-series from cache in the least recently used order.")())
      .def_property(
        "cache_ts_initial_size_estimate",
        +[](py_server &server) {
          return server.ts_cache.get_ts_size();
        },
        +[](py_server &server, std::size_t ts_average_size) {
          server.ts_cache.set_ts_size(ts_average_size);
        },
        doc.intro("int: The initial time-series size estimate in bytes for the cache mechanism.")
          .intro("memory-target = cache_ts_initial_size_estimate * cache_max_items")
          .intro("algorithm. Notice that assigning a lower value than the existing value will also flush out")
          .intro("time-series from cache in the least recently used order.")())
      .def_property(
        "cache_memory_target",
        +[](py_server &server) {
          return server.ts_cache.get_mem_max();
        },
        +[](py_server &server, std::size_t max_size) {
          server.ts_cache.set_mem_max(max_size);
        },
        doc.intro("int: The memory max target in number of bytes.")
          .intro("If not set directly the following equation is  use:")
          .intro("cache_memory_target = cache_ts_initial_size_estimate * cache_max_items")
          .intro("When setting the target directly, number of items in the chache is ")
          .intro("set so that real memory usage is less than the specified target.")
          .intro("The setter could cause elements to be flushed out of cache.")())
      .def_property(
        "graceful_close_timeout_ms",
        &DtsServer::get_graceful_close_timeout,
        &DtsServer::set_graceful_close_timeout,
        doc.intro("int: how long to let a connection linger after message is processed to allow for")
          .intro("flushing out reply to client.")
          .intro("Ref to dlib.net dlib.net/dlib/server/server_kernel_abstract.h.html ")())
      .def(
        "start_web_api",
        &DtsServer::start_web_api,
        doc.intro("starts the dtss web-api on the specified host_ip, port, doc_root and number of threads")
          .parameters()
          .parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only etc.")
          .parameter("port", "int", "port number to serve the web_api on, ensure it's available!")
          .parameter("doc_root", "str", "directory from which we will serve http/https documents, like index.html etc.")
          .parameter("fg_threads", "int", "number of web-api foreground threads, typical 1-4 depending on load")
          .parameter(
            "bg_threads", "int", "number of long running  background threads workers to serve dtss-request etc.")
          .parameter("tls_only", "bool", "default false, set to true to enforce tls sessions only.")
          .returns("port", "int", "real port number used, if 0 is passed as `port` it is auto-allocated")(),
        py::arg("host_ip"),
        py::arg("port"),
        py::arg("doc_root"),
        py::arg("fg_threads") = 2,
        py::arg("bg_threads") = 4,
        py::arg("tls_only") = false)
      .def("stop_web_api", &DtsServer::stop_web_api, doc.intro("Stops any ongoing web-api service")())
      .def_property_readonly(
        "alive_connections",
        &DtsServer::get_alive_connections,
        doc.intro("int: returns currently alive connections to the server")())
      .def_property_readonly(
        "failed_connections",
        &DtsServer::get_failed_connections,
        doc.intro("int: returns count of connections closed due to bad requests")())
      .def_property_readonly(
        "stale_connection_close_count",
        &DtsServer::get_stale_close_count,
        doc.intro("int: returns count of connection closed due to stale/no communication activity")())
      .def_property_readonly(
        "auth_needed",
        &DtsServer::auth_needed,
        doc.intro("bool: returns true if the server is setup with auth-tokens, requires web-api clients to pass a "
                  "valid token")())
      .def(
        "auth_tokens",
        &DtsServer::auth_tokens,

        doc.intro("returns the registered authentication tokens.")())
      .def(
        "add_auth_tokens",
        &DtsServer::add_auth_tokens,
        doc.intro("Adds auth tokens, and activate authentication.")
          .intro("The tokens is compared exactly to the autorization token passed in the request.")
          .intro("Authorization should onlye be used for the https/wss, unless other measures(vpn/ssh tunnels etc.) "
                 "are used to protect auth tokens on the wire")
          .intro("Important! Ensure to start_web_api with tls_only=True when using auth!")
          .parameters()
          .parameter(
            "tokens", "", "list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")(),
        py::arg("tokens"))
      .def(
        "remove_auth_tokens",
        &DtsServer::remove_auth_tokens,
        doc
          .intro("removes auth tokens, if it matches all available tokens, then deactivate auth requirement for "
                 "clients")
          .parameters()
          .parameter(
            "tokens", "", "list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")(),
        py::arg("tokens"))
      .def_readwrite(
        "default_geo_db_config",
        &DtsServer::default_geo_db_cfg,
        doc.intro("GeoTimeSeriesConfiguration: Default parameters for geo db created by clients")())
      .def_readwrite(
        "default_geo_db_type",
        &DtsServer::default_geo_db_type,
        doc.intro("str: default container type for geo db created by clients,(`ts_rdb`,`ts_db`), defaults set "
                  "to `ts_rdb`")())
      .def(
        "get_geo_db_ts_info",
        +[](py_server &server) -> vector<geo::ts_db_config_> {
          return do_get_geo_info(server);
        },
        doc.intro("Returns the configured geo-ts data-bases on the server, so queries can be specified and formulated")
          .returns("r", "GeoTimeseriesConfigurationVector", "A strongly typed list of GeoTimeseriesConfiguration")
          .see_also(".geo_evaluate()")())
      .def(
        "get_container_names",
        +[](py_server &server) -> id_vector_t {
          return do_get_container_names(server);
        },
        doc.intro("Return a list of the names of containers available on the server")())
      .def(
        "find",
        &DtsServer::find,
        doc.intro("Find ts information that fully matches the regular search-expression.")
          .intro("For the shyft file based backend, take care to specify path elements precisely,")
          .intro("so that the directories visited is minimised.")
          .intro("e.g:`a\\/.*\\/my\\.ts`")
          .intro("Will prune out  any top level directory not starting with `a`,")
          .intro("but will match any subdirectories below that level.")
          .intro("Refer to python test-suites for a wide range of examples using find.")
          .intro("Notice that the regexp search algoritm uses ignore case.")
          .intro("Please be aware that custom backend by python extension might have different rules.")
          .parameters()
          .parameter(
            "search_expression", "str", "regular search-expression, to be interpreted by the back-end tss server")
          .returns("ts_info_vector", "TsInfoVector", "The search result, as vector of TsInfo objects")
          .see_also("TsInfo,TsInfoVector")(),
        py::arg("search_expression"))
      .def(
        "store",
        &DtsServer::store,
        doc.intro("Store the time-series in the ts-vector in the dtss backend.")
          .pure(
            "Stores the time-series fragments data passed to the backend.\n"
            "If store_policy.strict == True:\n"
            "It is semantically stored as if \n\n"
            "first erasing the existing stored points in the range of ts.time_axis().total_period()\n\n"
            "then inserting the points of the ts.\n\n"
            "Thus, only modifying the parts of time-series that are covered by the ts-fragment passed.\n"
            "If there is no previously existing time-series, its merely stores the ts-fragment as the initial\n"
            "content and definition of the time-series.\n\n"
            "When creating time-series 1st time, pay attention to the time-axis, and point-interpretation as this\n"
            "remains the properties of the newly created time-series.\n"
            "Storing 15min data to a time-series defined initially as hourly series will raise exception.\n"
            "On the other hand, the variable interval time-series are generic and will accept any\n"
            "time-resolution to be stored\n\n"
            "If store_policy.strict ==False\n"
            "The passed time-series fragment is interpreted as a f(t), and projected to\n"
            "the time-axis time-points/intervals of the target time-series\n"
            "If the target time-series is a stair-case type (POINT_AVERAGE_VALUE),\n"
            "then the true average of the passed time-series fragment is used to align with the target.\n"
            "If the target time-series is a linear type (POINT_INSTANT_VALUE),\n"
            "then the f(t) of the passed time-series fragment for the time-points of\n"
            "the target-series is used.\n"
            "The store_policy.recreate == True,  is used to *replace* the entire definition of any previously stored "
            "time-series.\n"
            "This is semantically as if erasing the previously stored time-series and replacing its entire content\n"
            "and definition, starting fresh with the newly passed time-series.\n"
            "The store_policy.best_effort == True or False, controls how logical errors are handled.\n"
            "If best_effort is set to True, then all time-series are attempted stored,\n"
            "and if any failed, the returned value of the function will be non-empty list of diagnostics identifying\n"
            "those that failed and the diagnostics.\n"
            "If best_effort is set to False, then exception is raised on the first item that fails, the remaining "
            "items\n"
            "are not stored.\n"

            "The time-series should be created like this, with url and a concrete point-ts:\n"
            "\n"
            ">>>   a=sa.TimeSeries(ts_url,ts_points)\n"
            ">>>   tsv.append(a)\n")
          .parameters()
          .parameter(
            "tsv", "TsVector", "ts-vector with time-series, url-reference and values to be stored at dtss server")
          .parameter(
            "store_policy",
            "StorePolicy",
            "Determines how to project the passed time-series fragments to the backend stored time-series")
          .returns("diagnostics", "TsDiagnosticsItemList", "For any failed items, normally empty")
          .see_also("TsVector")(),
        py::arg("tsv"),
        py::arg("store_policy"))
      .def(
        "read",
        &DtsServer::read,
        doc.intro("Reads from the db-backend/cache the specified ts_ids for covering read_period.")
          .intro("NOTE: That the ts-backing-store, either cached or by read, will return data for:")
          .intro("    * at least the period needed to evaluate the read_period")
          .intro("    * In case of cached result, this will currently involve the entire matching cached time-series "
                 "segment.")
          .parameters()
          .parameter("ts_ids", "StringVector", "a list of shyft-urrls, like shyft://abc/def")
          .parameter(
            "read_period",
            "UtcPeriod",
            "the valid non-zero length period that the binding service should read from the backing "
            "ts-store/ts-service")
          .parameter("use_ts_cached_read", "bool", "use of server-side ts-cache")
          .parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data")
          .returns("tsvector", "TsVector", "an evaluated list of point time-series in the same order as the input list")
          .see_also("DtsServer")(),
        py::arg("ts_ids"),
        py::arg("read_period"),
        py::arg("use_ts_cached_read") = true,
        py::arg("update_ts_cache") = true);
  }

  static void dtss_client(py::module_ &m) {
    typedef py_srv_connection DtsConnection;
    py::class_<DtsConnection>(m, "DtsConnection")
      .def_readonly(
        "host_port", &DtsConnection::host_port, "str: Endpoint network address of the remote server, like `host:port`.")
      .def_readonly(
        "timeout_ms", &DtsConnection::timeout_ms, "int: Timout for remote server operations, in number milliseconds.")
      .def_readonly("is_open", &DtsConnection::is_open, "bool: If the connection to the remote server is (still) open.")
      .def_readonly(
        "reconnect_count",
        &DtsConnection::reconnect_count,
        "int: Number of reconnects to the remote server that have been performed.");

    // typedef std::vector<DtsConnection> DtsConnectionVector;
    // py::class_<DtsConnectionVector>(
    //   "DtsConnectionVector", doc.intro("A strongly typed list of DtsConnection")(), py::init())
    //   .def(py::vector_indexing_suite<DtsConnectionVector>())
    //   //.def(py::init<const DtsConnectionVector&>((py::arg("clone_me")))) // don't have to be clonable
    //   // since it's only exposed read-only to python
    //   .def(py::self == py::self)
    //   .def(py::self != py::self);

    typedef py_client DtsClient;
    py::class_<DtsClient>(
      m,
      "DtsClient",
      doc.intro("The client side part of the distributed time series system(DTSS).")
        .intro("")
        .pure(
          "The DtsClient communicate with the DtsServer using an efficient raw socket\n"
          "protocol using boost binary serialization. Provide a protocol version in the constructor\n"
          "to guarantee compatibility with dtss servers over several shyft releases.\n"
          "A typical operation would be that\n"
          "the DtsClient forwards TsVector that represents lists and structures of\n"
          "time-series expressions) to the DtsServer(s), that takes care of binding\n"
          "unbound symbolic time-series, evaluate and return the results back to the DtsClient.\n"
          "This class is closely related to the")
        .ref_class("DtsServer")
        .pure(
          "and useful reference\n"
          "is also")
        .ref_class("TsVector")
        .intro(".\n\n")
        .pure(
          "Best practice for client/server is to use cache following two simple rules(the default):\n    \n"
          "    1. Always caching writes (because then consumers get it fresh and fast).\n"
          "    2. Always use caching reads(utilize and maintain the adaptive cache).\n\n"
          "The only two known very rare and special scenarios where *uncached writes* can be useful\n"
          "are when loading large initial content of time-series db.\n"
          "Another special scenario, where caching reads should be turned off is when using the\n"
          "3rd party dtss backend extension, where the 3rd party db is written/modified\n"
          "*outside the control of dtss*\n"
          "Also note that the caching works with the ts-terminals, not the result of the expressions.\n"
          "When reading time-series expressions, such as `ts = ts1 - ts2`, implementation of the cache is such\n"
          "that it contains the ts-terminals (here, `ts1` and `ts2`), not the expression itself (`ts`).\n"
          "The `.cache_stats`, provides cache statistics for the server.\n"
          "The cache can be flushed, useful for some special cases of loading data outside cache.")(),
      py::dynamic_attr())
      .def(
        py::init< std::string const &, bool, int, int>(),
        doc.intro("Constructs a dts-client with the specifed host_port parameter.")
          .intro("A connection is immediately done to the server at specified port.")
          .intro("If no such connection can be made, it raises a RuntimeError.")
          .intro(
            "If protocol_version is provided the client uses versioned protocols (see get_supported_protocols to "
            "get the supported protocols on the server)")
          .parameter("host_port", "string", "a string of the format 'host:portnumber', e.g. 'localhost:20000'")
          .parameter(
            "auto_connect",
            "bool",
            "default True, connection pr. call. if false, connection last lifetime of object unless explicitely "
            "closed/reopened")
          .parameter("timeout_ms", "int", "default 1000ms, used for timeout connect/reconnect/close operations")
          .parameter(
            "protocol_version",
            "int",
            "default to internal, set protocol version on the client (see get_valid_versions to see what the client "
            "supports)")(),
        py::arg("host_port"),
        py::arg("auto_connect") = true,
        py::arg("timeout_ms") = 1000,
        py::arg("protocol_version") = internal_version)
      .def(
        py::init< std::vector<std::string> const &, bool, int, int>(),
        doc.intro("Constructs a dts-client with the specifed host_ports parameters.")
          .intro("A connection is immediately done to the server at specified port.")
          .intro("If no such connection can be made, it raises a RuntimeError.")
          .intro(
            "If several servers are passed, the .evaluate and .percentile function will partition the ts-vector "
            "between the")
          .intro("provided servers and scale out the computation")
          .intro(
            "If protocol_version is provided the client uses versioned protocols (see get_supported_protocols to "
            "get the supported protocols on the server)")
          .parameter(
            "host_ports", "StringVector", "a a list of string of the format 'host:portnumber', e.g. 'localhost:20000'")
          .parameter(
            "auto_connect",
            "bool",
            "default True, connection pr. call. if false, connection last lifetime of object unless explicitly "
            "closed/reopened")
          .parameter("timeout_ms", "int", "default 1000ms, used for timeout connect/reconnect/close operations")
          .parameter(
            "protocol_version",
            "int",
            "default to internal, set protocol version on the client (see get_valid_versions to see what the client "
            "supports)")(),
        py::arg("host_ports"),
        py::arg("auto_connect"),
        py::arg("timeout_ms"),
        py::arg("protocol_version") = internal_version)
      .def(
        py::init< std::string const &, int, int, int>(),
        doc.intro("Constructs a dts-client with the specifed host_port parameter.")
          .intro("A connection is immediately done to the server at specified port.")
          .intro("If no such connection can be made, it raises a RuntimeError.")
          .intro(
            "If protocol_version is provided the client uses versioned protocols (see get_supported_protocols to "
            "get the supported protocols on the server)")
          .parameter("host_port", "string", "a string of the format 'host:portnumber', e.g. 'localhost:20000'")
          .parameter("timeout_ms", "int", "default 1000ms, used for timeout connect/reconnect/close operations")
          .parameter(
            "operation_timeout_ms",
            "int",
            "default 0, if set, terminate operations that takes longer then specified limit")
          .parameter(
            "protocol_version",
            "int",
            "default to internal, set protocol version on the client (see get_valid_versions to see what the client "
            "supports)")(),
        py::arg("host_port"),
        py::arg("timeout_ms") = 1000,
        py::kw_only(),
        py::arg("operation_timeout_ms") = 0,
        py::arg("protocol_version") = internal_version)
      .def(
        py::init< std::vector<std::string> const &, int, int, int>(),
        doc.intro("Constructs a dts-client with the specifed host_ports parameters.")
          .intro("A connection is immediately done to the server at specified port.")
          .intro("If no such connection can be made, it raises a RuntimeError.")
          .intro(
            "If several servers are passed, the .evaluate and .percentile function will partition the ts-vector "
            "between the")
          .intro("provided servers and scale out the computation")
          .intro(
            "If protocol_version is provided the client uses versioned protocols (see get_supported_protocols to "
            "get the supported protocols on the server)")
          .parameter(
            "host_ports", "StringVector", "a a list of string of the format 'host:portnumber', e.g. 'localhost:20000'")
          .parameter("timeout_ms", "int", "default 1000ms, used for timeout connect/reconnect/close operations")
          .parameter(
            "operation_timeout_ms",
            "int",
            "default 0, if set, terminate operations that takes longer then specified limit")
          .parameter(
            "protocol_version",
            "int",
            "default to internal, set protocol version on the client (see get_valid_versions to see what the client "
            "supports)")(),
        py::arg("host_ports"),
        py::arg("timeout_ms"),
        py::kw_only(),
        py::arg("operation_timeout_ms") = 0,
        py::arg("protocol_version") = internal_version)
      .def_property_readonly_static(
        "total_clients",
        [](py::object) {
          return DtsClient::get_client_count();
        })
      .def_property_readonly(
        "auto_connect",
        &DtsClient::get_auto_connect,
        "bool: If connections are made as needed, and kept short, otherwise externally managed.")
      .def_property(
        "compress_expressions",
        &DtsClient::get_compress_expressions,
        &DtsClient::set_compress_expressions,
        doc.intro("bool: If True, the expressions are compressed before sending to the server.")
          .intro("For expressions of any size, like 100 elements, with expression")
          .intro("depth 100 (e.g. nested sums), this can speed up")
          .intro("the transmission by a factor or 3.")())
      .def_property_readonly(
        "connections", &DtsClient::get_connections, doc.intro("int: Get remote server connections.")())
      .def(
        "close",
        &DtsClient::close,
        doc.intro("Close the connection. If auto_connect is enabled it will automatically reopen if needed.")(),
        py::arg("timeout_ms") = 1000)
      .def(
        "reopen",
        &DtsClient::reopen,
        doc.intro("(Re)open a connection after close or server restart.")(),
        py::arg("timeout_ms") = 1000)
      .def(
        "get_server_version",
        &DtsClient::get_server_version,
        doc.intro(
          "Returns the server version major.minor.patch string, if multiple servers, the version of the first "
          "is returned")())
      .def(
        "get_server_supported_protocols",
        &DtsClient::get_server_supported_protocols,
        doc.returns("versions", "List", "the versioned protocols the server supports (min, max)")())
      .def(
        "get_client_supported_protocols",
        &DtsClient::get_client_supported_protocols,
        doc.returns("versions", "List", "the versioned protocols the client supports (min, max)")())
      .def_property(
        "operation_timeout_ms",
        &DtsClient::get_operation_timeout_ms,
        &DtsClient::set_operation_timeout_ms,
        "int: Operation timeout for remote server operations, in number milliseconds.")
      .def(
        "percentiles",
        &DtsClient::percentiles,
        doc.intro("Evaluates the expressions in the ts_vector for the specified utcperiod.")
          .intro("If the expression includes unbound symbolic references to time-series,")
          .intro("these time-series will be passed to the binding service callback")
          .intro("on the serverside.")
          .parameters()
          .parameter(
            "ts_vector", "TsVector", "a list of time-series (expressions), including unresolved symbolic references")
          .parameter(
            "utcperiod",
            "UtcPeriod",
            "the valid non-zero length period that the binding service should read from the backing "
            "ts-store/ts-service")
          .parameter("time_axis", "TimeAxis", "the time_axis for the percentiles, e.g. a weekly time_axis")
          .parameter(
            "percentile_list", "IntVector", "a list of percentiles, where -1 means true average, 25=25percentile etc")
          .parameter("use_ts_cached_read", "bool", "utilize server-side cached results")
          .parameter("update_ts_cache", "bool", "when reading time-series, also update the server-side cache")
          .returns(
            "tsvector",
            "TsVector",
            "an evaluated list of percentile time-series in the same order as the percentile input list")
          .see_also(".evaluate(), DtsServer")(),
        py::arg("ts_vector"),
        py::arg("utcperiod"),
        py::arg("time_axis"),
        py::arg("percentile_list"),
        py::arg("use_ts_cached_read") = true,
        py::arg("update_ts_cache") = true)
      .def(
        "evaluate",
        &DtsClient::evaluate,
        doc.intro("Evaluates the expressions in the ts_vector.")
          .intro("If the expression includes unbound symbolic references to time-series,")
          .intro("these time-series will be passed to the binding service callback")
          .intro("on the serverside, passing on the specifed utcperiod.")
          .intro("")
          .intro("NOTE: That the ts-backing-store, either cached or by read, will return data for:")
          .intro("    * at least the period needed to evaluate the utcperiod")
          .intro(
            "    * In case of cached result, this will currently involve the entire matching cached time-series "
            "segment.")
          .intro(
            "In particular, this means that the returned result **could be larger** than the specified utcperiod, "
            "unless you specify `clip_result`")
          .intro("      Other available methods, such as the expression (x.average(ta)), including time-axis,")
          .intro("      can be used to exactly control the returned result size.")
          .intro("      Also note that the semantics of utcperiod is ")
          .intro("      to ensure that enough data is read from the backend, so that it can evaluate the expressions.")
          .intro(
            "      Use clip_result argument to clip the time-range of the resulting time-series to fit your need "
            "if needed")
          .intro(
            "      - this will typically be in scenarios where you have not supplied time-axis operations "
            "(unbounded eval),")
          .intro("      and you also are using caching.")
          .intro("      ")
          .intro("\nSee also")
          .ref_meth("DtsClient.percentiles")
          .intro("if you want to evaluate percentiles of an expression.")

          .parameters()
          .parameter(
            "ts_vector", "TsVector", "a list of time-series (expressions), including unresolved symbolic references")
          .parameter(
            "utcperiod",
            "UtcPeriod",
            "the valid non-zero length period that the binding service should read from the backing "
            "ts-store/ts-service")
          .parameter("use_ts_cached_read", "bool", "use of server-side ts-cache")
          .parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data")
          .parameter(
            "clip_result",
            "UtcPeriod",
            "If supplied, clip the time-range of the resulting time-series to cover evaluation f(t) over this period "
            "only")
          .returns("tsvector", "TsVector", "an evaluated list of point time-series in the same order as the input list")
          .see_also("DtsServer")(),
        py::arg("ts_vector"),
        py::arg("utcperiod"),
        py::arg("use_ts_cached_read") = true,
        py::arg("update_ts_cache") = true,
        py::arg("clip_result") = utcperiod{})
      .def(
        "set_container",
        &DtsClient::set_container,
        doc.intro("create an internal shyft store container to the dtss-server with a root relative path.")
          .intro("All ts-urls with shyft://<container>/ will resolve")
          .intro("to this internal time-series storage for find/read/store operations")
          .intro("will not replace existing containers that have the same name")
          .parameters()
          .parameter("name", "str", "Name of the container as pr. url definition above")
          .parameter(
            "relative_path", "str", "A valid directory for the container relative to the root path of the server.")
          .parameter("container_type", "str", "one of ('ts_rdb', 'ts_db'), container type to add.")(),
        py::arg("name"),
        py::arg("relative_path"),
        py::arg("container_type") = std::string{"ts_db"},
        py::arg("cfg") = db_cfg{})
      .def(
        "remove_container",
        &DtsClient::remove_container,
        doc.intro("remove an internal shyft store container or an external container from the dtss-server.")
          .intro("container_url on the form shyft://<container>/ will remove internal containers")
          .intro("all other urls with be forwarded to the remove_external_cb callback on the server")
          .intro("removal of containers can take a long time to finish")
          .parameters()
          .parameter("container_url", "str", "url of the container as pr. url definition above")
          .parameter("delete_from_disk", "bool", "Flag to indicate if the container should be deleted from disk")(),
        py::arg("container_url"),
        py::arg("delete_from_disk") = false)
      .def(
        "swap_container",
        &DtsClient::swap_container,
        doc.intro("Swap the backend storage for container `a` and `b`.")
          .intro("The content of a and b should be equal prior to the call to ensure wanted semantics,")
          .intro("as well as cache correctness.")
          .intro("This is the case if `a` is immutable, and copied to `b` prior to the operation.")
          .intro("If `a` is not permanently immutable, it has to be ensured at least for")
          .intro("the time where the copy/swap operation is done.")
          .intro("The intended purpose is to support migration and moving ts-db backends.")
          .intro("When swap is done, the remove_container can be used for the container that is redunant.")
          .intro("A typical operation is copy `a`->`a_tmp`, then swap(`a`,`a_tmp`), then remove(`shyft://a_tmp`,True)")
          .parameters()
          .parameter("container_name_a", "str", "Name of container a")
          .parameter("container_name_b", "str", "Name of container b")(),
        py::arg("container_name_a"),
        py::arg("container_name_b") = false)
      .def(
        "find",
        &DtsClient::find,
        doc.intro("Find ts information that fully matches the regular search-expression.")
          .intro("For the shyft file based backend, take care to specify path elements precisely,")
          .intro("so that the directories visited is minimised.")
          .intro("e.g:`a\\/.*\\/my\\.ts`")
          .intro("Will prune out  any top level directory not starting with `a`,")
          .intro("but will match any subdirectories below that level.")
          .intro("Refer to python test-suites for a wide range of examples using find.")
          .intro("Notice that the regexp search algoritm uses ignore case.")
          .intro("Please be aware that custom backend by python extension might have different rules.")
          .parameters()
          .parameter(
            "search_expression", "str", "regular search-expression, to be interpreted by the back-end tss server")
          .returns("ts_info_vector", "TsInfoVector", "The search result, as vector of TsInfo objects")
          .see_also("TsInfo,TsInfoVector")(),
        py::arg("search_expression"))
      .def(
        "get_ts_info",
        &DtsClient::get_ts_info,
        doc.intro("Get ts information for a time-series from the backend")
          .parameters()
          .parameter("ts_url", "str", "Time-series url to lookup ts info for")
          .returns("ts_info", "TsInfo", "A TsInfo object")
          .see_also("TsInfo")(),
        py::arg("ts_url"))
      .def(
        "store_ts",
        &DtsClient::store_ts,
        doc.intro("Store the time-series in the ts-vector in the dtss backend.")
          .pure(
            "Stores the time-series fragments data passed to the backend.\n"
            "It is semantically stored as if \n\n"
            "first erasing the existing stored points in the range of ts.time_axis().total_period()\n\n"
            "then inserting the points of the ts.\n\n"
            "Thus, only modifying the parts of time-series that are covered by the ts-fragment passed.\n"
            "If there is no previously existing time-series, its merely stores the ts-fragment as the initial\n"
            "content and definition of the time-series.\n\n"
            "When creating time-series 1st time, pay attention to the time-axis, and point-interpretation as this\n"
            "remains the properties of the newly created time-series.\n"
            "Storing 15min data to a time-series defined initially as hourly series will raise exception.\n"
            "On the otherhand, the variable interval time-series are generic and will accept any\n"
            "time-resolution to be stored\n\n"
            "The `overwrite_on_write` = True,  is used to *replace* the entire definition of any previously stored "
            "time-series.\n"
            "This is semantically as if erasing the previously stored time-series and replacing its entire content\n"
            "and definition, starting fresh with the newly passed time-series.\n"
            "The time-series should be created like this, with url and a concrete point-ts:\n"
            "\n"
            ">>>   a=sa.TimeSeries(ts_url,ts_points)\n"
            ">>>   tsv.append(a)\n")
          .parameters()
          .parameter(
            "tsv", "TsVector", "ts-vector with time-series, url-reference and values to be stored at dtss server")
          .parameter(
            "overwrite_on_write",
            "bool",
            "When True the backend replaces the entire content and definition of any existing time-series with the "
            "passed time-series")
          .parameter(
            "cache_on_write",
            "bool",
            "defaults True, if set to False, the cache is not updated, and should only be considered used in very "
            "special use-cases.")
          .returns("None", "", "")
          .see_also("TsVector")(),
        py::arg("tsv"),
        py::arg("overwrite_on_write") = false,
        py::arg("cache_on_write") = true)
      .def(
        "store",
        &DtsClient::store_ts_w_policy,
        doc.intro("Store the time-series in the ts-vector in the dtss backend.")
          .pure(
            "Stores the time-series fragments data passed to the backend.\n"
            "If store_policy.strict == True:\n"
            "It is semantically stored as if \n\n"
            "first erasing the existing stored points in the range of ts.time_axis().total_period()\n\n"
            "then inserting the points of the ts.\n\n"
            "Thus, only modifying the parts of time-series that are covered by the ts-fragment passed.\n"
            "If there is no previously existing time-series, its merely stores the ts-fragment as the initial\n"
            "content and definition of the time-series.\n\n"
            "When creating time-series 1st time, pay attention to the time-axis, and point-interpretation as this\n"
            "remains the properties of the newly created time-series.\n"
            "Storing 15min data to a time-series defined initially as hourly series will raise exception.\n"
            "On the other hand, the variable interval time-series are generic and will accept any\n"
            "time-resolution to be stored\n\n"
            "If store_policy.strict ==False\n"
            "The passed time-series fragment is interpreted as a f(t), and projected to\n"
            "the time-axis time-points/intervals of the target time-series\n"
            "If the target time-series is a stair-case type (POINT_AVERAGE_VALUE),\n"
            "then the true average of the passed time-series fragment is used to align with the target.\n"
            "If the target time-series is a linear type (POINT_INSTANT_VALUE),\n"
            "then the f(t) of the passed time-series fragment for the time-points of\n"
            "the target-series is used.\n"
            "The store_policy.recreate == True,  is used to *replace* the entire definition of any previously stored "
            "time-series.\n"
            "This is semantically as if erasing the previously stored time-series and replacing its entire content\n"
            "and definition, starting fresh with the newly passed time-series.\n"
            "The store_policy.best_effort == True or False, controls how logical errors are handled.\n"
            "If best_effort is set to True, then all time-series are attempted stored,\n"
            "and if any failed, the returned value of the function will be non-empty list of diagnostics "
            "identifying\n"
            "those that failed and the diagnostics.\n"
            "If best_effort is set to False, then exception is raised on the first item that fails, the remaining "
            "items\n"
            "are not stored.\n"

            "The time-series should be created like this, with url and a concrete point-ts:\n"
            "\n"
            ">>>   a=sa.TimeSeries(ts_url,ts_points)\n"
            ">>>   tsv.append(a)\n")
          .parameters()
          .parameter(
            "tsv", "TsVector", "ts-vector with time-series, url-reference and values to be stored at dtss server")
          .parameter(
            "store_policy",
            "StorePolicy",
            "Determines how to project the passed time-series fragments to the backend stored time-series")
          .returns("diagnostics", "TsDiagnosticsItemList", "For any failed items, normally empty")
          .see_also("TsVector")(),
        py::arg("tsv"),
        py::arg("store_policy"))
      .def(
        "merge_store_ts_points",
        &DtsClient::merge_store_ts,
        doc.intro("Merge the ts-points supplied in the tsv into the existing time-series on the server side.")
          .intro("The effect of each ts is similar to **as if**:")
          .intro("")
          .intro("    1. **read** ts.total_period() from ts point store")
          .intro("    2. **in memory appy** the TimeSeries.merge_points(ts) on the read-ts")
          .intro("    3. **write** the resulting merge-result back to the ts-store")
          .intro("")
          .intro("This function is suitable for typical data-collection tasks")
          .intro("where the points collected is from an external source, appears as batches,")
          .intro("that should just be added to the existing point-set")
          .parameters()
          .parameter(
            "tsv", "TsVector", "ts-vector with time-series, url-reference and values to be stored at dtss server")
          .parameter(
            "cache_on_write",
            "bool",
            "updates the cache with the result of the merge operation, if set to False, this is skipped, notice that "
            "this is only useful for very special use-cases.")
          .returns("None", "", "")
          .see_also("TsVector")(),
        py::arg("tsv"),
        py::arg("cache_on_write") = true)
      .def(
        "remove",
        &DtsClient::remove,
        doc.intro("Remove a time-series from the dtss backend")
          .intro("The time-series referenced by ``ts_url`` is removed from the backend DtsServer.")
          .intro("Note that the DtsServer may prohibit removing time-series.")
          .parameters()
          .parameter("ts_url", "str", "shyft url referencing a time series")(),
        py::arg("ts_url"))
      .def(
        "cache_flush",
        &DtsClient::cache_flush,
        doc.intro(
          "Flush the cache (including statistics) on the server. This can be useful in scenario when "
          "cache_on_write=False in the store operations.")())
      .def_property_readonly(
        "cache_stats",
        &DtsClient::get_cache_stats,
        doc.intro("CacheStats: Get the cache_stats (including statistics) on the server.")())
      .def(
        "geo_evaluate",
        &DtsClient::geo_evaluate,
        doc.intro("Evaluates a geo-temporal query on the server, and return the results")
          .parameters()
          .parameter("geo_ts_db_name", "string", "The name of the geo_ts_db, e.g. arome, ec, arome_cc ec_cc etc.")
          .parameter(
            "variables",
            "StringVector",
            "list  of variables, like 'temperature','precipitation'. If empty, return data for all available "
            "variables")
          .parameter("ensembles", "IntVector", "list of ensembles to read, if empty return all available")
          .parameter(
            "time_axis",
            "TimeAxis",
            "return geo_ts where t0 matches time-points of this time-axis. If concat, the ta.total_period().end "
            "determines how long to extend latest forecast")
          .parameter("ts_dt", "time", "specifies the length of the time-slice to read from each time-series")
          .parameter("geo_range", "GeoQuery", "Specify polygon to include, empty means all")
          .parameter(
            "concat",
            "bool",
            "If true, the geo_ts for each ensemble/point is joined together to form one singe time-series, "
            "concatenating a slice from each of the forecasts")
          .parameter(
            "cc_dt0",
            "time",
            "concat delta time to skip from beginning of each geo_ts, so you can specify 3h, then select +3h.. "
            "slice-end from each forecast")
          .parameter("use_cache", "bool", "use cache if available(speedup)")
          .parameter(
            "update_cache", "bool", "if reading data from backend, also stash it to the cache for faster evaluations")
          .returns(
            "r",
            "GeoMatrix",
            "A matrix where the elements are GeoTimeSeries, accessible using indicies time,variable, ensemble, t0")
          .see_also(".get_geo_ts_db_info()")(),
        py::arg("geo_ts_db_name"),
        py::arg("variables"),
        py::arg("ensembles"),
        py::arg("time_axis"),
        py::arg("ts_dt"),
        py::arg("geo_range"),
        py::arg("concat"),
        py::arg("cc_dt0"),
        py::arg("use_cache") = true,
        py::arg("update_cache") = true)
      .def(
        "geo_evaluate",
        &DtsClient::geo_evaluate2,
        doc.intro("Evaluates a geo-temporal query on the server, and return the results")
          .parameters()
          .parameter(
            "eval_args",
            "GeoEvalArgs",
            "complete set of arguments for geo-evaluation, including geo-db, scope for variables, ensembles, time "
            "and "
            "geo-range")
          .parameter("use_cache", "bool", "use cache if available(speedup)")
          .parameter(
            "update_cache", "bool", "if reading data from backend, also stash it to the cache for faster evaluations")
          .returns(
            "r",
            "GeoMatrix",
            "A matrix where the elements are GeoTimeSeries, accessible using indicies time,variable, ensemble, t0")
          .see_also(".get_geo_ts_db_info()")(),
        py::arg("eval_args"),
        py::arg("use_cache") = true,
        py::arg("update_cache") = true)
      .def(
        "get_geo_db_ts_info",
        &DtsClient::get_geo_ts_db_info,
        doc.intro("Returns the configured geo-ts data-bases on the server, so queries can be specified and formulated")
          .returns("r", "GeoTimeseriesConfigurationVector", "A strongly typed list of GeoTimeseriesConfiguration")
          .see_also(".geo_evaluate()")())
      .def(
        "geo_store",
        &DtsClient::geo_store,
        doc.intro("Store a ts-matrix with needed dimensions and data to the specified geo-ts-db")
          .parameters()
          .parameter("geo_ts_db_name", "string", "The name of the geo_ts_db, e.g. arome, ec, arome_cc ec_cc etc.")
          .parameter(
            "tsm",
            "TsMatrix",
            "A dense matrix with dimensionality complete for variables, ensembles and geo-points,flexible "
            "time-dimension 1..n")
          .parameter(
            "replace",
            "bool",
            "Replace existing geo time-series with the new ones, does not extend existing ts, replaces them!")
          .parameter("cache", "bool", "Also put values to the cache")
          .see_also(".get_geo_ts_db_info(),.geo_evaluate")(),
        py::arg("geo_ts_db_name"),
        py::arg("tsm"),
        py::arg("replace"),
        py::arg("cache") = true)
      .def(
        "update_geo_ts_db_info",
        &DtsClient::update_geo_ts_db,
        doc.intro("Update info fields of the geo ts db configuration to the supplied parameters")
          .parameters()
          .parameter("geo_ts_db_name", "string", "The name of the geo_ts_db, e.g. arome, ec, arome_cc ec_cc etc.")
          .parameter("description", "str", "The description field of the database")
          .parameter("json", "str", "The user specified json like string")
          .parameter("origin_proj4", "str", "The origin proj4 field update")
          .see_also(".get_geo_ts_db_info()")(),
        py::arg("geo_ts_db_name"),
        py::arg("description"),
        py::arg("json"),
        py::arg("origin_proj4"))
      .def(
        "add_geo_ts_db",
        &DtsClient::add_geo_ts_db,
        doc.intro("Adds a new geo time-series database to the dtss-server with the given specifications")
          .parameter(
            "geo_cfg",
            "GeoTimeSeriesConfiguration",
            "the configuration to be added to the server specifying the dimensionality etc.")
          .see_also(".get_geo_db_ts_info()")(),
        py::arg("geo_cfg"))
      .def(
        "remove_geo_ts_db",
        &DtsClient::remove_geo_ts_db,
        doc.intro("Remove the specified geo time-series database from dtss-server")
          .parameter("geo_ts_db_name", "string", "the name of the geo-ts-database to be removed")
          .see_also(".get_geo_db_ts_info(),add_geo_ts_db()")(),
        py::arg("geo_ts_db_name"))
      .def(
        "get_container_names",
        &DtsClient::get_container_names,
        doc.intro("Return a list of the names of containers available on the server")())
      .def(
        "q_add",
        &DtsClient::q_add,
        doc.intro("Add a a named queue to the dtss server")
          .parameters()
          .parameter("name", "", "the name of the new queue, required to be unique")(),
        py::arg("name"))
      .def(
        "q_remove",
        &DtsClient::q_remove,
        doc.intro("Removes a named queue from dtss server, including all data in flight on the queue")
          .parameters()
          .parameter("name", "", "the name of the queue")(),
        py::arg("name"))
      .def(
        "q_maintain",
        &DtsClient::q_maintain,
        doc.intro("Maintains, removes items that has passed through the queue, and are marked as done.")
          .intro("To flush absolutely all items, pass flush_all=True.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter(
            "keep_ttl_items",
            "",
            "If true, the ttl set for the done messages are respected, and they are not removed until the create+ttl "
            "has expired")
          .parameter("flush_all", "", "removes all items in the queue and kept by the queue, the queue is emptied")(),
        py::arg("name"),
        py::arg("keep_ttl_items"),
        py::arg("flush_all") = false)
      .def("q_list", &DtsClient::q_list, doc.intro("returns a list of defined queues on the dtss server")())
      .def(
        "q_msg_infos",
        &DtsClient::q_msg_infos,
        doc.intro("Returns all message informations from a queue, including not yet pruned fetched/done messages")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .returns("msg_infos", "", "the list of information keept in the named queue")(),
        py::arg("name"))
      .def(
        "q_msg_info",
        &DtsClient::q_msg_info,
        doc.intro("From the specified queue, fetch info about specified msg_id.")
          .intro(
            "By inspecting the provided information, one can see when the messaage is created, fetched, and done "
            "with.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the msg_id")
          .returns("msg_info", "", "the information/state of the identified message")(),
        py::arg("name"),
        py::arg("msg_id"))
      .def(
        "q_put",
        &DtsClient::q_put,
        doc.intro("Put a message, as specified with the supplied parameters, into the specified named queue.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the msg_id, required to be unique within current messages keept by the queue")
          .parameter(
            "description", "", "the freetext description to but along with the message, we recommend json formatted")
          .parameter(
            "ttl",
            "",
            "time-to-live for the message after done, if specified, the q_maintain process can be asked to keep done "
            "messages that have ttl")
          .parameter("tsv", "", "time-series vector, with the wanted payload of time-series")(),
        py::arg("name"),
        py::arg("msg_id"),
        py::arg("description"),
        py::arg("ttl"),
        py::arg("tsv"))
      .def(
        "q_get",
        &DtsClient::q_get,
        doc.intro("Get a message out from the named queue, waiting max_wait time for it if it's not already there.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("max_wait", "", "max_time to wait for message to arrive")
          .returns(
            "q_msg",
            "",
            "A queue message consisting of .info describing the message, and the time-series vector .tsv")(),
        py::arg("name"),
        py::arg("max_wait"))
      .def(
        "q_find",
        &DtsClient::q_find,
        doc.intro("Find a historical message with the given id")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the id of the message")
          .returns("q_msg", "", "The historical message, or None if the message dosen't exist")(),
        py::arg("name"),
        py::arg("msg_id"))
      .def(
        "q_ack",
        &DtsClient::q_ack,
        doc.intro("After q_get, q_ack confirms that the message is ok/handled back to the process that called q_put.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .parameter("msg_id", "", "the msg_id, required to be unique within current messages keept by the queue")
          .parameter(
            "diagnostics", "", "the freetext diagnostics to but along with the message, we recommend json formatted")(),
        py::arg("name"),
        py::arg("msg_id"),
        py::arg("diagnostics"))
      .def(
        "q_size",
        &DtsClient::q_size,
        doc.intro("Returns number of queue messages waiting to be read by q_get.")
          .parameters()
          .parameter("name", "", "the name of the queue")
          .returns("unread count", "", "number of elements queued up")(),
        py::arg("name"))
      .def(
        "start_transfer",
        &DtsClient::start_transfer,
        doc.intro("Starts a transfer on the server using the provided TransferConfiguration.")
          .parameters()
          .parameter("cfg", "", "the configuration for the transfer")(),
        py::arg("cfg"))
      .def(
        "get_transfers",
        &DtsClient::get_transfers,
        doc.intro("returns configured active transfers.")
          .returns("transfer_configurations", "", "A list of configured Transfers")())
      .def(
        "stop_transfer",
        &DtsClient::stop_transfer,
        doc.intro("Stop,cancel, removes a named transfer.")
          .parameters()
          .parameter("name", "", "the name of the transfer to remove")
          .parameter("max_wait", "", "to let existing transfers gracefully finish")(),
        py::arg("name"),
        py::arg("max_wait"))
      .def(
        "get_transfer_status",
        &DtsClient::get_transfer_status,
        doc.intro("Get status of specified status, if clear_status is True, also clear it.")
          .parameters()
          .parameter("name", "", "the name of the transfer")
          .parameter("clear_status", "", "if true, also clear status at server")
          .returns("transfer_status", "", "The TransferStatus")(),
        py::arg("name"),
        py::arg("clear_status"))
      .def(
        "start_q_bridge",
        &DtsClient::start_q_bridge,
        doc.intro("Starts a transfer on the server using the provided configuration.")
          .parameters()
          .parameter("cfg", "", "the configuration for the transfer")(),
        py::arg("cfg"))
      .def(
        "get_q_bridges",
        &DtsClient::get_q_bridges,
        doc.intro("returns configured active queue bridges.")
          .returns("configurations", "", "A list of configured queue bridges")())
      .def(
        "stop_q_bridge",
        &DtsClient::stop_q_bridge,
        doc.intro("Stop,cancel, removes a named queue bridge.")
          .parameters()
          .parameter("name", "", "the name of the transfer to remove")
          .parameter("max_wait", "", "to let existing transfers gracefully finish")(),
        py::arg("name"),
        py::arg("max_wait"))
      .def(
        "get_q_bridge_status",
        &DtsClient::get_q_bridge_status,
        doc.intro("Get status of specified configured queue bridge, if clear_status is True, also clear it.")
          .parameters()
          .parameter("name", "", "the name of the queue bridge configuration")
          .parameter("clear_status", "", "if true, also clear status at server")
          .returns("status", "", "The status as collected for the queue bridge")(),
        py::arg("name"),
        py::arg("clear_status"));
  }

  void dtss_cache_stats(py::module_ &m) {
    using CacheStats = cache_stats;
    py::class_<CacheStats>(m, "CacheStats", doc.intro("Cache statistics for the DtsServer.")())
      .def(py::init())
      .def_readwrite("hits", &CacheStats::hits, doc.intro("int: number of hits by time-series id")())
      .def_readwrite("misses", &CacheStats::misses, doc.intro("int: number of misses by time-series id")())
      .def_readwrite(
        "coverage_misses",
        &CacheStats::coverage_misses,
        doc.intro(
          "int: number of misses where we did find the time-series id, but the period coverage was "
          "insufficient")())
      .def_readwrite(
        "id_count",
        &CacheStats::id_count,

        doc.intro("int: number of unique time-series identities in cache")())
      .def_readwrite(
        "point_count", &CacheStats::point_count, doc.intro("int: total number of time-series points in the cache")())
      .def_readwrite(
        "fragment_count",
        &CacheStats::fragment_count,
        doc.intro("int: number of time-series fragments in the cache, (greater or equal to id_count)")())
      .def(py::self == py::self)
      .def(py::self == py::self);
  }

  inline auto ext_query_url(std::string const &prefix, std::string const &container, std::string const &ts_name) {
    return prefix + container + "?" + urlencode(ts_name);
  }

  inline auto ext_path_url(std::string const &prefix, std::string const &container, std::string const &ts_name) {
    return prefix + container + "/" + urlencode(ts_name);
  }

  void url_utils(py::module_ &m) {
    m.def(
      "shyft_url",
      static_cast<std::string (*)(
        std::string const &, std::string const &, std::map<std::string, std::string> const &)>(&shyft_url),
      doc.intro("Construct a Shyft URL from a container, a TS-path, and an optional collection of query flags.")
        .intro("")
        .intro("Query keys and values are always urlencoded. The separating `?`, `&`, and `=` are not encoded.")
        .parameters()
        .parameter("container", "str", "Shyft TS container.")
        .parameter("ts_path", "str", "Time-series path.")
        .parameter(
          "queries", "Dict[str,str]", "Optional mapping from query keys to values. Defaults to an empty dictionary.")
        .returns("url", "str", "Constructed Shyft URL.")
        .see_also(
          "shyft_url, urlencode, extract_shyft_url_container, extract_shyft_url_path, "
          "extract_shyft_url_query_parameters")(),
      py::arg("container"),
      py::arg("ts_path"),
      py::arg("queries") = std::map<std::string, std::string>{});
    m.def(
      "ext_path_url",
      ext_path_url,
      doc.intro("Construct a prefix container / urlencode(ts_name) fast")
        .intro("")
        .parameters()
        .parameter("prefix", "str", "like fame:// mydb:// or similar")
        .parameter("container", "str", "TS container.")
        .parameter("ts_path", "str", "Time-series path.")
        .returns("url", "str", "Constructed url with url-encoded ts_path")
        .see_also(
          "shyft_url, ext_query_url, urlencode, extract_shyft_url_container, extract_shyft_url_path, "
          "extract_shyft_url_query_parameters")(),
      py::arg("prefix"),
      py::arg("container"),
      py::arg("ts_path"));
    m.def(
      "ext_query_url",
      ext_query_url,
      doc.intro("Construct a prefix container?urlencode(ts_name) fast")
        .intro("")
        .parameters()
        .parameter("prefix", "str", "like fame:// mydb:// or similar")
        .parameter("container", "str", "TS container.")
        .parameter("ts_path", "str", "Time-series path.")
        .returns("url", "str", "Constructed url with url-encoded ts_path")
        .see_also(
          "shyft_url,ext_path_url, urlencode, extract_shyft_url_container, extract_shyft_url_path, "
          "extract_shyft_url_query_parameters")(),
      py::arg("prefix"),
      py::arg("container"),
      py::arg("ts_path"));
    m.def(
      "extract_shyft_url_container",
      extract_shyft_url_container,
      doc.intro("Extract the container part from a Shyft URL.")
        .parameters()
        .parameter("url", "str", "Shyft URL to extract container from.")
        .returns(
          "container",
          "str",
          "Container part of `url`, if the string is invalid as a Shyft URL\n\t"
          "an empty string is retuned instead.")
        .see_also("shyft_url")(),
      py::arg("url"));
    m.def(
      "extract_shyft_url_path",
      extract_shyft_url_path_after_container,
      doc.intro("Extract the time-series path part from a Shyft URL.")
        .parameters()
        .parameter("url", "str", "Shyft URL to extract the TS-path from.")
        .returns(
          "ts_path",
          "str",
          "TS-path part of `url`, if the string is invalid as a Shyft URL\n\t"
          "an empty string is retuned instead.")
        .see_also("shyft_url")(),
      py::arg("url"));
    m.def(
      "extract_shyft_url_query_parameters",
      extract_shyft_url_query_parameters,
      doc.intro("Extract query parameters from a Shyft URL.")
        .intro("")
        .intro("The query string is assumed to be on the format `?key1=value1&key2=value2&key3=&key4=value4`.")
        .intro("This will be parsed into a map with four keys: `key1` through `key4`, where `key3` have a")
        .intro("empty string value, while the rest have respectivly values `value1`, `value2`, and `value4`.")
        .intro("")
        .intro("Both query keys and values are assumed to be urlencoded, thus urlencode is called on every")
        .intro("key and every value.")
        .parameters()
        .parameter("url", "str", "Shyft URL to extract the TS-path from.")
        .returns(
          "queries",
          "Dict[str,str]",
          "A dict with all queries defined in `url`. The dictionary is "
          "empty if `url` is invalid as a Shyft URL.")
        .see_also("shyft_url, urldecode")(),
      py::arg("url"));
    m.def(
      "urlencode",
      urlencode,
      doc.intro("Percent-encode a string for use in URLs.")
        .intro("")
        .intro("All characters designated as reserved in RFC3986 (Jan. 2005), sec. 2.2 are always percent")
        .intro("encoded, while all character explitily stated as unreserved (RFC3986, Jan. 2005, sec. 2.3)")
        .intro("are never percent encoded. Space characters are encoded as `+` characters, while all other")
        .intro("characters are precent-encoded.")
        .intro("")
        .intro("The implementation only handles 8-bit character values. The behavior for multibyte characters")
        .intro("is unspecified.")
        .intro("")
        .intro("The reverse operation of this is `urldecode`.")
        .parameters()
        .parameter("text", "str", "Text string to encode.")
        .parameter(
          "percent_plus",
          "bool",
          "When true the `SP` character (ASCII 0x20) is encoded as `+` instead of its "
          "percent encoding `%20`. Defaults to true.")
        .returns("encoded", "str", "Percent-encoded representation if the input.")
        .see_also("urldecode")(),
      py::arg("text"),
      py::arg("space_pluss") = true);
    m.def(
      "urldecode",
      urldecode,
      doc.intro("Decode a percent-encoded string to its original representation.")
        .intro("")
        .intro("All characters designated as unreserved in RFC3986 (Jan. 2005), sec. 2.3 are always passed")
        .intro("through unmodified, except where they are encountered while parsing a percent-encoded value.")
        .intro("")
        .intro("The implementation only handles 8-bit character values. The behavior for multibyte characters")
        .intro("is unspecified.")
        .intro("Additionally it is undefined if characters outside the range `0-9A-Fa-f` are encountered as")
        .intro("one of the two characters immidiatly succeeding a percent-sign.")
        .intro("")
        .intro("This is the reverse operation of `urlencode`.")
        .parameters()
        .parameter("encoded", "str", "Text string to decode.")
        .parameter(
          "percent_plus",
          "bool",
          "When true `+` characters are decoded to `SP` characters (ASCII 0x20). When this "
          "is true and a `+` is encountered and exception is thrown. The default value is true.")
        .returns("text", "str", "Original representation of the encoded input.")
        .intro("")
        .raises()
        .raise(
          "RuntimeError",
          "Thrown if unencoded characters outside the unreserved range is encountered,"
          " includes `+` when the `percent_plus` argument is false. The exception message"
          " contains the character and its location in the string.")
        .see_also("urlencode")(),
      py::arg("encoded"),
      py::arg("space_pluss") = true);
  }

  void db_stuff(py::module_ &m) {
    py::class_<db_cfg> db_cfg_c(
      m,
      "DtssCfg",
      doc.intro("Configuration for google level db specific parameters.")
        .intro("")
        .intro(
          "Each parameter have reasonable defaults, have a look at google level db documentation for the effect\n"
          "of max_file_size, write_buffer_size and compression.\n"
          "The ppf remains constant once db is created (any changes will be ignored).\n"
          "The other can be changed on persisted/existing databases.\n"
          "\n"
          "About compression:\n"
          "Turns out although very effective for a lot of time-series, it have a single thread performance cost of "
          "2..3x\n"
          "native read/write performance due to compression/decompression.\n"
          "\n"
          "However, for geo dtss we are using multithreaded writes, so performance is limited to  the io-capacity,\n"
          "so it might be set to true for those kind of scenarios.\n")());

    db_cfg_c.def(py::init())
      .def(
        py::init<int64_t, bool, int64_t, int64_t, int64_t, int64_t, int64_t, int64_t>(),
        doc.intro("construct a DtssCfg  with all values specified")(),
        py::arg("ppf"),
        py::arg("compress"),
        py::arg("max_file_size"),
        py::arg("write_buffer_size"),
        py::arg("log_level") = 200,
        py::arg("test_mode") = 0,
        py::arg("ix_cache") = 0,
        py::arg("ts_cache") = 0)
      .def_readwrite(
        "ppf",
        &db_cfg::ppf,
        "int: (default 1024) ts-points per fragment(e.g.key/value), how large ts is chunked into fragments, "
        "read/write "
        "operations to key-value storage are in fragment sizes.")
      .def_readwrite(
        "compression",
        &db_cfg::compression,
        "bool: (default False), using snappy compression, could reduce storage 1::3 at similar cost of performance")
      .def_readwrite(
        "max_file_size",
        &db_cfg::max_file_size,
        "int: (default 100Mega), choose to make a reasonable number of files for storing time-series")
      .def_readwrite(
        "write_buffer_size", &db_cfg::write_buffer_size, "int: (default 10Mega), to balance write io-activity.")
      .def_readwrite(
        "log_level",
        &db_cfg::log_level,
        "int: default warn(200), trace(-1000),debug(0),info(100),error(300),fatal(400)")
      .def_readwrite(
        "test_mode", &db_cfg::test_mode, "int: for internal use only, should always be set to 0(the default)")
      .def_readwrite(
        "ix_cache",
        &db_cfg::ix_cache,
        "int: low-level index-cache, could be useful when working with large compressed databases")
      .def_readwrite(
        "ts_cache",
        &db_cfg::ts_cache,
        "int: low-level data-cache, could be useful in case of very large compressed databases")
      .def(py::self == py::self)
      .def(py::self == py::self);
    auto db_cfg_str = [](db_cfg const &c) {
      return fmt::format(
        "DtssCfg(ppf={}, compress={}, "
        "max_file_size={},write_buffer_size={},log_level={},test_mode={},ix_cache={},ts_cache={})",
        c.ppf,
        c.compression,
        c.max_file_size,
        c.write_buffer_size,
        c.log_level,
        c.test_mode,
        c.ix_cache,
        c.ts_cache);
    };
    db_cfg_c.def("__repr__", db_cfg_str).def("__repr__", db_cfg_str);
    m.def(
      "rocksdb_rollback_migration_at",
      ts_db_rocks::rollback_migration_at,
      doc
        .intro("Rollback a migrated dtss rocksdb at specified directory to its previous state using split rdb_h/rdb_d.")
        .intro(
          "Preconditions:\n\n"
          "  1. The rocksdb_root_dir should contain the rdb_d sub-directory with a migrated database.\n"
          "  2. There should no rdb_h sub-directory(true if the auto-migration was done, its renamed to .migrated).\n"
          "  3. The dtss services should be shutdown prior to rollback, the rollback needs exclusive access to the "
          "directories\n\n")
        .intro("The migration takes short time, and the rdb_d column-family `info` is then copied")
        .intro(
          "to a new rdb_h rocksdb  `header` database, and the `info` column-family is removed from the `rdb_d` "
          "database.")
        .intro("When done, a prior version of of Shyft can be run on the open directory.")
        .parameters()
        .parameter(
          "rocksdb_root_dir", "", "the root directory of the rocksdb, it should contain the rdb_d sub directory")
        .returns("success", "", "True if the rollback was successful.")
        .notes()
        .note("Should be called only once, will fail on second invocation due to preconditions.")
        .note("If pre-conditons are not met, RuntimeError is raised and the db is left untouched.")(),
      py::arg("rocksdb_root_dir"));
  }

  void dtss_diagnostics(py::module_ &m) {
    py::enum_<ts_diagnostics>(m, "TsDiagnostics", doc.intro("Diagnostics for failed to store time-series items.")())
      .value("UNDEFINED", ts_diagnostics::undefined)
      .value("MISS_MATCHED_RESOLUTION", ts_diagnostics::miss_matched_resolution)
      .value("EXTERNAL_STORE_FAILED", ts_diagnostics::external_store_failed)
      .export_values();

    py::enum_<lookup_error>(m, "LookupError", doc.intro("Diagnostics for dtss lookups.")())
      .value("UNKNOWN_SCHEMA", lookup_error::unknown_schema)
      .value("CONTAINER_NOT_FOUND", lookup_error::container_not_found)
      .value("CONTAINER_THROWS", lookup_error::container_throws)
      .export_values();

    py::class_<diagnostics> cdiag(
      m, "TsDiagnosticsItem", doc.intro("Pair of index and its TsDiagnostics code").intro("")());
    cdiag.def_readwrite("index", &diagnostics::ix, "int: The index in the stored list of the time-series that failed")
      .def_readwrite(
        "diagnostics", &diagnostics::diag, "TsDiagnostics: The diagnostics code for the failed time-series item")
      .def(py::self == py::self)
      .def(py::self != py::self);
    pyapi::expose_format(cdiag);
  }

  void pyexport(py::module_ &m) {
    dtss_cache_stats(m);
    dtss_diagnostics(m);
    dtss_store_policy_object(m);
    dtss_transfer_configuration(m);
    dtss_transfer_status(m);
    dtss_q_bridge_configuration(m);
    dtss_q_bridge_status(m);
    dtss_messages(m);
    db_stuff(m);
    dtss_q_messages(m);
    dtss_server(m);
    dtss_client(m);
    url_utils(m);
  }
}
