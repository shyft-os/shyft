#include <stdexcept>
#include <string>

#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>

#ifdef _WIN32

#include <sdkddkver.h>
#include <stdio.h>
#include <windows.h>
#pragma comment(lib, "ntdll.lib")
typedef long NTSTATUS;
EXTERN_C NTSTATUS NTAPI NtSetInformationProcess(HANDLE, ULONG, PVOID, ULONG);
// these values determined by poking around in the debugger - use at your own risk!
const DWORD ProcessInformationMemoryPriority = 0x27;
const DWORD ProcessInformationIoPriority = 0x21;
const DWORD DefaultMemoryPriority = 5;
const DWORD LowMemoryPriority = 3;
const DWORD DefaultIoPriority = 2;
const DWORD LowIoPriority = 1;

static void win_throw_on_error(DWORD r, std::string msg) {
  if (r != 0)
    throw std::runtime_error(msg + std::string(", error-code:") + std::to_string(GetLastError()));
}

static void win_set_priority(int p_class) {
  DWORD cpu, io, mem;
  if (p_class == 0) {
    cpu = NORMAL_PRIORITY_CLASS;
    mem = DefaultMemoryPriority;
    io = DefaultIoPriority;
  } else if (p_class == -1) {
    cpu = BELOW_NORMAL_PRIORITY_CLASS;
    mem = LowMemoryPriority;
    io = LowIoPriority;
  } else {
    throw std::runtime_error("Only 0=normal and -1=low are supported for priority_class");
  }
  // locate the functions for querying/setting memory and IO priority
  auto me = GetCurrentProcess(); // we don't need to release this accoring to
                                 // https://msdn.microsoft.com/en-us/library/windows/desktop/ms683179(v=vs.85).aspx
  if (!me)
    throw std::runtime_error("Failed to get current process handle");
  win_throw_on_error(!SetPriorityClass(me, cpu), "Failed setting cpu-priority");
  win_throw_on_error(
    NtSetInformationProcess(me, ProcessInformationMemoryPriority, &mem, sizeof(mem)), "Failed setting mem-priority");
  win_throw_on_error(
    NtSetInformationProcess(me, ProcessInformationIoPriority, &io, sizeof(io)), "Failed setting mem-priority");
}

std::string win_short_path(std::string const &long_path) {
  long length = GetShortPathName(long_path.c_str(), NULL, 0);
  std::string r(length + 1, '\0');
  length = GetShortPathName(long_path.c_str(), (char *) r.data(), length);
  r.resize(length);
  return r;
}
#else
//-- on linux we are not even close to having these problems
std::string win_short_path(std::string const &long_path) {
  return long_path;
}

void win_set_priority(int) {
}
#endif

namespace shyft {
  void pyexport_winutils(py::module_ &m) {
    m.def(
      "win_short_path",
      win_short_path,
      py::arg("path"),
      doc.intro("WinApi function GetShortPath exposed to python")
        .intro("https://msdn.microsoft.com/en-us/library/windows/desktop/aa364989(v=vs.85).aspx")
        .intro("Note that it only works for file-paths that exists, returns null string for not-existing files")
        .parameters()
        .parameter("path", "str", "a long path form")
        .returns(
          "short_path",
          "str",
          "windows 8.3 path string if on windows for *existing* files, otherwise same as input path")());
    m.def(
      "win_set_priority",
      win_set_priority,
      py::arg("p_class"),
      doc.intro("Win32 Api function to set normal (=0) or low(=-1) priority")
        .intro("This is *very* specific to windows, and especially task-scheduler/bg.tasks")
        .intro("get by default a complete garbled priority leaving the process you run")
        .intro("close to useless when it comes to cpu,io and memory performance")
        .intro("The UI does not help fixing this, and even the xml edit of files does not solve memory/io issues")
        .parameters()
        .parameter("p_class", "int", "priority class, 0=normal, -1=below normal(slightly above useless")());
  }
}
