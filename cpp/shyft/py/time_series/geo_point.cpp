/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/hydrology/geo_algorithms.h>
#include <shyft/hydrology/geo_point.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>

namespace shyft::core {

  void pyexport_geo_point(py::module_ &m) {
    py::class_<geo_point> c(
      m,
      "GeoPoint",
      "GeoPoint commonly used in the shyft::core for\n"
      "  representing a 3D point in the terrain model.\n"
      "  The primary usage is in geo-located time-series and the interpolation routines\n"
      "\n"
      "  Absolutely a primitive point model aiming for efficiency and simplicity.\n"
      "\n"
      "  Units of x,y,z are metric, z positive upwards to sky, represents elevation\n"
      "  x is east-west axis\n"
      "  y is south-north axis\n",
      py::dynamic_attr());
    c.def(py::init());
    c.def(
       py::init<double, double, double>(),
       doc.intro("construct a geo_point with x,y,z ")
         .parameters()
         .parameter("x", "float", "meter units")
         .parameter("y", "float", "meter units")
         .parameter("z", "float", "meter units")(),
       py::arg("x"),
       py::arg("y"),
       py::arg("z"))
      .def(
        py::init<geo_point const &>(),
        doc.intro("create a copy").parameters().parameter("clone", "GeoPoint", "the object to clone")(),
        py::arg("clone"))
      .def_readwrite("x", &geo_point::x, "float: east->west")
      .def_readwrite("y", &geo_point::y, "float: south->north")
      .def_readwrite("z", &geo_point::z, "float: ground->upwards")
      .def(
        "transform",
        +[](core::geo_point me, int f, int t) {
          return epsg_map(me, f, t);
        },
        doc.intro("compute transformed point `from_epsg` to `to_epsg` coordinate")
          .parameters()
          .parameter("from_epsg", "int", "interpret the current point as cartesian epsg coordinate")
          .parameter("to_epsg", "int", "the returned points cartesian epsg coordinate system")
          .returns("new point", "GeoPoint", "The new point in the specified cartesian epsg coordinate system")(),
        py::arg("from_epsg"),
        py::arg("to_epsg"))
      .def(
        "transform",
        +[](core::geo_point me, std::string const &f, std::string const &t) {
          return proj4_map(me, f, t);
        },
        doc.intro("compute transformed point `from_epsg` to `to_epsg` coordinate")
          .parameters()
          .parameter("from_proj4", "str", "interpret the current point as this proj4 specification")
          .parameter("to_proj4", "str", "the returned points proj4 specified coordinate system")
          .returns("new point", "GeoPoint", "The new point in the specified proj4 coordinate system")(),
        py::arg("from_proj4"),
        py::arg("to_proj4"))
      .def_static("distance2", &geo_point::distance2, "returns the euclidian distance^2 ", py::arg("a"), py::arg("b"))
      .def_static(
        "distance_measure",
        &geo_point::distance_measure,
        "return sum(a-b)^p",
        py::arg("a"),
        py::arg("b"),
        py::arg("p"),
        py::arg("zscale") = 0.0)
      .def_static(
        "zscaled_distance",
        &geo_point::zscaled_distance,
        "sqrt( (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale)",
        py::arg("a"),
        py::arg("b"),
        py::arg("zscale"))
      .def_static(
        "xy_distance",
        geo_point::xy_distance,
        "returns sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y))",
        py::arg("a"),
        py::arg("b"))
      .def_static(
        "difference",
        geo_point::difference,
        "returns GeoPoint(a.x - b.x, a.y - b.y, a.z - b.z)",
        py::arg("a"),
        py::arg("b"))
      .def(py::self == py::self)
      .def(py::self != py::self);
    pyapi::expose_format(c);
  }

  static auto create_from_x_y_z_vectors(
    std::vector<double> const &x,
    std::vector<double> const &y,
    std::vector<double> const &z) {
    if (!(x.size() == y.size() && y.size() == z.size()))
      throw std::runtime_error("x,y,z vectors need to have same number of elements");
    std::vector<core::geo_point> r(x.size());
    for (std::size_t i = 0; i < x.size(); ++i)
      r[i] = core::geo_point(x[i], y[i], z[i]);
    return r;
  }

  void pyexport_geo_point_vector(py::module_ &m) {

    auto c = pyapi::bind_vector<std::vector<core::geo_point>>(m, "GeoPointVector", "A vector, list, of GeoPoints", py::dynamic_attr())
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;

    c.def(
       "polygon_buffer",
       &core::polygon_buffer,
       doc.intro("Computes the buffer polygon for the supplied points using the `distance` parameter.")
         .intro("boost.geometry.buffer algorithm is used, and if it results in multiple polygons, then")
         .intro("boost.convex_hull is used to reduce it from many to one polygon envelope.")
         .intro("If number of points is large, currently > 50,  a reduction algorithm using convex_hull is applied as "
                "first step.")
         .intro("On the return, the point `z` coordinate is set to the average of the polygon points `z`.")
         .parameters()
         .parameter("distance", "float", "The buffer distance to nearest point in self")
         .returns("polygon", "GeoPointVector", "A closed buffer polygon surrounding the passed points")(),
       py::arg("distance"))
      .def(
        "transform",
        +[](std::vector<core::geo_point> const &me, int f, int t) {
          return core::epsg_map(me, f, t);
        },
        doc.intro("compute transformed point `from_epsg` to `to_epsg` coordinate")
          .parameters()
          .parameter("from_epsg", "int", "interpret the current point as cartesian epsg coordinate")
          .parameter("to_epsg", "int", "the returned points cartesian epsg coordinate system")
          .returns("new point", "GeoPointVector", "The new points in the specified cartesian epsg coordinate system")(),
        py::arg("from_epsg"),
        py::arg("to_epsg"))
      .def(
        "transform",
        +[](std::vector<core::geo_point> const &me, std::string const &f, std::string const &t) {
          return core::proj4_map(me, f, t);
        },
        doc.intro("compute transformed point `from_epsg` to `to_epsg` coordinate")
          .parameters()
          .parameter("from_proj4", "str", "interpret the current point as this proj4 specification")
          .parameter("to_proj4", "str", "the returned points proj4 specified coordinate system")
          .returns("new GeoPointVector", "GeoPointVector", "The new points in the specified proj4 coordinate system")(),
        py::arg("from_proj4"),
        py::arg("to_proj4"))
      .def(
        "area",
        +[](std::vector<core::geo_point> const &me) {
          return core::polygon_area(me);
        },
        doc
          .intro("compute polygon area, if less than 3 points NAN is returned, otherwise "
                 "boost::geometry::area(polygon)")
          .returns("area", "float", "the area in m2 (or according to coordinate projection)")())
      .def(
        "mid_point",
        +[](std::vector<core::geo_point> const &me) {
          return core::polygon_mid_point(me);
        },
        doc
          .intro("compute polygon mid point x,y as boost::geometry::centroid, and z as average of points.z(leaving out "
                 "the closing point).")
          .returns("mid_point", "GeoPoint", "The computed mid point, (nan,nan,nan) if there are no points")())
      .def(
        "nearest_neighbours",
        +[](std::vector<core::geo_point> const &me, core::geo_point p, int n, double z_scale) {
          return core::nearest_neighbours(me, p, std::size_t(n), z_scale);
        },
        doc.intro("Return the `n` nearest neighbours of point `p` using `z_scaled` distance  for `z` direction")
          .parameters()
          .parameter("p", "GeoPoint", "Finds nearest neighbours to this point")
          .parameter("n", "int", "Number of neighbours to find")
          .parameter("z_scale", "float", "Default 0.0, e.g. only x,y direction counts, if 1.0, z counts equal etc.")
          .returns("neighbours", "GeoPointVector", "The neighbours found")(),
        py::arg("p"),
        py::arg("n"),
        py::arg("z_scale") = 0.0);
    c.def_static(
      "create_from_x_y_z",
      create_from_x_y_z_vectors,
      "Create a GeoPointVector from x,y and z DoubleVectors of equal length",
      py::arg("x"),
      py::arg("y"),
      py::arg("z"));

    auto cv = pyapi::bind_vector<std::vector<std::vector<core::geo_point>>>(
      m, "GeoPointVectorVector", "A strongly typed list, of GeoPointVectors")
      .def(py::self == py::self)
      .def(py::self != py::self)
      ;

    cv.def(
        "areas",
        &core::polygons_area,
        doc.intro("Returns the polygon area for each polygon in the list")
          .parameters()
          .returns("areas", "DoubleVector", "The computed polygon areas")())
      .def(
        "mid_points",
        &core::polygons_mid_points,
        doc.intro("Returns the polygon mid_point area for each polygon in the list")
          .parameters()
          .returns("mid_points", "GeoPointVector", "The computed mid-points")())
      .def(
        "intersection_areas",
        &core::polygon_intersection_areas,
        doc.intro("Returns the intersection area with `polygon`, each polygon in the list")
          .intro("Notice that the algo first compute the 8 nearest neighbours, then computes intersections of those.")
          .parameters()
          .parameter("polygon", "GeoPointVector", "The polygon to compute the intersection with")
          .parameter(
            "n_max_neighbours",
            "int",
            "Limits the intersection work considering only the nearest neighbours, default 8")
          .returns(
            "areas", "DoubleVector", "The computed intersection area for each polygon in polygons, -the same order")(),
        py::arg("polygon"),
        py::arg("n_max_neighbours") = 8);
  }

}
