#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/formatters.h>
#include <shyft/srv/fast_server_iostream.h>

namespace shyft::srv {

  void pyexport_server_config(py::module_ &m) {
    auto lconf =
      py::class_<log_config>(m, "LogConfig", doc.intro("Class to configure log on a shyft process")())
        .def(
          py::init<std::string, int>(),
          doc.intro("Provide file and log level")
            .parameters()
            .parameter("file", "string", "Location of logfile, empty string resolves to stdout")
            .parameter(
              "level",
              "int",
              " -1000=everything, -100=TRACE, 0=DEBUG,100=INFO,200=WARN,300=ERROR,400=FATAL,1000=nologs")(),
          py::arg("file") = "",
          py::arg("level") = dlib::LWARN.priority)
        .def_readwrite("file", &log_config::file, "str, logfile location")
        .def_readwrite("level", &log_config::level, "int, minimum level to show");
    auto lconf_str = [](log_config const &c) {
      return fmt::format("shyft.time_series.LogConfig(file='{}', level={})", c.file, c.level);
    };
    lconf.def("__repr__", lconf_str);
    lconf.def("__str__", lconf_str);

    auto server_config =
      py::class_<fast_server_iostream_config>(m, "ServerConfig", doc.intro("Class to configure server instances")())
        .def(
          py::init<core::utctimespan, core::utctimespan, log_config>(),
          py::arg("stale_duration") = core::utctime{3'600'000'000l},
          py::arg("stale_sweep_interval") = core::utctime{500'000l},
          py::arg("log") = log_config{})
        .def_readwrite(
          "stale_duration", &fast_server_iostream_config::stale_duration, "time: time for a connection to go stale")
        .def_readwrite(
          "stale_sweep_interval", &fast_server_iostream_config::stale_sweep_interval, "time: time between stale checks")
        .def_readwrite("log_config", &fast_server_iostream_config::log, "LogConfig: ");
    auto server_config_str = [&](fast_server_iostream_config const &c) {
      return fmt::format(
        "shyft.time_series.ServerConfig(stale_duration={}, stale_sweep_interval={}, log={})",
        c.stale_duration,
        c.stale_sweep_interval,
        lconf_str(c.log));
    };
    server_config.def("__repr__", server_config_str);
    server_config.def("__str__", server_config_str);
  }
}
