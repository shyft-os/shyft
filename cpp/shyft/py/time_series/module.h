#pragma once

#include <cmath>
#include <cstdint>
#include <stdexcept>

#include <fmt/core.h>

#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

namespace shyft {
  void pyexport_winutils(py::module_ &);
}

namespace shyft::core {
  void pyexport_geo_point(py::module_ &);
  void pyexport_geo_point_vector(py::module_ &m);

  constexpr auto utctime_range_check(auto sec) {
    if (std::abs(double(sec)) > to_seconds(max_utctime))
      throw std::runtime_error(
        fmt::format("time(s) construct:  specified seconds, {}, is outside range min_utctime .. max_utctime", sec));
    return sec;
  }

  void pyexport_calendar_and_time(py::module_ &);
}

namespace shyft::srv {
  void pyexport_model_info(py::module_ &);
  void pyexport_server_config(py::module_ &);
}

namespace shyft::dtss {
  void pyexport(py::module_ &);
}

namespace shyft::time_axis {
  generic_dt time_axis_merge(generic_dt const &a, generic_dt const &b);
  void pyexport(py::module_ &);
}

namespace shyft::time_series {
  void pyexport_time_series(py::module_ &);
  void pyexport_geo_time_series(py::module_ &);
  void pyexport(py::module_ &);
}
