/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <cstdint>
#include <string>

#include <shyft/py/bindings.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/containers.h>
#include <shyft/py/formatters.h>
#include <shyft/srv/model_info.h>

namespace shyft::srv {
  void pyexport_model_info(py::module_ &m) {
    py::class_<model_info> c(
      m, "ModelInfo", doc.intro("Provides model-information useful for selection and filtering")());
    c
      .def(py::init())
      .def(
       py::init<std::int64_t, std::string const &, core::utctime, std::string>(),
       doc.intro("Constructs ModelInfo with the supplied parameters")
         .parameters()
         .parameter("id", "int", "the unique model id")
         .parameter("name", "str", "any useful name or description")
         .parameter("created", "time", "time of creation")
         .parameter("json", "str", "extra information, preferably a valid json")(),
       py::arg("id"),
       py::arg("name"),
       py::arg("created"),
       py::arg("json") = std::string{""})
      .def_readwrite("id", &model_info::id, "int: the unique model id, can be used to retrieve the real model")
      .def_readwrite("name", &model_info::name, "str: any useful name or description")
      .def_readwrite("created", &model_info::created, "time: the time of creation, or last modification of the model")
      .def_readwrite(
        "json",
        &model_info::json,
        "str: a json formatted string to enable scripting and python to store more information")
      .def(py::self == py::self)
      .def(py::self != py::self);
    pyapi::expose_format(c);
  }
}
