/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <cstdint>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <shyft/core/dlib_utils.h>
#include <shyft/core/fs_compat.h>
#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/time_series/module.h>
#include <shyft/srv/model_info.h>
#include <shyft/version.h>
#ifdef _MSC_VER
#include <boost/dll.hpp>

#include <shyft/core/fs_compat.h>

#include <winsock2.h>
#endif
namespace shyft::time_series {

  static auto byte_vector_from_file(std::string path) {
    std::ostringstream buf(std::ios::binary);
    std::ifstream input;
    input.open(path.c_str(), std::ios::in | std::ios::binary);
    if (!input.is_open())
      throw std::runtime_error(fmt::format("failed to open file for read: {}", path));
    buf << input.rdbuf();
    auto s = buf.str();
    return std::vector<char>{std::begin(s), std::end(s)};
  }

  static void byte_vector_to_file(std::string path, std::vector<char> const &bytes) {
    std::ofstream out;
    out.open(path, std::ios::out | std::ios::binary | std::ios::trunc);
    if (!out.is_open())
      throw std::runtime_error(fmt::format("failed to open file for write: {}", path));
    out.write(bytes.data(), bytes.size());
    out.flush();
    out.close();
  }

  void pyfinalize() {
#ifdef _MSC_VER
    // FIXME: might be needed for mingw as well
    WSACleanup(); // NOTE: for dtss
#endif
  }

  void pyexport_module_attributes(py::module_ &m) {
    m.attr("__doc__") = "Shyft time-series api";
    m.attr("__version__") = _version_string();
    m.attr("__release__") = _release_string();
    m.attr("__release_type__") = _release_type;
    auto builtins = py::module_::import("builtins");
    auto list_class = builtins.attr("list");
    // m.attr("ByteVector") = list_class;
    auto bv = pyapi::bind_vector<std::vector<char>>(m, "ByteVector", "A binary blob of bytes");
    bv.def("__str__", [](std::vector<char> const &v) {
      return pyapi::to_hex(v);
    });
    bv.def("__repr__", [](std::vector<char> const &v) {
      return fmt::format("ByteVector()");
    });
    m.attr("UtcTimeVector") = list_class;
    m.attr("DoubleVector") = list_class;
    m.attr("IntVector") = list_class;
    m.attr("StringVector") = list_class;
    m.attr("TsInfoVector") = list_class;
    m.attr("TsBindInfoVector") = list_class;
    m.attr("ModelInfoVector") = list_class;
    m.attr("RatingCurveSegments") = list_class;
    m.attr("RatingCurveTimeFunctions") = list_class;
    // m.attr("TsVectorSet")= list_class;
    std::string lib_path;
    // Windows/special: fixup os.add_dll_directory(path) so that the *other*
    // python extensions of shyft works.
    // notice that only paths within dll_paths are valid(the general PATH does not work)
    //
#ifdef _MSC_VER
    try {
      auto const module_path = boost::dll::this_line_location(); // Path to the time_seres.so file
      auto const lib_dir = module_path.parent_path();            // shyft dir hydrology.so
      lib_path = fs::absolute(lib_dir).string();
      auto const os = py::module_::import("os");
      os.attr("add_dll_directory")(lib_path);
    } catch (...) {
      lib_path = "";
    }
#endif
    // Expose the lib_path to Python
    m.attr("lib_path") = lib_path;
  }

  void pyexport(py::module_ &m) {
    pyexport_module_attributes(m);
    pyexport_calendar_and_time(m);
    pyexport_winutils(m);
    pyexport_geo_point(m);
    pyexport_geo_point_vector(m);
    time_axis::pyexport(m);
    pyexport_time_series(m);

    m.def(
      "byte_vector_from_file",
      byte_vector_from_file,
      "reads specified file and returns its contents as a ByteVector",
      py::arg("ByteVector"));
    m.def(
      "byte_vector_to_file",
      byte_vector_to_file,
      "write the supplied ByteVector to file as specified by path",
      py::arg("path"),
      py::arg("byte_vector"));
    m.def(
      "byte_vector_from_hex_str",
      pyapi::to_byte_vector,
      "converts earlier byte-vector hex-string,[0-9a-f]x2 etc, to byte-vector",
      py::arg("hex_str"));
    m.def(
      "byte_vector_to_hex_str",
      pyapi::to_hex,
      "return hex-string [0-9a-f]x2 etc of byte-vector",
      py::arg("byte_vector"));

    pyexport_geo_time_series(m);
    srv::pyexport_model_info(m);
    srv::pyexport_server_config(m);
    dtss::pyexport(m);

    m.def(
      "log_to_file",
      core::logging::output_file,
      doc
        .intro(
          "Direct all logs to specified file, leaving `filename` ='' logs to stdout\n"
          "This affects all logging in Shyft.\n"
          "In particular the dtss backends, rocksdb and leveldb will generate verbose output\n"
          "when they are configured to log via the `DtssCfg` class."
          "Note: Ensure there is space on device to consume the logs, and ensure separate log partition for "
          "sensitive/critical systems")
        .parameters()
        .parameter("filename", "str", "path to file")(),
      py::arg("filename"));
    m.def(
      "log_level",
      +[](int ll) {
        core::logging::level(dlib::log_level{ll, "custom"});
      },
      doc
        .intro(
          "Set log_level for **all**  loggers,\n"
          "Note: Ensure there is space on device to consume the logs, and ensure separate log partition for "
          "sensitive/critical systems")
        .parameters()
        .parameter(
          "log_level",
          "int",
          " -1000=everything, -100=TRACE, 0=DEBUG,100=INFO,200=WARN,300=ERROR,400=FATAL,1000=nologs")(),
      py::arg("log_level"));
  }
}

SHYFT_PYTHON_MODULE(time_series, m) {
  shyft::time_series::pyexport(m);
}
