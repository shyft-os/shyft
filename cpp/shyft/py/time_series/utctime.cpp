/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <cstdint>
#include <functional>
#include <string>

#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/py/time_series/module.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::core {

  static void pyexport_utctime(py::module_ &m) {
    auto get_int_seconds = +[](utctime const &dt) {
      auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
      if (dt_s == dt)
        return std::int64_t(dt_s.count());
      return std::int64_t(to_seconds(dt));
    };
    py::enum_<core::trim_policy>(
      m,
      "trim_policy",
      doc.intro(
        "Enum to decide if to trim inwards or outwards where TRIM_IN means inwards, TRIM_ROUND rounds "
        "halfway "
        "cases away from zero.")())
      .value("TRIM_IN", core::trim_policy::TRIM_IN)
      .value("TRIM_OUT", core::trim_policy::TRIM_OUT)
      .value("TRIM_ROUND", core::trim_policy::TRIM_ROUND)
      .export_values();

    auto t_time =
      py::class_<utctime>(
        m,
        "time",
        doc.intro("**time** is represented as a number, in SI-unit seconds.\n")
          .intro(
            "For accuracy and performance, it's internally represented as 64bit integer, at micro-second "
            "resolution")
          .intro("It is usually used in two roles:\n\n")
          .intro(" 1) time measured in seconds since epoch (1970.01.01UTC)")
          .intro("      often constructed using the Calendar class that takes calendar-coordinates (YMDhms etc) and ")
          .intro("      returns the corresponding time-point, taking time-zone and dst etc. into account.\n")
          .intro(">>>      utc = Calendar()")
          .intro(">>>      t1 = utc.time(2018,10,15,16,30,15)")
          .intro(">>>      t2 = time('2018-10-15T16:30:15Z')")
          .intro(">>>      t3 = time(1539621015)\n\n")
          .intro(" 2) time measure, in unit of seconds (resolution up to 1us)")
          .intro("      often constructed from numbers, always use SI-unit of seconds\n")
          .intro(">>>      dt1 = time(3600)  # 3600 seconds")
          .intro(">>>      dt2 = time(0.123456)  #  0.123456 seconds\n")
          .intro("\n")
          .intro("It can be constructed supplying number of seconds, or a well defined iso8601 string\n")
          .intro("To convert it to a python number, use float() or int() to cast operations")
          .intro(
            "If dealing with time-zones/calendars conversion, use the calendar.time(..)/.calendar_units "
            "functions.")
          .intro(
            "If you want to use time-zone/calendar semantic add/diff/trim, use the corresponding Calendar "
            "methods.")
          .intro("")
          .see_also("Calendar,deltahours,deltaminutes")())
        .def(py::init([] {
          return utctime{};
        }))
        .def(
          py::init([](std::int64_t v) {
            return utctime_from_seconds64(utctime_range_check(v));
          }),
          py::arg("seconds"))
        .def(
          py::init([](double v) {
            return from_seconds(utctime_range_check(v));
          }),
          py::arg("seconds"))
        .def(
          py::init([](std::string const &v) {
            return create_from_iso8601_string(v);
          }),
          doc.intro("Construct time from an iso8601 YYYY-MM-DDThh:mm:ss[.xxxxxx]Z string")
            .parameters()
            .parameter("time", "str", "An iso8601 time string")(),
          py::arg("time"))
        .def(py::init([](utctime v) {
          return utctime(v);
        }))
        .def(
          py::pickle(
            [](utctime const &t) {
              return py::make_tuple(t.count());
            },
            [](py::tuple const &t) {
              return utctime(t[0].cast<std::int64_t>());
            }))
        .def_property_readonly(
          "seconds",
          [](utctime const &dt) -> std::variant<std::int64_t, double> {
            auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
            if (dt_s == dt)
              return std::int64_t(dt_s.count());
            return to_seconds(dt);
          })
        .def(
          "__abs__",
          +[](utctime const &x) {
            return x >= x.zero() ? x : -x;
          })
        .def(
          "__round__",
          +[](utctime const &t) {
            return from_seconds(std::round(to_seconds(t)));
          })
        .def("__float__", &core::to_seconds)
        .def("__int__", get_int_seconds, doc.intro("time as int seconds")())
        .def("__long__", get_int_seconds, doc.intro("time as int seconds")())
        .def(
          "__repr__",
          +[](utctime const &dt) {
            auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
            if (dt == no_utctime)
              return std::string("time.undefined");
            else if (dt == max_utctime)
              return std::string("time.max");
            else if (dt == min_utctime)
              return std::string("time.min");
            else {
              std::string s;
              if (dt_s == dt)
                s = fmt::format("time({})", dt_s.count());
              else
                s = fmt::format("time({:.6f})", to_seconds(dt));
              return s;
            }
          })
        .def(
          "__str__",
          [](utctime const &dt) {
            // note: this is somewhat fuzzy logic to make nice- strings in the debugger...
            // presenting it as a iso 8601 if > 1 year from 1970, otherwise just report seconds
            auto dt_s = std::chrono::duration_cast<std::chrono::seconds>(dt);
            static calendar utc;
            auto max_year = utc.time(YMDhms::YEAR_MAX, 1, 1);
            auto min_year = utc.time(YMDhms::YEAR_MIN, 1, 1);

            if (
              (dt > calendar::YEAR || dt < -calendar::YEAR)
              && (dt < max_year && dt > min_year)) { // assume user want to see year..
              return utc.to_string(dt);
            } else {
              std::string s;
              if (dt_s == dt) {
                s = fmt::format("{}s", dt_s.count());
              } else {
                if (dt == no_utctime)
                  s = "<undefined>";
                else if (dt == max_utctime)
                  s = "+oo";
                else if (dt == min_utctime)
                  s = "-oo";
                else
                  s = fmt::format("{:.6f}s", to_seconds(dt));
              }
              return s;
            }
          })
        .def(py::self + py::self)
        .def(py::self - py::self)
        .def(
          "__radd__",
          [](utctime const &t, std::int64_t o) {
            return t + from_seconds(utctime_range_check(o));
          })
        .def(
          "__radd__",
          [](utctime const &t, double o) {
            return t + from_seconds(utctime_range_check(o));
          })
        .def(
          "__rsub__",
          [](utctime const &t, std::int64_t o) {
            return from_seconds(utctime_range_check(o)) - t;
          })
        .def(
          "__rsub__",
          [](utctime const &t, double o) {
            return from_seconds(utctime_range_check(o)) - t;
          })
        .def(
          "__mod__",
          [](utctime const &t, utctime const &o) {
            return t % o;
          })
        .def(
          "__mul__",
          [](utctime const &t, utctime const &o) {
            return from_seconds(to_seconds(t) * to_seconds(o));
          })
        .def(
          "__rmul__",
          [](utctime const &t, double o) {
            return from_seconds(to_seconds(t) * o);
          })
        .def(
          "__truediv__",
          [](utctime const &t, utctime const &o) {
            return from_seconds(to_seconds(t) / to_seconds(o));
          })
        .def(
          "__rtruediv__",
          [](utctime const &t, double o) {
            return from_seconds(o / to_seconds(t));
          })
        .def(
          "__floordiv__",
          [](utctime const &t, utctime const &o) {
            return from_seconds(truncf(to_seconds(t) / to_seconds(o)));
          })
        .def(
          "__rfloordiv__",
          [](utctime const &t, double o) {
            return from_seconds(truncf(o / to_seconds(t)));
          })
        .def(-py::self)
        .def(
          "__hash__",
          +[](utctime const &t) {
            return std::hash<std::int64_t>{}(t.count());
          })
        .def_property_readonly_static(
          "min",
          +[](py::object const &) {
            return min_utctime;
          })
        .def_property_readonly_static(
          "max",
          +[](py::object const &) {
            return max_utctime;
          })
        .def_property_readonly_static(
          "undefined",
          +[](py::object const &) {
            return no_utctime;
          })
        .def_property_readonly_static(
          "epoch",
          +[](py::object const &) {
            return utctime_0;
          })
        .def_static(
          "now", +[] {
            return utctime_now();
          });

    pyapi::expose_comparison(t_time);
    py::implicitly_convertible<std::string, utctime>();
    py::implicitly_convertible<std::int64_t, utctime>();
    py::implicitly_convertible<double, utctime>();


    m.def("utctime_now", utctime_now, "returns time now as seconds since 1970s");
    m.def("deltahours", deltahours, "returns time equal to specified n hours", py::arg("n"));
    m.def("deltaminutes", deltaminutes, "returns time equal to specified n minutes", py::arg("n"));
    m.def(
      "is_npos",
      [](std::size_t n) {
        return n == std::string::npos;
      },
      "returns true if n is npos, - meaning no position",
      py::arg("n"));

    m.attr("max_utctime") = max_utctime;
    m.attr("min_utctime") = min_utctime;
    m.attr("no_utctime") = no_utctime;
    m.attr("npos") = std::string::npos;
  }

  static void pyexport_calendar(py::module_ &m) {
    utctime (calendar::*time_YMDhms)(YMDhms) const = &calendar::time;
    utctime (calendar::*time_YWdhms)(YWdhms) const = &calendar::time;
    utctime (calendar::*time_6)(int, int, int, int, int, int, int) const = &calendar::time;
    utctime (calendar::*time_from_week_6)(int, int, int, int, int, int, int) const = &calendar::time_from_week;

    auto t_YMDhms =
      py::class_<YMDhms>(
        m,
        "YMDhms",
        doc.intro("Defines calendar coordinates as Year Month Day hour minute second and micro_second")
          .intro("The intended usage is ONLY as result from the Calendar.calendar_units(t),")
          .intro("to ensure a type-safe return of these entities for a given time.")
          .note("Please use this as a read-only return type from the Calendar.calendar_units(t)")())
        .def(
          py::init<int, int, int, int, int, int, int>(),
          "Creates calendar coordinates specifying Y,M,D,h,m,s,us",
          py::arg("Y"),
          py::arg("M") = 1,
          py::arg("D") = 1,
          py::arg("h") = 0,
          py::arg("m") = 0,
          py::arg("s") = 0,
          py::arg("us") = 0)
        .def("is_valid", &YMDhms::is_valid, "returns true if YMDhms values are reasonable")
        .def("is_null", &YMDhms::is_null, "returns true if all values are 0, - the null definition")
        .def_readwrite("year", &YMDhms::year, "int: ")
        .def_readwrite("month", &YMDhms::month, "int: ")
        .def_readwrite("day", &YMDhms::day, "int: ")
        .def_readwrite("hour", &YMDhms::hour, "int: ")
        .def_readwrite("minute", &YMDhms::minute, "int: ")
        .def_readwrite("second", &YMDhms::second, "int: ")
        .def_readwrite("micro_second", &YMDhms::micro_second, "int: ")
        .def_static("max", &YMDhms::max, "returns the maximum representation")
        .def_static("min", &YMDhms::min, "returns the minimum representation")
        .def(py::self == py::self)
        .def(py::self != py::self);

    {
      auto str = +[](YMDhms const &t) {
        return fmt::format(
          "YMDhms({},{},{},{},{},{},{})", t.year, t.month, t.day, t.hour, t.minute, t.second, t.micro_second);
      };
      t_YMDhms.def("__str__", str);
      t_YMDhms.def("__repr__", str);
    }

    auto t_YWdhms =
      py::class_<YWdhms>(
        m,
        "YWdhms",
        doc.intro("Defines calendar coordinates as iso Year Week week-day hour minute second and micro_second")
          .intro("The intended usage is ONLY as result from the Calendar.calendar_week_units(t),")
          .intro("to ensure a type-safe return of these entities for a given time.")
          .notes()
          .note("Please use this as a read-only return type from the Calendar.calendar_week_units(t)")())
        .def(
          py::init<int, int, int, int, int, int, int>(),
          doc.intro("Creates calendar coordinates specifying iso Y,W,wd,h,m,s")
            .parameters()
            .parameter("Y", "int", "iso-year")
            .parameter("W", "int", "iso week [1..53]")
            .parameter("wd", "int", "week_day [1..7]=[mo..sun]")
            .parameter("h", "int", "hour [0..23]")
            .parameter("m", "int", "minute [0..59]")
            .parameter("s", "int", "second [0..59]")
            .parameter("us", "int", "micro_second [0..999999]")(),
          py::arg("Y") = 0,
          py::arg("W") = 1,
          py::arg("wd") = 1,
          py::arg("h") = 0,
          py::arg("m") = 0,
          py::arg("s") = 0,
          py::arg("us") = 0)
        .def("is_valid", &YWdhms::is_valid, "returns true if YWdhms values are reasonable")
        .def("is_null", &YWdhms::is_null, "returns true if all values are 0, - the null definition")
        .def_readwrite("iso_year", &YWdhms::iso_year, "int: ")
        .def_readwrite("iso_week", &YWdhms::iso_week, "int: ")
        .def_readwrite("week_day", &YWdhms::week_day, doc.intro("int: week_day,[1..7]=[mo..sun]")())
        .def_readwrite("hour", &YWdhms::hour, "int: ")
        .def_readwrite("minute", &YWdhms::minute, "int: ")
        .def_readwrite("second", &YWdhms::second, "int: ")
        .def_readwrite("micro_second", &YWdhms::micro_second, "int: ")
        .def_static("max", &YWdhms::max, "returns the maximum representation")
        .def_static("min", &YWdhms::min, "returns the minimum representation")
        .def(py::self == py::self)
        .def(py::self != py::self);

    {
      auto str = +[](YWdhms const &t) {
        return fmt::format(
          "YWdhms({},{},{},{},{},{},{})",
          t.iso_year,
          t.iso_week,
          t.week_day,
          t.hour,
          t.minute,
          t.second,
          t.micro_second);
      };
      t_YWdhms.def("__str__", str);
      t_YWdhms.def("__repr__", str);
    }


    py::class_<time_zone::tz_info_t, std::shared_ptr<time_zone::tz_info_t>>(
      m,
      "TzInfo",
      doc.intro(
        "The TzInfo class is responsible for providing information about the\n"
        "time-zone of the calendar.\n"
        "This includes:\n\n"
        " * name (olson identifier)\n"
        " * base_offset\n"
        " * utc_offset(t) time-dependent\n\n"
        "The Calendar class provides a shared pointer to it's TzInfo object")())
      .def(py::init<utctimespan>(), "creates a TzInfo with a fixed utc-offset(no dst-rules)", py::arg("base_tz"))
      .def("name", &time_zone::tz_info_t::name, "returns the olson time-zone identifier or name for the TzInfo")
      .def("base_offset", &time_zone::tz_info_t::base_offset, "returnes the time-invariant part of the utc-offset")
      .def(
        "utc_offset",
        &time_zone::tz_info_t::utc_offset,
        "returns the utc_offset at specified utc-time, takes DST into account if applicable",
        py::arg("t"))
      .def(
        "is_dst", &time_zone::tz_info_t::is_dst, "returns true if DST is observed at given utc-time t", py::arg("t"));
    py::class_<calendar, std::shared_ptr<calendar>>(
      m,
      "Calendar",
      doc.intro("Calendar deals with the concept of a human calendar\n")
        .intro("In Shyft we practice the 'utc-time perimeter' principle,\n")
        .intro("  * the core is utc-time exclusively ")
        .intro("  * we deal with time-zone and calendars at the interfaces/perimeters\n")
        .intro(
          "In python, this corresponds to timestamp[64], or as the integer version of the time package "
          "representation")
        .intro("e.g. the difference between time.time()- utctime_now() is in split-seconds")
        .intro("\n")
        .intro("Calendar functionality:\n")
        .intro(
          " * Conversion between the calendar coordinates YMDhms or iso week YWdhms and utctime, taking  any "
          "timezone and DST into account\n")
        .intro(" * Calendar constants, utctimespan like values for Year,Month,Week,Day,Hour,Minute,Second\n")
        .intro(" * Calendar arithmetic, like adding calendar units, e.g. day,month,year etc.\n")
        .intro(
          " * Calendar arithmetic, like trim/truncate a utctime down to nearest timespan/calendar unit. eg. "
          "day\n")
        .intro(
          " * Calendar arithmetic, like calculate difference in calendar units(e.g days) between two utctime "
          "points\n")
        .intro(" * Calendar Timezone and DST handling\n")
        .intro(" * Converting time to string and vice-versa\n")
        .intro("\n")
        .notes()
        .note("Please notice that although the calendar concept is complete:\n")
        .note(" * We only implement features as needed in the core and interfaces")
        .note("   Currently this includes most options, including olson time-zone handling")
        .note("   The time-zone support is currently a snapshot of rules ~2023")
        .note("   but we plan to use standard packages like Howard Hinnant's online approach for this later.")
        .note(" * Working range for DST is 1905..2105 (dst is considered 0 outside)")
        .note(" * Working range,validity of the calendar computations is limited to gregorian as of boost::date.")
        .note(" * Working range avoiding overflows is -4000 .. + 4000 Years")())
      .def(py::init(), doc.intro("creates a calendar")())
      .def(
        py::init<int>(),
        doc.intro("creates a calendar with constant tz-offset")
          .parameters()
          .parameter("tz_offset", "int", "seconds utc offset, 3600 gives UTC+01 zone")(),
        py::arg("tz_offset"))
      .def(
        py::init<std::string>(),
        doc.intro("create a Calendar from Olson timezone id, eg. 'Europe/Oslo'")
          .parameters()
          .parameter("olson_tz_id", "str", "Olson time-zone id, e.g. 'Europe/Oslo'")(),
        py::arg("olson_tz_id"))
      .def(
        "to_string",
        static_cast<std::string (calendar::*)(utctime) const>(&calendar::to_string),
        doc.intro("convert time t to readable iso standard string taking ")
          .intro(" the current calendar properties, including timezone into account")
          .parameters()
          .parameter("utctime", "time", "seconds utc since epoch")
          .returns("iso time string", "str", "iso standard formatted string,including tz info")(),
        py::arg("time"))
      .def(
        "to_string",
        static_cast<std::string (calendar::*)(utcperiod) const>(&calendar::to_string),
        doc
          .intro(
            "convert utcperiod p to readable string taking current calendar properties, including timezone "
            "into "
            "account")
          .parameters()
          .parameter("utcperiod", "UtcPeriod", "An UtcPeriod object")
          .returns("period-string", "str", "[start..end>, iso standard formatted string,including tz info")(),
        py::arg("utcperiod"))
      .def_static(
        "region_id_list",
        &calendar::region_id_list,
        doc.intro("Returns a list over predefined Olson time-zone identifiers")
          .notes()
          .note("the list is currently static and reflects tz-rules approx as of 2014")())
      .def(
        "calendar_units",
        &calendar::calendar_units,
        doc
          .intro(
            "returns YMDhms (.year,.month,.day,.hour,.minute..)  for specified t, in the time-zone as given "
            "by "
            "the calendar")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .returns("calendar_units", "YMDhms", "calendar units as in year-month-day hour-minute-second")(),
        py::arg("t"))
      .def(
        "calendar_week_units",
        &calendar::calendar_week_units,
        doc
          .intro(
            "returns iso YWdhms, with properties (.iso_year,.iso_week,.week_day,.hour,..)\n"
            "for specified t, in the time-zone as given by the calendar\n")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .returns("calendar_week_units", "YWdms", "calendar units as in iso year-week-week_day hour-minute-second")(),
        py::arg("t"))
      .def(
        "time",
        time_YMDhms,
        doc.intro("convert calendar coordinates into time using the calendar time-zone")
          .parameters()
          .parameter("YMDhms", "YMDhms", "calendar cooordinate structure containg year,month,day, hour,minute,second")
          .returns("timestamp", "int", "timestamp as in seconds utc since epoch")(),
        py::arg("YMDhms"))
      .def(
        "time",
        time_YWdhms,
        doc.intro("convert calendar iso week coordinates structure into time using the calendar time-zone")
          .parameters()
          .parameter("YWdhms", "YWdhms", "structure containg iso specification calendar coordinates")
          .returns("timestamp", "int", "timestamp as in seconds utc since epoch")(),
        py::arg("YWdhms"))

      .def(
        "time",
        time_6,
        doc.intro("convert calendar coordinates into time using the calendar time-zone")
          .parameters()
          .parameter("Y", "int", "Year")
          .parameter("M", "int", "Month  [1..12], default=1")
          .parameter("D", "int", "Day    [1..31], default=1")
          .parameter("h", "int", "hour   [0..23], default=0")
          .parameter("m", "int", "minute [0..59], default=0")
          .parameter("s", "int", "second [0..59], default=0")
          .parameter("us", "int", "micro second[0..999999], default=0")
          .returns("timestamp", "time", "timestamp as in seconds utc since epoch")(),
        py::arg("Y"),
        py::arg("M") = 1,
        py::arg("D") = 1,
        py::arg("h") = 0,
        py::arg("m") = 0,
        py::arg("s") = 0,
        py::arg("us") = 0)
      .def(
        "time_from_week",
        time_from_week_6,
        doc.intro("convert calendar iso week coordinates into time using the calendar time-zone")
          .parameters()
          .parameter("Y", "int", "ISO Year")
          .parameter("W", "int", "ISO Week  [1..54], default=1")
          .parameter("wd", "int", "week_day  [1..7]=[mo..su], default=1")
          .parameter("h", "int", "hour   [0..23], default=0")
          .parameter("m", "int", "minute [0..59], default=0")
          .parameter("s", "int", "second [0..59], default=0")
          .parameter("us", "int", "micro second[0..999999], default=0")
          .returns("timestamp", "time", "timestamp as in seconds utc since epoch")(),
        py::arg("Y"),
        py::arg("W") = 1,
        py::arg("wd") = 1,
        py::arg("h") = 0,
        py::arg("m") = 0,
        py::arg("s") = 0,
        py::arg("us") = 0)
      .def(
        "add",
        &calendar::add,
        doc.intro("This function does a calendar semantic add.\n")
          .intro("Conceptually this is similar to t + deltaT*n")
          .intro("but with deltaT equal to calendar::DAY,WEEK,MONTH,YEAR")
          .intro("and/or with dst enabled time-zone the variation of length due to dst")
          .intro("as well as month/year length is taken into account.")
          .intro("E.g. add one day, and calendar have dst, could give 23,24 or 25 hours due to dst.")
          .intro("Similar for week or any other time steps.")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .parameter("delta_t", "time", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .parameter("n", "int", "number of timesteps to add")
          .returns("t", "time", "new timestamp with the added time-steps, seconds utc since epoch")
          .notes()
          .note("ref. to related functions .diff_units(...) and .trim(..)")(),
        py::arg("t"),
        py::arg("delta_t"),
        py::arg("n"))
      .def(
        "diff_units",
        static_cast<std::int64_t (calendar::*)(utctime, utctime, utctimespan, trim_policy) const>(
          &calendar::diff_units),
        doc.intro("calculate the distance t1..t2 in specified units, taking dst into account if observed")
          .intro("The function takes calendar semantics when delta_t is calendar::DAY,WEEK,MONTH,YEAR,")
          .intro("and in addition also dst if observed.")
          .intro("e.g. the diff_units of calendar::DAY over summer->winter shift is 1,")
          .intro(
            "even if the number of hours during those days are 23 and 25 summer and winter transition "
            "respectively.")
          .intro(
            "It computes the calendar semantics of (t2-t1)/deltaT, where deltaT could be calendar units "
            "DAY,WEEK,MONTH,YEAR")
          .parameters()
          .parameter("t1", "time", "timestamp utc seconds since epoch")
          .parameter("t2", "time", "timestamp utc seconds since epoch")
          .parameter("delta_t", "time", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .parameter("trim_policy", "trim_policy", "Default TRIM_IN, could be TRIM_OUT or TRIM_ROUND.")
          .returns(
            "n_units",
            "int",
            "number of units, so that t2 = c.add(t1,delta_t,n) + remainder(discarded). "
            "Depending on the trim_policy, the remainder results will add/subtract one unit to the result.")
          .notes()
          .note("ref. to related functions .add(...) and .trim(...)")(),
        py::arg("t1"),
        py::arg("t2"),
        py::arg("delta_t"),
        py::arg("trim_policy") = trim_policy::TRIM_IN)
      .def(
        "trim",
        static_cast<utctime (calendar::*)(utctime, utctimespan) const>(&calendar::trim),
        doc.intro("Round time t down to the nearest calendar time-unit delta_t,")
          .intro("taking the calendar time-zone and dst into account.\n")
          .parameters()
          .parameter("t", "time", "timestamp utc seconds since epoch")
          .parameter(
            "delta_t", "time", "timestep in seconds, with semantic interpretation of Calendar.{DAY|WEEK|MONTH|YEAR}")
          .returns("t", "time", "new trimmed timestamp, seconds utc since epoch")
          .notes()
          .note("ref to related functions .add(t,delta_t,n),.diff_units(t1,t2,delta_t)")(),
        py::arg("t"),
        py::arg("delta_t"))
      .def(
        "quarter",
        &calendar::quarter,
        doc.intro("returns the quarter of the specified t, -1 if invalid t")
          .parameters()
          .parameter("t", "int", "timestamp utc seconds since epoch")
          .returns("quarter", "int", "in range[1..4], -1 if invalid time")(),
        py::arg("t"))
      .def_property_readonly_static(
        "YEAR",
        [](py::object const &) {
          return calendar::YEAR;
        },
        "time: defines a semantic year")
      .def_property_readonly_static(
        "MONTH",
        [](py::object const &) {
          return calendar::MONTH;
        },
        "time: defines a semantic calendar month")
      .def_property_readonly_static(
        "QUARTER",
        [](py::object const &) {
          return calendar::QUARTER;
        },
        "time: defines a semantic calendar quarter (3 months)")
      .def_property_readonly_static(
        "DAY",
        [](py::object const &) {
          return calendar::DAY;
        },
        "time: defines a semantic calendar day")
      .def_property_readonly_static(
        "WEEK",
        [](py::object const &) {
          return calendar::WEEK;
        },
        "time: defines a semantic calendar week")
      .def_property_readonly_static(
        "HOUR",
        [](py::object const &) {
          return calendar::HOUR;
        },
        "time: hour, 3600 seconds")
      .def_property_readonly_static(
        "MINUTE",
        [](py::object const &) {
          return calendar::MINUTE;
        },
        "time: minute, 60 seconds")
      .def_property_readonly_static(
        "SECOND",
        [](py::object const &) {
          return calendar::SECOND;
        },
        "time: second, 1 second")
      .def_property_readonly(
        "tz_info",
        &calendar::get_tz_info,
        "TzInfo: keeping the time-zone name, utc-offset and DST rules (if any)",
        py::return_value_policy::reference_internal)
      .def(
        "day_of_year",
        &calendar::day_of_year,
        doc.intro("returns the day of year for the specified time")
          .parameters()
          .parameter("t", "time", "time to use for computation")
          .returns("day_of_year", "int", "in range 1..366")(),
        py::arg("t"))
      .def_property_readonly_static(
        "TZ_RANGE",
        [](py::object const &) {
          return calendar::tz_range;
        },
        "UtcPeriod: Working range of DST and time-zone rules")
      .def_property_readonly_static(
        "RANGE",
        [](py::object const &) {
          return calendar::range;
        },
        "UtcPeriod: Working range of calendar arithmetic, to avoid overflow, the validity is furhter limited by "
        "gregorian calendar")
      .def(py::self == py::self)
      .def(py::self != py::self);
  }

  static py::class_<utcperiod> pyexport_fwd_utcperiod(py::module_ &m) {
    return py::class_<utcperiod>(
             m,
             "UtcPeriod",
             doc.intro("UtcPeriod defines the open utc-time range [start..end>")
               .intro("where end is required to be equal or greater than start")())
      .def(py::init([]() {
        return utcperiod{};
      }));
  }

  static void pyexport_utcperiod(py::module_ &m, py::class_<utcperiod> &c) {
    bool (utcperiod::*contains_t)(utctime) const = &utcperiod::contains;
    bool (utcperiod::*contains_i)(std::int64_t) const = &utcperiod::contains;
    bool (utcperiod::*contains_p)(utcperiod const &) const = &utcperiod::contains;
    utcperiod (utcperiod::*trim_i)(calendar const &, std::int64_t, trim_policy) const = &utcperiod::trim;
    utcperiod (utcperiod::*trim_t)(calendar const &, utctimespan, trim_policy) const = &utcperiod::trim;
    std::int64_t (utcperiod::*diff_units_t)(calendar const &, utctimespan) const = &utcperiod::diff_units;
    std::int64_t (utcperiod::*diff_units_i)(calendar const &, std::int64_t) const = &utcperiod::diff_units;
    c.def(
       py::init([](utctime t, utctime u) {
         return utcperiod{t, u};
       }),
       "Create utcperiod given start and end",
       py::arg("start"),
       py::arg("end"))
      .def(
        py::init([](std::int64_t b, std::int64_t e) {
          return utcperiod{
            utctime_from_seconds64(utctime_range_check(b)), utctime_from_seconds64(utctime_range_check(e))};
        }),
        "Create utcperiod given start and end",
        py::arg("start"),
        py::arg("end"))
      .def(
        py::init([](double b, double e) {
          return utcperiod{from_seconds(utctime_range_check(b)), from_seconds(utctime_range_check(e))};
        }),
        "Create utcperiod given start and end",
        py::arg("start"),
        py::arg("end"))
      .def("valid", &utcperiod::valid, "returns true if start<=end otherwise false")
      .def("contains", contains_t, "returns true if time t is contained in this utcperiod", py::arg("t"))
      .def("contains", contains_i, "returns true if time t is contained in this utcperiod", py::arg("t"))
      .def("contains", contains_p, "returns true if utcperiod p is contained in this utcperiod", py::arg("p"))
      .def("overlaps", &utcperiod::overlaps, "returns true if period p overlaps this utcperiod", py::arg("p"))
      .def(
        "trim",
        trim_t,
        doc.intro("Round UtcPeriod up or down to the nearest calendar time-unit delta_t")
          .intro("taking the calendar time-zone and dst into account")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter(
            "delta_t", "time", "timestep in seconds, with semantic interpretation of Calendar.(DAY,WEEK,MONTH,YEAR)")
          .parameter("trim_policy", "trim_policy", "TRIM_IN if rounding period inwards, else rounding outwards")
          .returns("trimmed_UtcPeriod", "UtcPeriod", "new trimmed UtcPeriod")(),
        py::arg("calendar"),
        py::arg("delta_t"),
        py::arg("trim_policy") = core::trim_policy::TRIM_IN)
      .def(
        "trim",
        trim_i,
        doc.intro("Round UtcPeriod up or down to the nearest calendar time-unit delta_t")
          .intro("taking the calendar time-zone and dst into account")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter(
            "delta_t", "int", "timestep in seconds, with semantic interpretation of Calendar.(DAY,WEEK,MONTH,YEAR)")
          .parameter("trim_policy", "trim_policy", "TRIM_IN if rounding period inwards, else rounding outwards")
          .returns("trimmed_UtcPeriod", "UtcPeriod", "new trimmed UtcPeriod")(),
        py::arg("calendar"),
        py::arg("delta_t"),
        py::arg("trim_policy") = core::trim_policy::TRIM_IN)
      .def(
        "diff_units",
        diff_units_t,
        doc
          .intro(
            "Calculate the distance from start to end of UtcPeriod in specified units, taking dst into "
            "account "
            "if "
            "observed")
          .intro("The function takes calendar semantics when delta_t is calendar::DAY,WEEK,MONTH,YEAR,")
          .intro("and in addition also dst if observed.")
          .intro("e.g. the diff_units of calendar::DAY over summer->winter shift is 1,")
          .intro(
            "even if the number of hours during those days are 23 and 25 summer and winter transition "
            "respectively")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter("delta_t", "time", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .returns("n_units", "int", "number of units in UtcPeriod")(),
        py::arg("calendar"),
        py::arg("delta_t"))
      .def(
        "diff_units",
        diff_units_i,
        doc
          .intro(
            "Calculate the distance from start to end of UtcPeriod in specified units, taking dst into "
            "account "
            "if "
            "observed")
          .intro("The function takes calendar semantics when delta_t is calendar::DAY,WEEK,MONTH,YEAR,")
          .intro("and in addition also dst if observed.")
          .intro("e.g. the diff_units of calendar::DAY over summer->winter shift is 1,")
          .intro(
            "even if the number of hours during those days are 23 and 25 summer and winter transition "
            "respectively")
          .parameters()
          .parameter("calendar", "calendar", "shyft calendar")
          .parameter("delta_t", "int", "timestep in seconds, with semantic interpretation of DAY,WEEK,MONTH,YEAR")
          .returns("n_units", "int", "number of units in UtcPeriod")(),
        py::arg("calendar"),
        py::arg("delta_t"))
      .def("__str__", &utcperiod::to_string, "returns the str using time-zone utc to convert to readable time")
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def("__hash__", utcperiod_hasher{})
      .def("timespan", &utcperiod::timespan, "returns end-start, the timespan of the period")
      .def_readwrite("start", &utcperiod::start, "time: Defines the start of the period, inclusive")
      .def_readwrite("end", &utcperiod::end, "time: Defines the end of the period, not inclusive")
      .def(
        "intersection",
        +[](utcperiod const &a, utcperiod const &b) {
          return intersection(a, b);
        },
        doc.intro("Returns the intersection of two periods")
          .intro("if there is an intersection, the resulting period will be .valid() and .timespan()>0")
          .intro("If there is no intersection, an empty not .valid() period is returned")
          .parameters()
          .parameter("self", "UtcPeriod", "1st UtcPeriod argument")
          .parameter("rhs", "UtcPeriod", "2nd UtcPeriod argument")
          .returns("intersection", "UtcPeriod", "The computed intersection, or an empty not .valid() UtcPeriod")(),
        py::arg("rhs"))
      .def("__str__", &utcperiod::to_string, "the str representation")
      .def("__repr__", &utcperiod::to_string, "the str representation")
      .def("to_string", &utcperiod::to_string, "A readable representation in UTC")
      .def(
        py::pickle(
          [](utcperiod const &p) {
            return py::make_tuple(p.start.count(), p.end.count());
          },
          [](py::tuple const &p) {
            return utcperiod(utctime(p[0].cast<std::int64_t>()), utctime(p[1].cast<std::int64_t>()));
          }));
    m.def(
      "intersection",
      &intersection,
      doc.intro("Returns the intersection of two periods")
        .intro("if there is an intersection, the resulting period will be .valid() and .timespan()>0")
        .intro("If there is no intersection, an empty not .valid() period is returned")
        .parameters()
        .parameter("a", "UtcPeriod", "1st UtcPeriod argument")
        .parameter("b", "UtcPeriod", "2nd UtcPeriod argument")
        .returns("intersection", "UtcPeriod", "The computed intersection, or an empty not .valid() UtcPeriod")(),
      py::arg("a"),
      py::arg("b"));
  }

  void pyexport_calendar_and_time(py::module_ &m) {
    pyexport_utctime(m);
    auto utc_period_fwd = pyexport_fwd_utcperiod(m);
    pyexport_calendar(m);
    pyexport_utcperiod(m, utc_period_fwd);
  }
}
