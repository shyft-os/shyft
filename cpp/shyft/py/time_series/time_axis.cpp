/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/bindings.h>
#include <shyft/py/containers.h>
#include <shyft/py/doc_builder.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_axis {

  generic_dt time_axis_merge(generic_dt const &a, generic_dt const &b) {
    if (a.size() == 0)
      return b;
    if (b.size() == 0)
      return a;
    auto ap = a.total_period();
    auto bp = b.total_period();
    if (ap.end < bp.start || ap.start > bp.end) { // a gap, that we need to fix with a temporary axis w. 1 step between
      utcperiod gp = ap.end < bp.start ? utcperiod(ap.end, bp.start) : utcperiod(bp.end, ap.start);
      generic_dt g{gp.start, gp.timespan(), 1};
      return merge(merge(a, g), b);
    }
    if (a == b) // easy& efficient compared to alternative: just return the one
      return a;
    if (ap.end == bp.start || ap.start == bp.end) { // exact joint
      return merge(a, b);
    }
    // ok some overlapping,
    // we use a.merge(combine(a,b)).merge(b)
    auto c = combine(a, b); // c combined overlap-zone
    auto a_c = merge(c, a); // a || c
    return merge(a_c, b);   // a||c||b
  }

  using core::utctime;
  using core::utcperiod;

  template <typename T>
  struct time_axis_iter {
    T *time_axis;
    std::size_t i = 0, n = time_axis->size();

    auto operator*() const {
      return time_axis->period(i);
    }

    auto &operator++() {
      ++i;
      return *this;
    }

    auto operator<=>(time_axis_iter const &) const = default;

    friend bool operator==(time_axis_iter const &i, std::default_sentinel_t) {
      return i.i == i.n;
    }
  };

  std::vector<utctime> to_vector_of_utctime(py::array_t<double> const &time_points) {
    std::vector<utctime> t;
    t.reserve(time_points.size());
    auto const info = time_points.request();
    if (info.ndim != 1)
      throw std::runtime_error("time_points must be a 1d array");
    auto const *b = static_cast<double const *>(info.ptr);
    std::transform(b, b + info.size, std::back_inserter(t), from_seconds);
    return t;
  }

  template <typename T, typename... O>
  auto expose_time_axis(py::class_<T, O...> c) {
    c.def(
       "total_period",
       &T::total_period,
       doc.returns("total_period", "UtcPeriod", "the period that covers the entire time-axis")())
      .def("size", &T::size, doc.returns("n", "int", "number of periods in time-axis")())
      .def("__len__", &T::size, doc.intro("Returns the number of periods in the time-axis")())
      .def(
        "time",
        &T::time,
        doc.parameters()
          .parameter("i", "int", "the i'th period, 0..n-1")
          .returns("utctime", "int", "the start(utctime) of the i'th period of the time-axis")(),
        py::arg("i"))
      .def(
        "period",
        &T::period,
        doc.parameters()
          .parameter("i", "int", "the i'th period, 0..n-1")
          .returns("period", "UtcPeriod", "the i'th period of the time-axis")(),
        py::arg("i"))
      .def(
        "__call__",
        &T::period,
        doc.parameters()
          .parameter("i", "int", "the i'th period, 0..n-1")
          .returns("period", "UtcPeriod", "the i'th period of the time-axis")(),
        py::arg("i"))
      .def(
        "index_of",
        [](T const &ta, utctime t, std::size_t i) {
          if constexpr (requires { ta.index_of(t, i); })
            return ta.index_of(t, i);
          else
            return ta.index_of(t);
        },
        doc.parameters()
          .parameter("t", "utcime", "utctime in seconds 1970.01.01")
          .returns("index", "int", "the index of the time-axis period that contains t, npos if outside range")(),
        py::arg("t"),
        py::arg("ix_hint") = std::string::npos)
      .def(
        "open_range_index_of",
        [](T const &ta, utctime t, std::size_t ix_hint) {
          return ta.open_range_index_of(t, ix_hint);
        },
        doc.intro("returns the index that contains t, or is before t")
          .parameters()
          .parameter("t", "int", "utctime in seconds 1970.01.01")
          .parameter("ix_hint", "int", "index-hint to make search in point-time-axis faster")
          .returns(
            "index",
            "int",
            "the index the time-axis period that contains t, npos if before first period n-1, if t is after last "
            "period")(),
        py::arg("t"),
        py::arg("ix_hint") = std::string::npos)
      .def(
        "slice",
        &T::slice,
        doc.intro("returns slice of time-axis as a new time-axis")
          .parameters()
          .parameter("start", "int", "first interval to include")
          .parameter("n", "int", "number of intervals to include")
          .returns("time-axis", "TimeAxis", "A new time-axis with the specified slice")(),
        py::arg("start"),
        py::arg("n"))
      .def(
        "empty", &T::empty, doc.intro("true if empty time-axis").returns("empty", "bool", "true if empty time-axis")())
      .def(py::self == py::self)
      .def(py::self != py::self)
      .def("__iter__", [](T const &t) {
        return py::make_iterator(time_axis_iter{&t}, std::default_sentinel);
      });
    ;
    pyapi::expose_format(c);
    return c;
  }

  static void pyexport_fixed_dt(py::module_ &m) {
    auto f_dt =
      py::class_<fixed_dt>(
        m,
        "TimeAxisFixedDeltaT",
        doc.intro("A time-axis is a set of ordered non-overlapping periods,")
          .intro("and this class implements a fixed delta-t time-axis by")
          .intro("specifying the minimal t-start, delta-t and number of consecutive periods.")
          .intro("This class is a wrapper of the Shyft core-library high performance time-axis")
          .see_also("TimeAxisCalendarDeltaT,TimeAxisByPoints,TimeAxis")())
        .def(
          py::init<utctime, utctimespan, std::int64_t>(),
          doc.intro("creates a time-axis with n intervals, fixed delta_t, starting at start")
            .parameters()
            .parameter("start", "int", "utc-time 1970 utc based")
            .parameter("delta_t", "int", "number of seconds delta-t, length of periods in the time-axis")
            .parameter("n", "int", "number of periods in the time-axis")(),
          py::arg("start"),
          py::arg("delta_t"),
          py::arg("n"))
        .def_readonly("n", &fixed_dt::n, "int: number of periods")
        .def_readonly("start", &fixed_dt::t, "time: start of the time-axis, in seconds since 1970.01.01 UTC")
        .def_readonly("delta_t", &fixed_dt::dt, "time: time-span of each interval in seconds")
        .def_static("full_range", &fixed_dt::full_range, "returns a timeaxis that covers [-oo..+oo> ")
        .def_static("null_range", &fixed_dt::null_range, "returns a null timeaxis");
    ;
    expose_time_axis(f_dt);
  }

  static void pyexport_calendar_dt(py::module_ &m) {
    auto c_dt =
      py::class_<calendar_dt>(
        m,
        "TimeAxisCalendarDeltaT",
        doc.intro("A time-axis is a set of ordered non-overlapping periods,")
          .intro("and this class implements a calendar-unit fixed delta-t time-axis by")
          .intro("specifying the minimal calendar, t-start, delta-t and number of consecutive periods.")
          .intro("This class is particularly useful if you need to work with calendar and daylight-saving time,")
          .intro("or calendar-specific periods like day,week,month,quarters or years.")
          .intro("\n")
          .notes()
          .note("For performance reasons, dt < DAY results in a TimeAxisFixedDeltaT.")
          .see_also("TimeAxisFixedDeltaT,TimeAxisByPoints,TimeAxis")())
        .def(
          py::init<std::shared_ptr<calendar> const &, utctime, utctimespan, std::int64_t>(),
          doc.intro("creates a calendar time-axis")
            .parameters()
            .parameter(
              "calendar", "Calendar", "specifies the calendar to be used, keeps the time-zone and dst-arithmetic rules")
            .parameter("start", "int", "utc-time 1970 utc based")
            .parameter(
              "delta_t",
              "int",
              "number of seconds delta-t, length of periods in the time-axis."
              "For performance reasons, dt < DAY results in a TimeAxisFixedDeltaT.")
            .parameter("n", "int", "number of periods in the time-axis")(),
          py::arg("calendar"),
          py::arg("start"),
          py::arg("delta_t"),
          py::arg("n"))
        .def_readonly("n", &calendar_dt::n, "int: number of periods")
        .def_readonly("start", &calendar_dt::t, "time: start of the time-axis, in seconds since 1970.01.01 UTC")
        .def_readonly(
          "delta_t",
          &calendar_dt::dt,
          "time: timespan of each interval,use Calendar.DAY|.WEEK|.MONTH|.QUARTER|.YEAR, or seconds")
        .def_property_readonly("calendar", &calendar_dt::get_calendar, "Calendar: the calendar of the time-axis");
    expose_time_axis(c_dt);
  }

  static void pyexport_point_dt(py::module_ &m) {
    auto p_dt =
      py::class_<point_dt>(
        m,
        "TimeAxisByPoints",
        doc.intro("A time-axis is a set of ordered non-overlapping periods,")
          .intro("and this class implements this by a set of ")
          .intro("ordered unique time-points. This is the most flexible time-axis representation,")
          .intro("that allows every period in the time-axis to have different length.")
          .intro("It comes at the cost of space&performance in certain cases, so ")
          .intro("avoid use in scenarios where high-performance is important.")
          .see_also("TimeAxisCalendarDeltaT,TimeAxisFixedDeltaT,TimeAxis")())
        .def(
          py::init< std::vector<utctime> const &, utctime>(),
          doc.intro("creates a time-axis by specifying the time_points and t-end of the last interval")
            .parameters()
            .parameter(
              "time_points",
              "UtcTimeVector",
              "ordered set of unique utc-time points, the start of each consecutive period")
            .parameter(
              "t_end",
              "int",
              "the end of the last period in time-axis, utc-time 1970 utc based, must be > time_points[-1]")(),
          py::arg("time_points"),
          py::arg("t_end"))
        .def(
          py::init< std::vector<utctime> const & >(),
          doc.intro("create a time-axis supplying n+1 points to define n intervals")
            .parameters()
            .parameter(
              "time_points",
              "UtcTimeVector",
              "ordered set of unique utc-time points, 0..n-2:the start of each consecutive period,n-1: end of last "
              "period")(),
          py::arg("time_points"))
        .def_readonly("t", &point_dt::t, "UtcTimeVector: time_points except end of last period, see t_end")
        .def_readonly("t_end", &point_dt::t_end, "time: end of time-axis");
    expose_time_axis(p_dt);
  }

  static auto time_axis_extract_time_points(generic_dt const &ta) {
    std::vector<std::int64_t> v;

    v.reserve(ta.size() + 1);
    for (size_t i = 0; i < ta.size(); ++i) {
      v.emplace_back(to_seconds64(ta.time(i)));
    }
    if (ta.size())
      v.emplace_back(to_seconds64(ta.total_period().end));

    return pyapi::make_array(std::move(v));
  }

  static auto time_axis_extract_time_points_double(generic_dt const &ta) {
    std::vector<double> v;

    v.reserve(ta.size() + 1);
    for (size_t i = 0; i < ta.size(); ++i) {
      v.emplace_back(to_seconds(ta.time(i)));
    }
    if (ta.size())
      v.emplace_back(to_seconds(ta.total_period().end));

    return pyapi::make_array(std::move(v));
  }

  static std::vector<utctime> time_axis_extract_time_points_as_utctime(generic_dt const &ta) {
    std::vector<utctime> r;
    r.reserve(ta.size() + 1);
    for (size_t i = 0; i < ta.size(); ++i) {
      r.emplace_back(ta.time(i));
    }
    if (ta.size())
      r.emplace_back(ta.total_period().end);
    return r;
  }

  static std::vector<utctime> time_axis_extract_time_points_as_utctime_tz(generic_dt const &ta, calendar const &c) {
    std::vector<utctime> r;
    r.reserve(ta.size() + 1);
    auto tz = c.get_tz_info();
    for (size_t i = 0; i < ta.size(); ++i) {
      auto t = ta.time(i);
      r.emplace_back(t + tz->utc_offset(t));
    }
    if (ta.size())
      r.emplace_back(ta.total_period().end + tz->utc_offset(ta.total_period().end));
    return r;
  }

  static void pyexport_generic_dt(py::module_ &m) {
    py::enum_<generic_dt::generic_type>(
      m, "TimeAxisType", doc.intro("enumeration for the internal time-axis representations")())
      .value("FIXED", generic_dt::generic_type::FIXED)
      .value("CALENDAR", generic_dt::generic_type::CALENDAR)
      .value("POINT", generic_dt::generic_type::POINT);

    auto g_dt =
      py::class_<generic_dt>(
        m,
        "TimeAxis",
        doc.intro("A time-axis is a set of ordered non-overlapping periods,")
          .intro("and TimeAxis provides the most generic implementation of this.")
          .intro("The internal representation is selected based on provided parameters")
          .intro("to the constructor.")
          .intro("The internal representation is one of TimeAxis FixedDeltaT CalendarDelataT or ByPoints.")
          .intro("The internal representation type and corresponding realizations are available as properties.")
          .notes()
          .note(
            "The internal representation can be one of "
            "TimeAxisCalendarDeltaT,TimeAxisFixedDeltaT,TimeAxisByPoints")())
        .def(py::init())
        .def(
          py::init< calendar_dt const &>(),
          doc.intro("create a time-axis from a calendar time-axis")
            .parameters()
            .parameter("calendar_dt", "TimeAxisCalendarDeltaT", "existing calendar time-axis")(),
          py::arg("calendar_dt"))
        .def(
          py::init< fixed_dt const &>(),
          doc.intro("create a time-axis from a a fixed delta-t time-axis")
            .parameters()
            .parameter("fixed_dt", "TimeAxisFixedDeltaT", "existing fixed delta-t time-axis")(),
          py::arg("fixed_dt"))
        .def(
          py::init< point_dt const &>(),
          doc.intro("create a time-axis from a a by points  time-axis")
            .parameters()
            .parameter("point_dt", "TimeAxisByPoints", "existing by points time-axis")(),
          py::arg("point_dt"))
        .def(
          py::init<utctime, utctimespan, std::int64_t>(),
          doc.intro("creates a time-axis with n intervals, fixed delta_t, starting at start")
            .parameters()
            .parameter("start", "utctime", "utc-time 1970 utc based")
            .parameter("delta_t", "utctime", "number of seconds delta-t, length of periods in the time-axis")
            .parameter("n", "int", "number of periods in the time-axis")(),
          py::arg("start"),
          py::arg("delta_t"),
          py::arg("n"))
        .def(
          py::init<std::shared_ptr<calendar>, utctime, utctimespan, std::int64_t>(),
          doc.intro("creates a calendar time-axis")
            .parameters()
            .parameter(
              "calendar", "Calendar", "specifies the calendar to be used, keeps the time-zone and dst-arithmetic rules")
            .parameter("start", "utctime", "utc-time 1970 utc based")
            .parameter("delta_t", "utctime", "number of seconds delta-t, length of periods in the time-axis")
            .parameter("n", "int", "number of periods in the time-axis")(),
          py::arg("calendar"),
          py::arg("start"),
          py::arg("delta_t"),
          py::arg("n"))
        .def(
          py::init([](py::array_t<double, py::array::forcecast | py::array::c_style> time_points, utctime t_end) {
            return generic_dt{to_vector_of_utctime(time_points), t_end};
          }),
          py::arg("time_points"),
          py::arg("t_end"),
          doc.intro("creates a time-axis by specifying the time_points and t-end of the last interval")
            .parameters()
            .parameter(
              "time_points",
              "numpy.array[float|int]",
              "ordered set of unique utc-time points, the start of each consecutive period")
            .parameter(
              "t_end",
              "time",
              "the end of the last period in time-axis, utc-time 1970 utc based, must be > time_points[-1]")())
        .def(
          py::init< std::vector<utctime> const &, utctime>(),
          doc.intro("creates a time-axis by specifying the time_points and t-end of the last interval")
            .parameters()
            .parameter(
              "time_points",
              "UtcTimeVector",
              "ordered set of unique utc-time points, the start of each consecutive period")
            .parameter(
              "t_end",
              "time",
              "the end of the last period in time-axis, utc-time 1970 utc based, must be > time_points[-1]")(),
          py::arg("time_points"),
          py::arg("t_end"))
        .def(
          py::init([](py::array_t<double, py::array::forcecast | py::array::c_style> time_points) {
            return generic_dt{to_vector_of_utctime(time_points)};
          }),
          py::arg("time_points"),
          doc.intro("create a time-axis supplying n+1 points to define n intervals")
            .parameters()
            .parameter(
              "time_points",
              "numpy.array[float|int]",
              "ordered set of unique utc-time points, 0..n-2:the start of each consecutive period,n-1: end of last")())
        .def(
          py::init< std::vector<utctime> const & >(),
          doc.intro("create a time-axis supplying n+1 points to define n intervals")
            .parameters()
            .parameter(
              "time_points",
              "UtcTimeVector",
              "ordered set of unique utc-time points, 0..n-2:the start of each consecutive period,n-1: end of last "
              "period")(),
          py::arg("time_points"))
        .def_property_readonly(
          "timeaxis_type",
          &generic_dt::gt,
          "TimeAxisType: describes what time-axis representation type this is,e.g (fixed|calendar|point)_dt ")
        .def_property_readonly(
          "fixed_dt",
          +[](generic_dt const &me) {
            return me.f();
          },
          "TimeAxisFixedDeltaT: The fixed dt representation (if active)")
        .def_property_readonly(
          "calendar_dt",
          +[](generic_dt const &me) {
            return me.c();
          },
          "TimeAxisCalendarDeltaT: The calendar dt representation(if active)")
        .def_property_readonly(
          "point_dt",
          +[](generic_dt const &me) {
            return me.p();
          },
          "TimeAxisByPoints:  point_dt representation(if active)")
        .def(
          "merge",
          &time_axis_merge,
          doc.intro("Returns a new time-axis that contains the union of time-points/periods of the two time-axis.")
            .intro("If there is a gap between, it is filled")
            .intro("merge with empty time-axis results into the other time-axis")
            .parameters()
            .parameter("other", "TimeAxis", "The other time-axis to merge with")
            .returns("merge_result", "TimeAxis", "the resulting merged time-axis")(),
          py::arg("other"))
        .def_property_readonly("time_points_double", &time_axis_extract_time_points_double)
        .def_property_readonly("time_points", &time_axis_extract_time_points)
        .def("__hash__", [](generic_dt &g) {
          return reinterpret_cast<std::intptr_t>(&g);
        });

    expose_time_axis(g_dt);

    m.def(
      "time_axis_extract_time_points",
      time_axis_extract_time_points,
      doc.intro("Extract all time_axis.period(i).start plus time_axis.total_period().end into a UtcTimeVector")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis to extract all time-points from")
        .returns("time_points", "list[int]", "all time_axis.period(i).start plus time_axis.total_period().end")(),
      py::arg("time_axis"));
    m.def(
      "time_axis_extract_time_points_as_utctime",
      time_axis_extract_time_points_as_utctime,
      doc.intro("Extract all time_axis.period(i).start plus time_axis.total_period().end into a UtcTimeVector")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis to extract all time-points from")
        .returns("time_points", "UtcTimeVector", "all time_axis.period(i).start plus time_axis.total_period().end")(),
      py::arg("time_axis"));
    m.def(
      "time_axis_extract_time_points_as_utctime_tz",
      time_axis_extract_time_points_as_utctime_tz,
      doc.intro("Extract all time_axis.period(i).start plus time_axis.total_period().end into a UtcTimeVector")
        .intro(" - adding the calendar.tz_info.utc_offset(t) for all t in UtcTimeVector ")
        .intro("Effectively you get out 'tz' version of the time-points, that might not be strictly ascending")
        .intro("Intended usage is within the shyft.dashboard time-series rendering where we currently ")
        .intro("need to pass tz-version of the time for rendering due to lack of proper view-handling of")
        .intro("time-zones in bokeh")
        .intro("NOT recommended for other usage!")
        .parameters()
        .parameter("time_axis", "TimeAxis", "time-axis to extract all time-points from")
        .parameter("calendar", "Calendar", "calendar with tz-info to use for adding tz_info.utc_offset(t)")
        .returns("time_points", "UtcTimeVector", "all time_axis.period(i).start plus time_axis.total_period().end")(),
      py::arg("time_axis"),
      py::arg("calendar"));
  }

  void pyexport(py::module_ &m) {
    pyexport_fixed_dt(m);
    pyexport_point_dt(m);
    pyexport_calendar_dt(m);
    pyexport_generic_dt(m);
  }

}
