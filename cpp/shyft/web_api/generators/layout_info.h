#pragma once
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/web_api/generators/json_emit.h>

namespace shyft::web_api::generator {

  template <class OutputIterator>
  struct emit<OutputIterator, shyft::energy_market::ui::layout_info> {
    emit(OutputIterator oi, shyft::energy_market::ui::layout_info const & o) {
      emit_object<OutputIterator> oo(oi);
      oo.def("id", o.id).def("name", o.name).def("json", o.json);
    }
  };

  x_emit_vec(shyft::energy_market::ui::layout_info);
}