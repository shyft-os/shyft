/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {
  template <class OutputIterator>
  geo_point_generator<OutputIterator>::geo_point_generator()
    : geo_point_generator::base_type(pg) {
    using ka::_val;
    using ka::_1;
    pg = "{\"x\":" << d_[_1 = phx::bind(&geo_point::x, _val)] << ",\"y\":" << d_[_1 = phx::bind(&geo_point::y, _val)]
                   << ",\"z\":" << d_[_1 = phx::bind(&geo_point::z, _val)] << '}'

      ;
    pg.name("point");
  }

  template struct geo_point_generator<generator_output_iterator>;
}
