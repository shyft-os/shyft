/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {
  template <class OutputIterator>
  geo_ts_url_generator<OutputIterator>::geo_ts_url_generator(std::string prefix)
    : geo_ts_url_generator::base_type(pg)
    , prefix{std::move(prefix)} {
    using ka::int_;
    using ka::long_;
    pg = ka::lit(this->prefix)
      << ka::string[ka::_1 = phx::bind(&geo_ts_id::geo_db, ka::_val)] << ka::lit("/")
      << int_[ka::_1 = phx::bind(&geo_ts_id::v, ka::_val)] << ka::lit("/")
      << int_[ka::_1 = phx::bind(&geo_ts_id::g, ka::_val)] << ka::lit("/")
      << int_[ka::_1 = phx::bind(&geo_ts_id::e, ka::_val)] << ka::lit("/")
      << long_[ka::_1 = phx::bind(&geo_ts_id::secs, ka::_val)];
    pg.name("geo_ts_url");
  }

  template struct geo_ts_url_generator<generator_output_iterator>;
}
