#include <shyft/energy_market/ui/server.h>
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>
#include <shyft/web_api/generators/layout_info.h>
#include <shyft/web_api/generators/proxy_attr.h>
#include <shyft/web_api/ui/request_handler.h>

namespace shyft::web_api::ui {
  using shyft::core::utctime_now;
  using shyft::energy_market::ui::layout_info;
  using shyft::web_api::grammar::phrase_parser;
  using shyft::web_api::grammar::request_grammar;
  using shyft::web_api::generator::emit;
  using shyft::web_api::generator::emit_object;
  using shyft::web_api::generator::emit_vector_fx;
  using shyft::srv::model_info;

  bg_work_result request_handler::handle_request(request const &req, string const &input) {
    if (req.keyword == "get_layouts") {
      return handle_get_layouts_request(req.request_data);
    } else if (req.keyword == "read_layout") {
      return handle_read_layout_request(req.request_data);
    } else if (req.keyword == "store_layout") {
      return handle_store_layout_request(req.request_data, input);
    }
    return bg_work_result("Unknown keyword '" + req.keyword + "'");
  }

  bg_work_result request_handler::do_the_work(string const &input) {
    // 1. Parse into a request
    // 2. Error handling: On success, do a switch on keyword
    // 3. Handle every case and emit a response string

    // 1. Parse the request
    request arequest;
    request_grammar<char const *> request_;
    bool ok_parse = false;
    bg_work_result response;
    try {
      ok_parse = phrase_parser(input, request_, arequest);
      if (ok_parse) {
        response = handle_request(arequest, input);
      } else {
        response = bg_work_result{"not understood: " + input};
      }
    } catch (std::runtime_error const &re) {
      response = bg_work_result{string("request_parse: ") + re.what()};
    }
    return response;
  }

  bg_work_result request_handler::do_subscription_work(observer_base_ const &o) {
    // we want subscription on proxy-type expressions
    // so ts_expression_observer_ , url, might look different for the in-memory em server
    if (o->recalculate()) {
      // What to emit if a change is observed:
      return bg_work_result{};
    } else {
      return bg_work_result{};
    }
  }

  std::string_view request_handler::extract_json_part(std::string_view input) {

    std::string const json_key_word = "\"json\"";
    auto json_start = input.find(json_key_word);
    if (json_start == std::string::npos)
      throw std::runtime_error("store_layout: failed to find `json` key field");
    auto json_end = input.rfind("}");
    if (json_end == std::string::npos || json_end < json_start)
      throw std::runtime_error("store_layout: failed to find `}` at end of json request");
    // then we want to find the `:`, skipping any whitespace char.
    json_start += json_key_word.size();
    while (std::isspace(input[json_start]) && json_start < json_end)
      ++json_start;
    if (input[json_start] != ':')
      throw std::runtime_error("Failed to locate `:` after `json` keyword");
    ++json_start; // skip spaces after :
    while (std::isspace(input[json_start]) && json_start < json_end)
      ++json_start;
    return input.substr(json_start, json_end - json_start);
  }

  bg_work_result request_handler::handle_store_layout_request(json const &data, string const &input) const {
    auto req_id = data.required<string>("request_id");
    auto mid = data.required<std::int64_t>("layout_id");
    auto name = boost::get_optional_value_or(data.optional<string>("name"), "");
    auto json_obj = data.required<json>("json"); // required!
    // we need to preserve the json part, so by def. it is validated due to the parsing
    // but we want to store it exactly as received.
    // so we try to locate the `"json" :`  and the terminating `}`
    // using the assumption that json is the last 'key' in the
    // passed storage clause
    std::string json_str{extract_json_part(input)};
    model_info mi(mid, name, utctime_now(), json_str);
    auto li = std::make_shared<layout_info>(mid, name, json_str);

    auto stored_mid = srv->db.store_model(li, mi);
    string response = R"_({"request_id":")_" + req_id + R"_(","result":)_" + (stored_mid != 0 ? "true" : "false") + "}";

    return bg_work_result{response};
  }

  bg_work_result request_handler::handle_get_layouts_request(json const &data) const {
    // Communicate with server:
    auto model_infos = srv->db.get_model_infos({});

    // Prepare response
    auto req_id = data.required<string>("request_id");
    string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
    auto sink = std::back_inserter(response);
    emit_vector_fx(sink, model_infos, [](auto oi, auto li) {
      emit_object<decltype(oi)> oo(oi);
      oo.def("layout_id", li.id).def("name", li.name);
    });
    response += "}";
    return bg_work_result{response};
  }

  bg_work_result request_handler::handle_read_layout_request(json const &data) const {
    // Required data:
    auto req_id = data.required<string>("request_id");
    auto lid = data.required<std::int64_t>("layout_id");

    // Optional data:
    auto name = boost::get_optional_value_or(data.optional<string>("name"), "");
    auto args = boost::get_optional_value_or(data.optional<json>("args"), json{});
    auto store_layout = boost::get_optional_value_or(data.optional<bool>("store"), false);
    // Generate string from json args:
    string jargs;
    {
      auto sink = std::back_inserter(jargs);
      emit(sink, args);
    }
    // Read, or generate layout:
    auto li = srv->db.read_model_with_args(lid, name, jargs, store_layout);

    // Create response:
    string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
    {
      auto sink = std::back_inserter(response);
      emit_object<decltype(sink)> oo(sink);
      oo.def("layout_id", li->id).def("name", li->name);

      if (li->json.size() == 0) {
        oo.def_fx("layout", [](auto &oi) {
          *oi++ = '{';
          *oi++ = '}';
        });
      } else {
        oo.def_fx("layout", [&li_json = li->json](auto &oi) {
          for (auto const &c : li_json) {
            *oi++ = c;
          }
        });
      }
    }
    response += "}";
    return bg_work_result{response};
  }


}
