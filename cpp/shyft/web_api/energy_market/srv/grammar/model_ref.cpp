#include <shyft/web_api/energy_market/srv/grammar.h>

namespace shyft::web_api::grammar {
  template <class Iterator, class Skipper>
  model_ref_grammar<Iterator, Skipper>::model_ref_grammar()
    : model_ref_grammar::base_type(start, "model_ref") {
    // clang-format off
    labels_ = lit('[') >> -(string_ % ',') >> lit(']');
    start = lit('{')
          >> lit("\"host\"") >> ':' >> string_[phx::bind(&model_ref::host, _val) = _1] >> ','
         >> lit("\"port_num\"") >> ':' >> int64_[phx::bind(&model_ref::port_num, _val) = _1] >> ','
         >> lit("\"api_port_num\"") >> ":" >> int64_[phx::bind(&model_ref::api_port_num, _val) = _1] >> ','
         >> lit("\"model_key\"") >> ':' >> string_[phx::bind(&model_ref::model_key, _val) = _1]
         >> (-(lit(',')>>lit("\"stm_id\"") >> ':' >> int64_)[phx::bind(&model_ref::stm_id, _val) = _1])
         >> (-(lit(',')>>lit("\"labels\"") >> ':' >> labels_))[phx::bind(&model_ref::labels, _val) = phx::bind(get_value_or<vector<string>>, _1, vector<string>{})]
         >> lit('}');
    // clang-format on
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct model_ref_grammar<request_iterator_t, request_skipper_t>;
}
