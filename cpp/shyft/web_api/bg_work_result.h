#pragma once
#include <atomic>
#include <memory>
#include <set>
#include <string_view>
#include <vector>

#include <boost/beast/core/flat_buffer.hpp>

#include <shyft/core/subscription.h>

namespace shyft::web_api {
  using observer_base_ = std::shared_ptr<shyft::core::subscription::observer_base>;
  using std::string;
  using std::vector;

  /**
   * @brief the background worker result structure
   * @details
   * Usually just a ready formatted buffer to emit,
   * but also other results, that the front-end io-worker could need
   * in order to maintain something that should span the socket state,
   * like subscription mechnanism.
   * @notes
   * Considered: instead of letting the bg produce the response, delegate that to be done
   *             in the front-end fiber. Yes, could be efficient, producing
   *             output response as a multi-buffer construct-write chain,
   *             but also more involved, since it require a shared_ptr to the
   *             structure (with safe consistent const access during write),
   *             -- so we do simple approach: do this in the bgw, and
   *                   we get a consistent result at exec time, detached from
   *                   data-structures.
   *
   */
  struct bg_work_result {
    boost::beast::flat_buffer response; ///< the response to emit to the client
    observer_base_ subscription;        ///< null or the subscription to add
    string unsubscribe_id;              ///< empty or the sub-id to unsubscribe.

    inline void copy_response(string const & srep) {
      auto n = boost::asio::buffer_copy(response.prepare(srep.size()), boost::asio::buffer(srep));
      response.commit(n);
    }

    //-- useful constructs
    bg_work_result() = default;

    /** just a plain response */
    explicit bg_work_result(string const & srep) {
      copy_response(srep);
    }

    /** plain response, plus unsubscribe_id */
    bg_work_result(string const & srep, string unsub_id)
      : unsubscribe_id{std::move(unsub_id)} {
      copy_response(srep);
    }

    /** plain response, plus a subscription to maintain */
    bg_work_result(string const & srep, observer_base_ s)
      : subscription{std::move(s)} {
      copy_response(srep);
    }
  };

  /**
   * @brief Simplest possible structure to deal with server auth tokens
   */
  struct server_auth {
    std::set<string> auth_tokens; ///< list of valid auth tokens

    /**
     * @brief Check if this api requires auth
     *
     * @return true if api is configured to require authentication/authorization
     */
    [[nodiscard]] bool needed() const;

    /**
     * @brief check if token is valid
     * @details If the token is in the auth list, then its considered as ok.
     * This puts the task of adding the correct tockens
     * to the orcestration part of the code hosting the service.
     * Needless to say, the requirement for safe handling of the tokens
     * both on the client and the server apps are imperative.
     * @return true if token compares to one in the list.
     */
    [[nodiscard]] bool valid(string const & token) const;

    void add(vector<string> const & tokens);
    void remove(vector<string> const & tokens);
    [[nodiscard]] vector<string> tokens() const;
  };

  /**
   * @brief The Base request handler class for the boost-beast web_api
   * @details Contains needed protocols for dispatching background worker threads, as well as auth support.
   */
  struct base_request_handler {
    std::atomic_bool running; ///< true when starting to serve, false before and after serving
    server_auth auth;         ///< the _basic_ auth handling is delegated here for the https/wss requests
    std::atomic_int64_t real_port{0};         ///< the real port, either configured, or auto port., valid after running

    virtual ~base_request_handler() =default;

    /**
     * @brief Do the bacground work on separate thread-pool
     * @details The `input` parameter contains the web socket message, the impl. should parse/interpret
     * the contents, and then return with bg_work_result filled in.
     * @param input the web socket message
     * @return filled in `bg_work_result`
     */
    [[nodiscard]] virtual bg_work_result do_the_work(string const & input) = 0;

    /**
     * @brief Do the subscription work in background
     * @details The web-api methods/calls that supports subscription
     * needs to implement methods to check/update(as in generate update) the subscriptions,
     * as described by the observer_base.
     */
    [[nodiscard]] virtual bg_work_result do_subscription_work(observer_base_ const & o) = 0;

    /**
     * Wait until running, then wait for real_port different from 0
     * if 0 is returned, it failed, either on running or timeout
     * @param max_ms_wait for completion milliseconds
     * @return real_port listening for the connections
     */
    [[nodiscard]] std::int64_t wait_for_real_port(int max_ms_wait=5000) const {
      std::chrono::milliseconds waited{0};
      std::chrono::milliseconds dt{2};
      std::chrono::milliseconds max_wait{max_ms_wait};
      while(!running && waited<max_wait) {
        std::this_thread::sleep_for(dt);
        waited += dt;
      }
      while(running && real_port.load()==0 && waited <max_wait) {
        std::this_thread::sleep_for(dt);
        waited += dt;
      }
      return real_port.load();
    }


  };

  /**
   * @brief run_web_server
   *
   * Start the beast flex advanced web-server with background request-handler for long running ops,
   * with specified document root and port parameters.
   *
   * To stop the server, use std::raise(SIGTERM) (from another thread)
   *
   * When signals are caught, the web-server will try to do graceful close, join the worker threads, and return 0.
   *
   * bg_request_handler the type that is required to keep the bg-server, and providing the do_the_work(req)->buffer
   * method
   *
   * @param bg_server of type bg_request_handler, reference, we do not take a copy, just forward the reference, so it's
   * life time should outperform this call
   * @param address_s the listening ip-port number, so that you can be selective regarding scope
   * @param port the ip port number to listen on
   * @param doc_root the document root where you can stash the serverside plain mime-type docs.
   * @param threads number of io-threads to serve the front-end
   * @param bg_threads number of io-threads to serve for long running requests
   * @param tls_only if true accept only tls sessions, https/wss, otherwise accept unencrypted as well
   */

  extern int run_web_server(
    base_request_handler& bg_server,
    std::string address_s,
    unsigned short port,
    std::shared_ptr<std::string const> doc_root,
    int threads,
    int bg_threads,
    bool tls_only = false);
}
