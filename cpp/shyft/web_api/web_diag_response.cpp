
#include <fmt/core.h>

#include <shyft/web_api/generators/escaped_string.h>
#include <shyft/web_api/web_diag_response.h>

namespace shyft::web_api {

  std::string safe_json_diag(std::string_view s) {
    std::string diag;
    auto bi = std::back_inserter(diag);
    generator::escaped_string_generator<decltype(bi)> esc_s;
    generate(bi, esc_s, s);
    return diag;
  }

  /** ref header file */
  std::string gen_diagnostics_response(std::string_view req_id, std::string_view diag) {
    return fmt::format("{{\"request_id\":\"{}\",\"diagnostics\":\"{}\"}}",req_id,safe_json_diag(diag));
  }

  /** ref header file */
  std::string recover_request_id(std::string_view request) {
    auto r_pos = request.find("request_id");
    if (r_pos == std::string::npos)
      return "";
    auto c_pos = request.find(":", r_pos);
    if (c_pos == std::string::npos)
      return "";
    auto q1_pos = request.find("\"", c_pos + 1);
    if (q1_pos == std::string::npos)
      return "";
    auto q2_pos = request.find("\"", q1_pos + 1);
    if (q2_pos == std::string::npos)
      return "";
    if (q2_pos - q1_pos > 200)
      return ""; // give up,dont allow crap strings
    return std::string(request.substr(q1_pos + 1, q2_pos - q1_pos - 1));
  }

}
