#include <iostream>
#include <string>

#include <boost/beast/core/string.hpp>
#include <boost/beast/version.hpp>
#include <boost/system/error_code.hpp>

#include <shyft/web_api/beast_server.h>

#include "targetver.h"

#if BOOST_BEAST_VERSION >= 248
#include <boost/asio/error.hpp>
#include <boost/beast/ssl.hpp>
namespace net = boost::asio;
#endif

namespace shyft::web_api {
  using boost::system::error_code;
  using boost::beast::string_view;
  using std::string;

  /**  Return a reasonable mime type based on the extension of a file.*/
  string_view mime_type(string_view path) {
    using boost::beast::iequals;
    auto const ext = [&path] {
      auto const pos = path.rfind(".");
      if (pos == string_view::npos)
        return string_view{};
      return path.substr(pos);
    }();
    if (iequals(ext, ".htm"))
      return "text/html";
    if (iequals(ext, ".html"))
      return "text/html";
    if (iequals(ext, ".php"))
      return "text/html";
    if (iequals(ext, ".css"))
      return "text/css";
    if (iequals(ext, ".txt"))
      return "text/plain";
    if (iequals(ext, ".js"))
      return "application/javascript";
    if (iequals(ext, ".json"))
      return "application/json";
    if (iequals(ext, ".xml"))
      return "application/xml";
    if (iequals(ext, ".swf"))
      return "application/x-shockwave-flash";
    if (iequals(ext, ".flv"))
      return "video/x-flv";
    if (iequals(ext, ".png"))
      return "image/png";
    if (iequals(ext, ".jpe"))
      return "image/jpeg";
    if (iequals(ext, ".jpeg"))
      return "image/jpeg";
    if (iequals(ext, ".jpg"))
      return "image/jpeg";
    if (iequals(ext, ".gif"))
      return "image/gif";
    if (iequals(ext, ".bmp"))
      return "image/bmp";
    if (iequals(ext, ".ico"))
      return "image/vnd.microsoft.icon";
    if (iequals(ext, ".tiff"))
      return "image/tiff";
    if (iequals(ext, ".tif"))
      return "image/tiff";
    if (iequals(ext, ".svg"))
      return "image/svg+xml";
    if (iequals(ext, ".svgz"))
      return "image/svg+xml";
    return "application/text";
  }

  /**
   * Append an HTTP rel-path to a local filesystem path.
   * The returned path is normalized for the platform.
   */
  string path_cat(string_view base, string_view path) {
    if (base.empty())
      return string{path};
    string result{base};
#if defined(BOOST_MSVC)
    char constexpr path_separator = '\\';
    if (result.back() == path_separator)
      result.resize(result.size() - 1);
    result.append(path.data(), path.size());
    for (auto& c : result)
      if (c == '/')
        c = path_separator;
#else
    char constexpr path_separator = '/';
    if (result.back() == path_separator)
      result.resize(result.size() - 1);
    result.append(path.data(), path.size());
#endif
    return result;
  }

  void fail(error_code ec, char const * what) {
#if BOOST_BEAST_VERSION >= 248
    // ssl::error::stream_truncated, also known as an SSL "short read",
    // indicates the peer closed the connection without performing the
    // required closing handshake (for example, Google does this to
    // improve performance). Generally this can be a security issue,
    // but if your communication protocol is self-terminated (as
    // it is with both HTTP and WebSocket) then you may simply
    // ignore the lack of close_notify.
    //
    // https://github.com/boostorg/beast/issues/38
    //
    // https://security.stackexchange.com/questions/91435/how-to-handle-a-malicious-ssl-tls-shutdown
    //
    // When a short read would cut off the end of an HTTP message,
    // Beast returns the error beast::http::error::partial_message.
    // Therefore, if we see a short read here, it has occurred
    // after the message has been completed, so it is safe to ignore it.

    if (ec == net::ssl::error::stream_truncated)
      return;
    if (ec == net::error::bad_descriptor && what == string("shutdown.https"))
      return; // We get this during shutdown https, it's not clear how we could/should handle it
#endif
    // no output: std::cerr << what << ": ("<<ec.value()<<") " << ec.message() << "\n";
  }

  using std::string;
  using std::shared_ptr;
  using std::make_shared;
  using std::optional;

  int run_web_server(
    base_request_handler& bg_server,
    string address_s,
    unsigned short port,
    shared_ptr<string const> doc_root,
    int num_threads,
    int num_bg_threads,
    bool tls_only) {

    num_threads = std::max(1, num_threads); // at least one counting main thread
    num_bg_threads = std::max(0, num_bg_threads);

    auto const address = boost::asio::ip::make_address(address_s);

    // shorthand for now, using web-api request handler.
    using bg_work = bg_worker<base_request_handler>;

    // The io_context is required for all I/O
    net::io_context ioc{num_threads};
    net::io_context bg_ioc{num_bg_threads};
    auto bg_work_lock = net::make_work_guard(
      bg_ioc);                      // keep alive the bg_work io-service until we zero out the work flag
    bg_work bgw{bg_server, bg_ioc}; // background worker consist of a server and an ioc that we can post on

    // The SSL context is required, and holds certificates
    ssl::context ctx{ssl::context::tls};
    ctx.set_default_verify_paths();

    // This holds the self-signed certificate used by the server
    load_server_certificate(ctx);

    // Create and launch a listening port
    // it needs to carry all the stuff
    // that needs to passed on to
    // the down-stream classes to handle
    // incoming connections, including the background worker
    make_shared<listener<bg_work>>(ioc, ctx, tcp::endpoint{address, port}, doc_root, bgw, tls_only)
      ->run(); // will hopefully post work to the ioc, that will get alive later when we start the run.


    // This defines what spawned threads do
    auto run_context = [&bg_server](auto ioc_ref) -> void {
      while (bg_server.running) {
        try {
          ioc_ref.get().run(); // for non-bg threads this does not return until ioc is stopped
        } catch (std::exception const & ex) {
          std::cerr << fmt::format("beast server: terminated with exception: {}\n", ex.what());
        }
      }
    };

    // main thread also closes all io_context when the server is shutting down
    auto run_main = [&]() -> void {
      while (bg_server.running) {
        try {
          ioc.run_for(std::chrono::milliseconds(100l)); // run_for returns after set time
        } catch (std::exception const & ex) {
          std::cerr << fmt::format("beast server: terminated with exception: {}\n", ex.what());
        }
      }
      // server shutting down; stop the io contexts so that spawned threads can return
      bgw.ioc.stop();
      ioc.stop();
    };

    // Capture SIGINT and SIGTERM to trigger a clean shutdown
    boost::asio::signal_set signals(ioc, SIGINT, SIGTERM);
    signals.async_wait([&bg_server](beast::error_code const &, int) {
      bg_server.running = false;
    });

    // Run the I/O service on the requested number of threads
    bg_server.running = true;
    auto threads = std::vector<std::jthread>{};
    threads.reserve(num_threads + num_bg_threads - 1); // the -1 accounts for the main thread
    while (threads.size() + 1 < static_cast<std::size_t>(num_threads))
      threads.emplace_back(run_context, std::ref(ioc));
    while (threads.size() + 1 < static_cast<std::size_t>(num_threads + num_bg_threads))
      threads.emplace_back(run_context, std::ref(bg_ioc));

    run_main(); // blocking until shutdown

    // remove the motivation for the bg.io_context to stay alive (but we did actually stop above..)
    bg_work_lock.reset();
    return 0;
  }
}
