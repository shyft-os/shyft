#pragma once
#include <string>
#include <string_view>

namespace shyft::web_api {

  /**
   * @brief safe_json_diag quote etc. of a string
   * @details
   * So it can be passed back as a string to the caller over a json diagnostics.
   */
  std::string safe_json_diag(std::string_view s);

  /**
   * @brief gen_diagnostics_response
   * @details
   * make a valid request_id tagged diagnostics response, take care of quotes
   * of the form `{ "request_id": "req_id","diagnostics":"quoted diag"}`
   */
  std::string gen_diagnostics_response(std::string_view req_id, std::string_view diag);

  /**
   * @brief recover_request_id
   * @details
   * Brute forece fuzzy request-id recovery
   * because we want to keep the request-id topic for the
   * web/app.
   *
   * @return request_id if found or ""
   */
  std::string recover_request_id(std::string_view request);

  static constexpr std::string_view request_id_not_found{"__request_id_not_found__"};
}
