#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline store_ts_request mk_store_ts_request(
    std::string const & req_id,
    bool merge_store,
    bool recreate_ts,
    bool cache,
    boost::optional<bool> strict,
    ats_vector const & tsv) {
    return store_ts_request{req_id, merge_store, recreate_ts, cache, tsv, strict.get_value_or(true)};
  }

  template <typename Iterator, typename Skipper>
  store_ts_request_grammar<Iterator, Skipper>::store_ts_request_grammar()
    : store_ts_request_grammar::base_type(start, "store_ts_request") {
    // clang-format off
    start =
      (lit("store_ts") > lit('{') >
        lit("\"request_id\"") > ':' > quoted_string > ',' >
        lit("\"merge_store\"") > ':'> bool_ > ',' >
        lit("\"recreate_ts\"") > ':' > bool_ > ',' >
        lit("\"cache\"") > ':' > bool_ > ',' >
        -(lit("\"strict\"") > ':' > bool_ > ',') >
        lit("\"tsv\"") > ':' > tsv_ >
      lit('}'))
      [_val = phx::bind(mk_store_ts_request, _1, _2, _3, _4, _5,_6)];
    // clang-format on
    start.name("store_ts_request");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct store_ts_request_grammar<request_iterator_t, request_skipper_t>;

}
