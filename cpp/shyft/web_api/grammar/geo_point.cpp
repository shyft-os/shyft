#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline geo_point mk_geo_point(double x, double y, double z) {
    return {x, y, z};
  }

  template <typename Iterator, typename Skipper>
  geo_point_grammar<Iterator, Skipper>::geo_point_grammar()
    : geo_point_grammar::base_type(start, "geo_point_grammar") {
    start =
      (lit('{') >> lit("\"x\"") >> lit(':') >> double_ >> lit(',') >> lit("\"y\"") >> lit(':') >> double_ >> lit(',')
       >> lit("\"z\"") >> lit(':') >> double_ >> lit('}'))[_val = phx::bind(mk_geo_point, _1, _2, _3)];
    on_error<fail>(start, error_handler(_4, _3, _2));
  }
  template struct geo_point_grammar<request_iterator_t, request_skipper_t>;

}
