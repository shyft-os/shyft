#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  inline unsubscribe_request mk_unsubscribe_request(std::string const & req_id, std::string const & sub_id) {
    return unsubscribe_request{req_id, sub_id};
  }

  template <typename Iterator, typename Skipper>
  unsubscribe_request_grammar<Iterator, Skipper>::unsubscribe_request_grammar()
    : unsubscribe_request_grammar::base_type(start, "unsubscribe_request_grammar") {

    start =
      (lit("unsubscribe") > lit('{') > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"subscription_id\"")
       > ':' > quoted_string > lit('}'))[_val = phx::bind(mk_unsubscribe_request, _1, _2)];
    start.name("unsubscribe_request");
    on_error<fail>(start, error_handler(_4, _3, _2));
  }

  template struct unsubscribe_request_grammar<request_iterator_t, request_skipper_t>;
}
