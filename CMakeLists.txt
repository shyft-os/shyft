cmake_minimum_required(VERSION 3.27 FATAL_ERROR)

include(${CMAKE_CURRENT_LIST_DIR}/tools/cmake/shyft_prologue.cmake)

project(
    shyft
    LANGUAGES CXX
    VERSION ${SHYFT_VERSION_MAJOR}.${SHYFT_VERSION_MINOR}.${SHYFT_VERSION_PATCH}
    DESCRIPTION ${SHYFT_DESCRIPTION}
    HOMEPAGE_URL ${SHYFT_URL}
)
include(shyft_config)
if(SHYFT_WITH_COVERAGE)
  include(shyft_coverage)
endif()

add_subdirectory(cpp)

if(SHYFT_WITH_SHOP_REGEN)
  add_subdirectory(tools/shop_gen)
endif()

if(SHYFT_WITH_VERSION_FORGE)
  add_subdirectory(tools/version_forge)
endif()

if(SHYFT_WITH_PYTHON)
  add_subdirectory(python)
endif()

if(SHYFT_WITH_BENCHMARKS)
  add_subdirectory(benchmarks)
endif()

shyft_install_cmake_package_targets(COMPONENT development TARGET_COMPONENTS runtime)
shyft_install_cmake_package_config(COMPONENT development)
shyft_install_cmake_package_configs()

shyft_install_licenses()

if(SHYFT_WITH_CPACK)
  include(shyft_cpack)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/tools/cmake/shyft_epilogue.cmake)
